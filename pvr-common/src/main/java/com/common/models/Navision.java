package com.common.models;

import javax.persistence.Transient;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "navision")
@JsonInclude(content = Include.NON_NULL)
public class Navision extends AbstractEntity {

	private static final long serialVersionUID = -4047610681277858850L;

	@Id
	private String id;
	private String year;
	private String quater;
	private String misLoc;
	private String navCinemaCode;
	private String cinemaName;
	private String city;
	private String state;
	private int noOfScreen;
	private String compType;
	private long personalExpense;
	private long electricAndWaterExpense;
	private long rAndmEngg;
	private long rAndmIt;
	private long rAndMFb;
	private long houseKeepingCharges;
	private long securityCharges;
	private long marketingExpenses;
	private long printingAndStationary;
	private long communicationCost;
	private long internateCharges;
	private long legalAndProfessional;
	private long travellingAndConveyance;
	private long insuranceExpanse;
	private long miscellaneousExpense;
	private long advertisement;
	private long cam;
	
	@Transient
	private double distance;

}
