package com.common.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "state_tier")
@JsonInclude(content = Include.NON_NULL)
public class CityTier extends AbstractEntity {

	private static final long serialVersionUID = -4047610611277858850L;

	@Id
	private String id;
	private String city;
	private String tier;

}
