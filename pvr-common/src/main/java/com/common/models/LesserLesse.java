package com.common.models;

import java.util.HashMap;

import com.common.constants.Constants;

import lombok.Data;

//@AllArgsConstructor
@Data
/*@Getter
@Setter*/
public class LesserLesse {
	
	private Integer leasePeriod;
	private Integer pvrLockInPeriod;
	private Integer devloperLockedInPeriod;
	private Double coveredArea;
	private Double carpetArea;
	private Double loadingPercentage;
	private Double leasableArea;
	private Double height;
	private String rentPaymentTerm;
	private Integer rent;
	private Integer rentFrequencyOfEscaltionYears;
	private Integer percentageEscalationInRent;
	private Integer boxOfficePercentage;
	private Integer fAndBPercentage;
	private Integer startingYearForMG;
	private String mgType;
	private String camPaymentTerm;
	private Integer camRate;
	private Integer percentageEscalationInCam;
	private Integer frequencyOfEscaltionYears;
	private Integer securityDeposit;
	private Integer camSecurityDeposit;
	private Double utilitySecurityDeposit;
	private Integer additionDepositOnEscaltion=Constants.ZERO;
	private String devloperScopeOfWorkOption;
	private Integer securityDepositForZeroMg;
	private String paymentSchedule;
	private Double fromDevloper;
	private Double toDevloper;
	private Double propertyTax;
	
	private String camType;
	private String rentType;
	
	private Integer securityDepositForMonthCam;
	private Integer securityDepositForMonthRent;
	
	private Double camSecurityDepositForFixedZeroMg;
	private Double rentSecurityDepositForFixedZeroMg;
	
	private Double advanceRent;
    private Integer advanceRentPeriod;
    private HashMap<Integer,Double> yearWiseAdvanceRentAdjustment;

}
