package com.common.models;

import java.util.List;

import com.common.mappers.QueryMapper;

import lombok.Data;

@Data
public class FieldValue {
	private long suggestedValues;
	private Double inputValues;
	private String comments;
	private Double costType;
	private String lastQueryUserId;
	private List<QueryMapper> query;

}