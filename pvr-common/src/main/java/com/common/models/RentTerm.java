package com.common.models;

import java.util.HashMap;

import lombok.Data;

@Data
public class RentTerm {
	
	
	private String paymentTerm;
	private Double rent;
	private Integer freqOfEscalation;
	private Double percOfEscalation;
	private String explainRentAgreement;
	
	private HashMap<Integer, Double> yearWiseRent; 
	
	//Revenue Share
	private Double boxOfficePercentage;
	private Double fAndBPercentage;
	private Double adSalePercentage;
	private Double othersPercentage;
	private Double percentageEscalationInRent_fixed;
	private Double rentFrequencyOfEscaltionYears_fixed;
	
	private HashMap<Integer, Double> yearWiseBoxOfficePercentage; 
	private HashMap<Integer, Double> yearWiseFAndBPercentage; 
	private HashMap<Integer, Double> yearWiseAdSalePercentage; 
	private HashMap<Integer, Double> yearWiseOthersPercentage; 
	
	private HashMap<Integer,Double> yearWiseRentAfterAdjustment;
	

}
