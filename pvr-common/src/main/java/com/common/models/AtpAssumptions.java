package com.common.models;

import java.util.List;

import com.common.mappers.QueryMapper;

import lombok.Data;

@Data
public class AtpAssumptions {

	private String cinemaFormatType;
	private AssumptionsValues normal;
	private AssumptionsValues recliner;
	private AssumptionsValues lounger;
	private List<QueryMapper> query;
	private AssumptionsValues blendedAtp;

}
