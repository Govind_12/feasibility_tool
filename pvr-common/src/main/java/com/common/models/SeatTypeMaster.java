package com.common.models;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "seat_type_master")
@JsonInclude(content = Include.NON_NULL)
public class SeatTypeMaster extends AbstractEntity{
	@Id
	private String id;
	private String seatType;
	private List<String> seatSubType;
}
