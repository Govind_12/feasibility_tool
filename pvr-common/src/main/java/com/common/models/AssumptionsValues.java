package com.common.models;

import java.util.List;

import com.common.mappers.QueryMapper;

import lombok.Data;

@Data
public class AssumptionsValues {
	
	
	private String formatType;
	private Double suggestedValue;
	private Double inputValues;
	private String comments;
	private Long costType;
	private SeatType admits;
	private String lastQueryUserId;
	private List<QueryMapper> query;
	private Boolean isPresent;
}
 