package com.common.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "log_detail")
@JsonInclude(content = Include.NON_NULL)
public class LogDetail extends AbstractEntity {

	private static final long serialVersionUID = -4047610611277858850L;

	@Id
	private String id;
	private String requestTime;
	private String requestURI;
	private String requestIP;
	private String requestBrowser;
	private String requestStatus;
	private String exceptionInCase;
	private String username;
	private String fullName;
	private String loginTime;
	private String logoutTime;
	private String feasibilityAccessed;
	private String token;
	private long loginTimeLong;

}
