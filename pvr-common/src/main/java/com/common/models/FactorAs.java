package com.common.models;

import lombok.Data;

@Data
public class FactorAs {
private float means;
private float consumption;
private float awareness;
private float mktSupport;
}
