package com.common.models;

import lombok.Data;

@Data
public class SeatType {
	private long normals;
	private long recliner;
	private long longers;
}
