package com.common.models;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Data
@Setter
@Getter
public class PropertyDetail {
	
	private String propertyName;
	private String address;
	private Integer noOfScreen;
	private Integer seatCapacity;
	private String tenatativeOpeningDate;
	private ProjectLocation projectLocation;

	private Integer date;
}
