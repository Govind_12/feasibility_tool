package com.common.models;

import java.util.List;
import java.util.Map;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

import com.common.mappers.MsgMapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "assumptions_calculation")
@JsonInclude(content = Include.NON_NULL)
public class AssumptionsCalculationData {
	
	@Id
	private String id;
	private String projectId;
	private Double totalshow;
	private Double totalCapacity;
	private Map<String, Map<String, Double>> capacityforEachScreenSeatType;
	private Map<String,Double> occupancyForEachScreenType;
	private Map<String, Map<String, Double>> admitsForEachScreenSeatType;
	private Map<String,Double> admitsForEachScreenType;
	private Double totalAdmits;
	private Map<String, Map<String, Double>> atpForEachScreenSeatType;
	private Map<String,Double> totalAtpForEachFormat;
	private Map<String,Double> totalAtpForEachSeatType;
	private Double totalAtp;
	private Double grossBoxOfficeCollection;
	private Map<String,Double> grossBoxOfficeCollectionForEachFormat;
	private Map<String,Double> gstOnTicketRevenueForEachFormat;
	private Map<String,Double> netBoxOfficeCollectionForEachFormat;
	private Double totalNetBoxOfficeCollection;
	private Double netGstOnBoxOffice;
	private Map<String,Double>  formatWiseFilmHireCost;
	private Double totalFilmHireCost;
	private Double totalFilmHireCostPercentage;
	private Map<String,Double> sphForEachScreeenType;
	private Double totalSph;
	private Map<String,Double> grossFoodAndBeveragesRevenueByScreenType;
	private Map<String,Double> netFoodAndBeveragesRevenueByScreenType;
	private Map<String,Double>  cogsByScreenType;
	private Double vpfIncome;
	private Double webSalePercentageOfTotalSale;
	private Double netConvenienceFee;
	private Double municipalTaxesPerAnnum;

}
