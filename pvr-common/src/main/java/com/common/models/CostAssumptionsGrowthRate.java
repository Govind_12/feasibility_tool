package com.common.models;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
public class CostAssumptionsGrowthRate {

	
	private Map<Integer,Double> personnelExpenses;
	private Map<Integer,Double> electricityAndAirconditioning;
	private Map<Integer,Double> repairAndMaintenance;
	private Map<Integer,Double> otherOverheadExpenses;
	private Map<Integer,Double> propertyTax;
	
}
