package com.common.models;

import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@Document(collection = "compition_master")
@JsonInclude(content = Include.NON_NULL)
public class CompitionMaster extends AbstractEntity{
	
	private static final long serialVersionUID = -4847610611277958850L;
	
	@Id
	private String id;
	private Integer hopk;
	private String compititionName;
	private String city;
	private String compitionChain;
	private Integer noOfScreen;
	private Long seatCapacity;
	private Long admits;
	private Integer atp;
	private Double occupancy;
	private Double latitude;
	private Double longitude;

	@Transient
	private double distance;
	@Transient
	private char alphaCounter;
	
}
