package com.common.models;

import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "multipliers")
@JsonInclude(content = Include.NON_NULL)
public class MultipliersMaster extends AbstractEntity {

	@Id
	private String id;
	private String costType;
	private Double singleScreenMultiplier;
	private Double goldMultiplier;
	private Double dxMultiplier;
	private Double iconMutiplier;
	private Double imaxMultiplier;
	private Integer status;
	
	@Transient
	private String costTypeText;
}
