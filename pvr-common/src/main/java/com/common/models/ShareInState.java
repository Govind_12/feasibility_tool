package com.common.models;

import lombok.Data;

@Data
public class ShareInState {

	private float mpv;
	private float popn;
	
}
