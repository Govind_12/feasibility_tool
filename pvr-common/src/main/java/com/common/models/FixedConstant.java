package com.common.models;

import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.common.mappers.FhcMapper;
import com.common.mappers.OperatingMapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@Document(collection = "fixed_constant")
@JsonInclude(content = Include.NON_NULL)
public class FixedConstant  extends AbstractEntity{
	
	private static final long serialVersionUID = -5317602511431088794L;
	
	@Id
	private String id; 
	
	private  Map<String,Double> avgShowPerDay;
	private Integer workingDays;
	private Double yearOneFootfallAssumedLowerBy;
	private Double complimentaryTicketsPercent;
	private Double complimentaryFnBPercent;
	private Double yearOneAdIncomeLowerByPercent;
	private Double fnbGst;
	private Double fnbCogs;
	private Double netConvenienceFeePerTicket;
	
	private Map<String,Double> cogs;
	private Double factor;
	private Map<String,Double> filmHire;

	private Double tax;
	
	private RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate;
	private CostAssumptionsGrowthRate costAssumptionsGrowthRate;
	
	private List<AtpCapDetials> atpCapDetails;
	
	private Map<String,Double> healthCess;
	private Map<String,Double> serviceChargePerTicket;
	private Map<String,Double> lbtAverageRate;
	private Double income3DAsPercentOfGboc;
	
	private Double initailCapexInOneYear;
	private Double initailCapexInZeroYear;
	private Double initailCapexInSevenYear;
	private Double debtProportion;
	private Double equityProportion;
	private Double costOfDebt;
	private Double debtTenure;
	private Double costOfEquity;
	private Double capexContingency;
	private Double incomeTax;
	
	private GstType personnel;
	private GstType repairAndMaintenance;
	private GstType rent;
	private GstType cam;
	private GstType electricityAndWater;
	private GstType other;
	private GstType capexWOInputCredit;
	private GstType capexWPartialInputCredit;
	private GstType capexWFullInputCredit;
	
	private Double rentAndCamDiscountRate;
	
	private Double maintenanceCapexOfRevenueForAllYearTillLastRenovation;
	private Double maintenanceCapexOfRevenueForYearAfterLastRenovation;
	
	private Double marketingExpenseLaunch;
	
	private Integer adminLeasePeriod;
	
	private Double sphToAtpRatio;

	Map<String, Double> gstOnTicketForEachFormat ;
	
	@Transient
	private  FhcMapper fhcMapper;
	
	@Transient
	private List<OperatingMapper> operating;
	
	private List<VpfIncomeDetails> vpfIncomeDetails;
	
	@Transient
	private Double cap;
	
	@Transient
	private String seatType;
	
	@Transient
	private String screenType;
	
	private String projectLocation;
	
	private RevenueAssumptionsGrowthRate adminRevenueAssumptionsGrowthRate;
	private Double atpGrowthRateNormalYearOne;
	private Double atpGrowthRateReclinerYearOne;
	private Double atpGrowthRateLoungerYearOne;
	
}
