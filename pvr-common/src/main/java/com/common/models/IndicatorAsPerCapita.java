package com.common.models;

import lombok.Data;

@Data
public class IndicatorAsPerCapita {

	private float income;
	private float bankDeposit;
	private float affluentPopn;
	
	
}
