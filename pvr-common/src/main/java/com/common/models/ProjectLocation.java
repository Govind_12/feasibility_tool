package com.common.models;

import lombok.Data;

@Data
public class ProjectLocation {

	private String city;
	private String state;
	private String locality;
	private long pincode;
	private Double latitude;
	private Double longitude;
	private String cityTier;
	
}
