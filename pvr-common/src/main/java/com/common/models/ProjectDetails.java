package com.common.models;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Transient;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

import com.common.constants.Constants;
import com.common.mappers.MsgMapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "project_details")
@JsonInclude(content = Include.NON_NULL)
public class ProjectDetails {

	private static final long serialVersionUID = -4047610611277858850L;

	@Id
	private String id;
	private String reportId;
	private Integer reportIdSubVersion;
	private Integer reportIdVersion;
	private String reportType=Constants.REPORT_TYPE_NEW;
	private String feasibilityState;
	private String projectType;
	private String projectName;
	private ProjectLocation projectLocation;
	private String developer;
	private String developerOwner;
	private String grossTurnOver;
	private String projectDelivered1;
	private String projectDelivered2;
	private String projectDelivered3;
	private String retailProjectDelivered;
	private Double totalDevlopedAreDelivered;
	private List<Integer> pvrProjectDelivered;
	private List<String> pvrProjectPipeline;
	private Double totalDeveloperSize;
	private String currentProjectFinancing;
	private Double retailAreaOfProject;
	private String tenancyMix;
	private String upcomingInfrastructureDevlopment;
	// private ProjectInfrastructure projectInfraStructure;
	private List<CinemaFormat> cinemaFormat;
	private List<CinemaFormat> seatDimension;
	private String projectCostUrl;
	private int status;
	private String DateOfHandover;
	private List<PropertyDetail> pvrProperties;
	private List<PropertyDetail> competitveProperties;
	private String DateOfOpening;
	private List<String> chat;
	private Integer noOfScreens;
	private Integer normalsValSum;
	private Integer reclinerValSum;
	private Integer loungersValSum;
	private Integer totalColumnValSum;
	private Integer avgNoOfShows;
	private String cinemaCategory;
	private RevenueAssumption incomeApplicable;
	private RevenueAssumption serviceCharge;
	private RevenueAssumption lbtCharge;
	private LesserLesse lesserLesse;
	// private RentAgreemement rentAgreement;
	private Double projectCost;
	private List<AssumptionsValues> occupancy;
	private List<AtpAssumptions> atp;
	private List<AssumptionsValues> sph;
	private AssumptionsValues adRevenue;
	
	
	private FinalSummary finalSummary;
	private AssumptionsValues webSale;
	private OtherParameter miscIncome;
	private AssumptionsValues personalExpenses;
	private AssumptionsValues electricityWaterBill;
	private AssumptionsValues totalRMExpenses;
	private AssumptionsValues totalOverhead;
	private AssumptionsValues houseKeepingExpense;
	private AssumptionsValues securityExpense;
	private AssumptionsValues communicationExpense;
	private AssumptionsValues travellingAndConveyanceExpense;
	private AssumptionsValues legalFee;
	private AssumptionsValues insuranceExpense;
	private AssumptionsValues internetExpense;
	private AssumptionsValues printingAndStationaryExpense;
	private AssumptionsValues marketingExpense;
	private AssumptionsValues miscellaneousExpenses;
	private Double totalOverheadSuggestedSum;
	private Double totalOverheadInputSum;
	
	private OtherParameter showTax;
	private boolean income3dApplicable;
	private Double depreciation;
	private List<AtpAssumptions> admits;
	private float cityPopulation;
	
	
	private String rejectedBy;
	@Transient
	private boolean flag;

	private String fileURLStep1;

	private String fileURLStep2;
	
	private String fileURLStep5;
	@Transient
	private MultipartFile uploadFileStep1;
	
	@Transient
	private MultipartFile uploadFileStep2;
	
	@Transient
	private MultipartFile uploadFileStep5;

	@Transient
	private String approverId = Constants.APPROVER1;

	@Transient
	private MsgMapper msgMapper;

	public RentTerm rentTerm;

	public CamTerm camTerm;

	@Transient
	private String reportSignature;
	
	@Transient
	private Double areaPerSeat;

	@Transient
	private Integer totalSeats;
	
	@Transient
	private Boolean isProjectRole=false;
	
	private Boolean isProjectSubmit=false;
	private Boolean isOperationSubmit=false;
	private String projectBdId;
	private Boolean reportStatus=false;
	
	private Double projectCostPerScreen;
	
	@Transient
	private MsgMapper rejectReason;
	
	private String rejectReasonMsg;
	
	private String DateOfSendForApproval;
	
	@Transient
	private boolean pvrProjectDeliveredFlag;
	
	@Transient
	private String dashboardStatusName;
	
	@Transient
	private String dashboardReviewButtonFlag;
	
	@Transient
	private String dashboardEditButtonFlag;
	
	@Transient
	private String dashboardUpdateButtonFlag;
	
	@Transient
	private String dashboardDownloadButtonFlag;
	
	@Transient
	private String dashboardArchiveButtonFlag;
	
	@Transient
	private String dashboardDeleteButtonFlag;
	
	
	
    protected Long createdDate;
    protected String createdBy;
    protected Long updatedDate;
    protected String updatedBy;
    
    public void setCreatedDate(){
		  this.createdDate = System.currentTimeMillis();
	  }
	  
	  public void setUpdatedDate(){
		  this.updatedDate = System.currentTimeMillis();
	  }
	  
	  public void createdBy(String createdBy){
		  this.createdBy = createdBy;
	  }
	  
	  public void updatedBy(String updatedBy){
		  this.updatedBy = updatedBy;
	  }
	
	  private AssumptionsValues operatingOccupancyquery;
	  private AssumptionsValues operatingAtpquery;
	  private AssumptionsValues operatingSphquery;
	  private AssumptionsValues operatingProjectquery;
	  
	  @Transient
	  private String reportsImagePath; 
	  
	  @Transient
	  private String googleImagePath; 
	  private String projectReasonMsg;
	  
	  private String operationReasonMsg;
	
	  private Set<Integer> checkStatus;
	  private Boolean isSendForOpsAndProject=false;
	  private Boolean isRejected=false;
	  
	  private AssumptionsValues cogs;
	  private AssumptionsValues noOfShows;
	  private boolean allowSaveAndClose;
	  
	  private RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate;
	  private CostAssumptionsGrowthRate costAssumptionsGrowthRate;
	  private Double yearOneFootfallAssumedLowerBy;
	  private Double yearOneAdIncomeLowerByPercent;
	  
	  @Transient
	  private String projectBdName;
	  
	  private boolean saleModel;
	  private boolean leaseModel;
	  private String architectNameOfMall;
	  private String developerWebsite;
	  private String executiveNote;
	  private Map<String, String> executiveNoteMap; 
	  private String userDesignation;
      private List<String> multipleFileURLStep5;
      
      private Double totalBlendedAtp;

}
