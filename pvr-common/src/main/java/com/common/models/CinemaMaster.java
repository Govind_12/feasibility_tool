package com.common.models;
import javax.persistence.Transient;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@Document(collection = "cinema_master")
@JsonInclude(content = Include.NON_NULL)
public class CinemaMaster extends AbstractEntity {

	private static final long serialVersionUID = -4847610611277858850L;
	

	@Id
	private String id;
	private int hopk;
	private String cinema_strCode;
	private String cinema_name;
	private String vista_info_Cinema_Name;
	private String cinema_location;
	private String state_strName;
	private String navCinemaCode;
	private String owner_strCompany;
	private String cinema_Resign;
	private String nav_cinemadesc;
	private String cinema_str_Run;
	private String cityTier;
	private String infoworks_cin_str;
	private int loyalty_complex_id;
	private double[] location;
	private String cinemaCategory;
	private int noOfScreen;
	private int gold;
	private int pxl;
	private int ultraPremium;
	private int dx;
	private int premier;
	private int imax;
	private int playHouse;
	private int onxy;
	private int mainStream;
	private int totalCapacity;
	private Double latitude;
	private Double longitude;
	
	
	@Transient
	private double distance;

	@Transient
	private String screenType;
	
	@Transient
	private String category;
	

	@Transient
	private Long seatCapacity;
	
	@Transient
	private Long atp;
	
	@Transient
	private Double occupancy;
	
	@Transient
	private Long admits;
	
	@Transient
	private Integer noOfScreens;
	
	@Transient
	private Long sph;
	
	@Transient
	private Double sphAtp;

	@Transient
	private char alphaCounter ;
	
	@Transient
	private Double webSale;
	
	@Transient
	private Long adRevenue;
}
