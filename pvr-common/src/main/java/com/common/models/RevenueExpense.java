package com.common.models;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "revenue_expense")
@JsonInclude(content = Include.NON_NULL)
public class RevenueExpense extends AbstractEntity {

	@Id
	private String id;
	private String projectId;
	private String cinemaFormatType;
	private double occupancy;
	private SeatType admits;
	private SeatType atp;
	private long sph;
}
