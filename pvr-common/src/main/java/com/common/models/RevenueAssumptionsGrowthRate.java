package com.common.models;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Data
public class RevenueAssumptionsGrowthRate {
	
	private Map<Integer,Double> admitsGrowth;
	private Map<Integer,Double> atpGrowth;
	private Map<Integer,Double> sphGrowth;
	private Map<Integer,Double> sponsorshipIncomeGrowth;
	private Map<Integer,Double> convenienceFeeGrowth;
	private Map<Integer,Double> otherIncomeGrowth;

}
