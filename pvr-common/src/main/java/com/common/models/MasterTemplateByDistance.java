package com.common.models;

import java.util.List;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

import com.common.mappers.MsgMapper;
import com.common.sql.models.CinemaMasterSql;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "master_template")
@JsonInclude(content = Include.NON_NULL)
public class MasterTemplateByDistance extends AbstractEntity{
	
	private static final long serialVersionUID = -4047580611277858850L;
	
	@Id
	private String id;
	private String projectId;
	private String templateType;
	private List<CinemaMasterSql> cinemaMaster;
	private List<CompitionMaster> compitionMaster;
}
