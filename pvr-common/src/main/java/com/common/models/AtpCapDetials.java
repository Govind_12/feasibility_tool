package com.common.models;

import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import lombok.Data;
import lombok.NoArgsConstructor;



@NoArgsConstructor
@Data
public class AtpCapDetials {
	
	private String state;
	//private Double atpCap;
	private Double atpGrowthRate;
	private Integer growthYear;
	private Integer startYear;
	private Map<String,Map<String,Double>> atpDetails;
	
	@Transient
	private List<String> seatType;
	
	@Transient
	private List<Double> camValue;
	private String dateOfRevision;
	

}
