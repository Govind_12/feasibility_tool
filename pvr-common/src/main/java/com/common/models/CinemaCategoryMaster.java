package com.common.models;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "cinema_category_master")
@JsonInclude(content = Include.NON_NULL)
public class CinemaCategoryMaster extends AbstractEntity{
	
	@Id
	private String id;
	private String cinemaCategory;
	private List<String> cinemaSubCategory;

}
