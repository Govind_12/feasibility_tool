package com.common.models;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Transient;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "cinema_format_master")
@JsonInclude(content = Include.NON_NULL)
public class CinemaFormatMaster extends AbstractEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String cinemaCategory;
	private List<String> cinemaSubCategory;
	private List<String> cinemaFormatMapping;
	@Transient
	private boolean saveMapping;
	@Transient
	private String checked;
	private double fhcValue;
	
}
