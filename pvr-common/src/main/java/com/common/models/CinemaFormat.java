package com.common.models;

import lombok.Data;

@Data
public class CinemaFormat {

	private String formatType;
	private int normal;
	private int recliners;
	private int lounger;
	private Integer total;
	private String normalSeatDimension;
	private String reclinerSeatDimension;
	private String loungerSeatDimension;
}
