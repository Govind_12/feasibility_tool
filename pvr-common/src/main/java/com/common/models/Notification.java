package com.common.models;

import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.common.constants.Constants;
import com.common.mappers.MsgMapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Document(collection="notification")
@JsonInclude(content=Include.NON_NULL)
@EqualsAndHashCode(callSuper=false)
public class Notification{
	
	@Id
	private String id;
	private String msg;
	private Integer status=1;
	private String type=Constants.ACTION_ITEMS;    //UPDATES or ACTION_ITEMS
	private String projectId;
	private String approverId;
	private String projectType;
	private int projectStatus;
	private String reportId;
	private int actionByType;
	private String actionById;
	private String feasibilityState=Constants.PRE_SIGNING;
	
	@Transient
	private String role;
	
	@Transient
	private String updates;
	
	private MsgMapper msgMapper;
	private String projectName;
	
	private Boolean isProjectSubmit=false;
	private Boolean isOperationSubmit=false;
	private String projectBdId;
	
	private Boolean reportStatus=false;
	
	protected Long createdDate;
    protected String createdBy;
    protected Long updatedDate;
    protected String updatedBy;
    
    public void setCreatedDate(){
		  this.createdDate = System.currentTimeMillis();
	  }
	  
	  public void setUpdatedDate(){
		  this.updatedDate = System.currentTimeMillis();
	  }
	  
	  public void createdBy(String createdBy){
		  this.createdBy = createdBy;
	  }
	  
	  public void updatedBy(String updatedBy){
		  this.updatedBy = updatedBy;
	  }
	
	

}
