package com.common.models;


import java.util.List;

import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@Document(collection = "project_cost")
@JsonInclude(content = Include.NON_NULL)
public class ProjectCostSummary {
	
	private static final long serialVersionUID = -4047610614277858850L;
	
	@Id
	private String id;
	private String parentId;
	private List<ProjectCostData>  projectCostDataList;
	
	
	@Transient
	private Double depreciation;
	
	@Transient
	private Double totalProjectCost;
	
	@Transient
	private Double projectCostPerScreen;

}
