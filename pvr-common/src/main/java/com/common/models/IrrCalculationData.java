package com.common.models;

import java.util.List;
import java.util.Map;

import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "irr_calculation_data")
@JsonInclude(content = Include.NON_NULL)
public class IrrCalculationData  extends AbstractEntity{
	

	private static final long serialVersionUID = 8711430624942125951L;

	
	@Id
	private String id;
	private String projectId;
	
	
	//row 103 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	
	private Map<Integer,Double> admitsYearWise;
	//admits
	
	private Map<Integer,Double> occupancyForEachYear;
	
	private Map<Integer,Double> atpForEachYear;
	
	private Map<Integer,Double> grossTicketSalesForEachYear;
	
	private Map<Integer,Double> gstForEachYear;
	
	private Map<Integer,Double> showtaxForEachYear;
	
	private Map<Integer,Double> healthCess;
	
	private Map<Integer,Double> serviceChargePerYear;
	
	private Map<Integer,Double> lbtExpenseForEachYear;
	
	private Map<Integer,Double> netTicketSalesForFhcForEachYear;
	
	private Map<Integer,Double> net3dSales;
	
	private Map<Integer,Double> netComplimentaryTickets;
	
	private Map<Integer,Double> netTicketSales;
	
	private Map<Integer,Double> sphForEachYear;
	//sph
	
	private Map<Integer,Double> grossFnBForEachYear;
	
	private Map<Integer,Double> gstFoodForEachYear;
	
	private Map<Integer,Double> netFnBRevenueForCogs;
	
	private Map<Integer,Double> netComplimentaryFnBForEachYear;
	
	private Map<Integer,Double> netFnBSalesForEachYear;
	
	private Map<Integer,Double> adRevenueForEachYear;
	//adsale
	
	private Map<Integer,Double> conveniencefeeForEachYear;
	
	private Map<Integer,Double> vpfIncomeYearWise;
	
	private Map<Integer,Double> miscellaneousIncomeForEachYear;
	
	private Map<Integer,Double> housekeepingChargesForEachYear;
	
	private Map<Integer,Double> securityChargesForEachYear;
	
	private Map<Integer,Double> marketingExpenseForEachYear;
	
	private Map<Integer,Double> communicationChargesForEachYear;
	
	private Map<Integer,Double> internetChargesForEachYear;
	
	private Map<Integer,Double> legalChargesForEachYear;
	
	private Map<Integer,Double> travellingChargesForEachYear;
	
	private Map<Integer,Double> insuranceChargesForEachYear;
	
	private Map<Integer,Double> miscellaneousCharges;
	
	private Map<Integer,Double> printingAndStationaryChargesForEachYear;
	
	private Map<Integer,Double> rateAndTaxes;
	
	private Map<Integer,Double> municipalTaxForEachYear;
	
	private Map<Integer,Double> totalOverheadExpensesForEachYear;
	
	private Map<Integer,Double> rentForEachYear;
	//rent
	
	private Map<Integer,Double> camForEachYear;
	//cam
	
	
	private Double wacc;
	
	
	
	
	
	// row 4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	
	private Map<Integer,Double> sponsorshipRevenueForEachYear;
	
	private Map<Integer,Double> vasIncomeForEachYear;
	
	private Map<Integer,Double> otherOperatingIncome;
	
	private Map<Integer,Double> totalRevenueForEachYear;
	//revenue
	
	
	private Map<Integer,Double> openingGrossBlock;
	
	private Map<Integer,Double> closingGrossBlock;
	
	private Map<Integer,Double> additions;
	
	private Map<Integer,Double> depreciationForEachYear;
	
	private Map<Integer,Double> netGrossForEachYear;
	
	private Map<Integer,Double> filmDistributorShare;
	
	private Map<Integer,Double> consumptionOfFnBForEachYear;
	
	private Map<Integer,Double> grossMarginForEachYear;
	
	
	private Map<Integer,Double> grossMarginPercent;
	
	private Map<Integer,Double> personnelExpenseForEachYear;
	
	private Map<Integer,Double> electricityAndWaterExpenseForEachYear;
	
	private Map<Integer,Double> repairAndMaintenanceExpenseExpenseForEachYear;
	
	private Map<Integer,Double> otherOperatingExpensesForEachYear;
	
	private Map<Integer,Double> expenditureExRentAndCamForEachYear;
	
	private Map<Integer,Double> ebitdarForEachYear;
	
	private Map<Integer,Double> ebitdarMarginPercentForEachYear;
	
	private Map<Integer,Double> rentEachYear;
	
	
	private Map<Integer,Double> camEachYear;
	
	
	private Map<Integer,Double> getEBITDAForEachYear;//edita
	
	private Map<Integer,Double> getEBITDAMarginForEachYear;//ebdita margin
	
	private Map<Integer,Double> getEBITForEachYear;
	
	
	// row 76 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	private Map<Integer,Double> cumulativeEBITDAForEachYear;
	
	private Double getEBITDAPayback;
	//payback year
	
	private Map<Integer,Double> taxForEachYear;
	
	private Map<Integer,Double> cashInflowAfterTaxForEachYear;
	
	private Map<Integer,Double> capexForEachYear;
	
	private Map<Integer,Double> securityDepositForEachYear;
	
	private Map<Integer,Double> disposalOfAssets;
	
	private Map<Integer,Double> totalOutflowsForEachYear;
	
	private Map<Integer,Double> freeCashFlowPreTaxForEachYear;
	
	private Map<Integer,Double> freeCashFlowPostTaxForEachYear;
	
	private Map<Integer,Double> cumulativeFCFPostTaxForEachYear;
	
	private Double getFCFPayback;
	
	
	// row 190 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	private Map<Integer,Double> drawdownForEachYear;
	
	
	private Map<Integer,Double> repaymentForEachYear;
	
	private Map<Integer,Double> closingDebtForEachYear;
	
	private Map<Integer,Double> interestUnderDebtScheduleForEachYear;
	
	private Map<Integer,Double> openingDebt;
	
	
	
	// row 27 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	private Map<Integer,Double> interestForEachYear;
	
	private Map<Integer,Double> getPBTForEachYear;
	
	private Map<Integer,Double> getTaxUnderPATForEachYear;
	
	private Map<Integer,Double> getPATForEachYear;
	
	
	
	
	// row 97 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	private Map<Integer,Double> lessCapexForEachYear;
	
	private Map<Integer,Double> equityCashFlowsForEachYear;
	
	
	
	// row 39 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	private Double projectIRRPreTax;
	
	
	private Double projectIRRPostTax ;
	//irrposttax
	
	private Double equityIRR ;
	//eqityirr
	
	private Double npv;
	
	private Double getROCE;
	//roce
	
	
	
	
//  row 46 – common size P&L >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	
	private Map<Integer,Double> netSaleOfTicket;
	
	private Map<Integer,Double> saleOfFandB;
	
	private Map<Integer,Double> sponsorshipRevenue;
	
	private Map<Integer,Double> vasIncomeNextYear;
	
	private Map<Integer,Double> otherOperatingRevenue;
	
	private Map<Integer,Double> totalRevenue;
	
	private Map<Integer,Double> filmDistributorShares;
	
	private Map<Integer,Double> consumptionFandB;
	
	private Map<Integer,Double> personnelExpenses;
	
	
	private Map<Integer,Double> electricityAndWaterCharges;
	
	private Map<Integer,Double> repairAndMaintaince;
	
	private Map<Integer,Double> otherOperatingExpense;
	
	private Map<Integer,Double> expenditureExRentAndCamp;
	
	private Map<Integer,Double> ebitDar;
	
	private Map<Integer,Double> rentDetails;
	
	private Map<Integer,Double> camDetails;
	
	private Map<Integer,Double> ebitdaDetails;
	
	@Transient
	private Map<Integer,Double> rentRationDetails;
	
	@Transient
	private Map<Integer,Double> rentCamRevenueRatioDetails;
	
	private Double totalFilmHireCostPercentage;
	
	private Double vpfIncome;
	
	@Transient
	private List<Integer> leasePeriod;
}
