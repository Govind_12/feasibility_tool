package com.common.models;

import lombok.Data;

@Data
public class VpfIncomeDetails {
	private Integer screen;
	private String operator;
	private Double value;
	private Integer applicableYear;

}
