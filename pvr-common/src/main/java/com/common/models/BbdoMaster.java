package com.common.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@Document(collection = "bbdo_master")
@JsonInclude(content = Include.NON_NULL)
public class BbdoMaster extends AbstractEntity {
	
	private static final long serialVersionUID = -4047610614277858850L;
	
	@Id
	private String id;
	private String state;
	private String town;
	private String grade;
	private float mii;
	private float mpv;
	private float mei;
	private float popn;
	private float popnHalfyearly;
	private AllIndiaRank allIndiaRank;
	private ShareInState shareInState;
	private int allIndiaranks;
	private IndicatorAsPerCapita indicatorAsperCapita;
	private IndicatorAsAcross indicatorAsCross;
	private float tv;
	private float radio;
	private float cinema;
	private float printMedia;
	private float femaleLiteracy;
	private FactorAs factorAsMpv;
	
}
