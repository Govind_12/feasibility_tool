package com.common.models;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "category_master")
@JsonInclude(content = Include.NON_NULL)
public class CategoryDataSource extends AbstractEntity{

	private static final long serialVersionUID = -4047610614278858850L;
	
	@Id
	private String id;
	private int hopk;
	private String cinemaName;
	private String cinemaNavCode;
	private String cinemaCategory;
}
