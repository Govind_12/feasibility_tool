package com.common.models;

import lombok.Data;

@Data
public class FinalSummary {

	private String parentId;
	private String projectType;
	private Double irr;
	private Double payBackPBDIT;
	private FieldValue totalAdmits;
	private FieldValue occupancy;
	private FieldValue atp;
	private FieldValue sph;
	private FieldValue adRevenue;
	private FieldValue personalExpense;
	private FieldValue electricityWaterExpanse;
	private FieldValue totalRmExpenses;
	private FieldValue totalOverheadExpenses;
	private Integer leasePeriod;
	private Double annualRent;
	private Double annualCam;
	private FieldValue totalProjectCost;

}
