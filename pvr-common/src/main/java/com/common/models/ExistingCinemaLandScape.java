package com.common.models;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Document(collection = "existing_cinema_landscpae")
@JsonInclude(content = Include.NON_NULL)
public class ExistingCinemaLandScape extends AbstractEntity{

	private static final long serialVersionUID = -4047610681277858850L;

	@Id
	private String id;
	private String projectId;
	private List<CinemaMaster> cinemaMaster;
	private List<CompitionMaster> compitionMaster;
}
