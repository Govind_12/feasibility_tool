package com.common.models;

import lombok.Data;

@Data
public class IndicatorAsAcross {
	private float cd1;
	private float cd2;
	private float cd3;
	private float car;
	private float phone;
	private float fmcg;
	private Employement employment;
	private Credit credit;
	
}
