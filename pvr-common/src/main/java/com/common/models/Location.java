package com.common.models;

import java.util.List;

import lombok.Data;

@Data
public class Location {
	private String type="Point";
	List<Double> coordinates;
	/*private float latitude;
	private float longitude;*/
}
