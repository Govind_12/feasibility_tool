package com.common.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Data
@Setter
@Getter
public class SecurityDepositAssumption {
	
	private int secDepositNoOfMonths;
	private int camSecurityDeposit;
	private int unitySecurityDeposit;
	private String additionalDepositOnEscalation;
	

}
