package com.common.models;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class CamTerm {
	
	
	
	private String camPaymentTerm;
	private Double camRate;
	private Double percentageEscalation;
	private Integer frequencyOfEscaltionYears;
	private String explainCamAgreement;
	private HashMap<Integer,Double> yearWiseCamRent;

}
