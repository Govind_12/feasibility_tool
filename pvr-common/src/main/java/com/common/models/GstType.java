package com.common.models;

import lombok.Data;

@Data
public class GstType {
	
	private Double gstApplicable;
	private Double gstCreditDisallowed;
	private Double effectiveGstCost;

}
