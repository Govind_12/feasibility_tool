package com.common.models;

import javax.persistence.Id;

import lombok.Data;

@Data
public class ProjectCostData {

	
private static final long serialVersionUID = -4047610614277858850L;
	
	private String description;
	private Double cost;
	private Double depRate;
	private int annualDep;
	
}
