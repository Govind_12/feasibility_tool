package com.common.models;

import java.util.List;

import com.common.mappers.QueryMapper;

import lombok.Data;

@Data
public class OtherParameter {
	
	private Double inputValues;
	private String comments;
	private String lastQueryUserId;
	private List<QueryMapper> query;

}
