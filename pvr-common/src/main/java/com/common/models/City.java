package com.common.models;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@Document(collection = "city")
@JsonInclude(content = Include.NON_NULL)
public class City {
	
	
	@Id
	private String id;
	private int city_id;
	private String city_name;
	private String city_state;

}
