
package com.common.constants;

public class Constants {

	public static final String DATE_FORMAT = "MM/dd/yyyy";

	public static final int STATUS_DELETE = 0;
	public static final int STATUS_ACTIVE = 1;
	public static final int STATUS_COMPLETE = 2;
	public final static int STATUS_PENDING = 3;
	public final static int STATUS_DRAFT = 4;

	/* Project status */
	/*
	 * public static final int STEP_1 = 101; public static final int STEP_2 = 102;
	 * public static final int STEP_3 = 103; public static final int STEP_4 = 104;
	 */

	public static final int STEP_1 = 91;
	public static final int STEP_2 = 92;
	public static final int STEP_3 = 93;
	public static final int STEP_4 = 94;
	public static final int STEP_5 = 95;
	public static final int STEP_6 = 96;

	public final static int STATUS_SEND_FOR_APPROVAL = 105;
	
	//PRE-SIGN
	public final static int STATUS_REJECTED = 108;
	public final static int STATUS_PENDING_QUERY = 109;
	public static final int STATUS_ARCHIVED = 110;
	
	//POST
	public final static int POST_STATUS_REJECTED = 208;
	public final static int POST_STATUS_PENDING_QUERY = 209;
	public static final int POST_STATUS_ARCHIVED = 210;

	//PRE-HAND
	public final static int PRE_HAND_STATUS_REJECTED = 308;
	public final static int PRE_HAND_STATUS_PENDING_QUERY = 309;
	public static final int PRE_HAND_STATUS_ARCHIVED = 310;
	
	
	//Approver
	public final static int STATUS_APPROVER_1 = 106;
	public final static int STATUS_APPROVER_2 = 107;
	public final static int STATUS_APPROVER_3 = 108;
	public final static int STATUS_APPROVER_4 = 109;
	public final static int STATUS_APPROVER_5 = 110;
	public final static int STATUS_APPROVER_6 = 111;
	public final static int STATUS_APPROVER_7 = 112;
	public final static int STATUS_APPROVER_8 = 113;
	
	
	public final static int STATUS_ACCEPTED = 200;

	public static final String ADMIN = "ADMIN";
	public static final String P_STRING = "a3345swer1oip34677ksla";
	public final static String ADMIN_REQUEST = "adminRequest";
	public final static String MAPPING_BASE_PACKAGE = "com.api.models";
	public final static String S3_USER_PROFILE_FOLDER = "users/profile";
	public final static String S3_MEDIA_FOLDER = "media";
	public static final String AUTH_TOKEN = "token";
	public static final String USER = "user";

	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";
	public static final String ROLE_BD = "ROLE_BD";
	public static final String ROLE_OPERATION_TEAM = "ROLE_OPERATION_TEAM";
	public static final String ROLE_PROJECT_TEAM = "ROLE_PROJECT_TEAM";
	public static final String ROLE_APPROVER = "ROLE_APPROVER";
	public static final String ROLE_APPROVER_1 = "ROLE_APPROVER_1";
	public static final String ROLE_APPROVER_2 = "ROLE_APPROVER_2";
	public static final String ROLE_APPROVER_3 = "ROLE_APPROVER_3";
	public static final String ROLE_APPROVER_4 = "ROLE_APPROVER_4";
	public static final String ROLE_APPROVER_5 = "ROLE_APPROVER_5";
	public static final String ROLE_APPROVER_6 = "ROLE_APPROVER_6";
	public static final String ROLE_APPROVER_7 = "ROLE_APPROVER_7";
	public static final String ROLE_APPROVER_8 = "ROLE_APPROVER_8";
	
	public static final String ROLE_DRIVER_DEFAULT_PASS = "12345";

	public static final Integer NO_OF_RECORDS_DATA_TABLE = 10;

	public static final boolean TRUE = true;
	public static final boolean FALSE = false;

	public static final String CRN = "crn";
	public static final String PARENT_ID = "parentId";
	public static final String ROLE = "authorities";
	public static final String STATUS = "status";
	
	public final static String REPORT_TYPE_NEW="new";
	public final static String REPORT_TYPE_EXISTING="existing";

	public final static short ERROR = 0;
	public final static short MAXSIZE = 50;
	public final static short DESCRIPTION_MAXSIZE = 200;

	public final static String ALL = "777";
	public final static String NOT_ALL = "111";

	public final static int COMPITION_MASTER = 1001;
	public final static int BBDO_MASTER = 1002;
	public final static int CINEMA_MASTER = 1003;
	public final static int CITY_TIER_MASTER = 1004;
	public final static int NAVISION_MASTER = 1005;
	public final static int CATEGORY_MASTER = 1006;
	public final static int SEAT_CATEGORY = 1007;

	public final static String MAINSTREAM = "mainstream";
	public final static String GOLD = "gold";
	public final static String IMAX = "imax";
	public final static String PXL = "pxl";
	public final static String LX = "4xl";
	public final static String PLAYHOUSE = "playhouse";
	public final static String ULTRAPREMIUM = "ultrapremium";
	public final static String DX = "4dx";
	public final static String PREMIER = "premier";
	public final static String ONYX = "onyx";
	public final static String LUXE = "luxe";

	public final static String CITY_TIER_1 = "Tier 1";
	public final static String CITY_TIER_METRO = "Metro";
	public final static String CITY_TIER_3 = "Tier 3";
	public final static String CITY_TIER_2 = "Tier 2";
	public final static String CITY_TIER_5 = "Tier 5";
	public final static String CITY_TIER_4 = "Tier 4";

	public final static String COMP = "Comp";
	public final static String NONCOMP = "Non Comp";
	public final static String APPROVER1 = "5bf247904308663100500613";
	public final static String APPROVER2 = "5bf4ef464308663100511ce0";

	public final static double SINGLE_SCREEN_MULTIPLIER = 1.92;
	public final static double GOLD_MULTIPLIER = 1.16;
	public final static double DX_MULTIPLIER = 1.1;
	public final static double IMAX_MUTIPLIER = 1.1;

	public final static String UPDATES = "updates";
	public final static String ACTION_ITEMS = "action_items";

	public final static String QUATER_1 = "Qtr1";
	public final static String QUATER_2 = "Qtr2";
	public final static String QUATER_3 = "Qtr3";
	public final static String QUATER_4 = "Qtr4";
	public final static String QUATER_5 = "Qtr5";
	public final static String QUATER_6 = "Qtr6";

	public final static int COLUMN_INDEX_0 = 0;
	public final static int COLUMN_INDEX_1 = 1;
	public final static int COLUMN_INDEX_2 = 2;
	public final static int COLUMN_INDEX_3 = 3;
	public final static int COLUMN_INDEX_4 = 4;
	public final static int COLUMN_INDEX_5 = 5;
	public final static int COLUMN_INDEX_6 = 6;
	public final static int COLUMN_INDEX_7 = 7;
	public final static int COLUMN_INDEX_8 = 8;
	public final static int COLUMN_INDEX_9 = 9;
	public final static int COLUMN_INDEX_10 = 10;
	public final static int COLUMN_INDEX_11 = 11;
	public final static int COLUMN_INDEX_12 = 12;
	public final static int COLUMN_INDEX_13 = 13;
	public final static int COLUMN_INDEX_14 = 14;
	public final static int COLUMN_INDEX_15 = 15;
	public final static int COLUMN_INDEX_16 = 16;
	public final static int COLUMN_INDEX_17 = 17;
	public final static int COLUMN_INDEX_18 = 18;
	public final static int COLUMN_INDEX_19 = 19;
	public final static int COLUMN_INDEX_20 = 20;
	public final static int COLUMN_INDEX_21 = 21;
	public final static int COLUMN_INDEX_22 = 22;
	public final static int COLUMN_INDEX_23 = 23;
	public final static int COLUMN_INDEX_24 = 24;
	public final static int COLUMN_INDEX_25 = 25;
	public final static int COLUMN_INDEX_26 = 26;
	public final static int COLUMN_INDEX_27 = 27;

	public final static String APPROVER_ = "approver";
	public final static String ICON = "ICON";

	public final static String PERSONNAL_COST = "Personnel_Cost";
	public final static String RANDM_COST = "R&M_Cost";
	public final static String HOUSE_KEEPING = "House_Keeping";
	public final static String MARKETING_COST = "Marketing_Cost";
	public final static String COMMUNICATION = "Communication_Cost";
	public final static String PRINTINGANDSTATIONARY = "Printing&Stationary_Cost";
	public final static String TRAVELLINGANDCONVEYANCE = "Travelling&Conveyance_Cost";
	public final static String LEGALFEE = "LegalFees_Cost";
	public final static String MISCELLANEOUS = "Miscellaneous_expanses";
	public final static String SECURITY = "Security";
	public final static String WATERANDELECTRICITY = "Electricity_Water";
	public final static String LEGAL_EXPENSE = "legalExpense";
	public final static String INTERNET_CHARGES = "internetCharges";
	public final static String INSURANCE_EXPENSE = "insuranceExpense";
	public final static String TRAVELLING_CONVEYANCE = "travellingConvetance";
	public final static String AD_REVENUE = "adRevenue";

	public final static String OCCUPANCY = "occupancy";

	public final static String OVER_HEAD_COST = "totalOverhead";
	public final static String WEB_SALE = "webSale";
	public final static String ATP = "atp";
	public final static String SPH = "sph";

	public final static double GOLDMULTIPLIERCOUNT = 1d;
	public final static long MARKETINGGOLDMULTIPLIERCOUNT = 1l;

	public final static String A = "A";
	public final static String AA = "A+";
	public final static String AAA = "A++";
	public final static String ICONCATEGORY = "ICON";

	public final static String STARTDATE = "startdate";
	public final static String ENDDATE = "enddate";

	public final static Integer HOPK = 0;
	public final static int SEAT_CAT_ADMITS=1;
	public final static int SEAT_CAT_TOTALSEATS=2;
	public final static int ADMITS = 13;
	public final static int TOTALSEATS = 15;
	public final static int CinemaType = 4;
	public final static int GBOC = 14;

	public final static double YEAR = 360;
	public final static String SEATCATEGORY_MAINSTREAM = "Mainstream";
	public final static String SEATCATEGORY_MAINTSTREAM = "Maintstream";

	public final static String SEATCATEGORY_GOLD = "Gold";
	public final static String SEATCATEGORY_DX = "4DX";

	public final static String SEATCATEGORY_IMAX = "IMAX";

	public final static String SEATCATEGORY_ONYX = "ONYX";
	public final static String SEATCATEGORY_PXL = "P[XL]";

	public final static String SEATCATEGORY_PLAYHOUSE = "playhouse";
	public final static String SEATCATEGORY_PLAYHOUSE1 = "play House";

	public final static String SEATCATEGORY_PREMIER = "Premier";

	public final static String SEATCATEGORY_ULTRAPREMIUM = "Ultra Premium";

	public final static String NORMAL = "normal";
	public final static String RECLINER = "recliners";
	public final static String LOUNGER = "lounger";

	public final static int CONCESSION_NET_REVENUE = 7;

	public final static int CINEMA_PERFORMANCE_GBOC = 6;

	public static final String URL = "http://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDB_5K2ufiKQELVMLsbXLm8d6yqTH45JZw";
	public static final String KEY = "AIzaSyDB_5K2ufiKQELVMLsbXLm8d6yqTH45JZw";

	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";

	public static final int EARTH_RADIUS = 6371;

	public static final int ONE = 1;
	public static final int TWO = 2;
	public final static int HUNDRED = 100;
	public final static int TWELVE = 12;
	
	
	
	public final static int INDEX_0 = 0;

	public final static String OPENING_GROSS_BLOCK = "openingGrossBlock";
	public final static String ADDITIONS = "additions";
	public final static String CLOSING_GROSS_BLOCK = "closingGrossBlock";
	public final static String DEPRECATION = "deprecation";
	public final static String NET_BLOCK = "netBlock";

	public final static int ZERO = 0;

	/*
	 * public final static double PROJECT_DEPRECATION_COST = 14.5d; public final
	 * static double PROJECT_COST_CONTINGENCIES = 500.5d; public final static double
	 * INITIAL_CAPEX_IN_YEAR_1 = 30d;
	 */

	public final static double LEASE_PERIOD = 42d;

	public final static String FIXED = "fixed";

	public final static String REVENUE_SHARE = "revenue_share";

	public final static String MAX = "mg_revenue_share";

	public final static String OTHER_1 = "other_1";

	public final static String OTHER_2 = "other_2";

	public final static String ACTUALS = "actuals";

	public final static String INCLUDED = "included";
	
	public final static String AVERAGE = "average";
	

	public final static double PROJECT_DEPRECATION_COST = 14.5d;
	public final static double PROJECT_COST_CONTINGENCIES = 500.5d;
	public final static double INITIAL_CAPEX_IN_YEAR_1 = 30d;

	public final static int FOUR = 4;

	public static final String ADMIT = "admits";

	public static final String ONLINE = "Online";
	public static final String OFFLINE = "Offline";

	public static final String GRAND_TOTAL="Grand Total";
	public static final String TOTAL_PROJECT_COST="Total Project Cost";
	public static final String SALVAGE_DISPOSAL_ASSETS =  "Salvage from disposal of assets";
	public static final String TOTAL_PROJECT_COST_AFTER_CONTINGENCIES = "Total Project Cost after Contingencies";
	
	public final static String SUFFIX = ".html";
	public final static String PREFIX = ".pdf";
	
	// Report Download
	public final static String HOME = "home";
	public final static String INDEX = "index";
	public final static String CATCHMENT_DETAILS = "catchmentDetails";
	public final static String DEVELOPMENT_DETAILS = "developmentDetails";
	public final static String DEVELOPER_DETAILS = "developerDetails";	
	public final static String EXISTING_CINEMA_LANDSACPE = "existingCinemaLandscape";
	public final static String UPCOMING_CINEMA_LANDSCAPE="upcomingCinemaLandscape";
	public final static String CINEMA_LANDSCAPE_IN_NEXT_5_YEAR="cinemaLandscapeInNext5Year";
	public final static String CINEMA_SPECS="cinemaSpecs";
	public final static String KEY_LEASE_TERMS="keyLeaseTerms";
	public final static String KEY_LEASE_TERMS_FIXED="keyLeaseTerms-Rent(fixed)";
	public final static String KEY_LEASE_TERMS_RENT_REVENUE="keyLeaseTerms-Rent(Revenue share)";
	public final static String KEY_LEASE_TERMS_RENT_MAX="keyLeaseTerms-Rent(Max)";
	public final static String KEY_LEASE_TERMS_RENT_OTHER_ONE_FIXED="keyLeaseTerms-Rent(Others1-fixed)";
	public final static String KEY_LEASE_TERMS_RENT_OTHER_ONE_AVERAGE="keyLeaseTerms-Rent(Others1- average)";
	public final static String KEY_LEASE_TERMS_RENT_OTHER_TWO="keyLeaseTerms-Rent(Others2)";
	public final static String KEY_LEASE_TERMS_CAM_FIXED="keyLeaseTerms-CAM(fixed)";
	public final static String KEY_LEASE_TERMS_CAM_ACTUALS="keyLeaseTerms-CAM(actuals)";
	public final static String KEY_LEASE_TERMS_CAM_INCLUDEDINRENT="keyLeaseTerms-CAM(includedInRent)";
	public final static String KEY_LEASE_TERMS_SECURITY_DEPOSITE="keyLeaseTerms-SecurityDeposit";
	public final static String DEVELOPER_SCOPE_OF_WORK="developerScopeOfWork";
	public final static String FEASIBIITY_SUMMARY_AND_PAYBACKS_ANALYSIS="feasibilitySummaryAndPaybackAnalysis";
	public final static String SUMMARY_PANDL="summaryPandL";
	public final static String COMMON_SIZE_PL_AND_L="commonSizePlAndL";
	public final static String KEY_REVENUE_ASSUMPTION_OCCUPANCY="keyRevenueAssumptionOccupancy";
	public final static String KEY_REVENUE_ASSUMPTION_ADMITS="keyRevenueAssumptionAdmits";
	public final static String KEY_REVENUE_ASSUMPTION_ATP="keyRevenueAssumptionAtp";
	public final static String KEY_REVENUE_ASSUMPTION_SPH="keyRevenueAssumptionShp";
	public final static String KEY_REVENUE_ASSUMPTION_AD_SALE_AND_OTHERS="keyRevenueAssumptionAdSaleAndOthers";
	public final static String KEY_OPERATING_COST_ASSUMPTION="keyOperatingCostAssumption";
	public final static String KEY_PROJECT_COST_ASSUMPTION="keyProjectCostAssumption";
	public final static String KEY_ASSUMPTIONS="keyAssumptions";
	public final static String UPCOMING_CINEMA_PROPOSITION="upcomingCinemaProposition";
	public final static String APPENDIX_PROJECT_PRESENTATION="projectPresentation";
	public final static String APPENDIX_DEVELOPER_CREDIT_RATING_REPORT="developerCreditRatingReport";
	public final static String PROJECT_SYNOPSIS="projectSynopsis";
	public final static String CINEMA_FORMAT_SEATDIMENSION="seatDimension";
	public final static String APPENDIX="appendix";
	public final static String StepOne="stepOne";
	public final static String StepTwo="stepTwo";
	public final static String StepFive="stepFive";
	public final static int MAX_NOTIFICATION=3;
	public final static String COMMENTS_SECTION = "commentsSection";
	public final static String POST_SIGNING_COST_COMPARISON = "postSigningCostComparison";
	public final static String PRE_HANDOVER_COST_COMPARISON = "preHandoverCostComparison";
	
	//city name
	public final static String Prayagraj="Prayagraj";
	public final static String Allahabad="Allahabad";
	public final static String Bengaluru="Bengaluru";
	public final static String Bangalore="Bangalore";
	public final static String Mysuru="Mysuru";
	public final static String Mysore="Mysore";
	public final static String Vijayawada="Vijayawada";
	public final static String Vijaywada="Vijaywada";
	public final static String NEW_DELHI="New Delhi";
	public final static String DELHI="Delhi";
	public final static String GURUGRAM="Gurugram";
	public final static String GURGAON="Gurgaon";
	
	public final static String PRE_SIGNING="pre_signing";
	public final static String POST_SIGNING="post_signing";
	public final static String PRE_HANDOVER="pre_handover";
	
	public final static Integer SEAT_CAPCITY=200;
	
	public final static Integer YEAR_1=1;
	public final static Integer YEAR_2=2;
	public final static Integer YEAR_3=3;
	public final static Integer YEAR_4=4;
	public final static Integer YEAR_5=5;

	
	public final static Double FACTOR=100000.0;
	
	public final static String COMPITION="compitionMaster";
	
	public final static String ADMIT_GROWTH="admitgrwoth";
	public final static String ATP_GROWTH="atpGrowth";
	public final static String SPH_GROWTH="sphGrowth";
	public final static String SPONSORSHIP_GROWTH="sponshorshipGrowth";
	public final static String CONVENIENCE_GROWTH="conveninceGrowth";
	public final static String OTHER_INCOME_GROWTH="otherIncomegrowth";
	public final static String OTHER_OVERHEAD_EXPENSE="otherOverHeadExpense";
	public final static String RENT_EXPENSE="rentExpense";
	public final static String COMMON_AREA_MAINTAINCE="camAreaMaintance";
	public final static String PROPERTY_TAX="propertyTax";
	public final static String MAINTENCE_CAPEX_INITIAL="maintenceCapexInitial";
	public final static String REPAIRANDMAINTANCE="repairMaintaince";
	public final static String F_B_COGS="fnbCogs";
	public final static String F_H_C="fhcPercentage";
	
	public final static String PDF_NAME="result";
	
	public final static String NO_OF_SCREEN="noOfScreen";
	public final static String NO_OF_CINEMA="noOfCinema";
	public final static String NO_OF_SEATS="noOfSeat";
	
	public final static String NO_OF_SCREEN_COMPITION="noOfScreenCompition";
	public final static String NO_OF_CINEMA_COMPITION="noOfCinemaCompition";
	public final static String NO_OF_SEATS_COMPITION="noOfSeatCompition";
	
	public final static String NO_OF_SCREEN_MARKET="noOfScreenMarket";
	public final static String NO_OF_CINEMA_MARKET="noOfCinemaMarket";
	public final static String NO_OF_SEATS_MARKET="noOfSeatMarket";
	
	public final static String PDF="result.pdf";

	public final static String AVG_FIXED_SHOW="fixed";
	public final static String OCCUPANCY_DETAILS="occupancy";
	public final static String COMPITION_DETAILS="compitition";
	public final static String PERSONNAL_EXPENSE = "PersonalExpense";
	
	
	public final static String UPCOMING_CINEMAS = "upcomingCinemas";
	public final static String LESSER_LESSEE = "lesserLessee";
	public final static String PROJECT_LAYOUT = "projectLayout";
	public final static String OPERATING_ASSUMPTION = "operatingAssumptions";
	public final static String SUMMARY_SHEET = "summary-sheet";
	
	public final static String DEVELOPMENT_DETAILS_POST_SINGING = "development-details";
	public final static String DEVELOPER_DETAILS_POST_SINGING = "developer-details";
	public final static String UPCOMING_CINEMAS_POST_SINGING = "upcoming-cinemas";
	public final static String LESSE_LESSE_POST_SINGING = "lessor-lessee-schedule";
	public final static String PROJECT_LAYOUT_POST_SINGING = "project-layout-assumptions";
	public final static String OPERATING_ASSUMPTION_POST_SINGING = "operating-assumptions";
	public final static String SUMMARY_SHEET_POST_SINGING = "summary-sheet";
	public final static String NOT_PENDING="NOTPENDING";
	public final static String PENDING="PENDING";
	
	public final static String DESIGNATION_CDO="CDO";
	public final static String DESIGNATION_CO="CO";
	public final static String DESIGNATION_CPO="CPO";
	public final static String DESIGNATION_CEO="CEO";
	public final static String DESIGNATION_JMD="JMD";
	public final static String DESIGNATION_MD="MD";
	public final static int USER_ACTIVE_STATUS=1;
	public final static int USER_INACTIVE_STATUS=0;
	
	public final static String OPERATION="submitForOperation";
	public final static String PROJECT="submitForProject";
	
	public final static Integer STEP6=6;
	public final static Integer STEP7=7;
	
	
}
