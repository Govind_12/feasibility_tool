package com.common.constants;

public class MessageConstants {

	public static final String BD_INCOMPLETE="is incomplete";
	public static final String BD_PENDING_FOR_APPROVAL="pending for approval";
	public static final String APPROVAL_PENDING_FROM_YOU="has pending approval for you";
	public static final String APPROVAL_PENDING_FROM="has pending approval from ";
	public static final String APPROVAL_ACCEPTED="Accepted";
	public static final String REPORT_CANCELLED="Cancelled";
	public static final String REJECTED_BY="rejected ";
	public static final String BLANCK="BLANCK";
	
	public static final String PENDING_QUERY="has a pending query from ";
	public static final String QUERY_ANSWERED="has been answered by ";
	
	//pre signing
	public static final String APPROVAL_STATUS_1="(1/2)";
	public static final String APPROVAL_STATUS_2="(2/2)";
	
	//post-signing
	public static final String POST_APPROVAL_STATUS_1="(1/4)";
	public static final String POST_APPROVAL_STATUS_2="(2/4)";
	public static final String POST_APPROVAL_STATUS_3="(3/4)";
	public static final String POST_APPROVAL_STATUS_4="(4/4)";
	
	//post-signing
	public static final String PRE_APPROVAL_STATUS_1="(1/6)";
	public static final String PRE_APPROVAL_STATUS_2="(2/6)";
	public static final String PRE_APPROVAL_STATUS_3="(3/6)";
	public static final String PRE_APPROVAL_STATUS_4="(4/6)";
	public static final String PRE_APPROVAL_STATUS_5="(5/6)";
	public static final String PRE_APPROVAL_STATUS_6="(6/6)";
	
	
	public static final String MAIL_SUBJECT_APPROVE="Report Approved";
	public static final String MAIL_SUBJECT_REJECT="Report Rejected";
	public static final String MAIL_SUBJECT_ACCEPT="Report Accepted";
	public static final String MAIL_SUBJECT_QUERY="Query asked";
	public static final String MAIL_SUBJECT_ANSWER="Query Answered";
	public static final String MAIL_SUBJECT_APPROVAL_PENDING="Approval Pending";
	
	
	public static final String MSG_PRE_HANDOVER="Pre-Handover";
	public static final String MSG_POST_SIGNING="Post-Signing";
	
	public static final String PENDING_FROM_PROJECT="pending from Project Team";
	public static final String PENDING_FROM_OPERATION="pending from Operation Team";
	public static final String PENDING_FROM_PROJECT_AND_OPERATION="pending from Project and Operation Team";
	public static final String PENDING_FROM_YOU_PROJECT_AND_OPERATION="pending from you";
	public static final String PENDING_FROM_BD="pending from ";
	public static final String PENDING_FROM_YOU_BD="pending from you";
	public static final String DASHBORAD_DRAFT_STATUS="Draft";
	public static final String DASHBORAD_PENDING_APPROVAl_STATUS="Approval Pending";
	public static final String DASHBORAD_REJECTED_STATUS="Rejected";
	
	// dashboard project and operation
	public static final String DASHBORAD_PENDING_STATUS_PROJECT_TEAM="Pending for You";
	public static final String DASHBORAD_PENDING_FROM_BD="Pending with BD";
	public static final String DASHBORAD_PENDING_FROM_OPERATION="Pending with Operation";
	public static final String FEASIBILITY_REPORT_GENERATED="Feasibility Report Generated";
	public static final String SEND_FOR_APPROVAL="Sent For Approval";
	public static final String DASHBORAD_PENDING_FROM_PROJECT="Pending with Project Team";
	public static final String PENDING_FROM_OPS_AND_PROJECT="Pending with ops and project Team";
	public static final String ACCEPTED_FROM="Accepted by ";
	public static final String REJECTED_FROM="Rejected by ";
}