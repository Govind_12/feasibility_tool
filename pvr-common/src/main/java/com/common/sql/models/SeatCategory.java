package com.common.sql.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "temp_table_seat_Category")

@Data
public class SeatCategory implements Serializable {
	private static final long serialVersionUID = -4047610614277858850L;

	@Id
	@Column(name = "cinema_hopk")
	private int cinema_hopk;
	@Column(name = "Cinema_Name")
	private String Cinema_Name;
	@Column(name = "Cinema_state")
	private String Cinema_state;
	@Column(name = "Cinema_City")
	private String Cinema_City;
	@Column(name = "Cinema_Format")
	private String Cinema_Format;
	@Column(name = "Movie_show_Time")
	private Timestamp Movie_show_Time;
	//@Column(name = "CinOperator_strCode")
	@Column(name="cin_operator_str_code")
	private String CinOperator_strCode;
	@Column(name = "Audi_Number")
	private int Audi_Number;
	@Column(name = "Area_category")
	private String Area_category;
	@Column(name = "area_cat_Code")
	private String area_cat_Code;
	@Column(name = "Session_lngSessionId")
	private int Session_lngSessionId;
	@Column(name = "Film_strCode")
	private String Film_strCode;
	@Column(name = "Movie_Name")
	private String Movie_Name;
	@Column(name = "admits")
	private int admits;
	@Column(name = "GBOC")
	private double GBOC;
	@Column(name = "Total_Seats")
	private int Total_Seats;
	@Column(name = "finyear")
	private String finyear;
	@Column(name = "QTR")
	private String QTR;
	@Transient
	private Long admitsSum; 
	
	@Transient
	private Long sumGboc;
	
	@Transient
	private Long totalSeats;
}
