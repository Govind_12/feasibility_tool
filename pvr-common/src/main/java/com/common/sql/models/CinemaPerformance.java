package com.common.sql.models;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name="tbl_Cinema_performance")
@Data
public class CinemaPerformance {

	@Id
	private int Cinema_hopk;
	private String Cinema_Name;
	private String Cinema_State;
	private String Cinema_City;
	private Timestamp Business_Date;
	private int Admits;
	private double GBOC;
	private double Concession_Net_Revenue;
	private int Total_Seats_Available;
	private int BO_No_of_Trans;
	private int Conc_No_of_Trans;
	
	@Transient
	private Long totalConscessionNetRevenue;
	
	@Transient
	private Long totalAdmits;
}
