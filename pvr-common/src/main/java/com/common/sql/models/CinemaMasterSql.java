package com.common.sql.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.common.models.ProjectDetails;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "Cinema_master_Data_v1")
@Data
@EqualsAndHashCode(of = { "HOPK" }, callSuper = false)
public class CinemaMasterSql {
	
	@Id
	private String HOPK;
	private String Cinema_strCode;
	private String Cinema_Name;
	private String Vista_info_Cinema_Name;
	private String Cinema_Location;
	private String State_strName;
	private String Nav_cinemaCode;
	private String OWNER_strCompany;
	private String Cinema_Region;
	private String Nav_cinemadesc;
	private String Cinema_strRun;
	private String latitude;
	private String longitude;
	private int loyalty_complex_id;
	private int No_of_Audi;
	private int Gold;
	private int PXL; 
	private int UltraPremium;
	private int DX;
	private int Premier;
	private int IMAX;
	private int Playhouse;
	private int ONYX;
	private int Mainstream;
	
	@Transient
	private String category;
	
	@Transient
	private double distance;
	
	@Transient
	private Long seatCapacity;
	
	@Transient
	private Long atp;
	
	@Transient
	private Double occupancy;
	
	@Transient
	private Long admits;
	
	@Transient
	private Integer noOfScreens;
	
	@Transient
	private Long sph;
	
	@Transient
	private Double sphAtp;
}
