package com.common.sql.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "Cinema_audi_seat_Master_v1")
@Data
public class CinemaAudiSeatMaster {

	@Id
	private String Screen_bytNum;
	private String Screen_strName;
	private String Screen_intSeats;
	private String Area_strDescription;
	private String Area_strShortDesc;
	private String Area_intNumSeatsTot;
	private String Cinema_HOPK;
	private String AreaCat_strCode;
	private String AreaCat_strDesc;
	private String Area_cat_status;
	

}
