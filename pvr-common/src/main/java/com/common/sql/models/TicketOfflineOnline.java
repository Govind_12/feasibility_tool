package com.common.sql.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "Ticket_online_Offline")

@Data
public class TicketOfflineOnline {

	private static final long serialVersionUID = -4047610614277858850L;

	@Id
	@Column(name = "cinema_hopk")
	private int cinema_hopk;
	@Column(name = "Session_Business_Date")
	private Timestamp session_business_date;
	@Column(name = "Channel_Group")
	private String channel_group;
	@Column(name = "Session_no")
	private int session_no;
	@Column(name = "Audi_number")
	private short audi_number;
	@Column(name = "Admits")
	private int admits;

	@Transient
	private long avg;
	
	@Transient
	private long sum;
}
