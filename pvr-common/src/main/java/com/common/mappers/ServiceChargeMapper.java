package com.common.mappers;

import lombok.Data;

@Data
public class ServiceChargeMapper {
	private String state;
	private Double lbtCharge;
	private Double healthCess;
	private Double ticketServiceCharge;

}
