package com.common.mappers;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class UploadFilesMapper {

	private int type;
	private MultipartFile file;
	private String projectId;
}
