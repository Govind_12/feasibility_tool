package com.common.mappers;

import lombok.Data;

@Data
public class VpfIncomeMapper {

	private Integer noOfScreen;
	private Double averageVpfIncome;
	private Double totalVpfincomePerAnnum;
	private Integer vpfIncomeValidity;
	
}
