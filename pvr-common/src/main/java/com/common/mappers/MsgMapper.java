package com.common.mappers;

import com.common.constants.Constants;

import lombok.Data;

@Data
public class MsgMapper {
	
	private String projectId;
	private String queryType;
	private String msg;
	private String msgById;
	private String msgByRole;
	private String isProjectOrOperation;
	private boolean isProjectChat;
	private boolean isOperationChat;
	private Integer step =Constants.STEP7;
}
