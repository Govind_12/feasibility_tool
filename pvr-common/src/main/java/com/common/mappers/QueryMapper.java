package com.common.mappers;

import lombok.Data;

@Data
public class QueryMapper {

	private String userId;
	private String msg;
	private Long date_time;
	private String userRole;
	private String userFullName;
	private String date;
}
