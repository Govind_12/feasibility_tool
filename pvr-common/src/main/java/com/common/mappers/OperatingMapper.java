package com.common.mappers;

import lombok.Data;

@Data
public class OperatingMapper {
	private String key;
	private Double value;

}
