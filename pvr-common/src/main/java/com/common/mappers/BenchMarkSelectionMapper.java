package com.common.mappers;

import java.util.List;
import java.util.Set;

import com.common.models.CinemaMaster;

import lombok.Data;

@Data
public class BenchMarkSelectionMapper {

	private String formats;
	private String screens;
	private List<CinemaMaster> cinemaMasterDetails;
	private int totalShow;
	private int totalCapacity;

}
