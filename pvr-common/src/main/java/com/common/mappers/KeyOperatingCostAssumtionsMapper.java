package com.common.mappers;

import lombok.Data;

@Data
public class KeyOperatingCostAssumtionsMapper {
	private String opertaingCostType;
	private Double costType;
	private String comments;
	private Double inputValues;
}
