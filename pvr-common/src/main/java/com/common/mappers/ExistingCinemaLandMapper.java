package com.common.mappers;

import lombok.Data;

@Data
public class ExistingCinemaLandMapper {

	private String cinemaName;
	private String city;
	private Integer noOfScreen;
	private Integer seatCapacity;
	private String cinemaCategory;
	private Integer atp;
	private Double occupancy;
	private Long admits;
}
