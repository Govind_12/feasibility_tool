package com.common.mappers;

import lombok.Data;

@Data
public class AtpMapper {

	private String cinemaFormat;
	private Long normal;
	private Long Recline;
	private Long Lounger;
}
