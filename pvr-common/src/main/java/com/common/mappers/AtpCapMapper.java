package com.common.mappers;

import lombok.Data;

@Data
public class AtpCapMapper {
	private String screenType;
	private String seatType;
	private Double capValue;

}
