package com.common.mappers;

import lombok.Data;

@Data
public class BenchmarkCinema{

	private String cinemaName;
	private long NavCode;
	private int noOfScreen;
}
