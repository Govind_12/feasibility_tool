package com.common.mappers;

import java.util.List;

import lombok.Data;

@Data
public class ProjectAnalysisMapper {

	private List<BenchMarkSelectionMapper> benchmarkCinemaMapper;
}
