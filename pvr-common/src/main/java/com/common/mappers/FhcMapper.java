package com.common.mappers;

import lombok.Data;

@Data
public class FhcMapper {
	private String name;
	private double value;
	private String message;
}
