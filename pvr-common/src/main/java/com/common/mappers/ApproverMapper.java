package com.common.mappers;

import lombok.Data;

@Data
public class ApproverMapper {
	private String approver;
	private int value;
}
