/*<<<<<<< HEAD
package com.common.mappers;

import java.util.HashMap;
import java.util.List;

import com.common.models.CinemaFormat;
import com.common.models.LesserLesse;
import com.common.models.OtherParameter;
import com.common.models.ProjectLocation;
import com.common.models.PropertyDetail;

import lombok.Data;

@Data
public class MisMapper {
	private String id;
	private String reportId;
	private Integer reportIdSubVersion;
	private Integer reportIdVersion;
	private String reportType;
	private String feasibilityState;
	private String projectType;
	private String projectName;
	private ProjectLocation projectLocation;
	private String developer;
	private String developerOwner;
	private String grossTurnOver;
	private String projectDelivered1;
	private String projectDelivered2;
	private String projectDelivered3;
	private String retailProjectDelivered;
	private Double totalDevlopedAreDelivered;
	private List<String> pvrProjectDelivered;
	private List<String> pvrProjectPipeline;
	private Double totalDeveloperSize;
	private String currentProjectFinancing;
	private Double retailAreaOfProject;
	private String tenancyMix;
	private String upcomingInfrastructureDevlopment;
	private List<CinemaFormat> cinemaFormat;
	private List<CinemaFormat> seatDimension;
	private String projectCostUrl;
	private int status;
	private String DateOfHandover;
	private String DateOfSendForApproval;
	private List<PropertyDetail> pvrProperties;
	private List<PropertyDetail> competitveProperties;
	private String DateOfOpening;
	private List<String> chat;
	private Integer noOfScreens;
	private Integer normalsValSum;
	private Integer reclinerValSum;
	private Integer loungersValSum;
	private Integer totalColumnValSum;
	private Integer avgNoOfShows;
	private String cinemaCategory;
	private LesserLesse lesserLesse;
	private Double projectCost;
	private Object occupancy;
	private Object atp;
	private Object sph;
	private Double totalOverheadSuggestedSum;
	private Double totalOverheadInputSum;
	private OtherParameter showTax;
	private boolean income3dApplicable;
	private Double depreciation;
	private Object admits;
	private Object cityPopulation;
	private Double areaPerSeat;
	private Integer totalSeats;
	private Double projectCostPerScreen;
	private String paymentTerm;
	private Integer freqOfEscalation;
	private Double percOfEscalation;
	private String rentAgreement;
	private Double percentageEscalation;
	private String camAgreement;
	private HashMap<Integer,Double> yearWiseCamRent;
	private HashMap<Integer, Double> yearWiseRent; 
	
	//Revenue Share
	private Object adSalePercentage;
	private Object othersPercentage;
	private Double percentageEscalationInRent_fixed;
	private Double rentFrequencyOfEscaltionYears_fixed;
	
	private String securityDepositType;
	private Object securityCapex;
	private Integer leasePeriod;
	private Integer pvrLockInPeriod;
	private Integer devloperLockedInPeriod;
	private Double coveredArea;
	private Double carpetArea;
	private Object carpetAreaPerSeat;
	private Double loadingPercentage;
	private Double leasableArea;
	private Double height;
	private String rentPaymentTerm;
	private Object rentPerSqFt;
	private Object camPerSqFt;
	private Object rentFrequencyOfEscaltionYears;
	private Object percentageEscalationInRent;
	private Object boxOfficePercentage;
	private Object fAndBPercentage;
	private Double startingYearForMG;
	private String mgType;
	private String camPaymentTerm;
	private Double camRate;
	private Object percentageEscalationInCam;
	private Object frequencyOfEscaltionYears;
	private Object securityDeposit;
	private Object camSecurityDeposit;
	private Object utilitySecurityDeposit;
	private Double securityDepositForZeroMg;
	private String paymentSchedule;
	private Double fromDevloper;
	private Double toDevloper;
	private Double propertyTax;
	private Object atpSphRatio;
	
	private Object revenue;
	private Object netSaleOfTickets;
	private Object salseOfFnB;
	private Object adRevenue;
	private Object vasIncome;
	private Object otherOperatingIncome;
	private Object ebitda;
	private Object ebitdaMargin;
	private Object filmDistributorShare;
	private Object fnbConsumption;
	private Object grossMargin;
	private Object personnelExpenses;
	private Object rent;
	private Object cam;
	private Object electricityWater;
	private Object rm;
	private Object otherOperatingExpense;
	private Object totalFixedExp;
	private Object ebitdaPaybackPeriod;
	private Object postTaxIrr;
	
	private String camType;
	private String rentType;
	
	private Object securityDepositForMonthCam;
	private Object securityDepositForMonthRent;
	
	private Object camSecurityDepositForFixedZeroMg;
	private Object rentSecurityDepositForFixedZeroMg;
	
	private Object mainstream;
	private Object directorsCut;
	private Object gold;
	private Object imax;
	private Object pxl;
	private Object dx;
	private Object playhouse;
	private Object onyx;
	private Object others;
	
=======*/
package com.common.mappers;

import java.util.HashMap;
import java.util.List;

import com.common.models.CinemaFormat;
import com.common.models.LesserLesse;
import com.common.models.OtherParameter;
import com.common.models.ProjectLocation;
import com.common.models.PropertyDetail;

import lombok.Data;

@Data
public class MisMapper {
	private String id;
	private String reportId;
	private Integer reportIdSubVersion;
	private Integer reportIdVersion;
	private String reportType;
	private String feasibilityState;
	private String projectType;
	private String projectName;
	private ProjectLocation projectLocation;
	private String developer;
	private String developerOwner;
	private String grossTurnOver;
	private String projectDelivered1;
	private String projectDelivered2;
	private String projectDelivered3;
	private String retailProjectDelivered;
	private Double totalDevlopedAreDelivered;
	private List<String> pvrProjectDelivered;
	private List<String> pvrProjectPipeline;
	private Double totalDeveloperSize;
	private String currentProjectFinancing;
	private Double retailAreaOfProject;
	private String tenancyMix;
	private String upcomingInfrastructureDevlopment;
	private List<CinemaFormat> cinemaFormat;
	private List<CinemaFormat> seatDimension;
	private String projectCostUrl;
	private int status;
	private String DateOfHandover;
	private String DateOfSendForApproval;
	private List<PropertyDetail> pvrProperties;
	private List<PropertyDetail> competitveProperties;
	private String DateOfOpening;
	private List<String> chat;
	private Integer noOfScreens;
	private Integer normalsValSum;
	private Integer reclinerValSum;
	private Integer loungersValSum;
	private Integer totalColumnValSum;
	private Integer avgNoOfShows;
	private String cinemaCategory;
	private LesserLesse lesserLesse;
	private Double projectCost;
	private Object occupancy;
	private Object atp;
	private Object sph;
	private Double totalOverheadSuggestedSum;
	private Double totalOverheadInputSum;
	private OtherParameter showTax;
	private boolean income3dApplicable;
	private Double depreciation;
	private Object admits;
	private Object cityPopulation;
	private Double areaPerSeat;
	private Integer totalSeats;
	private Double projectCostPerScreen;
	private String paymentTerm;
	private Integer freqOfEscalation;
	private Double percOfEscalation;
	private String rentAgreement;
	private Double percentageEscalation;
	private String camAgreement;
	private HashMap<Integer,Double> yearWiseCamRent;
	private HashMap<Integer, Double> yearWiseRent; 
	
	//Revenue Share
	private Object adSalePercentage;
	private Object othersPercentage;
	private Double percentageEscalationInRent_fixed;
	private Double rentFrequencyOfEscaltionYears_fixed;
	
	private String securityDepositType;
	private Object securityCapex;
	private Integer leasePeriod;
	private Integer pvrLockInPeriod;
	private Integer devloperLockedInPeriod;
	private Double coveredArea;
	private Double carpetArea;
	private Object carpetAreaPerSeat;
	private Double loadingPercentage;
	private Double leasableArea;
	private Double height;
	private String rentPaymentTerm;
	private Object rentPerSqFt;
	private Object camPerSqFt;
	private Object rentFrequencyOfEscaltionYears;
	private Object percentageEscalationInRent;
	private Object boxOfficePercentage;
	private Object fAndBPercentage;
	private Double startingYearForMG;
	private String mgType;
	private String camPaymentTerm;
	private Double camRate;
	private Object percentageEscalationInCam;
	private Object frequencyOfEscaltionYears;
	private Object securityDeposit;
	private Object camSecurityDeposit;
	private Object utilitySecurityDeposit;
	private Double securityDepositForZeroMg;
	private String paymentSchedule;
	private Double fromDevloper;
	private Double toDevloper;
	private Double propertyTax;
	private Object atpSphRatio;
	
	private Object revenue;
	private Object netSaleOfTickets;
	private Object salseOfFnB;
	private Object adRevenue;
	private Object vasIncome;
	private Object otherOperatingIncome;
	private Object ebitda;
	private Object ebitdaMargin;
	private Object filmDistributorShare;
	private Object fnbConsumption;
	private Object grossMargin;
	private Object personnelExpenses;
	private Object rent;
	private Object cam;
	private Object electricityWater;
	private Object rm;
	private Object otherOperatingExpense;
	private Object totalFixedExp;
	private Object ebitdaPaybackPeriod;
	private Object postTaxIrr;
	
	private String camType;
	private String rentType;
	
	private Object securityDepositForMonthCam;
	private Object securityDepositForMonthRent;
	
	private Object camSecurityDepositForFixedZeroMg;
	private Object rentSecurityDepositForFixedZeroMg;
	
	private Object mainstream;
	private Object directorsCut;
	private Object gold;
	private Object imax;
	private Object pxl;
	private Object dx;
	private Object playhouse;
	private Object onyx;
	private Object others;
	private String createdBy;
	
}