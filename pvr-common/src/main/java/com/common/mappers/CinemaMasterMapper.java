package com.common.mappers;

import lombok.Data;

@Data
public class CinemaMasterMapper {

	private int hopk;
	private String cinema_strCode;
	private String cinema_name;
	private String vista_info_Cinema_Name;
	private String cinema_location;
	private String state_strName;
	private String nav_cinemaCode;
	private String owner_strCompany;
	private String cinema_Resign;
	private String nav_cinemadesc;
	private String cinema_str_Run;
	private String city_tier;
	private String infoworks_cin_str;
	private int loyalty_complex_id;
	private float latitude;
	private float longitude;
}
