package com.api.utils;

import org.springframework.stereotype.Component;
import com.common.constants.Constants;

@Component
public class StateAndCityUtil {

	public static String getCity(String cityName) {
		String city = "";
		if (cityName.equals(Constants.Prayagraj)) {
			city = Constants.Allahabad;
		} else if (cityName.equals(Constants.Mysuru)) {
			city = Constants.Mysore;
		} else if (cityName.equals(Constants.Vijayawada)) {
			city = Constants.Vijaywada;
		} else if (cityName.equals(Constants.NEW_DELHI)) {
			city = Constants.DELHI;
		} else if (cityName.equals(Constants.GURUGRAM)) {
			city = Constants.GURGAON;
		} else {
			city = cityName;
		}
		return city;
	}
}
