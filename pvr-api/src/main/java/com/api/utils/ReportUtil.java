package com.api.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.common.constants.Constants;

@Component
public class ReportUtil {

	public static List<String> getReportHtmlTemplate(){
		List<String> htmlTemplate=new ArrayList<>();
		htmlTemplate.add(Constants.HOME);
		htmlTemplate.add(Constants.INDEX);
		htmlTemplate.add(Constants.PROJECT_SYNOPSIS);
		htmlTemplate.add(Constants.CATCHMENT_DETAILS);
		htmlTemplate.add(Constants.EXISTING_CINEMA_LANDSACPE);
		htmlTemplate.add(Constants.UPCOMING_CINEMA_LANDSCAPE);
		htmlTemplate.add(Constants.CINEMA_LANDSCAPE_IN_NEXT_5_YEAR);
		htmlTemplate.add(Constants.UPCOMING_CINEMA_PROPOSITION);
		htmlTemplate.add(Constants.APPENDIX_PROJECT_PRESENTATION);
		htmlTemplate.add(Constants.APPENDIX_DEVELOPER_CREDIT_RATING_REPORT);
		htmlTemplate.add(Constants.DEVELOPMENT_DETAILS);
		htmlTemplate.add(Constants.DEVELOPER_DETAILS);
		htmlTemplate.add(Constants.CINEMA_SPECS);
		htmlTemplate.add(Constants.CINEMA_FORMAT_SEATDIMENSION);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS_FIXED);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS_RENT_REVENUE);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS_RENT_MAX);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS_RENT_OTHER_ONE_FIXED);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS_RENT_OTHER_ONE_AVERAGE);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS_RENT_OTHER_TWO);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS_CAM_FIXED);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS_CAM_ACTUALS);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS_CAM_INCLUDEDINRENT);
		htmlTemplate.add(Constants.KEY_LEASE_TERMS_SECURITY_DEPOSITE);
		htmlTemplate.add(Constants.FEASIBIITY_SUMMARY_AND_PAYBACKS_ANALYSIS);
		htmlTemplate.add(Constants.SUMMARY_PANDL);
		htmlTemplate.add(Constants.COMMON_SIZE_PL_AND_L);
		htmlTemplate.add(Constants.KEY_REVENUE_ASSUMPTION_OCCUPANCY);
		htmlTemplate.add(Constants.KEY_REVENUE_ASSUMPTION_ADMITS);
		htmlTemplate.add(Constants.KEY_REVENUE_ASSUMPTION_ATP);
		htmlTemplate.add(Constants.KEY_REVENUE_ASSUMPTION_SPH);
		htmlTemplate.add(Constants.KEY_REVENUE_ASSUMPTION_AD_SALE_AND_OTHERS);
		htmlTemplate.add(Constants.KEY_OPERATING_COST_ASSUMPTION);
		htmlTemplate.add(Constants.KEY_PROJECT_COST_ASSUMPTION);
		htmlTemplate.add(Constants.COMMENTS_SECTION);
		htmlTemplate.add(Constants.POST_SIGNING_COST_COMPARISON);
		htmlTemplate.add(Constants.PRE_HANDOVER_COST_COMPARISON);
		htmlTemplate.add(Constants.KEY_ASSUMPTIONS);
		htmlTemplate.add(Constants.DEVELOPER_SCOPE_OF_WORK);
		htmlTemplate.add(Constants.APPENDIX);
		htmlTemplate.add(Constants.StepOne);
		htmlTemplate.add(Constants.StepTwo);
		htmlTemplate.add(Constants.StepFive);
		
		return htmlTemplate;
	}
	
	public static List<String> getReportHtmlImages(){
		List<String> htmlTemplate=new ArrayList<>();
		htmlTemplate.add(Constants.HOME);
		htmlTemplate.add(Constants.CATCHMENT_DETAILS);
		htmlTemplate.add(Constants.EXISTING_CINEMA_LANDSACPE);
		htmlTemplate.add(Constants.UPCOMING_CINEMA_LANDSCAPE);
		htmlTemplate.add(Constants.CINEMA_LANDSCAPE_IN_NEXT_5_YEAR);
		return htmlTemplate;
	}
}
