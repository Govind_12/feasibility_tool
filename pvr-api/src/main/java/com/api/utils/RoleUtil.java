package com.api.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.common.constants.Constants;
import com.common.models.FeasibilityUser;

public class RoleUtil {

	public static boolean isAdmin(FeasibilityUser hitechUser) {

		for (GrantedAuthority authorities : hitechUser.getAuthorities()) {
			if ((authorities.getAuthority()).equals(Constants.ROLE_ADMIN)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isCurrentUserHasAdminRole() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		FeasibilityUser hitechUser = (FeasibilityUser) auth.getPrincipal();
		for (GrantedAuthority authorities : hitechUser.getAuthorities()) {
			if ((authorities.getAuthority()).equals(Constants.ROLE_ADMIN)) {
				return true;
			}
		}

		return false;
	}

	public static boolean isCurrentUserHasSuperAdminRole() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		FeasibilityUser hitechUser = (FeasibilityUser) auth.getPrincipal();
		for (GrantedAuthority authorities : hitechUser.getAuthorities()) {
			if ((authorities.getAuthority()).equals(Constants.ROLE_SUPER_ADMIN)) {
				return true;
			}
		}

		return false;
	}
	
	
	public static boolean isCurrentUserHasApproverRole() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		FeasibilityUser hitechUser = (FeasibilityUser) auth.getPrincipal();
		for (GrantedAuthority authorities : hitechUser.getAuthorities()) {
			if ((authorities.getAuthority()).equals(Constants.ROLE_APPROVER)) {
				return true;
			}
		}

		return false;
	}
	
	
	public static boolean isCurrentUserHasBDRole() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		FeasibilityUser hitechUser = (FeasibilityUser) auth.getPrincipal();
		for (GrantedAuthority authorities : hitechUser.getAuthorities()) {
			if ((authorities.getAuthority()).equals(Constants.ROLE_BD)) {
				return true;
			}
		}

		return false;
	}
	
	public static boolean isCurrentUserHasProjectTeamRole() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		FeasibilityUser hitechUser = (FeasibilityUser) auth.getPrincipal();
		for (GrantedAuthority authorities : hitechUser.getAuthorities()) {
			if ((authorities.getAuthority()).equals(Constants.ROLE_PROJECT_TEAM)) {
				return true;
			}
		}

		return false;
	}
	
	public static boolean isCurrentUserHasOpeartionRole() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		FeasibilityUser hitechUser = (FeasibilityUser) auth.getPrincipal();
		for (GrantedAuthority authorities : hitechUser.getAuthorities()) {
			if ((authorities.getAuthority()).equals(Constants.ROLE_OPERATION_TEAM)) {
				return true;
			}
		}

		return false;
	}
	
	
	

	public static FeasibilityUser getCurrentUseInfo() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();		
		return (FeasibilityUser) auth.getPrincipal();
	}
	
	public static String getCurrentUserID() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();		
		FeasibilityUser PVRUser = (FeasibilityUser) auth.getPrincipal(); 
		return PVRUser.getId();
	}
	
	public static String getCurrentUserSingleRoleString() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();		
		FeasibilityUser PVRUser = (FeasibilityUser) auth.getPrincipal(); 
		return PVRUser.getAuthorities().toString().substring(1, PVRUser.getAuthorities().toString().length()-1);
	}

	public static boolean isCurrentUserHasViewerRole() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		FeasibilityUser hitechUser = (FeasibilityUser) auth.getPrincipal();
		for (GrantedAuthority authorities : hitechUser.getAuthorities()) {
			if (authorities.getAuthority().equals("ROLE_VIEW"))
				return true;
		}
		return false;
	}
	
}

