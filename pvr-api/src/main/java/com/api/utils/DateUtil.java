package com.api.utils;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.common.constants.Constants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DateUtil {

	public static long getCurrentTimeInMilliseconds() {
		return System.currentTimeMillis();
	}

	public static Date getDate(String date, String pattern) {
		DateFormat format = new SimpleDateFormat(pattern);
		try {
			return format.parse(date);
		} catch (ParseException e) {
			log.debug("Invalid date format " + date);
			return null;
		}
	}

	public static boolean isValidDate(String dateString, String pattern) {
		Date date = getDate(dateString, pattern);
		if (date == null) {
			return false;
		}
		return true;
	}

	public static List<Date> splitDuration(Date startDate, Date endDate, long splitSize) {
		long startMillis = startDate.getTime();
		long endMillis = endDate.getTime();
		List<Date> list = new ArrayList<Date>();
		while (startMillis <= endMillis) {
			list.add(new Date(startMillis));
			startMillis += splitSize;
		}
		return list;
	}

	public static Date getStartOfDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static Date getEndOfDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	public static String getMonthName(int monthId) {
		return new DateFormatSymbols().getMonths()[monthId];
	}

	public static int getCurrentYear() {
		Calendar now = Calendar.getInstance();
		return now.get(Calendar.YEAR);
	}

	public static int getCurrentMonth() {
		Calendar now = Calendar.getInstance();
		return now.get(Calendar.MONTH) + 1;
	}

	public static String getCurrentQuaterYear() {
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int previousYear = Calendar.getInstance().get(Calendar.YEAR) - 1;
		String str1 = String.valueOf(currentYear).substring(2);
		String year = previousYear + "-" + str1;
		return year;
	}

	public static String getnextQuaterYear() {
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int previousYear = Calendar.getInstance().get(Calendar.YEAR) + 1;
		String str = String.valueOf(previousYear).substring(2);
		int prev = Integer.parseInt(str);
		String finalYear = currentYear + "-" + prev;
		return finalYear;
	}
	
	public static String getPreviousYear() {
		int currentYear = Calendar.getInstance().get(Calendar.YEAR)-2;
		int previousYear = Calendar.getInstance().get(Calendar.YEAR) - 1;
		String str = String.valueOf(previousYear).substring(2);
		int prev = Integer.parseInt(str);
		String finalYear = currentYear + "-" + prev;
		//System.out.println(finalYear);
		return finalYear;
	}
	
	public static Map<String, Date> getDateByCurrentQuater() {
		Map<String, Date> map = new HashMap<>();
		String currentQuarter = getCurrentQuater();

		if (currentQuarter.equals("Q1")) {

			Calendar calender = Calendar.getInstance();
			calender.set(Calendar.getInstance().get(Calendar.YEAR) - 1, 3, 1);
			Date d1 = calender.getTime();

			Calendar calender1 = Calendar.getInstance();
			calender1.set(Calendar.getInstance().get(Calendar.YEAR), 2, 31);
			Date d2 = calender1.getTime();

			map.put(Constants.STARTDATE, d1);
			map.put(Constants.ENDDATE, d2);
		} else if (currentQuarter.equals("Q2")) {

			Calendar calender = Calendar.getInstance();

			calender.set(Calendar.getInstance().get(Calendar.YEAR) - 1, 6, 1);
			Date d1 = calender.getTime();

			Calendar calender1 = Calendar.getInstance();
			calender1.set(Calendar.getInstance().get(Calendar.YEAR), 5, 30);
			Date d2 = calender1.getTime();

			map.put(Constants.STARTDATE, d1);
			map.put(Constants.ENDDATE, d2);
		} else if (currentQuarter.equals("Q3")) {
			Calendar calender = Calendar.getInstance();
			calender.set(Calendar.getInstance().get(Calendar.YEAR) - 1, 9, 1);
			Date d1 = calender.getTime();

			Calendar calender1 = Calendar.getInstance();
			calender1.set(Calendar.getInstance().get(Calendar.YEAR), 8, 30);
			Date d2 = calender1.getTime();

			map.put(Constants.STARTDATE, d1);
			map.put(Constants.ENDDATE, d2);
		} else if (currentQuarter.equals("Q4")) {
			Calendar calender = Calendar.getInstance();
			calender.set(Calendar.getInstance().get(Calendar.YEAR) - 1, 0, 1);
			Date d1 = calender.getTime();

			Calendar calender1 = Calendar.getInstance();
			calender1.set(Calendar.getInstance().get(Calendar.YEAR), 11, 31);
			Date d2 = calender1.getTime();

			map.put(Constants.STARTDATE, d1);
			map.put(Constants.ENDDATE, d2);
		}

		return map;
	}
	
	public static String getCurrentQuater() {
		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);

		return (month >= Calendar.JANUARY && month <= Calendar.MARCH) ? "Q4"
				: (month >= Calendar.APRIL && month <= Calendar.JUNE) ? "Q1"
						: (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) ? "Q2" : "Q3";
	}
	
	public static Date getDateBy(String dates) throws ParseException {
		DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = sourceFormat.parse(dates);
		return date;
	}

}
