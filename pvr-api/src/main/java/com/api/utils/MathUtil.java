package com.api.utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.common.constants.Constants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MathUtil {

	public static Double sum(List<Double> list) {
		Double sum = 0.0;
		for (Double i : list) {
			sum += i;
		}
		return sum;
	}

	public static double distance(double startLat, double startLong, double endLat, double endLong) {

		double dLat = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return Constants.EARTH_RADIUS * c;
	}

	public static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}

	public static double distanceInKm(double lat1, double lon1, double lat2, double lon2) {
		int R = 6373; // radius of the earth in kilometres
		double lat1rad = Math.toRadians(lat1);
		double lat2rad = Math.toRadians(lat2);
		double deltaLat = Math.toRadians(lat2 - lat1);
		double deltaLon = Math.toRadians(lon2 - lon1);

		double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2)
				+ Math.cos(lat1rad) * Math.cos(lat2rad) * Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		double d = R * c;
		return d;
	}

	public static List<String> getAllCostTypeAlgo() {
		List<String> costType = new ArrayList<>();
		costType.add(Constants.LEGAL_EXPENSE);
		costType.add(Constants.INTERNET_CHARGES);
		costType.add(Constants.INSURANCE_EXPENSE);
		costType.add(Constants.TRAVELLING_CONVEYANCE);
		costType.add(Constants.WATERANDELECTRICITY);
		costType.add(Constants.PRINTINGANDSTATIONARY);
		costType.add(Constants.PERSONNAL_COST);
		costType.add(Constants.HOUSE_KEEPING);
		costType.add(Constants.SECURITY);
		costType.add(Constants.RANDM_COST);
		costType.add(Constants.MARKETING_COST);
		costType.add(Constants.COMMUNICATION);
		costType.add(Constants.MISCELLANEOUS);
		costType.add(Constants.AD_REVENUE);
		costType.add(Constants.WEB_SALE);
		costType.add(Constants.OCCUPANCY);
		costType.add(Constants.OVER_HEAD_COST);
		costType.add(Constants.ATP);
		return costType;
	}

	public static List<String> getAllReveNueTypeAlgo() {
		List<String> costType = new ArrayList<>();
		costType.add(Constants.OCCUPANCY);
		costType.add(Constants.ATP);
		costType.add(Constants.SPH);
		return costType;
	}

	public static double roundTwoDecimals(double d) {
		DecimalFormat twoDecimals = new DecimalFormat("#.00");
		return Double.valueOf(twoDecimals.format(d));
	}
	
	public static List<Integer> getAllYear() {
		List<Integer> year = new ArrayList<>();
		year.add(Constants.YEAR_1);
		year.add(Constants.YEAR_2);
		year.add(Constants.YEAR_3);
		year.add(Constants.YEAR_4);
		year.add(Constants.YEAR_5);
		return year;
	}
}
