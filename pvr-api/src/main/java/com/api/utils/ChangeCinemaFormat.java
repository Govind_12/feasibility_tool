package com.api.utils;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.ObjectUtils;

import com.common.constants.Constants;
import com.common.models.AssumptionsValues;
import com.common.models.AtpAssumptions;
import com.common.models.ProjectDetails;

public class ChangeCinemaFormat {

	public static List<AssumptionsValues> changeOccupancyFormat(List<AssumptionsValues> assumptionValues) {
		// List<AssumptionsValues> assumptionValues = projectDetails.getOccupancy();
//		System.out.println("2 : " + assumptionValues.toString());
		List<AssumptionsValues> cinemaValues = new ArrayList<>();
		if (!ObjectUtils.isEmpty(assumptionValues)) {
			for (AssumptionsValues assumptionsValues : assumptionValues) {
				AssumptionsValues c = new AssumptionsValues();
				if(!ObjectUtils.isEmpty(assumptionsValues.getFormatType())){
					if (assumptionsValues.getFormatType().equals(Constants.MAINSTREAM))
						c.setFormatType(CinemaFormat.MAINSTREAM.getValue());
					if (assumptionsValues.getFormatType().equals(Constants.GOLD))
						c.setFormatType(CinemaFormat.GOLD.getValue());
					if (assumptionsValues.getFormatType().equals(Constants.IMAX))
						c.setFormatType(CinemaFormat.IMAX.getValue());
					if (assumptionsValues.getFormatType().equals(Constants.PXL))
						c.setFormatType(CinemaFormat.PXL.getValue());
					if (assumptionsValues.getFormatType().equals(Constants.PLAYHOUSE))
						c.setFormatType(CinemaFormat.PLAYHOUSE.getValue());
					if (assumptionsValues.getFormatType().equals(Constants.ULTRAPREMIUM))
						c.setFormatType(CinemaFormat.ULTRAPREMIUM.getValue());
					if (assumptionsValues.getFormatType().equals(Constants.DX))
						c.setFormatType(CinemaFormat.DX.getValue());
					if (assumptionsValues.getFormatType().equals(Constants.ONYX))
						c.setFormatType(CinemaFormat.ONYX.getValue());
					if (assumptionsValues.getFormatType().equals(Constants.LUXE))
						c.setFormatType(CinemaFormat.LUXE.getValue());
				}
				
				c.setCostType(assumptionsValues.getCostType());
				c.setInputValues(assumptionsValues.getInputValues());
				c.setComments(assumptionsValues.getComments());
				c.setAdmits(assumptionsValues.getAdmits());
				c.setIsPresent(assumptionsValues.getIsPresent());
				c.setQuery(assumptionsValues.getQuery());
				c.setSuggestedValue(assumptionsValues.getSuggestedValue());
				cinemaValues.add(c);
			}
		}

		return cinemaValues;
	}

	public static List<AssumptionsValues> changeSphFormat(List<AssumptionsValues> sph, ProjectDetails projectDetails) {
		List<AssumptionsValues> cinemaValues = new ArrayList<>();

		long blendedSuggestedValue = 0;
		double blendedInputValue = 0;
		for (AssumptionsValues assumptionsValues : sph) {
			AssumptionsValues c = new AssumptionsValues();
			if(!ObjectUtils.isEmpty(assumptionsValues.getFormatType())){
				if (assumptionsValues.getFormatType().equals(Constants.MAINSTREAM))
					c.setFormatType(CinemaFormat.MAINSTREAM.getValue());
				if (assumptionsValues.getFormatType().equals(Constants.GOLD))
					c.setFormatType(CinemaFormat.GOLD.getValue());
				if (assumptionsValues.getFormatType().equals(Constants.IMAX))
					c.setFormatType(CinemaFormat.IMAX.getValue());
				if (assumptionsValues.getFormatType().equals(Constants.PXL))
					c.setFormatType(CinemaFormat.PXL.getValue());
				if (assumptionsValues.getFormatType().equals(Constants.PLAYHOUSE))
					c.setFormatType(CinemaFormat.PLAYHOUSE.getValue());
				if (assumptionsValues.getFormatType().equals(Constants.ULTRAPREMIUM))
					c.setFormatType(CinemaFormat.ULTRAPREMIUM.getValue());
				if (assumptionsValues.getFormatType().equals(Constants.DX))
					c.setFormatType(CinemaFormat.DX.getValue());
				if (assumptionsValues.getFormatType().equals(Constants.ONYX))
					c.setFormatType(CinemaFormat.ONYX.getValue());
				if (assumptionsValues.getFormatType().equals(Constants.LUXE))
					c.setFormatType(CinemaFormat.LUXE.getValue());
			
				c.setCostType(assumptionsValues.getCostType());
				c.setInputValues(assumptionsValues.getInputValues());
				c.setComments(assumptionsValues.getComments());
				c.setAdmits(assumptionsValues.getAdmits());
				c.setIsPresent(assumptionsValues.getIsPresent());
				c.setQuery(assumptionsValues.getQuery());
				c.setSuggestedValue(assumptionsValues.getSuggestedValue());
				cinemaValues.add(c);
				
				List<com.common.models.CinemaFormat> cinemaFormat = projectDetails.getCinemaFormat();
				for(com.common.models.CinemaFormat cinema : cinemaFormat) {
					if(cinema.getFormatType().equalsIgnoreCase(c.getFormatType())) {
						blendedSuggestedValue += (!ObjectUtils.isEmpty(cinema.getTotal()) ? cinema.getTotal() : 0);
						blendedInputValue += (!ObjectUtils.isEmpty(assumptionsValues.getInputValues()) ? assumptionsValues.getInputValues() : 0)
								* (!ObjectUtils.isEmpty(cinema.getTotal()) ? cinema.getTotal() : 0);
					}
					
				}
				
//				blendedSuggestedValue += (!ObjectUtils.isEmpty(assumptionsValues.getCostType()) ? assumptionsValues.getCostType() : 0);
//				blendedInputValue += (!ObjectUtils.isEmpty(assumptionsValues.getInputValues()) ? assumptionsValues.getInputValues() : 0)
//						* (!ObjectUtils.isEmpty(assumptionsValues.getCostType()) ? assumptionsValues.getCostType() : 0);
			}
		}
		
    	AssumptionsValues blended = new AssumptionsValues();
		if (sph.size() != 0) {
			blended.setFormatType("BLENDED SPH");
			if (blendedSuggestedValue > 0) {
				blended.setInputValues(Double
						.parseDouble(new DecimalFormat("#.00").format(blendedInputValue / blendedSuggestedValue)));
			}else {
				blended.setInputValues(0.0);
			}
			blended.setCostType((long) 0);
		}
        cinemaValues.add(blended);
	
		return cinemaValues;

	}

	public static List<AtpAssumptions> changeAtpFormat(List<AtpAssumptions> atp, ProjectDetails projectDetails) {
		List<AtpAssumptions> cinemaValues = new ArrayList<>();

		for (AtpAssumptions atpValues : atp) {
			
			AtpAssumptions c = new AtpAssumptions();
			AssumptionsValues blendedAtp = new AssumptionsValues();
			double blendedSuggestedValue = 0;
			double blendedInputValue = 0;
			
			if(!ObjectUtils.isEmpty(atpValues.getCinemaFormatType())){
				if (atpValues.getCinemaFormatType().equals(Constants.MAINSTREAM))
					c.setCinemaFormatType(CinemaFormat.MAINSTREAM.getValue());
				if (atpValues.getCinemaFormatType().equals(Constants.GOLD))
					c.setCinemaFormatType(CinemaFormat.GOLD.getValue());
				if (atpValues.getCinemaFormatType().equals(Constants.IMAX))
					c.setCinemaFormatType(CinemaFormat.IMAX.getValue());
				if (atpValues.getCinemaFormatType().equals(Constants.PXL))
					c.setCinemaFormatType(CinemaFormat.PXL.getValue());
				if (atpValues.getCinemaFormatType().equals(Constants.PLAYHOUSE))
					c.setCinemaFormatType(CinemaFormat.PLAYHOUSE.getValue());
				if (atpValues.getCinemaFormatType().equals(Constants.ULTRAPREMIUM))
					c.setCinemaFormatType(CinemaFormat.ULTRAPREMIUM.getValue());
				if (atpValues.getCinemaFormatType().equals(Constants.DX))
					c.setCinemaFormatType(CinemaFormat.DX.getValue());
				if (atpValues.getCinemaFormatType().equals(Constants.ONYX))
					c.setCinemaFormatType(CinemaFormat.ONYX.getValue());
				if (atpValues.getCinemaFormatType().equals(Constants.LUXE))
					c.setCinemaFormatType(CinemaFormat.LUXE.getValue());
				
				blendedAtp.setFormatType("BLENDED "+(c.getCinemaFormatType() != null ? c.getCinemaFormatType().toUpperCase() : "")+" ATP");
			
				c.setLounger(atpValues.getLounger());
				c.setNormal(atpValues.getNormal());
				c.setRecliner(atpValues.getRecliner());
				c.setQuery(atpValues.getQuery());
				
				List<com.common.models.CinemaFormat> cinemaFormat = projectDetails.getCinemaFormat();
				for(com.common.models.CinemaFormat cinema : cinemaFormat) {
					if(cinema.getFormatType().equalsIgnoreCase(c.getCinemaFormatType())) {
						blendedSuggestedValue = (!ObjectUtils.isEmpty(cinema.getNormal()) ? cinema.getNormal() : 0)
					              + (!ObjectUtils.isEmpty(cinema.getLounger()) ? cinema.getLounger() : 0)
					              + (!ObjectUtils.isEmpty(cinema.getRecliners()) ? cinema.getRecliners() : 0);
						
						blendedInputValue = ((!ObjectUtils.isEmpty(atpValues.getNormal().getInputValues()) ? atpValues.getNormal().getInputValues() : 0)
					            * (!ObjectUtils.isEmpty(cinema.getNormal()) ? cinema.getNormal() : 0))
				              + ((!ObjectUtils.isEmpty(atpValues.getLounger().getInputValues()) ? atpValues.getLounger().getInputValues() : 0)
				               * (!ObjectUtils.isEmpty(cinema.getLounger()) ? cinema.getLounger() : 0))
				              + ((!ObjectUtils.isEmpty(atpValues.getRecliner().getInputValues()) ? atpValues.getRecliner().getInputValues() : 0)
				              * (!ObjectUtils.isEmpty(cinema.getRecliners()) ? cinema.getRecliners() : 0));

					}
					
				}
							
//				blendedSuggestedValue = (!ObjectUtils.isEmpty(atpValues.getNormal().getCostType()) ? atpValues.getNormal().getCostType() : 0)
//						              + (!ObjectUtils.isEmpty(atpValues.getLounger().getCostType()) ? atpValues.getLounger().getCostType() : 0)
//						              + (!ObjectUtils.isEmpty(atpValues.getRecliner().getCostType()) ? atpValues.getRecliner().getCostType() : 0);
//				
//				blendedInputValue = ((!ObjectUtils.isEmpty(atpValues.getNormal().getInputValues()) ? atpValues.getNormal().getInputValues() : 0)
//						            * (!ObjectUtils.isEmpty(atpValues.getNormal().getCostType()) ? atpValues.getNormal().getCostType() : 0))
//					              + ((!ObjectUtils.isEmpty(atpValues.getLounger().getInputValues()) ? atpValues.getLounger().getInputValues() : 0)
//					               * (!ObjectUtils.isEmpty(atpValues.getLounger().getCostType()) ? atpValues.getLounger().getCostType() : 0))
//					              + ((!ObjectUtils.isEmpty(atpValues.getRecliner().getInputValues()) ? atpValues.getRecliner().getInputValues() : 0)
//					              * (!ObjectUtils.isEmpty(atpValues.getRecliner().getCostType()) ? atpValues.getRecliner().getCostType() : 0));
//	
				blendedAtp.setCostType((long)0);
				if(blendedSuggestedValue > 0) {
				  blendedAtp.setInputValues(Double.parseDouble(new DecimalFormat("#.00").format(blendedInputValue/blendedSuggestedValue)));
				}else {
					blendedAtp.setInputValues(0.0);
				}
				c.setBlendedAtp(blendedAtp);
				
				cinemaValues.add(c);
			}
			
		}

		return cinemaValues;

	}

	public static List<AssumptionsValues> reformatOccupancy(List<AssumptionsValues> occupancy) {

		List<AssumptionsValues> cinemaValues = new ArrayList<>();
		for (AssumptionsValues assumptionsValues : occupancy) {
			AssumptionsValues c = new AssumptionsValues();
			if (assumptionsValues.getFormatType().equals(CinemaFormat.MAINSTREAM.getValue()))
				c.setFormatType(Constants.MAINSTREAM);
			if (assumptionsValues.getFormatType().equals(CinemaFormat.GOLD.getValue()))
				c.setFormatType(Constants.GOLD);
			if (assumptionsValues.getFormatType().equals(CinemaFormat.IMAX.getValue()))
				c.setFormatType(Constants.IMAX);
			if (assumptionsValues.getFormatType().equals(CinemaFormat.PXL.getValue()))
				c.setFormatType(Constants.PXL);
			if (assumptionsValues.getFormatType().equals(CinemaFormat.PLAYHOUSE.getValue()))
				c.setFormatType(Constants.PLAYHOUSE);
			if (assumptionsValues.getFormatType().equals(CinemaFormat.ULTRAPREMIUM.getValue()))
				c.setFormatType(Constants.ULTRAPREMIUM);
			if (assumptionsValues.getFormatType().equals(CinemaFormat.DX.getValue()))
				c.setFormatType(Constants.DX);
			if (assumptionsValues.getFormatType().equals(CinemaFormat.ONYX.getValue()))
				c.setFormatType(Constants.ONYX);
			if (assumptionsValues.getFormatType().equals(CinemaFormat.LUXE.getValue()))
				c.setFormatType(Constants.LUXE);

			c.setCostType(assumptionsValues.getCostType());
			c.setInputValues(assumptionsValues.getInputValues());
			c.setComments(assumptionsValues.getComments());
			c.setAdmits(assumptionsValues.getAdmits());
			c.setIsPresent(assumptionsValues.getIsPresent());
			c.setQuery(assumptionsValues.getQuery());
			c.setSuggestedValue(assumptionsValues.getSuggestedValue());
			cinemaValues.add(c);
		}

		return cinemaValues;
	}

	public static List<AtpAssumptions> reformatAtp(List<AtpAssumptions> atp) {
		List<AtpAssumptions> cinemaValues = new ArrayList<>();

		for (AtpAssumptions atpValues : atp) {
			AtpAssumptions c = new AtpAssumptions();
			AssumptionsValues blendedAtp = new AssumptionsValues();
			double blendedSuggestedValue = 0;
			double blendedInputValue = 0;
			
			if(!ObjectUtils.isEmpty(atpValues.getCinemaFormatType())){
				if (atpValues.getCinemaFormatType().equals(CinemaFormat.MAINSTREAM.getValue()))
					c.setCinemaFormatType(Constants.MAINSTREAM);
				if (atpValues.getCinemaFormatType().equals(CinemaFormat.GOLD.getValue()))
					c.setCinemaFormatType(Constants.GOLD);
				if (atpValues.getCinemaFormatType().equals(CinemaFormat.IMAX.getValue()))
					c.setCinemaFormatType(Constants.IMAX);
				if (atpValues.getCinemaFormatType().equals(CinemaFormat.PXL.getValue()))
					c.setCinemaFormatType(Constants.PXL);
				if (atpValues.getCinemaFormatType().equals(CinemaFormat.PLAYHOUSE.getValue()))
					c.setCinemaFormatType(Constants.PLAYHOUSE);
				if (atpValues.getCinemaFormatType().equals(CinemaFormat.ULTRAPREMIUM.getValue()))
					c.setCinemaFormatType(Constants.ULTRAPREMIUM);
				if (atpValues.getCinemaFormatType().equals(CinemaFormat.DX.getValue()))
					c.setCinemaFormatType(Constants.DX);
				if (atpValues.getCinemaFormatType().equals(CinemaFormat.ONYX.getValue()))
					c.setCinemaFormatType(Constants.ONYX);
				if (atpValues.getCinemaFormatType().equals(CinemaFormat.LUXE.getValue()))
					c.setCinemaFormatType(Constants.LUXE);
	
				c.setLounger(atpValues.getLounger());
				c.setNormal(atpValues.getNormal());
				c.setRecliner(atpValues.getRecliner());
				c.setQuery(atpValues.getQuery());
				
//				blendedAtp.setFormatType("BLENDED "+(c.getCinemaFormatType() != null ? c.getCinemaFormatType().toUpperCase() : "")+" ATP");
//							
//				blendedSuggestedValue = (!ObjectUtils.isEmpty(atpValues.getNormal().getCostType()) ? atpValues.getNormal().getCostType() : 0)
//						              + (!ObjectUtils.isEmpty(atpValues.getLounger().getCostType()) ? atpValues.getLounger().getCostType() : 0)
//						              + (!ObjectUtils.isEmpty(atpValues.getRecliner().getCostType()) ? atpValues.getRecliner().getCostType() : 0);
//				
//				blendedInputValue = ((!ObjectUtils.isEmpty(atpValues.getNormal().getInputValues()) ? atpValues.getNormal().getInputValues() : 0)
//						            * (!ObjectUtils.isEmpty(atpValues.getNormal().getCostType()) ? atpValues.getNormal().getCostType() : 0))
//					              + ((!ObjectUtils.isEmpty(atpValues.getLounger().getInputValues()) ? atpValues.getLounger().getInputValues() : 0)
//					               * (!ObjectUtils.isEmpty(atpValues.getLounger().getCostType()) ? atpValues.getLounger().getCostType() : 0))
//					              + ((!ObjectUtils.isEmpty(atpValues.getRecliner().getInputValues()) ? atpValues.getRecliner().getInputValues() : 0)
//					              * (!ObjectUtils.isEmpty(atpValues.getRecliner().getCostType()) ? atpValues.getRecliner().getCostType() : 0));
//	
//				blendedAtp.setCostType((long)0);
//				if(blendedSuggestedValue > 0) {
//				  blendedAtp.setInputValues(Double.parseDouble(new DecimalFormat("#.00").format(blendedInputValue/blendedSuggestedValue)));
//				}else {
//					blendedAtp.setInputValues(0.0);
//				}
//				c.setBlendedAtp(blendedAtp);
				
				cinemaValues.add(c);
			}
		}

		return cinemaValues;
	}

	public static List<AssumptionsValues> reformatSph(List<AssumptionsValues> sph) {
		List<AssumptionsValues> cinemaValues = new ArrayList<>();

		long blendedSuggestedValue = 0;
		double blendedInputValue = 0;

		for (AssumptionsValues assumptionsValues : sph) {
			AssumptionsValues c = new AssumptionsValues();
			if (!ObjectUtils.isEmpty(assumptionsValues.getFormatType())) {
				if (assumptionsValues.getFormatType().equals(CinemaFormat.MAINSTREAM.getValue()))
					c.setFormatType(Constants.MAINSTREAM);
				if (assumptionsValues.getFormatType().equals(CinemaFormat.GOLD.getValue()))
					c.setFormatType(Constants.GOLD);
				if (assumptionsValues.getFormatType().equals(CinemaFormat.IMAX.getValue()))
					c.setFormatType(Constants.IMAX);
				if (assumptionsValues.getFormatType().equals(CinemaFormat.PXL.getValue()))
					c.setFormatType(Constants.PXL);
				if (assumptionsValues.getFormatType().equals(CinemaFormat.PLAYHOUSE.getValue()))
					c.setFormatType(Constants.PLAYHOUSE);
				if (assumptionsValues.getFormatType().equals(CinemaFormat.ULTRAPREMIUM.getValue()))
					c.setFormatType(Constants.ULTRAPREMIUM);
				if (assumptionsValues.getFormatType().equals(CinemaFormat.DX.getValue()))
					c.setFormatType(Constants.DX);
				if (assumptionsValues.getFormatType().equals(CinemaFormat.ONYX.getValue()))
					c.setFormatType(Constants.ONYX);
				if (assumptionsValues.getFormatType().equals(CinemaFormat.LUXE.getValue()))
					c.setFormatType(Constants.LUXE);

				c.setCostType(assumptionsValues.getCostType());
				c.setInputValues(assumptionsValues.getInputValues());
				c.setComments(assumptionsValues.getComments());
				c.setAdmits(assumptionsValues.getAdmits());
				c.setIsPresent(assumptionsValues.getIsPresent());
				c.setQuery(assumptionsValues.getQuery());
				c.setSuggestedValue(assumptionsValues.getSuggestedValue());
				cinemaValues.add(c);

//				blendedSuggestedValue += (!ObjectUtils.isEmpty(assumptionsValues.getCostType())
//						? assumptionsValues.getCostType()
//						: 0);
//				blendedInputValue += (!ObjectUtils.isEmpty(assumptionsValues.getInputValues())
//						? assumptionsValues.getInputValues()
//						: 0)
//						* (!ObjectUtils.isEmpty(assumptionsValues.getCostType()) ? assumptionsValues.getCostType() : 0);
			}
		}

//		AssumptionsValues blended = new AssumptionsValues();
//		if (sph.size() != 0) {
//			blended.setFormatType("BLENDED SPH");
//			if (blendedSuggestedValue > 0) {
//				blended.setInputValues(Double
//						.parseDouble(new DecimalFormat("#.00").format(blendedInputValue / blendedSuggestedValue)));
//			} else {
//				blended.setInputValues(0.0);
//			}
//			blended.setCostType((long) 0);
//		}
//		cinemaValues.add(blended);

		return cinemaValues;

	}
}

enum CinemaFormat {
	MAINSTREAM("Mainstream"), GOLD("Gold"), IMAX("IMAX"), PXL("PXL"), PLAYHOUSE("Playhouse"),
	ULTRAPREMIUM("Director's Cut"), DX("4DX"), PREMIER("Premier"), ONYX("Onyx"), LUXE("Luxe");

	private String value;

	private CinemaFormat(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
