package com.api.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.dto.Response;
import com.api.services.AdminService;
import com.api.services.LogService;
import com.api.services.helpers.LogHelper;
import com.common.mappers.FhcMapper;
import com.common.models.CinemaCategoryMaster;
import com.common.models.CinemaFormatMaster;
import com.common.models.FixedConstant;
import com.common.models.LogDetail;
import com.common.models.ProjectDetails;
import com.common.models.SeatTypeMaster;

@RestController
@RequestMapping(value = "admin", produces = MediaType.APPLICATION_JSON_VALUE)
public class AdminController {

	@Autowired
	private AdminService adminService;
	
	@Autowired
	private LogService logService;

	@RequestMapping(value = "fhc", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> saveFhc(@RequestBody FhcMapper fhcMapper) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "FHC saved successfully.", adminService.saveFhc(fhcMapper)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "isFhcPresent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> isFhcPresent(@RequestBody FhcMapper fhcMapper) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "FHC fetched successfully.",
				adminService.isFhcPresent(fhcMapper)), HttpStatus.OK);
	}

	@RequestMapping(value = "delete-atp", method = RequestMethod.POST)
	public ResponseEntity<Response<Boolean>> deleteAtp(@RequestBody FixedConstant fixedConstant) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "ATP deleted successfully.",
				adminService.deleteAtp(fixedConstant)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-cinemaFormat-List", method = RequestMethod.GET)
	public ResponseEntity<Response<List<CinemaFormatMaster>>> getCinemaFormatMasterList() {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"Cinema category master list fetched successfully", adminService.getAllCinemaFormat()), HttpStatus.OK);
	}

	@RequestMapping(value = "get-cinemaFormat/{id}", method = RequestMethod.GET)
	public ResponseEntity<Response<CinemaFormatMaster>> getCinemaFormatMaster(@PathVariable("id") String id) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"Cinema category master  fetched successfully", adminService.getCinemaFormat(id)), HttpStatus.OK);
	}

	@RequestMapping(value = "save/cinemaFormat", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> saveCinemaFormat(@RequestBody CinemaFormatMaster cinemaCategoryMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				adminService.saveCinemaFormat(cinemaCategoryMaster)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-cinemaCategory-List", method = RequestMethod.GET)
	public ResponseEntity<Response<List<CinemaCategoryMaster>>> getCinemaCategoryMasterList() {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"Cinema category master list fetched successfully", adminService.getAllCinemaCategory()),
				HttpStatus.OK);
	}

	@RequestMapping(value = "get-cinemaCategory/{id}", method = RequestMethod.GET)
	public ResponseEntity<Response<CinemaCategoryMaster>> getCinemaCategoryMaster(@PathVariable("id") String id) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"Cinema category master  fetched successfully", adminService.getCinemaCategory(id)), HttpStatus.OK);
	}

	@RequestMapping(value = "save/cinemaCategory", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> saveCinemaCategory(
			@RequestBody CinemaCategoryMaster cinemaCategoryMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				adminService.saveCinemaCategory(cinemaCategoryMaster)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-seatType-List", method = RequestMethod.GET)
	public ResponseEntity<Response<List<SeatTypeMaster>>> getSeatTypeList() {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"Cinema category master list fetched successfully", adminService.getAllSeatType()), HttpStatus.OK);
	}

	@RequestMapping(value = "get-seatType/{id}", method = RequestMethod.GET)
	public ResponseEntity<Response<SeatTypeMaster>> getSeatTypeMaster(@PathVariable("id") String id) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"Cinema category master  fetched successfully", adminService.getSeatType(id)), HttpStatus.OK);
	}

	@RequestMapping(value = "save/seatType", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> saveSeatType(@RequestBody SeatTypeMaster seatTypeMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				adminService.saveSeatType(seatTypeMaster)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "cogs", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> saveCogs(@RequestBody FhcMapper fhcMapper) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Cogs saved successfully.", adminService.saveCogs(fhcMapper)),
				HttpStatus.OK);
	}
	
	@RequestMapping(value = "delete-operating-assumption", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> deleteOperatingAssumption(@RequestBody FhcMapper fhcMapper) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Cogs deleted successfully.", adminService.deleteOperatingAssumption(fhcMapper)),
				HttpStatus.OK);
	}
	
	@RequestMapping(value = "gst-on-ticket", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> saveGstOnTicket(@RequestBody FhcMapper fhcMapper) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Gst on ticket saved successfully.", adminService.saveGstOnTicket(fhcMapper)),
				HttpStatus.OK);
	}
	
	@RequestMapping(value = "save-log-details", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> saveLogDetails(@RequestBody LogHelper logHelper) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Log information saved successfully.", logService.saveLog(logHelper)),
				HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "log-detail", method = RequestMethod.GET)
	public ResponseEntity<Response<List<LogDetail>>> getAllLogs() {
		return new ResponseEntity<Response<List<LogDetail>>>(new Response<List<LogDetail>>(HttpStatus.OK.value(),
				"Log fetched successfully", logService.getAllLog()), HttpStatus.OK);
	}

	@RequestMapping(value = { "getOpsReports" }, method = { RequestMethod.GET }, consumes = { "application/json" })
	public ResponseEntity<Response<List<ProjectDetails>>> getOpsReports() {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Reports Fetched successfully.",
				this.adminService.getOpsReportsList()), HttpStatus.OK);
	}

	@RequestMapping(value = { "updateOpsReports" }, method = { RequestMethod.POST }, consumes = { "application/json" })
	public ResponseEntity<Response<List<ProjectDetails>>> updateOpsReports(
			@RequestBody HashMap<String, List<Integer>> idmaps) {
		List<Integer> rptIds = idmaps.get("ids");
		this.adminService.updateOpsReportsList(rptIds);
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Reports Fetched successfully.",
				this.adminService.getOpsReportsList()), HttpStatus.OK);
	}

	@RequestMapping(value = { "getExitReports" }, method = { RequestMethod.GET }, consumes = { "application/json" })
	public ResponseEntity<Response<List<ProjectDetails>>> getExitReports() {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Reports Fetched successfully.",
				this.adminService.getExitReportsList()), HttpStatus.OK);
	}

	@RequestMapping(value = { "updateExtReports" }, method = { RequestMethod.POST }, consumes = { "application/json" })
	public ResponseEntity<Response<List<ProjectDetails>>> updateExtReports(
			@RequestBody HashMap<String, List<Integer>> idmaps) {
		List<Integer> rptIds = idmaps.get("ids");
		this.adminService.updateExtReports(rptIds);
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Reports Fetched successfully.",
				this.adminService.getExitReportsList()), HttpStatus.OK);
	}

	@GetMapping({ "getListForProjName" })
	public ResponseEntity<Response<List<ProjectDetails>>> getReportsForProjName() {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Reports Fetched successfully.",
				this.adminService.getListForProjName()), HttpStatus.OK);
	}

	@PostMapping({ "updateProjName" })
	public ResponseEntity<Response<List<ProjectDetails>>> updateProjName(@RequestBody Map<String, String> projData) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Reports Updated and Fetched successfully.",
				this.adminService.updateProjectName(projData)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "save/screen/type", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> saveScreenType(@RequestBody CinemaFormatMaster cinemaFormatMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				adminService.saveScreenType(cinemaFormatMaster)), HttpStatus.OK);
	}
	
}
