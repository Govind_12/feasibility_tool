package com.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.dto.Filter;
import com.api.dto.Response;
import com.api.services.CityService;
import com.api.utils.QueryParser;
import com.common.models.City;

@RestController
@RequestMapping(value = "city", produces = MediaType.APPLICATION_JSON_VALUE)
public class CityController {

	@Autowired
	private CityService cityService;

	@RequestMapping(value = "get-all-city", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<City>>> getAllProjectDetails(
			@RequestParam(value = "q", required = false) String query) {
		List<Filter> filters = QueryParser.parse(query);

		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"Project Details fetched successfully by projectId.", cityService.fetchAllcities(filters)),
				HttpStatus.OK);
	}

}
