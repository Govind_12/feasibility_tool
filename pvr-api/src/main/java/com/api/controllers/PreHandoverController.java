package com.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.dto.Response;
import com.api.services.PreHandoverService;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.ProjectDetails;

@RestController
@RequestMapping(value = "pre-handover", produces = MediaType.APPLICATION_JSON_VALUE)
public class PreHandoverController {

	@Autowired
	private PreHandoverService preHandoverService;

	@GetMapping("get-all-project")
	public ResponseEntity<Response<List<ProjectDetails>>> getProjectById() {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(),
						"Project Details fetched successfully",
						preHandoverService.getAllExistingProjects()),HttpStatus.OK);

	}

	@PostMapping("development-details")
	public ResponseEntity<Response<ProjectDetails>> saveDevelopmentDetails(@RequestBody ProjectDetails projectDetail) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				preHandoverService.saveDevelopmentDetails(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = "save-query")
	public ResponseEntity<Response<QueryMapper>> saveQuery(@RequestBody MsgMapper msgMapper) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "save query successfully",
				preHandoverService.saveQueryAtOperatingAssumptions(msgMapper)), HttpStatus.OK);
	}

	@RequestMapping(value = "save-query-final")
	public ResponseEntity<Response<QueryMapper>> saveFinalQuery(@RequestBody MsgMapper msgMapper) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "save query successfully",
				preHandoverService.saveQuery(msgMapper)), HttpStatus.OK);
	}

	@PostMapping(value = "send-for-project-or-operation")
	public ResponseEntity<Response<String>> sendForProjectAndOperation(@RequestBody MsgMapper msgMapper) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "project details updated succesfully",
				preHandoverService.updateProjectDetails(msgMapper)), HttpStatus.OK);
	}
	
	@GetMapping("updateNotificationStatus/{projectId}/{queryType}/{step}")
	@ResponseBody
	public ResponseEntity<Response<Boolean>> updateNotificationStatus(@PathVariable("projectId") String projectId,
			@PathVariable("queryType") String queryType,
			@PathVariable("step") String step) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "project details updated succesfully",
				preHandoverService.updateNotificationStatus(projectId, queryType,step)), HttpStatus.OK);
	}
	
	
}
