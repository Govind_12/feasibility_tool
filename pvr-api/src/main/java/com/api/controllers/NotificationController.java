package com.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.dto.Response;
import com.api.services.NotificationService;
import com.common.models.Notification;

@RestController
@RequestMapping("notification")
public class NotificationController {
	
	@Autowired
	private NotificationService notificationService;
	
	@RequestMapping("{type}")
	public ResponseEntity<Response<List<Notification>>> get(@PathVariable String type){
		
		List<Notification> list =notificationService.getNotficationByType(type);
		
		return new ResponseEntity<>(new Response<List<Notification>>(HttpStatus.OK.value(), "Notification fetched successfully", list), HttpStatus.OK);
		
	}
	
	@RequestMapping(value="save", method=RequestMethod.POST)
	public ResponseEntity<Response<Notification>> save(@RequestBody Notification notification){
		
		return new ResponseEntity<>(new Response<Notification>(HttpStatus.OK.value(), "Notification saved successfully", notificationService.save(notification)), HttpStatus.OK);
	}

}
