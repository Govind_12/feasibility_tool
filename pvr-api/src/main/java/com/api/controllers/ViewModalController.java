package com.api.controllers;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.dto.Response;
import com.api.service.popup.ViewModalService;

@RestController
@RequestMapping(value = "view-modal", produces = MediaType.APPLICATION_JSON_VALUE)
public class ViewModalController {

	@Autowired
	private ViewModalService viewModalService;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "get-total-admits/{projectId}")
	public ResponseEntity<Response<Map<String, ? super ArrayList>>> getTotalAdmits(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Get successfully.", viewModalService.getTotalAdmits(projectId)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "get-personal-expenses/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getAtp(@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getPersonalExpense(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-ad-revenue/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getAdRevenue(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Get successfully.", viewModalService.getAdRevenue(projectId)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "get-water-electricity-expenses/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getWaterAndElectricity(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getElectricityExpense(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-repair-maintaince-expenses/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getRepairAndMaintaince(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getRepairAndMaintaince(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-occupancy/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getOccupancy(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Get successfully.", viewModalService.getOccupancy(projectId)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "get-atp/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getAtpDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Get successfully.", viewModalService.getAtpDetails(projectId)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "get-sph/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getSphDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Get successfully.", viewModalService.getSphDetails(projectId)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "get-websale/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getWebsaleDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getWebsaleDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-housekeeping/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getHousekeepingDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getHousekeepingDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-miscellaneous/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getMiscellaneousDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getMiscellaneousDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-travelling/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getTravellingConveyenceDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getTravellingAndConyenceDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-security/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getSecurityDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getSecurityDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-legal/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getLegalFeeDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getLegalfeeDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-communnication/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getCommunicationDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getCommunicationFeeDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-marketing/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getMarketingDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getMarketingDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-internet/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getInternetDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getInternetDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-insurance/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getInsuranceDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getInsuranceDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-printingAndStationary/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getPrintingStationaryDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getPrintingAndStationaryDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-annual-rent/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getAnnualRentDetails(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getAnnualRentDetails(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-calculation/{projectId}")
	public ResponseEntity<Response<Map<String, ? super Object>>> getCalculation(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Get successfully.",
				viewModalService.getViewCalculation(projectId)), HttpStatus.OK);
	}
}
