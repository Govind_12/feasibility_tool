package com.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.dto.Response;
import com.api.services.PostSigningService;
import com.common.models.ProjectDetails;

@RestController
@RequestMapping(value = "post-signing", produces = MediaType.APPLICATION_JSON_VALUE)
public class PostSigningController {

	@Autowired
	private PostSigningService postSigningService;

	@GetMapping("get-all-project")
	public ResponseEntity<Response<List<ProjectDetails>>> getProjectById() {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(),
						"Project Details fetched successfully",
						postSigningService.getAllExistingProjects()),HttpStatus.OK);

	}
	
	@PostMapping("development-details")
	public ResponseEntity<Response<ProjectDetails>> saveDevelopmentDetails(@RequestBody ProjectDetails projectDetail) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				postSigningService.saveDevelopmentDetails(projectDetail)), HttpStatus.OK);
	}

}
