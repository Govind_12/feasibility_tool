package com.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.dto.Filter;
import com.api.dto.Response;
import com.api.services.DataSourceUploadService;
import com.api.utils.QueryParser;
import com.common.models.BbdoMaster;
import com.common.models.CategoryDataSource;
import com.common.models.CinemaMaster;
import com.common.models.CityTier;
import com.common.models.CompitionMaster;
import com.common.models.Navision;
import com.common.models.ProjectCostSummary;
import com.common.sql.models.SeatCategory;

@RestController
@RequestMapping(value = "upload", produces = MediaType.APPLICATION_JSON_VALUE)
public class DataSourceuploadController {

	@Autowired
	private DataSourceUploadService dataSourceUploadService;

	@RequestMapping(value = "/cinema_master", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<CinemaMaster>>> saveCinemaMaster(@RequestBody List<CinemaMaster> cinemaMater) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "cinema master saved successfully.",
				dataSourceUploadService.save(cinemaMater)), HttpStatus.OK);
	}

	@RequestMapping(value = "/compition_master", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<CompitionMaster>>> saveCompition(
			@RequestBody List<CompitionMaster> compitionMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "compition master saved successfully.",
				dataSourceUploadService.saveCompitionMaster(compitionMaster)), HttpStatus.OK);
	}

	@RequestMapping(value = "/bbdo_master", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<BbdoMaster>>> saveBbdo(@RequestBody List<BbdoMaster> bbdoMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "bbdo master saved successfully.",
				dataSourceUploadService.saveBbdoMaster(bbdoMaster)), HttpStatus.OK);
	}

	@RequestMapping(value = "/city_tier", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<CityTier>>> saveCityTier(@RequestBody List<CityTier> cityTierMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "city tier master saved successfully.",
				dataSourceUploadService.saveCityTierMaster(cityTierMaster)), HttpStatus.OK);
	}

	@RequestMapping(value = "/navision-master", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<Navision>>> saveNavisionMaster(@RequestBody List<Navision> navisionMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), " navision master saved successfully.",
				dataSourceUploadService.saveNavisionMaster(navisionMaster)), HttpStatus.OK);
	}

	@RequestMapping(value = "/category-master", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<CategoryDataSource>>> saveCategoryMaster(
			@RequestBody List<CategoryDataSource> categoryMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), " category master saved successfully.",
				dataSourceUploadService.saveCategoryMaster(categoryMaster)), HttpStatus.OK);
	}

	@RequestMapping(value = "/seatCategory-master", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<SeatCategory>>> saveSeatCategoryMaster(
			@RequestBody List<SeatCategory> seatCategoryMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), " seat category master saved successfully.",
				dataSourceUploadService.saveSeatCategoryMaster(seatCategoryMaster)), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/projectCostSummary", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectCostSummary>> saveProjectCostSummary(
			@RequestBody ProjectCostSummary projectCostSummary) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), " Project cost saved successfully.",
				dataSourceUploadService.saveProjectCostSummary(projectCostSummary)), HttpStatus.OK);
	}
	
	
	@RequestMapping(value ="/getAllProjectCostSummary/{projectId}" )
	public ResponseEntity<Response<ProjectCostSummary>> getAllProjectCostSummary(
			@PathVariable("projectId") String projectId){

		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project cost fetch successfully.",
			dataSourceUploadService.fetchAllProjectCost(projectId)), HttpStatus.OK);
	}
	

}

