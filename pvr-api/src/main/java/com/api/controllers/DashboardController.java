package com.api.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.dto.Response;
import com.api.services.DashboardService;
import com.api.services.ProjectDetailsServiceImpl;
import com.common.models.BDData;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("dashboard")
public class DashboardController {
	
	
	@Autowired
	private DashboardService dashboardService;
	
	
	@RequestMapping(value = "")
	public ResponseEntity<Response<Map<String,Object>>> getDashboardDetails() {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Dashboard details fetched successfully.",
				dashboardService.getDashboardDetails()), HttpStatus.OK);
	}

	@RequestMapping({ "getreport/{userid}" })
	public ResponseEntity<Response<Map<String, Object>>> getProjectDetails(@PathVariable String userid) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Dashboard details fetched successfully.",
				dashboardService.getDashboardDetails2(userid)), HttpStatus.OK);
	}

	@PostMapping({ "setreport" })
	ResponseEntity<Response<Map<String, Object>>> setProjectDetails(@RequestBody BDData bdata) {
		log.info("inside set report method api");
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Dashboard details fetched successfully.", dashboardService
						.setreportdata(bdata)),
				HttpStatus.OK);
	}

}
