package com.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.dto.Response;
import com.api.service.report.ReportService;
import com.common.models.ProjectDetails;


@RestController
@RequestMapping(value = "report-download", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReportController {

	@Autowired
	private ReportService reportService;
	
	@RequestMapping(value = "generate/{projectId}")
	public ResponseEntity<Response<ProjectDetails>> downloadPdf(@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "PDF created successfully.",
				reportService.HtmlToPdfConversion(projectId,null)), HttpStatus.OK);
	}
}
