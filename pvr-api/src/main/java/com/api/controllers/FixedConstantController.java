package com.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.common.mappers.ServiceChargeMapper;
import com.api.dto.Response;
import com.api.services.FixedConstantService;
import com.common.models.FixedConstant;

@RestController
@RequestMapping("fixedConstants")
public class FixedConstantController {
	
	@Autowired
	private FixedConstantService fixedConstantService;
	
	@RequestMapping("")
	public ResponseEntity<Response<FixedConstant>> getFixedConstatnts(){
		
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Fixed Constants fetched Successfully", fixedConstantService.getFixedConstant()), HttpStatus.OK);
		
	}
	
	@RequestMapping(value="serviceCharge", method=RequestMethod.POST)
	public ResponseEntity<Response<FixedConstant>> saveServiceChargePerTicket(@RequestBody ServiceChargeMapper serviceCharge){
	return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "ServiceChargePerTicket saved Successfully", fixedConstantService.saveServiceChargePerTicket(serviceCharge)), HttpStatus.OK);
		
	}
	
	@RequestMapping(value="capexAssumption", method=RequestMethod.POST)
	public ResponseEntity<Response<FixedConstant>> saveCapexAssumptions(@RequestBody FixedConstant fixedConstant){
	return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Capex_Assumption saved Successfully", fixedConstantService.saveCapexAssumption(fixedConstant)), HttpStatus.OK);
		
	}
	
	@RequestMapping(value="taxAssumption", method=RequestMethod.POST)
	public ResponseEntity<Response<FixedConstant>> saveTaxAssumptions(@RequestBody FixedConstant fixedConstant){
	return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Tax_Assumption saved Successfully", fixedConstantService.saveTaxAssumption(fixedConstant)), HttpStatus.OK);
		
	}
	
	@RequestMapping(value="operatAssumption", method=RequestMethod.POST)
	public ResponseEntity<Response<FixedConstant>> saveOperatingAssumptions(@RequestBody FixedConstant fixedConstant){
	return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Operating_Assumption saved Successfully", fixedConstantService.saveOperatingAssumption(fixedConstant)), HttpStatus.OK);
		
	}
	@RequestMapping(value="saveGrowthAssumptions", method=RequestMethod.POST)
	public ResponseEntity<Response<FixedConstant>> saveGrowthAssumptions(@RequestBody FixedConstant fixedConstant){
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"Growth Assumptions saved Successfully", fixedConstantService.saveGrowthAssumptions(fixedConstant)), HttpStatus.OK);
	}
	
	@RequestMapping(value="atpCap", method=RequestMethod.POST)
	public ResponseEntity<Response<FixedConstant>> saveAtpCaps(@RequestBody FixedConstant fixedConstant){
		
		System.out.println("rest: "+fixedConstant);
		FixedConstant savedFixedConstant=fixedConstantService.saveAtpsCap(fixedConstant);
		
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"ATP Cap saved Successfully",savedFixedConstant ), HttpStatus.OK);
	}
	
	@RequestMapping(value="irr-constant-details", method=RequestMethod.POST)
	public ResponseEntity<Response<FixedConstant>> irrConstantDetails(@RequestBody FixedConstant fixedConstant){
	return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Fixed Constant saved Successfully", fixedConstantService.irrConstantDetails(fixedConstant)), HttpStatus.OK);
		
	}
	
}


