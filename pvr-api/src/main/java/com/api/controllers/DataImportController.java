package com.api.controllers;

import com.api.sqlentity.CinemaAudiSeatMaster;
import com.api.sqlentity.CinemaMasterData;
import com.api.sqlentity.TblCinemaPerformance;
import com.api.sqlentity.TempTableSeatCategory;
import com.api.sqlentity.TicketOnlineOffline;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping({"/data"})
public class DataImportController {
  Map<String, JdbcTemplate> map = new HashMap<>();
  
  @GetMapping({"/connected"})
  public String getConnection(@RequestParam("from") String from, @RequestParam("to") String to) {
    if (from != null && to != null) {
      String[] fromarr = from.split(",");
      String[] toarr = to.split(",");
      DriverManagerDataSource fromdatasource = new DriverManagerDataSource();
      fromdatasource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      fromdatasource.setUrl("jdbc:sqlserver://" + fromarr[0] + ";databaseName=" + fromarr[1]);
      fromdatasource.setUsername(fromarr[2]);
      fromdatasource.setPassword(fromarr[3]);
      JdbcTemplate fromtemplate = new JdbcTemplate((DataSource)fromdatasource);
      this.map.put("from", fromtemplate);
      DriverManagerDataSource todatasource = new DriverManagerDataSource();
      todatasource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      todatasource.setUrl("jdbc:sqlserver://" + toarr[0] + ";databaseName=" + toarr[1]);
      todatasource.setUsername(toarr[2]);
      todatasource.setPassword(toarr[3]);
      JdbcTemplate totemplate = new JdbcTemplate((DataSource)todatasource);
      this.map.put("to", totemplate);
      return "Success";
    } 
    return "Failure";
  }
  
  @GetMapping({"/startimport"})
  public String startImport(@RequestParam("year") String year, @RequestParam("qtr") String qtr) {
    try {
      cinemaAudiMaster(this.map.get("from"), this.map.get("to"));
    } catch (Exception e) {
      e.printStackTrace();
    } 
    try {
      cinemaMasterData(this.map.get("from"), this.map.get("to"));
    } catch (Exception e) {
      e.printStackTrace();
    } 
    try {
      tblCinemaPerformance(this.map.get("from"), this.map.get("to"), year, qtr);
    } catch (Exception e) {
      e.printStackTrace();
    } 
    try {
      offlineOnline(this.map.get("from"), this.map.get("to"), year, qtr);
    } catch (Exception e) {
      e.printStackTrace();
    } 
    try {
      tempTableSeatCategory(this.map.get("from"), this.map.get("to"), year, qtr);
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return "Success";
  }
  
  private void tblCinemaPerformance(JdbcTemplate fromtemplate, JdbcTemplate totemplate, String year, String qtr) throws SQLException {
    List<TblCinemaPerformance> audi = fromtemplate.query("select * from tbl_Cinema_performance where finyear='" + year + "' and QTR='" + qtr + "'", (RowMapper)new Object());
    DataSource ab = totemplate.getDataSource();
    Connection connection = ab.getConnection();
    connection.setAutoCommit(false);
    String sql = "insert into tbl_cinema_performance(Admits,BO_No_of_Trans,Business_Date,Cinema_City,Cinema_Name,Cinema_State,Cinema_hopk,Conc_No_of_Trans,Concession_Net_Revenue,GBOC,QTR,Total_Seats_Available,finyear) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    PreparedStatement ps = connection.prepareStatement(sql);
    int batchSize = 1000;
    int count = 0;
    for (int i = 0; i < audi.size(); i++) {
      ps.setInt(1, ((TblCinemaPerformance)audi.get(i)).getAdmits().intValue());
      ps.setInt(2, ((TblCinemaPerformance)audi.get(i)).getBO_No_of_Trans().intValue());
      ps.setDate(3, (Date)((TblCinemaPerformance)audi.get(i)).getBusiness_Date());
      ps.setString(4, ((TblCinemaPerformance)audi.get(i)).getCinema_City());
      ps.setString(5, ((TblCinemaPerformance)audi.get(i)).getCinema_Name());
      ps.setString(6, ((TblCinemaPerformance)audi.get(i)).getCinema_State());
      ps.setInt(7, ((TblCinemaPerformance)audi.get(i)).getCinema_hopk().intValue());
      ps.setInt(8, ((TblCinemaPerformance)audi.get(i)).getConc_No_of_Trans().intValue());
      ps.setString(9, ((TblCinemaPerformance)audi.get(i)).getConcession_Net_Revenue());
      ps.setString(10, ((TblCinemaPerformance)audi.get(i)).getGBOC());
      ps.setString(11, ((TblCinemaPerformance)audi.get(i)).getQTR());
      ps.setInt(12, ((TblCinemaPerformance)audi.get(i)).getTotal_Seats_Available().intValue());
      ps.setString(13, ((TblCinemaPerformance)audi.get(i)).getFinyear());
      ps.addBatch();
      count++;
      if (count % 1000 == 0 || count == audi.size()) {
        System.out.println("TblCinemaPerformance batch==" + count);
        ps.executeBatch();
        ps.clearBatch();
      } 
    } 
    connection.commit();
    ps.close();
    System.out.println("TblCinemaPerformance import data ");
  }
  
  private void tempTableSeatCategory(JdbcTemplate fromtemplate, JdbcTemplate totemplate, String year, String qtr) throws SQLException {
    List<TempTableSeatCategory> audi = fromtemplate.query("select * from temp_table_seat_Category where finyear='" + year + "' and QTR='" + qtr + "'", (RowMapper)new Object());
    DataSource ab = totemplate.getDataSource();
    Connection connection = ab.getConnection();
    connection.setAutoCommit(false);
    String sql = "insert into temp_table_seat_category(Area_category,Audi_Number,CinOperator_strCode,Cinema_City,Cinema_Format,Cinema_Name,Cinema_state,Film_strCode,GBOC,Movie_Name,Movie_show_Time,QTR,Session_lngSessionId,Total_Seats,admits,area_cat_Code,cinema_hopk,finyear) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    PreparedStatement ps = connection.prepareStatement(sql);
    int batchSize = 1000;
    int count = 0;
    for (int i = 0; i < audi.size(); i++) {
      ps.setString(1, ((TempTableSeatCategory)audi.get(i)).getArea_category());
      ps.setInt(2, ((TempTableSeatCategory)audi.get(i)).getAudi_Number().intValue());
      ps.setString(3, ((TempTableSeatCategory)audi.get(i)).getCinOperator_strCode());
      ps.setString(4, ((TempTableSeatCategory)audi.get(i)).getCinema_City());
      ps.setString(5, ((TempTableSeatCategory)audi.get(i)).getCinema_Format());
      ps.setString(6, ((TempTableSeatCategory)audi.get(i)).getCinema_Name());
      ps.setString(7, ((TempTableSeatCategory)audi.get(i)).getCinema_state());
      ps.setString(8, ((TempTableSeatCategory)audi.get(i)).getFilm_strCode());
      ps.setString(9, ((TempTableSeatCategory)audi.get(i)).getGBOC());
      ps.setString(10, ((TempTableSeatCategory)audi.get(i)).getMovie_Name());
      ps.setDate(11, (Date)((TempTableSeatCategory)audi.get(i)).getMovie_show_Time());
      ps.setString(12, ((TempTableSeatCategory)audi.get(i)).getQTR());
      ps.setInt(13, ((TempTableSeatCategory)audi.get(i)).getSession_lngSessionId().intValue());
      ps.setInt(14, ((TempTableSeatCategory)audi.get(i)).getTotal_Seats().intValue());
      ps.setString(15, ((TempTableSeatCategory)audi.get(i)).getAdmits());
      ps.setString(16, ((TempTableSeatCategory)audi.get(i)).getArea_cat_Code());
      ps.setInt(17, ((TempTableSeatCategory)audi.get(i)).getCinema_hopk().intValue());
      ps.setString(18, ((TempTableSeatCategory)audi.get(i)).getFinyear());
      ps.addBatch();
      count++;
      if (count % 1000 == 0 || count == audi.size()) {
        System.out.println("TempTableSeatCategory batch==" + count);
        ps.executeBatch();
        ps.clearBatch();
      } 
    } 
    connection.commit();
    ps.close();
    System.out.println("TempTableSeatCategory import data ");
  }
  
  private void cinemaMasterData(JdbcTemplate fromtemplate, JdbcTemplate totemplate) throws SQLException {
    List<CinemaMasterData> audi = fromtemplate.query("select * from Cinema_master_Data_v1", (RowMapper)new Object());
    DataSource ab = totemplate.getDataSource();
    Connection connection = ab.getConnection();
    connection.setAutoCommit(false);
    String sql = "insert into cinema_master_data_v1(Cinema_Location,Cinema_Name,Cinema_strRun,DX,Gold,IMAX,Mainstream,Nav_cinemaCode,Nav_cinemadesc,No_of_Audi,ONYX,OWNER_strCompany,PXL,Playhouse,Premier,State_strName,UltraPremium,Vista_info_Cinema_Name,latitude,longitude,loyalty_complex_id,HOPK,Cinema_strCode) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    PreparedStatement ps = connection.prepareStatement(sql);
    int batchSize = 1000;
    int count = 0;
    for (int i = 0; i < audi.size(); i++) {
      if (((CinemaMasterData)audi.get(i)).getCinema_Location() != null)
        ps.setString(1, ((CinemaMasterData)audi.get(i)).getCinema_Location()); 
      if (((CinemaMasterData)audi.get(i)).getCinema_Name() != null)
        ps.setString(2, ((CinemaMasterData)audi.get(i)).getCinema_Name()); 
      if (((CinemaMasterData)audi.get(i)).getCinema_Name() != null)
        ps.setString(3, ((CinemaMasterData)audi.get(i)).getCinema_Name()); 
      if (((CinemaMasterData)audi.get(i)).getDX() != null)
        ps.setLong(4, ((CinemaMasterData)audi.get(i)).getDX().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getGold() != null)
        ps.setLong(5, ((CinemaMasterData)audi.get(i)).getGold().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getIMAX() != null)
        ps.setLong(6, ((CinemaMasterData)audi.get(i)).getIMAX().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getMainstream() != null)
        ps.setInt(7, ((CinemaMasterData)audi.get(i)).getMainstream().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getNav_cinemaCode() != null)
        ps.setString(8, ((CinemaMasterData)audi.get(i)).getNav_cinemaCode()); 
      if (((CinemaMasterData)audi.get(i)).getNav_cinemadesc() != null)
        ps.setString(9, ((CinemaMasterData)audi.get(i)).getNav_cinemadesc()); 
      if (((CinemaMasterData)audi.get(i)).getNo_of_Audi() != null)
        ps.setInt(10, ((CinemaMasterData)audi.get(i)).getNo_of_Audi().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getONYX() != null)
        ps.setLong(11, ((CinemaMasterData)audi.get(i)).getONYX().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getOWNER_strCompany() != null)
        ps.setString(12, ((CinemaMasterData)audi.get(i)).getOWNER_strCompany()); 
      if (((CinemaMasterData)audi.get(i)).getPXL() != null)
        ps.setInt(13, ((CinemaMasterData)audi.get(i)).getPXL().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getPlayhouse() != null)
        ps.setInt(14, ((CinemaMasterData)audi.get(i)).getPlayhouse().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getPremier() != null)
        ps.setInt(15, ((CinemaMasterData)audi.get(i)).getPremier().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getState_strName() != null)
        ps.setString(16, ((CinemaMasterData)audi.get(i)).getState_strName()); 
      if (((CinemaMasterData)audi.get(i)).getUltraPremium() != null)
        ps.setInt(17, ((CinemaMasterData)audi.get(i)).getUltraPremium().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getVista_info_Cinema_Name() != null)
        ps.setString(18, ((CinemaMasterData)audi.get(i)).getVista_info_Cinema_Name()); 
      if (((CinemaMasterData)audi.get(i)).getLatitude() != null)
        ps.setString(19, ((CinemaMasterData)audi.get(i)).getLatitude()); 
      if (((CinemaMasterData)audi.get(i)).getLongitude() != null)
        ps.setString(20, ((CinemaMasterData)audi.get(i)).getLongitude()); 
      if (((CinemaMasterData)audi.get(i)).getLoyalty_complex_id() != null)
        ps.setInt(21, ((CinemaMasterData)audi.get(i)).getLoyalty_complex_id().intValue()); 
      if (((CinemaMasterData)audi.get(i)).getHOPK() != null)
        ps.setString(22, ((CinemaMasterData)audi.get(i)).getHOPK()); 
      if (((CinemaMasterData)audi.get(i)).getCinema_strCode() != null)
        ps.setString(23, ((CinemaMasterData)audi.get(i)).getCinema_strCode()); 
      ps.addBatch();
      count++;
      if (count % 1000 == 0 || count == audi.size()) {
        System.out.println("CinemaMasterData batch==" + count);
        ps.executeBatch();
        ps.clearBatch();
      } 
    } 
    connection.commit();
    ps.close();
    System.out.println("CinemaMasterData import data ");
  }
  
  private void cinemaAudiMaster(JdbcTemplate fromtemplate, JdbcTemplate totemplate) throws SQLException {
    List<CinemaAudiSeatMaster> audi = fromtemplate.query("select * from cinema_audi_seat_master_v1", (RowMapper)new Object());
    DataSource ab = totemplate.getDataSource();
    Connection connection = ab.getConnection();
    connection.setAutoCommit(false);
    String sql = "insert into cinema_audi_seat_master_v1(Screen_bytNum,Screen_strName,Screen_intSeats,Area_strDescription,Area_strShortDesc,Area_intNumSeatsTot,Cinema_HOPK,AreaCat_strCode,AreaCat_strDesc,Area_cat_status) values (?,?,?,?,?,?,?,?,?,?)";
    PreparedStatement ps = connection.prepareStatement(sql);
    int batchSize = 1000;
    int count = 0;
    for (int i = 0; i < audi.size(); i++) {
      if (((CinemaAudiSeatMaster)audi.get(i)).getScreen_bytNum() != null)
        ps.setString(1, ((CinemaAudiSeatMaster)audi.get(i)).getScreen_bytNum()); 
      if (((CinemaAudiSeatMaster)audi.get(i)).getScreen_strName() != null)
        ps.setString(2, ((CinemaAudiSeatMaster)audi.get(i)).getScreen_strName()); 
      if (((CinemaAudiSeatMaster)audi.get(i)).getScreen_intSeats() != null)
        ps.setString(3, ((CinemaAudiSeatMaster)audi.get(i)).getScreen_intSeats()); 
      if (((CinemaAudiSeatMaster)audi.get(i)).getArea_strDescription() != null)
        ps.setString(4, ((CinemaAudiSeatMaster)audi.get(i)).getArea_strDescription()); 
      if (((CinemaAudiSeatMaster)audi.get(i)).getArea_strShortDesc() != null)
        ps.setString(5, ((CinemaAudiSeatMaster)audi.get(i)).getArea_strShortDesc()); 
      if (((CinemaAudiSeatMaster)audi.get(i)).getArea_intNumSeatsTot() != null)
        ps.setString(6, ((CinemaAudiSeatMaster)audi.get(i)).getArea_intNumSeatsTot()); 
      if (((CinemaAudiSeatMaster)audi.get(i)).getCinema_HOPK() != null)
        ps.setString(7, ((CinemaAudiSeatMaster)audi.get(i)).getCinema_HOPK()); 
      if (((CinemaAudiSeatMaster)audi.get(i)).getAreaCat_strCode() != null)
        ps.setString(8, ((CinemaAudiSeatMaster)audi.get(i)).getAreaCat_strCode()); 
      if (((CinemaAudiSeatMaster)audi.get(i)).getArea_strDescription() != null)
        ps.setString(9, ((CinemaAudiSeatMaster)audi.get(i)).getArea_strDescription()); 
      if (((CinemaAudiSeatMaster)audi.get(i)).getArea_cat_status() != null)
        ps.setString(10, ((CinemaAudiSeatMaster)audi.get(i)).getArea_cat_status()); 
      ps.addBatch();
      count++;
      if (count % 1000 == 0 || count == audi.size()) {
        System.out.println("CinemaAudiSeatMaster batch==" + count);
        ps.executeBatch();
        ps.clearBatch();
      } 
    } 
    connection.commit();
    ps.close();
    System.out.println("CinemaAudiSeatMaster import data ");
  }
  
  public static void offlineOnline(JdbcTemplate fromtemplate, JdbcTemplate totemplate, String year, String qtr) throws SQLException {
    List<TicketOnlineOffline> audi = fromtemplate.query("Select  * from Ticket_online_Offline where finyear='" + year + "' and QTR='" + qtr + "' ", (RowMapper)new Object());
    DataSource ab = totemplate.getDataSource();
    Connection connection = ab.getConnection();
    connection.setAutoCommit(false);
    String sql = "insert into ticket_online_offline(Admits,Audi_number,Channel_Group,QTR,Session_Business_Date,Session_no,cinema_hopk,finyear) values (?,?,?,?,?,?,?,?)";
    PreparedStatement ps = connection.prepareStatement(sql);
    int batchSize = 1000;
    int count = 0;
    for (int i = 0; i < audi.size(); i++) {
      System.out.println(i);
      ps.setInt(1, ((TicketOnlineOffline)audi.get(i)).getAdmits().intValue());
      ps.setBoolean(2, ((TicketOnlineOffline)audi.get(i)).getAudi_number().booleanValue());
      ps.setString(3, ((TicketOnlineOffline)audi.get(i)).getChannel_Group());
      ps.setString(4, ((TicketOnlineOffline)audi.get(i)).getQTR());
      ps.setDate(5, (Date)((TicketOnlineOffline)audi.get(i)).getSession_Business_Date());
      ps.setInt(6, ((TicketOnlineOffline)audi.get(i)).getSession_no().intValue());
      ps.setInt(7, ((TicketOnlineOffline)audi.get(i)).getCinema_hopk().intValue());
      ps.setString(8, ((TicketOnlineOffline)audi.get(i)).getFinyear());
      ps.addBatch();
      count++;
      if (count % 1000 == 0 || count == audi.size()) {
        System.out.println("Offline online batch==" + count);
        ps.executeBatch();
        ps.clearBatch();
      } 
    } 
    connection.commit();
    ps.close();
    System.out.println("Offline online import data ");
  }
}
