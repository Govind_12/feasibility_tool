package com.api.controllers;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.dao.FixedConstantDao;
import com.api.dao.IrrCalculationDao;
import com.api.dao.LogDetailDao;
import com.api.dto.Filter;
import com.api.dto.Response;
import com.api.service.report.MisService;
import com.api.services.ProjectDetailsService;
import com.api.utils.ChangeCinemaFormat;
import com.api.utils.QueryParser;
import com.api.utils.RoleUtil;
import com.common.mappers.MisMapper;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.CinemaMaster;
import com.common.models.FeasibilityUser;
import com.common.models.FinalSummary;
import com.common.models.FixedConstant;
import com.common.models.IrrCalculationData;
import com.common.models.LesserLesse;
import com.common.models.LogDetail;
import com.common.models.ProjectDetails;

@RestController
@RequestMapping(value = "projectDetails", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectDetailsController {

	@Autowired
	private ProjectDetailsService projectDetailsService;

	@Autowired
	private MisService misService;

	@Autowired
	private LogDetailDao logDao;
	
	@Autowired
	private IrrCalculationDao irrCalculationDao;
	
	@Autowired
	private FixedConstantDao fixedConstDao;

	@RequestMapping(value = "step1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> saveStep1(@RequestBody ProjectDetails projectDetail) {
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				projectDetailsService.step1(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = "step2", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> saveStep2(@RequestBody ProjectDetails projectDetail) {
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				projectDetailsService.step2(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = "step3", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> saveStep3(@RequestBody ProjectDetails projectDetail) {
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				projectDetailsService.step3(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = "step4", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> saveStep4(@RequestBody ProjectDetails projectDetail) {
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				projectDetailsService.step4(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = "step5", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> saveStep5(@RequestBody ProjectDetails projectDetail) {
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				projectDetailsService.step5(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = "step6", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> saveStep6(@RequestBody ProjectDetails projectDetail) {
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				projectDetailsService.step6(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = "step7", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> saveStep7(@RequestBody ProjectDetails projectDetail) {
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				projectDetailsService.step7(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-all-project-detail", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<ProjectDetails>>> getAllProjectDetails(
			@RequestParam(value = "q", required = false) String query, Pageable pageable) {
		List<Filter> filters = QueryParser.parse(query);
		List<ProjectDetails> allProjectDetails = projectDetailsService.fetchAllProjectDetails(pageable, filters);
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project details fetch successfully.",
				allProjectDetails, allProjectDetails.size()), HttpStatus.OK);
	}

	@RequestMapping(value = "getProjectById/{projectId}")
	public ResponseEntity<Response<ProjectDetails>> getProjectById(@PathVariable("projectId") String projectId) {
		ProjectDetails projectDetails = projectDetailsService.get(projectId);
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetails.getProjectName())
				? (projectDetails.getReportId() + "-" + projectDetails.getProjectName())
				: projectDetails.getReportId());
		projectDetails.setIsProjectRole(RoleUtil.isCurrentUserHasProjectTeamRole());
		if (!ObjectUtils.isEmpty(projectDetails.getOccupancy())) {
			projectDetails.setOccupancy(ChangeCinemaFormat.changeOccupancyFormat(projectDetails.getOccupancy()));
			projectDetails.setSph(ChangeCinemaFormat.changeSphFormat(projectDetails.getSph(), projectDetails));
			projectDetails.setAtp(ChangeCinemaFormat.changeAtpFormat(projectDetails.getAtp(), projectDetails));
		}
		if(!ObjectUtils.isEmpty(projectDetails.getLesserLesse())){
			if(ObjectUtils.isEmpty(projectDetails.getLesserLesse().getRentSecurityDepositForFixedZeroMg())){
				projectDetails.getLesserLesse().setRentSecurityDepositForFixedZeroMg(0.0);
			}
			else {
				Double security = projectDetails.getLesserLesse().getRentSecurityDepositForFixedZeroMg();
				projectDetails.getLesserLesse().setRentSecurityDepositForFixedZeroMg(security);
				
			}
			if(ObjectUtils.isEmpty(projectDetails.getLesserLesse().getAdvanceRent())){
				projectDetails.getLesserLesse().setAdvanceRent(0.0);
			}
			else {
				Double advanceRent = projectDetails.getLesserLesse().getAdvanceRent();
				projectDetails.getLesserLesse().setAdvanceRent(advanceRent);
			}
			if(ObjectUtils.isEmpty(projectDetails.getLesserLesse().getAdvanceRentPeriod())){
				projectDetails.getLesserLesse().setAdvanceRentPeriod(0);
			}
			else {
				Integer advanceRentPeriod = projectDetails.getLesserLesse().getAdvanceRentPeriod();
				projectDetails.getLesserLesse().setAdvanceRentPeriod(advanceRentPeriod);
			}
		}
		else {
			projectDetails.setLesserLesse(new LesserLesse());
			projectDetails.getLesserLesse().setRentSecurityDepositForFixedZeroMg(0.0);
			projectDetails.getLesserLesse().setAdvanceRent(0.0);
		}
		projectDetails.setProjectCost(!ObjectUtils.isEmpty(projectDetails.getProjectCost()) ? projectDetails.getProjectCost() : 0.0);
		
		projectDetails.setProjectCostPerScreen(!ObjectUtils.isEmpty(projectDetails.getProjectCostPerScreen())
				? projectDetails.getProjectCostPerScreen()
				: 0.0);
		projectDetails.setNoOfScreens(
				!ObjectUtils.isEmpty(projectDetails.getNoOfScreens()) ? projectDetails.getNoOfScreens() : 1);

		FeasibilityUser user = RoleUtil.getCurrentUseInfo();
		projectDetails.setUserDesignation(user.getDesignation());
		
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"Project Details fetched successfully by projectId.", projectDetails), HttpStatus.OK);

	}

	@RequestMapping(value = "approve-project/{projectId}")
	public ResponseEntity<Response<ProjectDetails>> approveProject(@PathVariable("projectId") String projectId) {
		ProjectDetails projectDetails = projectDetailsService.get(projectId);
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetails.getProjectName())
				? (projectDetails.getReportId() + "-" + projectDetails.getProjectName())
				: projectDetails.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Approved successfully.",
				projectDetailsService.approveProject(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "reject-project/{projectId}")
	public ResponseEntity<Response<ProjectDetails>> rejectProject(@PathVariable("projectId") String projectId) {
		ProjectDetails projectDetails = projectDetailsService.get(projectId);
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetails.getProjectName())
				? (projectDetails.getReportId() + "-" + projectDetails.getProjectName())
				: projectDetails.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Reject successfully.",
				projectDetailsService.rejectProject(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "getProjectDetailsById/{id}")
	public ResponseEntity<Response<ProjectDetails>> getProjectDetailsById(@PathVariable("id") String id) {
		ProjectDetails projectDetails = projectDetailsService.get(id);
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetails.getProjectName())
				? (projectDetails.getReportId() + "-" + projectDetails.getProjectName())
				: projectDetails.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
				"Project Details fetched successfully by projectId.", projectDetailsService.getProjectDetailsById(id)),
				HttpStatus.OK);

	}

	@RequestMapping(value = "get-query-by-project-id/{projectId}")
	public ResponseEntity<Response<FinalSummary>> getQueryByProjectId(@PathVariable("projectId") String projectId) {
		ProjectDetails projectDetails = projectDetailsService.get(projectId);
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetails.getProjectName())
				? (projectDetails.getReportId() + "-" + projectDetails.getProjectName())
				: projectDetails.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Query by project id.",
				projectDetailsService.getQueryByProjectId(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "save-query")
	public ResponseEntity<Response<QueryMapper>> saveQuery(@RequestBody MsgMapper msgMapper) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "save query successfully",
				projectDetailsService.saveQuery(msgMapper)), HttpStatus.OK);
	}

	@RequestMapping(value = "updateProjectAndNotificationStatus/{projectId}/{status}")
	public ResponseEntity<Response<Boolean>> updateProjectAndNotificationStatus(
			@PathVariable("projectId") String projectId, @PathVariable("status") int status) {
		ProjectDetails projectDetail = projectDetailsService.get(projectId);
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());

		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Project and Notification Updated successfully",
						projectDetailsService.updateProjectAndNotificationStatus(projectId, status)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "getLatestProject/{reportType}")
	public ResponseEntity<Response<ProjectDetails>> getLatestProject(@PathVariable("reportType") String reportType,
			@RequestParam(value = "reportId", required = false) String reportId) {

		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Latest Project details fetched successfully",
				projectDetailsService.getLatestProject(reportType, reportId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-pre-signing-project-by-status", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<ProjectDetails>>> getPreSigningProjectsByStatus(
			@RequestParam(value = "q", required = false) String query, Pageable pageable) {
		List<Filter> filters = QueryParser.parse(query);
		List<ProjectDetails> allProjectDetails = projectDetailsService.fetchPreSigningProjectByStatus(pageable,
				filters);
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project details fetch successfully.",
				allProjectDetails, allProjectDetails.size()), HttpStatus.OK);
	}

	@RequestMapping(value = "deleteProjectById/{projectId}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> updateProjectDetailsId(
			@PathVariable("projectId") String projectId) {
		ProjectDetails projectDetail = projectDetailsService.get(projectId);
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Project Details updated successfully by projectId.",
						projectDetailsService.updateProjectDetailsById(projectId)),
				HttpStatus.OK);

	}

	@RequestMapping(value = "access-all-reports", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<ProjectDetails>>> accessAllReports(
			@RequestParam(value = "q", required = false) String query, Pageable pageable) {
		List<Filter> filters = QueryParser.parse(query);
		List<ProjectDetails> allProjectDetails = projectDetailsService.fetchAcessOldReports(pageable, filters);

		Collections.sort(allProjectDetails, Comparator.comparingLong(ProjectDetails::getCreatedDate).reversed());

		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "old reports fetch successfully.",
				allProjectDetails, allProjectDetails.size()), HttpStatus.OK);
	}

	@RequestMapping(value = "operating-assumtion-for-all", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> submitPeratingAssumption(
			@RequestBody ProjectDetails projectDetail) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail submit successfully.",
				projectDetailsService.submitOperatingAssumption(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = "reject", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> rejectProjectDetails(@RequestBody MsgMapper msgMapper) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				projectDetailsService.rejectProjectDetails(msgMapper)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-all-cinema-master-details", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<CinemaMaster>>> getAllCinemaMasterDetails() {
		List<CinemaMaster> allCinemaMasterDetails = projectDetailsService.getAllCinemaMasterDetails();
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "old reports fetch successfully.",
				allCinemaMasterDetails, allCinemaMasterDetails.size()), HttpStatus.OK);
	}

	@RequestMapping(value = "update-project/{projectId}")
	public ResponseEntity<Response<ProjectDetails>> updateProject(@PathVariable("projectId") String projectId) {
		ProjectDetails projectDetail = projectDetailsService.get(projectId);
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project status updated successfully.",
				projectDetailsService.updateProject(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "get-existing-project", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<ProjectDetails>>> getExistingProject() {
		List<ProjectDetails> allProjectDetails = projectDetailsService.getExistingProject();
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project details fetch successfully.",
				allProjectDetails, allProjectDetails.size()), HttpStatus.OK);
	}

	@RequestMapping(value = "save-close-step5", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> saveCloseStep5(@RequestBody ProjectDetails projectDetail) {
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				projectDetailsService.saveCloseStep5(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = "fetch-all-project-detail")
	// , method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<MisMapper>>> getAllProjectDetails() {
		List<MisMapper> allProjectDetails = misService.getMisData();
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project details fetch successfully.",
				allProjectDetails, allProjectDetails.size()), HttpStatus.OK);
	}

	@RequestMapping(value = "operatingAssumptions/getRevenueAssumptionConstant/{projectId}", method = RequestMethod.GET)
	public ResponseEntity<Response<FixedConstant>> getRevenueAssumptionConstant(
			@PathVariable("projectId") String projectId) {
		ProjectDetails projectDetail = projectDetailsService.get(projectId);
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Fixed Constants fetched Successfully",
				projectDetailsService.getRevenueAssumptionConstant(projectId, projectDetail.getProjectLocation().getState())), HttpStatus.OK);
	}

	public void updateFeasibilityAccessed(String projectId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		FeasibilityUser hitechUser = (FeasibilityUser) auth.getPrincipal();
		List<LogDetail> logList = logDao.findAllByUsernameOrderByCreatedDateDesc(hitechUser.getUsername());
		if (!logList.isEmpty()) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
			LogDetail log = logList.get(0);
			String feasibility = log.getFeasibilityAccessed() == null ? "" : log.getFeasibilityAccessed();
			if (!feasibility.contains(projectId)) {
				feasibility += '\n' + projectId;
			}
			log.setFeasibilityAccessed(feasibility);
			logDao.save(log);
		}
	}
	
	@RequestMapping(value = "save-executive-note", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<ProjectDetails>> saveExecutiveNote(@RequestBody ProjectDetails projectDetail) {
		updateFeasibilityAccessed(StringUtils.isNotEmpty(projectDetail.getProjectName())
				? (projectDetail.getReportId() + "-" + projectDetail.getProjectName())
				: projectDetail.getReportId());
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project Detail saved successfully.",
				projectDetailsService.saveExecutiveNote(projectDetail)), HttpStatus.OK);
	}

	@RequestMapping(value = { "get-all-existing-project-detail-for-copy" }, method = { RequestMethod.GET }, consumes = {
			"application/json" })
	public ResponseEntity<Response<List<ProjectDetails>>> getAllExistingProjectDetailsForCopy(
			@RequestParam(value = "q", required = false) String query, Pageable pageable) {
		List<Filter> filters = QueryParser.parse(query);
		List<ProjectDetails> allProjectDetails = projectDetailsService
				.fetchAllExistingProjectDetailsForCopy(pageable, filters);
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project details fetch successfully.",
				allProjectDetails, Integer.valueOf(allProjectDetails.size())), HttpStatus.OK);
	}

	@RequestMapping({ "fetch-all-ops-project-detail" })
	public ResponseEntity<Response<List<MisMapper>>> getAllOpsProjectDetails() {
		List<MisMapper> allProjectDetails = this.misService.getOpsOrExtMisData("ops");
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project details fetch successfully.",
				allProjectDetails, Integer.valueOf(allProjectDetails.size())), HttpStatus.OK);
	}

	@RequestMapping({ "copyFromExistingRerport/{projectId}" })
	public ResponseEntity<Response<Boolean>> copyFromExistingRerport(@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project and Notification Updated successfully",
				projectDetailsService.copyFromExistingReport(projectId)), HttpStatus.OK);
	}
	
	@RequestMapping({ "fetch-all-ext-project-detail" })
	public ResponseEntity<Response<List<MisMapper>>> getAllExtProjectDetails() {
		List<MisMapper> allProjectDetails = this.misService.getOpsOrExtMisData("ext");
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project details fetch successfully.",
				allProjectDetails, Integer.valueOf(allProjectDetails.size())), HttpStatus.OK);
	}
	
	@RequestMapping({"get-pandl-tab/{projectId}"})
	public ResponseEntity<Response<IrrCalculationData>> getPAndLTabData(@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Irr calculation data fetched successfully.",
				projectDetailsService.getPAndLTabData(projectId)), HttpStatus.OK);
	}

	@RequestMapping({ "getProjectIrrCalcById/{projectId}" })
	public ResponseEntity<Response<IrrCalculationData>> getProjectIrrCalcById(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Irr Data Fetched Successfully..",
				irrCalculationDao.findByProjectId(projectId)), HttpStatus.OK);
	}

	@RequestMapping(value = "getAllFixedConsts" , method = RequestMethod.GET)
	public ResponseEntity<Response<List<FixedConstant>>> getAllFixedConsts() {
		List<FixedConstant> fixedConstant = (List<FixedConstant>) fixedConstDao.findAll();
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Project and Notification Updated successfully",
				fixedConstant), HttpStatus.OK);
	}

}
