package com.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.dto.Filter;
import com.api.dto.Response;
import com.api.services.MultiplierService;
import com.api.utils.QueryParser;
import com.common.models.MultipliersMaster;

@RestController
@RequestMapping(value = "multiplier", produces = MediaType.APPLICATION_JSON_VALUE)
public class MultiplierController {

	@Autowired
	private MultiplierService multiplierService;

	@RequestMapping(value = "{id}")
	public ResponseEntity<Response<MultipliersMaster>> getMultiplierbyId(@PathVariable("id") String id) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "multiplier fetched successfully.", multiplierService.get(id)),
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<MultipliersMaster>> save(@RequestBody MultipliersMaster multipliersMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Multiplier saved successfully.",
				multiplierService.save(multipliersMaster)), HttpStatus.OK);
	}

	@RequestMapping(value = "all", method = RequestMethod.GET)
	public ResponseEntity<Response<List<MultipliersMaster>>> getAllCompanies(
			@RequestParam(value = "q", required = false) String query, Pageable pageable) {
		List<Filter> filters = QueryParser.parse(query);
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "multipliers fetched successfully",
				multiplierService.getAllMultiplier(filters, pageable),
				multiplierService.getAllMultiplier(filters, null).size()), HttpStatus.OK);
	}
	
	@RequestMapping(value="if-multiplier_present",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Boolean>> ifMultiplierPresent(@RequestBody MultipliersMaster multipliersMaster) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "Multiplier fetched successfully.",
				multiplierService.ifMultiplierPresent(multipliersMaster)), HttpStatus.OK);
	}

	
	
}
