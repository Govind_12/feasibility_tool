package com.api.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.dao.sql.SeatCategoryDao;
import com.api.dto.Response;
import com.api.services.CommonAnalysisService;
import com.api.services.ProjectAnalysisService;
import com.api.services.ProjectRevenueAnalysisService;
import com.common.mappers.ProjectAnalysisMapper;
import com.common.models.AssumptionsValues;
import com.common.models.AtpAssumptions;
import com.common.models.Navision;
import com.common.models.RevenueExpense;
import com.common.sql.models.SeatCategory;

@RestController
@RequestMapping(value = "projectAnalysis", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectAnalysisController {
	
	@Autowired
	private ProjectAnalysisService projectAnalysisService;

	@Autowired
	private CommonAnalysisService commonAnalysisService;

	@Autowired
	private ProjectRevenueAnalysisService projectRevenueAnalysisService;

	@Autowired
	private SeatCategoryDao seatCategoryDao;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<Response<SeatCategory>> getDemo() {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "fetched successfully.",
				projectAnalysisService.getSeatCategoryMasterDetails()), HttpStatus.OK);
	}

	@RequestMapping(value = "save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<SeatCategory>> save(@RequestBody SeatCategory seatCategory) {
		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(), "seat-category saved successfully.",
				seatCategoryDao.save(seatCategory)), HttpStatus.OK);
	}

	@RequestMapping(value = "{projectId}")
	public ResponseEntity<Response<ProjectAnalysisMapper>> fetchProjectAnalysisById(
			@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "project details analysis fetched successfully by surveyId.",
						projectAnalysisService.getProjectAnalysisById(projectId)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "getCompDetails/{projectId}")
	public ResponseEntity<Response<List<Navision>>> getNavisionData(@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Project comp-nonComp Details fetched successfully by projectId.",
						commonAnalysisService.getAllNavisionByComp()),
				HttpStatus.OK);
	}

//	@RequestMapping(value = "getLegalExpenseBy/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectLegalExpense(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project legal expense details fetched successfully by projectId.",
//				projectAnalysisService.getProjectLegalExpense(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getInsuranceExpenseBy/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectInsuranceExpense(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project Insurance expanse details fetched successfully by projectId.",
//				projectAnalysisService.getProjectInsuranceExpense(projectId, null)), HttpStatus.OK);
//	}

//	@RequestMapping(value = "getProjectInternetCharges/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectInternetCharges(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project Internet Charges details fetched successfully by projectId.",
//				projectAnalysisService.getProjectInternetCharges(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectTravellingConveyance/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectTravellingConveyance(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project travelling conveyance details fetched successfully by projectId.",
//				projectAnalysisService.getProjectTravellingConveyance(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectWaterAndExpanse/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectWaterAndExpanse(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project water and electricity  details fetched successfully by projectId.",
//				projectAnalysisService.getWaterAndElectricity(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectPrintingAndStationary/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectPrintingAndStationary(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project printing and stationary details fetched successfully by projectId.",
//				projectAnalysisService.getPrintingAndStationary(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectPersonnelExpense/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectPersonnelExpense(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project personnal expense details fetched successfully by projectId.",
//				projectAnalysisService.getProjectPersonnalExpanses(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectHouseKeepingExpense/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectHouseKeepingExpense(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project house keeping expense details fetched successfully by projectId.",
//				projectAnalysisService.getProjectHousekeepingExpense(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectSecurityExpense/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectSecurityExpense(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project security expense details fetched successfully by projectId.",
//				projectAnalysisService.getProjectSecurityExpense(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectRepairMaintanceExpense/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectRepairMaintanceExpense(
//			@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project repair and maintaince expense details fetched successfully by projectId.",
//				projectAnalysisService.getProjectRepairAndMaintainceExpense(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectMarketingExpense/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectMarketingExpense(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project marketing expense details fetched successfully by projectId.",
//				projectAnalysisService.getProjectMarketingExpense(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectCommunicationExpense/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectCommunicationExpense(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project communication expense details fetched successfully by projectId.",
//				projectAnalysisService.getProjectCommunicationExpense(projectId, null)), HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectMiscellaneousExpense/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectMiscellaneousExpense(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project miscellaneous expense details fetched successfully by projectId.",
//				projectAnalysisService.getProjectMiscellaneousExpense(projectId, null)), HttpStatus.OK);
//	}

//	@RequestMapping(value = "getProjectMainstreamOccupancyBenchMarkCinemas/{projectId}")
//	public ResponseEntity<Response<Double>> getProjectOccupancyBenchMarkCinemas(
//			@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(
//				new Response<>(HttpStatus.OK.value(),
//						"Project occupancy benchmark cinemas details fetched successfully by projectId.",
//						projectRevenueAnalysisService.getProjectOccupancyBenchMarkCinemas(projectId, null)),
//				HttpStatus.OK);
//	}

//	@RequestMapping(value = "getProjectUpcomingCinemaOccupancy/{projectId}")
//	public ResponseEntity<Response<List<RevenueExpense>>> getProjectUpcomingCinemaOccupancy(
//			@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(
//				new Response<>(HttpStatus.OK.value(),
//						"Project upcoming cinema occupancy details fetched successfully by projectId.",
//						projectRevenueAnalysisService.getProjectUpcomingCinemasOccupancy(projectId, null)),
//				HttpStatus.OK);
//	}

//	@RequestMapping(value = "getProjectUpcomingCinemaAtp/{projectId}")
//	public ResponseEntity<Response<List<AtpAssumptions>>> getProjectUpcomingCinemaAtp(
//			@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project upcoming cinema Atp details fetched successfully by projectId.",
//				projectRevenueAnalysisService.getUpcomingCinemaAtp(projectId, null)), HttpStatus.OK);
//	}

//	@RequestMapping(value = "getProjectUpcomingCinemaSph/{projectId}")
//	public ResponseEntity<Response<List<AssumptionsValues>>> getProjectUpcomingCinemaSph(
//			@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project upcoming cinema Sph details fetched successfully by projectId.",
//				projectRevenueAnalysisService.getUpcomingCinemaSph(projectId, null)), HttpStatus.OK);
//	}

//	@RequestMapping(value = "getProjectUpcomingCinemaAdvertisingRevenue/{projectId}")
//	public ResponseEntity<Response<Long>> getProjectUpcomingCinemaAdvertisingRevenue(
//			@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(
//				new Response<>(HttpStatus.OK.value(), "Project upcoming cinema advertising revenue .",
//						projectRevenueAnalysisService.getAdvertisingRevenue(projectId, null)),
//				HttpStatus.OK);
//	}

	@RequestMapping(value = "getProjectCamExpense/{projectId}")
	public ResponseEntity<Response<Long>> getProjectCam(@PathVariable("projectId") String projectId) {
		return new ResponseEntity<>(
				new Response<>(HttpStatus.OK.value(), "Project cam details fetched successfully by projectId.",
						projectAnalysisService.getProjectCamDetails(projectId, null)),
				HttpStatus.OK);
	}

//	@RequestMapping(value = "getProjectTestAlgo/{projectId}")
//	public ResponseEntity<Response<Set<String>>> getProjectTestAlgo(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(
//				new Response<>(HttpStatus.OK.value(), "Project test details fetched successfully by projectId.",
//						projectRevenueAnalysisService.getProjectTestAlgo(projectId)),
//				HttpStatus.OK);
//
//	}

//	@RequestMapping(value = "getProjectWebSale/{projectId}")
//	public ResponseEntity<Response<Double>> getProjectWebSale(@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(
//				new Response<>(HttpStatus.OK.value(), "Project web sale  details fetched successfully by projectId.",
//						projectRevenueAnalysisService.getProjectWebSaleDetails(projectId, null)),
//				HttpStatus.OK);
//
//	}
	
//	@RequestMapping(value = "getProjectmainstreamAtp/{projectId}")
//	public ResponseEntity<Response<Double>> getProjectmainStreamAtp(
//			@PathVariable("projectId") String projectId) {
//		return new ResponseEntity<>(new Response<>(HttpStatus.OK.value(),
//				"Project mainstream Atp details fetched successfully by projectId.",
//				projectRevenueAnalysisService.getMainstreamBeanchmarkAtp(projectId, null)), HttpStatus.OK);
//	}
}
