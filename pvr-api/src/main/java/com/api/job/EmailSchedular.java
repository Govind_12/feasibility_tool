package com.api.job;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.api.dao.UserDao;
import com.api.services.MailService;
import com.api.services.ProjectDetailsService;
import com.api.utils.RoleUtil;
import com.common.constants.Constants;
import com.common.models.FeasibilityUser;
import com.common.models.ProjectDetails;
import com.common.models.User;

@Component
public class EmailSchedular {

	@Autowired
	private ProjectDetailsService projectDetailsService;

	@Autowired
	private UserDao userDao;

	@Autowired
	MailService mailService;
	
	private long creationTime = 1636502400000l;//10-Nov-2021

	@Scheduled(cron = "0 0 0 * * ?")
	public void emailSchedularForPreSigning() {

		List<ProjectDetails> projectList = projectDetailsService
				.getProjectGreaterThanEqualStatus(Constants.STATUS_SEND_FOR_APPROVAL);
		List<ProjectDetails> preSigningFirstApprover = new ArrayList<ProjectDetails>();
		List<ProjectDetails> preSigningSecondApprover = new ArrayList<ProjectDetails>();
		System.out.println("Email schedular for Pre-Staging started." + new Date());
		if (!ObjectUtils.isEmpty(projectList)) {
			preSigningFirstApprover = projectList.parallelStream()
					.filter(p -> Constants.PRE_SIGNING.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_SEND_FOR_APPROVAL && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
			preSigningSecondApprover = projectList.parallelStream()
					.filter(p -> Constants.PRE_SIGNING.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_APPROVER_1 && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
		}

		User firstApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_1);// CDO
		User secondApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_2);// CO
		preSigningFirstApprover.forEach(p -> {
				long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
				String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
				String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

				if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9) {
					System.out.println("first approver 3,6,8,9,10 days------------->>>");
					reminderMail(firstApprover.getEmail(), p.getReportId(), p.getProjectName(),
							firstApprover.getFullName(), diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
				}
				if (diff == 10 || diff > 10 ? (diff - 10) % 7 == 0 : false) {
					System.out.println("first approver 11th day and weekly reminder------------->>>");
					reminderMail(firstApprover.getEmail(), p.getReportId(), p.getProjectName(),
							firstApprover.getFullName(), diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
					escalationMail(secondApprover.getEmail(), p.getReportId(), p.getProjectName(),
							firstApprover.getFullName(), diff);
				}

		});
		preSigningSecondApprover.forEach(p -> {
				System.out.println("second approver and weekly reminder------------->>>");
				long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
				String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
				String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

				if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9 || diff == 10 || (diff > 10
						? (diff - 10) % 7 == 0
						: false)) {
					reminderMail(secondApprover.getEmail(), p.getReportId(), p.getProjectName(),
							secondApprover.getFullName(), diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
				}
		});
		System.out.println("Email schedular for Pre-Staging stoped." + new Date());
	}

	@Scheduled(cron = "0 0 1 * * ?")
	public void emailSchedularForPostSigning() {
		System.out.println("Email schedular for Post-Staging started.");
		List<ProjectDetails> projectList = projectDetailsService
				.getProjectGreaterThanEqualStatus(Constants.STATUS_SEND_FOR_APPROVAL);
		List<ProjectDetails> postSigningFirstApprover = new ArrayList<ProjectDetails>();
		List<ProjectDetails> postSigningSecondApprover = new ArrayList<ProjectDetails>();
		List<ProjectDetails> postSigningThirdApprover = new ArrayList<ProjectDetails>();
		List<ProjectDetails> postSigningFourthApprover = new ArrayList<ProjectDetails>();

		if (!ObjectUtils.isEmpty(projectList)) {
			postSigningFirstApprover = projectList.parallelStream()
					.filter(p -> Constants.POST_SIGNING.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_SEND_FOR_APPROVAL && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
			postSigningSecondApprover = projectList.parallelStream()
					.filter(p -> Constants.POST_SIGNING.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_APPROVER_1 && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
			postSigningThirdApprover = projectList.parallelStream()
					.filter(p -> Constants.POST_SIGNING.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_APPROVER_2 && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
			postSigningFourthApprover = projectList.parallelStream()
					.filter(p -> Constants.POST_SIGNING.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_APPROVER_3 && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
		}

		User firstApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_1);// CDO
		User secondApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_2);// CO
		User thirdApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_3);// JMD
		User fourthApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_4);// MD

		postSigningFirstApprover.forEach(p -> {
			long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
			String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
			String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

			if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9) {
				System.out.println("first approver 3,6,8,9,10 days------------->>>");
				reminderMail(firstApprover.getEmail(), p.getReportId(), p.getProjectName(), firstApprover.getFullName(),
						diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
			}
			if (diff == 10 || diff > 10 ? (diff - 10) % 7 == 0 : false) {
				System.out.println("first approver 11th day and weekly reminder------------->>>");
				reminderMail(firstApprover.getEmail(), p.getReportId(), p.getProjectName(), firstApprover.getFullName(),
						diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
				escalationMail(secondApprover.getEmail(), p.getReportId(), p.getProjectName(),
						firstApprover.getFullName(), diff);
			}
		});
		postSigningSecondApprover.forEach(p -> {
			long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
			String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
			String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

			if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9) {
				System.out.println("second approver 3,6,8,9,10 days------------->>>");
				reminderMail(secondApprover.getEmail(), p.getReportId(), p.getProjectName(),
						secondApprover.getFullName(), diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
			}
			if (diff == 10 || diff > 10 ? (diff - 10) % 7 == 0 : false) {
				System.out.println("second approver 11th day and weekly reminder------------->>>");
				reminderMail(secondApprover.getEmail(), p.getReportId(), p.getProjectName(),
						secondApprover.getFullName(), diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
				escalationMail(thirdApprover.getEmail(), p.getReportId(), p.getProjectName(),
						secondApprover.getFullName(), diff);
			}

		});
		postSigningThirdApprover.forEach(p -> {
			long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
			String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
			String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

			System.out.println("third approver and weekly reminder------------->>>");
			if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9 || diff == 10 || (diff > 10
					? (diff - 10) % 7 == 0
					: false)) {
				System.out.println("third approver and weekly reminder------------->>>");
				reminderMail(thirdApprover.getEmail(), p.getReportId(), p.getProjectName(), thirdApprover.getFullName(),
						diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
			}

		});
		postSigningFourthApprover.forEach(p -> {
			System.out.println("fourth approver and weekly reminder------------->>>");
			long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
			String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
			String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

			if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9 || diff == 10 || (diff > 10
					? (diff - 10) % 7 == 0
					: false)) {
				reminderMail(fourthApprover.getEmail(), p.getReportId(), p.getProjectName(),
						fourthApprover.getFullName(), diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
			}
		});
	}

	@Scheduled(cron = "0 0 2 * * ?")
	public void emailSchedularForPreHandOver() {
		System.out.println("Email schedular for Pre-handover started.");
		List<ProjectDetails> projectList = projectDetailsService
				.getProjectGreaterThanEqualStatus(Constants.STATUS_SEND_FOR_APPROVAL);
		List<ProjectDetails> preHandOverFirstApprover = new ArrayList<ProjectDetails>();
		List<ProjectDetails> preHandOverSecondApprover = new ArrayList<ProjectDetails>();
		List<ProjectDetails> preHandOverThirdApprover = new ArrayList<ProjectDetails>();
		List<ProjectDetails> preHandOverFourthApprover = new ArrayList<ProjectDetails>();
		List<ProjectDetails> preHandOverFifthApprover = new ArrayList<ProjectDetails>();
		List<ProjectDetails> preHandOverSixthApprover = new ArrayList<ProjectDetails>();

		if (!ObjectUtils.isEmpty(projectList)) {
			preHandOverFirstApprover = projectList.parallelStream()
					.filter(p -> Constants.PRE_HANDOVER.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_SEND_FOR_APPROVAL && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
			preHandOverSecondApprover = projectList.parallelStream()
					.filter(p -> Constants.PRE_HANDOVER.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_APPROVER_1 && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
			preHandOverThirdApprover = projectList.parallelStream()
					.filter(p -> Constants.PRE_HANDOVER.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_APPROVER_2 && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
			preHandOverFourthApprover = projectList.parallelStream()
					.filter(p -> Constants.PRE_HANDOVER.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_APPROVER_3 && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
			preHandOverFifthApprover = projectList.parallelStream()
					.filter(p -> Constants.PRE_HANDOVER.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_APPROVER_4 && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
			preHandOverSixthApprover = projectList.parallelStream()
					.filter(p -> Constants.PRE_HANDOVER.equals(p.getFeasibilityState())
							&& p.getStatus() == Constants.STATUS_APPROVER_5 && p.getCreatedDate()>=creationTime)
					.collect(Collectors.toList());
		}

		User firstApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_1);// CDO
		User secondApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_2);// CO
		User thirdApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_5);// CPO
		User fourthApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_6);// CEO
		User fifthApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_3);// JMD
		User sixthApprover = userDao.findOneByApproverType(Constants.STATUS_APPROVER_4);// MD

		preHandOverFirstApprover.forEach(p -> {
			long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
			String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
			String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

			if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9) {
				System.out.println("first approver 3,6,8,9,10 days------------->>>");
				reminderMail(firstApprover.getEmail(), p.getReportId(), p.getProjectName(), firstApprover.getFullName(),
						diff, p.getFeasibilityState(), submitedBy,sendForApprovalDate);
			}
			if (diff == 10 || diff > 10 ? (diff - 10) % 7 == 0 : false) {
				System.out.println("first approver 11th day and weekly reminder------------->>>");
				reminderMail(firstApprover.getEmail(), p.getReportId(), p.getProjectName(), firstApprover.getFullName(),
						diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
				escalationMail(secondApprover.getEmail(), p.getReportId(), p.getProjectName(),
						firstApprover.getFullName(), diff);
			}
		});
		preHandOverSecondApprover.forEach(p -> {
			long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
			String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
			String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

			if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9) {
				System.out.println("second approver 3,6,8,9,10 days------------->>>");
				reminderMail(secondApprover.getEmail(), p.getReportId(), p.getProjectName(),
						secondApprover.getFullName(), diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
			}
			if (diff == 10 || diff > 10 ? (diff - 10) % 7 == 0 : false) {
				System.out.println("second approver 11th day and weekly reminder------------->>>");
				reminderMail(secondApprover.getEmail(), p.getReportId(), p.getProjectName(),
						secondApprover.getFullName(), diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
				escalationMail(fifthApprover.getEmail(), p.getReportId(), p.getProjectName(),
						secondApprover.getFullName(), diff);
			}
		});
		preHandOverThirdApprover.forEach(p -> {
			long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
			String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
			String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

			if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9) {
				System.out.println("third approver 3,6,8,9,10 days------------->>>");
				reminderMail(thirdApprover.getEmail(), p.getReportId(), p.getProjectName(), thirdApprover.getFullName(),
						diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
			}
			if (diff == 10 || diff > 10 ? (diff - 10) % 7 == 0 : false) {
				System.out.println("third approver 11th day and weekly reminder------------->>>");
				reminderMail(thirdApprover.getEmail(), p.getReportId(), p.getProjectName(), thirdApprover.getFullName(),
						diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
				escalationMail(secondApprover.getEmail(), p.getReportId(), p.getProjectName(),
						thirdApprover.getFullName(), diff);
			}
		});
		preHandOverFourthApprover.forEach(p -> {
			long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
			String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
			String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

			if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9) {
				System.out.println("fourth approver 3,6,8,9,10 days------------->>>");
				reminderMail(fourthApprover.getEmail(), p.getReportId(), p.getProjectName(),
						fourthApprover.getFullName(), diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
			}
			if (diff == 10 || diff > 10 ? (diff - 10) % 7 == 0 : false) {
				System.out.println("fourth approver 11th day and weekly reminder------------->>>");
				reminderMail(fourthApprover.getEmail(), p.getReportId(), p.getProjectName(),
						fourthApprover.getFullName(), diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
				escalationMail(fifthApprover.getEmail(), p.getReportId(), p.getProjectName(),
						fourthApprover.getFullName(), diff);
			}
		});
		preHandOverFifthApprover.forEach(p -> {
			long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
			String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
			String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

			if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9 || diff == 10 || (diff > 10
					? (diff - 10) % 7 == 0
					: false)) {
				System.out.println("fifth approver and weekly reminder------------->>>");
				reminderMail(fifthApprover.getEmail(), p.getReportId(), p.getProjectName(), fifthApprover.getFullName(),
						diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
			}
		});
		preHandOverSixthApprover.forEach(p -> {
			System.out.println("sixth approver and weekly reminder------------->>>");
			long diff = daysDifferenceFromToday(p.getDateOfSendForApproval());
			String submitedBy = !ObjectUtils.isEmpty(p.getProjectBdName()) ? p.getProjectBdName() : "NA";
			String sendForApprovalDate = !ObjectUtils.isEmpty(p.getDateOfSendForApproval()) ? p.getDateOfSendForApproval() : "NA";

			if (diff == 2 || diff == 5 || diff == 7 || diff == 8 || diff == 9 || diff == 10 || (diff > 10
					? (diff - 10) % 7 == 0
					: false)) {
				reminderMail(sixthApprover.getEmail(), p.getReportId(), p.getProjectName(), sixthApprover.getFullName(),
						diff, p.getFeasibilityState(),submitedBy,sendForApprovalDate);
			}
		});

	}

	private long daysDifferenceFromToday(String date) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date firstDate = sdf.parse(date);
			String currentDate = sdf.format(new Date());
			Date secondDate = sdf.parse(currentDate);
			long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
			long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
			return diff;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	private void reminderMail(String to, String projectCode, String feasibilityName, String approverName, long diff, String feasibilityType,
			String submitedBy, String sendForApprovalDate) {
		System.out.println("reminder Mail");

		String subject = "Reminder for feasibility approval";
		String message = "Report: " + projectCode + " " + feasibilityName + " submitted for approval by " + submitedBy + " on " + sendForApprovalDate    
				+ " has been pending for approval with " + approverName + " since " + diff + " days." ;
		boolean status = mailService.sendEmail(to, subject, message);
		if (status) {
			System.out.println("Reminder mail sent successfully.");
		}
	}
	
	private void escalationMail(String to, String projectCode, String feasibilityName, String approverName, long diff) {
		System.out.println("escalation Mail");
		String subject = "Escalation for feasibility approval";
		String message = projectCode + " - " + feasibilityName + " is overdue for approval from " + approverName
				+ " from last " + diff + " days.";
		boolean status = mailService.sendEmail(to, subject, message);
		if (status) {
			System.out.println("Escalation mail sent successfully.");
		}
	}
}
