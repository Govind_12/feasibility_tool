
package com.api.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.BbdoMasterDao;
import com.api.dao.CategoryMasterDao;
import com.api.dao.CinemaMasterDao;
import com.api.dao.CityTierDao;
import com.api.dao.CompitionMasterDao;
import com.api.dao.NavisionDao;
import com.api.dao.ProjectCostSummaryDao;
import com.api.dao.ProjectDetailsDao;
import com.api.dao.sql.SeatCategoryDao;
import com.api.exception.FeasibilityException;
import com.common.constants.Constants;
import com.common.models.BbdoMaster;
import com.common.models.CategoryDataSource;
import com.common.models.CinemaMaster;
import com.common.models.CityTier;
import com.common.models.CompitionMaster;
import com.common.models.Navision;
import com.common.models.ProjectCostData;
import com.common.models.ProjectCostSummary;
import com.common.models.ProjectDetails;
import com.common.sql.models.SeatCategory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("dataSourceUploadService")
public class DataSourceUploadServiceImpl implements DataSourceUploadService {

	@Autowired
	private CinemaMasterDao cinemaMasterDao;
	
	@Autowired
	private BbdoMasterDao bbdoMasterDao;

	@Autowired
	private CompitionMasterDao compitionMasterDao;

	@Autowired
	private CityTierDao cityTierDao;

	@Autowired
	private NavisionDao navisionDao;

	@Autowired
	private CategoryMasterDao categoryMasterDao;

	@Autowired
	private SeatCategoryDao seatCategoryDao;

	@Autowired
	private ProjectCostSummaryDao projectCostSummaryDao;
	
	@Autowired
	private ProjectDetailsDao projectDetailsDao;
	
	@Value("${google.api.key}")
	private String googleKey;

	@Override
	public List<CinemaMaster> save(List<CinemaMaster> cinemaMasterMapper) {
		List<CinemaMaster> cinema = (List<CinemaMaster>) cinemaMasterDao.saveAll(cinemaMasterMapper);
		return cinema;
	}

	@Override
	public List<CompitionMaster> saveCompitionMaster(List<CompitionMaster> compitionMaster) {
		compitionMasterDao.deleteAll();
		compitionMaster.parallelStream().forEach(compition->{
			try {
				Map<String, Double> latLng = getAddressByLatLong(compition.getCompititionName());
				compition.setLatitude(latLng.get(Constants.LATITUDE));
				compition.setLongitude(latLng.get(Constants.LONGITUDE));
			} catch (IOException | ParseException e) {
				e.printStackTrace();
			}
		});
		
		List<CompitionMaster> compitionMasters = (List<CompitionMaster>) compitionMasterDao.saveAll(compitionMaster);
		
		return compitionMasters;
	}

	@Override
	public List<BbdoMaster> saveBbdoMaster(List<BbdoMaster> bbdoMaster) {
		bbdoMasterDao.deleteAll();
		List<BbdoMaster> bbdomaster = (List<BbdoMaster>) bbdoMasterDao.saveAll(bbdoMaster);
		return bbdomaster;
	}

	@Override
	public List<CityTier> saveCityTierMaster(List<CityTier> cityTier) {
		cityTierDao.deleteAll();
		List<CityTier> cityTierMaster = (List<CityTier>) cityTierDao.saveAll(cityTier);
		return cityTierMaster;
	}

	@Override
	public List<Navision> saveNavisionMaster(List<Navision> navision) {
		List<Navision> navisionMaster = (List<Navision>) navisionDao.saveAll(navision);
		return navisionMaster;
	}

	@Override
	public List<CategoryDataSource> saveCategoryMaster(List<CategoryDataSource> categoryMaster) {
		categoryMasterDao.deleteAll();
		List<CategoryDataSource> categoryMasterDataSorce = (List<CategoryDataSource>) categoryMasterDao
				.saveAll(categoryMaster);
		return categoryMasterDataSorce;
	}

	@Override
	public List<SeatCategory> saveSeatCategoryMaster(List<SeatCategory> SeatcategoryMaster) {
		List<SeatCategory> seatCategoryMasterDataSorce = (List<SeatCategory>) seatCategoryDao
				.saveAll(SeatcategoryMaster);
		return seatCategoryMasterDataSorce;
	}

	@Override
	public ProjectCostSummary saveProjectCostSummary(ProjectCostSummary projectCostSummary) {
		log.info("parent id in api : "+projectCostSummary.getParentId());
		
		ProjectCostSummary existingProjectCost = projectCostSummaryDao.findByParentId(projectCostSummary.getParentId());
		if(!ObjectUtils.isEmpty(existingProjectCost)) {
			
		
			List<ProjectCostData> data = projectCostSummary.getProjectCostDataList();
			existingProjectCost.setProjectCostDataList(projectCostSummary.getProjectCostDataList());
			ProjectCostSummary projectCostSummaryDataSource = projectCostSummaryDao.save(existingProjectCost);
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectCostSummaryDataSource.getParentId());

			data.forEach(action->{
				if(!ObjectUtils.isEmpty(action.getDescription())){
					if(action.getDescription().equals(Constants.TOTAL_PROJECT_COST_AFTER_CONTINGENCIES)) {
						projectCostSummaryDataSource.setTotalProjectCost(action.getCost());
						projectCostSummaryDataSource.setDepreciation(action.getDepRate());
						
						if(projectDetails.isPresent()) {
							projectCostSummaryDataSource.setProjectCostPerScreen(action.getCost()/projectDetails.get().getNoOfScreens());
						}
						
						
					}
					
				}
			});
			
			
			return projectCostSummaryDataSource;
		}else {
			
			ProjectCostSummary projectCostSummaryDataSource = projectCostSummaryDao.save(projectCostSummary);
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectCostSummaryDataSource.getParentId());

			List<ProjectCostData> data = projectCostSummary.getProjectCostDataList();

			data.forEach(action->{
				if(!ObjectUtils.isEmpty(action.getDescription())){
					if(action.getDescription().equals(Constants.TOTAL_PROJECT_COST_AFTER_CONTINGENCIES)) {
						projectCostSummaryDataSource.setTotalProjectCost(action.getCost());
						projectCostSummaryDataSource.setDepreciation(action.getDepRate());
						
						if(projectDetails.isPresent()) {
							projectCostSummaryDataSource.setProjectCostPerScreen(action.getCost()/projectDetails.get().getNoOfScreens());
						}
						
						
					}
					
				}
			});
			return projectCostSummaryDataSource;
			
		}

}
	
	@Override
	public ProjectCostSummary fetchAllProjectCost(String projectId) {
		try {
			log.info(projectId);
			/*
			 * Query query = QueryBuilder.createQuery(filters, pageable);
			 * 
			 * List<ProjectCostSummary> projectCostList = mongoTemplate.find(query,
			 * ProjectCostSummary.class);
			 */
			ProjectCostSummary projectCost = projectCostSummaryDao.findByParentId( projectId);
			
			Optional<ProjectDetails> projectDetail = projectDetailsDao.findById(projectId);
			ProjectCostData advanceRentCost = new ProjectCostData();
			ProjectCostData securityDeposit = new ProjectCostData();
			ProjectCostData projectCostPerScreen = new ProjectCostData();
			
			projectCost.getProjectCostDataList().remove(37);
			if(!ObjectUtils.isEmpty(projectDetail.get().getLesserLesse()) && 
					!ObjectUtils.isEmpty(projectDetail.get().getLesserLesse().getAdvanceRent())) {
				
				advanceRentCost.setCost(projectDetail.get().getLesserLesse().getAdvanceRent()/100000);
				projectCost.getProjectCostDataList().add(37, advanceRentCost);
			}else {
				advanceRentCost.setCost(0.0);
				projectCost.getProjectCostDataList().add(37, advanceRentCost);
			}
			
			projectCost.getProjectCostDataList().remove(35);
			if(!ObjectUtils.isEmpty(projectDetail.get().getLesserLesse()) && 
					!ObjectUtils.isEmpty(projectDetail.get().getLesserLesse().getRentSecurityDepositForFixedZeroMg())) {
				
				securityDeposit.setCost(projectDetail.get().getLesserLesse().getRentSecurityDepositForFixedZeroMg()/100000);
				projectCost.getProjectCostDataList().add(35, securityDeposit);
			}else {
				securityDeposit.setCost(0.0);
				projectCost.getProjectCostDataList().add(35, securityDeposit);
			}
			
			Double projectCosts = !ObjectUtils.isEmpty(projectDetail.get().getProjectCost())
					? projectDetail.get().getProjectCost()
					: 0.0;
			
			Double totalProjectCostPerScreen = (projectCosts
					+ projectCost.getProjectCostDataList().get(37).getCost()
					+ projectCost.getProjectCostDataList().get(35).getCost())
					/ (!ObjectUtils.isEmpty(projectDetail.get().getNoOfScreens()) ? projectDetail.get().getNoOfScreens()
							: 1);
			projectCostPerScreen.setCost(totalProjectCostPerScreen); 
			projectCost.getProjectCostDataList().add(39, projectCostPerScreen);

			
			return projectCost;
		} catch (Exception e) {
			log.error("error while fetching Project cost Details.", e);
			throw new FeasibilityException("error while fetching Project cost Details");
		}
	}
	public Map<String, Double> getAddressByLatLong(String fullAddress) throws IOException, ParseException {
		Map<String, Double> map = new HashMap<>();
		double lat = 0;
		double lng = 0;
		try {
			synchronized (this) {
				URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json" + "?address="
						+ URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false" + "&" + "key=" + googleKey + " ");
				URLConnection conn = url.openConnection();
				ByteArrayOutputStream output = new ByteArrayOutputStream(1024);

				IOUtils.copy(conn.getInputStream(), output);
				output.close();

				ObjectMapper mapper = new ObjectMapper();
				JsonNode array = mapper.readValue(output.toString(), JsonNode.class);

				JsonNode object = array.get("results").get(0);

				JsonNode geometry = object.get("geometry");
				JsonNode location = geometry.get("location");

				lat = location.get("lat").asDouble();
				lng = location.get("lng").asDouble();

			}
			map.put(Constants.LATITUDE, lat);
			map.put(Constants.LONGITUDE, lng);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching google api geocoding api");
		}
		return map;
	}
}
