package com.api.services;

import java.util.List;

import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.ProjectDetails;

public interface PreHandoverService extends FeasibilityService<ProjectDetails>{
	
	public ProjectDetails saveStep1(ProjectDetails projectDetails);

	public List<ProjectDetails>  getAllExistingProjects();
	
	ProjectDetails saveDevelopmentDetails(ProjectDetails projectDetail);
	
	QueryMapper saveQuery(MsgMapper msgMapper);

	QueryMapper saveQueryAtOperatingAssumptions(MsgMapper msgMapper);
	
	String updateProjectDetails(MsgMapper msgMapper);
	
	public boolean updateNotificationStatus(String projectId,String queryType,String step);
}
