package com.api.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.api.dto.Filter;
import com.common.models.MultipliersMaster;

public interface MultiplierService  extends FeasibilityService<MultipliersMaster>{

	List<MultipliersMaster> getAllMultiplier(List<Filter> filters,Pageable pageable);

	boolean ifMultiplierPresent(MultipliersMaster multipliersMaster);

}
