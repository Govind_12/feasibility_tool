package com.api.services;

import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.api.dto.Filter;
import com.api.exception.FeasibilityException;
import com.api.utils.QueryBuilder;
import com.common.models.City;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("cityService")
public class CityServiceImpl implements CityService {
	
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public City save(City entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public City update(String id, City entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public City get(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<City> fetchAllcities( List<Filter> filters) {
		try {
			Query query = QueryBuilder.createQuery(filters, null);

			List<City> surveyResultList = mongoTemplate.find(query, City.class);
			
			surveyResultList.sort(new Comparator<City>() {
				@Override
				public int compare(City city1, City city2) {
					return city1.getCity_name().compareTo(city2.getCity_name());
				}
			});

			return surveyResultList;
		} catch (Exception e) {
			log.error("error while fetching Project Details.", e);
			throw new FeasibilityException("error while fetching Project Details");
		}
	}

}
