package com.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.api.dao.MultiplierMasterDao;
import com.api.dto.Filter;
import com.api.exception.FeasibilityException;
import com.api.utils.QueryBuilder;
import com.common.constants.Constants;
import com.common.models.MultipliersMaster;

import lombok.extern.slf4j.Slf4j;

@Service("multiplierService")
@Slf4j
public class MultiplierServiceImpl implements MultiplierService {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private MultiplierMasterDao multiplierMasterDao;

	@Override
	public List<MultipliersMaster> getAllMultiplier(List<Filter> filters, Pageable pageable) {
		try {
			Query query = QueryBuilder.createQuery(filters, pageable);

			query.addCriteria(Criteria.where(Constants.STATUS).is(Constants.STATUS_ACTIVE));
			List<MultipliersMaster> multipliers = mongoTemplate.find(query, MultipliersMaster.class);
			return multipliers;
		} catch (Exception e) {
			log.error("Error while fetching multipliers.", e);
			throw new FeasibilityException("Error while fetching multipliers.");
		}

	}

	@Override
	public MultipliersMaster save(MultipliersMaster entity) {
		entity.setStatus(Constants.STATUS_ACTIVE);
		
		if(entity.getId() !=null) {
			return multiplierMasterDao.save(entity);
		}else {
			MultipliersMaster multi=get(entity.getId());
			return multiplierMasterDao.save(entity);
		}
	}

	@Override
	public MultipliersMaster update(String id, MultipliersMaster entity) {

		return null;
	}

	@Override
	public MultipliersMaster get(String id) {

		return multiplierMasterDao.findOneById(id, Constants.STATUS_ACTIVE);
	}

	@Override
	public boolean ifMultiplierPresent(MultipliersMaster multipliersMaster) {
		List<MultipliersMaster> multiList=this.getAllMultiplier(null, null);
		boolean flag = false;
		for (MultipliersMaster multipliers : multiList) {
			if(multipliers.getCostType().equals(multipliersMaster.getCostType())) {
				flag = true;
				break;
			}
			
		}
		
		return flag;
	}

}
