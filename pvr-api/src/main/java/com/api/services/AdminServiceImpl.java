package com.api.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation.GroupOperationBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.CinemaCategoryMasterDao;
import com.api.dao.CinemaFormatMasterDao;
import com.api.dao.FixedConstantDao;
import com.api.dao.ProjectDetailsDao;
import com.api.dao.SeatTypeMasterDao;
import com.api.exception.FeasibilityException;
import com.api.utils.RoleUtil;
import com.common.constants.Constants;
import com.common.mappers.FhcMapper;
import com.common.models.AtpCapDetials;
import com.common.models.CinemaCategoryMaster;
import com.common.models.CinemaFormatMaster;
import com.common.models.FixedConstant;
import com.common.models.Notification;
import com.common.models.ProjectDetails;
import com.common.models.SeatTypeMaster;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("adminService")
public class AdminServiceImpl implements AdminService {

	@Autowired
	private FixedConstantDao fixedConstantDao;
	
	@Autowired
	private CinemaFormatMasterDao cinemaFormatMasterDao;
	
	@Autowired
	private CinemaCategoryMasterDao cinemaCategoryMasterDao;
	
	@Autowired
	private SeatTypeMasterDao seatTypeMasterDao;

	@Autowired
	private ProjectDetailsDao projectDetailsDao;

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public boolean saveFhc(FhcMapper fhcMapper) {
		if (ObjectUtils.isEmpty(fhcMapper)) {
			log.info("Service charge object cannot be null or empty");
			throw new FeasibilityException("Fhc values cannot be null or empty");
		}

		List<FixedConstant> fixedConstant = (List<FixedConstant>) fixedConstantDao.findAll();
		try {
			if (!ObjectUtils.isEmpty(fixedConstant)) {
				fixedConstant.get(0).getFilmHire().put(fhcMapper.getName().trim().toLowerCase(), fhcMapper.getValue());
				fixedConstantDao.save(fixedConstant.get(0));
				return true;

			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		return false;
	}

	@Override
	public boolean isFhcPresent(FhcMapper fhcMapper) {
		if (ObjectUtils.isEmpty(fhcMapper)) {
			log.info("Service charge object cannot be null or empty");
			throw new FeasibilityException("Fhc values cannot be null or empty");
		}

		List<FixedConstant> fixedConstant = (List<FixedConstant>) fixedConstantDao.findAll();

		Map<String, Double> filmHire = fixedConstant.get(0).getFilmHire();

		return filmHire.containsKey(fhcMapper.getName().trim().toLowerCase());
	}
	
	@Override
	public List<CinemaFormatMaster> getAllCinemaFormat() {
		
		List<CinemaFormatMaster> cinemaCategoryMasterList =null;
		try {
		 cinemaCategoryMasterList = cinemaFormatMasterDao.findAll();
		}catch (Exception e) {
			log.error("Error while fetching CinemaFormatMasterList",e);
		}
		return cinemaCategoryMasterList;
	}
	
	@Override
	public CinemaFormatMaster getCinemaFormat(String id) {
		
		CinemaFormatMaster cinemaCategoryMaster =null;
		try {
			cinemaCategoryMaster = cinemaFormatMasterDao.findOneById(id);
		}catch (Exception e) {
			log.error("Error while fetching CinemaFormatMaster",e);
		}
		return cinemaCategoryMaster;
	}
	
	@Override
	public boolean saveCinemaFormat(CinemaFormatMaster cinemaCategoryMaster) {
		if(ObjectUtils.isEmpty(cinemaCategoryMaster)) {
			log.info("CinemaFormatMaster object cannot be null or empty");
			throw new FeasibilityException("CinemaFormatMaster values cannot be null or empty");
		}
		
		CinemaFormatMaster existingCinemaCategoryMaster =  cinemaFormatMasterDao.findOneById(cinemaCategoryMaster.getId());
		try {
			if(!ObjectUtils.isEmpty(existingCinemaCategoryMaster)) {
				if(cinemaCategoryMaster.isSaveMapping())
				{
					List <String> addedList=cinemaCategoryMaster.getCinemaFormatMapping().stream().filter(predicate_->predicate_!=null)
							.collect(Collectors.toList());
					existingCinemaCategoryMaster.setCinemaFormatMapping(addedList);
				}
				else
				{
					existingCinemaCategoryMaster.setCinemaSubCategory(cinemaCategoryMaster.getCinemaSubCategory());
				}
				cinemaFormatMasterDao.save(existingCinemaCategoryMaster);
				return true;
				 
			}
			else {
				log.error("cannot CinemaFormatMaster in db with id:"+cinemaCategoryMaster.getId());
				return false;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("cannot save CinemaFormatMaster ");
			
		}
		
		return false;
	}
	
	@Override
	public List<CinemaCategoryMaster> getAllCinemaCategory() {
		
		List<CinemaCategoryMaster> cinemaCategoryMasterList =null;
		try {
		 cinemaCategoryMasterList = cinemaCategoryMasterDao.findAll();
		}catch (Exception e) {
			log.error("Error while fetching CinemaCategoryMasterList",e);
		}
		return cinemaCategoryMasterList;
	}
	
	@Override
	public CinemaCategoryMaster getCinemaCategory(String id) {
		
		CinemaCategoryMaster cinemaCategoryMaster =null;
		try {
			cinemaCategoryMaster = cinemaCategoryMasterDao.findOneById(id);
		}catch (Exception e) {
			log.error("Error while fetching CinemaCategoryMaster",e);
		}
		return cinemaCategoryMaster;
	}
	
	@Override
	public boolean saveCinemaCategory(CinemaCategoryMaster cinemaCategoryMaster) {
		if(ObjectUtils.isEmpty(cinemaCategoryMaster)) {
			log.info("CinemaCategoryMaster object cannot be null or empty");
			throw new FeasibilityException("CinemaCategoryMaster values cannot be null or empty");
		}
		
		CinemaCategoryMaster existingCinemaCategoryMaster =  cinemaCategoryMasterDao.findOneById(cinemaCategoryMaster.getId());
		try {
			if(!ObjectUtils.isEmpty(existingCinemaCategoryMaster)) {
				existingCinemaCategoryMaster.setCinemaSubCategory(cinemaCategoryMaster.getCinemaSubCategory());
				cinemaCategoryMasterDao.save(existingCinemaCategoryMaster);
				return true;
				 
			}
			else {
				log.error("cannot CinemaCategoryMaster in db with id:"+cinemaCategoryMaster.getId());
				return false;
			}
		}
		catch (Exception e) {
			log.error("cannot save CinemaCategoryMaster ");
			
		}
		
		return false;
	}
	
	@Override
	public List<SeatTypeMaster> getAllSeatType() {
		
		List<SeatTypeMaster> seatTypeMasterList =null;
		try {
			seatTypeMasterList = seatTypeMasterDao.findAll();
		}catch (Exception e) {
			log.error("Error while fetching seatTypeMasterList",e);
		}
		return seatTypeMasterList;
	}
	
	@Override
	public SeatTypeMaster getSeatType(String id) {
		
		SeatTypeMaster seatTypeMaster =null;
		try {
			seatTypeMaster = seatTypeMasterDao.findOneById(id);
		}catch (Exception e) {
			log.error("Error while fetching SeatTypeMasterList",e);
		}
		return seatTypeMaster;
	}
	
	@Override
	public boolean saveSeatType(SeatTypeMaster seatTypeMaster) {
		if(ObjectUtils.isEmpty(seatTypeMaster)) {
			log.info("SeatTypeMaster object cannot be null or empty");
			throw new FeasibilityException("SeatTypeMaster values cannot be null or empty");
		}
		
		SeatTypeMaster existingSeatTypeMaster =  seatTypeMasterDao.findOneById(seatTypeMaster.getId());
		try {
			if(!ObjectUtils.isEmpty(existingSeatTypeMaster)) {
				existingSeatTypeMaster.setSeatSubType(seatTypeMaster.getSeatSubType());
				seatTypeMasterDao.save(existingSeatTypeMaster);
				return true;
				 
			}
			else {
				log.error("cannot SeatTypeMaster in db with id:"+seatTypeMaster.getId());
				return false;
			}
		}
		catch (Exception e) {
			log.error("cannot save SeatTypeMaster ");
			
		}
		
		return false;
	}
	
	

	@Override
	public boolean deleteAtp(FixedConstant fixedConstant) {
		log.info(fixedConstant.getAtpCapDetails().get(0).getState());
		List<FixedConstant> fixedConstantList = (List<FixedConstant>) fixedConstantDao.findAll();
		FixedConstant data = fixedConstantList.get(0);
		
		List<AtpCapDetials> atpList = data.getAtpCapDetails().stream().filter(predicate->predicate!=null)
				.collect(Collectors.toList());
		try {
			if (!ObjectUtils.isEmpty(atpList)) {
				log.info(atpList.toString());
				
				for (int i = 0; i < atpList.size(); i++) {
					AtpCapDetials atpCapDetials = atpList.get(i);
					if(fixedConstant.getAtpCapDetails().get(0).getState().equalsIgnoreCase(atpCapDetials.getState())) {
						Map<String, Map<String, Double>> details = atpList.get(i).getAtpDetails();
						if(details.containsKey(fixedConstant.getScreenType())) {
							atpList.remove(atpCapDetials);
						}
					}
				}
			}
			data.setAtpCapDetails(atpList);
			fixedConstantDao.save(data);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return false;
		
	}

	@Override
	public boolean saveCogs(FhcMapper fhcMapper) {
		if (ObjectUtils.isEmpty(fhcMapper)) {
			log.info("Service charge object cannot be null or empty");
			throw new FeasibilityException("Fhc values cannot be null or empty");
		}

		List<FixedConstant> fixedConstant = (List<FixedConstant>) fixedConstantDao.findAll();
		try {
			if (!ObjectUtils.isEmpty(fixedConstant)) {
				fixedConstant.get(0).getCogs().put(fhcMapper.getName().trim().toLowerCase(), fhcMapper.getValue());
				fixedConstantDao.save(fixedConstant.get(0));
				return true;

			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean deleteOperatingAssumption(FhcMapper fhcMapper) {
		List<FixedConstant> fixedConstantList = (List<FixedConstant>) fixedConstantDao.findAll();
		FixedConstant data = fixedConstantList.get(0);
		
		Map<String, Double> showList = data.getAvgShowPerDay();
		try {
			if (!ObjectUtils.isEmpty(showList)) {
				log.info(showList.toString());
				
				showList.remove(fhcMapper.getName());
			}
			data.setAvgShowPerDay(showList);
			fixedConstantDao.save(data);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return false;
	}

	@Override
	public boolean saveGstOnTicket(FhcMapper fhcMapper) {
		List<FixedConstant> fixedConstantList = (List<FixedConstant>) fixedConstantDao.findAll();
		FixedConstant data = fixedConstantList.get(0);
		
		Map<String, Double> showList = data.getGstOnTicketForEachFormat();
		try {
			if (!ObjectUtils.isEmpty(showList)) {
				log.info(showList.toString());
				showList.put(fhcMapper.getName(), fhcMapper.getValue());
			}
			data.setGstOnTicketForEachFormat(showList);
			fixedConstantDao.save(data);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return false;
	}

	public List<ProjectDetails> getOpsReportsList() {
		List<ProjectDetails> projectList = null;
		try {
			projectList = this.projectDetailsDao
					.findAllByFeasibilityStateAndStatus("pre_handover", 200);
			Collections.sort(projectList,
					Comparator.<ProjectDetails>comparingLong(ProjectDetails::getCreatedDate).reversed());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectList;
	}

	public List<ProjectDetails> updateOpsReportsList(List<Integer> projectIds) {
		List<ProjectDetails> projectList = null;
		try {
			Query query = new Query();
			Update update = new Update();
			query.addCriteria((CriteriaDefinition) Criteria.where("reportIdVersion").in(projectIds));
			update.set("isOperational", "1");
			this.mongoTemplate.updateMulti(query, update, ProjectDetails.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		projectList = this.projectDetailsDao.findAllByFeasibilityStateAndStatus("pre_handover", 200);
		Collections.sort(projectList,
				Comparator.<ProjectDetails>comparingLong(ProjectDetails::getCreatedDate).reversed());
		return projectList;
	}

	public List<ProjectDetails> getExitReportsList() {
		List<ProjectDetails> postSigningprojectList = null;
		try {
			postSigningprojectList = this.projectDetailsDao
					.findAllByFeasibilityStateAndStatus("post_signing", 200);
			Collections.sort(postSigningprojectList,
					Comparator.<ProjectDetails>comparingLong(ProjectDetails::getCreatedDate).reversed());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return postSigningprojectList;
	}

	public List<ProjectDetails> updateExtReports(List<Integer> projectIds) {
		List<ProjectDetails> projectList = null;
		try {
			Query query = new Query();
			Update update = new Update();
			query.addCriteria((CriteriaDefinition) Criteria.where("reportIdVersion").in(projectIds));
			update.set("isExit", "1");
			this.mongoTemplate.updateMulti(query, update, ProjectDetails.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		projectList = this.projectDetailsDao.findAllByFeasibilityStateAndStatus("post_signing", 200);
		Collections.sort(projectList,
				Comparator.<ProjectDetails>comparingLong(ProjectDetails::getCreatedDate).reversed());
		return projectList;
	}

	public List<ProjectDetails> getListForProjName() {
		try {
			Query query = new Query();
			query.getSortObject();
			List<Integer> status = new ArrayList<>();
			List<ProjectDetails> projectList = null;
//			status.add(Integer.valueOf(0));
//			status.add(Integer.valueOf(208));
//			status.add(Integer.valueOf(308));
			status.add(Integer.valueOf(106));
			status.add(Integer.valueOf(107));
			status.add(Integer.valueOf(108));
			status.add(Integer.valueOf(109));
			status.add(Integer.valueOf(110));
			status.add(Integer.valueOf(111));
			status.add(Integer.valueOf(112));
			status.add(Integer.valueOf(113));
//			query.addCriteria((CriteriaDefinition) Criteria.where("status").not().in(status));
			query.addCriteria((CriteriaDefinition) Criteria.where("status").in(status).and("projectBdId")
					.is(RoleUtil.getCurrentUseInfo().getId()));   
			projectList = this.mongoTemplate.find(query, ProjectDetails.class);
			
			projectList = projectList.stream()
					.filter(p -> ((p.getFeasibilityState().equals("pre_signing") && p.getStatus() != 108)
							|| p.getFeasibilityState().equals("post_signing")
							|| p.getFeasibilityState().equals("pre_handover")))
					.collect(Collectors.toList());
			
			GroupOperation group = Aggregation.group("reportIdVersion","reportIdSubVersion")
					                    .first("reportId").as("reportId")
					                    .first("reportIdVersion").as("reportIdVersion")
					                    .first("projectName").as("projectName")
					                    .first("feasibilityState").as("feasibilityState")
										.first("createdDate").as("createdDate")
							            .first("reportIdSubVersion").as("reportIdSubVersion")
							            .max("reportIdSubVersion").as("maxSubId");
			MatchOperation matchStage = Aggregation.match(Criteria.where("status").in(status));
			
			SortOperation sort = Aggregation.sort(Sort.Direction.DESC, "reportIdVersion");
	
			Aggregation aggregation = Aggregation.newAggregation(matchStage,group, sort);
			AggregationResults<ProjectDetails> output = mongoTemplate.aggregate(aggregation, "project_details",
					ProjectDetails.class);
			List<ProjectDetails> resultList = output.getMappedResults();
			
			List<ProjectDetails> finalResult = new ArrayList<>();
			if (!ObjectUtils.isEmpty(resultList) && !ObjectUtils.isEmpty(projectList)) {
				Set<String> reportIds = resultList.stream().map(ProjectDetails::getReportId)
						.collect(Collectors.toSet());

				finalResult = projectList.stream().filter(p -> reportIds.contains(p.getReportId()))
						.collect(Collectors.toList());
			}
			return finalResult;
		} catch (Exception e) {
			log.error("error while fetching Project Details.", e);
			throw new FeasibilityException("error while fetching Project Details");
		}
	}

	public List<ProjectDetails> updateProjectName(Map<String, String> data) {
		List<ProjectDetails> projectDetails = new ArrayList<>();
		List<String> projectListIds = new ArrayList<>();
		try {
			Query query = new Query();
			Update update = new Update();
			query.addCriteria((CriteriaDefinition) Criteria.where("reportIdVersion")
					.in(new Object[] { Integer.valueOf(Integer.parseInt(data.get("projectId"))) }));
			update.set("projectName", data.get("newProjName"));
			this.mongoTemplate.updateMulti(query, update, ProjectDetails.class);
			query = null;
			update = null;
			query = new Query();
			update = new Update();
			projectDetails = this.projectDetailsDao
					.findAllByReportIdVersion(Integer.valueOf(Integer.parseInt(data.get("projectId"))));
			projectListIds = (List<String>) projectDetails.stream().map(e -> e.getId()).collect(Collectors.toList());
			query.addCriteria((CriteriaDefinition) Criteria.where("projectId").in(projectListIds));
			update.set("projectName", data.get("newProjName"));
			this.mongoTemplate.updateMulti(query, update, Notification.class);
			projectDetails = getListForProjName();
			return projectDetails;
		} catch (Exception e) {
			log.error("error while fetching Project Details.", e);
			throw new FeasibilityException("error while fetching Project Details");
		}
	}

	@Override
	public boolean saveScreenType(CinemaFormatMaster cinemaFormatMaster) {
		if (ObjectUtils.isEmpty(cinemaFormatMaster)) {
			log.info("CinemaFormatMaster object cannot be null or empty");
			throw new FeasibilityException("CinemaFormatMaster values cannot be null or empty");
		}

		try {
			if (!ObjectUtils.isEmpty(cinemaFormatMaster)) {
				
				List<CinemaFormatMaster> cinemaCategoryMasterList = cinemaFormatMasterDao.findAll();
				
				CinemaFormatMaster cinemaFormat = new CinemaFormatMaster();
				cinemaFormat.setCinemaCategory(cinemaFormatMaster.getCinemaCategory());
				cinemaFormat.setCinemaSubCategory(cinemaFormatMaster.getCinemaSubCategory());
				cinemaFormat.setCinemaFormatMapping(cinemaFormatMaster.getCinemaFormatMapping());
				cinemaCategoryMasterList.add(cinemaFormat);
				cinemaFormatMasterDao.saveAll(cinemaCategoryMasterList);
				
				List<FixedConstant> fixedConstant = (List<FixedConstant>) fixedConstantDao.findAll();

				FhcMapper fhcMapper = new FhcMapper();
				fhcMapper.setName(cinemaFormatMaster.getCinemaCategory());
				fhcMapper.setValue(cinemaFormatMaster.getFhcValue());
				if (!ObjectUtils.isEmpty(fixedConstant)) {
					fixedConstant.get(0).getFilmHire().put(fhcMapper.getName().trim().toLowerCase(), fhcMapper.getValue());
					fixedConstantDao.save(fixedConstant.get(0));
				}
				return true;

			} else {
				log.error("Unable to save this screen type."+ cinemaFormatMaster);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in saving screen type "+e.getMessage());

		}
		
		return false;
	}

}
