package com.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.common.constants.Constants;
import com.common.models.FeasibilityUser;
import com.common.models.User;
import com.api.dao.UserDao;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("userDetailServiceImpl")
public class UserDetailServiceImpl implements UserDetailsService {

	@Autowired
	private UserDao userDao;
	
	@Override
	public UserDetails loadUserByUsername(String userName) {

		User user = userDao.findOneByUsernameAndStatus(userName,Constants.STATUS_ACTIVE);

		if (user == null) {
			log.info("No user found with username.");
			throw new UsernameNotFoundException("No user found with username.");
		}

		return new FeasibilityUser(user.getId(), user.getUsername(), (!ObjectUtils.isEmpty(user.getFullName()) ? user.getFullName() : user.getFirstName()), Constants.P_STRING,
				AuthorityUtils.commaSeparatedStringToAuthorityList(user.getAuthorities()),
				user.getEmail(), user.getStatus() > 0, user.getParentId(),user.getCrn(),user.getHashedUserImage(), user.getApproverType(), user.getPreHandOverApproverType(),user.getDesignation());
	}
	
	public UserDetails loadUserByUsernameAndPassword(String userName, String password) {

		User user = userDao.findOneByUsernameAndPasswordAndStatus(userName,password,Constants.STATUS_ACTIVE);
		if (user == null) {
			log.info("No user found with username ."+ userName +" pass "+ password.length());
			throw new UsernameNotFoundException("No user found with username.");
		}

		return new FeasibilityUser(user.getId(), user.getUsername(), (!ObjectUtils.isEmpty(user.getFullName()) ? user.getFullName() : user.getFirstName()), Constants.P_STRING,
				AuthorityUtils.commaSeparatedStringToAuthorityList(user.getAuthorities()),
				user.getEmail(), user.getStatus() > 0,user.getParentId(),user.getCrn(),user.getHashedUserImage(), user.getApproverType(), user.getPreHandOverApproverType(),user.getDesignation());
	}

	public FeasibilityUser loadUserByEmailAndPassword(String email, String password) {
		User user = userDao.findOneByEmailAndPasswordAndStatus(email,password,Constants.STATUS_ACTIVE);
		if (user == null) {
			log.info("No user found with email and password "+ email +" pass "+ password.length());
			throw new UsernameNotFoundException("No user found with username.");
		}

		return new FeasibilityUser(user.getId(), user.getUsername(), (!ObjectUtils.isEmpty(user.getFullName()) ? user.getFullName() : user.getFirstName()), Constants.P_STRING,
				AuthorityUtils.commaSeparatedStringToAuthorityList(user.getAuthorities()),
				user.getEmail(), user.getStatus() > 0,user.getParentId(),user.getCrn(),user.getHashedUserImage(), user.getApproverType(), user.getPreHandOverApproverType(),user.getDesignation());
	}
}
