package com.api.services;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.controllers.DashboardController;
import com.api.dao.ProjectDetailsDao;
import com.api.dao.UserDao;
import com.api.utils.RoleUtil;
import com.common.constants.Constants;
import com.common.models.BDData;
import com.common.models.Notification;
import com.common.models.ProjectDetails;
import com.common.models.User;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

@Slf4j
@Service("dashboardService")
public class DashboardServiceImpl implements DashboardService {

	@Autowired
	private ProjectDetailsService projectDetailsService;

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private ProjectDetailsDao projectDetailsDao;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Map<String, Object> getDashboardDetails() {

		Map<String, Object> dashboardDetails = new HashMap<>();

		if (RoleUtil.isCurrentUserHasApproverRole()) {

			List<ProjectDetails> projectList = null;
			int type = RoleUtil.getCurrentUseInfo().getApproverType();
			// String designation = RoleUtil.getCurrentUseInfo().getDesignation();
			int preHandoverApproverType = RoleUtil.getCurrentUseInfo().getPreHandOverApproverType();
			int statusType = type - 1;
			int statusTypeForPrehandover = preHandoverApproverType - 1;
			/*
			 * if(Constants.DESIGNATION_CEO.equals(designation) ||
			 * Constants.DESIGNATION_CPO.equals(designation)) { projectList =
			 * projectDetailsService.getProjectGreaterThanEqualStatus(statusType); }else {
			 * projectList =
			 * projectDetailsService.getProjectGreaterThanEqualStatus(statusType); }
			 */
			projectList = projectDetailsService.getProjectGreaterThanEqualStatus(Constants.STATUS_SEND_FOR_APPROVAL);
			List<ProjectDetails> approverList = new ArrayList<>();

			if (!ObjectUtils.isEmpty(projectList)) {
				for (int i = 0; i < projectList.size(); i++) {
					User submittedByUser = null;
					if (!ObjectUtils.isEmpty(projectList.get(i).getProjectBdId())) {
						String submittedBy = projectList.get(i).getProjectBdId();
						submittedByUser = userDao.findOneById(submittedBy);
						projectList.get(i).setProjectBdName(submittedByUser.getFullName());
					}
					User user = null;
					if (!ObjectUtils.isEmpty(projectList.get(i).getRejectedBy())) {
						String rejectedBy = projectList.get(i).getRejectedBy();
						user = userDao.findOneById(rejectedBy);
					}
					
					

					if (Constants.PRE_SIGNING.equals(projectList.get(i).getFeasibilityState())) {

						if (projectList.get(i).getStatus() != Constants.STATUS_ARCHIVED
								&& projectList.get(i).getStatus() != Constants.STATUS_ACCEPTED) {
							if (user != null && projectList.get(i).getStatus() == Constants.STATUS_REJECTED) {
								if (user.getApproverType() < type) {
									continue;
								} else {
									approverList.add(projectList.get(i));
								}
							} else {
								if (projectList.get(i).getStatus() < statusType) {
									continue;
								} else {
									approverList.add(projectList.get(i));
								}
							}
						}

						continue;
					}
					if (Constants.POST_SIGNING.equals(projectList.get(i).getFeasibilityState())) {

						if (projectList.get(i).getStatus() != Constants.POST_STATUS_ARCHIVED
								&& projectList.get(i).getStatus() != Constants.STATUS_ACCEPTED) {
							if (user != null && projectList.get(i).getStatus() == Constants.POST_STATUS_REJECTED) {
								if (user.getApproverType() < type) {
									continue;
								} else {
									approverList.add(projectList.get(i));
								}
							} else {
								if (projectList.get(i).getStatus() < statusType) {
									continue;
								} else {
									approverList.add(projectList.get(i));
								}

							}
						}
						continue;
					}

					if (Constants.PRE_HANDOVER.equals(projectList.get(i).getFeasibilityState())) {

						if (projectList.get(i).getStatus() != Constants.PRE_HAND_STATUS_ARCHIVED
								&& projectList.get(i).getStatus() != Constants.STATUS_ACCEPTED) {
							if (user != null && projectList.get(i).getStatus() == Constants.PRE_HAND_STATUS_REJECTED) {
								if (user.getPreHandOverApproverType() < preHandoverApproverType) {
									continue;
								} else {
									approverList.add(projectList.get(i));
								}
							} else {
								if (projectList.get(i).getStatus() < statusTypeForPrehandover) {
									continue;
								} else {
									approverList.add(projectList.get(i));
								}
							}
						}
						continue;
					}

				}

			}

			projectList.clear();
			projectList = approverList;

			Collections.sort(projectList, Comparator.comparingLong(ProjectDetails::getCreatedDate).reversed());
			dashboardDetails.put("projectDetails", projectList);

		}
		if (RoleUtil.isCurrentUserHasBDRole()) {
			List<ProjectDetails> projectList = null;

			projectList = projectDetailsService.fetchAllProjectDetails(null, null);

			projectList = projectList.stream()
					.filter(filter -> filter.getStatus() != Constants.STATUS_DELETE
							&& filter.getProjectBdId().equals(RoleUtil.getCurrentUserID()))
					.collect(Collectors.toList());
			Collections.sort(projectList, Comparator.comparingLong(ProjectDetails::getCreatedDate).reversed());
			dashboardDetails.put("projectDetails", projectList);
		}

		if (RoleUtil.isCurrentUserHasOpeartionRole() || RoleUtil.isCurrentUserHasProjectTeamRole()) {

			List<ProjectDetails> list = projectDetailsService.fetchAllProjectDetails(null, null);
			List<ProjectDetails> projectList = list.stream()
					.filter(p -> ((p.getStatus() >= Constants.STEP_5 ||  p.getIsRejected()) && p.getStatus() != Constants.STATUS_DELETE
							&& p.getStatus() != Constants.STATUS_ACCEPTED
							&& p.getStatus() != Constants.PRE_HAND_STATUS_ARCHIVED)
							&& p.getFeasibilityState().equals(Constants.PRE_HANDOVER))
					.collect(Collectors.toList());

			Collections.sort(projectList, Comparator.comparingLong(ProjectDetails::getCreatedDate).reversed());
			dashboardDetails.put("projectDetails", projectList);
		}

		return dashboardDetails;
	}

	public Map<String, Object> getDashboardDetails2(String userid) {
		Map<String, Object> dashboardDetails = new HashMap<>();
		List<ProjectDetails> projectList = null;
		projectList = projectDetailsService.fetchAllProjectDetails2(null, null, userid);
		projectList = (List<ProjectDetails>) projectList.stream()
				.filter(filter -> (filter.getStatus() != 0 && filter.getProjectBdId().equals(userid)))
				.collect(Collectors.toList());
		Collections.sort(projectList,
				Comparator.<ProjectDetails>comparingLong(ProjectDetails::getCreatedDate).reversed());
		dashboardDetails.put("projectDetails", projectList);
		return dashboardDetails;
	}

	public Map<String, Object> setreportdata(BDData bdata) {
		List<Integer> bdlist = bdata.getIds();
		String toid = bdata.getTo();
		String fromid = bdata.getFrom();
		List<ProjectDetails> details = new ArrayList<>();
		Map<String, Object> mappr = new HashMap<>();
		Query query = null;
		Update update = null;
		for (Integer i : bdlist)
			details.addAll(this.projectDetailsDao.findByReportIdVersion(i).get());
		for (ProjectDetails pd : details) {
			query = new Query();
			update = new Update();
			pd.setProjectBdId(toid);
			query.addCriteria((CriteriaDefinition) Criteria.where("projectId").is(pd.getId()));
			update.set("projectBdId", pd.getProjectBdId());
			mongoTemplate.updateMulti(query, update, Notification.class);
			projectDetailsDao.save(pd);
			query = null;
			update = null;
		}
		log.info("mapper size=" + mappr.size());
		mappr.put("update", Integer.valueOf(details.size()));
		return mappr;
	}

}
