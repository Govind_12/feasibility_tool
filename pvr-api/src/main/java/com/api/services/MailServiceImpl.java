package com.api.services;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.api.dao.UserDao;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MailServiceImpl implements MailService {

	/*
	 * @Autowired private JavaMailSenderImpl javaMail;
	 */

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private UserDao userDao;

	@Value("${spring.mail.from}")
	private String fromMail;

	@Override
	public void sendEmail(String to, String subject, String message, String[] ccList) {
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			helper.setFrom(fromMail);
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(message);
			helper.setCc(ccList);

			javaMailSender.send(mimeMessage);
			System.out.println("mail sent");
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void mailService(String toId, String subject, String message, List<String> ccList) {

		try {
			
			CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(() -> {
				try {
					String toEmail = userDao.findOneById(toId).getEmail();

					String[] ccArray = {};
					if (ccList.size() > 0) {
						ccArray = new String[ccList.size()];
						ccArray = ccList.toArray(ccArray);
					}

					sendEmail(toEmail, subject, message, ccArray);
				} catch (Exception e) {
					throw e;
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.debug("Exceprion in sending mail");
		}

	}

	@Override
	public boolean sendEmail(String to, String subject, String message) {
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		try {
			mimeMessage.setContent(message,  "text/html; charset=utf-8");
		} catch (MessagingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true,"UTF-8");
			helper.setFrom(fromMail);
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(message);
			javaMailSender.send(mimeMessage);
			System.out.println("mail sent");
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return false;
	}

}
