package com.api.services;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.bouncycastle.cert.ocsp.Req;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.LogDetailDao;
import com.api.exception.FeasibilityException;
import com.api.services.helpers.LogHelper;
import com.common.constants.Constants;
import com.common.models.FeasibilityUser;
import com.common.models.LogDetail;

import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("logService")
public class LogServiceImpl implements LogService {

	@Autowired
	private LogDetailDao logDao;

	@Override
	public boolean saveLog(LogHelper logHelper) {
		if (ObjectUtils.isEmpty(logHelper)) {
			log.info("Service charge object cannot be null or empty");
			throw new FeasibilityException("Log info values cannot be null or empty");
		}
		try {
			if (!ObjectUtils.isEmpty(logHelper)) {
				if("/logout".equalsIgnoreCase(logHelper.getRequestURI())) {
					System.out.println("Logout Called.");
					List<LogDetail> logList = logDao.findAllByUsernameOrderByCreatedDateDesc(logHelper.getRequestStatus());
					if(!logList.isEmpty()) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa");
						LogDetail log = logList.get(0);
						log.setLogoutTime(sdf.format(new Date()));
						logDao.save(log);
					}
				}
				return true;

			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return false;
	}

	@Override
	public List<LogDetail> getAllLog() {
		List<LogDetail> logList = null;
		try {
			logList = (List<LogDetail>) logDao.findAllByOrderByCreatedDateDesc();
			logList.removeIf(l->l.getUsername()==null);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return logList;
	}

}
