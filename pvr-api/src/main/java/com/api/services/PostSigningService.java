package com.api.services;

import java.util.List;

import com.common.models.ProjectDetails;

public interface PostSigningService extends FeasibilityService<ProjectDetails>{

	public ProjectDetails saveStep1(ProjectDetails projectDetails);

	public List<ProjectDetails>  getAllExistingProjects();
	
	ProjectDetails saveDevelopmentDetails(ProjectDetails projectDetail);

	ProjectDetails getPostSigningProject(Integer integer);
}
