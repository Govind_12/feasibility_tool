package com.api.services;

import java.util.List;

public interface MailService {

	void mailService(String toId, String subject, String message, List<String> ccList);

	public void sendEmail(String to, String subject, String message, String[] ccList);
	
	public boolean sendEmail(String to, String subject, String message);

}
