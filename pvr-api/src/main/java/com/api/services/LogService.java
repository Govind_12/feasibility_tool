package com.api.services;

import java.util.List;

import com.api.services.helpers.LogHelper;
import com.common.models.LogDetail;

public interface LogService {
	boolean saveLog(LogHelper logHelper);

	List<LogDetail> getAllLog();
}
