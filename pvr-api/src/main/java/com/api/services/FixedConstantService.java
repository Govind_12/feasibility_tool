package com.api.services;

import com.common.mappers.ServiceChargeMapper;
import com.common.models.FixedConstant;

public interface FixedConstantService {
	
	public FixedConstant getFixedConstant();
	
	public FixedConstant saveServiceChargePerTicket(ServiceChargeMapper serviceChargeMapper);
	
	public FixedConstant saveCapexAssumption(FixedConstant fixedConstant);
	
	public FixedConstant saveTaxAssumption(FixedConstant fixedConstant);
	
	public FixedConstant saveOperatingAssumption(FixedConstant fixedConstant);
	
	public FixedConstant saveGrowthAssumptions(FixedConstant fixedConstant);
	
	public FixedConstant saveAtpsCap(FixedConstant fixedConstant);
	
	public FixedConstant irrConstantDetails(FixedConstant fixedConstant);
	

	

}