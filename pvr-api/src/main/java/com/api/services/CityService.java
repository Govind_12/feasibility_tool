package com.api.services;

import java.util.List;

import com.api.dto.Filter;
import com.common.models.City;

public interface CityService extends FeasibilityService<City> {

	List<City> fetchAllcities(List<Filter> filters);
	
}
