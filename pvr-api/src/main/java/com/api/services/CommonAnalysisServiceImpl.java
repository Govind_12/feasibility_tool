
package com.api.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.CategoryMasterDao;
import com.api.dao.CinemaCategoryMasterDao;
import com.api.dao.CinemaFormatMasterDao;
import com.api.dao.CinemaMasterDao;
import com.api.dao.CityTierDao;
import com.api.dao.MultiplierMasterDao;
import com.api.dao.NavisionDao;
import com.api.dao.SeatTypeMasterDao;
import com.api.dao.sql.CinemaMasterSqlDao;
import com.api.exception.FeasibilityException;
import com.common.constants.Constants;
import com.common.models.CategoryDataSource;
import com.common.models.CinemaCategoryMaster;
import com.common.models.CinemaFormat;
import com.common.models.CinemaFormatMaster;
import com.common.models.CityTier;
import com.common.models.MultipliersMaster;
import com.common.models.Navision;
import com.common.models.SeatTypeMaster;
import com.common.sql.models.CinemaMasterSql;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommonAnalysisServiceImpl implements CommonAnalysisService {

	@Autowired
	private CategoryMasterDao categoryMasterDao;

	@Autowired
	private MultiplierMasterDao multiplierMasterDao;

	@Autowired
	private CinemaMasterDao cinemaMasterDao;

	@Autowired
	private CinemaFormatMasterDao cinemaFormatMasterDao;

	@Autowired
	private CinemaCategoryMasterDao cinemaCategoryMasterDao;

	@Autowired
	private SeatTypeMasterDao seatTypeMasterDao;

	@Autowired
	private CityTierDao cityTierDao;

	@Autowired
	private NavisionDao navisionDao;

	@Autowired
	private CinemaMasterSqlDao cinemaMasterSqlDao;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Navision> filterBycategoryAndIcon(List<Navision> listNavision, String categoryType) {
		List<Navision> newNavision = new ArrayList<>();
		try {
			List<String> getAllnavCode = listNavision.stream().distinct().map(Navision::getNavCinemaCode)
					.collect(Collectors.toList());
			List<CategoryDataSource> allcategorybynavCode = categoryMasterDao.findAllByCinemaNavCodeIn(getAllnavCode);

			if (categoryType.equals(Constants.ICON)) {
				List<String> updatednavCode = allcategorybynavCode.stream()
						.filter(predicate -> !predicate.getCinemaCategory().equals(categoryType))
						.map(CategoryDataSource::getCinemaNavCode).collect(Collectors.toList());

				updatednavCode.stream().forEach(navCode -> {
					listNavision.forEach(navision -> {
						if (navision.getNavCinemaCode().equals(navCode)) {
							newNavision.add(navision);
						}
					});
				});

			} else {

				List<String> updatednavCode = allcategorybynavCode.stream()
						.filter(predicate -> predicate.getCinemaCategory().equals(categoryType))
						.map(CategoryDataSource::getCinemaNavCode).collect(Collectors.toList());

				updatednavCode.stream().forEach(navCode -> {
					listNavision.forEach(navision -> {
						if (navision.getNavCinemaCode().equals(navCode)) {
							newNavision.add(navision);
						}
					});
				});
			}
			return newNavision;
		} catch (Exception e) {
			log.error("error while fetching filter by category.", e);
			throw new FeasibilityException("error while fetching filter by category.");
		}

	}

	@Override
	public double electricityAndWaterMultiplier(List<Navision> allNavision, int noOfScreen, double averageYearly,
			List<CinemaFormat> allCinema) {
		double finalAverage = 0;
		try {
			long goldCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.GOLD) || predicate.getFormatType().equals(Constants.LUXE))
					.count();
			long imaxCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.IMAX))
					.count();
			long dxCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.DX))
					.count();
			Optional<MultipliersMaster> MultiplierByCostType = multiplierMasterDao
					.findByCostType(Constants.WATERANDELECTRICITY);
			if (MultiplierByCostType.isPresent()) {
				double a = (double) averageYearly
						* ((double) noOfScreen - (double) goldCount - (double) dxCount - (double) imaxCount);
				double b = (double) averageYearly * MultiplierByCostType.get().getGoldMultiplier() * (double) goldCount;
				double c = (double) averageYearly * MultiplierByCostType.get().getDxMultiplier() * (double) dxCount;
				double d = (double) averageYearly * MultiplierByCostType.get().getImaxMultiplier() * (double) imaxCount;
				finalAverage = a + b + c + d;
				/*
				 * finalAverage = (double) averageYearly ((double) noOfScreen - (double)
				 * goldCount - (double) dxCount - (double) imaxCount) + (double) averageYearly *
				 * MultiplierByCostType.get().getGoldMultiplier() * (double) goldCount +
				 * (double) averageYearly * MultiplierByCostType.get().getDxMultiplier() *
				 * (double) dxCount + (double) averageYearly *
				 * MultiplierByCostType.get().getImaxMultiplier() * (double) imaxCount;
				 */
				if (noOfScreen == 1) {
					finalAverage = (double) finalAverage
							* (double) MultiplierByCostType.get().getSingleScreenMultiplier();
				}

			}

		} catch (Exception e) {
			log.error("error while fetching multiplier for the electricity and water.", e);
			throw new FeasibilityException("error while fetching multiplier for the electricity and water.");
		}
		return Math.round(finalAverage);
	}

	@Override
	public long personnalCostMultiplier(List<Navision> allNavision, int noOfScreen, long averageYearly,
			List<CinemaFormat> allCinema, String categoryType) {
		double finalAveragePersonnalCost = 0;
		try {

			long goldCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.GOLD) || predicate.getFormatType().equals(Constants.LUXE))      
					.count();
			long imaxCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.IMAX))
					.count();
			long dxCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.DX))
					.count();

			Optional<MultipliersMaster> MultiplierByCostType = multiplierMasterDao
					.findByCostType(Constants.PERSONNAL_COST);
			if (MultiplierByCostType.isPresent()) {

				double goldMutilplier = MultiplierByCostType.get().getGoldMultiplier() != null
						? MultiplierByCostType.get().getGoldMultiplier()
						: 0.0;
				double imaxMutilplier = MultiplierByCostType.get().getImaxMultiplier() != null
						? MultiplierByCostType.get().getImaxMultiplier()
						: 0.0;

				if (categoryType.equals(Constants.ICON)) {
					goldMutilplier = 1;
				}

				double d1 = (double) averageYearly * (double) (noOfScreen - goldCount);
				double d2 = (double) averageYearly * (double) goldMutilplier * (double) goldCount;

				finalAveragePersonnalCost = d1 + d2;

				if (categoryType.equals(Constants.ICON)) {
					finalAveragePersonnalCost = (double) finalAveragePersonnalCost
							* (double) MultiplierByCostType.get().getIconMutiplier();

				}

				if (noOfScreen == 1) {
					finalAveragePersonnalCost = (double) finalAveragePersonnalCost
							* (double) MultiplierByCostType.get().getSingleScreenMultiplier();
				}
			}

		} catch (Exception e) {
			log.error("error while fetching multiplier for the personnal cost.", e);
			throw new FeasibilityException("error while fetching multiplier for the personnal cost.");
		}
		return Math.round(finalAveragePersonnalCost);
	}

	@Override
	public double houseKeepingMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema, String cinemaCategory, boolean categoryRelaxed) {
		double finalAverageHousekeeping = 0;
		try {
			long goldCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.GOLD) || predicate.getFormatType().equals(Constants.LUXE))
					.count();

			Optional<MultipliersMaster> MultiplierByCostType = multiplierMasterDao
					.findByCostType(Constants.HOUSE_KEEPING);
			if (MultiplierByCostType.isPresent()) {

				double goldMutilplier = MultiplierByCostType.get().getGoldMultiplier() != null
						? MultiplierByCostType.get().getGoldMultiplier()
						: 0.0;

				boolean isIconCategory = cinemaCategory.equals(Constants.ICON);

				if (isIconCategory) {
					goldMutilplier = Constants.GOLDMULTIPLIERCOUNT;
				}

				double d1 = (double) averageyearly * ((double) noOfScreen - (double) goldCount);
				double d2 = (double) averageyearly * (double) goldMutilplier * (double) goldCount;
				finalAverageHousekeeping = d1 + d2;

				if (isIconCategory && categoryRelaxed) {
					finalAverageHousekeeping = finalAverageHousekeeping
							* (double) MultiplierByCostType.get().getIconMutiplier();
				}

			}

			return Math.round(finalAverageHousekeeping);
		} catch (Exception e) {
			log.error("error while fetching multiplier for the house keeping cost.", e);
			throw new FeasibilityException("error while fetching multiplier for the house keeping cost.");
		}

	}

	@Override
	public List<Navision> getGoldScreenFilter(List<Navision> listNavision) {

		List<Navision> updatedGoldFilter = new ArrayList<>();

		try {
			List<String> getAllnavCodeby = listNavision.stream().map(Navision::getNavCinemaCode).distinct()
					.collect(Collectors.toList());

			if (!ObjectUtils.isEmpty(getAllnavCodeby)) {

				List<CinemaMasterSql> getAllCinemabyNavCode = cinemaMasterSqlDao
						.findAllCinemaMasterByNavCode(getAllnavCodeby);

				/*
				 * List<CinemaMasterSql> getAllCinemabyGoldFilter =
				 * getAllCinemabyNavCode.stream() .filter(predicate -> predicate.getGold() == 0
				 * && predicate.getUltraPremium() == 0) .collect(Collectors.toList());
				 */
				List<CinemaMasterSql> getAllCinemabyGoldFilter = screenTypeCost(Constants.GOLD, getAllCinemabyNavCode);
				List<String> getAllNavCode = getAllCinemabyGoldFilter.stream().map(CinemaMasterSql::getNav_cinemaCode)
						.collect(Collectors.toList());

				getAllNavCode.forEach(navcode -> {
					listNavision.forEach(navision -> {
						if (navision.getNavCinemaCode().equals(navcode)) {
							updatedGoldFilter.add(navision);
						}
					});
				});
			}

			return updatedGoldFilter;
		} catch (Exception e) {
			log.error("error while fetching multiplier for the gold screen cost.", e);
			throw new FeasibilityException("error while fetching multiplier for the gold screen cost.");
		}
	}

	@Override
	public List<Navision> getByCityFilter(List<Navision> navision, String cityTier) {
		try {
			List<String> tierList = new ArrayList<>();
			if (cityTier.equals(Constants.CITY_TIER_1) || cityTier.equals(Constants.CITY_TIER_METRO)) {
				tierList.add(Constants.CITY_TIER_1);
				tierList.add(Constants.CITY_TIER_METRO);
			} else {
				tierList.add(cityTier);
			}
			List<CityTier> CityTiers = cityTierDao.findAllByTierIn(tierList);
			Set<String> cityName = CityTiers.stream().map(CityTier::getCity).collect(Collectors.toSet());
			List<Navision> updatedNavision = new ArrayList<>();

			cityName.forEach(city -> {
				navision.forEach(nav -> {
					if (!ObjectUtils.isEmpty(nav.getCity()) ? nav.getCity().equalsIgnoreCase(city.trim()) : false) {
						updatedNavision.add(nav);
					}
				});
			});
			return updatedNavision;
		} catch (Exception e) {
			log.error("error while fetching  for the city filter.", e);
			throw new FeasibilityException("error while fetching for the city filter.");
		}

	}
	
	@Override
	public List<Navision> getAllNavisionByComp() {
		
		List<Navision> lastQuarterNonCompData = new ArrayList<>();
		
		try {		
			lastQuarterNonCompData = (List<Navision>) navisionDao.findAll();
			
			Navision maxYear = lastQuarterNonCompData.stream()
					.filter(nav-> !ObjectUtils.isEmpty(nav.getYear()))
	                .max((x, y) -> Integer.parseInt(x.getYear().split("-")[0]) - Integer.parseInt(y.getYear().split("-")[0]))
	                .get();
			log.info("max year is: "+ maxYear.getYear());
			
			Navision maxQuarter = lastQuarterNonCompData.stream()
					.filter(nav-> !ObjectUtils.isEmpty(nav.getYear()) && nav.getYear().equals(maxYear.getYear()))
	                .max((x, y) -> Integer.parseInt(x.getQuater().split("Qtr")[1]) - Integer.parseInt(y.getQuater().split("Qtr")[1]))
	                .get();
			log.info("max quarter is: "+ maxQuarter.getQuater());
			
			lastQuarterNonCompData = lastQuarterNonCompData.stream()
					.filter(nav-> maxYear.getYear().equals(nav.getYear()) && maxQuarter.getQuater().equals(nav.getQuater()) && "Non Comp".equals(nav.getCompType()))
					.collect(Collectors.toList());
			log.info("finally non comp data is: "+ lastQuarterNonCompData.size());
			
		}catch(Exception e) {
			log.info("exception in navision data "+ e.getMessage());
			e.printStackTrace();
		}
		
		return lastQuarterNonCompData;
	}

//	@Override
//	public List<Navision> getAllNavisionByComp() {
//
//		List<Navision> allNavisionData = new ArrayList<>();
//		List<String> currentYears = new ArrayList<>();
//		List<String> nextYears = new ArrayList<>();
//		Calendar instance = Calendar.getInstance();
//
//		int currentyear = instance.get(Calendar.YEAR);
//		Date ab=new Date();
//		int q=(instance.get(Calendar.MONTH)/3);
//		System.out.println(q);
//		int prevoiusYear = instance.get(Calendar.YEAR) - 1;
//
//		String currentQuarter = getCurrentQuater();
//
//		List<String> previousQuater = new ArrayList<>();
//		List<String> currentQuater = new ArrayList<>();
//
//		if ("Q1".equals(currentQuarter)) {
//
//			currentQuater.add(Constants.QUATER_1);
//			currentQuater.add(Constants.QUATER_2);
//			currentQuater.add(Constants.QUATER_3);
//			currentQuater.add(Constants.QUATER_4);
//			currentYears.add(DateUtil.getCurrentQuaterYear());
//		} else if ("Q2".equals(currentQuarter)) {
//			currentQuater.add(Constants.QUATER_1);
//			currentQuater.add(Constants.QUATER_2);
//			currentQuater.add(Constants.QUATER_3);
//			currentQuater.add(Constants.QUATER_4);
//			currentYears.add(DateUtil.getCurrentQuaterYear());
//		} else if ("Q3".equals(currentQuarter)) {
//			currentQuater.add(Constants.QUATER_1);
//			currentQuater.add(Constants.QUATER_2);
//			currentQuater.add(Constants.QUATER_3);
//			currentQuater.add(Constants.QUATER_4);
//			currentYears.add(DateUtil.getCurrentQuaterYear());
//		} else if ("Q4".equals(currentQuarter)) {
//			currentQuater.add(Constants.QUATER_1);
//			currentQuater.add(Constants.QUATER_2);
//			currentYears.add(DateUtil.getCurrentQuaterYear());
//
//			previousQuater.add(Constants.QUATER_3);
//			previousQuater.add(Constants.QUATER_4);
//			System.out.println();
//		//String ab1=	DateUtil.getPreviousYear();
//			
//
//		}
//		 
//		for(int i=1;i<=q;i++) {
//			
//			previousQuater.add("Qtr"+i);
//		}
//		
////		nextYears.add("2019-20");
//		nextYears.add(DateUtil.getnextQuaterYear());
//		List<Navision> allDataNavision = new ArrayList<>();
//		
//		List<Navision> curentYearNavision = navisionDao.findAllByYearInAndQuaterIn(currentYears, currentQuater);
//		List<Navision> previousYearNavision = navisionDao.findAllByYearInAndQuaterIn(nextYears, previousQuater);
//
//		if (!ObjectUtils.isEmpty(curentYearNavision)) {
//			allDataNavision.addAll(curentYearNavision);
//		}
//		if (!ObjectUtils.isEmpty(previousYearNavision)) {
//			allDataNavision.addAll(previousYearNavision);
//		}
//
//		Set<String> getNavCode = allDataNavision.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
//
//		List<Navision> finalNavisionDataByComp = new ArrayList<>();
//		for (String navCode : getNavCode) {
//			boolean isAnyNonCompQuaterPresent = true;
//			List<Navision> navisionListByNavCode = allDataNavision.stream()
//					.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).collect(Collectors.toList());
//
//			if (navisionListByNavCode.size() == Constants.FOUR) {
//				isAnyNonCompQuaterPresent = navisionListByNavCode.stream()
//						.anyMatch(predicate -> predicate.getCompType() != null
//								? predicate.getCompType().equalsIgnoreCase(Constants.NONCOMP)
//								: true);
//			}
//
//			if (!isAnyNonCompQuaterPresent) {
//				finalNavisionDataByComp.addAll(navisionListByNavCode);
//			}
//		}
//
//		return finalNavisionDataByComp;
//	}

	@Override
	public double securityMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema) {
		double securityMultiplier = 0;
		try {
			long goldCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.GOLD))
					.count();
			long imaxCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.IMAX))
					.count();
			long dxCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.DX))
					.count();

			/* securityMultiplier = averageyearly * noOfScreen; */
			Optional<MultipliersMaster> MultiplierByCostType = multiplierMasterDao.findByCostType(Constants.SECURITY);
			if (MultiplierByCostType.isPresent()) {
				if (noOfScreen == 1) {
					securityMultiplier = (double) averageyearly
							* (double) MultiplierByCostType.get().getSingleScreenMultiplier();
				} else {
					securityMultiplier = (double) averageyearly;
				}
			}

		} catch (Exception e) {
			log.error("error while fetching  for the security multiplier.", e);
			throw new FeasibilityException("error while fetching for the security multiplier.");
		}
		return Math.round(securityMultiplier);
	}

	@Override
	public double randmMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema, String categoryType) {
		double randmMultiplier = 0;
		try {
			Optional<MultipliersMaster> MultiplierByCostType = multiplierMasterDao.findByCostType(Constants.RANDM_COST);
			if (MultiplierByCostType.isPresent()) {
				long goldCount = allCinema.stream()
						.filter(predicate -> predicate.getFormatType().equals(Constants.GOLD) || predicate.getFormatType().equals(Constants.LUXE)).count();
				long imaxCount = allCinema.stream()
						.filter(predicate -> predicate.getFormatType().equals(Constants.IMAX)).count();
				long dxCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.DX))
						.count();

				double goldMutilplier = MultiplierByCostType.get().getGoldMultiplier() != null
						? MultiplierByCostType.get().getGoldMultiplier()
						: 0.0;
				double imaxMutilplier = MultiplierByCostType.get().getImaxMultiplier() != null
						? MultiplierByCostType.get().getImaxMultiplier()
						: 0.0;

				if (categoryType.equals(Constants.ICON)) {
					goldMutilplier = 1;
					imaxMutilplier = 1;
				}
				double a = averageyearly * ((double) noOfScreen - (double) goldCount - (double) imaxCount);
				double b = averageyearly * (double) goldMutilplier * (double) goldCount;
				double c = (double) averageyearly * imaxMutilplier * (double) imaxCount;

				randmMultiplier = a + b + c;
				/*randmMultiplier = (averageyearly * ((double) noOfScreen - (double) goldCount - (double) imaxCount)
						+ averageyearly * (double) goldMutilplier * (double) goldCount
						+ (double) averageyearly * imaxMutilplier * (double) imaxCount);*/

				if (categoryType.equals(Constants.ICON)) {
					randmMultiplier = randmMultiplier * (double) MultiplierByCostType.get().getIconMutiplier();

				}

				if (noOfScreen == 1) {
					randmMultiplier = randmMultiplier * (double) MultiplierByCostType.get().getSingleScreenMultiplier();

				}
			}

		} catch (Exception e) {
			log.error("error while fetching  for the r&m multiplier.", e);
			throw new FeasibilityException("error while fetching for the r&m multiplier.");
		}
		return Math.round(randmMultiplier);
	}

	@Override
	public double communicationMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema) {
		double communicationMultiplier = 0;
		try {
			Optional<MultipliersMaster> MultiplierByCostType = multiplierMasterDao
					.findByCostType(Constants.COMMUNICATION);
			long goldCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.GOLD))
					.count();
			long imaxCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.IMAX))
					.count();
			long dxCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.DX))
					.count();

			if (noOfScreen == 1) {
				communicationMultiplier = (double) averageyearly
						* (double) MultiplierByCostType.get().getSingleScreenMultiplier();
			} else {
				communicationMultiplier = (double) averageyearly;
			}
		} catch (Exception e) {
			log.error("error while fetching  for the communication multiplier.", e);
			throw new FeasibilityException("error while fetching for the communication multiplier.");
		}
		return Math.round(communicationMultiplier);
	}

	@Override
	public double marketingMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema, String categoryType) {
		double marketingMultiplier = 0;
		try {
			Optional<MultipliersMaster> MultiplierByCostType = multiplierMasterDao
					.findByCostType(Constants.MARKETING_COST);
			if (MultiplierByCostType.isPresent()) {
				long goldCount = allCinema.stream()
						.filter(predicate -> predicate.getFormatType().equals(Constants.GOLD) || predicate.getFormatType().equals(Constants.LUXE)).count();
				long imaxCount = allCinema.stream()
						.filter(predicate -> predicate.getFormatType().equals(Constants.IMAX)).count();
				long dxCount = allCinema.stream().filter(predicate -> predicate.getFormatType().equals(Constants.DX))
						.count();

				double goldMutilplier = MultiplierByCostType.get().getGoldMultiplier() != null
						? MultiplierByCostType.get().getGoldMultiplier()
						: 0.0;
				double imaxMutilplier = MultiplierByCostType.get().getImaxMultiplier() != null
						? MultiplierByCostType.get().getImaxMultiplier()
						: 0.0;

				if (categoryType.equals(Constants.ICON)) {
					goldMutilplier = 1.41;
				}

				long d1 = averageyearly * (noOfScreen - goldCount);
				double d2 = averageyearly * (double) goldMutilplier * (double) goldCount;

				marketingMultiplier = (double) d1 + d2;

				if (categoryType.equals(Constants.ICON)) {
					marketingMultiplier = (double) marketingMultiplier
							* (double) MultiplierByCostType.get().getIconMutiplier();
				}

				if (noOfScreen == 1) {
					marketingMultiplier = (double) marketingMultiplier
							* (double) MultiplierByCostType.get().getSingleScreenMultiplier();
				}
			}

		} catch (Exception e) {
			log.error("error while fetching  for the marketing multiplier.", e);
			throw new FeasibilityException("error while fetching for the marketing multiplier.");
		}
		return Math.round(marketingMultiplier);
	}

	@Override
	public double printingAndStationayMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema) {
		double printingAndStationaryCost = 0;
		try {
			Optional<MultipliersMaster> MultiplierByCostType = multiplierMasterDao
					.findByCostType(Constants.PRINTINGANDSTATIONARY);
			if (MultiplierByCostType.isPresent()) {
				if (noOfScreen == 1) {

					printingAndStationaryCost = (double) averageyearly
							* (double) MultiplierByCostType.get().getSingleScreenMultiplier();

				} else {
					printingAndStationaryCost = (double) averageyearly;
				}
			}

		} catch (Exception e) {
			log.error("error while fetching  for the printing and marketing multiplier.", e);
			throw new FeasibilityException("error while fetching for the printing and marketing multiplier.");
		}
		return Math.round(printingAndStationaryCost);
	}

	@Override
	public double travellingAndConveyanceMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema) {
		double travelAndConveyanve = 0;
		try {
			travelAndConveyanve = (double) averageyearly;

		} catch (Exception e) {
			log.error("error while fetching  for the travelling and conveyance multiplier.", e);
			throw new FeasibilityException("error while fetching for the travelling and conveyance multiplier.");
		}
		return travelAndConveyanve;
	}

	@Override
	public double legalFeeMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema) {
		double legalFee = 0;
		try {
			legalFee = (double) averageyearly;
		} catch (Exception e) {
			log.error("error while fetching  for the legal fee multiplier.", e);
			throw new FeasibilityException("error while fetching for the legal fee multiplier.");
		}
		return legalFee;
	}

	@Override
	public double insuranceExpenseMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema) {
		double insuranceFee = 0;
		try {
			insuranceFee = (double) averageyearly;
		} catch (Exception e) {
			log.error("error while fetching  for the insurance expanse multiplier.", e);
			throw new FeasibilityException("error while fetching for the insurance expanse multiplier.");
		}
		return insuranceFee;
	}

	@Override
	public double internetExpanseMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema) {
		double internetExpenseFee = 0;
		try {
			internetExpenseFee = (double) averageyearly;
		} catch (Exception e) {
			log.error("error while fetching  for the internet fee multiplier.", e);
			throw new FeasibilityException("error while fetching for the internet fee multiplier.");
		}
		return internetExpenseFee;
	}

	@Override
	public double miscellaneousExpenseMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema) {
		double miscellaneaousExpanse = 0;
		try {
			Optional<MultipliersMaster> MultiplierByCostType = multiplierMasterDao
					.findByCostType(Constants.MISCELLANEOUS);
			if (MultiplierByCostType.isPresent()) {

				if (noOfScreen == 1) {
					miscellaneaousExpanse = (double) averageyearly
							* (double) MultiplierByCostType.get().getSingleScreenMultiplier();
				} else {
					miscellaneaousExpanse = (double) averageyearly;
				}
			}

		} catch (Exception e) {
			log.error("error while fetching  for the miscellaneous expense multiplier.", e);
			throw new FeasibilityException("error while fetching for the miscellaneous expense multiplier.");
		}
		return Math.round(miscellaneaousExpanse);
	}

	@Override
	public List<String> getRelatedCategoryByInput(String category) {
		List<String> list = null;

		CinemaCategoryMaster cinemaCategoryMaster = cinemaCategoryMasterDao.findOneByCinemaCategory(category);
		if (!ObjectUtils.isEmpty(cinemaCategoryMaster)) {
			if (cinemaCategoryMaster.getCinemaSubCategory() != null
					&& cinemaCategoryMaster.getCinemaSubCategory().size() > 0) {
				list = cinemaCategoryMaster.getCinemaSubCategory();
			} else {
				log.error(" Cinema Category list is empty");
				list = new ArrayList<String>();
				// throw new FeasibilityException("error while fetching Cinema Category..");
			}

		} else {
			log.error("error while fetching Cinema Category..");
			list = new ArrayList<String>();
			throw new FeasibilityException("error while fetching Cinema Category..");
		}
		/*
		 * if (category.equals(Constants.A)) { list.add(Constants.A);
		 * list.add(Constants.AA); } else if (category.equals(Constants.AA)) {
		 * list.add(Constants.A); list.add(Constants.AA); list.add(Constants.AAA); }
		 * else if (category.equals(Constants.AAA)) { list.add(Constants.AA);
		 * list.add(Constants.AAA); list.add(Constants.ICON); } else if
		 * (category.equals(Constants.ICONCATEGORY)) { list.add(Constants.AAA);
		 * list.add(Constants.ICON); }
		 */

		return list;
	}

	@Override
	public List<Navision> getAllVistaCompNonComp() {
		try {

		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	@Override
	public List<Navision> filterBycategory(List<Navision> listNavision, String categoryType) {
		List<Navision> newNavision = new ArrayList<>();
		try {
			List<String> getAllnavCode = listNavision.stream().map(Navision::getNavCinemaCode).distinct()
					.collect(Collectors.toList());
			List<CategoryDataSource> allcategorybynavCode = categoryMasterDao.findAllByCinemaNavCodeIn(getAllnavCode);

			List<String> updatednavCode = allcategorybynavCode.stream()
					.filter(predicate -> predicate.getCinemaCategory().equalsIgnoreCase(categoryType))
					.map(CategoryDataSource::getCinemaNavCode).collect(Collectors.toList());

			updatednavCode.stream().forEach(navCode -> {
				listNavision.forEach(navision -> {
					if (navision.getNavCinemaCode().equals(navCode)) {
						newNavision.add(navision);
					}
				});
			});

		} catch (Exception e) {
			log.error("error while fetching filter by category.", e);
			throw new FeasibilityException("error while fetching filter by category.");
		}
		return newNavision;
	}

	@Override
	public List<Navision> filterRevenueCategory(List<Navision> listNavision, String categoryType) {
		List<Navision> navision = new ArrayList<>();
		try {

			List<String> getAllnavCode = listNavision.stream().distinct().map(Navision::getNavCinemaCode)
					.collect(Collectors.toList());
			List<CategoryDataSource> allCategoryByNavCode = categoryMasterDao.findAllByCinemaNavCodeIn(getAllnavCode);
			List<String> listCategory = getRelatedCategoryByInput(categoryType);

			List<String> filterCode = new ArrayList<>();
			listCategory.forEach(code -> {
				allCategoryByNavCode.forEach(codeNav -> {
					if (codeNav.getCinemaCategory().equals(code)) {
						filterCode.add(codeNav.getCinemaNavCode());
					}
				});
			});

			/*
			 * allCategoryByNavCode.forEach(codeNav -> { listCategory.forEach(code -> { if
			 * (codeNav.getCinemaCategory().equals(code)) { filterCode.add(code); } }); });
			 */

			filterCode.forEach(action -> {
				listNavision.forEach(action1 -> {
					if (action.equals(action1.getNavCinemaCode())) {
						navision.add(action1);
					}
				});
			});

		} catch (Exception e) {
			log.error("error while fetching filter by RevenueNavision.", e);
			throw new FeasibilityException("error while fetching filter by RevenueNavision.");
		}
		return navision;
	}

	@Override
	public List<String> getCinemaFormatlist(String CategoryType) {
		List<String> cinemaFormat = null;
		CinemaFormatMaster cinemaCategoryMaster = cinemaFormatMasterDao.findOneByCinemaCategory(CategoryType);
		if (!ObjectUtils.isEmpty(cinemaCategoryMaster)) {
			if (cinemaCategoryMaster.getCinemaSubCategory() != null
					&& cinemaCategoryMaster.getCinemaSubCategory().size() > 0) {
				cinemaFormat = cinemaCategoryMaster.getCinemaSubCategory();
			} else {
				log.error(" cinama format sub category is empty");
				cinemaFormat = new ArrayList<String>();
				// throw new FeasibilityException("error while fetching cinama..");
			}

		} else {
			log.error("error while fetching cinama..");
			cinemaFormat = new ArrayList<String>();
			throw new FeasibilityException("error while fetching cinama..");
		}
		/*
		 * try { if (CategoryType.equals(Constants.MAINSTREAM)) {
		 * cinemaFormat.add(Constants.SEATCATEGORY_MAINSTREAM);
		 * cinemaFormat.add(Constants.SEATCATEGORY_MAINTSTREAM);
		 * cinemaFormat.add(Constants.SEATCATEGORY_PREMIER); } else if
		 * (CategoryType.equals(Constants.GOLD)) {
		 * cinemaFormat.add(Constants.SEATCATEGORY_GOLD);
		 * cinemaFormat.add(Constants.SEATCATEGORY_ULTRAPREMIUM); } else if
		 * (CategoryType.equals(Constants.DX)) {
		 * cinemaFormat.add(Constants.SEATCATEGORY_DX); } else if
		 * (CategoryType.equals(Constants.IMAX)) {
		 * cinemaFormat.add(Constants.SEATCATEGORY_IMAX); } else if
		 * (CategoryType.equals(Constants.ONYX)) {
		 * cinemaFormat.add(Constants.SEATCATEGORY_ONYX); } else if
		 * (CategoryType.equals(Constants.PXL)) {
		 * cinemaFormat.add(Constants.SEATCATEGORY_PXL); } else if
		 * (CategoryType.equals(Constants.PLAYHOUSE)) {
		 * cinemaFormat.add(Constants.SEATCATEGORY_PLAYHOUSE);
		 * cinemaFormat.add(Constants.SEATCATEGORY_PLAYHOUSE1); } else if
		 * (CategoryType.equals(Constants.PREMIER)) {
		 * cinemaFormat.add(Constants.SEATCATEGORY_PREMIER);
		 * cinemaFormat.add(Constants.SEATCATEGORY_MAINSTREAM);
		 * cinemaFormat.add(Constants.SEATCATEGORY_MAINTSTREAM); } else if
		 * (CategoryType.equals(Constants.ULTRAPREMIUM)) {
		 * cinemaFormat.add(Constants.SEATCATEGORY_ULTRAPREMIUM);
		 * cinemaFormat.add(Constants.SEATCATEGORY_GOLD); }
		 * 
		 * } catch (Exception e) { log.error("error while fetching cinama..", e); throw
		 * new FeasibilityException("error while fetching cinama..", e); }
		 */
		return cinemaFormat;
	}

	@Override
	public List<String> getAreaCategoryBySeatlist(String seatCategory) {
		List<String> list = null;

		SeatTypeMaster seatTypeMaster = seatTypeMasterDao.findOneBySeatType(seatCategory);
		if (!ObjectUtils.isEmpty(seatTypeMaster)) {
			if (seatTypeMaster.getSeatSubType() != null && seatTypeMaster.getSeatSubType().size() > 0) {
				list = seatTypeMaster.getSeatSubType();
			} else {
				log.error("seat category List is empty..");
				list = new ArrayList<String>();
				// throw new FeasibilityException("error while fetching area category by seat
				// category..");
			}

		} else {
			log.error("error while fetching area category by seat category..");
			list = new ArrayList<String>();
			throw new FeasibilityException("error while fetching area category by seat category..");
		}
		return list;
		/*
		 * try { if (seatCategory.equals(Constants.NORMAL)) { list.add("LOUNGER");
		 * list.add("LOUNGER.."); list.add("PXL LOUNGER"); list.add("PXL RECLINER");
		 * list.add("RECLINER"); list.add("RECLINER."); list.add("RECLINER.."); } else
		 * if (seatCategory.equals(Constants.RECLINER)) { list.add("PXL RECLINER");
		 * list.add("RECLINER"); list.add("RECLINER."); list.add("RECLINER.."); } else
		 * if (seatCategory.equals(Constants.LOUNGER)) { list.add("LOUNGER");
		 * list.add("LOUNGER.."); list.add("PXL LOUNGER"); } return list; } catch
		 * (Exception e) {
		 * log.error("error while fetching area category by seat category..", e); throw
		 * new
		 * FeasibilityException("error while fetching area category by seat category..",
		 * e); }
		 */

	}

	@Override
	public List<Navision> getImaxScreenFilter(List<Navision> listNavision) {
		List<CinemaMasterSql> getAllCinemabyNavCode = new ArrayList<>();
		try {
			List<String> getAllnavCodeby = listNavision.stream().map(Navision::getNavCinemaCode).distinct()
					.collect(Collectors.toList());

		
			if(!ObjectUtils.isEmpty(getAllnavCodeby))
			   getAllCinemabyNavCode = cinemaMasterSqlDao.findAllCinemaMasterByNavCode(getAllnavCodeby);

			/*
			 * List<CinemaMasterSql> getAllCinemabyGoldFilter =
			 * getAllCinemabyNavCode.stream() .filter(predicate -> predicate.getIMAX() ==
			 * 0).collect(Collectors.toList());
			 */

			List<CinemaMasterSql> getAllCinemabyGoldFilter = screenTypeCost(Constants.IMAX, getAllCinemabyNavCode);
			List<String> getAllNavCode = getAllCinemabyGoldFilter.stream().map(CinemaMasterSql::getNav_cinemaCode)
					.collect(Collectors.toList());

			List<Navision> updatedImaxFilter = new ArrayList<>();

			getAllNavCode.forEach(navcode -> {
				listNavision.forEach(navision -> {
					if (navision.getNavCinemaCode().equals(navcode)) {
						updatedImaxFilter.add(navision);
					}
				});
			});

			return updatedImaxFilter;
		} catch (Exception e) {
			log.error("error while filtering imax screen");
			throw new FeasibilityException("error while filtering imax screen");
		}

	}

	@Override
	public List<Navision> get4dxScreenFilter(List<Navision> listNavision) {
		List<CinemaMasterSql> getAllCinemabyNavCode = new ArrayList<>();
		try {
			List<String> getAllnavCodeby = listNavision.stream().map(Navision::getNavCinemaCode).distinct()
					.collect(Collectors.toList());

			if(!ObjectUtils.isEmpty(getAllnavCodeby))
				getAllCinemabyNavCode = cinemaMasterSqlDao.findAllCinemaMasterByNavCode(getAllnavCodeby);
			/*
			 * List<CinemaMasterSql> getAllCinemabyGoldFilter =
			 * getAllCinemabyNavCode.stream() .filter(predicate -> predicate.getDX() ==
			 * 0).collect(Collectors.toList());
			 */
			List<CinemaMasterSql> getAllCinemabyGoldFilter = screenTypeCost(Constants.DX, getAllCinemabyNavCode);
			List<String> getAllNavCode = getAllCinemabyGoldFilter.stream().map(CinemaMasterSql::getNav_cinemaCode)
					.collect(Collectors.toList());

			List<Navision> updated4dxFilter = new ArrayList<>();

			getAllNavCode.forEach(navcode -> {
				listNavision.forEach(navision -> {
					if (navision.getNavCinemaCode().equals(navcode)) {
						updated4dxFilter.add(navision);
					}
				});
			});

			return updated4dxFilter;
		} catch (Exception e) {
			log.error("error while filtering 4dx screen");
			throw new FeasibilityException("error while filtering 4dx screen");
		}
	}

	@Override
	public double camExpenseMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema) {
		double camExpense = 0;
		try {
			camExpense = (double) averageyearly;
		} catch (Exception e) {
			log.error("error while fetching  for the cam expense multiplier.", e);
			throw new FeasibilityException("error while fetching for the cam expense multiplier.");
		}
		return camExpense;
	}

	public static String getCurrentQuater() {
		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);
		return (month >= Calendar.JANUARY && month <= Calendar.MARCH) ? "Q4"
				: (month >= Calendar.APRIL && month <= Calendar.JUNE) ? "Q1"
						: (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) ? "Q2" : "Q3";
	}

	@Override
	public List<Navision> filterOutByIcon(List<Navision> listNavision) {
		List<Navision> newNavision = new ArrayList<>();
		try {
			List<String> getAllnavCode = listNavision.stream().distinct().map(Navision::getNavCinemaCode)
					.collect(Collectors.toList());
			List<CategoryDataSource> allcategorybynavCode = categoryMasterDao.findAllByCinemaNavCodeIn(getAllnavCode);
			List<String> updatednavCode = allcategorybynavCode.stream()
					.filter(predicate -> !predicate.getCinemaCategory().equals(Constants.ICON))
					.map(CategoryDataSource::getCinemaNavCode).collect(Collectors.toList());

			updatednavCode.stream().forEach(navCode -> {
				listNavision.forEach(navision -> {
					if (navision.getNavCinemaCode().equals(navCode)) {
						newNavision.add(navision);
					}
				});
			});
			return newNavision;
		} catch (Exception e) {
			log.error("error while fetching  for the filter out icon.", e);
			throw new FeasibilityException("error while fetching  for the filter out icon.");
		}

	}

	@Override
	public List<Navision> getByCityFilterInSecurity(List<Navision> navision, String cityTier) {
		try {
			List<String> tierList = new ArrayList<>();
			if (cityTier.equals(Constants.CITY_TIER_METRO)) {
				tierList.add(cityTier);
			} else {
				tierList.add(Constants.CITY_TIER_1);
				tierList.add(Constants.CITY_TIER_2);
				tierList.add(Constants.CITY_TIER_3);
				tierList.add(Constants.CITY_TIER_4);
				tierList.add(Constants.CITY_TIER_5);

			}
			List<CityTier> CityTiers = cityTierDao.findAllByTierIn(tierList);
			Set<String> cityName = CityTiers.stream().map(CityTier::getCity).collect(Collectors.toSet());
			List<Navision> updatedNavision = new ArrayList<>();

			cityName.forEach(city -> {
				navision.forEach(nav -> {
					if (!ObjectUtils.isEmpty(nav.getCity()) ? nav.getCity().equalsIgnoreCase(city.trim()) : false) {
						updatedNavision.add(nav);
					}
				});
			});
			return updatedNavision;
		} catch (Exception e) {
			log.error("error while fetching  for the filter out icon.", e);
			throw new FeasibilityException("error while fetching  for the filter out icon.");
		}
	}

	@Override
	public List<Navision> getByCityFilterInMiscellaneous(List<Navision> navision, String cityTier) {
		try {
			List<String> tierList = new ArrayList<>();
			if (cityTier.equals(Constants.CITY_TIER_1) || cityTier.equals(Constants.CITY_TIER_METRO)) {
				tierList.add(Constants.CITY_TIER_1);
				tierList.add(Constants.CITY_TIER_METRO);
			} else {

				tierList.add(Constants.CITY_TIER_2);
				tierList.add(Constants.CITY_TIER_3);
				tierList.add(Constants.CITY_TIER_4);
				tierList.add(Constants.CITY_TIER_5);
			}
			List<CityTier> CityTiers = cityTierDao.findAllByTierIn(tierList);
			Set<String> cityName = CityTiers.stream().map(CityTier::getCity).collect(Collectors.toSet());
			List<Navision> updatedNavision = new ArrayList<>();

			for (String city : cityName) {
				List<Navision> data = navision.stream()
						.filter(predicate -> predicate.getCity().trim().equalsIgnoreCase(city.trim()))
						.collect(Collectors.toList());
				updatedNavision.addAll(data);
			}

			return updatedNavision;
		} catch (Exception e) {
			log.error("error while fetching  for the filter out icon.", e);
			throw new FeasibilityException("error while fetching  for the filter out icon.");
		}
	}

	@Override
	public List<Navision> getByCityFilterInMarketing(List<Navision> navision, String cityTier) {
		try {
			List<String> tierList = new ArrayList<>();
			if (cityTier.equals(Constants.CITY_TIER_1) || cityTier.equals(Constants.CITY_TIER_METRO)) {
				tierList.add(cityTier);

			} else {

				tierList.add(Constants.CITY_TIER_2);
				tierList.add(Constants.CITY_TIER_3);
				tierList.add(Constants.CITY_TIER_4);
				tierList.add(Constants.CITY_TIER_5);
			}
			List<CityTier> CityTiers = cityTierDao.findAllByTierIn(tierList);
			Set<String> cityName = CityTiers.stream().map(CityTier::getCity).collect(Collectors.toSet());
			List<Navision> updatedNavision = new ArrayList<>();

			for (String city : cityName) {
				List<Navision> data = navision.stream()
						.filter(predicate -> predicate.getCity().trim().equalsIgnoreCase(city.trim()))
						.collect(Collectors.toList());
				updatedNavision.addAll(data);
			}

			return updatedNavision;
		} catch (Exception e) {
			log.error("error while fetching  for the tier marketing.", e);
			throw new FeasibilityException("error while fetching  for the tier marketing.");
		}
	}

	@Override
	public List<Navision> getByCityFilterHouseKeeping(List<Navision> navision, String cityTier) {
		try {
			List<String> tierList = new ArrayList<>();
			if (cityTier.equals(Constants.CITY_TIER_1) || cityTier.equals(Constants.CITY_TIER_METRO)) {
				tierList.add(Constants.CITY_TIER_1);
				tierList.add(Constants.CITY_TIER_METRO);
			} else {

				tierList.add(Constants.CITY_TIER_2);
				tierList.add(Constants.CITY_TIER_3);
				tierList.add(Constants.CITY_TIER_4);
				tierList.add(Constants.CITY_TIER_5);
			}
			List<CityTier> CityTiers = cityTierDao.findAllByTierIn(tierList);
			Set<String> cityName = CityTiers.stream().map(CityTier::getCity).collect(Collectors.toSet());
			List<Navision> updatedNavision = new ArrayList<>();

			for (String city : cityName) {
				List<Navision> data = navision.stream()
						.filter(predicate -> predicate.getCity().trim().equalsIgnoreCase(city.trim()))
						.collect(Collectors.toList());
				updatedNavision.addAll(data);
			}

			return updatedNavision;
		} catch (Exception e) {
			log.error("error while fetching  for the tier house keeping.", e);
			throw new FeasibilityException("error while fetching  for the tier house keeping.");
		}
	}

	@Override
	public List<Navision> getByCityFilterPersonnalExpense(List<Navision> navision, String cityTier) {
		try {
			List<String> tierList = new ArrayList<>();
			if (cityTier.equals(Constants.CITY_TIER_1) || cityTier.equals(Constants.CITY_TIER_METRO)) {
				tierList.add(Constants.CITY_TIER_1);
				tierList.add(Constants.CITY_TIER_METRO);
			} else {

				tierList.add(Constants.CITY_TIER_2);
				tierList.add(Constants.CITY_TIER_3);
				tierList.add(Constants.CITY_TIER_4);
				tierList.add(Constants.CITY_TIER_5);
			}
			List<CityTier> CityTiers = cityTierDao.findAllByTierIn(tierList);
			Set<String> cityName = CityTiers.stream().map(CityTier::getCity).collect(Collectors.toSet());
			List<Navision> updatedNavision = new ArrayList<>();

			for (String city : cityName) {
				List<Navision> data = navision.stream()
						.filter(predicate -> predicate.getCity().trim().equalsIgnoreCase(city.trim()))
						.collect(Collectors.toList());
				updatedNavision.addAll(data);
			}

			return updatedNavision;
		} catch (Exception e) {
			log.error("error while fetching  for the tier personnal expense.", e);
			throw new FeasibilityException("error while fetching  for the tier personnal expense.");
		}
	}

	@Override
	public List<Navision> getByCityFilterRevenueExpense(List<Navision> navision, String cityTier) {
		try {
			List<String> tierList = new ArrayList<>();
			if (cityTier.equals(Constants.CITY_TIER_1) || cityTier.equals(Constants.CITY_TIER_METRO)) {
				tierList.add(Constants.CITY_TIER_1);
				tierList.add(Constants.CITY_TIER_METRO);
			} else {

				tierList.add(Constants.CITY_TIER_2);
				tierList.add(Constants.CITY_TIER_3);
				tierList.add(Constants.CITY_TIER_4);
				tierList.add(Constants.CITY_TIER_5);
			}
			List<CityTier> CityTiers = cityTierDao.findAllByTierIn(tierList);
			Set<String> cityName = CityTiers.stream().map(CityTier::getCity).collect(Collectors.toSet());
			List<Navision> updatedNavision = new ArrayList<>();

			for (String city : cityName) {
				List<Navision> data = navision.stream()
						.filter(predicate -> predicate.getCity().trim().equalsIgnoreCase(city.trim()))
						.collect(Collectors.toList());
				updatedNavision.addAll(data);
			}

			return updatedNavision;
		} catch (Exception e) {
			log.error("error while fetching  for the tier revenue expense.", e);
			throw new FeasibilityException("error while fetching  for the tier revenue expense.");
		}
	}

	List<CinemaMasterSql> screenTypeCost(String screen, List<CinemaMasterSql> filterByScreenFormat) {
		List<String> cinemas = new ArrayList<>();
		CinemaFormatMaster format = cinemaFormatMasterDao.findOneByCinemaCategory(screen);
		cinemas.add(screen);
		if (format.getCinemaFormatMapping() != null) {
			cinemas.addAll(format.getCinemaFormatMapping());
		}
		// List<CinemaMasterSql> filterByScreenFormat =
		// cinemaMasterSqlDao.findAllCinemaMasterByNavCode(code);
		List<CinemaMasterSql> filterbyScreen = new ArrayList<>();
		Set<CinemaMasterSql> filterbyScreenSet = new HashSet<>();

		if (cinemas.contains(Constants.GOLD)) {

			if (filterbyScreenSet.size() == 0) {
				List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
						.filter(predicate -> predicate.getGold() == 0).collect(Collectors.toList());
				// filterbyScreenList.forEach(action->System.out.println(action.getNav_cinemaCode()));
				if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
					filterbyScreenSet.addAll(filterbyScreenList);
				}
			} else {
				List<CinemaMasterSql> filterbyScreenList1 = filterbyScreenSet.stream()
						.filter(predicate -> predicate.getGold() == 0).collect(Collectors.toList());
				if (filterbyScreenList1 != null && filterbyScreenList1.size() > 0) {
					filterbyScreenSet.clear();
					filterbyScreenSet.addAll(filterbyScreenList1);
				}
			}

		}
		if (cinemas.contains(Constants.ULTRAPREMIUM)) {

			if (filterbyScreenSet.size() == 0) {
				List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
						.filter(predicate -> predicate.getUltraPremium() == 0).collect(Collectors.toList());
				if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
					filterbyScreenSet.addAll(filterbyScreenList);
				}
			} else {
				List<CinemaMasterSql> filterbyScreenList2 = filterbyScreenSet.stream()
						.filter(predicate -> predicate.getUltraPremium() == 0).collect(Collectors.toList());
				if (filterbyScreenList2 != null && filterbyScreenList2.size() > 0) {
					filterbyScreenSet.clear();
					filterbyScreenSet.addAll(filterbyScreenList2);
				}
			}
		}

		if (cinemas.contains(Constants.MAINSTREAM)) {

			if (filterbyScreenSet.size() == 0) {
				List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
						.filter(predicate -> predicate.getMainstream() == 0).collect(Collectors.toList());
				if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
					filterbyScreenSet.addAll(filterbyScreenList);
				}
			} else {
				List<CinemaMasterSql> filterbyScreenList3 = filterbyScreenSet.stream()
						.filter(predicate -> predicate.getMainstream() == 0).collect(Collectors.toList());
				if (filterbyScreenList3 != null && filterbyScreenList3.size() > 0) {
					filterbyScreenSet.clear();
					filterbyScreenSet.addAll(filterbyScreenList3);
				}
			}

		}

		if (cinemas.contains(Constants.PREMIER)) {

			if (filterbyScreenSet.size() == 0) {
				List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
						.filter(predicate -> predicate.getPremier() == 0).collect(Collectors.toList());
				if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
					filterbyScreenSet.addAll(filterbyScreenList);
				}
			} else {
				List<CinemaMasterSql> filterbyScreenList4 = filterbyScreenSet.stream()
						.filter(predicate -> predicate.getPremier() == 0).collect(Collectors.toList());
				if (filterbyScreenList4 != null && filterbyScreenList4.size() > 0) {
					filterbyScreenSet.clear();
					filterbyScreenSet.addAll(filterbyScreenList4);
				}
			}

		}

		if (cinemas.contains(Constants.DX)) {
			if (filterbyScreenSet.size() == 0) {
				List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
						.filter(predicate -> predicate.getDX() == 0).collect(Collectors.toList());
				if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
					filterbyScreenSet.addAll(filterbyScreenList);
				}
			} else {
				List<CinemaMasterSql> filterbyScreenList5 = filterbyScreenSet.stream()
						.filter(predicate -> predicate.getDX() == 0).collect(Collectors.toList());
				if (filterbyScreenList5 != null && filterbyScreenList5.size() > 0) {
					filterbyScreenSet.clear();
					filterbyScreenSet.addAll(filterbyScreenList5);
				}
			}

		}

		if (cinemas.contains(Constants.PXL)) {
			if (filterbyScreenSet.size() == 0) {
				List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
						.filter(predicate -> predicate.getPXL() == 0).collect(Collectors.toList());
				if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
					filterbyScreenSet.addAll(filterbyScreenList);
				}
			} else {
				List<CinemaMasterSql> filterbyScreenList6 = filterbyScreenSet.stream()
						.filter(predicate -> predicate.getPXL() == 0).collect(Collectors.toList());
				if (filterbyScreenList6 != null && filterbyScreenList6.size() > 0) {
					filterbyScreenSet.clear();
					filterbyScreenSet.addAll(filterbyScreenList6);
				}
			}

		}

		if (cinemas.contains(Constants.IMAX)) {

			if (filterbyScreenSet.size() == 0) {
				List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
						.filter(predicate -> predicate.getIMAX() == 0).collect(Collectors.toList());
				if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
					filterbyScreenSet.addAll(filterbyScreenList);
				}
			} else {
				List<CinemaMasterSql> filterbyScreenList7 = filterbyScreenSet.stream()
						.filter(predicate -> predicate.getIMAX() == 0).collect(Collectors.toList());
				if (filterbyScreenList7 != null && filterbyScreenList7.size() > 0) {
					filterbyScreenSet.clear();
					filterbyScreenSet.addAll(filterbyScreenList7);
				}
			}

		}

		if (cinemas.contains(Constants.PLAYHOUSE)) {

			if (filterbyScreenSet.size() == 0) {
				List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
						.filter(predicate -> predicate.getPlayhouse() == 0).collect(Collectors.toList());
				if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
					filterbyScreenSet.addAll(filterbyScreenList);
				}
			} else {
				List<CinemaMasterSql> filterbyScreenList8 = filterbyScreenSet.stream()
						.filter(predicate -> predicate.getPlayhouse() == 0).collect(Collectors.toList());
				if (filterbyScreenList8 != null && filterbyScreenList8.size() > 0) {
					filterbyScreenSet.clear();
					filterbyScreenSet.addAll(filterbyScreenList8);
				}
			}

		}

		if (cinemas.contains(Constants.ONYX)) {
			if (filterbyScreenSet.size() == 0) {
				List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
						.filter(predicate -> predicate.getONYX() == 0).collect(Collectors.toList());
//				filterbyScreenList.forEach(action -> System.out.println(action.getNav_cinemaCode()));
				if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
					filterbyScreenSet.addAll(filterbyScreenList);
				}
			} else {
				List<CinemaMasterSql> filterbyScreenList9 = filterbyScreenSet.stream()
						.filter(predicate -> predicate.getONYX() == 0).collect(Collectors.toList());
//				filterbyScreenList9.forEach(action -> System.out.println(action.getNav_cinemaCode()));
				if (filterbyScreenList9 != null && filterbyScreenList9.size() > 0) {
					filterbyScreenSet.clear();
					filterbyScreenSet.addAll(filterbyScreenList9);
				}
			}
		}

		if (filterbyScreenSet.size() > 0) {
			filterbyScreen.addAll(filterbyScreenSet);
		}
		return filterbyScreen;
	}
}
