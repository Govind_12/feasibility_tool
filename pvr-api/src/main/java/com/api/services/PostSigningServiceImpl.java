package com.api.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.CityTierDao;
import com.api.dao.IrrCalculationDao;
import com.api.dao.ProjectCostSummaryDao;
import com.api.dao.ProjectDetailsDao;
import com.api.exception.FeasibilityException;
import com.api.utils.RoleUtil;
import com.api.utils.StateAndCityUtil;
import com.common.constants.Constants;
import com.common.models.CityTier;
import com.common.models.IrrCalculationData;
import com.common.models.ProjectCostSummary;
import com.common.models.ProjectDetails;
import com.common.models.ProjectLocation;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PostSigningServiceImpl implements PostSigningService{

	@Autowired
	private ProjectDetailsDao projectDetailsDao;
	
	@Autowired
	private CityTierDao cityTierDao;
	
	@Autowired
	private ProjectDetailsServiceImpl projectDetailsServiceImpl; 
	
	@Autowired
	private ProjectCostSummaryDao projectCostSummaryDao;
	
	@Autowired
	private IrrCalculationDao irrCalculationDao;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public ProjectDetails saveStep1(ProjectDetails projectDetails) {
		return null;
	}



	@Override
	public ProjectDetails save(ProjectDetails entity) {
		return null;
	}



	@Override
	public ProjectDetails update(String id, ProjectDetails entity) {
		return null;
	}



	@Override
	public ProjectDetails get(String id) {
		Optional<ProjectDetails> pD = projectDetailsDao.findById(id);
		return pD.isPresent() ? pD.get() : null;
	}



	@Override
	public List<ProjectDetails> getAllExistingProjects() {
		
		List<ProjectDetails> projectDetails = new ArrayList<>();
		try {
			
			GroupOperation group = Aggregation.group(Fields.fields("reportIdVersion"));
			/*GroupOperation group = Aggregation.group("reportIdVersion").count().as("reportId");*/
			MatchOperation matchStage = Aggregation
					.match(Criteria.where("feasibilityState").is(Constants.PRE_SIGNING).and("projectBdId").is(RoleUtil.getCurrentUserID()));

			Aggregation aggregation = Aggregation.newAggregation(matchStage,group);
			AggregationResults<ProjectDetails> output = mongoTemplate.aggregate(aggregation, "project_details",
					ProjectDetails.class);
			List<ProjectDetails> resultList = output.getMappedResults();
			
			
			
			if(!ObjectUtils.isEmpty(resultList)) {
				for(int i=0;i<resultList.size();i++) {
					
					Integer reportIdVersion = Integer.parseInt(resultList.get(i).getId());
					
					SortOperation sort = Aggregation.sort(Sort.Direction.DESC, "reportIdSubVersion");
					MatchOperation match = Aggregation
							.match(Criteria.where("reportIdVersion").is(reportIdVersion).and("projectBdId").is(RoleUtil.getCurrentUserID()));
					
					Aggregation agg = Aggregation.newAggregation(match, sort);

					AggregationResults<ProjectDetails> result = mongoTemplate.aggregate(agg, "project_details",
							ProjectDetails.class);
					List<ProjectDetails> list = result.getMappedResults();
					
					for(int j=0; j<list.size(); j++) {
						ProjectDetails details = list.get(j);
						if(details.getStatus() == Constants.STATUS_DELETE  && (details.getFeasibilityState().equals(Constants.PRE_SIGNING) || details.getFeasibilityState().equals(Constants.POST_SIGNING) || details.getFeasibilityState().equals(Constants.PRE_HANDOVER) )) {
							continue;
						}
						else {
							if((details.getStatus() == Constants.STATUS_ARCHIVED && details.getFeasibilityState().equals(Constants.PRE_SIGNING)) ||
									(details.getStatus() == Constants.POST_STATUS_ARCHIVED && details.getFeasibilityState().equals(Constants.POST_SIGNING)) ||
									(details.getStatus() == Constants.PRE_HAND_STATUS_ARCHIVED && details.getFeasibilityState().equals(Constants.PRE_HANDOVER))) {
								break;
							}
							if(details.getStatus() != Constants.STATUS_ACCEPTED  && details.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
								break;
							}
							if(details.getStatus() == Constants.STATUS_ACCEPTED  && details.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
								break;
							}
							else if(details.getStatus() != Constants.STATUS_ACCEPTED && details.getFeasibilityState().equals(Constants.POST_SIGNING)) {
								break;
							}
							else if(details.getStatus() == Constants.STATUS_ACCEPTED && details.getFeasibilityState().equals(Constants.POST_SIGNING)) {
								projectDetails.add(details);
								break;
							}
							else if(details.getStatus() != Constants.STATUS_ACCEPTED && details.getFeasibilityState().equals(Constants.PRE_SIGNING)) {
								break;
							}else if(details.getStatus() == Constants.STATUS_ACCEPTED && details.getFeasibilityState().equals(Constants.PRE_SIGNING)) {
								projectDetails.add(details);
								break;
							}
							
						} 
					}
					
					
				}
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching existing projectDetails.");
		}
		return projectDetails;
	}



	@Override
	public ProjectDetails saveDevelopmentDetails(ProjectDetails entity) {
		
		if (ObjectUtils.isEmpty(entity)) {
			log.error("Error while saving Project Details.");
			throw new FeasibilityException("Error while saving Project Details.");
		}

		// city-tier

		String tier = "";
		CityTier cityTier = cityTierDao.findByCity(StateAndCityUtil.getCity(entity.getProjectLocation().getCity()));
		if (!ObjectUtils.isEmpty(cityTier)) {
			tier = cityTier.getTier();
		} else {
			tier = "";
		}
		

		ProjectLocation projectLocation = new ProjectLocation();
		projectLocation.setCityTier(tier);
		projectLocation.setCity(StateAndCityUtil.getCity(entity.getProjectLocation().getCity()));
		projectLocation.setLatitude(entity.getProjectLocation().getLatitude());
		projectLocation.setLongitude(entity.getProjectLocation().getLongitude());
		projectLocation.setLocality(entity.getProjectLocation().getLocality());
		projectLocation.setState(entity.getProjectLocation().getState());
		
		ProjectDetails existingProject = this.get(entity.getId());
		ProjectDetails projectDetails = null;
		
		if(Constants.REPORT_TYPE_EXISTING.equals(entity.getReportType()) && (Constants.PRE_SIGNING.equals(existingProject.getFeasibilityState()) || Constants.POST_SIGNING.equals(existingProject.getFeasibilityState()))) {
			synchronized (this) {

				ProjectDetails existingProjectDetails = this.get(entity.getId());
				existingProjectDetails.setId(null);
				existingProjectDetails.setReportType(Constants.REPORT_TYPE_EXISTING);
				
				ProjectDetails latestProjectDetails = projectDetailsServiceImpl.getLatestProject(existingProjectDetails.getReportType(), existingProjectDetails.getReportId());
				
				existingProjectDetails.setFeasibilityState(Constants.POST_SIGNING);
				existingProjectDetails.setReportId(latestProjectDetails.getReportId());
				existingProjectDetails.setReportIdVersion(latestProjectDetails.getReportIdVersion());
				existingProjectDetails.setReportIdSubVersion(latestProjectDetails.getReportIdSubVersion());
				existingProjectDetails.setReportType(Constants.REPORT_TYPE_NEW);
				
				
				existingProjectDetails.setProjectLocation(projectLocation);
				existingProjectDetails.setTotalDeveloperSize(entity.getTotalDeveloperSize());
				existingProjectDetails.setRetailAreaOfProject(entity.getRetailAreaOfProject());
				existingProjectDetails.setTenancyMix(entity.getTenancyMix());
				existingProjectDetails.setUpcomingInfrastructureDevlopment(entity.getUpcomingInfrastructureDevlopment());
				existingProjectDetails.setDateOfHandover(entity.getDateOfHandover());
				existingProjectDetails.setDateOfOpening(entity.getDateOfOpening());
				existingProjectDetails.setProjectType(entity.getProjectType());
				existingProjectDetails.setDateOfSendForApproval(null);
				existingProjectDetails.setStatus(Constants.STEP_1);
				existingProjectDetails.createdBy(RoleUtil.getCurrentUserID());
				existingProjectDetails.setApproverId(null);
				existingProjectDetails.setMsgMapper(null);
				this.emptyQueryDataForBDApprover(existingProjectDetails);
				existingProjectDetails.setCreatedDate(new Date().getTime());
				existingProjectDetails.createdBy(RoleUtil.getCurrentUserID());
				existingProjectDetails.setIsOperationSubmit(false);
				existingProjectDetails.setIsProjectSubmit(false);
				existingProjectDetails.setProjectBdId(RoleUtil.getCurrentUserID());
				existingProjectDetails.setRejectedBy(null);
				existingProjectDetails.setRejectReasonMsg(null);
				
				if(!ObjectUtils.isEmpty(entity.getFileURLStep1())) {
					existingProjectDetails.setFileURLStep1(entity.getFileURLStep1());
				}
				
				
				log.debug("Saved Project Details.");
				projectDetails = projectDetailsDao.save(existingProjectDetails);
				
				ProjectCostSummary projectCostSummary  =    projectCostSummaryDao.findByParentId(existingProject.getId());
				projectCostSummary.setId(null);
				projectCostSummary.setParentId(projectDetails.getId());
				projectCostSummaryDao.save(projectCostSummary);
				
				IrrCalculationData  irrCalculationData  = irrCalculationDao.findByProjectId(existingProject.getId());
				irrCalculationData.setId(null);
				irrCalculationData.setProjectId(projectDetails.getId());
				irrCalculationDao.save(irrCalculationData);
				
				
				
				
				
			}
			
		}
		
		if(Constants.REPORT_TYPE_NEW.equals(entity.getReportType()) && Constants.POST_SIGNING.equals(existingProject.getFeasibilityState())) {
				
			synchronized (this) {
				ProjectDetails existingProjectDetails = this.get(entity.getId());
				
				existingProjectDetails.setProjectLocation(projectLocation);
				existingProjectDetails.setProjectName(entity.getProjectName());
				existingProjectDetails.setTotalDeveloperSize(entity.getTotalDeveloperSize());
				existingProjectDetails.setRetailAreaOfProject(entity.getRetailAreaOfProject());
				existingProjectDetails.setProjectType(entity.getProjectType());
				existingProjectDetails.setTenancyMix(entity.getTenancyMix());
				existingProjectDetails.setUpcomingInfrastructureDevlopment(entity.getUpcomingInfrastructureDevlopment());
				existingProjectDetails.setDateOfHandover(entity.getDateOfHandover());
				existingProjectDetails.setDateOfOpening(entity.getDateOfOpening());
				if (!ObjectUtils.isEmpty(entity.getFileURLStep1())) {
					existingProjectDetails.setFileURLStep1(entity.getFileURLStep1());
				}
				
				projectDetails = projectDetailsDao.save(existingProjectDetails);
			}
			
		}
		
			projectDetailsServiceImpl.createNotification(projectDetails);
			return projectDetails;
	
	}



	@Override
	public ProjectDetails getPostSigningProject(Integer reportIdVersion) {
		ProjectDetails projectDetails = new ProjectDetails();
		try {

			MatchOperation matchStage = Aggregation.match(Criteria.where("feasibilityState").is(Constants.POST_SIGNING)
					.and("reportIdVersion").is(reportIdVersion).and("status").is(Constants.STATUS_ACCEPTED));

			SortOperation sort = Aggregation.sort(Sort.Direction.DESC, "reportIdSubVersion");

			Aggregation aggregation = Aggregation.newAggregation(matchStage, sort);

			AggregationResults<ProjectDetails> output = mongoTemplate.aggregate(aggregation, "project_details",
					ProjectDetails.class);
			List<ProjectDetails> resultList = output.getMappedResults();
			if(!ObjectUtils.isEmpty(resultList)) {
				projectDetails = resultList.get(0);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching existing projectDetails...");
		}
		return projectDetails;
	}
	
	public void  emptyQueryDataForBDApprover(ProjectDetails projectDetails)
	{
		if(!ObjectUtils.isEmpty(projectDetails)) {
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getTotalAdmits()!=null)
			{
				projectDetails.getFinalSummary().getTotalAdmits().setQuery(null);
				projectDetails.getFinalSummary().getTotalAdmits().setLastQueryUserId(null);
			}
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getOccupancy()!=null)
			{
				projectDetails.getFinalSummary().getOccupancy().setQuery(null);
				projectDetails.getFinalSummary().getOccupancy().setLastQueryUserId(null);
			}
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getAtp()!=null)
			{
				projectDetails.getFinalSummary().getAtp().setQuery(null);
				projectDetails.getFinalSummary().getAtp().setLastQueryUserId(null);
			}
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getSph()!=null)
			{
				projectDetails.getFinalSummary().getSph().setQuery(null);
				projectDetails.getFinalSummary().getSph().setLastQueryUserId(null);
			}
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getAdRevenue()!=null)
			{
				projectDetails.getFinalSummary().getAdRevenue().setQuery(null);
				projectDetails.getFinalSummary().getAdRevenue().setLastQueryUserId(null);
			}
			
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getPersonalExpense()!=null)
			{
				projectDetails.getFinalSummary().getPersonalExpense().setQuery(null);
				projectDetails.getFinalSummary().getPersonalExpense().setLastQueryUserId(null);
			}
			
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getElectricityWaterExpanse()!=null)
			{
				projectDetails.getFinalSummary().getElectricityWaterExpanse().setQuery(null);
				projectDetails.getFinalSummary().getElectricityWaterExpanse().setLastQueryUserId(null);
			}
			
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getTotalRmExpenses()!=null)
			{
				projectDetails.getFinalSummary().getTotalRmExpenses().setQuery(null);
				projectDetails.getFinalSummary().getTotalRmExpenses().setLastQueryUserId(null);
			}
			
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getTotalProjectCost()!=null)
			{
				projectDetails.getFinalSummary().getTotalProjectCost().setQuery(null);
				projectDetails.getFinalSummary().getTotalProjectCost().setLastQueryUserId(null);
			}
			
			
			
			
		}
		
		
	}

}


