package com.api.services;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.api.dao.NotificationDao;
import com.api.dao.UserDao;
import com.api.exception.FeasibilityException;
import com.api.utils.RoleUtil;
import com.common.constants.Constants;
import com.common.constants.MessageConstants;
import com.common.models.Notification;

@Service
public class NotificationService {

	@Autowired
	private NotificationDao notificationDao;
	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private UserDao userDao;

	public List<Notification> getNotficationByType(String type) {

		try {
			MatchOperation matchStage = null;
			String msg = "";

			GroupOperation groupByStateAndSumPop = Aggregation.group("projectId").last("projectId").as("projectId")
					.last("msg").as("msg").last("status").as("status").last("type").as("type").last("approverId")
					.as("approverId").last("projectType").as("projectType").last("createdBy").as("createdBy")
					.last("createdDate").as("createdDate").last("projectStatus").as("projectStatus").last("reportId")
					.as("reportId").last("actionByType").as("actionByType").last("msgMapper").as("msgMapper")
					.last("projectName").as("projectName").last("feasibilityState").as("feasibilityState")
					.last("isProjectSubmit").as("isProjectSubmit").last("isOperationSubmit").as("isOperationSubmit")
					.last("projectBdId").as("projectBdId").last("reportStatus").as("reportStatus");

			SortOperation sort = Aggregation.sort(Sort.Direction.ASC, "createdDate");

			if (RoleUtil.isCurrentUserHasBDRole()) {
				matchStage = Aggregation.match(Criteria.where("status").gt(0)
						.andOperator(Criteria.where("status").ne(Constants.STATUS_ARCHIVED)
								.andOperator(Criteria.where("projectStatus").ne(Constants.STATUS_DELETE).andOperator(
										Criteria.where("projectStatus").ne(Constants.STATUS_ARCHIVED).andOperator(
												Criteria.where("projectBdId").is(RoleUtil.getCurrentUserID()))))));
			} else {
				matchStage = Aggregation.match(Criteria.where("status").gt(0)
						//.andOperator(Criteria.where("status").ne(Constants.STATUS_ARCHIVED)
								.andOperator(Criteria.where("projectStatus").ne(Constants.STATUS_DELETE)
										));
			}

			Aggregation aggregation = Aggregation.newAggregation(matchStage, groupByStateAndSumPop, sort);

			AggregationResults<Notification> output = mongoTemplate.aggregate(aggregation, "notification",
					Notification.class);

			List<Notification> list = output.getMappedResults();
			list = addMessages(list, type);

			return list;

		} catch (Exception e) {
			e.printStackTrace();
			throw new FeasibilityException("Exception in notification aggregation", e);
		}

	}

	public Notification save(Notification notification) {
		return notificationDao.save(notification);
	}

	private List<Notification> addMessages(List<Notification> list, String type) {

		list = new LinkedList<>(list);
		Iterator<Notification> itr = list.iterator();
		Notification n = null;

		//BD
		if (RoleUtil.isCurrentUserHasBDRole()) 
			addMessageForBD(n,itr);

		// approver
		else if (RoleUtil.isCurrentUserHasApproverRole())
			addMessageForApprover( n, itr);
			
		//Operation
		else if (RoleUtil.isCurrentUserHasOpeartionRole()) 
			addMessageForOperation( n, itr);

		//Project
		else if (RoleUtil.isCurrentUserHasProjectTeamRole()) 
			addMessageForProject( n, itr);
			
		// other role
		else {
			// blank that list
			list = new LinkedList<>();

		}

		
		if (type.equals(Constants.UPDATES)) {
			list.removeIf(f -> {
				return StringUtils.isEmpty(f.getUpdates());
			});
		} else
			list.removeIf(f -> {
				return StringUtils.isEmpty(f.getMsg());
			});

		return list;
	}
	
	
	public void addMessageForBD(Notification n, Iterator<Notification> itr)
	{
			while (itr.hasNext()) {
						n = itr.next();
						n.setRole(Constants.ROLE_BD);
						if (n.getProjectStatus() <= Constants.STEP_6 && !n.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
								n.setMsg(MessageConstants.BD_INCOMPLETE);
								continue;
						}
						
						//Pre-Sign
						if(n.getFeasibilityState().equals(Constants.PRE_SIGNING)) {
							
							if (n.getProjectStatus() == Constants.STATUS_REJECTED) {

								String rejectedBy = userDao.findOneByApproverType(n.getActionByType()).getFullName();
								
								n.setMsg(MessageConstants.REJECTED_BY +" by "+rejectedBy);
										/*+ ((n.getActionByType() == 106) ? MessageConstants.APPROVAL_STATUS_1
												: MessageConstants.APPROVAL_STATUS_2));*/

							} else if (!ObjectUtils.isEmpty(n.getMsgMapper()) && n.getMsgMapper().getStep()==Constants.STEP7
									&& !n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD)) {
								
								n.setMsg(MessageConstants.PENDING_QUERY + " "
										+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
								continue;
							} 
							
							else if (n.getProjectStatus() == Constants.STATUS_SEND_FOR_APPROVAL)
								n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL +" from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getFullName());

							else if (n.getProjectStatus() <= Constants.STATUS_APPROVER_2)
								n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL +" from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).getFullName());
							

							else if (n.getProjectStatus() == Constants.STATUS_ACCEPTED)
								n.setUpdates(MessageConstants.APPROVAL_ACCEPTED);

							else
								itr.remove();
						 
						}
						
						//POST-SIGN
						else if(n.getFeasibilityState().equals(Constants.POST_SIGNING)) {
							
							if (n.getProjectStatus() == Constants.POST_STATUS_REJECTED) {

								String rejectStatus="";
								String rejectedBy = userDao.findOneByApproverType(n.getActionByType()).getFullName();
								
								switch(n.getActionByType()) {
								 case Constants.STATUS_APPROVER_1: 
									 rejectStatus= MessageConstants.POST_APPROVAL_STATUS_1;
									 break;
								 case Constants.STATUS_APPROVER_2: 
									 rejectStatus= MessageConstants.POST_APPROVAL_STATUS_2;
									 break;
								 case Constants.STATUS_APPROVER_3: 
									 rejectStatus= MessageConstants.POST_APPROVAL_STATUS_3;
									 break;
								 case Constants.STATUS_APPROVER_4: 
									 rejectStatus= MessageConstants.POST_APPROVAL_STATUS_4;
								}
								n.setMsg(MessageConstants.REJECTED_BY +" by "+rejectedBy);

							} else if (!ObjectUtils.isEmpty(n.getMsgMapper())
									&& !n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD)) {
								n.setMsg(MessageConstants.PENDING_QUERY + " "
										+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
								continue;
							}
							
							else if (n.getProjectStatus() == Constants.STATUS_ACCEPTED)
								n.setUpdates(MessageConstants.APPROVAL_ACCEPTED);
							
							else if(n.getProjectStatus() >= Constants.STATUS_SEND_FOR_APPROVAL) {
								
								switch(n.getProjectStatus()) {
									case Constants.STATUS_SEND_FOR_APPROVAL:{
										n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getFullName());
										break;
									}
									case Constants.STATUS_APPROVER_1: {
										n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).getFullName());
										break;
									}
									case Constants.STATUS_APPROVER_2: {
										n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_3).getFullName());
										break;
									}
									case Constants.STATUS_APPROVER_3: {
										n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_4).getFullName());
									}
								}
								
							}
							else
								itr.remove();
						}
						
						//PRE-Handover
						else {
							
							//< step 6
							if(n.getProjectStatus()<=Constants.STEP_6) {
								
								if ( !ObjectUtils.isEmpty(n.getMsgMapper()) && n.getMsgMapper().getStep()==Constants.STEP6
										&& (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_PROJECT_TEAM) || n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_OPERATION_TEAM))) {
									n.setMsg(MessageConstants.QUERY_ANSWERED + " "
											+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
								}
								else if ( !n.getIsOperationSubmit() && !n.getIsProjectSubmit())
										n.setUpdates(MessageConstants.PENDING_FROM_PROJECT_AND_OPERATION);
									else if ( !n.getIsOperationSubmit())
										n.setUpdates(MessageConstants.PENDING_FROM_OPERATION);
									else if ( !n.getIsProjectSubmit())
										n.setUpdates(MessageConstants.PENDING_FROM_PROJECT);
									else if( n.getProjectStatus() <= Constants.STEP_6 && n.getIsProjectSubmit() && n.getIsOperationSubmit())
										n.setMsg(MessageConstants.PENDING_FROM_YOU_BD);
								
							}
							
							//> step 6
							else if (n.getProjectStatus()>Constants.STEP_6 && !ObjectUtils.isEmpty(n.getMsgMapper()) && n.getMsgMapper().getStep()==Constants.STEP6
									&& (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_PROJECT_TEAM) || n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_OPERATION_TEAM))) {
								n.setMsg(MessageConstants.QUERY_ANSWERED + " "
										+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
							}
							else if ( n.getProjectStatus()>Constants.STEP_6 && !n.getIsOperationSubmit() && !n.getIsProjectSubmit())
									n.setUpdates(MessageConstants.PENDING_FROM_PROJECT_AND_OPERATION);
							else if ( n.getProjectStatus()>Constants.STEP_6 && !n.getIsOperationSubmit())
									n.setUpdates(MessageConstants.PENDING_FROM_OPERATION);
							else if (n.getProjectStatus()>Constants.STEP_6 && !n.getIsProjectSubmit())
									n.setUpdates(MessageConstants.PENDING_FROM_PROJECT); 
								
							
							//others
							else if (n.getFeasibilityState().equals(Constants.PRE_HANDOVER) && !n.getIsOperationSubmit() && !n.getIsProjectSubmit())
									n.setUpdates(MessageConstants.PENDING_FROM_PROJECT_AND_OPERATION);
								else if (n.getFeasibilityState().equals(Constants.PRE_HANDOVER) && !n.getIsOperationSubmit())
									n.setUpdates(MessageConstants.PENDING_FROM_OPERATION);
								else if (n.getFeasibilityState().equals(Constants.PRE_HANDOVER) && !n.getIsProjectSubmit())
									n.setUpdates(MessageConstants.PENDING_FROM_PROJECT);
								else if(n.getFeasibilityState().equals(Constants.PRE_HANDOVER) && n.getProjectStatus() <= Constants.STEP_6 && n.getIsProjectSubmit() && n.getIsOperationSubmit())
									n.setMsg(MessageConstants.PENDING_FROM_YOU_BD);
							
							
							else if (n.getProjectStatus() == Constants.PRE_HAND_STATUS_REJECTED) {

								String rejectedBy = userDao.findOneByApproverType(n.getActionByType()).getFullName();
								
								n.setMsg(MessageConstants.REJECTED_BY +" by "+rejectedBy);

							} else if (!ObjectUtils.isEmpty(n.getMsgMapper())
									&& n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_APPROVER)) {
								n.setMsg(MessageConstants.PENDING_QUERY + " "
										+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
								//continue;
							}
							
							else if (n.getProjectStatus() == Constants.STATUS_ACCEPTED)
								n.setUpdates(MessageConstants.APPROVAL_ACCEPTED);
							
							else if(n.getProjectStatus() >= Constants.STATUS_SEND_FOR_APPROVAL) {
								
								switch(n.getProjectStatus()) {
									case Constants.STATUS_SEND_FOR_APPROVAL:{
										n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getFullName());
										break;
									}
									case Constants.STATUS_APPROVER_1: {
										n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).getFullName());
										break;
									}
									case Constants.STATUS_APPROVER_2: {
										n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_5).getFullName());
										break;
									}
									case Constants.STATUS_APPROVER_3: {
										n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_6).getFullName());
										break;
									}
									case Constants.STATUS_APPROVER_4: {
										n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_3).getFullName());
										break;
									}
									case Constants.STATUS_APPROVER_5: {
										n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_4).getFullName());
									}
								}
								
							}
							else
								itr.remove();
							
							
						}

					}
		
	}
	
	public void addMessageForApprover(Notification n, Iterator<Notification> itr)
	{
		while (itr.hasNext())
		{
			n = itr.next();
			n.setRole(Constants.ROLE_APPROVER);

			
			if (n.getProjectStatus() <= Constants.STEP_6) {
				itr.remove();
				continue;
			}

			
			
			//Pre-Sign
			if(n.getFeasibilityState().equals(Constants.PRE_SIGNING) && RoleUtil.getCurrentUseInfo().getApproverType() <= Constants.STATUS_APPROVER_2) {

				if (!ObjectUtils.isEmpty(n.getMsgMapper())) {
					
					/*if (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && ( (RoleUtil.getCurrentUseInfo().getApproverType()-1) == n.getProjectStatus() || (RoleUtil.getCurrentUseInfo().getApproverType()== n.getProjectStatus()) )) {
						n.setMsg(MessageConstants.QUERY_ANSWERED + " "+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
						continue;
					}*/
					
					if (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && (n.getProjectStatus() == RoleUtil.getCurrentUseInfo().getApproverType()-1)) {
						n.setMsg(MessageConstants.QUERY_ANSWERED + " "+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
						continue;
					}
					if (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && (n.getProjectStatus() > RoleUtil.getCurrentUseInfo().getApproverType()-1)) {
						n.setUpdates(MessageConstants.QUERY_ANSWERED + " "+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
						continue;
					}
					
					else if ( n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_APPROVER)  && !n.getMsgMapper().getMsgById().equals(RoleUtil.getCurrentUseInfo().getId())  && (n.getProjectStatus() >= RoleUtil.getCurrentUseInfo().getApproverType())) {
						n.setUpdates(MessageConstants.PENDING_QUERY + " "
								+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
						continue;
					}
					else if ( n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_APPROVER)  && n.getMsgMapper().getMsgById().equals(RoleUtil.getCurrentUseInfo().getId())  ) {
						itr.remove();
						continue;
					}
					
				}
				
				else if ((RoleUtil.getCurrentUseInfo().getApproverType() - 1) == n.getProjectStatus()) {
					n.setMsg(MessageConstants.APPROVAL_PENDING_FROM_YOU);
				}

				else if ((RoleUtil.getCurrentUseInfo().getApproverType() - 1) > n.getProjectStatus()
						|| (RoleUtil.getCurrentUseInfo().getApproverType()) == n.getProjectStatus()) {

					if (RoleUtil.getCurrentUseInfo().getApproverType() == Constants.STATUS_APPROVER_1)
						n.setUpdates(MessageConstants.APPROVAL_PENDING_FROM + " "
								+ userDao.findOneById(n.getApproverId()).getFullName());
				}

				else if (n.getProjectStatus() == Constants.STATUS_REJECTED) {
					if ((n.getActionByType()) == (RoleUtil.getCurrentUseInfo().getApproverType()))
						n.setUpdates(MessageConstants.REJECTED_BY + " by you");
					else if ((n.getActionByType()) < (RoleUtil.getCurrentUseInfo().getApproverType()))
						itr.remove();
					else if ((n.getActionByType()) > (RoleUtil.getCurrentUseInfo().getApproverType()))
						n.setUpdates(MessageConstants.REJECTED_BY + " by "
								+ userDao.findOneById(n.getCreatedBy()).getFullName());
				}

				else if (n.getProjectStatus() == Constants.STATUS_ARCHIVED  && (userDao.findOneById(n.getCreatedBy()).getApproverType() >= RoleUtil.getCurrentUseInfo().getApproverType())) {
					n.setUpdates(MessageConstants.REPORT_CANCELLED);
				}

				else if (n.getProjectStatus() == Constants.STATUS_ACCEPTED)
					n.setUpdates(MessageConstants.APPROVAL_ACCEPTED);
				else
					itr.remove();
			
			}
			
			//Post-Sign
			else if(n.getFeasibilityState().equals(Constants.POST_SIGNING) && RoleUtil.getCurrentUseInfo().getApproverType() <= Constants.STATUS_APPROVER_4) {
				
				 if (!ObjectUtils.isEmpty(n.getMsgMapper())  && ( !n.getMsgMapper().isProjectChat() && !n.getMsgMapper().isOperationChat())) {

					
					/*if (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && (n.getProjectStatus() == RoleUtil.getCurrentUseInfo().getApproverType()-1)) {
						n.setMsg(MessageConstants.QUERY_ANSWERED + " "+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
						continue;
					}*/
				  	if (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && (n.getProjectStatus() == RoleUtil.getCurrentUseInfo().getApproverType()-1)) {
							n.setMsg(MessageConstants.QUERY_ANSWERED + " "+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
							continue;
						}
				  	else if (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && (n.getProjectStatus() > RoleUtil.getCurrentUseInfo().getApproverType()-1)) {
							n.setUpdates(MessageConstants.QUERY_ANSWERED + " "+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
							continue;
					}
					 
					if (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && (n.getProjectStatus() > RoleUtil.getCurrentUseInfo().getApproverType()-1)) {
						n.setUpdates(MessageConstants.QUERY_ANSWERED + " "+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
						continue;
					}
					else if ( n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_APPROVER)  && !n.getMsgMapper().getMsgById().equals(RoleUtil.getCurrentUseInfo().getId())  && (n.getProjectStatus() >= RoleUtil.getCurrentUseInfo().getApproverType())) {
						n.setUpdates(MessageConstants.PENDING_QUERY + " "
								+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
						continue;
					}
					else if ( n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_APPROVER)  && n.getMsgMapper().getMsgById().equals(RoleUtil.getCurrentUseInfo().getId())  ) {
						itr.remove();
						continue;
					}
					
				}
				else if ((RoleUtil.getCurrentUseInfo().getApproverType() - 1) == n.getProjectStatus()) {
					n.setMsg(MessageConstants.APPROVAL_PENDING_FROM_YOU);
				}

				
				else if ((RoleUtil.getCurrentUseInfo().getApproverType() - 1) <= n.getProjectStatus() &&
						(n.getProjectStatus() < Constants.STATUS_ACCEPTED) && 
						(RoleUtil.getCurrentUseInfo().getApproverType() < Constants.STATUS_APPROVER_4)) {
					
						n.setUpdates(MessageConstants.APPROVAL_PENDING_FROM + " "+ userDao.findOneById(n.getApproverId()).getFullName());
				}
				

				else if (n.getProjectStatus() == Constants.POST_STATUS_REJECTED) {
					
					if ((n.getActionByType()) == (RoleUtil.getCurrentUseInfo().getApproverType()))
						n.setUpdates(MessageConstants.REJECTED_BY + " by you");
					else if ((n.getActionByType()) < (RoleUtil.getCurrentUseInfo().getApproverType()))
						itr.remove();
					else if ((n.getActionByType()) > (RoleUtil.getCurrentUseInfo().getApproverType()))
						n.setUpdates(MessageConstants.REJECTED_BY + " by "+ userDao.findOneById(n.getCreatedBy()).getFullName());
					
				}
				

				else if (n.getProjectStatus() == Constants.POST_STATUS_ARCHIVED  && (userDao.findOneById(n.getCreatedBy()).getApproverType() >= RoleUtil.getCurrentUseInfo().getApproverType()))
					n.setUpdates(MessageConstants.REPORT_CANCELLED);
				else if (n.getProjectStatus() == Constants.STATUS_ACCEPTED)
					n.setUpdates(MessageConstants.APPROVAL_ACCEPTED);
				else
					itr.remove();
				
			}
			
			//Pre Hand over
			else if(n.getFeasibilityState().equals(Constants.PRE_HANDOVER) && RoleUtil.getCurrentUseInfo().getApproverType() <= Constants.STATUS_APPROVER_6) {
				
				 if (!ObjectUtils.isEmpty(n.getMsgMapper())  && ( !n.getMsgMapper().isProjectChat() && !n.getMsgMapper().isOperationChat())) {

						
						if (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && (n.getProjectStatus() == RoleUtil.getCurrentUseInfo().getApproverType()-1)) {
								n.setMsg(MessageConstants.QUERY_ANSWERED + " "+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
								continue;
						}
						else if (n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && (n.getProjectStatus() > RoleUtil.getCurrentUseInfo().getApproverType()-1)) {
								n.setUpdates(MessageConstants.QUERY_ANSWERED + " "+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
								continue;
						}
							
						else if ( n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_APPROVER)  && !n.getMsgMapper().getMsgById().equals(RoleUtil.getCurrentUseInfo().getId())  && (n.getProjectStatus() >= RoleUtil.getCurrentUseInfo().getPreHandOverApproverType())) {
							n.setUpdates(MessageConstants.PENDING_QUERY + " "
									+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
							continue;
						}
						else if ( n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_APPROVER)  && n.getMsgMapper().getMsgById().equals(RoleUtil.getCurrentUseInfo().getId())  ) {
							itr.remove();
							continue;
						}
						
					}
					else if ((RoleUtil.getCurrentUseInfo().getPreHandOverApproverType() - 1) == n.getProjectStatus()) {
						n.setMsg(MessageConstants.APPROVAL_PENDING_FROM_YOU);
					}

					
					else if ((RoleUtil.getCurrentUseInfo().getPreHandOverApproverType() - 1) <= n.getProjectStatus() &&
							(n.getProjectStatus() < Constants.STATUS_ACCEPTED) && 
							(RoleUtil.getCurrentUseInfo().getPreHandOverApproverType() < Constants.STATUS_APPROVER_6)) {
						
							n.setUpdates(MessageConstants.APPROVAL_PENDING_FROM + " "+ userDao.findOneById(n.getApproverId()).getFullName());
					}
					

					else if (n.getProjectStatus() == Constants.PRE_HAND_STATUS_REJECTED) {
						
						if ((n.getActionByType()) == (RoleUtil.getCurrentUseInfo().getPreHandOverApproverType()))
							n.setUpdates(MessageConstants.REJECTED_BY + " by you");
						else if ((n.getActionByType()) < (RoleUtil.getCurrentUseInfo().getPreHandOverApproverType()))
							itr.remove();
						else if ((n.getActionByType()) > (RoleUtil.getCurrentUseInfo().getPreHandOverApproverType()))
							n.setUpdates(MessageConstants.REJECTED_BY + " by "+ userDao.findOneById(n.getCreatedBy()).getFullName());
						
					}
					

					else if (n.getProjectStatus() == Constants.PRE_HAND_STATUS_ARCHIVED  && (userDao.findOneById(n.getCreatedBy()).getApproverType() >= RoleUtil.getCurrentUseInfo().getApproverType()))
						n.setUpdates(MessageConstants.REPORT_CANCELLED);
					else if (n.getProjectStatus() == Constants.STATUS_ACCEPTED)
						n.setUpdates(MessageConstants.APPROVAL_ACCEPTED);
					else
						itr.remove();
				
			}
			
			else
			{
				itr.remove();
			}

		}
	}
	
	public void addMessageForOperation(Notification n, Iterator<Notification> itr)
	{
		while (itr.hasNext()) {
			n = itr.next();
			n.setRole(Constants.ROLE_OPERATION_TEAM);

			//chat active
			 if(!ObjectUtils.isEmpty(n.getMsgMapper()) && (n.getMsgMapper().isProjectChat() || n.getMsgMapper().isOperationChat())){
				
				if ( n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && n.getMsgMapper().isOperationChat()) {
					n.setMsg(MessageConstants.PENDING_QUERY + " "
							+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
					continue;
				}
				else if((n.getProjectStatus() <= Constants.STEP_6 && n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && n.getMsgMapper().isOperationChat())) {
					n.setUpdates(MessageConstants.PENDING_FROM_BD+userDao.findById(n.getProjectBdId()).get().getFullName());
					continue;
				}
				
				
				
			}
			
			if (n.getProjectStatus() <= Constants.STEP_6 && n.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
				
				if (  ObjectUtils.isEmpty(n.getMsgMapper())  || (!ObjectUtils.isEmpty(n.getMsgMapper()) &&  n.getMsgMapper().isProjectChat() )) {
					if (!n.getIsOperationSubmit() && !n.getIsProjectSubmit())
						n.setMsg(MessageConstants.PENDING_FROM_YOU_PROJECT_AND_OPERATION);
					else if (!n.getIsOperationSubmit())
						n.setMsg(MessageConstants.PENDING_FROM_YOU_PROJECT_AND_OPERATION);
					else if (!n.getIsProjectSubmit())
						n.setUpdates(MessageConstants.PENDING_FROM_PROJECT);
					else
						n.setUpdates(MessageConstants.PENDING_FROM_BD+userDao.findById(n.getProjectBdId()).get().getFullName());
				}

			}
			
			//flow after submit step6
			else if(n.getProjectStatus() >= Constants.STEP_6 && n.getFeasibilityState().equals(Constants.PRE_HANDOVER)){

				
				if (n.getProjectStatus() == Constants.PRE_HAND_STATUS_REJECTED) {

					String rejectedBy = userDao.findOneByApproverType(n.getActionByType()).getFullName();
					
					n.setUpdates(MessageConstants.REJECTED_BY +" by "+rejectedBy);

				} 
				
				else if (n.getProjectStatus() == Constants.STATUS_ACCEPTED)
					n.setUpdates(MessageConstants.APPROVAL_ACCEPTED);
				
				else if(n.getProjectStatus() >= Constants.STATUS_SEND_FOR_APPROVAL) {
					
					switch(n.getProjectStatus()) {
						case Constants.STATUS_SEND_FOR_APPROVAL:{
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getFullName());
							break;
						}
						case Constants.STATUS_APPROVER_1: {
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).getFullName());
							break;
						}
						case Constants.STATUS_APPROVER_2: {
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_5).getFullName());
							break;
						}
						case Constants.STATUS_APPROVER_3: {
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_6).getFullName());
							break;
						}
						case Constants.STATUS_APPROVER_4: {
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_3).getFullName());
							break;
						}
						case Constants.STATUS_APPROVER_5: {
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_4).getFullName());
						}
					}
					
				}
				else
					itr.remove();
				
			}
			
			
			

		}
	}
	
	
	public void addMessageForProject(Notification n, Iterator<Notification> itr)
	{
		while (itr.hasNext()) {
			n = itr.next();
			n.setRole(Constants.ROLE_PROJECT_TEAM);
			
			if(!ObjectUtils.isEmpty(n.getMsgMapper()) && (n.getMsgMapper().isProjectChat() || n.getMsgMapper().isOperationChat())) {
				if ( n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && n.getMsgMapper().isProjectChat()) {
					n.setMsg(MessageConstants.PENDING_QUERY + " "
							+ userDao.findById(n.getMsgMapper().getMsgById()).get().getFullName());
					continue;
				}
				else if(n.getProjectStatus() <= Constants.STEP_6 && n.getMsgMapper().getMsgByRole().equals(Constants.ROLE_BD) && n.getMsgMapper().isOperationChat()) {
					n.setUpdates(MessageConstants.PENDING_FROM_BD+userDao.findById(n.getProjectBdId()).get().getFullName());
					continue;
				}
				
			}
			
			if (n.getProjectStatus() <= Constants.STEP_6 && n.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
				
				if (  ObjectUtils.isEmpty(n.getMsgMapper()) || (!ObjectUtils.isEmpty(n.getMsgMapper()) &&  n.getMsgMapper().isOperationChat() ) ) {
					if (!n.getIsOperationSubmit() && !n.getIsProjectSubmit())
						n.setMsg(MessageConstants.PENDING_FROM_YOU_PROJECT_AND_OPERATION);
					else if (!n.getIsOperationSubmit())
						n.setUpdates(MessageConstants.PENDING_FROM_OPERATION);
					else if (!n.getIsProjectSubmit())
						n.setMsg(MessageConstants.PENDING_FROM_YOU_PROJECT_AND_OPERATION);
					else
						n.setUpdates(MessageConstants.PENDING_FROM_BD+userDao.findById(n.getProjectBdId()).get().getFullName());
				}

			}
			//flow after submit step6
			else if(n.getProjectStatus() >= Constants.STEP_6 && n.getFeasibilityState().equals(Constants.PRE_HANDOVER)){

				
				if (n.getProjectStatus() == Constants.PRE_HAND_STATUS_REJECTED) {

					String rejectedBy = userDao.findOneByApproverType(n.getActionByType()).getFullName();
					
					n.setUpdates(MessageConstants.REJECTED_BY +" by "+rejectedBy);

				} 
				
				else if (n.getProjectStatus() == Constants.STATUS_ACCEPTED)
					n.setUpdates(MessageConstants.APPROVAL_ACCEPTED);
				
				else if(n.getProjectStatus() >= Constants.STATUS_SEND_FOR_APPROVAL) {
					
					switch(n.getProjectStatus()) {
						case Constants.STATUS_SEND_FOR_APPROVAL:{
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getFullName());
							break;
						}
						case Constants.STATUS_APPROVER_1: {
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).getFullName());
							break;
						}
						case Constants.STATUS_APPROVER_2: {
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_5).getFullName());
							break;
						}
						case Constants.STATUS_APPROVER_3: {
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_6).getFullName());
							break;
						}
						case Constants.STATUS_APPROVER_4: {
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_3).getFullName());
							break;
						}
						case Constants.STATUS_APPROVER_5: {
							n.setUpdates(MessageConstants.BD_PENDING_FOR_APPROVAL + " from "+userDao.findOneByApproverType(Constants.STATUS_APPROVER_4).getFullName());
						}
					}
					
				}
				else
					itr.remove();
				
			}

		}
	}
	

}
