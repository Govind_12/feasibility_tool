package com.api.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.api.dao.CinemaFormatMasterDao;
import com.api.dao.CinemaMasterDao;
import com.api.dao.FixedConstantDao;
import com.api.dao.ProjectDetailsDao;
import com.api.dao.RevenueExpenseDao;
import com.api.dao.sql.CinemaMasterSqlDao;
import com.api.dao.sql.CinemaPerformanceDao;
import com.api.dao.sql.SeatCategoryDao;
import com.api.dao.sql.TicketOnlineOfflineDao;
import com.api.exception.FeasibilityException;
import com.api.service.report.RevenueDataService;
import com.api.utils.MathUtil;
import com.common.constants.Constants;
import com.common.mappers.BenchmarkCinema;
import com.common.models.AssumptionsValues;
import com.common.models.AtpAssumptions;
import com.common.models.AtpCapDetials;
import com.common.models.CinemaFormat;
import com.common.models.CinemaFormatMaster;
import com.common.models.CinemaMaster;
import com.common.models.FixedConstant;
import com.common.models.Navision;
import com.common.models.ProjectDetails;
import com.common.models.RevenueExpense;
import com.common.models.SeatType;
import com.common.sql.models.CinemaMasterSql;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Service("projectRevenueAnalysisService")
@Slf4j
public class ProjectRevenueAnalysisServiceImpl implements ProjectRevenueAnalysisService {

	@Autowired
	private CommonAnalysisService commonAnalsisService;

	@Autowired
	private CinemaMasterDao cinemaMasterDao;

	@Autowired
	private CinemaFormatMasterDao cinemaFormatMasterDao;

	@Autowired
	private ProjectDetailsDao projectDetailsDao;

	@Autowired
	private CinemaMasterSqlDao cinemaMasterSqlDao;

	@Autowired
	private SeatCategoryDao seatCategoryDao;

	@Autowired
	private CommonAnalysisService commonAnalysisService;

	@Autowired
	private RevenueExpenseDao revenueExpenseDao;

	@Autowired
	private FixedConstantDao fixedConstantDao;

	@Autowired
	private CinemaPerformanceDao cinemaPerformanceDao;

	@Autowired
	private TicketOnlineOfflineDao ticketOnlineOfflineDao;

	@Autowired
	private RevenueDataService revenueDataService;

	@Value("${google.api.key}")
	private String googleKey;

	@Override
	public double getProjectOccupancyBenchMarkCinemas(String projectId,
			Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		List<Navision> benchMarkCinemas = null;
		double occupancyMainstream = 0;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}

		try {

			if (projectDetails.isPresent()) {
//				final List<Navision> lastQuarterNonCompDataFinal =commonAnalysisService.getAllNavisionByComp();

				benchMarkCinemas = compareOccupancyBenchMarkCinemas(projectDetails.get(),lastQuarterNonCompDataFinal);
				Set<String> navCode = benchMarkCinemas.stream().map(Navision::getNavCinemaCode)
						.collect(Collectors.toSet());

				List<String> cinemaFormat = commonAnalsisService.getCinemaFormatlist(Constants.MAINSTREAM);

				/*
				 * List<Object[]> seatCategory =
				 * seatCategoryDao.findAllByCinemaHopkInAndCinemaFormat(
				 * getAllHopkByNavCode(navCode, Constants.MAINSTREAM), cinemaFormat,
				 * DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
				 * DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));
				 */

				List<Object[]> seatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormat(
						getAllHopkByNavCode(navCode, Constants.MAINSTREAM), cinemaFormat);

				occupancyMainstream = screenTypeOccupancy(getAllHopkByNavCode(navCode, Constants.MAINSTREAM),
						seatCategory, projectDetails.get().getCinemaFormat());

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching upcoming project occupancy");

		}
		return Math.round(occupancyMainstream * 10) / 10.0;
	}

	List<Navision> occupancyListFirst(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		List<Navision> filterCategoryInput = null;
		try {
//			List<Navision> filterCompDetails = commonAnalsisService.getAllNavisionByComp();
			List<Navision> filterCompDetails = lastQuarterNonCompDataFinal;
			List<Navision> filterByCityInput = filterCompDetails.stream()
					.filter(predicate -> !ObjectUtils.isEmpty(predicate.getCity())
							? predicate.getCity().equals(projectDetails.getProjectLocation().getCity())
							: false)
					.collect(Collectors.toList());

			filterCategoryInput = commonAnalsisService.filterBycategory(filterByCityInput,
					projectDetails.getCinemaCategory());

		} catch (Exception e) {
			log.error("error while searching benchmark cinemas first list", e);

		}
		return filterCategoryInput;
	}

	List<Navision> occupancyListSecond(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		List<Navision> filterByCityTierInput = null;
		try {
//			List<Navision> filterCompDetails = commonAnalsisService.getAllNavisionByComp();
			List<Navision> filterCompDetails = lastQuarterNonCompDataFinal;
			List<Navision> filterCategoryInput = commonAnalsisService.filterBycategory(filterCompDetails,
					projectDetails.getCinemaCategory());
			List<Navision> filterByStateInput = filterCategoryInput.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());
			filterByCityTierInput = commonAnalsisService.getByCityFilterRevenueExpense(filterByStateInput,
					projectDetails.getProjectLocation().getCityTier());

		} catch (Exception e) {
			log.error("error while searching benchmark cinemas second list", e);

		}
		return filterByCityTierInput;
	}

	List<Navision> occupancyListThird(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		List<Navision> filterCategoryInput = null;
		try {
//			List<Navision> filterCompDetails = commonAnalsisService.getAllNavisionByComp();
			List<Navision> filterCompDetails = lastQuarterNonCompDataFinal;
			List<Navision> filterByCityInput = filterCompDetails.stream()
					.filter(predicate -> !ObjectUtils.isEmpty(predicate.getCity())
							? predicate.getCity().equals(projectDetails.getProjectLocation().getCity())
							: false)
					.collect(Collectors.toList());
			filterCategoryInput = commonAnalsisService.filterRevenueCategory(filterByCityInput,
					projectDetails.getCinemaCategory());

		} catch (Exception e) {
			log.error("error while searching benchmark cinemas third list", e);

		}
		return filterCategoryInput;
	}

	List<Navision> occupancyListFour(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		List<Navision> filterByCityTierInput = null;
		try {
//			List<Navision> filterCompDetails = commonAnalsisService.getAllNavisionByComp();
			List<Navision> filterCompDetails = lastQuarterNonCompDataFinal;
			List<Navision> filterCategoryInput = commonAnalsisService.filterRevenueCategory(filterCompDetails,
					projectDetails.getCinemaCategory());
			List<Navision> filterByStateInput = filterCategoryInput.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());
			filterByCityTierInput = commonAnalsisService.getByCityFilterRevenueExpense(filterByStateInput,
					projectDetails.getProjectLocation().getCityTier());

		} catch (Exception e) {
			log.error("error while searching benchmark cinemas four list", e);

		}
		return filterByCityTierInput;
	}

	List<Navision> occupancyListFive(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		List<Navision> filterByStateInput = null;
		try {
//			List<Navision> filterCompDetails = commonAnalsisService.getAllNavisionByComp();
			List<Navision> filterCompDetails = lastQuarterNonCompDataFinal;
			List<Navision> filterCategoryInput = commonAnalsisService.filterBycategory(filterCompDetails,
					projectDetails.getCinemaCategory());
			filterByStateInput = filterCategoryInput.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());

		} catch (Exception e) {
			log.error("error while searching benchmark cinemas five list", e);
		}
		return filterByStateInput;
	}

	List<Navision> occupancyListSix(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		List<Navision> filterByStateInput = null;
		try {
//			List<Navision> filterCompDetails = commonAnalsisService.getAllNavisionByComp();
			List<Navision> filterCompDetails = lastQuarterNonCompDataFinal;
			List<Navision> filterCategoryInput = commonAnalsisService.filterRevenueCategory(filterCompDetails,
					projectDetails.getCinemaCategory());
			filterByStateInput = filterCategoryInput.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());

		} catch (Exception e) {
			log.error("error while searching benchmark cinemas six list", e);

		}
		return filterByStateInput;
	}

	List<Navision> occupancyListSeven(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		List<Navision> filterByCityTierInput = null;
		try {
//			List<Navision> filterCompDetails = commonAnalsisService.getAllNavisionByComp();
			List<Navision> filterCompDetails = lastQuarterNonCompDataFinal;
			List<Navision> filterCategoryInput = commonAnalsisService.filterBycategory(filterCompDetails,
					projectDetails.getCinemaCategory());
			filterByCityTierInput = commonAnalsisService.getByCityFilterRevenueExpense(filterCategoryInput,
					projectDetails.getProjectLocation().getCityTier());

		} catch (Exception e) {
			log.error("error while searching benchmark cinemas seven list", e);

		}
		return filterByCityTierInput;
	}

	List<Navision> occupancyListEight(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		List<Navision> filterByCityTierInput = null;
		try {
//			List<Navision> filterCompDetails = commonAnalsisService.getAllNavisionByComp();
			List<Navision> filterCompDetails = lastQuarterNonCompDataFinal;
			List<Navision> filterCategoryInput = commonAnalsisService.filterRevenueCategory(filterCompDetails,
					projectDetails.getCinemaCategory());
			filterByCityTierInput = commonAnalsisService.getByCityFilterRevenueExpense(filterCategoryInput,
					projectDetails.getProjectLocation().getCityTier());

		} catch (Exception e) {
			log.error("error while searching benchmark cinemas eight list", e);

		}
		return filterByCityTierInput;
	}

	public List<Navision> compareOccupancyBenchMarkCinemas(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {

		List<Navision> listFirst = occupancyListFirst(projectDetails, lastQuarterNonCompDataFinal);
		List<Navision> navision = new ArrayList<>();

		Set<String> navCode = null;
		navCode = listFirst.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
		if (navCode.size() >= 3) {
			navision = listFirst;
		} else {
			List<Navision> listSecond = occupancyListSecond(projectDetails,lastQuarterNonCompDataFinal);
			listSecond.addAll(!ObjectUtils.isEmpty(listFirst) ? listFirst : new ArrayList<>());
			navCode = listSecond.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
			if (navCode.size() >= 3) {
				navision = listSecond;
			} else {
				List<Navision> listThird = occupancyListThird(projectDetails, lastQuarterNonCompDataFinal);
				listThird.addAll(!ObjectUtils.isEmpty(listSecond) ? listSecond : new ArrayList<>());
				navCode = listThird.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
				if (navCode.size() >= 3) {
					navision = listThird;
				} else {
					List<Navision> listFour = occupancyListFour(projectDetails,lastQuarterNonCompDataFinal);
					listFour.addAll(!ObjectUtils.isEmpty(listThird) ? listThird : new ArrayList<>());
					navCode = listFour.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
					if (navCode.size() >= 3) {
						navision = listFour;
					} else {
						List<Navision> listFive = occupancyListFive(projectDetails,lastQuarterNonCompDataFinal);
						listFive.addAll(!ObjectUtils.isEmpty(listFour) ? listFour : new ArrayList<>());
						navCode = listFive.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
						if (navCode.size() >= 3) {
							navision = listFive;
						} else {
							List<Navision> listSix = occupancyListSix(projectDetails,lastQuarterNonCompDataFinal);
							listSix.addAll(!ObjectUtils.isEmpty(listFive) ? listFive : new ArrayList<>());
							navCode = listSix.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
							if (navCode.size() >= 3) {
								navision = listSix;
							} else {
								List<Navision> listSeven = occupancyListSeven(projectDetails,lastQuarterNonCompDataFinal);
								listSeven.addAll(!ObjectUtils.isEmpty(listSix) ? listSix : new ArrayList<>());
								navCode = listSeven.stream().map(Navision::getNavCinemaCode)
										.collect(Collectors.toSet());
								if (navCode.size() >= 3) {
									navision = listSeven;
								} else {
									List<Navision> listEight = occupancyListEight(projectDetails,lastQuarterNonCompDataFinal);
									listSeven.addAll(!ObjectUtils.isEmpty(listSeven) ? listSeven : new ArrayList<>());
									navCode = listEight.stream().map(Navision::getNavCinemaCode)
											.collect(Collectors.toSet());
									if (navCode.size() >= 3) {
										navision = listEight;
									} else {
										navision = listEight;
									}
								}
							}
						}
					}
				}
			}
		}
//		System.out.println(new Gson().toJson(navision));
		if (navCode.size() > 3) {
			navision = getLatLongCalculated(projectDetails, navision);
		}
//		System.out.println(new Gson().toJson(navision));

		/*
		 * Set<String> navCodes =
		 * navision.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet())
		 * ;
		 * 
		 * List<Navision> list = null; for (String nav : navCodes) { list =
		 * navision.stream().filter(pre -> pre.getNavCinemaCode().equalsIgnoreCase(nav))
		 * .collect(Collectors.toList()); System.out.println(list); }
		 */

		return navision;
	}

	public double screenTypeOccupancy(List<Integer> hopk, List<Object[]> seatCategory,
			List<CinemaFormat> cinemaFormat) {
		List<Map<Integer, Double>> occupancy = new ArrayList<>();
		List<Map<Integer, Long>> seatshopk = new ArrayList<>();

		try {

			hopk.forEach(a -> {
				Map<Integer, Double> map = new HashMap<>();
				Map<Integer, Long> totalSeatsHopk = new HashMap<>();
				List<Object[]> seatCategoryByhopk = seatCategory.stream()
						.filter(predicate -> (int) predicate[Constants.HOPK] == a).collect(Collectors.toList());

				long totalSeats = seatCategoryByhopk.stream()
						.mapToLong(mapper -> getMapperValueTotalSeats(mapper[Constants.SEAT_CAT_ADMITS])).sum();
				totalSeatsHopk.put(a, totalSeats);
				seatshopk.add(totalSeatsHopk);
				double occupancyPerHopk = Math.round((calculateOccupancy(seatCategoryByhopk) * 100) * 10) / 10.0;

				map.put(a, occupancyPerHopk);
				occupancy.add(map);
			});

		} catch (Exception e) {
			log.error("error while analysis by upcoming cinema format type");

		}
		return calculateMainstreamOccupancy(occupancy, seatshopk);
	}

	public double calculateOccupancy(List<Object[]> seatCategoryByhopk) {
		double totalOccupancy = 0;
		try {
			long totalAdmits = seatCategoryByhopk.stream()
					.mapToLong(mapper -> getMapperValueAdmits(mapper[Constants.SEAT_CAT_ADMITS])).sum();
			long totalSeats = seatCategoryByhopk.stream()
					.mapToLong(mapper -> getMapperValueTotalSeats(mapper[Constants.SEAT_CAT_TOTALSEATS])).sum();
			totalOccupancy = (double) totalAdmits / (double) totalSeats;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating occupancy by each hopk...");
		}
		return totalOccupancy;
	}

	/*
	 * public Map<String, Date> getDateByCurrentQuater() { Map<String, Date> map =
	 * new HashMap<>(); String currentQuarter = getCurrentQuater();
	 * 
	 * if (currentQuarter.equals("Q1")) {
	 * 
	 * Calendar calender = Calendar.getInstance();
	 * calender.set(Calendar.getInstance().get(Calendar.YEAR) - 1, 3, 1); Date d1 =
	 * calender.getTime();
	 * 
	 * Calendar calender1 = Calendar.getInstance();
	 * calender1.set(Calendar.getInstance().get(Calendar.YEAR), 2, 31); Date d2 =
	 * calender1.getTime();
	 * 
	 * map.put(Constants.STARTDATE, d1); map.put(Constants.ENDDATE, d2); } else if
	 * (currentQuarter.equals("Q2")) {
	 * 
	 * Calendar calender = Calendar.getInstance();
	 * 
	 * calender.set(Calendar.getInstance().get(Calendar.YEAR) - 1, 6, 1); Date d1 =
	 * calender.getTime();
	 * 
	 * Calendar calender1 = Calendar.getInstance();
	 * calender1.set(Calendar.getInstance().get(Calendar.YEAR), 5, 30); Date d2 =
	 * calender1.getTime();
	 * 
	 * map.put(Constants.STARTDATE, d1); map.put(Constants.ENDDATE, d2); } else if
	 * (currentQuarter.equals("Q3")) { Calendar calender = Calendar.getInstance();
	 * calender.set(Calendar.getInstance().get(Calendar.YEAR) - 1, 9, 1); Date d1 =
	 * calender.getTime();
	 * 
	 * Calendar calender1 = Calendar.getInstance();
	 * calender1.set(Calendar.getInstance().get(Calendar.YEAR), 8, 30); Date d2 =
	 * calender1.getTime();
	 * 
	 * map.put(Constants.STARTDATE, d1); map.put(Constants.ENDDATE, d2); } else if
	 * (currentQuarter.equals("Q4")) { Calendar calender = Calendar.getInstance();
	 * calender.set(Calendar.getInstance().get(Calendar.YEAR) - 1, 0, 1); Date d1 =
	 * calender.getTime();
	 * 
	 * Calendar calender1 = Calendar.getInstance();
	 * calender1.set(Calendar.getInstance().get(Calendar.YEAR), 11, 31); Date d2 =
	 * calender1.getTime();
	 * 
	 * map.put(Constants.STARTDATE, d1); map.put(Constants.ENDDATE, d2); }
	 * 
	 * return map; }
	 */

	public long getMapperValueAdmits(Object arr) {
		long admits = 0;
		if (!ObjectUtils.isEmpty(arr)) {
			admits = ((Number) arr).longValue();
		}
		return admits;
	}

	public long getMapperValueTotalSeats(Object arr) {
		long totalSeats = 0;
		if (!ObjectUtils.isEmpty(arr)) {
			totalSeats = ((Number) arr).longValue();
		}
		return totalSeats;
	}

	public double calculateMainstreamOccupancy(List<Map<Integer, Double>> mapOccupancy,
			List<Map<Integer, Long>> mapSeat) {
		double weightOccupancyMainstream = 0;
		try {
			List<Double> totalhopkSeatOccupancy = new ArrayList<>();
			mapOccupancy.forEach(occupancy -> {
				mapSeat.forEach(seats -> {
					occupancy.forEach((occupancuKey, occupancyValue) -> {
						seats.forEach((seatKey, seatsValue) -> {
							if (occupancuKey == seatKey) {
								totalhopkSeatOccupancy.add(occupancyValue * seatsValue);
							}
						});
					});
				});
			});

			List<Long> seatsTotal = new ArrayList<>();
			mapSeat.forEach(map -> {
				map.forEach((k, v) -> {
					seatsTotal.add(v);
				});
			});
			long total = seatsTotal.stream().mapToLong(a -> a).sum();
			double totalOccupancyMultiplyTotalSeats = totalhopkSeatOccupancy.stream().mapToDouble(m -> m).sum();
			weightOccupancyMainstream = totalOccupancyMultiplyTotalSeats / (double) total;

		} catch (Exception e) {
			log.error("error while calculating mainstream occupancy ..", e);

		}
		return weightOccupancyMainstream;
	}

	@Override
	public List<RevenueExpense> getProjectUpcomingCinemasOccupancy(String projectId,
			Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		List<RevenueExpense> revenueExpense = null;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}

		try {
			if (projectDetails.isPresent()) {
				List<CinemaFormat> cinema = projectDetails.get().getCinemaFormat().stream().distinct()
						.collect(Collectors.toList());

				Set<String> screen = cinema.stream().map(CinemaFormat::getFormatType).collect(Collectors.toSet());
				List<CinemaFormat> cinemaFOrmatList = new ArrayList<>();

				screen.forEach(action -> {
					List<CinemaFormat> list = cinema.stream()
							.filter(predicate -> predicate.getFormatType().equals(action)).collect(Collectors.toList());
					CinemaFormat cinemaFormat = new CinemaFormat();
					cinemaFormat.setFormatType(list.get(0).getFormatType());
					cinemaFormat.setNormal(list.get(0).getNormal());
					cinemaFormat.setRecliners(list.get(0).getRecliners());
					cinemaFormat.setLounger(list.get(0).getLounger());
					cinemaFOrmatList.add(cinemaFormat);
				});

				/*
				 * revenueExpense = getOccupancyByScreenFormat(projectDetails, projectId,
				 * cinema);
				 */
				List<CinemaFormat>cinemaFormatList=getCinemaFormatList(projectDetails, projectId, cinemaFOrmatList);
				double ocupancyMainstream = getProjectOccupancyBenchMarkCinemas(null, projectDetails, lastQuarterNonCompDataFinal);
				revenueExpense = getOccupancyByScreenFormat(projectDetails, projectId, cinemaFormatList,ocupancyMainstream);

			}
		} catch (Exception e) {
			log.error("error while fetching upcoming cinemas occupancy", e);

		}
		return revenueExpense;
	}
	
	public List<CinemaFormat>getCinemaFormatList(Optional<ProjectDetails> upcomingProjectDetails,
			String projectId, List<CinemaFormat> cinemaFormat)
	{
	
		List<CinemaFormat> cinemaFOrmatList = new ArrayList<>();
		Optional<ProjectDetails> projectDetails = null;
		if (!ObjectUtils.isEmpty(projectId))
		{
			projectDetails = projectDetailsDao.findById(projectId);
		} 
		else
		{
			projectDetails = upcomingProjectDetails;

			Set<String> screen = projectDetails.get().getCinemaFormat().stream().map(CinemaFormat::getFormatType)
					.collect(Collectors.toSet());
			

			for (String action : screen)
			{
				List<CinemaFormat> list = projectDetails.get().getCinemaFormat().stream()
						.filter(predicate -> predicate.getFormatType().equals(action)).collect(Collectors.toList());
				CinemaFormat cinemaFormats = new CinemaFormat();
				cinemaFormats.setFormatType(list.get(0).getFormatType());
				cinemaFormats.setNormal(list.get(0).getNormal());
				cinemaFormats.setRecliners(list.get(0).getRecliners());
				cinemaFormats.setLounger(list.get(0).getLounger());
				cinemaFOrmatList.add(cinemaFormats);

			}
			
		}
		return cinemaFOrmatList;
		
	}
	
	public List<RevenueExpense> getOccupancyByScreenFormat(Optional<ProjectDetails> upcomingProjectDetails,
			String projectId, List<CinemaFormat> cinemaFormat,double ocupancyMainstream) {

		List<RevenueExpense> revenueExpense = new ArrayList<>();

		/*
		 * Optional<ProjectDetails> projectDetails = null;
		 * 
		 * if (!ObjectUtils.isEmpty(projectId)) { projectDetails =
		 * projectDetailsDao.findById(projectId); } else { projectDetails =
		 * upcomingProjectDetails;
		 * 
		 * Set<String> screen =
		 * projectDetails.get().getCinemaFormat().stream().map(CinemaFormat::
		 * getFormatType) .collect(Collectors.toSet()); List<CinemaFormat>
		 * cinemaFOrmatList = new ArrayList<>();
		 * 
		 * for (String action : screen) { List<CinemaFormat> list =
		 * projectDetails.get().getCinemaFormat().stream() .filter(predicate ->
		 * predicate.getFormatType().equals(action)).collect(Collectors.toList());
		 * CinemaFormat cinemaFormats = new CinemaFormat();
		 * cinemaFormats.setFormatType(list.get(0).getFormatType());
		 * cinemaFormats.setNormal(list.get(0).getNormal());
		 * cinemaFormats.setRecliners(list.get(0).getRecliners());
		 * cinemaFormats.setLounger(list.get(0).getLounger());
		 * cinemaFOrmatList.add(cinemaFormats);
		 * 
		 * } cinemaFormat = cinemaFOrmatList;
		 * 
		 * }
		 * 
		 * double ocupancyMainstream = getProjectOccupancyBenchMarkCinemas(null,
		 * projectDetails);
		 */
		Iterator<CinemaFormat> itr=cinemaFormat.iterator();
		CinemaFormat cf=null;
		while(itr.hasNext())
		{	
			cf=itr.next();
			RevenueExpense rv = new RevenueExpense();
			double finalOccupancy = finalCalculationByScreenOccupancy(cf.getFormatType());

			rv.setCinemaFormatType(cf.getFormatType());

			if (finalOccupancy == 0.0) {
				rv.setOccupancy(1 * ocupancyMainstream);
			} else {
				rv.setOccupancy(Math.round(((double) finalOccupancy * ocupancyMainstream) * 10) / 10.0);
			}
			Double occupancy = Math.round(((double) finalOccupancy * ocupancyMainstream) * 10) / 10.0;
			SeatType admits = calculateAdmitsByScreen(upcomingProjectDetails, cf.getFormatType(), projectId,
					occupancy == 0.0 ? ocupancyMainstream : occupancy);
			rv.setAdmits(admits);
			revenueExpense.add(rv);
			revenueExpenseDao.save(rv);

		}
		return revenueExpense;
	}

	public Double finalCalculationByScreenOccupancy(String screenType) {
		Double filterByScreenType = getSeatCategoryByScreen(screenType);
		return filterByScreenType;
	}

	public Double getSeatCategoryByScreen(String ScreenType) {
		Double seat = 0.0;
		List<Navision> filterByComp = commonAnalsisService.getAllNavisionByComp();
		Set<String> navCode = filterByComp.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
		List<Integer> hp = getAllHopkByNavCode(navCode, ScreenType);

		List<String> cinemaFormat = commonAnalsisService.getCinemaFormatlist(ScreenType);

		if (!ObjectUtils.isEmpty(hp)) {
			/*
			 * List<Object[]> filterBySeatCategory =
			 * seatCategoryDao.findAllByCinemaHopkInAndCinemaFormat(hp, cinemaFormat,
			 * DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
			 * DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));
			 */
			List<String> newCinemaFormatList = new ArrayList<>();
			for(String cinema: cinemaFormat) {
				if(cinema.contains("Luxe"))
				  newCinemaFormatList.add("Gold");
				else
				  newCinemaFormatList.add(cinema);
			}
			
			List<Object[]> filterBySeatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormat(hp,
					newCinemaFormatList);
			return getTotalOccupancy(filterBySeatCategory, hp);
		} else {
			return seat;
		}
	}

	List<CinemaMasterSql> screenType(String screen, List<String> code) {
		List<String> cinemas = new ArrayList<>();
		CinemaFormatMaster format = cinemaFormatMasterDao.findOneByCinemaCategory(screen);
		cinemas.add(screen);
		if (format.getCinemaFormatMapping() != null) {
			cinemas.addAll(format.getCinemaFormatMapping());
		}
		List<CinemaMasterSql> filterByScreenFormat = cinemaMasterSqlDao.findAllCinemaMasterByNavCode(code);
		List<CinemaMasterSql> filterbyScreen = new ArrayList<>();
		Set<CinemaMasterSql> filterbyScreenSet = new HashSet<>();

		if (cinemas.contains(Constants.GOLD) || cinemas.contains(Constants.LUXE)) {

			/*
			 * if (filterbyScreenSet.size() == 0) { List<CinemaMasterSql> filterbyScreenList
			 * = filterByScreenFormat.stream() .filter(predicate -> predicate.getGold() >
			 * 0).collect(Collectors.toList());
			 * //filterbyScreenList.forEach(action->System.out.println(action.
			 * getNav_cinemaCode())); if (filterbyScreenList != null &&
			 * filterbyScreenList.size() > 0) {
			 * filterbyScreenSet.addAll(filterbyScreenList); } } else {
			 * List<CinemaMasterSql> filterbyScreenList1 = filterbyScreenSet.stream()
			 * .filter(predicate -> predicate.getGold() > 0).collect(Collectors.toList());
			 * if (filterbyScreenList1 != null && filterbyScreenList1.size() > 0) {
			 * //filterbyScreenSet.clear(); filterbyScreenSet.addAll(filterbyScreenList1); }
			 * }
			 */

			List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
					.filter(predicate -> predicate.getGold() > 0).collect(Collectors.toList());
			if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
				filterbyScreenSet.addAll(filterbyScreenList);
			}
		}
		if (cinemas.contains(Constants.ULTRAPREMIUM)) {

			/*
			 * if (filterbyScreenSet.size() == 0) { List<CinemaMasterSql> filterbyScreenList
			 * = filterByScreenFormat.stream() .filter(predicate ->
			 * predicate.getUltraPremium() > 0).collect(Collectors.toList()); if
			 * (filterbyScreenList != null && filterbyScreenList.size() > 0) {
			 * filterbyScreenSet.addAll(filterbyScreenList); } } else {
			 * List<CinemaMasterSql> filterbyScreenList2 = filterbyScreenSet.stream()
			 * .filter(predicate -> predicate.getUltraPremium() >
			 * 0).collect(Collectors.toList()); if (filterbyScreenList2 != null &&
			 * filterbyScreenList2.size() > 0) { //filterbyScreenSet.clear();
			 * filterbyScreenSet.addAll(filterbyScreenList2); } }
			 */

			List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
					.filter(predicate -> predicate.getUltraPremium() > 0).collect(Collectors.toList());
			if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
				filterbyScreenSet.addAll(filterbyScreenList);
			}
		}

		if (cinemas.contains(Constants.MAINSTREAM)) {

			/*
			 * if (filterbyScreenSet.size() == 0) { List<CinemaMasterSql> filterbyScreenList
			 * = filterByScreenFormat.stream() .filter(predicate ->
			 * predicate.getMainstream() > 0).collect(Collectors.toList()); if
			 * (filterbyScreenList != null && filterbyScreenList.size() > 0) {
			 * filterbyScreenSet.addAll(filterbyScreenList); } } else {
			 * List<CinemaMasterSql> filterbyScreenList3 = filterbyScreenSet.stream()
			 * .filter(predicate -> predicate.getMainstream() >
			 * 0).collect(Collectors.toList()); if (filterbyScreenList3 != null &&
			 * filterbyScreenList3.size() > 0) { //filterbyScreenSet.clear();
			 * filterbyScreenSet.addAll(filterbyScreenList3); } }
			 */
			List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
					.filter(predicate -> predicate.getMainstream() > 0).collect(Collectors.toList());
			if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
				filterbyScreenSet.addAll(filterbyScreenList);
			}
		}

		if (cinemas.contains(Constants.PREMIER)) {

			/*
			 * if (filterbyScreenSet.size() == 0) { List<CinemaMasterSql> filterbyScreenList
			 * = filterByScreenFormat.stream() .filter(predicate -> predicate.getPremier() >
			 * 0).collect(Collectors.toList()); if (filterbyScreenList != null &&
			 * filterbyScreenList.size() > 0) {
			 * filterbyScreenSet.addAll(filterbyScreenList); } } else {
			 * List<CinemaMasterSql> filterbyScreenList4 = filterbyScreenSet.stream()
			 * .filter(predicate -> predicate.getPremier() >
			 * 0).collect(Collectors.toList()); if (filterbyScreenList4 != null &&
			 * filterbyScreenList4.size() > 0) { ///filterbyScreenSet.clear();
			 * filterbyScreenSet.addAll(filterbyScreenList4); } }
			 */
			List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
					.filter(predicate -> predicate.getPremier() > 0).collect(Collectors.toList());
			if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
				filterbyScreenSet.addAll(filterbyScreenList);
			}
		}

		if (cinemas.contains(Constants.DX)) {

			/*
			 * if (filterbyScreenSet.size() == 0) { List<CinemaMasterSql> filterbyScreenList
			 * = filterByScreenFormat.stream() .filter(predicate -> predicate.getDX() >
			 * 0).collect(Collectors.toList()); if (filterbyScreenList != null &&
			 * filterbyScreenList.size() > 0) {
			 * filterbyScreenSet.addAll(filterbyScreenList); } } else {
			 * List<CinemaMasterSql> filterbyScreenList5 = filterbyScreenSet.stream()
			 * .filter(predicate -> predicate.getDX() > 0).collect(Collectors.toList()); if
			 * (filterbyScreenList5 != null && filterbyScreenList5.size() > 0) {
			 * //filterbyScreenSet.clear(); filterbyScreenSet.addAll(filterbyScreenList5); }
			 * }
			 */

			List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
					.filter(predicate -> predicate.getDX() > 0).collect(Collectors.toList());
			if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
				filterbyScreenSet.addAll(filterbyScreenList);
			}
		}

		if (cinemas.contains(Constants.PXL)) {

			/*
			 * if (filterbyScreenSet.size() == 0) { List<CinemaMasterSql> filterbyScreenList
			 * = filterByScreenFormat.stream() .filter(predicate -> predicate.getPXL() >
			 * 0).collect(Collectors.toList()); if (filterbyScreenList != null &&
			 * filterbyScreenList.size() > 0) {
			 * filterbyScreenSet.addAll(filterbyScreenList); } } else {
			 * List<CinemaMasterSql> filterbyScreenList6 = filterbyScreenSet.stream()
			 * .filter(predicate -> predicate.getPXL() > 0).collect(Collectors.toList()); if
			 * (filterbyScreenList6 != null && filterbyScreenList6.size() > 0) {
			 * //filterbyScreenSet.clear(); filterbyScreenSet.addAll(filterbyScreenList6); }
			 * }
			 */

			List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
					.filter(predicate -> predicate.getPXL() > 0).collect(Collectors.toList());
			if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
				filterbyScreenSet.addAll(filterbyScreenList);
			}

		}

		if (cinemas.contains(Constants.IMAX)) {
			List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
					.filter(predicate -> predicate.getIMAX() > 0).collect(Collectors.toList());
			if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
				filterbyScreenSet.addAll(filterbyScreenList);
			}

			/*
			 * if (filterbyScreenSet.size() == 0) { List<CinemaMasterSql> filterbyScreenList
			 * = filterByScreenFormat.stream() .filter(predicate -> predicate.getIMAX() >
			 * 0).collect(Collectors.toList()); if (filterbyScreenList != null &&
			 * filterbyScreenList.size() > 0) {
			 * filterbyScreenSet.addAll(filterbyScreenList); } } else {
			 * List<CinemaMasterSql> filterbyScreenList7 = filterbyScreenSet.stream()
			 * .filter(predicate -> predicate.getIMAX() > 0).collect(Collectors.toList());
			 * if (filterbyScreenList7 != null && filterbyScreenList7.size() > 0) {
			 * //filterbyScreenSet.clear(); filterbyScreenSet.addAll(filterbyScreenList7); }
			 * }
			 */
		}

		if (cinemas.contains(Constants.PLAYHOUSE)) {
			List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
					.filter(predicate -> predicate.getPlayhouse() > 0).collect(Collectors.toList());
			if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
				filterbyScreenSet.addAll(filterbyScreenList);
			}

			/*
			 * if (filterbyScreenSet.size() == 0) { List<CinemaMasterSql> filterbyScreenList
			 * = filterByScreenFormat.stream() .filter(predicate -> predicate.getPlayhouse()
			 * > 0).collect(Collectors.toList()); if (filterbyScreenList != null &&
			 * filterbyScreenList.size() > 0) {
			 * filterbyScreenSet.addAll(filterbyScreenList); } } else {
			 * List<CinemaMasterSql> filterbyScreenList8 = filterbyScreenSet.stream()
			 * .filter(predicate -> predicate.getPlayhouse() >
			 * 0).collect(Collectors.toList()); if (filterbyScreenList8 != null &&
			 * filterbyScreenList8.size() > 0) { //filterbyScreenSet.clear();
			 * filterbyScreenSet.addAll(filterbyScreenList8); } }
			 */
		}

		if (cinemas.contains(Constants.ONYX)) {
			List<CinemaMasterSql> filterbyScreenList = filterByScreenFormat.stream()
					.filter(predicate -> predicate.getONYX() > 0).collect(Collectors.toList());
			if (filterbyScreenList != null && filterbyScreenList.size() > 0) {
				filterbyScreenSet.addAll(filterbyScreenList);
			}
			/*
			 * if (filterbyScreenSet.size() == 0) { List<CinemaMasterSql> filterbyScreenList
			 * = filterByScreenFormat.stream() .filter(predicate -> predicate.getONYX() >
			 * 0).collect(Collectors.toList());
			 * filterbyScreenList.forEach(action->System.out.println(action.
			 * getNav_cinemaCode())); if (filterbyScreenList != null &&
			 * filterbyScreenList.size() > 0) {
			 * filterbyScreenSet.addAll(filterbyScreenList); } } else {
			 * List<CinemaMasterSql> filterbyScreenList9 = filterbyScreenSet.stream()
			 * .filter(predicate -> predicate.getONYX() > 0).collect(Collectors.toList());
			 * filterbyScreenList9.forEach(action->System.out.println(action.
			 * getNav_cinemaCode())); if (filterbyScreenList9 != null &&
			 * filterbyScreenList9.size() > 0) { //filterbyScreenSet.clear();
			 * filterbyScreenSet.addAll(filterbyScreenList9); } }
			 */
		}
		/*
		 * if ((screen.equalsIgnoreCase(Constants.GOLD)) ||
		 * (screen.equalsIgnoreCase(Constants.ULTRAPREMIUM))) { cinemas.add("Gold");
		 * cinemas.add("UltraPremium"); filterbyScreen = filterByScreenFormat.stream()
		 * .filter(predicate -> predicate.getGold() > 0 || predicate.getUltraPremium() >
		 * 0) .collect(Collectors.toList()); } else if
		 * (screen.equalsIgnoreCase(Constants.MAINSTREAM) ||
		 * (screen.equalsIgnoreCase(Constants.PREMIER))) { cinemas.add("Mainstream");
		 * cinemas.add("Premier"); filterbyScreen = filterByScreenFormat.stream()
		 * .filter(predicate -> predicate.getMainstream() > 0 || predicate.getPremier()
		 * > 0) .collect(Collectors.toList()); } else if
		 * (screen.equalsIgnoreCase(Constants.DX)) { cinemas.add("4DX"); filterbyScreen
		 * = filterByScreenFormat.stream().filter(predicate -> predicate.getDX() > 0)
		 * .collect(Collectors.toList()); } else if
		 * (screen.equalsIgnoreCase(Constants.PXL)) { cinemas.add("P[XL]");
		 * filterbyScreen = filterByScreenFormat.stream().filter(predicate ->
		 * predicate.getPXL() > 0) .collect(Collectors.toList()); } else if
		 * (screen.equalsIgnoreCase(Constants.IMAX)) { cinemas.add("IMAX");
		 * filterbyScreen = filterByScreenFormat.stream().filter(predicate ->
		 * predicate.getIMAX() > 0) .collect(Collectors.toList()); } else if
		 * (screen.equalsIgnoreCase(Constants.PLAYHOUSE)) { cinemas.add("Playhouse");
		 * filterbyScreen = filterByScreenFormat.stream().filter(predicate ->
		 * predicate.getPlayhouse() > 0) .collect(Collectors.toList()); } else if
		 * (screen.equalsIgnoreCase(Constants.ONYX)) { cinemas.add("ONYX");
		 * filterbyScreen = filterByScreenFormat.stream().filter(predicate ->
		 * predicate.getONYX() > 0) .collect(Collectors.toList()); }
		 */
		//
		if (filterbyScreenSet.size() > 0) {
			filterbyScreen.addAll(filterbyScreenSet);
		}
		return filterbyScreen;
	}

	public int cinemaVal(String cinemaVal) {
		int val = 0;
		String str = cinemaVal.replaceAll("\"", "");
		if (!StringUtils.isEmpty(str)) {
			val = Integer.parseInt(str.substring(0, 1));
		}
		return val;
	}

	public Double getTotalOccupancy(List<Object[]> seatCategory, List<Integer> hopk) {
		List<Double> allHopkValue = new ArrayList<>();
		Double totalValue = 0.0;
		try {
			Map<Integer, Double> map = new HashMap<>();
			List<Map<Integer, Double>> average = new ArrayList<>();
			hopk.forEach(hp -> {
				List<Object[]> seatCategoryByhopk = seatCategory.stream()
						.filter(predicate -> (int) predicate[Constants.HOPK] == hp).collect(Collectors.toList());
				Double occupancyUpComingCinemas = Math.round((calculateOccupancy(seatCategoryByhopk) * 100) * 10)
						/ 10.0;
				map.put(hp, occupancyUpComingCinemas);
				average.add(map);
			});
			Map<Integer, Double> mainStreamOccupancy = mainstreamOccupancyQuaterlyPerHopk(hopk);

			mainStreamOccupancy.forEach((mainstreamhopk, mainstreamOccupancy) -> {
				map.forEach((screenHopk, screenOccupancy) -> {
					if (mainstreamhopk == screenHopk) {
						double total = (double) screenOccupancy / (double) mainstreamOccupancy;
						allHopkValue.add((double) Math.round((total) * 10) / 10.0);
					}
				});
			});

			totalValue = allHopkValue.stream().mapToDouble(m -> m).sum();

		} catch (Exception e) {
			log.error("while calculating the total occupany between mainstream and upcoming cinema format");

		}
		return !ObjectUtils.isEmpty(allHopkValue) ? Math.round((totalValue / allHopkValue.size()) * 10) / 10.0 : 1;
	}

	@Override
	public Map<Integer, Double> mainstreamOccupancyQuaterlyPerHopk(List<Integer> hopk) {
		try {

		} catch (Exception e) {
			log.error("error while fetching mainstream occupancy for the quaterly basis");

		}
		return filterByScreen(Constants.MAINSTREAM, hopk);
	}

	Map<Integer, Double> filterByScreen(String screenType, List<Integer> hopk) {
		List<Object[]> filterBySeatCategory = new ArrayList<>();

		try {

			List<String> cinemaFormat = commonAnalsisService.getCinemaFormatlist(Constants.MAINSTREAM);
			/*
			 * filterBySeatCategory =
			 * seatCategoryDao.findAllByCinemaHopkInAndCinemaFormat(hopk, cinemaFormat,
			 * DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
			 * DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));
			 */

			filterBySeatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormat(hopk, cinemaFormat);

		} catch (Exception e) {
			log.error("error while filtering screen based cinemas", e);

		}
		return getTotalOccupancyMainstream(filterBySeatCategory, hopk);
	}

	public Map<Integer, Double> getTotalOccupancyMainstream(List<Object[]> seatCategory, List<Integer> hopk) {
		List<Map<Integer, Double>> average = new ArrayList<>();
		Map<Integer, Double> map = new HashMap<>();
		try {
			if (!ObjectUtils.isEmpty(hopk)) {
				hopk.forEach(hp -> {
					List<Object[]> seatCategoryByhopk = seatCategory.stream()
							.filter(predicate -> (int) predicate[Constants.HOPK] == hp).collect(Collectors.toList());
					Double occupancyUpComingCinemas = Math.round((calculateOccupancy(seatCategoryByhopk) * 100) * 10)
							/ 10.0;
					if (occupancyUpComingCinemas != 0) {
						map.put(hp, occupancyUpComingCinemas);
						average.add(map);
					}
				});
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating ocupancy mainstream per hopk..", e);

		}

		return map;
	}

	SeatType calculateAdmitsByScreen(Optional<ProjectDetails> upcomingProjectDetails, String screenType,
			String projectId, double occupancyScreen) {
		SeatType admits = new SeatType();

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;

		}
		try {
			if (projectDetails.isPresent()) {
				List<CinemaFormat> allCinemaFormat = projectDetails.get().getCinemaFormat().stream()
						.filter(predicate -> predicate.getFormatType().equals(screenType)).collect(Collectors.toList());

				List<FixedConstant> fixConstant = (List<FixedConstant>) fixedConstantDao.findAll();

//				double fixNoOfShows = !ObjectUtils.isEmpty(fixConstant) ? (double) fixConstant.get(0).getAvgShowPerDay()
//						.get(projectDetails.get().getProjectLocation().getCity() != null ? Constants.FIXED
//								: projectDetails.get().getProjectLocation().getCity())
//						: (double) 5;
				double fixNoOfShows = 0;	
				String currentProjectLocation = projectDetails.get().getProjectLocation().getState();
				log.info("current project location : " + currentProjectLocation);
				
				if(!ObjectUtils.isEmpty(fixConstant)) {
					if(fixConstant.get(0).getAvgShowPerDay().containsKey(currentProjectLocation)) {
						fixNoOfShows = fixConstant.get(0).getAvgShowPerDay().get(currentProjectLocation);
					}else {
						fixNoOfShows = fixConstant.get(0).getAvgShowPerDay().get("fixed");
					}
				}
				log.info("fix no of shows : " + fixNoOfShows);
				
				double totalNormal = (double) (allCinemaFormat.stream().mapToInt(mapper -> mapper.getNormal()).sum())
						* (double) Constants.YEAR * fixNoOfShows * (double) occupancyScreen;
				double totalRecliner = (double) (allCinemaFormat.stream().mapToInt(mapper -> mapper.getRecliners())
						.sum()) * (double) Constants.YEAR * fixNoOfShows * (double) occupancyScreen;
				double totalLounger = (double) (allCinemaFormat.stream().mapToInt(mapper -> mapper.getLounger()).sum())
						* (double) Constants.YEAR * fixNoOfShows * (double) occupancyScreen;

				admits.setNormals(Math.round(totalNormal / 100));
				admits.setRecliner(Math.round(totalRecliner / 100));
				admits.setLongers(Math.round(totalLounger / 100));
			}
		} catch (Exception e) {
			log.error("error while calculating ocupancy mainstream per hopk..", e);

		}
		return admits;
	}

	@Override
	public Double getMainstreamBeanchmarkAtp(String projectId, Optional<ProjectDetails> upcomingProjectDetails,List<Navision> lastQuarterNonCompDataFinal) {

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		List<Integer> hpok = null;
		try {
//			final List<Navision> lastQuarterNonCompDataFinal =commonAnalysisService.getAllNavisionByComp();

			List<Navision> benchmark = compareOccupancyBenchMarkCinemas(projectDetails.get(),lastQuarterNonCompDataFinal);
			List<String> navCode = benchmark.stream().map(Navision::getNavCinemaCode).collect(Collectors.toList());
			hpok = getAllhopkByNavCode(navCode);

		} catch (Exception e) {
			log.error("error while fetching mainstream atp", e);

		}
		return filterMainstreamAtp(hpok, upcomingProjectDetails.get().getProjectLocation().getState(),
				Constants.MAINSTREAM);
	}

	public Double filterMainstreamAtp(List<Integer> hopk, String state, String screenType) {
		Double finalValue = 0.0;
		try {
			List<String> cinemaFormat = commonAnalsisService.getCinemaFormatlist(Constants.MAINSTREAM);
			List<String> seatCategorylist = commonAnalsisService.getAreaCategoryBySeatlist(Constants.NORMAL);

			/*List<Object[]> filterBySeatCategory = seatCategoryDao
					.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(hopk, cinemaFormat, seatCategorylist,
							DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
							DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
			
			List<Object[]> filterBySeatCategory = seatCategoryDao
					.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(hopk, cinemaFormat, seatCategorylist);
			
			

			Map<Integer, Double> map = new HashMap<>();

			hopk.forEach(hp -> {
				List<Object[]> seatCategoryByhopk = filterBySeatCategory.stream()
						.filter(predicate -> (int) predicate[Constants.HOPK] == hp).collect(Collectors.toList());
				Double atpUpComingCinemas = Math.round(calculateAtpPerHopk((seatCategoryByhopk)) * 10) / 10.0;
				if (atpUpComingCinemas != 0) {
					map.put(hp, atpUpComingCinemas);
				}

			});
			List<Double> atp = new ArrayList<>();
			map.forEach((key, value) -> {
				atp.add(value);
			});
			double totalAtp = atp.stream().mapToDouble(mapper -> mapper).sum() / atp.size();
			finalValue = getAtpByCappedNormalSeat(totalAtp, state, screenType);
		} catch (Exception e) {
			log.error("error while filtering mainstream atp..", e);
			e.printStackTrace();
		}

		return Math.round((finalValue) * 10) / 10.0;
	}

	public Double calculateAtpPerHopk(List<Object[]> seatCategoryByhopk) {
		double totalAtp = 0;
		try {
			long totalGboc = seatCategoryByhopk.stream()
					.mapToLong(mapper -> getMapperValueAdmits(mapper[Constants.GBOC])).sum();
			long totalSeats = seatCategoryByhopk.stream()
					.mapToLong(mapper -> getMapperValueTotalSeats(mapper[Constants.ADMITS])).sum();
			totalAtp = (double) totalGboc / (double) totalSeats;

		} catch (Exception e) {
			log.error("error while calculating atp by each hopk..", e);

		}
		return Math.round((totalAtp) * 10) / 10.0;
	}

	@Override
	public List<AtpAssumptions> getUpcomingCinemaAtp(String projectId,
			Optional<ProjectDetails> upcomingProjectDetails,List<Navision> lastQuarterNonCompDataFinal) {
		List<AtpAssumptions> capAtp = null;
		Optional<ProjectDetails> projectDetails = null;
		List<AtpAssumptions> revenueExpense = null;
		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		try {
			if (projectDetails.isPresent()) {

				Set<String> screen = projectDetails.get().getCinemaFormat().stream().map(CinemaFormat::getFormatType)
						.collect(Collectors.toSet());
				List<CinemaFormat> cinemaFOrmatList = new ArrayList<>();

				for (String action : screen) {

					List<CinemaFormat> list = projectDetails.get().getCinemaFormat().stream()
							.filter(predicate -> predicate.getFormatType().equals(action)).collect(Collectors.toList());
					CinemaFormat cinemaFormat = new CinemaFormat();
					cinemaFormat.setFormatType(list.get(0).getFormatType());
					cinemaFormat.setNormal(list.get(0).getNormal());
					cinemaFormat.setRecliners(list.get(0).getRecliners());
					cinemaFormat.setLounger(list.get(0).getLounger());
					cinemaFOrmatList.add(cinemaFormat);

				}

				revenueExpense = getAtpByScreenFormat(projectId, cinemaFOrmatList, projectDetails, lastQuarterNonCompDataFinal);

				/*
				 * capAtp = getAtpByCapped(revenueExpense,
				 * projectDetails.get().getProjectLocation().getState());
				 */
			}

		} catch (Exception e) {
			log.error("error while upcoming cinema atp ..", e);
		}
		return revenueExpense;
	}

	public List<AtpAssumptions> getAtpByScreenFormat(String projectId, List<CinemaFormat> cinemaFormat,
			Optional<ProjectDetails> upcomingCinema, List<Navision> lastQuarterNonCompDataFinal) {
		List<AtpAssumptions> seat = new ArrayList<>();
		// List<CinemaFormat> cinemaSolution =
		// cinemaFormat.stream().distinct().collect(Collectors.toList());

		// Set<CinemaFormat> cinemaSolution =
		// cinemaFormat.stream().distinct().collect(Collectors.toSet());

		cinemaFormat.forEach(cinema -> {

			if (cinema.getFormatType().equals(Constants.MAINSTREAM)) {
				Double normal = getMainstreamBeanchmarkAtp(projectId, upcomingCinema,lastQuarterNonCompDataFinal);

				/* Double normalAtpNational = getMainstreamNormalNational(); */
				Double recliner = getMainstreamBeanchmarkAtpReclinerOrLounger(projectId, upcomingCinema,
						Constants.RECLINER,lastQuarterNonCompDataFinal);
				Double lounger = getMainstreamBeanchmarkAtpReclinerOrLounger(projectId, upcomingCinema,
						Constants.LOUNGER,lastQuarterNonCompDataFinal);

				/*
				 * Double valueRecliner = getMainstreamreclinerOrLounger(recliner,
				 * normalAtpNational); Double valueLounger =
				 * getMainstreamreclinerOrLounger(lounger, normalAtpNational);
				 */
				Double valueRecliner = recliner * normal;
				Double valueLounger = lounger * normal;

				AtpAssumptions atp = new AtpAssumptions();
				SeatType seatType = new SeatType();

				atp.setCinemaFormatType(cinema.getFormatType());

				AssumptionsValues normalAtp = new AssumptionsValues();
				normalAtp.setCostType(Math.round(normal));
				atp.setNormal(normalAtp);

				AssumptionsValues loungerAtp = new AssumptionsValues();
				loungerAtp.setCostType(getAtpByCappedReclinerLoungerSeat(valueLounger,
						upcomingCinema.get().getProjectLocation().getState(), Constants.MAINSTREAM, Constants.LOUNGER));
				atp.setLounger(loungerAtp);

				AssumptionsValues reclinerAtp = new AssumptionsValues();
				reclinerAtp.setCostType(getAtpByCappedReclinerLoungerSeat(valueRecliner,
						upcomingCinema.get().getProjectLocation().getState(), Constants.MAINSTREAM,
						Constants.RECLINER));
				atp.setRecliner(reclinerAtp);

				seat.add(atp);

			} else {

				AtpAssumptions atp = new AtpAssumptions();
				SeatType seatType = getAtpSeatCategoryByScreen(cinema.getFormatType(), upcomingCinema,lastQuarterNonCompDataFinal);

				atp.setCinemaFormatType(cinema.getFormatType());

				AssumptionsValues normal = new AssumptionsValues();

				long normalValue = seatType.getNormals();

				if (normalValue == 0) {
					Double normalBenchmark = getMainstreamBeanchmarkAtp(projectId, upcomingCinema,lastQuarterNonCompDataFinal);

					normal.setCostType((long) (double) normalBenchmark);
					atp.setNormal(normal);

					AssumptionsValues lounger = new AssumptionsValues();
					lounger.setCostType((long) (double) normalBenchmark);
					atp.setLounger(lounger);

					AssumptionsValues recliner = new AssumptionsValues();
					recliner.setCostType((long) (double) normalBenchmark);
					atp.setRecliner(recliner);

					seat.add(atp);
				} else {
					normal.setCostType(seatType.getNormals());
					atp.setNormal(normal);

					AssumptionsValues lounger = new AssumptionsValues();
					lounger.setCostType(seatType.getLongers());
					atp.setLounger(lounger);

					AssumptionsValues recliner = new AssumptionsValues();
					recliner.setCostType(seatType.getRecliner());
					atp.setRecliner(recliner);

					seat.add(atp);
				}

			}

		});
		return seat;
	}

	public SeatType getAtpSeatCategoryByScreen(String ScreenType, Optional<ProjectDetails> upcomingCinema,List<Navision> lastQuarterNonCompDataFinal) {
		SeatType seatTypeModel = new SeatType();
		try {
//			List<Navision> lastQuarterNonCompDataFinal = commonAnalsisService.getAllNavisionByComp();
			Set<String> navCode = lastQuarterNonCompDataFinal.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

			List<Integer> hp = getAllHopkByNavCode(navCode, ScreenType);
			List<String> cinemaFormat = commonAnalsisService.getCinemaFormatlist(ScreenType);
			
			List<String> finalCinemaFormatList = new ArrayList<>();
			for(String cinema: cinemaFormat) {
				if(cinema.equals("Luxe"))
					finalCinemaFormatList.add("Gold");
				else
					finalCinemaFormatList.add(cinema);
			}

			Double normalSeatCategory = getNormalSeatCategory(hp, finalCinemaFormatList, upcomingCinema, ScreenType,lastQuarterNonCompDataFinal);
			seatTypeModel.setNormals(Math.round(normalSeatCategory));

			long lounger = getLoungerSeatCategory(hp, finalCinemaFormatList, normalSeatCategory);

			seatTypeModel.setLongers(getAtpByCappedReclinerLoungerSeat((double) lounger,
					upcomingCinema.get().getProjectLocation().getState(), ScreenType, Constants.LOUNGER));

			long recliner = getReclinerSeatCategory(hp, finalCinemaFormatList, normalSeatCategory);
			seatTypeModel.setRecliner(getAtpByCappedReclinerLoungerSeat((double) recliner,
					upcomingCinema.get().getProjectLocation().getState(), ScreenType, Constants.RECLINER));

		} catch (Exception e) {
			log.error("error while filtering ATP  other screen format data..", e);
		}
		return seatTypeModel;
	}

	public long getTotalAtpUpcomingScreenFormat(List<Object[]> seatCategory, List<Integer> hopk) {
		long totalValue = 0;
		List<Long> allHopkValue = new ArrayList<>();
		try {
			Map<Integer, Long> map = new HashMap<>();
			List<Map<Integer, Long>> average = new ArrayList<>();
			hopk.forEach(hp -> {
				List<Object[]> seatCategoryByhopk = seatCategory.stream()
						.filter(predicate -> predicate[Constants.HOPK] == hp).collect(Collectors.toList());
				long atpUpComingCinemas = Math.round(calculateAtpPerHopk(seatCategoryByhopk) * 100);
				map.put(hp, atpUpComingCinemas);
				average.add(map);
			});

			average.forEach(mapVal -> {
				map.forEach((keyAtp, valueAtp) -> {
					allHopkValue.add(valueAtp);
				});
			});

			totalValue = allHopkValue.stream().mapToLong(m -> m).sum();

		} catch (Exception e) {
			log.error("while calculating the total atp  upcoming cinema format", e);

		}
		return Math.round(totalValue / allHopkValue.size());
	}

	Double getNormalSeatCategory(List<Integer> hp, List<String> cinemaFormat, Optional<ProjectDetails> upcomingCinema,
			String screenType, List<Navision> lastQuarterNonCompDataFinal) {
		Double normal = 0.0;
		Map<Integer, Double> otherScreenFormatNormal = null;
		Map<Integer, Double> mainstreamNormal = null;
		List<Object[]> filterBySeatCategory = null;
		try {
			List<String> seatCategorylist = commonAnalsisService.getAreaCategoryBySeatlist(Constants.NORMAL);

			if (!ObjectUtils.isEmpty(hp)) {
				/*filterBySeatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(hp,
						cinemaFormat, seatCategorylist, DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
						DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/

				filterBySeatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(hp,
						cinemaFormat, seatCategorylist);
				
				
				mainstreamNormal = getMainstreamAtpNationalLevel(hp);
				otherScreenFormatNormal = getHopkByAtp(hp, filterBySeatCategory);

				normal = getNormalSeatAverage(mainstreamNormal, otherScreenFormatNormal, upcomingCinema, screenType,lastQuarterNonCompDataFinal);
			}
		} catch (Exception e) {
			log.error("error while fetching cinama format normal..", e);

		}
		return normal;
	}

	long getReclinerSeatCategory(List<Integer> hp, List<String> cinemaFormat, Double normalAverage) {
		long recliner = 0;
		Map<Integer, Double> otherScreenFormatNormal = null;
		Map<Integer, Double> otherScreenFormatLounger = null;
		List<Object[]> filterBySeatCategoryrecliner = null;
		try {
			List<String> seatCategorylist = commonAnalsisService.getAreaCategoryBySeatlist(Constants.RECLINER);

			if (!ObjectUtils.isEmpty(hp)) {
				/*filterBySeatCategoryrecliner = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategory(hp,
						cinemaFormat, seatCategorylist, DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
						DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
				
				filterBySeatCategoryrecliner = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategory(hp,
						cinemaFormat, seatCategorylist);
				
				
			}

			if (!ObjectUtils.isEmpty(filterBySeatCategoryrecliner)) {
				List<String> seatCategorylistNormal = commonAnalsisService.getAreaCategoryBySeatlist(Constants.NORMAL);
				/*List<Object[]> filterBySeatCategoryNormal = seatCategoryDao
						.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(hp, cinemaFormat,
								seatCategorylistNormal, DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
								DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
				
				List<Object[]> filterBySeatCategoryNormal = seatCategoryDao
						.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(hp, cinemaFormat,
								seatCategorylistNormal);

				otherScreenFormatNormal = getHopkByAtp(hp, filterBySeatCategoryNormal);
				otherScreenFormatLounger = getHopkByAtp(hp, filterBySeatCategoryrecliner);
				recliner = (long) (ObjectUtils.isEmpty(filterBySeatCategoryrecliner) ? normalAverage
						: getReclinerOrLoungerSeatAverage(otherScreenFormatLounger, otherScreenFormatNormal)
								* normalAverage);
			} else {
				recliner = Math.round(normalAverage);
			}

		} catch (Exception e) {
			e.getStackTrace();
			log.error("error while fetching cinama format recliner..", e);
		}
		return recliner;

	}

	long getLoungerSeatCategory(List<Integer> hp, List<String> cinemaFormat, Double normalAverage) {
		long lounger = 0;
		List<Object[]> filterBySeatCategoryLounger = null;
		Map<Integer, Double> otherScreenFormatNormal = null;
		Map<Integer, Double> otherScreenFormatLounger = null;
		try {
			List<String> seatCategorylist = commonAnalsisService.getAreaCategoryBySeatlist(Constants.LOUNGER);

			if (!ObjectUtils.isEmpty(hp)) {
				/*filterBySeatCategoryLounger = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategory(hp,
						cinemaFormat, seatCategorylist, DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
						DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
				
				filterBySeatCategoryLounger = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategory(hp,
						cinemaFormat, seatCategorylist);

				if (!ObjectUtils.isEmpty(filterBySeatCategoryLounger)) {

					List<String> seatCategorylistNormal = commonAnalsisService
							.getAreaCategoryBySeatlist(Constants.NORMAL);
					/*List<Object[]> filterBySeatCategoryNormal = seatCategoryDao
							.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(hp, cinemaFormat,
									seatCategorylistNormal, DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
									DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
					
					
					List<Object[]> filterBySeatCategoryNormal = seatCategoryDao
							.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(hp, cinemaFormat,
									seatCategorylistNormal);

					otherScreenFormatNormal = getHopkByAtp(hp, filterBySeatCategoryNormal);
					otherScreenFormatLounger = getHopkByAtp(hp, filterBySeatCategoryLounger);

					lounger = (long) (ObjectUtils.isEmpty(filterBySeatCategoryLounger) ? normalAverage
							: getReclinerOrLoungerSeatAverage(otherScreenFormatLounger, otherScreenFormatNormal)
									* normalAverage);
				} else {
					lounger = Math.round(normalAverage);
				}
			}

		} catch (Exception e) {
			log.error("error while fetching cinama format lounger..", e);

		}
		return lounger;
	}

	@Override
	public List<AssumptionsValues> getUpcomingCinemaSph(String projectId,
			Optional<ProjectDetails> upcomingProjectDetails,List<Navision> lastQuarterNonCompDataFinal) {
		List<AssumptionsValues> updatedSph = null;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}

		try {

			if (projectDetails.isPresent()) {
				List<Navision> benchMarkCinemas = compareOccupancyBenchMarkCinemas(projectDetails.get(),lastQuarterNonCompDataFinal);
				Double benchMarkCinemsAverage = averageOfAllSph(benchMarkCinemas);
				updatedSph = getAllUpcomingCinemaSph(projectId, benchMarkCinemsAverage, projectDetails.get());
			}

		} catch (Exception e) {
			log.error("error while fetching cinama sph", e);
			throw new FeasibilityException("error while fetching cinama sph", e);
		}
		return updatedSph;
	}

	public Double averageOfAllSph(List<Navision> benchMarkCinemas) {
		Double value = 0.0;
		try {
			List<String> navCode = benchMarkCinemas.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toList());
			List<Integer> hopk = getAllhopkByNavCode(navCode);
			/*List<Object[]> cinemaFormance = cinemaPerformanceDao.findAllCinemaPerformanceByHopkInAndDate(hopk,
					DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
					DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
			List<Object[]> cinemaFormance = cinemaPerformanceDao.findAllCinemaPerformanceByHopkInAndDate(hopk);
			
			
			value = getSphTotalByEachHopk(cinemaFormance, hopk);
		} catch (Exception e) {
			log.error("error while fetching average calculating sph benchmark cinemas", e);

		}
		return value;
	}

	public Double getSphTotalByEachHopk(List<Object[]> cinemaFormance, List<Integer> hopk) {
		List<Double> totalHopkAverage = new ArrayList<>();
		Double sum = 0.0;
		Double finalValue = 0.0;
		try {
			hopk.forEach(hp -> {
				List<Object[]> cinemaFormanceByHopk = cinemaFormance.stream()
						.filter(predicate -> (int) predicate[Constants.HOPK] == hp).collect(Collectors.toList());
				Double totalAveragePerHopk = calculateSph(cinemaFormanceByHopk);
				if (totalAveragePerHopk != 0.0) {
					totalHopkAverage.add(totalAveragePerHopk);
				}
			});
			sum = totalHopkAverage.stream().mapToDouble(m -> m.doubleValue()).sum();
			DecimalFormat df = new DecimalFormat("#.###");
			df.setRoundingMode(RoundingMode.CEILING);
			finalValue = (double) sum / (double) totalHopkAverage.size();
			df.format(finalValue);
			/* Math.round((sum / totalHopkAverage.size()) * 1000) / 1000.000 */
		} catch (Exception e) {
			log.error("error while calculating each hopk seat category sph", e);
		}
		return Math.round((sum / totalHopkAverage.size()) * 1000) / 1000.000;
	}

	List<Integer> getAllHopkByNavCode(Set<String> navCode, String screenType) {
		List<Integer> hp = null;
		try {
			List<String> code = new ArrayList<>();
			code.addAll(navCode);
			List<CinemaMasterSql> cinemas = screenType(screenType, code);
			List<String> hopk = cinemas.stream().map(CinemaMasterSql::getHOPK).collect(Collectors.toList());
			hp = hopk.stream().map(Integer::valueOf).collect(Collectors.toList());

		} catch (Exception e) {
			log.error("error while fetching cinama hopk by navcode", e);

		}

		return hp;
	}

	public Double calculateSph(List<Object[]> cinemaPerformanceByHopk) {
		Double totalSph = 0.0;
		try {
			long concessionNetRevenue = cinemaPerformanceByHopk.stream()
					.mapToLong(mapper -> getMapperValueConcessionNetRevenue(mapper[Constants.CONCESSION_NET_REVENUE]))
					.sum();
			long totalGboc = cinemaPerformanceByHopk.stream()
					.mapToLong(mapper -> getMapperValueTotalGboc(mapper[Constants.CINEMA_PERFORMANCE_GBOC])).sum();
			totalSph = (double) concessionNetRevenue / (double) totalGboc;

		} catch (Exception e) {
			log.error("error while calculating shp by each hopk");

		}
		return totalSph;
	}

	public long getMapperValueConcessionNetRevenue(Object arr) {
		long netRevenue = ((Number) arr).longValue();
		return netRevenue;
	}

	public long getMapperValueTotalGboc(Object arr) {
		long totalGboc = ((Number) arr).longValue();
		return totalGboc;
	}

	@Override
	public Map<Integer, Double> getMainstreamAtpNationalLevel(List<Integer> hopk) {
		try {

		} catch (Exception e) {
			log.error("error while fetching mainstream atp national level..", e);

		}
		return getAllSeatCategory(hopk);
	}

	Map<Integer, Double> getAllSeatCategory(List<Integer> hopk) {
		List<Object[]> filterBySeatCategory = null;
		try {
			List<String> cinemaFormat = commonAnalsisService.getCinemaFormatlist(Constants.MAINSTREAM);
			List<String> seatCategorylist = commonAnalsisService.getAreaCategoryBySeatlist(Constants.NORMAL);
			/*filterBySeatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(hopk,
					cinemaFormat, seatCategorylist, DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
					DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
			
			filterBySeatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(hopk,
					cinemaFormat, seatCategorylist);
			
		} catch (Exception e) {
			log.error("error while fetching mainstream filtering seat category national level..", e);

		}
		return getHopkByAtp(hopk, filterBySeatCategory);
	}

	Map<Integer, Double> getHopkByAtp(List<Integer> hopk, List<Object[]> filterBySeatCategory) {
		Map<Integer, Double> map = new HashMap<>();
		try {

			hopk.forEach(hp -> {
				List<Object[]> seatCategoryByhopk = filterBySeatCategory.stream()
						.filter(predicate -> (int) predicate[Constants.HOPK] == hp).collect(Collectors.toList());
				Double atpUpComingCinemas = Math.round((calculateAtpPerHopk(seatCategoryByhopk)) * 10) / 10.0;

				if (atpUpComingCinemas != 0) {
					map.put(hp, atpUpComingCinemas);
				}
			});

		} catch (Exception e) {
			log.error("error while hopk By Atp..", e);
		}
		return map;
	}

	Double getNormalSeatAverage(Map<Integer, Double> mainstreamMap, Map<Integer, Double> otherScreenFormat,
			Optional<ProjectDetails> upcomingCinema, String screenType, List<Navision> lastQuarterNonCompDataFinal) {
		Double finaValue = 0.0;
		Double beforeMultipleCappedValue = 0.0;
		try {
			List<Double> ratioResult = new ArrayList<>();

			otherScreenFormat.forEach((hopkOtherScreen, averageOtherScreen) -> {
				mainstreamMap.forEach((hopkMainstreamhopk, averageMainstreamScreen) -> {
					if (hopkOtherScreen == hopkMainstreamhopk) {
						Double ratioOtherScreenByMainstream = (double) averageOtherScreen
								/ (double) averageMainstreamScreen;
						ratioResult.add(Math.round((ratioOtherScreenByMainstream) * 10) / 10.0);
					}
				});
			});

			Double totalRatioResult = ratioResult.stream().mapToDouble(mapper -> mapper.doubleValue()).sum();
			finaValue = Math.round((totalRatioResult != 0 ? totalRatioResult / ratioResult.size() : 0) * 10) / 10.0;
			beforeMultipleCappedValue = getAtpByCappedNormalSeat(finaValue,
					upcomingCinema.get().getProjectLocation().getState(), screenType);
		} catch (Exception e) {
			log.error("error while average of other screen format by mainstream for normal seat..", e);

		}
		return beforeMultipleCappedValue * getMainstreamBeanchmarkAtp(null, upcomingCinema,lastQuarterNonCompDataFinal);
	}

	Double getReclinerOrLoungerSeatAverage(Map<Integer, Double> reclinerOrLoungerMap, Map<Integer, Double> normalMap) {
		Double totalRatioResult = 0.0;
		List<Double> ratioResult = new ArrayList<>();
		try {

			reclinerOrLoungerMap.forEach((hopkOfreclinerOrLounger, averagereclinerOrLounger) -> {
				normalMap.forEach((hopkNormalMap, averageNormalMap) -> {
					if (hopkOfreclinerOrLounger == hopkNormalMap) {
						double ratioaveragereclinerOrLoungerByNormal = (double) averagereclinerOrLounger
								/ (double) averageNormalMap;
						ratioResult.add(Math.round((ratioaveragereclinerOrLoungerByNormal) * 10) / 10.0);
					}
				});
			});

			totalRatioResult = ratioResult.stream().mapToDouble(mapper -> mapper.doubleValue()).sum();

		} catch (Exception e) {
			log.error("error while average of other screen format by mainstream for normal seat..", e);

		}
		return Math.round((totalRatioResult / ratioResult.size()) * 10) / 10.0;
	}

	public List<Navision> getLatLongCalculated(ProjectDetails projectDetails, List<Navision> benchMarkCinemas) {
		List<Navision> updatedDistanceNearBy = new ArrayList<>();
		// System.out.println("benchmark :- " + benchMarkCinemas);
		try {

			double upcomingProjectLatitude = projectDetails.getProjectLocation().getLatitude();
			double upcomingProjectLogitude = projectDetails.getProjectLocation().getLongitude();

			benchMarkCinemas.forEach(navision -> {
				String cinemaName = navision.getCinemaName();
				try {
					/* Map<String, Double> latLng = getAddress(cinemaName); */
					CinemaMaster cinemaMaster = cinemaMasterDao.findByNavCinemaCode(navision.getNavCinemaCode());
					if (!ObjectUtils.isEmpty(cinemaMaster)) {
						double distanceBetween = MathUtil.distanceInKm(upcomingProjectLatitude, upcomingProjectLogitude,
								cinemaMaster.getLatitude(), cinemaMaster.getLongitude());
						navision.setDistance(distanceBetween);
						updatedDistanceNearBy.add(navision);

						// System.out.println(cinemaMaster.getNavCinemaCode() + "-" + distanceBetween);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			});

		} catch (Exception e) {
			log.error("error while calculating latitude  longitude between benchmark cinemas ..", e);

		}
		updatedDistanceNearBy
				.sort((d1, d2) -> Double.valueOf(d1.getDistance()).compareTo(Double.valueOf(d2.getDistance())));
//		List<Navision> data = updatedDistanceNearBy.stream().distinct().limit(12).collect(Collectors.toList());
		List<Navision> data = updatedDistanceNearBy.stream().distinct().limit(3).collect(Collectors.toList());

		return data;
	}

	public Map<String, Double> getAddress(String fullAddress) throws IOException, ParseException {
		Map<String, Double> map = new HashMap<>();
		double lat = 0;
		double lng = 0;
		try {
			synchronized (this) {

				URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json" + "?address="
						+ URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false" + "&" + "key=" + googleKey + " ");

				URLConnection conn = url.openConnection();
				ByteArrayOutputStream output = new ByteArrayOutputStream(1024);

				IOUtils.copy(conn.getInputStream(), output);
				output.close();

				ObjectMapper mapper = new ObjectMapper();
				JsonNode array = mapper.readValue(output.toString(), JsonNode.class);

				JsonNode object = array.get("results").get(0);

				JsonNode geometry = object.get("geometry");
				JsonNode location = geometry.get("location");

				lat = location.get("lat").asDouble();
				lng = location.get("lng").asDouble();

			}
			map.put(Constants.LATITUDE, lat);
			map.put(Constants.LONGITUDE, lng);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching google api geocoding api");
		}
		return map;
	}

	public List<AssumptionsValues> getAllUpcomingCinemaSph(String projectId, Double benchmarkCinemasAverage,
			ProjectDetails projectDetails) {

		List<AssumptionsValues> admin = projectDetails.getOccupancy();
		// Set<AssumptionsValues> adminUnique =
		// admin.stream().distinct().collect(Collectors.toSet());
		List<AtpAssumptions> atp = projectDetails.getAtp();
		// Set<AtpAssumptions> atpUnique =
		// atp.stream().distinct().collect(Collectors.toSet());
		List<AssumptionsValues> sphValues = new ArrayList<>();

		try {

			admin.forEach(adminAssumptions -> {
				SeatType admits = adminAssumptions.getAdmits();
				atp.forEach(atpAssumption -> {
					if (adminAssumptions.getFormatType().equals(atpAssumption.getCinemaFormatType())) {
						AssumptionsValues sph = new AssumptionsValues();
						long totalAdmits = admits.getNormals() + admits.getRecliner() + admits.getLongers();
						long atpCinemas = admits.getNormals() * atpAssumption.getNormal().getCostType()
								+ admits.getRecliner() * atpAssumption.getRecliner().getCostType()
								+ admits.getLongers() * atpAssumption.getLounger().getCostType();

						long finalValue = totalAdmits > 0 ? (atpCinemas / totalAdmits) : 0;
						sph.setFormatType(adminAssumptions.getFormatType());
						Double val = benchmarkCinemasAverage * finalValue;
						sph.setCostType((long) (double) val);
						sphValues.add(sph);
					}
				});
			});

			/*
			 * adminUnique.forEach(adminAssumptions -> { SeatType admits =
			 * adminAssumptions.getAdmits(); atpUnique.forEach(atpAssumption -> { if
			 * (adminAssumptions.getFormatType().equals(atpAssumption.getCinemaFormatType())
			 * ) { AssumptionsValues sph = new AssumptionsValues(); long totalAdmits =
			 * admits.getNormals() + admits.getRecliner() + admits.getLongers(); long
			 * atpCinemas = admits.getNormals() * atpAssumption.getNormal().getCostType() +
			 * admits.getRecliner() * atpAssumption.getRecliner().getCostType() +
			 * admits.getLongers() * atpAssumption.getLounger().getCostType();
			 * 
			 * long finalValue = atpCinemas / totalAdmits;
			 * sph.setFormatType(adminAssumptions.getFormatType()); Double val =
			 * benchmarkCinemasAverage * finalValue; sph.setCostType((long) (double) val);
			 * sphValues.add(sph);
			 * 
			 * }
			 * 
			 * }); });
			 */

		} catch (Exception e) {
			log.error("error while calculation upcoming cinema sph ..", e);
		}
		return sphValues;
	}

	@Override
	public long getAdvertisingRevenue(String projectId, Optional<ProjectDetails> upcomingProjectDetails,List<Navision> lastQuarterNonCompDataFinal) {
		long adRevenue = 0;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}

		try {

			if (projectDetails.isPresent()) {
				List<Navision> benchmarkCinemas = compareOccupancyBenchMarkCinemas(projectDetails.get(),lastQuarterNonCompDataFinal);
				long average = getAdvertisingRevenueAverageOfAllScreen(benchmarkCinemas);
				adRevenue = average * projectDetails.get().getNoOfScreens();
			}

		} catch (Exception e) {
			log.error("error while fetching advertising revenue ..", e);
			throw new FeasibilityException("error while fetching advertising revenue..", e);
		}
		return adRevenue;
	}

	public static long getAdvertisingRevenueAverageOfAllScreen(List<Navision> allNavisionData) {
		double finalAverageOfAllScreen = 0;
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Double> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getAdvertisement() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(sumAll);
			}
			List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());

			double sumAllValues = allSumOfSingScreen.stream().mapToDouble(Double::doubleValue).sum();
			finalAverageOfAllScreen = (double) sumAllValues / (double) uniqueNavCode.size();

		} catch (Exception e) {
			log.error("error while calculating advertising revenue per quater..");

		}
		return Math.round(finalAverageOfAllScreen);
	}

	public List<Navision> getGoldScreenFilterTest(List<Navision> listNavision) {
		List<Navision> updatedGoldFilter = new ArrayList<>();

		try {
			List<String> getAllnavCodeby = new ArrayList<>();

			List<CinemaMasterSql> getAllCinemabyNavCode = cinemaMasterSqlDao
					.findAllCinemaMasterByNavCode(getAllnavCodeby);
			List<CinemaMasterSql> getAllCinemabyGoldFilter = getAllCinemabyNavCode.stream()
					.filter(predicate -> predicate.getGold() == 0 && predicate.getUltraPremium() == 0)
					.collect(Collectors.toList());
			List<String> getAllNavCode = getAllCinemabyGoldFilter.stream().map(CinemaMasterSql::getNav_cinemaCode)
					.collect(Collectors.toList());

			getAllNavCode.forEach(navcode -> {
				listNavision.forEach(navision -> {
					if (navision.getNavCinemaCode().equals(navcode)) {
						updatedGoldFilter.add(navision);
					}
				});
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching multiplier for the gold screen cost.", e);

		}
		return updatedGoldFilter;
	}

	@Override
	public Map<Integer, Double> getMainstreamOccupancyBenchmarkCinemas(List<Integer> hopk) {
		List<Object[]> filterBySeatCategory = null;
		try {
			List<String> cinemaFormat = commonAnalsisService.getCinemaFormatlist(Constants.MAINSTREAM);
			
			/*filterBySeatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormat(hopk, cinemaFormat,
					DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
					DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
			
			filterBySeatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormat(hopk, cinemaFormat);

		} catch (Exception e) {
			log.error("error while fetching mainstream occupancy benchmark");

		}
		return getTotalOccupancyMainstream(filterBySeatCategory, hopk);
	}

	public static String getCurrentQuater() {
		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);

		return (month >= Calendar.JANUARY && month <= Calendar.MARCH) ? "Q4"
				: (month >= Calendar.APRIL && month <= Calendar.JUNE) ? "Q1"
						: (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) ? "Q2" : "Q3";
	}

	@Override
	public double getProjectWebSaleDetails(String projectId, Optional<ProjectDetails> upcomingProjectDetails,List<Navision> lastQuarterNonCompDataFinal) {

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		double finalAvg = 0;
		try {

			if (projectDetails.isPresent()) {

				List<Navision> benchmarkCinemas = compareOccupancyBenchMarkCinemas(projectDetails.get(),lastQuarterNonCompDataFinal);
				List<String> navCode = benchmarkCinemas.parallelStream().map(Navision::getNavCinemaCode)
						.collect(Collectors.toList());

				List<Integer> hopks = getAllhopkByNavCode(navCode);

				String channelGroupOnline = Constants.ONLINE;
				/*List<Integer> ob = ticketOnlineOfflineDao.avgAdmitsOnline(hopks,
						DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
						DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE), channelGroupOnline);*/
				
				List<Integer> ob = ticketOnlineOfflineDao.avgAdmitsOnline(hopks, channelGroupOnline);
				

				int onlineSum = ob.stream().mapToInt(mapper -> mapper).sum();

				int onlineAvg = onlineSum / hopks.size();
				String channelGroupOffline = Constants.OFFLINE;

				/*List<Integer> objectOffline = ticketOnlineOfflineDao.avgAdmitsOnline(hopks,
						DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
						DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE), channelGroupOffline);*/
				
				
				List<Integer> objectOffline = ticketOnlineOfflineDao.avgAdmitsOnline(hopks, channelGroupOffline);

				int offlineSum = objectOffline.stream().mapToInt(mapper -> mapper).sum();

				int offline = 0;

				int offlineAvg = offlineSum / hopks.size();

				int onlineOffline = onlineAvg + offlineAvg;
				finalAvg = (double) onlineAvg / onlineOffline;
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating web sale per quater..");

		}
		return finalAvg * 100;
	}

	List<Integer> getAllhopkByNavCode(List<String> navCode) {
		List<Integer> hopk = null;
		if (!ObjectUtils.isEmpty(navCode)) {
			List<CinemaMasterSql> cinemaMasterSql = cinemaMasterSqlDao.findAllCinemaMasterByNavCode(navCode);
			hopk = cinemaMasterSql.parallelStream().map(CinemaMasterSql::getHOPK).map(Integer::valueOf)
					.collect(Collectors.toList());
		}
		return hopk;
	}

	@Override
	public long getProjectTotalAdmitsDetails(String projectId, Optional<ProjectDetails> upcomingProjectDetails) {

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		List<Long> totalAdmits = new ArrayList<>();
		try {
			if (projectDetails.isPresent()) {
				List<AssumptionsValues> occupancy = projectDetails.get().getOccupancy();
				occupancy.forEach(admit -> {
					SeatType seat = admit.getAdmits();
					long normal = seat.getNormals();
					long recliner = seat.getRecliner();
					long lounger = seat.getLongers();
					totalAdmits.add(normal + recliner + lounger);
				});
			}

		} catch (Exception e) {
			log.error("error while calculating total admits..");
		}
		return totalAdmits.stream().mapToLong(mapper -> mapper).sum();
	}

//	@Override
//	public Set<String> getProjectTestAlgo(String projectId) {
//		Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);
//
//		List<Navision> benchmarkCinam = compareOccupancyBenchMarkCinemas(projectDetails.get());
//
//		long start = System.currentTimeMillis();
//		List<String> costType = MathUtil.getAllCostTypeAlgo();
//
//		long seconds = (int) ((start - System.currentTimeMillis() / 1000) % 60);
//		System.out.println("totalSeconds " + seconds);
//		return benchmarkCinam.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
//	}

	public List<AtpAssumptions> getAtpByCapped(List<AtpAssumptions> atpAssumptions, String state) {

		try {
			List<FixedConstant> fixedConstants = (List<FixedConstant>) fixedConstantDao.findAll();
			List<AtpCapDetials> atpCapDetails = fixedConstants.get(Constants.ZERO).getAtpCapDetails();

			List<AtpCapDetials> atpCityCapped = atpCapDetails.stream()
					.filter(predicate -> predicate.getState().equals(state)).collect(Collectors.toList());

			if (!ObjectUtils.isEmpty(atpCityCapped)) {
				atpAssumptions.forEach(assumption -> {

					atpCityCapped.forEach(action -> {

						Map<String, Map<String, Double>> mapAtpDetails = action.getAtpDetails();
						mapAtpDetails.forEach((screen, value) -> {

							if (screen.equals(assumption.getCinemaFormatType())) {
								value.forEach((seat, data) -> {
									if (seat.equals(Constants.NORMAL)) {
										Double normal = data;
										Long normalAtp = assumption.getNormal().getCostType();

										AssumptionsValues as = new AssumptionsValues();
										double values = normalAtp > normal ? normal : normalAtp;
										as.setCostType((long) values);
										assumption.setNormal(as);

									} else if (seat.equals(Constants.RECLINER)) {
										Double recliner = data;
										Long reclinerAtp = assumption.getRecliner().getCostType();

										AssumptionsValues as = new AssumptionsValues();
										double values = reclinerAtp > recliner ? recliner : reclinerAtp;
										as.setCostType((long) values);
										assumption.setRecliner(as);

									} else if (seat.equals(Constants.LOUNGER)) {
										Double lounger = data;
										Long loungerAtp = assumption.getLounger().getCostType();

										AssumptionsValues as = new AssumptionsValues();
										double values = loungerAtp > lounger ? lounger : loungerAtp;
										as.setCostType((long) values);
										assumption.setRecliner(as);
									}
								});
							}
						});
					});
				});

			}

		} catch (Exception e) {
			log.error("error while fetching atp capped");
		}

		return atpAssumptions;
	}

	@Override
	public Double getMainstreamBeanchmarkAtpReclinerOrLounger(String projectId,
			Optional<ProjectDetails> upcomingProjectDetails, String areaCategory,List<Navision> lastQuarterNonCompDataFinal) {

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		List<Integer> hpok = null;
		try {
			/*
			 * List<Navision> benchmark =
			 * compareOccupancyBenchMarkCinemas(projectDetails.get());
			 */
//			List<Navision> lastQuarterNonCompDataFinal = commonAnalysisService.getAllNavisionByComp();
			Set<String> navCode = lastQuarterNonCompDataFinal.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
			hpok = getAllHopkByNavCode(navCode, Constants.MAINSTREAM);
//			System.out.println(hpok);

		} catch (Exception e) {
			log.debug("error while fetching mainstream recliner or lounger...");
		}
		return filterMainstreamAtpReclinerOrLounger(hpok, areaCategory);
	}

	public Double filterMainstreamAtpReclinerOrLounger(List<Integer> hopk, String areaCategory) {
		double totalAtp = 0;
		try {
			List<String> cinemaFormat = commonAnalsisService.getCinemaFormatlist(Constants.MAINSTREAM);
			List<String> seatCategorylist = commonAnalsisService.getAreaCategoryBySeatlist(areaCategory);

			/*List<Object[]> filterBySeatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategory(
					hopk, cinemaFormat, seatCategorylist, DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
					DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
			
			List<Object[]> filterBySeatCategory = seatCategoryDao.findAllByCinemaHopkInAndCinemaFormatInAndAreaCategory(
					hopk, cinemaFormat, seatCategorylist);

			Map<Integer, Double> map = new HashMap<>();

			hopk.forEach(hp -> {
				List<Object[]> seatCategoryByhopk = filterBySeatCategory.stream()
						.filter(predicate -> (int) predicate[Constants.HOPK] == hp).collect(Collectors.toList());
				Double atpUpComingCinemas = Math.round(calculateAtpPerHopk((seatCategoryByhopk)) * 10) / 10.0;
				if (atpUpComingCinemas != 0) {
					map.put(hp, atpUpComingCinemas);
				}

			});
			Map<Integer, Double> mainstreamNormal = getMainstreamAtpNationalLevel(hopk);
			List<Double> atp = new ArrayList<>();
			map.forEach((key, value) -> {
				mainstreamNormal.forEach((keymainstream, valuemainstream) -> {
					if (key == keymainstream) {
						atp.add(value / valuemainstream);
					}
				});
			});

			/*
			 * map.forEach((key, value) -> { atp.add(value); });
			 */
			totalAtp = atp.stream().mapToDouble(mapper -> mapper).sum() / atp.size();

		} catch (Exception e) {
			log.error("error while filtering mainstream atp..", e);
			e.printStackTrace();
		}

		return Math.round((totalAtp) * 10) / 10.0;
	}

	public Double getMainstreamreclinerOrLounger(Double recliner, Double Normal) {
		Double reclinerAtp = recliner / Normal;
		return reclinerAtp * Normal;
	}

	/*
	 * @Override public Double getMainstreamNormalNational() { Double atp = 0.0;
	 * List<Integer> hopk = null; try {
	 * 
	 * List<Navision> benchmark = commonAnalysisService.getAllNavisionByComp();
	 * Set<String> navCode =
	 * benchmark.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet()
	 * ); hopk = getAllHopkByNavCode(navCode, Constants.MAINSTREAM); } catch
	 * (Exception e) {
	 * log.error("error while fetching mainstream normal national level"); } return
	 * filterMainstreamAtp(hopk); }
	 */

	public Double getAtpByCappedNormalSeat(Double normalValue, String state, String screenType) {
		List<Double> fixDouble = new ArrayList<>();
		Double finalValue = 0.0;
		try {
			List<FixedConstant> fixedConstants = (List<FixedConstant>) fixedConstantDao.findAll();
			List<AtpCapDetials> atpCapDetails = fixedConstants.get(Constants.ZERO).getAtpCapDetails();

			List<AtpCapDetials> atpCityCapped = atpCapDetails.stream()
					.filter(predicate -> predicate.getState().equals(state)).collect(Collectors.toList());

			if (!ObjectUtils.isEmpty(atpCityCapped)) {
				atpCityCapped.forEach(action -> {
					Map<String, Map<String, Double>> mapAtpDetails = action.getAtpDetails();
					mapAtpDetails.forEach((screen, value) -> {
						if (screen.equals(screenType)) {
							value.forEach((seat, data) -> {
								if (seat.equals(Constants.NORMAL)) {
									Double normal = data;
									Double values = normalValue > normal ? normal : normalValue;
									fixDouble.add(values);
								}
							});
						}

					});
				});

			}
			if (ObjectUtils.isEmpty(fixDouble)) {
				finalValue = normalValue;
			} else {
				finalValue = fixDouble.get(0);
			}

		} catch (Exception e) {
			log.error("error while fetching atp capped");
		}

		return finalValue;
	}

	public long getAtpByCappedReclinerLoungerSeat(Double valueReclinerOrLounger, String state, String screenType,
			String seatType) {
		List<Long> fixDouble = new ArrayList<>();
		long finalValue = 0;
		try {
			List<FixedConstant> fixedConstants = (List<FixedConstant>) fixedConstantDao.findAll();
			List<AtpCapDetials> atpCapDetails = fixedConstants.get(Constants.ZERO).getAtpCapDetails();

			List<AtpCapDetials> atpCityCapped = atpCapDetails.stream()
					.filter(predicate -> predicate.getState().equals(state)).collect(Collectors.toList());

			if (!ObjectUtils.isEmpty(atpCityCapped)) {
				atpCityCapped.forEach(action -> {
					Map<String, Map<String, Double>> mapAtpDetails = action.getAtpDetails();
					mapAtpDetails.forEach((screen, value) -> {
						if (screen.equals(screenType)) {
							value.forEach((seat, data) -> {
								if (seat.equals(seatType)) {
									Double recliner = data;
									Double values = valueReclinerOrLounger > recliner ? recliner
											: valueReclinerOrLounger;
									fixDouble.add(Math.round(values));
								} /*
									 * else if (seat.equals(Constants.LOUNGER)) { Double lounger = data; Double
									 * values = valueReclinerOrLounger > lounger ? lounger : valueReclinerOrLounger;
									 * fixDouble.add(Math.round(values)); }
									 */
							});
						}

					});
				});

			}

			if (ObjectUtils.isEmpty(fixDouble)) {
				finalValue = Math.round(valueReclinerOrLounger);
			} else {
				finalValue = fixDouble.get(0);
			}

		} catch (Exception e) {
			log.error("error while fetching atp capped");
		}

		return finalValue;
	}

	Double getGoldNationalLevelSph() {
		Double gold = 0.0;
		List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
		Set<String> navCode = filteredCompData.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
		List<Integer> hopk = getAllHopkByNavCode(navCode, Constants.GOLD);
		/*List<Object[]> cinemaFormance = cinemaPerformanceDao.findAllCinemaPerformanceByHopkInAndDate(hopk,
				DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
				DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
		
		List<Object[]> cinemaFormance = cinemaPerformanceDao.findAllCinemaPerformanceByHopkInAndDate(hopk);
		
		return getSphTotalByEachHopk(cinemaFormance, hopk);
	}

	@Override
	public List<CinemaMaster> getAllKeyRevenueAssumptionOccupancy(ProjectDetails projectDetails) {
		List<CinemaMaster> cinemaMaster = new ArrayList<>();
		try {
			
			final List<Navision> lastQuarterNonCompDataFinal = commonAnalsisService.getAllNavisionByComp();
			List<Navision> benchmarkCinemas = compareOccupancyBenchMarkCinemas(projectDetails,lastQuarterNonCompDataFinal);
			List<String> navCode = benchmarkCinemas.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toList());

			cinemaMaster = cinemaMasterDao.findAllByNavCinemaCodeIn(navCode);
			cinemaMaster.forEach(action -> {
				action.setSeatCapacity(revenueDataService.getAllSeatCapacityByHopk(String.valueOf(action.getHopk())));
				action.setOccupancy(revenueDataService.getAllOccupancyByHopk(action.getHopk()));
				action.setAdmits(revenueDataService.getAllAdmitsByHopk(action.getHopk()));
				action.setAtp(revenueDataService.getAllAtpByHopk(action.getHopk()));
				action.setSph(revenueDataService.getAllSphByHopk(action.getHopk()));
				action.setSphAtp(getSphAtp(action.getSph(), action.getAtp()));
				action.setWebSale(revenueDataService.getAllWebSaleByHopk(action.getHopk()));
				List<Navision> navisions = benchmarkCinemas.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equalsIgnoreCase(action.getNavCinemaCode()))
						.limit(4).collect(Collectors.toList());
//				System.out.println(new Gson().toJson(navisions));
				Long totalValue = navisions.stream().mapToLong(Navision::getAdvertisement).sum();
				action.setAdRevenue(totalValue);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching key revenue assumption ccupancy..");
		}
		return cinemaMaster;
	}

	@Override
	public Set<BenchmarkCinema> getRevenueBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {
			final List<Navision> lastQuarterNonCompDataFinal = commonAnalsisService.getAllNavisionByComp();
			List<Navision> benchmarkCinemas = compareOccupancyBenchMarkCinemas(projectDetails,lastQuarterNonCompDataFinal);

			if (!ObjectUtils.isEmpty(benchmarkCinemas)) {
				benchmarkCinemas.forEach(action -> {
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(action.getCinemaName());
					mark.add(marker);
				});
			}
		} catch (Exception e) {
			log.error("error while fetching personal benchmarkCinemas");
		}
		return mark;
	}

	public Double getSphAtp(Long sph, Long atp) {
		Double sphAtp = 0.0;
		if ((sph != null && sph != 0) && (atp != null && atp != 0)) {
			sphAtp = MathUtil.roundTwoDecimals((double) sph / (double) atp * 100);
		}
		return sphAtp;
	}
}
