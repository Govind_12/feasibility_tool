package com.api.services;

import java.util.Map;

import com.common.models.BDData;

public interface DashboardService {
		
	Map<String,Object> getDashboardDetails();
	
	Map<String, Object> getDashboardDetails2(String paramString);
	  
	Map<String, Object> setreportdata(BDData paramBDData);

}
