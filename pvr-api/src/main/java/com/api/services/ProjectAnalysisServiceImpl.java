
package com.api.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.CinemaMasterDao;
import com.api.dao.CityTierDao;
import com.api.dao.NavisionDao;
import com.api.dao.ProjectDetailsDao;
import com.api.dao.sql.CinemaMasterSqlDao;
import com.api.dao.sql.CinemaPerformanceDao;
import com.api.dao.sql.SeatCategoryDao;
import com.api.exception.FeasibilityException;
import com.common.constants.Constants;
import com.common.mappers.BenchMarkSelectionMapper;
import com.common.mappers.BenchmarkCinema;
import com.common.mappers.ProjectAnalysisMapper;
import com.common.models.CinemaFormat;
import com.common.models.CinemaMaster;
import com.common.models.Navision;
import com.common.models.ProjectDetails;
import com.common.sql.models.CinemaPerformance;
import com.common.sql.models.SeatCategory;
import com.common.sql.models.TicketOfflineOnline;

import lombok.extern.slf4j.Slf4j;

@Service("projectanalysisService")
@Slf4j
public class ProjectAnalysisServiceImpl implements ProjectAnalysisService {

	@Autowired
	private ProjectDetailsDao projectDetailsDao;

	@Autowired
	private SeatCategoryDao seatCategoryDao;

	@Autowired
	private CinemaMasterDao cinemaMasterDao;

	@Autowired
	private NavisionDao navisionDao;

	@Autowired
	private CityTierDao cityTierDao;

	@Autowired
	private CommonAnalysisService commonAnalysisService;

	@Autowired
	private CinemaMasterSqlDao cinemaMasterSqlDao;

	@Autowired
	private CinemaPerformanceDao cinemaPerformanceDao;

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public SeatCategory getSeatCategoryMasterDetails() {
		// List<CinemaMasterSql> str=cinemaMasterSqlDao.findAllCinemaMaster();
		// List<SeatCategory> data=seatCategoryDao.findAll();
		List<CinemaPerformance> cf = cinemaPerformanceDao.findAllCinemaPerformance();
		// System.out.println(cf);
		return new SeatCategory();
	}

	@Override
	public ProjectAnalysisMapper getProjectAnalysisById(String projectId) {
		ProjectAnalysisMapper projectAnalysis = new ProjectAnalysisMapper();
		List<BenchMarkSelectionMapper> benchMarkSelectionAll = new ArrayList<>();
		if (ObjectUtils.isEmpty(projectId)) {
			log.info("projectId cannot be null or empty.");
			throw new FeasibilityException("projectId cannot be null or empty.");
		}

		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			if (projectDetails.isPresent()) {
				benchMarkSelectionAll.add(getForMainStreamScreenFormat(projectDetails.get()));
				benchMarkSelectionAll.add(getForDxScreenFormat(projectDetails.get()));
				benchMarkSelectionAll.add(getForGoldScreenFormat(projectDetails.get()));
				benchMarkSelectionAll.add(getForImaxScreenFormat(projectDetails.get()));
				benchMarkSelectionAll.add(getForOnyxScreenFormat(projectDetails.get()));
				benchMarkSelectionAll.add(getForPlayhouseScreenFormat(projectDetails.get()));
				benchMarkSelectionAll.add(getForPremierScreenFormat(projectDetails.get()));
				benchMarkSelectionAll.add(getForUltraPremiumScreenFormat(projectDetails.get()));
				benchMarkSelectionAll.add(getForPxlScreenFormat(projectDetails.get()));

				boolean IsotherScreenFormatEmpty = benchMarkSelectionAll.stream()
						.anyMatch(predicate -> (ObjectUtils.isEmpty(predicate.getCinemaMasterDetails())
								&& !predicate.getFormats().equals(Constants.MAINSTREAM)));

				if (IsotherScreenFormatEmpty) {
					benchMarkSelectionAll.add(otherScreenFormatEmpty(projectDetails.get()));
				}

				projectAnalysis.setBenchmarkCinemaMapper(benchMarkSelectionAll);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while filtrering benchmark assumption");
			throw new FeasibilityException("error while filtrering benchmark assumption");
		}
		return projectAnalysis;

	}

	public List<CinemaMaster> filterBasedOntheCityTier(ProjectDetails projectDetails) {

		if (ObjectUtils.isEmpty(projectDetails)) {
			log.error("city-tier cannot be null or empty");
			throw new FeasibilityException("city-tier cannot be null or empty");
		}

		List<CinemaMaster> getAllCinemaMasterByCityTier = null;
		try {
			getAllCinemaMasterByCityTier = cinemaMasterDao
					.findAllByCityTier(projectDetails.getProjectLocation().getCityTier());

		} catch (Exception e) {
			log.error("error while filtering city-tier");
			throw new FeasibilityException("error while filtering city-tier");
		}

		return getAllCinemaMasterByCityTier;
	}

	public List<CinemaMaster> getNearByLatituteLongitude(ProjectDetails projectDetails) {

		if (ObjectUtils.isEmpty(projectDetails)) {
			log.info("entity cannot be null or empty");
			throw new FeasibilityException("entity cannot be null or empty");
		}
		List<CinemaMaster> filter = null;
		try {
			/*
			 * List<Double> listOfLatLong = new ArrayList<>(); listOfLatLong.add(42.5d);
			 * listOfLatLong.add(48.6d);
			 */
			// Position position=new Position(values)
			// Point point = new Point(12.92, 77.59);
			Point point = new Point(projectDetails.getProjectLocation().getLatitude(),
					projectDetails.getProjectLocation().getLongitude());
			/*
			 * Gson g = new Gson(); System.out.println(g.toJson(point)); Distance distance =
			 * new Distance(100000, Metrics.KILOMETERS);
			 */
			NearQuery nearQuery = NearQuery.near(point).maxDistance(new Distance(60000, Metrics.KILOMETERS));
			// filter=cinemaMasterDao.findByLocationNear(point);
			nearQuery.num(1);
			List<CinemaMaster> repoData = cinemaMasterDao.findByLocationNear(point,
					new Distance(60000, Metrics.KILOMETERS));
			// List<CinemaMaster> testData = mongoTemplate.find(query, Place.class);
			// System.out.println(repoData);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return filter;
	}

	public List<CinemaMaster> filterBasedOnCinemaCategory(ProjectDetails projectDetails) {
		if (ObjectUtils.isEmpty(projectDetails)) {
			log.info("entity cannot be null or empty");
			throw new FeasibilityException("entity cannot be null or empty");
		}
		List<CinemaMaster> getAllCinemaMasterByCategory = null;
		try {
			getAllCinemaMasterByCategory = cinemaMasterDao.findAllByCinemaCategory(projectDetails.getCinemaCategory());

		} catch (Exception e) {
			log.info("error while cinema master based on cinema category ");
			throw new FeasibilityException("error while cinema master based on cinema category ");
		}

		return getAllCinemaMasterByCategory;
	}

	public BenchMarkSelectionMapper getForMainStreamScreenFormat(ProjectDetails projectDetails) {

		List<BenchMarkSelectionMapper> getCinemaByCinemaFormat = new ArrayList<>();
		BenchMarkSelectionMapper benchmarkSelection = new BenchMarkSelectionMapper();

		try {

			projectDetails.getCinemaFormat().forEach(action -> {

				if (action.getFormatType().equals(Constants.MAINSTREAM)) {

					List<Navision> getAllNavision = commonAnalysisService.getAllNavisionByComp();
					List<String> navCinemaCode = null;
					List<CinemaMaster> getAllCinemabyNavCode = null;
					if (!ObjectUtils.isEmpty(getAllNavision)) {
						navCinemaCode = getAllNavision.stream().map(Navision::getNavCinemaCode)
								.collect(Collectors.toList());
					}
					if (!ObjectUtils.isEmpty(navCinemaCode)) {
						getAllCinemabyNavCode = cinemaMasterDao.findAllByNavCinemaCodeIn(navCinemaCode);
					}

					List<CinemaMaster> getAllCinemaByCityTier = getAllCinemabyNavCode.stream()
							.filter(x1 -> x1.getCityTier().equals(projectDetails.getProjectLocation().getCityTier()))
							.collect(Collectors.toList());

					List<CinemaMaster> filterByCinemaCategory = getAllCinemaByCityTier.stream()
							.filter(cinema -> cinema.getCinemaCategory().equals(projectDetails.getCinemaCategory()))
							.collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaByNearBy = new ArrayList<>();
					List<CinemaMaster> allDistictCinemaMaster = null;

					// for filter based on latitude longitude more than three cinamas
					if (filterByCinemaCategory.size() > 3) {

						for (CinemaMaster cinemaMaster : filterByCinemaCategory) {
							double totalDistance = distance(projectDetails.getProjectLocation().getLatitude(),
									projectDetails.getProjectLocation().getLongitude(), cinemaMaster.getLocation()[0],
									cinemaMaster.getLocation()[1]);
							cinemaMaster.setDistance(totalDistance);
							getAllCinemaByNearBy.add(cinemaMaster);
						}
						getAllCinemaByNearBy.sort((d1, d2) -> Double.valueOf(d1.getDistance())
								.compareTo(Double.valueOf(d2.getDistance())));
						allDistictCinemaMaster = getAllCinemaByNearBy.stream().distinct().limit(3)
								.collect(Collectors.toList());

						benchmarkSelection.setCinemaMasterDetails(allDistictCinemaMaster);
						benchmarkSelection.setFormats(Constants.MAINSTREAM);
					} else {

						benchmarkSelection.setCinemaMasterDetails(filterByCinemaCategory);
						benchmarkSelection.setFormats(Constants.MAINSTREAM);
					}

				} else {
					benchmarkSelection.setFormats(Constants.MAINSTREAM);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.info("error while fetching mainstream based on projectId ");
			throw new FeasibilityException("error while fetching mainstream based on projectId");
		}

		return benchmarkSelection;
	}

	public BenchMarkSelectionMapper getForGoldScreenFormat(ProjectDetails projectDetails) {
		List<BenchMarkSelectionMapper> getCinemaByCinemaFormat = new ArrayList<>();
		BenchMarkSelectionMapper benchmarkSelection = new BenchMarkSelectionMapper();
		try {
			projectDetails.getCinemaFormat().forEach(action -> {

				if (action.getFormatType().equals(Constants.GOLD)) {
					List<Navision> getAllNavision = commonAnalysisService.getAllNavisionByComp();
					List<String> navCinemaCode = null;
					List<CinemaMaster> getAllCinemabyNavCode = null;
					if (!ObjectUtils.isEmpty(getAllNavision)) {
						navCinemaCode = getAllNavision.stream().map(Navision::getNavCinemaCode)
								.collect(Collectors.toList());
					}
					if (!ObjectUtils.isEmpty(navCinemaCode)) {
						getAllCinemabyNavCode = cinemaMasterDao.findAllByNavCinemaCodeIn(navCinemaCode);
					}
					List<CinemaMaster> getAllCinemaByCityTier = getAllCinemabyNavCode.stream()
							.filter(x1 -> x1.getCityTier().equals(projectDetails.getProjectLocation().getCityTier()))
							.collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaMasterByScreenType = getAllCinemaByCityTier.stream()
							.filter(x2 -> x2.getGold() > 0).collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaByNearBy = new ArrayList<>();
					List<CinemaMaster> allDistictCinemaMaster = null;

					// for filter based on latitude longitude more than three cinamas
					if (getAllCinemaMasterByScreenType.size() > 3) {

						for (CinemaMaster cinemaMaster : getAllCinemaMasterByScreenType) {
							double totalDistance = distance(projectDetails.getProjectLocation().getLatitude(),
									projectDetails.getProjectLocation().getLongitude(), cinemaMaster.getLocation()[0],
									cinemaMaster.getLocation()[1]);
							cinemaMaster.setDistance(totalDistance);
							getAllCinemaByNearBy.add(cinemaMaster);
						}
						getAllCinemaByNearBy.sort((d1, d2) -> Double.valueOf(d1.getDistance())
								.compareTo(Double.valueOf(d2.getDistance())));
						allDistictCinemaMaster = getAllCinemaByNearBy.stream().distinct().limit(3)
								.collect(Collectors.toList());

						benchmarkSelection.setCinemaMasterDetails(allDistictCinemaMaster);
						benchmarkSelection.setFormats(Constants.GOLD);
					} else {

						benchmarkSelection.setCinemaMasterDetails(getAllCinemaMasterByScreenType);
						benchmarkSelection.setFormats(Constants.GOLD);
					}
				} else {
					benchmarkSelection.setFormats(Constants.GOLD);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.info("error while fetching gold based on projectId ");
			throw new FeasibilityException("error while fetching gold based on projectId");
		}

		return benchmarkSelection;
	}

	public BenchMarkSelectionMapper getForImaxScreenFormat(ProjectDetails projectDetails) {
		List<BenchMarkSelectionMapper> getCinemaByCinemaFormat = new ArrayList<>();
		BenchMarkSelectionMapper benchmarkSelection = new BenchMarkSelectionMapper();
		try {
			projectDetails.getCinemaFormat().forEach(action -> {

				if (action.getFormatType().equals(Constants.IMAX)) {
					List<Navision> getAllNavision = commonAnalysisService.getAllNavisionByComp();
					List<String> navCinemaCode = null;
					List<CinemaMaster> getAllCinemabyNavCode = null;
					if (!ObjectUtils.isEmpty(getAllNavision)) {
						navCinemaCode = getAllNavision.stream().map(Navision::getNavCinemaCode)
								.collect(Collectors.toList());
					}
					if (!ObjectUtils.isEmpty(navCinemaCode)) {
						getAllCinemabyNavCode = cinemaMasterDao.findAllByNavCinemaCodeIn(navCinemaCode);
					}
					List<CinemaMaster> getAllCinemaByCityTier = getAllCinemabyNavCode.stream()
							.filter(x1 -> x1.getCityTier().equals(projectDetails.getProjectLocation().getCityTier()))
							.collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaMasterByScreenType = getAllCinemaByCityTier.stream()
							.filter(x2 -> x2.getImax() > 0).collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaByNearBy = new ArrayList<>();
					List<CinemaMaster> allDistictCinemaMaster = null;

					// for filter based on latitude longitude more than three cinamas
					if (getAllCinemaMasterByScreenType.size() > 3) {

						for (CinemaMaster cinemaMaster : getAllCinemaMasterByScreenType) {
							double totalDistance = distance(projectDetails.getProjectLocation().getLatitude(),
									projectDetails.getProjectLocation().getLongitude(), cinemaMaster.getLocation()[0],
									cinemaMaster.getLocation()[1]);
							cinemaMaster.setDistance(totalDistance);
							getAllCinemaByNearBy.add(cinemaMaster);
						}
						getAllCinemaByNearBy.sort((d1, d2) -> Double.valueOf(d1.getDistance())
								.compareTo(Double.valueOf(d2.getDistance())));
						allDistictCinemaMaster = getAllCinemaByNearBy.stream().distinct().limit(3)
								.collect(Collectors.toList());

						benchmarkSelection.setCinemaMasterDetails(allDistictCinemaMaster);
						benchmarkSelection.setFormats(Constants.IMAX);
					} else {

						benchmarkSelection.setCinemaMasterDetails(getAllCinemaMasterByScreenType);
						benchmarkSelection.setFormats(Constants.IMAX);
					}
				} else {
					benchmarkSelection.setFormats(Constants.IMAX);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.info("error while fetching imax based on projectId ");
			throw new FeasibilityException("error while fetching imax based on projectId");
		}

		return benchmarkSelection;
	}

	public BenchMarkSelectionMapper getForPxlScreenFormat(ProjectDetails projectDetails) {
		List<BenchMarkSelectionMapper> getCinemaByCinemaFormat = new ArrayList<>();
		BenchMarkSelectionMapper benchmarkSelection = new BenchMarkSelectionMapper();
		try {
			projectDetails.getCinemaFormat().forEach(action -> {

				if (action.getFormatType().equals(Constants.PXL)) {
					List<Navision> getAllNavision = commonAnalysisService.getAllNavisionByComp();
					List<String> navCinemaCode = null;
					List<CinemaMaster> getAllCinemabyNavCode = null;
					if (!ObjectUtils.isEmpty(getAllNavision)) {
						navCinemaCode = getAllNavision.stream().map(Navision::getNavCinemaCode)
								.collect(Collectors.toList());
					}
					if (!ObjectUtils.isEmpty(navCinemaCode)) {
						getAllCinemabyNavCode = cinemaMasterDao.findAllByNavCinemaCodeIn(navCinemaCode);
					}
					List<CinemaMaster> getAllCinemaByCityTier = getAllCinemabyNavCode.stream()
							.filter(x1 -> x1.getCityTier().equals(projectDetails.getProjectLocation().getCityTier()))
							.collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaMasterByScreenType = getAllCinemaByCityTier.stream()
							.filter(x2 -> x2.getPxl() > 0).collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaByNearBy = new ArrayList<>();
					List<CinemaMaster> allDistictCinemaMaster = null;

					// for filter based on latitude longitude more than three cinamas
					if (getAllCinemaMasterByScreenType.size() > 3) {

						for (CinemaMaster cinemaMaster : getAllCinemaMasterByScreenType) {
							double totalDistance = distance(projectDetails.getProjectLocation().getLatitude(),
									projectDetails.getProjectLocation().getLongitude(), cinemaMaster.getLocation()[0],
									cinemaMaster.getLocation()[1]);
							cinemaMaster.setDistance(totalDistance);
							getAllCinemaByNearBy.add(cinemaMaster);
						}
						getAllCinemaByNearBy.sort((d1, d2) -> Double.valueOf(d1.getDistance())
								.compareTo(Double.valueOf(d2.getDistance())));
						allDistictCinemaMaster = getAllCinemaByNearBy.stream().distinct().limit(3)
								.collect(Collectors.toList());

						benchmarkSelection.setCinemaMasterDetails(allDistictCinemaMaster);
						benchmarkSelection.setFormats(Constants.PXL);
					} else {

						benchmarkSelection.setCinemaMasterDetails(getAllCinemaMasterByScreenType);
						benchmarkSelection.setFormats(Constants.PXL);
					}
				} else {
					benchmarkSelection.setFormats(Constants.PXL);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.info("error while fetching pxl based on projectId ");
			throw new FeasibilityException("error while fetching pxl based on projectId");
		}

		return benchmarkSelection;
	}

	public BenchMarkSelectionMapper getForPlayhouseScreenFormat(ProjectDetails projectDetails) {
		List<BenchMarkSelectionMapper> getCinemaByCinemaFormat = new ArrayList<>();
		BenchMarkSelectionMapper benchmarkSelection = new BenchMarkSelectionMapper();
		try {
			projectDetails.getCinemaFormat().forEach(action -> {

				if (action.getFormatType().equals(Constants.PLAYHOUSE)) {
					List<Navision> getAllNavision = commonAnalysisService.getAllNavisionByComp();
					List<String> navCinemaCode = null;
					List<CinemaMaster> getAllCinemabyNavCode = null;
					if (!ObjectUtils.isEmpty(getAllNavision)) {
						navCinemaCode = getAllNavision.stream().map(Navision::getNavCinemaCode)
								.collect(Collectors.toList());
					}
					if (!ObjectUtils.isEmpty(navCinemaCode)) {
						getAllCinemabyNavCode = cinemaMasterDao.findAllByNavCinemaCodeIn(navCinemaCode);
					}
					List<CinemaMaster> getAllCinemaByCityTier = getAllCinemabyNavCode.stream()
							.filter(x1 -> x1.getCityTier().equals(projectDetails.getProjectLocation().getCityTier()))
							.collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaMasterByScreenType = getAllCinemaByCityTier.stream()
							.filter(x2 -> x2.getPlayHouse() > 0).collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaByNearBy = new ArrayList<>();
					List<CinemaMaster> allDistictCinemaMaster = null;

					// for filter based on latitude longitude more than three cinamas
					if (getAllCinemaMasterByScreenType.size() > 3) {

						for (CinemaMaster cinemaMaster : getAllCinemaMasterByScreenType) {
							double totalDistance = distance(projectDetails.getProjectLocation().getLatitude(),
									projectDetails.getProjectLocation().getLongitude(), cinemaMaster.getLocation()[0],
									cinemaMaster.getLocation()[1]);
							cinemaMaster.setDistance(totalDistance);
							getAllCinemaByNearBy.add(cinemaMaster);
						}
						getAllCinemaByNearBy.sort((d1, d2) -> Double.valueOf(d1.getDistance())
								.compareTo(Double.valueOf(d2.getDistance())));
						allDistictCinemaMaster = getAllCinemaByNearBy.stream().distinct().limit(3)
								.collect(Collectors.toList());

						benchmarkSelection.setCinemaMasterDetails(allDistictCinemaMaster);
						benchmarkSelection.setFormats(Constants.PLAYHOUSE);
					} else {

						benchmarkSelection.setCinemaMasterDetails(getAllCinemaMasterByScreenType);
						benchmarkSelection.setFormats(Constants.PLAYHOUSE);
					}
				} else {
					benchmarkSelection.setFormats(Constants.PLAYHOUSE);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.info("error while fetching playhouse based on projectId ");
			throw new FeasibilityException("error while fetching playhouse based on projectId");
		}

		return benchmarkSelection;
	}

	public BenchMarkSelectionMapper getForUltraPremiumScreenFormat(ProjectDetails projectDetails) {
		List<BenchMarkSelectionMapper> getCinemaByCinemaFormat = new ArrayList<>();
		BenchMarkSelectionMapper benchmarkSelection = new BenchMarkSelectionMapper();
		try {
			projectDetails.getCinemaFormat().forEach(action -> {

				if (action.getFormatType().equals(Constants.ULTRAPREMIUM)) {

					List<Navision> getAllNavision = commonAnalysisService.getAllNavisionByComp();

					List<String> navCinemaCode = null;
					List<CinemaMaster> getAllCinemabyNavCode = null;
					if (!ObjectUtils.isEmpty(getAllNavision)) {
						navCinemaCode = getAllNavision.stream().map(Navision::getNavCinemaCode)
								.collect(Collectors.toList());
					}
					if (!ObjectUtils.isEmpty(navCinemaCode)) {
						getAllCinemabyNavCode = cinemaMasterDao.findAllByNavCinemaCodeIn(navCinemaCode);
					}
					List<CinemaMaster> getAllCinemaByCityTier = getAllCinemabyNavCode.stream()
							.filter(x1 -> x1.getCityTier().equals(projectDetails.getProjectLocation().getCityTier()))
							.collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaMasterByScreenType = getAllCinemaByCityTier.stream()
							.filter(x2 -> x2.getUltraPremium() > 0).collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaByNearBy = new ArrayList<>();
					List<CinemaMaster> allDistictCinemaMaster = null;

					// for filter based on latitude longitude more than three cinamas
					if (getAllCinemaMasterByScreenType.size() > 3) {

						for (CinemaMaster cinemaMaster : getAllCinemaMasterByScreenType) {
							double totalDistance = distance(projectDetails.getProjectLocation().getLatitude(),
									projectDetails.getProjectLocation().getLongitude(), cinemaMaster.getLocation()[0],
									cinemaMaster.getLocation()[1]);
							cinemaMaster.setDistance(totalDistance);
							getAllCinemaByNearBy.add(cinemaMaster);
						}
						getAllCinemaByNearBy.sort((d1, d2) -> Double.valueOf(d1.getDistance())
								.compareTo(Double.valueOf(d2.getDistance())));
						allDistictCinemaMaster = getAllCinemaByNearBy.stream().distinct().limit(3)
								.collect(Collectors.toList());

						benchmarkSelection.setCinemaMasterDetails(allDistictCinemaMaster);
						benchmarkSelection.setFormats(Constants.ULTRAPREMIUM);
					} else {

						benchmarkSelection.setCinemaMasterDetails(getAllCinemaMasterByScreenType);
						benchmarkSelection.setFormats(Constants.ULTRAPREMIUM);
					}
				} else {
					benchmarkSelection.setFormats(Constants.ULTRAPREMIUM);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.info("error while fetching ultra-premium based on projectId ");
			throw new FeasibilityException("error while fetching ultra-premium based on projectId");
		}

		return benchmarkSelection;
	}

	public BenchMarkSelectionMapper getForDxScreenFormat(ProjectDetails projectDetails) {
		List<BenchMarkSelectionMapper> getCinemaByCinemaFormat = new ArrayList<>();
		BenchMarkSelectionMapper benchmarkSelection = new BenchMarkSelectionMapper();
		try {
			projectDetails.getCinemaFormat().forEach(action -> {

				if (action.getFormatType().equals(Constants.DX)) {
					List<Navision> getAllNavision = commonAnalysisService.getAllNavisionByComp();
					List<String> navCinemaCode = null;
					List<CinemaMaster> getAllCinemabyNavCode = null;
					if (!ObjectUtils.isEmpty(getAllNavision)) {
						navCinemaCode = getAllNavision.stream().map(Navision::getNavCinemaCode)
								.collect(Collectors.toList());
					}
					if (!ObjectUtils.isEmpty(navCinemaCode)) {
						getAllCinemabyNavCode = cinemaMasterDao.findAllByNavCinemaCodeIn(navCinemaCode);
					}
					List<CinemaMaster> getAllCinemaByCityTier = getAllCinemabyNavCode.stream()
							.filter(x1 -> x1.getCityTier().equals(projectDetails.getProjectLocation().getCityTier()))
							.collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaMasterByScreenType = getAllCinemaByCityTier.stream()
							.filter(x2 -> x2.getDx() > 0).collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaByNearBy = new ArrayList<>();
					List<CinemaMaster> allDistictCinemaMaster = null;

					// for filter based on latitude longitude more than three cinamas
					if (getAllCinemaMasterByScreenType.size() > 3) {

						for (CinemaMaster cinemaMaster : getAllCinemaMasterByScreenType) {
							double totalDistance = distance(projectDetails.getProjectLocation().getLatitude(),
									projectDetails.getProjectLocation().getLongitude(), cinemaMaster.getLocation()[0],
									cinemaMaster.getLocation()[1]);
							cinemaMaster.setDistance(totalDistance);
							getAllCinemaByNearBy.add(cinemaMaster);
						}
						getAllCinemaByNearBy.sort((d1, d2) -> Double.valueOf(d1.getDistance())
								.compareTo(Double.valueOf(d2.getDistance())));
						allDistictCinemaMaster = getAllCinemaByNearBy.stream().distinct().limit(3)
								.collect(Collectors.toList());

						benchmarkSelection.setCinemaMasterDetails(allDistictCinemaMaster);
						benchmarkSelection.setFormats(Constants.DX);
					} else {

						benchmarkSelection.setCinemaMasterDetails(getAllCinemaMasterByScreenType);
						benchmarkSelection.setFormats(Constants.DX);
					}
				} else {
					benchmarkSelection.setFormats(Constants.DX);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.info("error while fetching 4dx based on projectId ");
			throw new FeasibilityException("error while fetching 4dx based on projectId");
		}

		return benchmarkSelection;
	}

	public BenchMarkSelectionMapper getForPremierScreenFormat(ProjectDetails projectDetails) {
		List<BenchMarkSelectionMapper> getCinemaByCinemaFormat = new ArrayList<>();
		BenchMarkSelectionMapper benchmarkSelection = new BenchMarkSelectionMapper();
		try {
			projectDetails.getCinemaFormat().forEach(action -> {

				if (action.getFormatType().equals(Constants.PREMIER)) {
					List<Navision> getAllNavision = commonAnalysisService.getAllNavisionByComp();
					List<String> navCinemaCode = null;
					List<CinemaMaster> getAllCinemabyNavCode = null;
					if (!ObjectUtils.isEmpty(getAllNavision)) {
						navCinemaCode = getAllNavision.stream().map(Navision::getNavCinemaCode)
								.collect(Collectors.toList());
					}
					if (!ObjectUtils.isEmpty(navCinemaCode)) {
						getAllCinemabyNavCode = cinemaMasterDao.findAllByNavCinemaCodeIn(navCinemaCode);
					}
					List<CinemaMaster> getAllCinemaByCityTier = getAllCinemabyNavCode.stream()
							.filter(x1 -> x1.getCityTier().equals(projectDetails.getProjectLocation().getCityTier()))
							.collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaMasterByScreenType = getAllCinemaByCityTier.stream()
							.filter(x2 -> x2.getPremier() > 0).collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaByNearBy = new ArrayList<>();
					List<CinemaMaster> allDistictCinemaMaster = null;

					// for filter based on latitude longitude more than three cinamas
					if (getAllCinemaMasterByScreenType.size() > 3) {

						for (CinemaMaster cinemaMaster : getAllCinemaMasterByScreenType) {
							double totalDistance = distance(projectDetails.getProjectLocation().getLatitude(),
									projectDetails.getProjectLocation().getLongitude(), cinemaMaster.getLocation()[0],
									cinemaMaster.getLocation()[1]);
							cinemaMaster.setDistance(totalDistance);
							getAllCinemaByNearBy.add(cinemaMaster);
						}
						getAllCinemaByNearBy.sort((d1, d2) -> Double.valueOf(d1.getDistance())
								.compareTo(Double.valueOf(d2.getDistance())));
						allDistictCinemaMaster = getAllCinemaByNearBy.stream().distinct().limit(3)
								.collect(Collectors.toList());

						benchmarkSelection.setCinemaMasterDetails(allDistictCinemaMaster);
						benchmarkSelection.setFormats(Constants.PREMIER);
					} else {

						benchmarkSelection.setCinemaMasterDetails(getAllCinemaMasterByScreenType);
						benchmarkSelection.setFormats(Constants.PREMIER);
					}
				} else {
					benchmarkSelection.setFormats(Constants.PREMIER);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.info("error while fetching premier based on projectId ");
			throw new FeasibilityException("error while fetching premier based on projectId");
		}

		return benchmarkSelection;
	}

	public BenchMarkSelectionMapper getForOnyxScreenFormat(ProjectDetails projectDetails) {
		List<BenchMarkSelectionMapper> getCinemaByCinemaFormat = new ArrayList<>();
		BenchMarkSelectionMapper benchmarkSelection = new BenchMarkSelectionMapper();
		try {
			projectDetails.getCinemaFormat().forEach(action -> {

				if (action.getFormatType().equals(Constants.ONYX)) {
					List<Navision> getAllNavision = commonAnalysisService.getAllNavisionByComp();
					List<String> navCinemaCode = null;
					List<CinemaMaster> getAllCinemabyNavCode = null;
					if (!ObjectUtils.isEmpty(getAllNavision)) {
						navCinemaCode = getAllNavision.stream().map(Navision::getNavCinemaCode)
								.collect(Collectors.toList());
					}
					if (!ObjectUtils.isEmpty(navCinemaCode)) {
						getAllCinemabyNavCode = cinemaMasterDao.findAllByNavCinemaCodeIn(navCinemaCode);
					}
					List<CinemaMaster> getAllCinemaByCityTier = getAllCinemabyNavCode.stream()
							.filter(x1 -> x1.getCityTier().equals(projectDetails.getProjectLocation().getCityTier()))
							.collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaMasterByScreenType = getAllCinemaByCityTier.stream()
							.filter(x2 -> x2.getOnxy() > 0).collect(Collectors.toList());

					List<CinemaMaster> getAllCinemaByNearBy = new ArrayList<>();
					List<CinemaMaster> allDistictCinemaMaster = null;

					// for filter based on latitude longitude more than three cinamas
					if (getAllCinemaMasterByScreenType.size() > 3) {

						for (CinemaMaster cinemaMaster : getAllCinemaMasterByScreenType) {
							double totalDistance = distance(projectDetails.getProjectLocation().getLatitude(),
									projectDetails.getProjectLocation().getLongitude(), cinemaMaster.getLocation()[0],
									cinemaMaster.getLocation()[1]);
							cinemaMaster.setDistance(totalDistance);
							getAllCinemaByNearBy.add(cinemaMaster);
						}
						getAllCinemaByNearBy.sort((d1, d2) -> Double.valueOf(d1.getDistance())
								.compareTo(Double.valueOf(d2.getDistance())));
						allDistictCinemaMaster = getAllCinemaByNearBy.stream().distinct().limit(3)
								.collect(Collectors.toList());

						benchmarkSelection.setCinemaMasterDetails(allDistictCinemaMaster);
						benchmarkSelection.setFormats(Constants.ONYX);
					} else {

						benchmarkSelection.setCinemaMasterDetails(getAllCinemaMasterByScreenType);
						benchmarkSelection.setFormats(Constants.ONYX);
					}
				} else {
					benchmarkSelection.setFormats(Constants.ONYX);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.info("error while fetching onyx based on projectId ");
			throw new FeasibilityException("error while fetching onyx based on projectId");
		}

		return benchmarkSelection;
	}

	public BenchMarkSelectionMapper otherScreenFormatEmpty(ProjectDetails projectDetails) {
		List<BenchMarkSelectionMapper> getCinemaByCinemaFormat = new ArrayList<>();
		BenchMarkSelectionMapper benchmarkSelection = new BenchMarkSelectionMapper();
		List<CinemaMaster> getAllCinemaFromAllScreenFormat = new ArrayList<>();
		try {

			List<Navision> getAllNavision = commonAnalysisService.getAllNavisionByComp();
			List<String> navCinemaCode = null;
			List<CinemaMaster> getAllCinemabyNavCode = null;
			if (!ObjectUtils.isEmpty(getAllNavision)) {
				navCinemaCode = getAllNavision.stream().map(Navision::getNavCinemaCode).collect(Collectors.toList());
			}
			if (!ObjectUtils.isEmpty(navCinemaCode)) {
				getAllCinemabyNavCode = cinemaMasterDao.findAllByNavCinemaCodeIn(navCinemaCode);
			}
			List<CinemaMaster> getAllCinemaMasterByScreenType = new ArrayList<>();

			for (CinemaFormat cinemaFormat : projectDetails.getCinemaFormat()) {

				if (cinemaFormat.getFormatType().equals(Constants.GOLD)) {
					getAllCinemaMasterByScreenType = getAllCinemabyNavCode.stream().filter(x2 -> x2.getGold() > 0)
							.collect(Collectors.toList());
					getAllCinemaMasterByScreenType.forEach(action -> action.setScreenType(Constants.GOLD));
					getAllCinemaFromAllScreenFormat.addAll(getAllCinemaMasterByScreenType);

				}
				if (cinemaFormat.getFormatType().equals(Constants.IMAX)) {
					getAllCinemaMasterByScreenType = getAllCinemabyNavCode.stream().filter(x2 -> x2.getImax() > 0)
							.collect(Collectors.toList());
					getAllCinemaMasterByScreenType.forEach(action -> action.setScreenType(Constants.IMAX));
					getAllCinemaFromAllScreenFormat.addAll(getAllCinemaMasterByScreenType);

				}

				if (cinemaFormat.getFormatType().equals(Constants.PXL)) {
					getAllCinemaMasterByScreenType = getAllCinemabyNavCode.stream().filter(x2 -> x2.getPxl() > 0)
							.collect(Collectors.toList());
					getAllCinemaMasterByScreenType.forEach(action -> action.setScreenType(Constants.PXL));
					getAllCinemaFromAllScreenFormat.addAll(getAllCinemaMasterByScreenType);

				}

				if (cinemaFormat.getFormatType().equals(Constants.PLAYHOUSE)) {
					getAllCinemaMasterByScreenType = getAllCinemabyNavCode.stream().filter(x2 -> x2.getPlayHouse() > 0)
							.collect(Collectors.toList());
					getAllCinemaMasterByScreenType.forEach(action -> action.setScreenType(Constants.PLAYHOUSE));
					getAllCinemaFromAllScreenFormat.addAll(getAllCinemaMasterByScreenType);

				}
				if (cinemaFormat.getFormatType().equals(Constants.ULTRAPREMIUM)) {
					getAllCinemaMasterByScreenType = getAllCinemabyNavCode.stream()
							.filter(x2 -> x2.getUltraPremium() > 0).collect(Collectors.toList());

				}
				if (cinemaFormat.getFormatType().equals(Constants.DX)) {
					getAllCinemaMasterByScreenType = getAllCinemabyNavCode.stream().filter(x2 -> x2.getDx() > 0)
							.collect(Collectors.toList());
					getAllCinemaMasterByScreenType.forEach(action -> action.setScreenType(Constants.DX));
					getAllCinemaFromAllScreenFormat.addAll(getAllCinemaMasterByScreenType);

				}
				if (cinemaFormat.getFormatType().equals(Constants.PREMIER)) {
					getAllCinemaMasterByScreenType = getAllCinemabyNavCode.stream().filter(x2 -> x2.getPremier() > 0)
							.collect(Collectors.toList());
					getAllCinemaMasterByScreenType.forEach(action -> action.setScreenType(Constants.PREMIER));
					getAllCinemaFromAllScreenFormat.addAll(getAllCinemaMasterByScreenType);

				}
				if (cinemaFormat.getFormatType().equals(Constants.ONYX)) {
					getAllCinemaMasterByScreenType = getAllCinemabyNavCode.stream().filter(x2 -> x2.getOnxy() > 0)
							.collect(Collectors.toList());
					getAllCinemaMasterByScreenType.forEach(action -> action.setScreenType(Constants.ONYX));
					getAllCinemaFromAllScreenFormat.addAll(getAllCinemaMasterByScreenType);

				}
			}

			// double minDist = 3958.75;
			List<CinemaMaster> getAllCinemaByNearBy = new ArrayList<>();
			CinemaMaster resCity = null;
			getAllCinemaFromAllScreenFormat.stream().distinct();

			for (CinemaMaster cinemaMaster : getAllCinemaFromAllScreenFormat) {
				double totalDistance = distance(projectDetails.getProjectLocation().getLatitude(),
						projectDetails.getProjectLocation().getLongitude(), cinemaMaster.getLocation()[0],
						cinemaMaster.getLocation()[1]);
				cinemaMaster.setDistance(totalDistance);
				getAllCinemaByNearBy.add(cinemaMaster);
			}
			getAllCinemaByNearBy
					.sort((d1, d2) -> Double.valueOf(d1.getDistance()).compareTo(Double.valueOf(d2.getDistance())));
			List<CinemaMaster> allDistictCinemaMaster = getAllCinemaByNearBy.stream().distinct().limit(3)
					.collect(Collectors.toList());

			benchmarkSelection.setFormats("OtherScreenFormatIsEmpty");
			benchmarkSelection.setCinemaMasterDetails(allDistictCinemaMaster);
			getCinemaByCinemaFormat.add(benchmarkSelection);

		} catch (Exception e) {
			e.printStackTrace();
			log.info("error while fetching other-screen-format based on projectId ");
			throw new FeasibilityException("error while fetching other-screen-format based on projectId");
		}
		// benchmarkSelection.setCinemaMasterDetails(getAll);
		return benchmarkSelection;
	}

	private static double distance(double lat1, double lon1, double lat2, double lon2) {
		int R = 6373; // radius of the earth in kilometres
		double lat1rad = Math.toRadians(lat1);
		double lat2rad = Math.toRadians(lat2);
		double deltaLat = Math.toRadians(lat2 - lat1);
		double deltaLon = Math.toRadians(lon2 - lon1);

		double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2)
				+ Math.cos(lat1rad) * Math.cos(lat2rad) * Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		double d = R * c;
		return d;
	}

	@Override
	public long getProjectLegalExpense(String projectId, Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		long projectLegalExpanse = 0;
		long output = 0;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}

		try {

			if (projectDetails.isPresent()) {

//				List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
				List<Navision> navisionData = lastQuarterNonCompDataFinal;
				double averageOfAllScreen = getLegalExpanseAverageOfAllScreen(navisionData);
				projectLegalExpanse = Math.round(averageOfAllScreen) * (long) projectDetails.get().getNoOfScreens();
				output = (long) commonAnalysisService.legalFeeMultiplier(navisionData,
						projectDetails.get().getNoOfScreens(), projectLegalExpanse,
						projectDetails.get().getCinemaFormat());
			}

		} catch (Exception e) {
			log.error("error while fetching project legal expense.", e);
			throw new FeasibilityException("error while fetching project legal expense.");
		}
		return output;
	}

	public static double getLegalExpanseAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());
			// uniqueNavCode.stream().forEach(System.out::println);

			List<Long> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getLegalAndProfessional() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(Math.round(sumAll));
			}
			List<Long> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());

			long sumAllValues = getAllPerScreenAvg.stream().mapToLong(Long::longValue).sum();
			double finalAverageOfAllScreen = (double) sumAllValues / (double) uniqueNavCode.size();

			return finalAverageOfAllScreen;
		} catch (Exception e) {
			log.error("error while calculating legal expense per quater");
			throw new FeasibilityException("error while calculating legal expense per quater");
		}

	}

	@Override
	public long getProjectInsuranceExpense(String projectId, Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		long projectInsuranceExpanse = 0;
		long output = 0;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}

		try {
			if (projectDetails.isPresent()) {
//				List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
				List<Navision> navisionData = lastQuarterNonCompDataFinal;
				if(!ObjectUtils.isEmpty(navisionData)) {
					double averageOfAllScreen = getInsuranceExpenseAverageOfAllScreen(navisionData);
					projectInsuranceExpanse = Math.round(averageOfAllScreen) * (long) projectDetails.get().getNoOfScreens();
					output = (long) commonAnalysisService.insuranceExpenseMultiplier(navisionData,
							projectDetails.get().getNoOfScreens(), projectInsuranceExpanse,
							projectDetails.get().getCinemaFormat());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching project Insurance expense.", e);
			throw new FeasibilityException("error while fetching project Insurance expense.");
		}
		return output;
	}

	public static double getInsuranceExpenseAverageOfAllScreen(List<Navision> allNavisionData) {

		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Long> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getInsuranceExpanse() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(Math.round(sumAll));
			}
			List<Long> allSumOfSingScreen = getAllPerScreenAvg.stream().collect(Collectors.toList());
			/*allSumOfSingScreen.stream().forEach(a->{
				System.out.println(a);
			});*/
			
			long sumAllValues = allSumOfSingScreen.stream().mapToLong(Long::longValue).sum();
			double finalAverageOfAllScreen = sumAllValues / uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating insurance expense per quater..");
			throw new FeasibilityException("error while calculating insurance expense per quater..");
		}
	}

	@Override
	public long getProjectInternetCharges(String projectId, Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		long projectInternetCharges = 0;
		double averageOfAllScreen = 0;
		long output = 0;
		Set<String> navCode = null;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		try {

			if (projectDetails.isPresent()) {

				List<Navision> listFirst = internetExpenseListFirst(projectDetails.get(), lastQuarterNonCompDataFinal);
				navCode = listFirst.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
				if (navCode.size() >= 3) {
					averageOfAllScreen = getInternetChargesAverageOfAllScreen(listFirst);
					projectInternetCharges = Math.round(averageOfAllScreen)
							* (long) projectDetails.get().getNoOfScreens();
					output = (long) commonAnalysisService.internetExpanseMultiplier(listFirst,
							projectDetails.get().getNoOfScreens(), projectInternetCharges,
							projectDetails.get().getCinemaFormat());
				} else {
					List<Navision> listSecond = internetExpenseListSecond(projectDetails.get(), lastQuarterNonCompDataFinal);
					listSecond.addAll(!ObjectUtils.isEmpty(listFirst) ? listFirst : new ArrayList<>());
					navCode = listSecond.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						averageOfAllScreen = getInternetChargesAverageOfAllScreen(listSecond);
						projectInternetCharges = Math.round(averageOfAllScreen)
								* (long) projectDetails.get().getNoOfScreens();
						output = (long) commonAnalysisService.internetExpanseMultiplier(listSecond,
								projectDetails.get().getNoOfScreens(), projectInternetCharges,
								projectDetails.get().getCinemaFormat());
					} else {
						List<Navision> listThird = internetExpenseListThird(projectDetails.get(),lastQuarterNonCompDataFinal);
						listThird.addAll(!ObjectUtils.isEmpty(listSecond) ? listSecond : new ArrayList<>());
						navCode = listThird.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

						if (navCode.size() >= 3) {
							averageOfAllScreen = getInternetChargesAverageOfAllScreen(listThird);
							projectInternetCharges = Math.round(averageOfAllScreen)
									* (long) projectDetails.get().getNoOfScreens();
							output = (long) commonAnalysisService.internetExpanseMultiplier(listThird,
									projectDetails.get().getNoOfScreens(), projectInternetCharges,
									projectDetails.get().getCinemaFormat());
						}

					}
				}

			}

		} catch (Exception e) {
			log.error("error while fetching project Internet Charges.", e);
			throw new FeasibilityException("error while fetching project Internet Charges.");
		}
		return output;
	}

	public List<Navision> internetExpenseListFirst(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterbyProjectInputCity = filteredCompData.stream()
					.filter(predicate -> !ObjectUtils.isEmpty(predicate.getCity())
							? predicate.getCity().equals(projectDetails.getProjectLocation().getCity())
							: false)
					.collect(Collectors.toList());
			return filterbyProjectInputCity;
		} catch (Exception e) {
			log.error("error while calculating internet expense first list..");
			throw new FeasibilityException("error while calculating internet expense first list..");
		}

	}

	public List<Navision> internetExpenseListSecond(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterInMiscellaneous(filteredCompData,
					projectDetails.getProjectLocation().getCityTier());
			return filterByCityTier;
		} catch (Exception e) {
			log.error("error while calculating internet second list");
			throw new FeasibilityException("error while calculating internet second list");
		}

	}

	public List<Navision> internetExpenseListThird(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			return filteredCompData;
		} catch (Exception e) {
			log.error("error while searching internet expense third list");
			throw new FeasibilityException("error while searching internet expense third list");
		}

	}

	public static double getInternetChargesAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Long> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getInternateCharges() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(Math.round(sumAll));
			}
			// List<Double> allSumOfSingScreen =
			// getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());
			long sumAllValues = getAllPerScreenAvg.stream().mapToLong(Long::longValue).sum();
			double finalAverageOfAllScreen = (double) sumAllValues / (double) uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating internet expense per quater..");
			throw new FeasibilityException("error while calculating internet expense per quater..");
		}

	}

	@Override
	public long getProjectTravellingConveyance(String projectId, Optional<ProjectDetails> upcomingProjectDetails,List<Navision> lastQuarterNonCompDataFinal) {
		long projectTravellingConveyance = 0;
		double averageOfAllScreen = 0;
		long output = 0;
		Set<String> navCode = null;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		try {

			if (projectDetails.isPresent()) {

				List<Navision> listFirst = travellingConveyanceListFirst(projectDetails.get(),lastQuarterNonCompDataFinal);
				navCode = listFirst.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
				if (navCode.size() >= 3) {
					averageOfAllScreen = getTravellingConveyanceAverageOfAllScreen(listFirst);
					projectTravellingConveyance = Math.round(averageOfAllScreen)
							* (long) projectDetails.get().getNoOfScreens();
					output = (long) commonAnalysisService.travellingAndConveyanceMultiplier(listFirst,
							projectDetails.get().getNoOfScreens(), projectTravellingConveyance,
							projectDetails.get().getCinemaFormat());
				} else {
					List<Navision> listSecond = travellingConveyanceListSecond(projectDetails.get(),lastQuarterNonCompDataFinal);
					listSecond.addAll(!ObjectUtils.isEmpty(listFirst) ? listFirst : new ArrayList<>());
					navCode = listSecond.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						averageOfAllScreen = getTravellingConveyanceAverageOfAllScreen(listSecond);
						projectTravellingConveyance = Math.round(averageOfAllScreen)
								* (long) projectDetails.get().getNoOfScreens();
						output = (long) commonAnalysisService.travellingAndConveyanceMultiplier(listSecond,
								projectDetails.get().getNoOfScreens(), projectTravellingConveyance,
								projectDetails.get().getCinemaFormat());
					} else {
						List<Navision> listThird = travellingConveyanceListThird(projectDetails.get(),lastQuarterNonCompDataFinal);
						listThird.addAll(!ObjectUtils.isEmpty(listSecond) ? listSecond : new ArrayList<>());
						navCode = listThird.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

						if (navCode.size() >= 3) {
							averageOfAllScreen = getTravellingConveyanceAverageOfAllScreen(listThird);
							projectTravellingConveyance = Math.round(averageOfAllScreen)
									* (long) projectDetails.get().getNoOfScreens();
							output = (long) commonAnalysisService.travellingAndConveyanceMultiplier(listThird,
									projectDetails.get().getNoOfScreens(), projectTravellingConveyance,
									projectDetails.get().getCinemaFormat());

						}

					}
				}
			}

		} catch (Exception e) {
			log.error("error while fetching project Travelling conveyance.", e);
			throw new FeasibilityException("error while fetching project  Travelling conveyance.");
		}
		return output;
	}

	public List<Navision> travellingConveyanceListFirst(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> navisionData = lastQuarterNonCompDataFinal;
			List<Navision> filterbyProjectInputCity = navisionData.stream()
					.filter(predicate -> !ObjectUtils.isEmpty(predicate.getCity())
							? predicate.getCity().equals(projectDetails.getProjectLocation().getCity())
							: false)
					.collect(Collectors.toList());
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterInMiscellaneous(
					filterbyProjectInputCity, projectDetails.getProjectLocation().getCityTier());
			return filterByCityTier;

		} catch (Exception e) {
			log.error("error while searching travelling conveyance first list..");
			throw new FeasibilityException("error while searching travelling conveyance first list..");
		}
	}

	public List<Navision> travellingConveyanceListSecond(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> navisionData = lastQuarterNonCompDataFinal;
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterInMiscellaneous(navisionData,
					projectDetails.getProjectLocation().getCityTier());
			return filterByCityTier;

		} catch (Exception e) {
			log.error("error while searching travelling and conveyance second list..");
			throw new FeasibilityException("error while searching travelling and conveyance second list..");
		}
	}

	public List<Navision> travellingConveyanceListThird(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> navisionData = lastQuarterNonCompDataFinal;
			return navisionData;

		} catch (Exception e) {
			log.error("error while searching travelling and conveyance third list..");
			throw new FeasibilityException("error while searching travelling and conveyance third list..");
		}
	}

	public static double getTravellingConveyanceAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Double> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getTravellingAndConveyance() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(sumAll);
			}
			/*List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());*/
			List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().collect(Collectors.toList());
			double sumAllValues = allSumOfSingScreen.stream().mapToDouble(Double::doubleValue).sum();
			double finalAverageOfAllScreen = sumAllValues / uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating travelling and conveyance per quater..");
			throw new FeasibilityException("error while calculating travelling and conveyance per quater..");
		}
	}

	@Override
	public long getWaterAndElectricity(String projectId, Optional<ProjectDetails> upcomingProjectDetails,List<Navision> lastQuarterNonCompDataFinal) {
		long projectWaterAndElectricity = 0;
		long output = 0;
		double averageOfAllScreen = 0;
		Set<String> navCode = null;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}

		try {

			if (projectDetails.isPresent()) {
				List<Navision> listFirst = getWaterAndElctricityFirstList(projectDetails.get(), lastQuarterNonCompDataFinal);
				navCode = listFirst.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
				if (navCode.size() >= 3) {
					averageOfAllScreen = getWaterAndElectricityAverageOfAllScreen(listFirst);
					output = (long) commonAnalysisService.electricityAndWaterMultiplier(listFirst,
							projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
							projectDetails.get().getCinemaFormat());

				} else {
					List<Navision> listSecond = getWaterAndElctricitySecondList(projectDetails.get(),lastQuarterNonCompDataFinal);
					listSecond.addAll(!ObjectUtils.isEmpty(listFirst) ? listFirst : new ArrayList<>());
					navCode = listSecond.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
					if (navCode.size() >= 3) {
						averageOfAllScreen = getWaterAndElectricityAverageOfAllScreen(listSecond);
						output = (long) commonAnalysisService.electricityAndWaterMultiplier(listSecond,
								projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
								projectDetails.get().getCinemaFormat());

					} else {
						List<Navision> listThird = getWaterAndElctricityThirdist(projectDetails.get(), lastQuarterNonCompDataFinal);
						listThird.addAll(!ObjectUtils.isEmpty(listSecond) ? listSecond : new ArrayList<>());
						navCode = listThird.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
						if (navCode.size() >= 3) {
							averageOfAllScreen = getWaterAndElectricityAverageOfAllScreen(listThird);
							output = (long) commonAnalysisService.electricityAndWaterMultiplier(listThird,
									projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
									projectDetails.get().getCinemaFormat());

						} else {
							List<Navision> listFour = getWaterAndElctricityFourList(projectDetails.get(), lastQuarterNonCompDataFinal);
							listFour.addAll(!ObjectUtils.isEmpty(listThird) ? listThird : new ArrayList<>());
							navCode = listFour.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
							// System.out.println(navCode);
							if (navCode.size() >= 3) {
								averageOfAllScreen = getWaterAndElectricityAverageOfAllScreen(listFour);
								output = (long) commonAnalysisService.electricityAndWaterMultiplier(listFour,
										projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
										projectDetails.get().getCinemaFormat());

							}
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("error while fetching project water and electricity.", e);
			throw new FeasibilityException("error while fetching project water and electricity.");
		}

		return output;
	}

	List<Navision> getWaterAndElctricityFirstList(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> navisionData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = navisionData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);
			List<Navision> imaxScreenFilter = commonAnalysisService.getImaxScreenFilter(goldScreenFilter);
			List<Navision> dxScreenFilter = commonAnalysisService.get4dxScreenFilter(imaxScreenFilter);
			List<Navision> filterState = dxScreenFilter.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());
			List<Navision> filterNoOfScreen = getAllWaterAndElectricityNavisionbyNoOfScreenInputRangeFilter(
					projectDetails, filterState);
			return filterNoOfScreen;
		} catch (Exception e) {
			log.error("error while searching water and elctricity list first");
			throw new FeasibilityException("error while searching water and elctricity list first");
		}
	}

	List<Navision> getWaterAndElctricitySecondList(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> navisionData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = navisionData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);
			List<Navision> imaxScreenFilter = commonAnalysisService.getImaxScreenFilter(goldScreenFilter);
			List<Navision> dxScreenFilter = commonAnalysisService.get4dxScreenFilter(imaxScreenFilter);
			List<Navision> filterState = dxScreenFilter.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());

			return filterState;
		} catch (Exception e) {
			log.error("error while searching water and elctricity list second");
			throw new FeasibilityException("error while searching water and elctricity list second");
		}
	}

	List<Navision> getWaterAndElctricityThirdist(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> navisionData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = navisionData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> filterNoOfScreen = new ArrayList<>();
			if (!ObjectUtils.isEmpty(filterOutSingleScreen)) {

				List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);
				List<Navision> imaxScreenFilter = commonAnalysisService.getImaxScreenFilter(goldScreenFilter);
				List<Navision> dxScreenFilter = commonAnalysisService.get4dxScreenFilter(imaxScreenFilter);

				List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterInMiscellaneous(dxScreenFilter,
						projectDetails.getProjectLocation().getCityTier());

				filterNoOfScreen = getAllWaterAndElectricityNavisionbyNoOfScreenInputRangeFilter(projectDetails,
						filterByCityTier);
			}
			return filterNoOfScreen;
		} catch (Exception e) {
			log.error("error while searching water and elctricity list Third");
			throw new FeasibilityException("error while searching water and elctricity list third");
		}
	}

	List<Navision> getWaterAndElctricityFourList(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> navisionData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = navisionData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);
			List<Navision> imaxScreenFilter = commonAnalysisService.getImaxScreenFilter(goldScreenFilter);
			List<Navision> dxScreenFilter = commonAnalysisService.get4dxScreenFilter(imaxScreenFilter);
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterInMiscellaneous(dxScreenFilter,
					projectDetails.getProjectLocation().getCityTier());
			// filterByCityTier.stream().map(Navision::getNavCinemaCode).distinct().forEach(System.out::println);
			return filterByCityTier;
		} catch (Exception e) {
			log.error("error while searching water and elctricity list four");
			throw new FeasibilityException("error while searching water and elctricity list four");
		}
	}

	public static double getWaterAndElectricityAverageOfAllScreen(List<Navision> allNavisionData) {

		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Long> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getElectricAndWaterExpense() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(Math.round(sumAll));
			}
			// List<Double> allSumOfSingScreen =
			// getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());

			long sumAllValues = getAllPerScreenAvg.stream().mapToLong(Long::longValue).sum();
			double finalAverageOfAllScreen = (double) sumAllValues / (double) uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating water and elctricity per quater..");
			throw new FeasibilityException("error while calculating water and elctricity per quater..");
		}
	}

	@Override
	public long getPrintingAndStationary(String projectId, Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		long projectPrintingAndStationary = 0;
		long output = 0;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		try {

			if (projectDetails.isPresent()) {

//				List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
				List<Navision> navisionData = lastQuarterNonCompDataFinal;
				List<Navision> filterOutSingleScreen = navisionData.stream()
						.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());

				double averageOfAllScreen = getPrintingAndStationaryAverageOfAllScreen(filterOutSingleScreen);
				projectPrintingAndStationary = Math.round(averageOfAllScreen)
						* (long) projectDetails.get().getNoOfScreens();
				output = (long) commonAnalysisService.printingAndStationayMultiplier(filterOutSingleScreen,
						projectDetails.get().getNoOfScreens(), projectPrintingAndStationary,
						projectDetails.get().getCinemaFormat());

			}

		} catch (Exception e) {
			log.error("error while fetching project printing and stationary.", e);
			throw new FeasibilityException("error while fetching project printing and stationary.");
		}
		return output;
	}

	public static double getPrintingAndStationaryAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Double> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getPrintingAndStationary() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(sumAll);
			}
			//List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());
			List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());
			double sumAllValues = allSumOfSingScreen.stream().mapToDouble(Double::doubleValue).sum();
			double finalAverageOfAllScreen = sumAllValues / uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating printing and stationary per quater..");
			throw new FeasibilityException("error while calculating printing and stationary per quater..");
		}
	}

	@Override
	public long getProjectPersonnalExpanses(String projectId, Optional<ProjectDetails> upcomingProjectDetails,List<Navision> lastQuarterNonCompDataFinal) {
		long personnalExpense = 0;
		long output = 0;
		Set<String> navCode = null;
		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}

		try {

			if (projectDetails.isPresent()) {
				List<Navision> filterListFirst = getFirstListPersonalExpense(projectDetails.get(),lastQuarterNonCompDataFinal);
				navCode = filterListFirst.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

				if (navCode.size() >= 1) {
					double averageOfAllScreen = getPersonnalExpanseAverageOfAllScreen(filterListFirst);
					output = (long) commonAnalysisService.personnalCostMultiplier(filterListFirst,
							projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
							projectDetails.get().getCinemaFormat(), projectDetails.get().getCinemaCategory());
				} else {
					List<Navision> filterListSecond = getSecondListPersonalExpense(projectDetails.get(),lastQuarterNonCompDataFinal);
					filterListSecond
							.addAll(!ObjectUtils.isEmpty(filterListFirst) ? filterListFirst : new ArrayList<>());
					navCode = filterListSecond.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 1) {
						double averageOfAllScreen = getPersonnalExpanseAverageOfAllScreen(filterListSecond);
						output = (long) commonAnalysisService.personnalCostMultiplier(filterListSecond,
								projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
								projectDetails.get().getCinemaFormat(), projectDetails.get().getCinemaCategory());
					} else {
						List<Navision> filterListThird = getThirdListPersonalExpense(projectDetails.get(),lastQuarterNonCompDataFinal);
						filterListThird
								.addAll(!ObjectUtils.isEmpty(filterListSecond) ? filterListSecond : new ArrayList<>());
						navCode = filterListThird.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

						if (navCode.size() >= 1) {
							double averageOfAllScreen = getPersonnalExpanseAverageOfAllScreen(filterListThird);
							output = (long) commonAnalysisService.personnalCostMultiplier(filterListThird,
									projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
									projectDetails.get().getCinemaFormat(), projectDetails.get().getCinemaCategory());
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("error while fetching project personnal expanse.", e);
			throw new FeasibilityException("error while fetching project personnal expanse.");
		}
		return output;
	}

	public List<Navision> getFirstListPersonalExpense(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> getCompNavison = commonAnalysisService.getAllNavisionByComp();
			List<Navision> getCompNavison = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = getCompNavison.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());

			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);

			List<Navision> filterIconCategory = commonAnalysisService.filterOutByIcon(goldScreenFilter);

			List<Navision> filterState = filterIconCategory.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterPersonnalExpense(filterState,
					projectDetails.getProjectLocation().getCityTier());

			List<Navision> filterOutNoOfScreenRange = getAllPersonalCostNavisionbyNoOfScreenInputRangeFilter(
					projectDetails, filterByCityTier);

			return filterOutNoOfScreenRange;

		} catch (Exception e) {
			log.error("error while searching personnal expense first list...");
			throw new FeasibilityException("error while searching personnal expense first list...");
		}
	}

	public List<Navision> getSecondListPersonalExpense(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> getCompNavison = commonAnalysisService.getAllNavisionByComp();
			List<Navision> getCompNavison = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = getCompNavison.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());

			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);

			List<Navision> filterIconCategory = commonAnalysisService.filterOutByIcon(goldScreenFilter);

			List<Navision> filterState = filterIconCategory.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());

			List<Navision> filterOutNoOfScreenRange = getAllPersonalCostNavisionbyNoOfScreenInputRangeFilter(
					projectDetails, filterState);

			return filterOutNoOfScreenRange;

		} catch (Exception e) {
			log.error("error while searching personnal expense second list..");
			throw new FeasibilityException("error while searching personnal expense second list..");
		}
	}

	public List<Navision> getThirdListPersonalExpense(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> getCompNavison = commonAnalysisService.getAllNavisionByComp();
			List<Navision> getCompNavison = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = getCompNavison.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());

			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);

			List<Navision> filterIconCategory = commonAnalysisService.filterOutByIcon(goldScreenFilter);

			List<Navision> filterState = filterIconCategory.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());

			return filterState;

		} catch (Exception e) {
			log.error("error while searching personnal expense third list..");
			throw new FeasibilityException("error while searching personnal expense third list..");
		}
	}

	public static double getPersonnalExpanseAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Double> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getPersonalExpense() / (double) Double.valueOf(na.getNoOfScreen());
					sumAll += fee;
				}
				getAllPerScreenAvg.add(sumAll);
			}
			//List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());
			List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().collect(Collectors.toList());
			double sumAllValues = allSumOfSingScreen.stream().mapToDouble(Double::doubleValue).sum();
			double finalAverageOfAllScreen = sumAllValues / uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating  personnal expense per quater..", e);
			throw new FeasibilityException("error while calculating  personnal expense per quater..", e);
		}
	}

	public List<Navision> getAllPersonalCostNavisionbyNoOfScreenInputRangeFilter(ProjectDetails projectDetails,
			List<Navision> filterState) {
		List<Navision> updateNavision = new ArrayList<>();
		int noOfScreenUserInput = projectDetails.getNoOfScreens();
		int initialValue = getPersonnalCostInitialValue(projectDetails.getNoOfScreens());
		int lastValue = getPersonalCostLastValue(projectDetails.getNoOfScreens());
		for (Navision navision : filterState) {
			if ((navision.getNoOfScreen() >= initialValue) && (navision.getNoOfScreen() <= lastValue)) {
				updateNavision.add(navision);
			}
		}
		return updateNavision;
	}

	public static int getInitialValue(int no) {
		int firstCondition = 0;
		if (no == 1) {
			firstCondition = 1;
		} else if (no == 2) {
			firstCondition = 2;
		} else if (no == 3) {
			firstCondition = 2;
		} else if (no == 4) {
			firstCondition = 2;
		} else if (no == 5) {
			firstCondition = 3;
		} else if (no == 6) {
			firstCondition = 4;
		} else if (no == 7) {
			firstCondition = 4;
		} else if (no == 8) {
			firstCondition = 4;
		} else if (no == 9) {
			firstCondition = 4;
		} else if (no == 10) {
			firstCondition = 4;
		} else if (no == 11) {
			firstCondition = 6;
		} else if (no == 12) {
			firstCondition = 6;
		} else if (no == 13) {
			firstCondition = 6;
		} else if (no == 14) {
			firstCondition = 6;
		} else if (no == 15) {
			firstCondition = 6;
		} else if (no == 16) {
			firstCondition = 6;
		} else if (no == 17) {
			firstCondition = 6;
		} else if (no == 18) {
			firstCondition = 6;
		} else if (no == 19) {
			firstCondition = 6;
		} else if (no == 20) {
			firstCondition = 6;
		} else if (no == 21) {
			firstCondition = 6;
		} else if (no == 22) {
			firstCondition = 6;
		} else if (no == 23) {
			firstCondition = 6;
		} else if (no == 24) {
			firstCondition = 6;
		} else if (no == 25) {
			firstCondition = 6;
		} else if (no == 26) {
			firstCondition = 6;
		} else if (no == 27) {
			firstCondition = 6;
		} else if (no == 28) {
			firstCondition = 6;
		} else if (no == 29) {
			firstCondition = 6;
		} else if (no == 30) {
			firstCondition = 6;
		}
		return firstCondition;
	}

	public static int getLastValue(int no) {
		int lastCondition = 0;
		if (no == 1) {
			lastCondition = 1;
		} else if (no == 2) {
			lastCondition = 4;
		} else if (no == 3) {
			lastCondition = 5;
		} else if (no == 4) {
			lastCondition = 6;
		} else if (no == 5) {
			lastCondition = 7;
		} else if (no == 6) {
			lastCondition = 8;
		} else if (no == 7) {
			lastCondition = 9;
		} else if (no == 8) {
			lastCondition = 10;
		} else if (no == 9) {
			lastCondition = 11;
		} else if (no == 10) {
			lastCondition = 12;
		} else if (no == 11) {
			lastCondition = 20;
		} else if (no == 12) {
			lastCondition = 20;
		} else if (no == 13) {
			lastCondition = 20;
		} else if (no == 14) {
			lastCondition = 20;
		} else if (no == 15) {
			lastCondition = 20;
		} else if (no == 16) {
			lastCondition = 20;
		} else if (no == 17) {
			lastCondition = 20;
		} else if (no == 18) {
			lastCondition = 20;
		} else if (no == 19) {
			lastCondition = 20;
		} else if (no == 20) {
			lastCondition = 20;
		} else if (no == 21) {
			lastCondition = 20;
		} else if (no == 22) {
			lastCondition = 20;
		} else if (no == 23) {
			lastCondition = 20;
		} else if (no == 24) {
			lastCondition = 20;
		} else if (no == 25) {
			lastCondition = 20;
		} else if (no == 26) {
			lastCondition = 20;
		} else if (no == 27) {
			lastCondition = 20;
		} else if (no == 28) {
			lastCondition = 20;
		} else if (no == 29) {
			lastCondition = 20;
		} else if (no == 30) {
			lastCondition = 20;
		}

		return lastCondition;
	}

	@Override
	public long getProjectHousekeepingExpense(String projectId, Optional<ProjectDetails> upcomingProjectDetails,List<Navision> lastQuarterNonCompDataFinal) {
		long housekeepingExpense = 0;
		double averageOfAllScreen = 0;
		long output = 0;
		Set<String> navCode = null;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		boolean flag = false;
		try {

			if (projectDetails.isPresent()) {

				List<Navision> filterList1 = getFirstListForHouseKeeping(projectDetails.get(),lastQuarterNonCompDataFinal);
				navCode = filterList1.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

				if (navCode.size() >= 3) {
					flag = false;
					averageOfAllScreen = getHouseKeepingExpenseAverageOfAllScreen(filterList1);
					output = (long) commonAnalysisService.houseKeepingMultiplier(filterList1,
							projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
							projectDetails.get().getCinemaFormat(), projectDetails.get().getCinemaCategory(), flag);
				} else {
					List<Navision> filterList2 = getSecondListForHouseKeeping(projectDetails.get(),lastQuarterNonCompDataFinal);
					filterList2.addAll(!ObjectUtils.isEmpty(filterList1) ? filterList1 : new ArrayList<>());
					navCode = filterList2.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						flag = false;
						averageOfAllScreen = getHouseKeepingExpenseAverageOfAllScreen(filterList2);
						output = (long) commonAnalysisService.houseKeepingMultiplier(filterList2,
								projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
								projectDetails.get().getCinemaFormat(), projectDetails.get().getCinemaCategory(), flag);
					} else {
						List<Navision> filterList3 = getThirdListForHouseKeeping(projectDetails.get(),lastQuarterNonCompDataFinal);
						filterList3.addAll(!ObjectUtils.isEmpty(filterList2) ? filterList2 : new ArrayList<>());
						navCode = filterList3.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

						if (navCode.size() >= 3) {
							flag = false;
							averageOfAllScreen = getHouseKeepingExpenseAverageOfAllScreen(filterList3);
							output = (long) commonAnalysisService.houseKeepingMultiplier(filterList3,
									projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
									projectDetails.get().getCinemaFormat(), projectDetails.get().getCinemaCategory(),
									flag);
						} else {

							List<Navision> filterList4 = getFourListForHouseKeeping(projectDetails.get(),lastQuarterNonCompDataFinal);
							filterList4.addAll(!ObjectUtils.isEmpty(filterList3) ? filterList3 : new ArrayList<>());
							navCode = filterList4.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

							if (navCode.size() >= 3) {
								flag = true;
								averageOfAllScreen = getHouseKeepingExpenseAverageOfAllScreen(filterList4);
								output = (long) commonAnalysisService.houseKeepingMultiplier(filterList4,
										projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
										projectDetails.get().getCinemaFormat(),
										projectDetails.get().getCinemaCategory(), flag);
							} else {
								List<Navision> filterList5 = getFiveListForHouseKeeping(projectDetails.get(),lastQuarterNonCompDataFinal);
								filterList5.addAll(!ObjectUtils.isEmpty(filterList4) ? filterList4 : new ArrayList<>());
								navCode = filterList5.stream().map(Navision::getNavCinemaCode)
										.collect(Collectors.toSet());

								if (navCode.size() >= 3) {
									flag = false;
									averageOfAllScreen = getHouseKeepingExpenseAverageOfAllScreen(filterList5);
									output = (long) commonAnalysisService.houseKeepingMultiplier(filterList5,
											projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
											projectDetails.get().getCinemaFormat(),
											projectDetails.get().getCinemaCategory(), flag);

								} else {
									List<Navision> filterList6 = getSixListForHouseKeeping(projectDetails.get(),lastQuarterNonCompDataFinal);
									filterList6.addAll(
											!ObjectUtils.isEmpty(filterList5) ? filterList5 : new ArrayList<>());
									navCode = filterList6.stream().map(Navision::getNavCinemaCode)
											.collect(Collectors.toSet());

									if (navCode.size() >= 3) {
										flag = false;
										averageOfAllScreen = getHouseKeepingExpenseAverageOfAllScreen(filterList6);
										output = (long) commonAnalysisService.houseKeepingMultiplier(filterList6,
												projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
												projectDetails.get().getCinemaFormat(),
												projectDetails.get().getCinemaCategory(), flag);

									} else {

										List<Navision> filterList7 = getSevenListForHouseKeeping(projectDetails.get(), lastQuarterNonCompDataFinal);
										filterList7.addAll(
												!ObjectUtils.isEmpty(filterList6) ? filterList6 : new ArrayList<>());
										navCode = filterList7.stream().map(Navision::getNavCinemaCode)
												.collect(Collectors.toSet());

										if (navCode.size() >= 3) {
											flag = true;
											averageOfAllScreen = getHouseKeepingExpenseAverageOfAllScreen(filterList7);
											output = (long) commonAnalysisService.houseKeepingMultiplier(filterList7,
													projectDetails.get().getNoOfScreens(),
													Math.round(averageOfAllScreen),
													projectDetails.get().getCinemaFormat(),
													projectDetails.get().getCinemaCategory(), flag);
										}
									}
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("error while fetching project house keeping expanse.", e);
			throw new FeasibilityException("error while fetching project house keeping expanse.");
		}
		return output;
	}

	public static double getHouseKeepingExpenseAverageOfAllScreen(List<Navision> allNavisionData) {

		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Long> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getHouseKeepingCharges() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(Math.round(sumAll));
			}
			// List<Long> allSumOfSingScreen =
			// getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());

			long sumAllValues = getAllPerScreenAvg.stream().mapToLong(Long::longValue).sum();
			double finalAverageOfAllScreen = sumAllValues / uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating house keeping per quater..");
			throw new FeasibilityException("error while calculating house keeping per quater..");
		}
	}

	public List<Navision> getFirstListForHouseKeeping(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterByProjectCategory = commonAnalysisService.filterBycategory(filteredCompData,
					projectDetails.getCinemaCategory());
			List<Navision> filterByGoldScreen = commonAnalysisService.getGoldScreenFilter(filterByProjectCategory);
			List<Navision> filterByStateInput = filterByGoldScreen.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());
			List<Navision> filterByNoOfScreenFilter = getAllHouseKeepingCostNavisionbyNoOfScreenInputRangeFilter(
					projectDetails, filterByStateInput);
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterHouseKeeping(
					filterByNoOfScreenFilter, projectDetails.getProjectLocation().getCityTier());
			return filterByCityTier;

		} catch (Exception e) {
			log.error("error while searching house keeping first list", e);
			throw new FeasibilityException("error while searching house keeping first list", e);
		}
	}

	public List<Navision> getSecondListForHouseKeeping(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterByProjectCategory = commonAnalysisService.filterBycategory(filteredCompData,
					projectDetails.getCinemaCategory());
			List<Navision> filterByGoldScreen = commonAnalysisService.getGoldScreenFilter(filterByProjectCategory);
			List<Navision> filterByStateInput = filterByGoldScreen.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());
			List<Navision> filterByNoOfScreenFilter = getAllHouseKeepingCostNavisionbyNoOfScreenInputRangeFilter(
					projectDetails, filterByStateInput);
			return filterByNoOfScreenFilter;

		} catch (Exception e) {
			log.error("error while searching house keeping second list", e);
			throw new FeasibilityException("error while searching house keeping second list", e);
		}

	}

	public List<Navision> getThirdListForHouseKeeping(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterByProjectCategory = commonAnalysisService.filterBycategory(filteredCompData,
					projectDetails.getCinemaCategory());
			List<Navision> filterByGoldScreen = commonAnalysisService.getGoldScreenFilter(filterByProjectCategory);
			List<Navision> filterByStateInput = filterByGoldScreen.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());

			return filterByStateInput;

		} catch (Exception e) {
			log.error("error while searching house keeping third list", e);
			throw new FeasibilityException("error while searching house keeping third list", e);
		}

	}

	public List<Navision> getFourListForHouseKeeping(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterByGoldScreen = commonAnalysisService.getGoldScreenFilter(filteredCompData);
			List<Navision> filterByStateInput = filterByGoldScreen.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());

			return filterByStateInput;

		} catch (Exception e) {
			log.error("error while searching house keeping fourth list", e);
			throw new FeasibilityException("error while searching house keeping fourth list", e);
		}
	}

	public List<Navision> getFiveListForHouseKeeping(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterByProjectCategory = commonAnalysisService.filterBycategory(filteredCompData,
					projectDetails.getCinemaCategory());
			List<Navision> filterByGoldScreen = commonAnalysisService.getGoldScreenFilter(filterByProjectCategory);
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterHouseKeeping(filterByGoldScreen,
					projectDetails.getProjectLocation().getCityTier());
			List<Navision> filterByNoOfScreenFilter = getAllHouseKeepingCostNavisionbyNoOfScreenInputRangeFilter(
					projectDetails, filterByCityTier);

			return filterByNoOfScreenFilter;

		} catch (Exception e) {
			log.error("error while searching house keeping five list", e);
			throw new FeasibilityException("error while searching house keeping five list", e);
		}
	}

	public List<Navision> getSixListForHouseKeeping(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterByProjectCategory = commonAnalysisService.filterBycategory(filteredCompData,
					projectDetails.getCinemaCategory());
			List<Navision> filterByGoldScreen = commonAnalysisService.getGoldScreenFilter(filterByProjectCategory);
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterHouseKeeping(filterByGoldScreen,
					projectDetails.getProjectLocation().getCityTier());

			return filterByCityTier;

		} catch (Exception e) {
			log.error("error while searching house keeping six list", e);
			throw new FeasibilityException("error while searching house keeping six list", e);
		}
	}

	public List<Navision> getSevenListForHouseKeeping(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterByGoldScreen = commonAnalysisService.getGoldScreenFilter(filteredCompData);
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterHouseKeeping(filterByGoldScreen,
					projectDetails.getProjectLocation().getCityTier());
			return filterByCityTier;

		} catch (Exception e) {
			log.error("error while searching house keeping seven list", e);
			throw new FeasibilityException("error while searching house keeping seven list", e);
		}
	}

	@Override
	public long getProjectSecurityExpense(String projectId, Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		long securityExpanseDetails = 0;
		double averageOfAllScreen = 0;
		long output = 0;
		Set<String> navCode = null;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}

		try {

			if (projectDetails.isPresent()) {
				List<Navision> filterList1 = getFirstListForSecurityExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
				navCode = filterList1.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
				if (navCode.size() >= 3) {
					averageOfAllScreen = getSecurityExpanseAverageOfAllScreen(filterList1);
					securityExpanseDetails = Math.round(averageOfAllScreen)
							* (long) projectDetails.get().getNoOfScreens();
					output = (long) commonAnalysisService.securityMultiplier(filterList1,
							projectDetails.get().getNoOfScreens(), securityExpanseDetails,
							projectDetails.get().getCinemaFormat());
				} else {
					List<Navision> filterList2 = getSecondListForSecurityExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
					filterList2.addAll(!ObjectUtils.isEmpty(filterList1) ? filterList1 : new ArrayList<>());
					navCode = filterList2.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						averageOfAllScreen = getSecurityExpanseAverageOfAllScreen(filterList2);
						securityExpanseDetails = Math.round(averageOfAllScreen)
								* (long) projectDetails.get().getNoOfScreens();
						output = (long) commonAnalysisService.securityMultiplier(filterList2,
								projectDetails.get().getNoOfScreens(), securityExpanseDetails,
								projectDetails.get().getCinemaFormat());
					} else {
						List<Navision> filterList3 = getThirdListForSecurityExpanse(projectDetails.get(), lastQuarterNonCompDataFinal);
						filterList3.addAll(!ObjectUtils.isEmpty(filterList2) ? filterList2 : new ArrayList<>());
						navCode = filterList3.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
						// System.out.println(navCode);
						if (navCode.size() >= 3) {
							averageOfAllScreen = getSecurityExpanseAverageOfAllScreen(filterList3);
							securityExpanseDetails = Math.round(averageOfAllScreen)
									* (long) projectDetails.get().getNoOfScreens();
							output = (long) commonAnalysisService.securityMultiplier(filterList3,
									projectDetails.get().getNoOfScreens(), securityExpanseDetails,
									projectDetails.get().getCinemaFormat());
						}
					}
				}

			}

		} catch (Exception e) {
			log.error("error while fetching project security expanse.", e);
			throw new FeasibilityException("error while fetching project security expanse.");
		}
		return output;
	}

	public static double getSecurityExpanseAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Double> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getSecurityCharges() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(sumAll);
			}
			//List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());
			
			List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().collect(Collectors.toList());
			double sumAllValues = allSumOfSingScreen.stream().mapToDouble(Double::doubleValue).sum();
			double finalAverageOfAllScreen = sumAllValues / uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating security expense per quater..", e);
			throw new FeasibilityException("error while calculating security expense per quater..", e);
		}
	}

	public List<Navision> getFirstListForSecurityExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> filterByStateInput = filterOutSingleScreen.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterInSecurity(filterByStateInput,
					projectDetails.getProjectLocation().getCityTier());
			return filterByCityTier;

		} catch (Exception e) {
			log.error("error while searching security expense first list..", e);
			throw new FeasibilityException("error while searching security expense first list..", e);
		}
	}

	public List<Navision> getSecondListForSecurityExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> filterByStateInput = filterOutSingleScreen.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());

			return filterByStateInput;

		} catch (Exception e) {
			log.error("error while searching security expense second list..", e);
			throw new FeasibilityException("error while searching security expense second list..", e);
		}

	}

	public List<Navision> getThirdListForSecurityExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> filterByCityTier = commonAnalysisService.getByCityFilterInSecurity(filterOutSingleScreen,
					projectDetails.getProjectLocation().getCityTier());
			return filterByCityTier;

		} catch (Exception e) {
			log.error("error while searching  security expense third list..", e);
			throw new FeasibilityException("error while searching security expense third list..", e);
		}

	}

	@Override
	public long getProjectRepairAndMaintainceExpense(String projectId,
			Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		long repairAndMaintanceDetails = 0;
		double averageOfAllScreen = 0;
		long output = 0;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		try {
			if (projectDetails.isPresent()) {
//				List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
				List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
				List<Navision> filterOutSingleScreen = filteredCompData.stream()
						.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
				List<Navision> filterOutGoldSingleScreenIcon = commonAnalysisService
						.getGoldScreenFilter(filterOutSingleScreen);
				List<Navision> filterOutIcon = commonAnalysisService.filterOutByIcon(filterOutGoldSingleScreenIcon);
				averageOfAllScreen = getRepairMaintanceAverageOfAllScreen(filterOutIcon);
				output = (long) commonAnalysisService.randmMultiplier(filterOutGoldSingleScreenIcon,
						projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
						projectDetails.get().getCinemaFormat(), projectDetails.get().getCinemaCategory());
			}

		} catch (Exception e) {
			log.error("error while fetching project repair and maintence expanse.", e);
			throw new FeasibilityException("error while fetching project repair and maintence expanse.");
		}
		return output;
	}

	public static double getRepairMaintanceAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Double> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getRAndmIt() / (double) na.getNoOfScreen();
					double fee1 = (double) na.getRAndMFb() / (double) na.getNoOfScreen();
					double fee2 = (double) na.getRAndmEngg() / (double) na.getNoOfScreen();
					double feeSum = fee + fee1 + fee2;
					sumAll += feeSum;
				}
				getAllPerScreenAvg.add(sumAll);
			}
			//List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());
			List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().collect(Collectors.toList());
			
			double sumAllValues = allSumOfSingScreen.stream().mapToDouble(a -> a.doubleValue()).sum();
			double finalAverageOfAllScreen = sumAllValues / uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating rapair and maintaince per quater..", e);
			throw new FeasibilityException("error while calculating rapair and maintaince per quater..", e);
		}
	}

	public static Double incassoMargherita(List<Double> finalList) {
		double sum = 0;
		for (Double d : finalList)
			sum += d;
		return sum;
	}

	@Override
	public long getProjectMarketingExpense(String projectId, Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		long marketingExpensense = 0;
		double averageOfAllScreen = 0;
		long output = 0;
		Set<String> navCode = null;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		try {

			if (projectDetails.isPresent()) {
				List<Navision> filterList1 = getFirstListForMarketingExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
				navCode = filterList1.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
				if (navCode.size() >= 3) {
					averageOfAllScreen = getMarketingExpenseAverageOfAllScreen(filterList1);
					output = (long) commonAnalysisService.marketingMultiplier(filterList1,
							projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
							projectDetails.get().getCinemaFormat(), projectDetails.get().getCinemaCategory());

				} else {
					List<Navision> filterList2 = getSecondListForMarketingExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
					filterList2.addAll(!ObjectUtils.isEmpty(filterList1) ? filterList1 : new ArrayList<>());
					navCode = filterList2.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						averageOfAllScreen = getMarketingExpenseAverageOfAllScreen(filterList2);
						output = (long) commonAnalysisService.marketingMultiplier(filterList2,
								projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
								projectDetails.get().getCinemaFormat(), projectDetails.get().getCinemaCategory());
					} else {
						List<Navision> filterList3 = getThirdListForMarketingExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
						filterList3.addAll(!ObjectUtils.isEmpty(filterList2) ? filterList2 : new ArrayList<>());
						navCode = filterList3.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

						if (navCode.size() >= 3) {
							averageOfAllScreen = getMarketingExpenseAverageOfAllScreen(filterList3);
							output = (long) commonAnalysisService.marketingMultiplier(filterList3,
									projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
									projectDetails.get().getCinemaFormat(), projectDetails.get().getCinemaCategory());
						} else {
							List<Navision> filterList4 = getFourListForMarketingExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
							filterList4.addAll(!ObjectUtils.isEmpty(filterList3) ? filterList3 : new ArrayList<>());
							navCode = filterList4.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

							if (navCode.size() >= 3) {
								averageOfAllScreen = getMarketingExpenseAverageOfAllScreen(filterList4);
								output = (long) commonAnalysisService.marketingMultiplier(filterList4,
										projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
										projectDetails.get().getCinemaFormat(),
										projectDetails.get().getCinemaCategory());
							} else {
								List<Navision> filterList5 = getFiveListForMarketingExpanse(projectDetails.get(), lastQuarterNonCompDataFinal);
								filterList5.addAll(!ObjectUtils.isEmpty(filterList4) ? filterList4 : new ArrayList<>());
								navCode = filterList5.stream().map(Navision::getNavCinemaCode)
										.collect(Collectors.toSet());
								if (navCode.size() >= 3) {
									averageOfAllScreen = getMarketingExpenseAverageOfAllScreen(filterList5);
									output = (long) commonAnalysisService.marketingMultiplier(filterList5,
											projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
											projectDetails.get().getCinemaFormat(),
											projectDetails.get().getCinemaCategory());
								} else {
									List<Navision> filterList6 = getSixListForMarketingExpanse(projectDetails.get(), lastQuarterNonCompDataFinal);
									filterList6.addAll(
											!ObjectUtils.isEmpty(filterList5) ? filterList5 : new ArrayList<>());
									navCode = filterList6.stream().map(Navision::getNavCinemaCode)
											.collect(Collectors.toSet());
									if (navCode.size() >= 3) {
										averageOfAllScreen = getMarketingExpenseAverageOfAllScreen(filterList6);
										output = (long) commonAnalysisService.marketingMultiplier(filterList6,
												projectDetails.get().getNoOfScreens(), Math.round(averageOfAllScreen),
												projectDetails.get().getCinemaFormat(),
												projectDetails.get().getCinemaCategory());
									}
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("error while fetching project marketing expanse.", e);
			throw new FeasibilityException("error while fetching project marketing expanse.");

		}
		return output;
	}

	public static double getMarketingExpenseAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Long> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getMarketingExpenses() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(Math.round(sumAll));
			}
			//List<Long> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());
			List<Long> allSumOfSingScreen = getAllPerScreenAvg.stream().collect(Collectors.toList());
			
			long sumAllValues = allSumOfSingScreen.stream().mapToLong(Long::longValue).sum();
			double finalAverageOfAllScreen = (double) sumAllValues / (double) uniqueNavCode.size();
			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating marketing expense per quater..", e);
			throw new FeasibilityException("error while calculating marketing expense per quater..", e);
		}
	}

	public List<Navision> getFirstListForMarketingExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);
			List<Navision> categoryFilter = commonAnalysisService.filterBycategory(goldScreenFilter,
					projectDetails.getCinemaCategory());
			List<Navision> filterBasedOnCity = categoryFilter.stream()
					.filter(predicate -> predicate.getCity().equals(projectDetails.getProjectLocation().getCity()))
					.collect(Collectors.toList());
			return filterBasedOnCity;

		} catch (Exception e) {
			log.error("error while searching  marketing expense first list..", e);
			throw new FeasibilityException("error while searching  marketing expense first list..", e);
		}
	}

	public List<Navision> getSecondListForMarketingExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);

			List<Navision> filterBasedOnCity = goldScreenFilter.stream()
					.filter(predicate -> predicate.getCity().equals(projectDetails.getProjectLocation().getCity()))
					.collect(Collectors.toList());
			return filterBasedOnCity;

		} catch (Exception e) {
			log.error("error while searching  marketing expense second list..", e);
			throw new FeasibilityException("error while searching  marketing expense second list..", e);
		}
	}

	public List<Navision> getThirdListForMarketingExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);
			List<Navision> categoryFilter = commonAnalysisService.filterBycategory(goldScreenFilter,
					projectDetails.getCinemaCategory());
			List<Navision> getByCityTier = commonAnalysisService.getByCityFilterInMarketing(categoryFilter,
					projectDetails.getProjectLocation().getCityTier());
			List<Navision> filterByStateInput = getByCityTier.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());
			return filterByStateInput;

		} catch (Exception e) {
			log.error("error while searching  marketing expense third list..", e);
			throw new FeasibilityException("error while searching  marketing expense third list..", e);
		}
	}

	public List<Navision> getFourListForMarketingExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);
			List<Navision> categoryFilter = commonAnalysisService.filterBycategory(goldScreenFilter,
					projectDetails.getCinemaCategory());
			List<Navision> filterBasedOnCity = categoryFilter.stream()
					.filter(predicate -> predicate.getCity().equals(projectDetails.getProjectLocation().getCity()))
					.collect(Collectors.toList());
			List<Navision> getByCityTier = commonAnalysisService.getByCityFilterInMarketing(filterBasedOnCity,
					projectDetails.getProjectLocation().getCityTier());
			return getByCityTier;

		} catch (Exception e) {
			log.error("error while searching  marketing expense fourth list..", e);
			throw new FeasibilityException("error while searching  marketing expense fourth list..", e);
		}
	}

	public List<Navision> getFiveListForMarketingExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);
			List<Navision> getByCityTier = commonAnalysisService.getByCityFilterInMarketing(goldScreenFilter,
					projectDetails.getProjectLocation().getCityTier());
			return getByCityTier;

		} catch (Exception e) {
			log.error("error while searching  marketing expense five list..", e);
			throw new FeasibilityException("error while searching  marketing expense five list..", e);
		}
	}

	public List<Navision> getSixListForMarketingExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> goldScreenFilter = commonAnalysisService.getGoldScreenFilter(filterOutSingleScreen);
			return goldScreenFilter;

		} catch (Exception e) {
			log.error("error while searching  marketing expense six list..", e);
			throw new FeasibilityException("error while searching  marketing expense six list..", e);
		}
	}

	@Override
	public long getProjectCommunicationExpense(String projectId, Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		long communicationExpense = 0;
		long output = 0;
		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		try {

			if (projectDetails.isPresent()) {
//				List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
				List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
				List<Navision> filterOutSingleScreen = filteredCompData.stream()
						.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
				double averageOfAllScreen = getCommunicationExpenseAverageOfAllScreen(filterOutSingleScreen);
				communicationExpense = Math.round(averageOfAllScreen) * (long) projectDetails.get().getNoOfScreens();
				output = (long) commonAnalysisService.communicationMultiplier(filterOutSingleScreen,
						projectDetails.get().getNoOfScreens(), communicationExpense,
						projectDetails.get().getCinemaFormat());
			}

		} catch (Exception e) {
			log.error("error while fetching project communication expanse.", e);
			throw new FeasibilityException("error while fetching project communication expanse.");
		}
		return output;
	}

	public static double getCommunicationExpenseAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Double> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getCommunicationCost() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(sumAll);
			}
			//List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());
			List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().collect(Collectors.toList());
			
			double sumAllValues = allSumOfSingScreen.stream().mapToDouble(Double::doubleValue).sum();
			double finalAverageOfAllScreen = sumAllValues / uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating communication expense per quater..", e);
			throw new FeasibilityException("error while calculating communication expense per quater..", e);
		}
	}

	@Override
	public long getProjectMiscellaneousExpense(String projectId, Optional<ProjectDetails> upcomingProjectDetails,List<Navision> lastQuarterNonCompDataFinal) {
		long miscellaneousExpanse = 0;
		double averageOfAllScreen = 0;
		long output = 0;
		Set<String> navCode = null;
		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}

		try {

			if (projectDetails.isPresent()) {
				List<Navision> filterList1 = getFirstListForMiscellaneousExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
				navCode = filterList1.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
				if (navCode.size() >= 3) {
					averageOfAllScreen = getMiscellaneousExpenseAverageOfAllScreen(filterList1);
					miscellaneousExpanse = Math.round(averageOfAllScreen)
							* (long) projectDetails.get().getNoOfScreens();
					output = (long) commonAnalysisService.miscellaneousExpenseMultiplier(filterList1,
							projectDetails.get().getNoOfScreens(), miscellaneousExpanse,
							projectDetails.get().getCinemaFormat());
				} else {
					List<Navision> filterList2 = getSecondListForMiscellaneousExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
					filterList2.addAll(!ObjectUtils.isEmpty(filterList1) ? filterList1 : new ArrayList<>());
					navCode = filterList2.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						averageOfAllScreen = getMiscellaneousExpenseAverageOfAllScreen(filterList2);
						miscellaneousExpanse = Math.round(averageOfAllScreen)
								* (long) projectDetails.get().getNoOfScreens();
						output = (long) commonAnalysisService.miscellaneousExpenseMultiplier(filterList2,
								projectDetails.get().getNoOfScreens(), miscellaneousExpanse,
								projectDetails.get().getCinemaFormat());
					} else {
						List<Navision> filterList3 = getThirdListForMiscellaneousExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
						filterList3.addAll(!ObjectUtils.isEmpty(filterList2) ? filterList2 : new ArrayList<>());
						navCode = filterList3.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

						if (navCode.size() >= 3) {
							averageOfAllScreen = getMiscellaneousExpenseAverageOfAllScreen(filterList3);
							miscellaneousExpanse = Math.round(averageOfAllScreen)
									* (long) projectDetails.get().getNoOfScreens();
							output = (long) commonAnalysisService.miscellaneousExpenseMultiplier(filterList3,
									projectDetails.get().getNoOfScreens(), miscellaneousExpanse,
									projectDetails.get().getCinemaFormat());
						} else {
							List<Navision> filterList4 = getFourListForMiscellaneousExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
							filterList4.addAll(!ObjectUtils.isEmpty(filterList3) ? filterList3 : new ArrayList<>());
							navCode = filterList4.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

							if (navCode.size() >= 3) {
								averageOfAllScreen = getMiscellaneousExpenseAverageOfAllScreen(filterList4);
								miscellaneousExpanse = Math.round(averageOfAllScreen)
										* (long) projectDetails.get().getNoOfScreens();
								output = (long) commonAnalysisService.miscellaneousExpenseMultiplier(filterList4,
										projectDetails.get().getNoOfScreens(), miscellaneousExpanse,
										projectDetails.get().getCinemaFormat());
							} else {
								List<Navision> filterList5 = getFiveListForMiscellaneousExpanse(projectDetails.get(),lastQuarterNonCompDataFinal);
								filterList5.addAll(!ObjectUtils.isEmpty(filterList4) ? filterList4 : new ArrayList<>());
								navCode = filterList5.stream().map(Navision::getNavCinemaCode)
										.collect(Collectors.toSet());

								if (navCode.size() >= 3) {
									averageOfAllScreen = getMiscellaneousExpenseAverageOfAllScreen(filterList5);
									miscellaneousExpanse = Math.round(averageOfAllScreen)
											* (long) projectDetails.get().getNoOfScreens();
									output = (long) commonAnalysisService.miscellaneousExpenseMultiplier(filterList5,
											projectDetails.get().getNoOfScreens(), miscellaneousExpanse,
											projectDetails.get().getCinemaFormat());
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("error while fetching project miscellaneous expanse.", e);
			throw new FeasibilityException("error while fetching project miscellaneous expanse.");
		}
		return output;
	}

	public static double getMiscellaneousExpenseAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Double> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getMiscellaneousExpense() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(sumAll);
			}
			//List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());
			List<Double> allSumOfSingScreen = getAllPerScreenAvg.stream().collect(Collectors.toList());
			
			double sumAllValues = allSumOfSingScreen.stream().mapToDouble(Double::doubleValue).sum();
			double finalAverageOfAllScreen = sumAllValues / uniqueNavCode.size();

			return finalAverageOfAllScreen;

		} catch (Exception e) {
			log.error("error while calculating miscellaneous expense per quater..", e);
			throw new FeasibilityException("error while calculating miscellaneous expense per quater..", e);
		}
	}

	public List<Navision> getFirstListForMiscellaneousExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> filterBasedOnCity = filterOutSingleScreen.stream()
					.filter(predicate -> predicate.getCity().equals(projectDetails.getProjectLocation().getCity()))
					.collect(Collectors.toList());
			List<Navision> filterByNoOfScreenFilter = getAllHouseKeepingCostNavisionbyNoOfScreenInputRangeFilter(
					projectDetails, filterBasedOnCity);
			return filterByNoOfScreenFilter;

		} catch (Exception e) {
			log.error("error while searching miscellaneous expense first list..", e);
			throw new FeasibilityException("error while searching miscellaneous expense first list..", e);
		}
	}

	public List<Navision> getSecondListForMiscellaneousExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> filterBasedOnCity = filterOutSingleScreen.stream()
					.filter(predicate -> predicate.getCity().equals(projectDetails.getProjectLocation().getCity()))
					.collect(Collectors.toList());
			return filterBasedOnCity;

		} catch (Exception e) {
			log.error("error while searching miscellaneous expense second list..", e);
			throw new FeasibilityException("error while searching miscellaneous expense second list..", e);
		}
	}

	public List<Navision> getThirdListForMiscellaneousExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> filterByStateInput = filterOutSingleScreen.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());
			List<Navision> getByCityTier = commonAnalysisService.getByCityFilterInMiscellaneous(filterByStateInput,
					projectDetails.getProjectLocation().getCityTier());
			List<Navision> filterByNoOfScreenFilter = getAllHouseKeepingCostNavisionbyNoOfScreenInputRangeFilter(
					projectDetails, getByCityTier);

			return filterByNoOfScreenFilter;

		} catch (Exception e) {
			log.error("error while searching miscellaneous expense third list..", e);
			throw new FeasibilityException("error while searching miscellaneous expense third list..", e);
		}
	}

	public List<Navision> getFourListForMiscellaneousExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> filterByStateInput = filterOutSingleScreen.stream()
					.filter(predicate -> predicate.getState().equals(projectDetails.getProjectLocation().getState()))
					.collect(Collectors.toList());
			List<Navision> getByCityTier = commonAnalysisService.getByCityFilterInMiscellaneous(filterByStateInput,
					projectDetails.getProjectLocation().getCityTier());

			return getByCityTier;

		} catch (Exception e) {
			log.error("error while searching miscellaneous expense four list..", e);
			throw new FeasibilityException("error while searching miscellaneous expense four list..", e);
		}

	}

	public List<Navision> getFiveListForMiscellaneousExpanse(ProjectDetails projectDetails, List<Navision> lastQuarterNonCompDataFinal) {
		try {
//			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filteredCompData = lastQuarterNonCompDataFinal;
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> getByCityTier = commonAnalysisService.getByCityFilterInMiscellaneous(filterOutSingleScreen,
					projectDetails.getProjectLocation().getCityTier());

			return getByCityTier;

		} catch (Exception e) {
			log.error("error while searching miscellaneous expense five list..", e);
			throw new FeasibilityException("error while searching miscellaneous expense five list..", e);
		}

	}

	public static int getPersonnalCostInitialValue(int no) {
		int firstCondition = 0;
		if (no == 1) {
			firstCondition = 1;
		} else if (no == 2) {
			firstCondition = 2;
		} else if (no == 3) {
			firstCondition = 2;
		} else if (no == 4) {
			firstCondition = 3;
		} else if (no == 5) {
			firstCondition = 5;
		} else if (no == 6) {
			firstCondition = 5;
		} else if (no == 7) {
			firstCondition = 5;
		} else if (no == 8) {
			firstCondition = 5;
		} else if (no == 9) {
			firstCondition = 5;
		} else if (no == 10) {
			firstCondition = 5;
		} else if (no == 11) {
			firstCondition = 5;
		} else if (no == 12) {
			firstCondition = 5;
		} else if (no == 13) {
			firstCondition = 5;
		} else if (no == 14) {
			firstCondition = 5;
		} else if (no == 15) {
			firstCondition = 5;
		} else if (no == 16) {
			firstCondition = 5;
		} else if (no == 17) {
			firstCondition = 5;
		} else if (no == 18) {
			firstCondition = 5;
		} else if (no == 19) {
			firstCondition = 5;
		} else if (no == 20) {
			firstCondition = 5;
		} else if (no == 21) {
			firstCondition = 5;
		} else if (no == 22) {
			firstCondition = 5;
		} else if (no == 23) {
			firstCondition = 5;
		} else if (no == 24) {
			firstCondition = 5;
		} else if (no == 25) {
			firstCondition = 5;
		} else if (no == 26) {
			firstCondition = 5;
		} else if (no == 27) {
			firstCondition = 5;
		} else if (no == 28) {
			firstCondition = 5;
		} else if (no == 29) {
			firstCondition = 5;
		} else if (no == 30) {
			firstCondition = 5;
		}
		return firstCondition;
	}

	public static int getPersonalCostLastValue(int no) {
		int lastCondition = 0;
		if (no == 1) {
			lastCondition = 1;
		} else if (no == 2) {
			lastCondition = 2;
		} else if (no == 3) {
			lastCondition = 3;
		} else if (no == 4) {
			lastCondition = 5;
		} else if (no == 5) {
			lastCondition = 20;
		} else if (no == 6) {
			lastCondition = 20;
		} else if (no == 7) {
			lastCondition = 20;
		} else if (no == 8) {
			lastCondition = 20;
		} else if (no == 9) {
			lastCondition = 20;
		} else if (no == 10) {
			lastCondition = 20;
		} else if (no == 11) {
			lastCondition = 20;
		} else if (no == 12) {
			lastCondition = 20;
		} else if (no == 13) {
			lastCondition = 20;
		} else if (no == 14) {
			lastCondition = 20;
		} else if (no == 15) {
			lastCondition = 20;
		} else if (no == 16) {
			lastCondition = 20;
		} else if (no == 17) {
			lastCondition = 20;
		} else if (no == 18) {
			lastCondition = 20;
		} else if (no == 19) {
			lastCondition = 20;
		} else if (no == 20) {
			lastCondition = 20;
		} else if (no == 21) {
			lastCondition = 20;
		} else if (no == 22) {
			lastCondition = 20;
		} else if (no == 23) {
			lastCondition = 20;
		} else if (no == 24) {
			lastCondition = 20;
		} else if (no == 25) {
			lastCondition = 20;
		} else if (no == 26) {
			lastCondition = 20;
		} else if (no == 27) {
			lastCondition = 20;
		} else if (no == 28) {
			lastCondition = 20;
		} else if (no == 29) {
			lastCondition = 20;
		} else if (no == 30) {
			lastCondition = 20;
		}

		return lastCondition;
	}

	public static int getHouseKeepingInitialValue(int no) {
		int firstCondition = 0;
		if (no == 1) {
			firstCondition = 1;
		} else if (no == 2) {
			firstCondition = 1;
		} else if (no == 3) {
			firstCondition = 2;
		} else if (no == 4) {
			firstCondition = 3;
		} else if (no == 5) {
			firstCondition = 4;
		} else if (no == 6) {
			firstCondition = 5;
		} else if (no == 7) {
			firstCondition = 5;
		} else if (no == 8) {
			firstCondition = 5;
		} else if (no == 9) {
			firstCondition = 5;
		} else if (no == 10) {
			firstCondition = 5;
		} else if (no == 11) {
			firstCondition = 5;
		} else if (no == 12) {
			firstCondition = 5;
		} else if (no == 13) {
			firstCondition = 5;
		} else if (no == 14) {
			firstCondition = 5;
		} else if (no == 15) {
			firstCondition = 5;
		} else if (no == 16) {
			firstCondition = 5;
		} else if (no == 17) {
			firstCondition = 5;
		} else if (no == 18) {
			firstCondition = 5;
		} else if (no == 19) {
			firstCondition = 5;
		} else if (no == 20) {
			firstCondition = 5;
		} else if (no == 21) {
			firstCondition = 5;
		} else if (no == 22) {
			firstCondition = 5;
		} else if (no == 23) {
			firstCondition = 5;
		} else if (no == 24) {
			firstCondition = 5;
		} else if (no == 25) {
			firstCondition = 5;
		} else if (no == 26) {
			firstCondition = 5;
		} else if (no == 27) {
			firstCondition = 5;
		} else if (no == 28) {
			firstCondition = 5;
		} else if (no == 29) {
			firstCondition = 5;
		} else if (no == 30) {
			firstCondition = 5;
		}
		return firstCondition;
	}

	public static int getHouseKeepingLastValue(int no) {
		int lastCondition = 0;
		if (no == 1) {
			lastCondition = 2;
		} else if (no == 2) {
			lastCondition = 3;
		} else if (no == 3) {
			lastCondition = 4;
		} else if (no == 4) {
			lastCondition = 5;
		} else if (no == 5) {
			lastCondition = 6;
		} else if (no == 6) {
			lastCondition = 20;
		} else if (no == 7) {
			lastCondition = 20;
		} else if (no == 8) {
			lastCondition = 20;
		} else if (no == 9) {
			lastCondition = 20;
		} else if (no == 10) {
			lastCondition = 20;
		} else if (no == 11) {
			lastCondition = 20;
		} else if (no == 12) {
			lastCondition = 20;
		} else if (no == 13) {
			lastCondition = 20;
		} else if (no == 14) {
			lastCondition = 20;
		} else if (no == 15) {
			lastCondition = 20;
		} else if (no == 16) {
			lastCondition = 20;
		} else if (no == 17) {
			lastCondition = 20;
		} else if (no == 18) {
			lastCondition = 20;
		} else if (no == 19) {
			lastCondition = 20;
		} else if (no == 20) {
			lastCondition = 20;
		} else if (no == 21) {
			lastCondition = 20;
		} else if (no == 22) {
			lastCondition = 20;
		} else if (no == 23) {
			lastCondition = 20;
		} else if (no == 24) {
			lastCondition = 20;
		} else if (no == 25) {
			lastCondition = 20;
		} else if (no == 26) {
			lastCondition = 20;
		} else if (no == 27) {
			lastCondition = 20;
		} else if (no == 28) {
			lastCondition = 20;
		} else if (no == 29) {
			lastCondition = 20;
		} else if (no == 30) {
			lastCondition = 20;
		}

		return lastCondition;
	}

	public List<Navision> getAllHouseKeepingCostNavisionbyNoOfScreenInputRangeFilter(ProjectDetails projectDetails,
			List<Navision> filterState) {
		List<Navision> updateNavision = new ArrayList<>();
		// int noOfScreenUserInput = projectDetails.getNoOfScreens();
		int initialValue = getHouseKeepingInitialValue(projectDetails.getNoOfScreens());
		int lastValue = getHouseKeepingLastValue(projectDetails.getNoOfScreens());
		for (Navision navision : filterState) {
			if ((navision.getNoOfScreen() >= initialValue) && (navision.getNoOfScreen() <= lastValue)) {
				updateNavision.add(navision);
			}
		}
		return updateNavision;
	}

	public List<Navision> getAllWaterAndElectricityNavisionbyNoOfScreenInputRangeFilter(ProjectDetails projectDetails,
			List<Navision> filterState) {
		List<Navision> updateNavision = new ArrayList<>();
		// int noOfScreenUserInput = projectDetails.getNoOfScreens();
		int initialValue = getInitialValue(projectDetails.getNoOfScreens());
		int lastValue = getLastValue(projectDetails.getNoOfScreens());
		for (Navision navision : filterState) {
			if ((navision.getNoOfScreen() >= initialValue) && (navision.getNoOfScreen() <= lastValue)) {
				updateNavision.add(navision);
			}
		}
		return updateNavision;
	}

	@Override
	public long getProjectCamDetails(String projectId, Optional<ProjectDetails> upcomingProjectDetails) {
		long projectCamExpense = 0;
		long output = 0;

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		try {

			if (projectDetails.isPresent()) {

				List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
				double averageOfAllScreen = getCamExpanseAverageOfAllScreen(navisionData);
				projectCamExpense = Math.round(averageOfAllScreen) * projectDetails.get().getNoOfScreens();
				output = (long) commonAnalysisService.camExpenseMultiplier(navisionData,
						projectDetails.get().getNoOfScreens(), projectCamExpense,
						projectDetails.get().getCinemaFormat());
			}
		} catch (Exception e) {
			log.error("error while fteching camp details..");
			throw new FeasibilityException("error while fteching camp details..");
		}
		return output;
	}

	public static double getCamExpanseAverageOfAllScreen(List<Navision> allNavisionData) {
		try {
			Set<String> uniqueNavCode = allNavisionData.stream().map(Navision::getNavCinemaCode)
					.collect(Collectors.toSet());

			List<Long> getAllPerScreenAvg = new ArrayList<>();
			for (String navCode : uniqueNavCode) {
				List<Navision> navCodeCinemaFilter = allNavisionData.stream()
						.filter(predicate -> predicate.getNavCinemaCode().equals(navCode)).distinct()
						.collect(Collectors.toList());
				double sumAll = 0;
				for (Navision na : navCodeCinemaFilter) {
					double fee = (double) na.getCam() / (double) na.getNoOfScreen();
					sumAll += fee;
				}
				getAllPerScreenAvg.add(Math.round(sumAll));
			}
			List<Long> allSumOfSingScreen = getAllPerScreenAvg.stream().distinct().collect(Collectors.toList());

			long sumAllValues = allSumOfSingScreen.stream().mapToLong(Long::longValue).sum();
			double finalAverageOfAllScreen = (double) sumAllValues / (double) uniqueNavCode.size();

			return finalAverageOfAllScreen;
		} catch (Exception e) {
			log.error("error while calculating legal expense per quater");
			throw new FeasibilityException("error while calculating legal expense per quater");
		}

	}

	@Override
	public long getProjectTestAlgo(String projectId) {
		long start = System.currentTimeMillis();
		Criteria crit = entityManager.unwrap(Session.class).createCriteria(TicketOfflineOnline.class);

		long seconds = (int) ((start - System.currentTimeMillis() / 1000) % 60);
		System.out.println("totalSeconds " + seconds);
		return 0;
	}

	public Session getSession() {
		Session session = entityManager.unwrap(Session.class);
		return session;
	}

	@Override
	public Set<BenchmarkCinema> getPersonalExpenseBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {
			final List<Navision> lastQuarterNonCompDataFinal = commonAnalysisService.getAllNavisionByComp();
			
			List<Navision> filterListFirst = getFirstListPersonalExpense(projectDetails, lastQuarterNonCompDataFinal);
			navCode = filterListFirst.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

			if (navCode.size() >= 1) {
				navision = filterListFirst;
			} else {
				List<Navision> filterListSecond = getSecondListPersonalExpense(projectDetails, lastQuarterNonCompDataFinal);
				filterListSecond.addAll(!ObjectUtils.isEmpty(filterListFirst) ? filterListFirst : new ArrayList<>());
				navCode = filterListSecond.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

				if (navCode.size() >= 1) {
					navision = filterListSecond;
				} else {
					List<Navision> filterListThird = getThirdListPersonalExpense(projectDetails, lastQuarterNonCompDataFinal);
					filterListThird
							.addAll(!ObjectUtils.isEmpty(filterListSecond) ? filterListSecond : new ArrayList<>());
					navCode = filterListThird.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 1) {
						navision = filterListThird;
					}
				}
			}

			if (!ObjectUtils.isEmpty(navision)) {

				Set<String> cinemaName = navision.stream().map(Navision::getCinemaName).collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = navision.stream()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getPersonalExpense).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
				/*
				 * navision.forEach(action -> { BenchmarkCinema marker = new BenchmarkCinema();
				 * marker.setCinemaName(action.getCinemaName());
				 * marker.setNavCode(action.getPersonalExpense()); mark.add(marker); });
				 */
			}
		} catch (Exception e) {
			log.error("error while fetching personal benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getEletricityDetailsBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {
			
			final List<Navision> lastQuarterNonCompDataFinal = commonAnalysisService.getAllNavisionByComp();

			List<Navision> listFirst = getWaterAndElctricityFirstList(projectDetails, lastQuarterNonCompDataFinal);
			navCode = listFirst.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
			if (navCode.size() >= 3) {
				navision = listFirst;
			} else {
				List<Navision> listSecond = getWaterAndElctricitySecondList(projectDetails, lastQuarterNonCompDataFinal);
				listSecond.addAll(!ObjectUtils.isEmpty(listFirst) ? listFirst : new ArrayList<>());
				navCode = listSecond.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
				if (navCode.size() >= 3) {
					navision = listSecond;

				} else {
					List<Navision> listThird = getWaterAndElctricityThirdist(projectDetails, lastQuarterNonCompDataFinal);
					listThird.addAll(!ObjectUtils.isEmpty(listSecond) ? listSecond : new ArrayList<>());
					navCode = listThird.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
					if (navCode.size() >= 3) {
						navision = listThird;

					} else {
						List<Navision> listFour = getWaterAndElctricityFourList(projectDetails, lastQuarterNonCompDataFinal);
						listFour.addAll(!ObjectUtils.isEmpty(listThird) ? listThird : new ArrayList<>());
						navCode = listFour.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

						if (navCode.size() >= 3) {
							navision = listFour;

						}
					}
				}
			}

			if (!ObjectUtils.isEmpty(navision)) {

				Set<String> cinemaName = navision.stream().map(Navision::getCinemaName).collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = navision.stream()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getElectricAndWaterExpense).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}

				/*
				 * navision.forEach(action -> { BenchmarkCinema marker = new BenchmarkCinema();
				 * marker.setCinemaName(action.getCinemaName());
				 * marker.setNavCode(action.getElectricAndWaterExpense()); mark.add(marker); });
				 */
			}
		} catch (Exception e) {
			log.error("error while fetching personal benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getRepairAndMaintainceBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {

			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			List<Navision> filterOutGoldSingleScreenIcon = commonAnalysisService
					.getGoldScreenFilter(filterOutSingleScreen);
			List<Navision> filterOutIcon = commonAnalysisService.filterOutByIcon(filterOutGoldSingleScreenIcon);

			if (!ObjectUtils.isEmpty(filterOutIcon)) {
				Set<String> cinemaName = filterOutIcon.stream().map(Navision::getCinemaName)
						.collect(Collectors.toSet());
				for (String cinemas : cinemaName) {
					List<Navision> navisions = filterOutIcon.stream()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getRAndmEngg).sum();
					Long totalrmIt = navisions.stream().mapToLong(Navision::getRAndmIt).sum();
					Long totalrmFb = navisions.stream().mapToLong(Navision::getRAndMFb).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue + totalrmIt + totalrmFb);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
				/*
				 * filterOutIcon.forEach(action -> { BenchmarkCinema marker = new
				 * BenchmarkCinema(); marker.setCinemaName(action.getCinemaName());
				 * marker.setNavCode(action.getRAndmEngg()); mark.add(marker); });
				 */
			}
		} catch (Exception e) {
			log.error("error while fetching repair and maintance benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getHousekeepingBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {

			final List<Navision> lastQuarterNonCompDataFinal = commonAnalysisService.getAllNavisionByComp();

			List<Navision> filterList1 = getFirstListForHouseKeeping(projectDetails,lastQuarterNonCompDataFinal);
			navCode = filterList1.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

			if (navCode.size() >= 3) {
				navision = filterList1;
			} else {
				List<Navision> filterList2 = getSecondListForHouseKeeping(projectDetails,lastQuarterNonCompDataFinal);
				filterList2.addAll(!ObjectUtils.isEmpty(filterList1) ? filterList1 : new ArrayList<>());
				navCode = filterList2.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

				if (navCode.size() >= 3) {
					navision = filterList2;
				} else {
					List<Navision> filterList3 = getThirdListForHouseKeeping(projectDetails,lastQuarterNonCompDataFinal);
					filterList3.addAll(!ObjectUtils.isEmpty(filterList2) ? filterList2 : new ArrayList<>());
					navCode = filterList3.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						navision = filterList3;
					} else {

						List<Navision> filterList4 = getFourListForHouseKeeping(projectDetails,lastQuarterNonCompDataFinal);
						filterList4.addAll(!ObjectUtils.isEmpty(filterList3) ? filterList3 : new ArrayList<>());
						navCode = filterList4.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

						if (navCode.size() >= 3) {
							navision = filterList4;
						} else {
							List<Navision> filterList5 = getFiveListForHouseKeeping(projectDetails,lastQuarterNonCompDataFinal);
							filterList5.addAll(!ObjectUtils.isEmpty(filterList4) ? filterList4 : new ArrayList<>());
							navCode = filterList5.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

							if (navCode.size() >= 3) {
								navision = filterList5;

							} else {
								List<Navision> filterList6 = getSixListForHouseKeeping(projectDetails,lastQuarterNonCompDataFinal);
								filterList6.addAll(!ObjectUtils.isEmpty(filterList5) ? filterList5 : new ArrayList<>());
								navCode = filterList6.stream().map(Navision::getNavCinemaCode)
										.collect(Collectors.toSet());

								if (navCode.size() >= 3) {
									navision = filterList6;

								} else {

									List<Navision> filterList7 = getSevenListForHouseKeeping(projectDetails,lastQuarterNonCompDataFinal);
									filterList7.addAll(
											!ObjectUtils.isEmpty(filterList6) ? filterList6 : new ArrayList<>());
									navCode = filterList7.stream().map(Navision::getNavCinemaCode)
											.collect(Collectors.toSet());

									if (navCode.size() >= 3) {
										navision = filterList7;
									}
								}
							}
						}
					}
				}
			}
			if (!ObjectUtils.isEmpty(navision)) {

				Set<String> cinemaName = navision.stream().map(Navision::getCinemaName).collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = navision.stream().distinct()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getHouseKeepingCharges).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
				/*
				 * navision.forEach(action -> { BenchmarkCinema marker = new BenchmarkCinema();
				 * marker.setCinemaName(action.getCinemaName());
				 * marker.setNavCode(action.getHouseKeepingCharges()); mark.add(marker); });
				 */
			}
		} catch (Exception e) {
			log.error("error while fetching house keeping benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getMiscellaneousBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {

			final List<Navision> lastQuarterNonCompDataFinal = commonAnalysisService.getAllNavisionByComp();

			List<Navision> filterList1 = getFirstListForMiscellaneousExpanse(projectDetails,lastQuarterNonCompDataFinal);
			navCode = filterList1.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
			if (navCode.size() >= 3) {
				navision = filterList1;
			} else {
				List<Navision> filterList2 = getSecondListForMiscellaneousExpanse(projectDetails,lastQuarterNonCompDataFinal);
				filterList2.addAll(!ObjectUtils.isEmpty(filterList1) ? filterList1 : new ArrayList<>());
				navCode = filterList2.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

				if (navCode.size() >= 3) {
					navision = filterList2;
				} else {
					List<Navision> filterList3 = getThirdListForMiscellaneousExpanse(projectDetails,lastQuarterNonCompDataFinal);
					filterList3.addAll(!ObjectUtils.isEmpty(filterList2) ? filterList2 : new ArrayList<>());
					navCode = filterList3.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						navision = filterList3;
					} else {
						List<Navision> filterList4 = getFourListForMiscellaneousExpanse(projectDetails,lastQuarterNonCompDataFinal);
						filterList4.addAll(!ObjectUtils.isEmpty(filterList3) ? filterList3 : new ArrayList<>());
						navCode = filterList4.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

						if (navCode.size() >= 3) {
							navision = filterList4;
						} else {
							List<Navision> filterList5 = getFiveListForMiscellaneousExpanse(projectDetails,lastQuarterNonCompDataFinal);
							filterList5.addAll(!ObjectUtils.isEmpty(filterList4) ? filterList4 : new ArrayList<>());
							navCode = filterList5.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

							if (navCode.size() >= 3) {
								navision = filterList5;
							}
						}
					}
				}
			}

			if (!ObjectUtils.isEmpty(navision)) {
				Set<String> cinemaName = navision.stream().map(Navision::getCinemaName).collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = navision.stream().distinct()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getMiscellaneousExpense).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
			}
		} catch (Exception e) {
			log.error("error while fetching house keeping benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getTravellingAndConveyanceBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {

			final List<Navision> lastQuarterNonCompDataFinal = commonAnalysisService.getAllNavisionByComp();

			List<Navision> listFirst = travellingConveyanceListFirst(projectDetails,lastQuarterNonCompDataFinal);
			navCode = listFirst.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
			if (navCode.size() >= 3) {
				navision = listFirst;
			} else {
				List<Navision> listSecond = travellingConveyanceListSecond(projectDetails,lastQuarterNonCompDataFinal);
				listSecond.addAll(!ObjectUtils.isEmpty(listFirst) ? listFirst : new ArrayList<>());
				navCode = listSecond.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

				if (navCode.size() >= 3) {
					navision = listSecond;
				} else {
					List<Navision> listThird = travellingConveyanceListThird(projectDetails,lastQuarterNonCompDataFinal);
					listThird.addAll(!ObjectUtils.isEmpty(listSecond) ? listSecond : new ArrayList<>());
					navCode = listThird.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						navision = listThird;
					}
				}
			}

			if (!ObjectUtils.isEmpty(navision)) {

				Set<String> cinemaName = navision.stream().map(Navision::getCinemaName).collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = navision.stream().distinct()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getTravellingAndConveyance).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
				/*
				 * navision.forEach(action -> { BenchmarkCinema marker = new BenchmarkCinema();
				 * marker.setCinemaName(action.getCinemaName());
				 * marker.setNavCode(action.getTravellingAndConveyance()); mark.add(marker); });
				 */
			}
		} catch (Exception e) {
			log.error("error while fetching travelling and conyence benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getSecurityBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {
			final List<Navision> lastQuarterNonCompDataFinal = commonAnalysisService.getAllNavisionByComp();

			List<Navision> filterList1 = getFirstListForSecurityExpanse(projectDetails,lastQuarterNonCompDataFinal);
			navCode = filterList1.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
			if (navCode.size() >= 3) {
				navision = filterList1;
			} else {
				List<Navision> filterList2 = getSecondListForSecurityExpanse(projectDetails,lastQuarterNonCompDataFinal);
				filterList2.addAll(!ObjectUtils.isEmpty(filterList1) ? filterList1 : new ArrayList<>());
				navCode = filterList2.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

				if (navCode.size() >= 3) {
					navision = filterList2;
				} else {
					List<Navision> filterList3 = getThirdListForSecurityExpanse(projectDetails,lastQuarterNonCompDataFinal);
					filterList3.addAll(!ObjectUtils.isEmpty(filterList2) ? filterList2 : new ArrayList<>());
					navCode = filterList3.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
					// System.out.println(navCode);
					if (navCode.size() >= 3) {
						navision = filterList3;
					}
				}
			}

			if (!ObjectUtils.isEmpty(navision)) {
				Set<String> cinemaName = navision.stream().map(Navision::getCinemaName).collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = navision.stream().distinct()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getSecurityCharges).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
				/*
				 * navision.forEach(action -> { BenchmarkCinema marker = new BenchmarkCinema();
				 * marker.setCinemaName(action.getCinemaName());
				 * marker.setNavCode(action.getSecurityCharges()); mark.add(marker); });
				 */

			}
		} catch (Exception e) {
			log.error("error while fetching security expene  benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getLegalFeeBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {
			List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
			if (!ObjectUtils.isEmpty(navisionData)) {
				Set<String> cinemaName = navisionData.stream().map(Navision::getCinemaName).collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = navisionData.stream().distinct()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getLegalAndProfessional).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}

			}
		} catch (Exception e) {
			log.error("error while fetching legal expene  benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getCommunicationBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {
			List<Navision> filteredCompData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filterOutSingleScreen = filteredCompData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			if (!ObjectUtils.isEmpty(filterOutSingleScreen)) {
				/*
				 * filterOutSingleScreen.forEach(action -> { BenchmarkCinema marker = new
				 * BenchmarkCinema(); marker.setCinemaName(action.getCinemaName());
				 * marker.setNavCode(action.getCommunicationCost()); mark.add(marker); });
				 */
				Set<String> cinemaName = filterOutSingleScreen.stream().map(Navision::getCinemaName)
						.collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = filterOutSingleScreen.stream().distinct()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getCommunicationCost).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
			}
		} catch (Exception e) {
			log.error("error while fetching legal expene  benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getMarketingBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {

			final List<Navision> lastQuarterNonCompDataFinal = commonAnalysisService.getAllNavisionByComp();

			List<Navision> filterList1 = getFirstListForMarketingExpanse(projectDetails,lastQuarterNonCompDataFinal);
			navCode = filterList1.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
			if (navCode.size() >= 3) {
				navision = filterList1;

			} else {
				List<Navision> filterList2 = getSecondListForMarketingExpanse(projectDetails,lastQuarterNonCompDataFinal);
				filterList2.addAll(!ObjectUtils.isEmpty(filterList1) ? filterList1 : new ArrayList<>());
				navCode = filterList2.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

				if (navCode.size() >= 3) {
					navision = filterList2;
				} else {
					List<Navision> filterList3 = getThirdListForMarketingExpanse(projectDetails,lastQuarterNonCompDataFinal);
					filterList3.addAll(!ObjectUtils.isEmpty(filterList2) ? filterList2 : new ArrayList<>());
					navCode = filterList3.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						navision = filterList3;
					} else {
						List<Navision> filterList4 = getFourListForMarketingExpanse(projectDetails,lastQuarterNonCompDataFinal);
						filterList4.addAll(!ObjectUtils.isEmpty(filterList3) ? filterList3 : new ArrayList<>());
						navCode = filterList4.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

						if (navCode.size() >= 3) {
							navision = filterList4;
						} else {
							List<Navision> filterList5 = getFiveListForMarketingExpanse(projectDetails,lastQuarterNonCompDataFinal);
							filterList5.addAll(!ObjectUtils.isEmpty(filterList4) ? filterList4 : new ArrayList<>());
							navCode = filterList5.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
							if (navCode.size() >= 3) {
								navision = filterList5;
							} else {
								List<Navision> filterList6 = getSixListForMarketingExpanse(projectDetails,lastQuarterNonCompDataFinal);
								filterList6.addAll(!ObjectUtils.isEmpty(filterList5) ? filterList5 : new ArrayList<>());
								navCode = filterList6.stream().map(Navision::getNavCinemaCode)
										.collect(Collectors.toSet());
								if (navCode.size() >= 3) {
									navision = filterList6;
								}
							}
						}
					}
				}
			}

			if (!ObjectUtils.isEmpty(navision)) {
				Set<String> cinemaName = navision.stream().map(Navision::getCinemaName).collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = navision.stream().distinct()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getMarketingExpenses).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
			}
		} catch (Exception e) {
			log.error("error while fetching marketing expene  benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getInternetFeeBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {
			final List<Navision> lastQuarterNonCompDataFinal = commonAnalysisService.getAllNavisionByComp();

			List<Navision> listFirst = internetExpenseListFirst(projectDetails,lastQuarterNonCompDataFinal);
			navCode = listFirst.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());
			if (navCode.size() >= 3) {
				navision = listFirst;
			} else {
				List<Navision> listSecond = internetExpenseListSecond(projectDetails,lastQuarterNonCompDataFinal);
				listSecond.addAll(!ObjectUtils.isEmpty(listFirst) ? listFirst : new ArrayList<>());
				navCode = listSecond.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

				if (navCode.size() >= 3) {
					navision = listSecond;
				} else {
					List<Navision> listThird = internetExpenseListThird(projectDetails,lastQuarterNonCompDataFinal);
					listThird.addAll(!ObjectUtils.isEmpty(listSecond) ? listSecond : new ArrayList<>());
					navCode = listThird.stream().map(Navision::getNavCinemaCode).collect(Collectors.toSet());

					if (navCode.size() >= 3) {
						navision = listThird;
					}

				}
			}
			if (!ObjectUtils.isEmpty(navision)) {
				Set<String> cinemaName = navision.stream().map(Navision::getCinemaName).collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = navision.stream().distinct()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getInternateCharges).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
			}
		} catch (Exception e) {
			log.error("error while fetching internet expense  benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getinsuranceBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {
			List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
			if (!ObjectUtils.isEmpty(navisionData)) {

				Set<String> cinemaName = navisionData.stream().map(Navision::getCinemaName).collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = navisionData.stream().distinct()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());
					
					Long totalValue = navisions.stream().mapToLong(Navision::getInsuranceExpanse).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
			}
		} catch (Exception e) {
			log.error("error while fetching insurance expense  benchmarkCinemas");
		}
		return mark;
	}

	@Override
	public Set<BenchmarkCinema> getPrintingStationaryBenchmarkCinemas(ProjectDetails projectDetails) {
		Set<String> navCode = null;
		List<Navision> navision = new ArrayList<>();
		Set<BenchmarkCinema> mark = new HashSet<>();
		try {
			List<Navision> navisionData = commonAnalysisService.getAllNavisionByComp();
			List<Navision> filterOutSingleScreen = navisionData.stream()
					.filter(predicate -> predicate.getNoOfScreen() > 1).collect(Collectors.toList());
			if (!ObjectUtils.isEmpty(filterOutSingleScreen)) {
				Set<String> cinemaName = filterOutSingleScreen.stream().map(Navision::getCinemaName)
						.collect(Collectors.toSet());

				for (String cinemas : cinemaName) {
					List<Navision> navisions = filterOutSingleScreen.stream()
							.filter(predicate -> predicate.getCinemaName().equals(cinemas))
							.limit(4)
							.collect(Collectors.toList());

					Long totalValue = navisions.stream().mapToLong(Navision::getPrintingAndStationary).sum();
					BenchmarkCinema marker = new BenchmarkCinema();
					marker.setCinemaName(cinemas);
					marker.setNavCode(totalValue);
					marker.setNoOfScreen(navisions.get(0).getNoOfScreen());
					mark.add(marker);
				}
			}
		} catch (Exception e) {
			log.error("error while fetching printing and stationary  expense  benchmarkCinemas");
		}
		return mark;
	}

}
