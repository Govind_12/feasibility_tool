package com.api.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.FixedConstantDao;
import com.api.exception.FeasibilityException;
import com.common.mappers.OperatingMapper;
import com.common.mappers.ServiceChargeMapper;
import com.common.models.AtpCapDetials;
import com.common.models.CostAssumptionsGrowthRate;
import com.common.models.FixedConstant;
import com.common.models.GstType;
import com.common.models.RevenueAssumptionsGrowthRate;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FixedConstantServiceImpl implements FixedConstantService {

	@Autowired
	private FixedConstantDao fixedConstantDao;

	@Override
	public FixedConstant getFixedConstant() {
		List<FixedConstant> list = (List<FixedConstant>) (fixedConstantDao.findAll());
		return list.size() > 0 ? list.get(0) : new FixedConstant();
	}

	@Override
	public FixedConstant saveServiceChargePerTicket(ServiceChargeMapper serviceChargeMapper) {

		if (ObjectUtils.isEmpty(serviceChargeMapper)) {
			log.info("Service charge object cannot be null or empty");
			throw new FeasibilityException("Service charge object cannot be null or empty");
		}

		FixedConstant fetchedFixedConstant = getFixedConstant();
		Map<String, Double> ticketCharge = new HashMap<>();
		Map<String, Double> lbt = new HashMap<>();
		Map<String, Double> healthCess = new HashMap<>();
		if (!ObjectUtils.isEmpty(fetchedFixedConstant.getServiceChargePerTicket())
				&& !ObjectUtils.isEmpty(fetchedFixedConstant.getLbtAverageRate())
				&& !ObjectUtils.isEmpty(fetchedFixedConstant.getHealthCess())) {
			ticketCharge = fetchedFixedConstant.getServiceChargePerTicket();
			lbt = fetchedFixedConstant.getLbtAverageRate();
			healthCess = fetchedFixedConstant.getHealthCess();
		}

		if (!ObjectUtils.isEmpty(serviceChargeMapper.getTicketServiceCharge())) {
			ticketCharge.put(serviceChargeMapper.getState().trim(), serviceChargeMapper.getTicketServiceCharge());
			fetchedFixedConstant.setServiceChargePerTicket(ticketCharge);
		}

		if (!ObjectUtils.isEmpty(serviceChargeMapper.getLbtCharge())) {
			lbt.put(serviceChargeMapper.getState().trim(), serviceChargeMapper.getLbtCharge());
			fetchedFixedConstant.setLbtAverageRate(lbt);
		}

		if (!ObjectUtils.isEmpty(serviceChargeMapper.getHealthCess())) {
			healthCess.put(serviceChargeMapper.getState().trim(), serviceChargeMapper.getHealthCess());
			fetchedFixedConstant.setHealthCess(healthCess);
		}
		return fixedConstantDao.save(fetchedFixedConstant);
	}

	@Override
	public FixedConstant saveCapexAssumption(FixedConstant fixedConstant) {

		if (ObjectUtils.isEmpty(fixedConstant)) {
			log.info("Capex_Assumption object cannot be null or empty");
			throw new FeasibilityException("Capex_Assumption object cannot be null or empty");
		}

		FixedConstant fetchedFixedConstant = getFixedConstant();

		if (!ObjectUtils.isEmpty(fixedConstant.getInitailCapexInZeroYear())) {
			fetchedFixedConstant.setInitailCapexInZeroYear(fixedConstant.getInitailCapexInZeroYear());
		}
		if (!ObjectUtils.isEmpty(fixedConstant.getInitailCapexInOneYear())) {
			fetchedFixedConstant.setInitailCapexInOneYear(fixedConstant.getInitailCapexInOneYear());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getInitailCapexInSevenYear())) {
			fetchedFixedConstant.setInitailCapexInSevenYear(fixedConstant.getInitailCapexInSevenYear());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getCapexContingency())) {
			fetchedFixedConstant.setCapexContingency(fixedConstant.getCapexContingency());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getDebtProportion())) {
			fetchedFixedConstant.setDebtProportion(fixedConstant.getDebtProportion());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getEquityProportion())) {
			fetchedFixedConstant.setEquityProportion(fixedConstant.getEquityProportion());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getCostOfDebt())) {
			fetchedFixedConstant.setCostOfDebt(fixedConstant.getCostOfDebt());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getDebtTenure())) {
			fetchedFixedConstant.setDebtTenure(fixedConstant.getDebtTenure());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getCostOfEquity())) {
			fetchedFixedConstant.setCostOfEquity(fixedConstant.getCostOfEquity());
		}
		return fixedConstantDao.save(fetchedFixedConstant);
	}

	@Override
	public FixedConstant saveTaxAssumption(FixedConstant fixedConstant) {
		if (ObjectUtils.isEmpty(fixedConstant)) {
			log.info("Tax_Assumption object cannot be null or empty");
			throw new FeasibilityException("Tax_Assumption object cannot be null or empty");
		}

		FixedConstant fetchedFixedConstant = getFixedConstant();

		if (!ObjectUtils.isEmpty(fixedConstant.getPersonnel())) {
			GstType personnel = fetchedFixedConstant.getPersonnel();

			personnel.setGstApplicable(fixedConstant.getPersonnel().getGstApplicable());
			personnel.setGstCreditDisallowed(fixedConstant.getPersonnel().getGstCreditDisallowed());
			personnel.setEffectiveGstCost(fixedConstant.getPersonnel().getEffectiveGstCost());

			fetchedFixedConstant.setPersonnel(personnel);
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getRepairAndMaintenance())) {
			GstType repairAndMaintenance = fetchedFixedConstant.getRepairAndMaintenance();

			repairAndMaintenance.setGstApplicable(fixedConstant.getRepairAndMaintenance().getGstApplicable());
			repairAndMaintenance
					.setGstCreditDisallowed(fixedConstant.getRepairAndMaintenance().getGstCreditDisallowed());
			repairAndMaintenance.setEffectiveGstCost(fixedConstant.getRepairAndMaintenance().getEffectiveGstCost());

			fetchedFixedConstant.setRepairAndMaintenance(repairAndMaintenance);
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getRent())) {
			GstType rent = fetchedFixedConstant.getRent();

			rent.setGstApplicable(fixedConstant.getRent().getGstApplicable());
			rent.setGstCreditDisallowed(fixedConstant.getRent().getGstCreditDisallowed());
			rent.setEffectiveGstCost(fixedConstant.getRent().getEffectiveGstCost());

			fetchedFixedConstant.setRent(rent);
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getCam())) {
			GstType cam = fetchedFixedConstant.getCam();

			cam.setGstApplicable(fixedConstant.getCam().getGstApplicable());
			cam.setGstCreditDisallowed(fixedConstant.getCam().getGstCreditDisallowed());
			cam.setEffectiveGstCost(fixedConstant.getCam().getEffectiveGstCost());

			fetchedFixedConstant.setCam(cam);
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getElectricityAndWater())) {
			GstType electricityAndWater = fetchedFixedConstant.getElectricityAndWater();

			electricityAndWater.setGstApplicable(fixedConstant.getElectricityAndWater().getGstApplicable());
			electricityAndWater.setGstCreditDisallowed(fixedConstant.getElectricityAndWater().getGstCreditDisallowed());
			electricityAndWater.setEffectiveGstCost(fixedConstant.getElectricityAndWater().getEffectiveGstCost());

			fetchedFixedConstant.setElectricityAndWater(electricityAndWater);
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getOther())) {
			GstType other = fetchedFixedConstant.getOther();

			other.setGstApplicable(fixedConstant.getOther().getGstApplicable());
			other.setGstCreditDisallowed(fixedConstant.getOther().getGstCreditDisallowed());
			other.setEffectiveGstCost(fixedConstant.getOther().getEffectiveGstCost());

			fetchedFixedConstant.setOther(other);
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getCapexWOInputCredit())) {
			GstType capexWOInputCredit = fetchedFixedConstant.getCapexWOInputCredit();

			capexWOInputCredit.setGstApplicable(fixedConstant.getCapexWOInputCredit().getGstApplicable());
			capexWOInputCredit.setGstCreditDisallowed(fixedConstant.getCapexWOInputCredit().getGstCreditDisallowed());
			capexWOInputCredit.setEffectiveGstCost(fixedConstant.getCapexWOInputCredit().getEffectiveGstCost());

			fetchedFixedConstant.setCapexWOInputCredit(capexWOInputCredit);
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getCapexWPartialInputCredit())) {
			GstType capexWPartialInputCredit = fetchedFixedConstant.getCapexWPartialInputCredit();

			capexWPartialInputCredit.setGstApplicable(fixedConstant.getCapexWPartialInputCredit().getGstApplicable());
			capexWPartialInputCredit
					.setGstCreditDisallowed(fixedConstant.getCapexWPartialInputCredit().getGstCreditDisallowed());
			capexWPartialInputCredit
					.setEffectiveGstCost(fixedConstant.getCapexWPartialInputCredit().getEffectiveGstCost());

			fetchedFixedConstant.setCapexWPartialInputCredit(capexWPartialInputCredit);
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getCapexWFullInputCredit())) {
			GstType capexWFullInputCredit = fetchedFixedConstant.getCapexWFullInputCredit();

			capexWFullInputCredit.setGstApplicable(fixedConstant.getCapexWFullInputCredit().getGstApplicable());
			capexWFullInputCredit
					.setGstCreditDisallowed(fixedConstant.getCapexWFullInputCredit().getGstCreditDisallowed());
			capexWFullInputCredit.setEffectiveGstCost(fixedConstant.getCapexWFullInputCredit().getEffectiveGstCost());

			fetchedFixedConstant.setCapexWFullInputCredit(capexWFullInputCredit);
		}

		return fixedConstantDao.save(fetchedFixedConstant);
	}

	@Override
	public FixedConstant saveOperatingAssumption(FixedConstant fixedConstant) {

		if (ObjectUtils.isEmpty(fixedConstant)) {
			log.info("Operating_Assumption object cannot be null or empty");
			throw new FeasibilityException("ATP-CAP object cannot be null or empty");
		}

		FixedConstant fetchedFixedConstant = getFixedConstant();
		Map<String, Double> existing = fetchedFixedConstant.getAvgShowPerDay();
		log.info("map: " + fixedConstant.getOperating().toString());

		if (!ObjectUtils.isEmpty(fixedConstant.getOperating())) {

			List<OperatingMapper> opratingMapper = fixedConstant.getOperating();
			opratingMapper.forEach(avg -> {
				existing.put(avg.getKey(), avg.getValue());
			});
			fetchedFixedConstant.setAvgShowPerDay(existing);
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getWorkingDays())) {
			fetchedFixedConstant.setWorkingDays(fixedConstant.getWorkingDays());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getYearOneFootfallAssumedLowerBy())) {
			fetchedFixedConstant.setYearOneFootfallAssumedLowerBy(fixedConstant.getYearOneFootfallAssumedLowerBy());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getComplimentaryTicketsPercent())) {
			fetchedFixedConstant.setComplimentaryTicketsPercent(fixedConstant.getComplimentaryTicketsPercent());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getComplimentaryFnBPercent())) {
			fetchedFixedConstant.setComplimentaryFnBPercent(fixedConstant.getComplimentaryFnBPercent());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getYearOneAdIncomeLowerByPercent())) {
			fetchedFixedConstant.setYearOneAdIncomeLowerByPercent(fixedConstant.getYearOneAdIncomeLowerByPercent());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getFnbGst())) {
			fetchedFixedConstant.setFnbGst(fixedConstant.getFnbGst());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getFnbCogs())) {
			fetchedFixedConstant.setFnbCogs(fixedConstant.getFnbCogs());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getNetConvenienceFeePerTicket())) {
			fetchedFixedConstant.setNetConvenienceFeePerTicket(fixedConstant.getNetConvenienceFeePerTicket());
		}
		/*
		 * if (!ObjectUtils.isEmpty(fixedConstant.getFilmHire())) { FilmHireMapper
		 * filmHire = fetchedFixedConstant.getFilmHires();
		 * 
		 * filmHire.setMainstream(fixedConstant.getFilmHires().getMainstream());
		 * filmHire.setGold(fixedConstant.getFilmHires().getGold());
		 * filmHire.setImax(fixedConstant.getFilmHires().getImax());
		 * filmHire.setPxl(fixedConstant.getFilmHires().getPxl());
		 * filmHire.setDx(fixedConstant.getFilmHires().getDx());
		 * filmHire.setPlayhouse(fixedConstant.getFilmHires().getPlayhouse());
		 * filmHire.setUlteraPremium(fixedConstant.getFilmHires().getUlteraPremium());
		 * filmHire.setOnyx(fixedConstant.getFilmHires().getOnyx());
		 * 
		 * fetchedFixedConstant.setFilmHires(filmHire); }
		 */

		/*
		 * if (!ObjectUtils.isEmpty(fixedConstant.getVpfIncomeDetails())) {
		 * 
		 * List<VpfIncomeDetails> exit = fetchedFixedConstant.getVpfIncomeDetails();
		 * 
		 * List<VpfIncomeDetails> vpfData = fixedConstant.getVpfIncomeDetails();
		 * vpfData.forEach(vpf -> { VpfIncomeDetails vpfIncome = new VpfIncomeDetails();
		 * vpfIncome.setScreen(vpf.getScreen());
		 * vpfIncome.setOperator(vpf.getOperator()); vpfIncome.setValue(vpf.getValue());
		 * vpfIncome.setApplicableYear(vpf.getApplicableYear()); exit.add(vpfIncome);
		 * });
		 * 
		 * fetchedFixedConstant.setVpfIncomeDetails(exit); }
		 */
		return fixedConstantDao.save(fetchedFixedConstant);
	}

	@Override
	public FixedConstant saveGrowthAssumptions(FixedConstant fixedConstant) {
		FixedConstant fetchedFixedConstant = null;
		try {
			fetchedFixedConstant = getFixedConstant();
			RevenueAssumptionsGrowthRate  revenueAssumptions = fixedConstant.getRevenueAssumptionsGrowthRate();
			CostAssumptionsGrowthRate  costAssumptions = fixedConstant.getCostAssumptionsGrowthRate();
			
			Map<Integer, Double> admitsGrowthRate = new TreeMap<Integer, Double>(revenueAssumptions.getAdmitsGrowth());
			Map<Integer, Double> atpGrowthRate = new TreeMap<Integer, Double>(revenueAssumptions.getAtpGrowth());
			Map<Integer, Double> sphGrowthRate = new TreeMap<Integer, Double>(revenueAssumptions.getSphGrowth());
			Map<Integer, Double> sponsorshipIncomeGrowth = new TreeMap<Integer, Double>(revenueAssumptions.getSponsorshipIncomeGrowth());
			Map<Integer, Double> convenienceFeeGrowth = new TreeMap<Integer, Double>(revenueAssumptions.getConvenienceFeeGrowth());
			Map<Integer, Double> otherIncomeGrowth = new TreeMap<Integer, Double>(revenueAssumptions.getOtherIncomeGrowth());
			
			revenueAssumptions.setAdmitsGrowth(admitsGrowthRate);
			revenueAssumptions.setAtpGrowth(atpGrowthRate);
			revenueAssumptions.setSphGrowth(sphGrowthRate);
			revenueAssumptions.setSponsorshipIncomeGrowth(sponsorshipIncomeGrowth);
			revenueAssumptions.setConvenienceFeeGrowth(convenienceFeeGrowth);
			revenueAssumptions.setOtherIncomeGrowth(otherIncomeGrowth);
			
			Map<Integer, Double> personnelExpenses = new TreeMap<Integer, Double>(costAssumptions.getPersonnelExpenses());
			Map<Integer, Double> electricityAndAirconditioning = new TreeMap<Integer, Double>(costAssumptions.getElectricityAndAirconditioning());
			Map<Integer, Double> repairAndMaintenance = new TreeMap<Integer, Double>(costAssumptions.getRepairAndMaintenance());
			Map<Integer, Double> otherOverheadExpenses = new TreeMap<Integer, Double>(costAssumptions.getOtherOverheadExpenses());
			Map<Integer, Double> propertyTax = new TreeMap<Integer, Double>(costAssumptions.getPropertyTax());
			
			costAssumptions.setPersonnelExpenses(personnelExpenses);
			costAssumptions.setElectricityAndAirconditioning(electricityAndAirconditioning);
			costAssumptions.setRepairAndMaintenance(repairAndMaintenance);
			costAssumptions.setOtherOverheadExpenses(otherOverheadExpenses);
			costAssumptions.setPropertyTax(propertyTax);
			
			fetchedFixedConstant.setRevenueAssumptionsGrowthRate(revenueAssumptions);
			fetchedFixedConstant.setCostAssumptionsGrowthRate(costAssumptions);
			fetchedFixedConstant.setAdminLeasePeriod(fixedConstant.getAdminLeasePeriod());
			fixedConstantDao.save(fetchedFixedConstant);

		} catch (Exception e) {
			log.error("error in saving growth assumption service");
			throw e;
		}
		return fetchedFixedConstant;
	}

	@Override
	public FixedConstant saveAtpsCap(FixedConstant fixedConstant) {

		System.out.println("rest service: " + fixedConstant);
		if (ObjectUtils.isEmpty(fixedConstant)) {
			log.info("Atp Cap object cannot be null or empty");
			throw new FeasibilityException("Service charge object cannot be null or empty");
		}

		FixedConstant fetchedFixedConstant = getFixedConstant();
		List<AtpCapDetials> atpCap = fetchedFixedConstant.getAtpCapDetails();
		
		String state = fixedConstant.getAtpCapDetails().get(0).getState();
		atpCap = atpCap.stream().filter(p-> !p.getState().equals(state)).collect(Collectors.toList());
		
		Map<String, Map<String, Double>> map = new HashMap<>();
		Map<String, Double> internalMap = new HashMap<>();

		internalMap.put(fixedConstant.getAtpCapDetails().get(0).getSeatType().get(0).trim(),
				fixedConstant.getAtpCapDetails().get(0).getCamValue().get(0));
		internalMap.put(fixedConstant.getAtpCapDetails().get(0).getSeatType().get(1).trim(),
				fixedConstant.getAtpCapDetails().get(0).getCamValue().get(1));
		internalMap.put(fixedConstant.getAtpCapDetails().get(0).getSeatType().get(2).trim(),
				fixedConstant.getAtpCapDetails().get(0).getCamValue().get(2));

		map.put(fixedConstant.getScreenType(), internalMap);
		
		AtpCapDetials updatedCapDetail = new AtpCapDetials();
		updatedCapDetail.setAtpDetails(map);
		updatedCapDetail.setState(state);
		
		updatedCapDetail.setAtpGrowthRate(20.0);
		updatedCapDetail.setGrowthYear(5);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
		String currentDate = sdf.format(new Date());
		log.info("date of revision is "+currentDate);
		updatedCapDetail.setDateOfRevision(currentDate);
		
		atpCap.add(updatedCapDetail);
		fetchedFixedConstant.setAtpCapDetails(atpCap);
		
//		if (!ObjectUtils.isEmpty(fixedConstant.getAtpCapDetails())) {
//
//			Map<String, Map<String, Double>> map = new HashMap<>();
//			Map<String, Double> internalMap = new HashMap<>();
//
//			fetchedFixedConstant = getFixedConstant();
//
//			internalMap.put(fixedConstant.getAtpCapDetails().get(0).getSeatType().get(0).trim(),
//					fixedConstant.getAtpCapDetails().get(0).getCamValue().get(0));
//			internalMap.put(fixedConstant.getAtpCapDetails().get(0).getSeatType().get(1).trim(),
//					fixedConstant.getAtpCapDetails().get(0).getCamValue().get(1));
//			internalMap.put(fixedConstant.getAtpCapDetails().get(0).getSeatType().get(2).trim(),
//					fixedConstant.getAtpCapDetails().get(0).getCamValue().get(2));
//
//			map.put(fixedConstant.getScreenType(), internalMap);
//			boolean matchedFlag = false;
//
//			for (AtpCapDetials action : fetchedFixedConstant.getAtpCapDetails()) {
//				if (action.getState().equalsIgnoreCase(fixedConstant.getAtpCapDetails().get(0).getState())) {
//					if (action.getAtpDetails().containsKey(fixedConstant.getScreenType())) {
//						action.getAtpDetails().put(fixedConstant.getScreenType(), internalMap);
//						action.setGrowthYear(fixedConstant.getAtpCapDetails().get(0).getGrowthYear());
//						action.setAtpGrowthRate(fixedConstant.getAtpCapDetails().get(0).getAtpGrowthRate());
//						matchedFlag = true;
//					}
//				}
//			}
//
//			if (matchedFlag != true) {
//				AtpCapDetials atpCapDetials = new AtpCapDetials();
//				atpCapDetials.setAtpDetails(map);
//				atpCapDetials.setGrowthYear(fixedConstant.getAtpCapDetails().get(0).getGrowthYear());
//				// atpCapDetials.setStartYear(fixedConstant.getAtpCapDetails().get(0).getStartYear());
//				atpCapDetials.setState(fixedConstant.getAtpCapDetails().get(0).getState());
//				atpCapDetials.setAtpGrowthRate(fixedConstant.getAtpCapDetails().get(0).getAtpGrowthRate());
//				fetchedFixedConstant.getAtpCapDetails().add(atpCapDetials);
//			}
//
//		}
		
		return fixedConstantDao.save(fetchedFixedConstant);
	}

	@Override
	public FixedConstant irrConstantDetails(FixedConstant fixedConstant) {

		if (ObjectUtils.isEmpty(fixedConstant)) {
			log.info("Fixed Constant object cannot be null or empty");
			throw new FeasibilityException("Fixed Constant object cannot be null or empty");
		}

		FixedConstant existingFixedConstant = getFixedConstant();

		if (!ObjectUtils.isEmpty(fixedConstant.getWorkingDays())) {
			existingFixedConstant.setWorkingDays(fixedConstant.getWorkingDays());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getTax())) {
			existingFixedConstant.setTax(fixedConstant.getTax());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getYearOneFootfallAssumedLowerBy())) {
			existingFixedConstant.setYearOneFootfallAssumedLowerBy(fixedConstant.getYearOneFootfallAssumedLowerBy());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getComplimentaryTicketsPercent())) {
			existingFixedConstant.setComplimentaryTicketsPercent(fixedConstant.getComplimentaryTicketsPercent());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getComplimentaryFnBPercent())) {
			existingFixedConstant.setComplimentaryFnBPercent(fixedConstant.getComplimentaryFnBPercent());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getYearOneAdIncomeLowerByPercent())) {
			existingFixedConstant.setYearOneAdIncomeLowerByPercent(fixedConstant.getYearOneAdIncomeLowerByPercent());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getFnbGst())) {
			existingFixedConstant.setFnbGst(fixedConstant.getFnbGst());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getFnbCogs())) {
			existingFixedConstant.setFnbCogs(fixedConstant.getFnbCogs());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getNetConvenienceFeePerTicket())) {
			existingFixedConstant.setNetConvenienceFeePerTicket(fixedConstant.getNetConvenienceFeePerTicket());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getInitailCapexInOneYear())) {
			existingFixedConstant.setInitailCapexInOneYear(fixedConstant.getInitailCapexInOneYear());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getInitailCapexInZeroYear())) {
			existingFixedConstant.setInitailCapexInZeroYear(fixedConstant.getInitailCapexInZeroYear());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getInitailCapexInSevenYear())) {
			existingFixedConstant.setInitailCapexInSevenYear(fixedConstant.getInitailCapexInSevenYear());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getDebtProportion())) {
			existingFixedConstant.setDebtProportion(fixedConstant.getDebtProportion());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getEquityProportion())) {
			existingFixedConstant.setEquityProportion(fixedConstant.getEquityProportion());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getCostOfDebt())) {
			existingFixedConstant.setCostOfDebt(fixedConstant.getCostOfDebt());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getDebtTenure())) {
			existingFixedConstant.setDebtTenure(fixedConstant.getDebtTenure());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getCostOfEquity())) {
			existingFixedConstant.setCostOfEquity(fixedConstant.getCostOfEquity());
		}

		/*
		 * if(!ObjectUtils.isEmpty(fixedConstant.getCapexContingency())) {
		 * existingFixedConstant.setCapexContingency(fixedConstant.getCapexContingency()
		 * ); }
		 */

		if (!ObjectUtils.isEmpty(fixedConstant.getIncomeTax())) {
			existingFixedConstant.setIncomeTax(fixedConstant.getIncomeTax());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getRentAndCamDiscountRate())) {
			existingFixedConstant.setRentAndCamDiscountRate(fixedConstant.getRentAndCamDiscountRate());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getMaintenanceCapexOfRevenueForAllYearTillLastRenovation())) {
			existingFixedConstant.setMaintenanceCapexOfRevenueForAllYearTillLastRenovation(
					fixedConstant.getMaintenanceCapexOfRevenueForAllYearTillLastRenovation());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getMaintenanceCapexOfRevenueForYearAfterLastRenovation())) {
			existingFixedConstant.setMaintenanceCapexOfRevenueForYearAfterLastRenovation(
					fixedConstant.getMaintenanceCapexOfRevenueForYearAfterLastRenovation());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getMarketingExpenseLaunch())) {
			existingFixedConstant.setMarketingExpenseLaunch(fixedConstant.getMarketingExpenseLaunch());
		}

		if (!ObjectUtils.isEmpty(fixedConstant.getSphToAtpRatio())) {
			existingFixedConstant.setSphToAtpRatio(fixedConstant.getSphToAtpRatio());
		}
		
		return fixedConstantDao.save(existingFixedConstant);
	}

}
