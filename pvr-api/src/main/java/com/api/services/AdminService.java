package com.api.services;

import java.util.List;
import java.util.Map;

import com.common.mappers.FhcMapper;
import com.common.models.CinemaCategoryMaster;
import com.common.models.CinemaFormatMaster;
import com.common.models.FixedConstant;
import com.common.models.ProjectDetails;
import com.common.models.SeatTypeMaster;


public interface AdminService {

	boolean saveFhc(FhcMapper fhcMapper);

	boolean isFhcPresent(FhcMapper fhcMapper);
	
	List<CinemaFormatMaster> getAllCinemaFormat();

	boolean deleteAtp(FixedConstant fixedConstant);

	CinemaFormatMaster getCinemaFormat(String id);
	
	boolean saveCinemaFormat(CinemaFormatMaster cinemaCategoryMaster);
	
	List<CinemaCategoryMaster> getAllCinemaCategory();

	CinemaCategoryMaster getCinemaCategory(String id);
	
	boolean saveCinemaCategory(CinemaCategoryMaster cinemaCategoryMaster);
	
	List<SeatTypeMaster> getAllSeatType();

	SeatTypeMaster getSeatType(String id);
	
	boolean saveSeatType(SeatTypeMaster seatTypeMaster);

	boolean saveCogs(FhcMapper fhcMapper);

	boolean deleteOperatingAssumption(FhcMapper fhcMapper);

	boolean saveGstOnTicket(FhcMapper fhcMapper);

	List<ProjectDetails> getOpsReportsList();

	List<ProjectDetails> getExitReportsList();

	List<ProjectDetails> updateOpsReportsList(List<Integer> paramList);

	List<ProjectDetails> updateExtReports(List<Integer> paramList);

	List<ProjectDetails> getListForProjName();

	List<ProjectDetails> updateProjectName(Map<String, String> projData);

	boolean saveScreenType(CinemaFormatMaster cinemaFormatMaster);
}
