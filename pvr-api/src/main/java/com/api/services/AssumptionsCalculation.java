package com.api.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.api.dao.FixedConstantDao;
import com.api.utils.MathUtil;
import com.common.constants.Constants;
import com.common.models.AssumptionsCalculationData;
import com.common.models.AssumptionsValues;
import com.common.models.AtpAssumptions;
import com.common.models.CinemaFormat;
import com.common.models.FixedConstant;
import com.common.models.ProjectDetails;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AssumptionsCalculation {

	@Autowired
	private FixedConstantDao constantDao;

	@Autowired
	private IrrCalculation irrCalculation;

	public ProjectDetails calculateAssumptionsDetails(Optional<ProjectDetails> projectDetails) {

		try {
			Integer noOfScreen = projectDetails.get().getNoOfScreens();

			String city = projectDetails.get().getProjectLocation().getCity();
			Integer totalSeat = projectDetails.get().getTotalColumnValSum();
			List<FixedConstant> fixedConstant = (List<FixedConstant>) constantDao.findAll();
			List<CinemaFormat> cinemaFormat = projectDetails.get().getCinemaFormat();

			Integer workingDays = fixedConstant.get(0).getWorkingDays();
			Double noOfShows = !ObjectUtils.isEmpty(projectDetails.get().getNoOfShows().getInputValues())
				    ? projectDetails.get().getNoOfShows().getInputValues() : projectDetails.get().getNoOfShows().getSuggestedValue();
				    
//			Double totalshow = getTotalShow(noOfScreen, city, fixedConstant);
			Double totalshow = MathUtil.roundTwoDecimals(workingDays * noOfScreen * noOfShows);

//			Double totalCapacity = getTotalCapacity(totalSeat, city, fixedConstant);
			Double totalCapacity = MathUtil.roundTwoDecimals(totalSeat * workingDays * noOfShows);

//			Map<String, Map<String, Double>> capacityforEachScreenSeatType = getCapacityByEachScreenSeatType(
//					fixedConstant, city, cinemaFormat);			
			Map<String, Map<String, Double>> capacityForEachScreenSeatType = new HashMap<>();
			if (!ObjectUtils.isEmpty(cinemaFormat)) {
				
				Map<String, Map<String, Integer>> eachScreenSeatType = new HashMap<>();
				cinemaFormat.forEach(eachScreenType -> {
					Map<String, Integer> maps = new HashMap<>();

					if (eachScreenSeatType.containsKey(eachScreenType.getFormatType())) {
						Map<String, Integer> temp = eachScreenSeatType.get(eachScreenType.getFormatType());
						temp.put(Constants.NORMAL, temp.get(Constants.NORMAL) + eachScreenType.getNormal());
						temp.put(Constants.RECLINER, temp.get(Constants.RECLINER) + eachScreenType.getRecliners());
						temp.put(Constants.LOUNGER, temp.get(Constants.LOUNGER) + eachScreenType.getLounger());
						eachScreenSeatType.put(eachScreenType.getFormatType(), temp);

					} else {
						maps.put(Constants.NORMAL, eachScreenType.getNormal());
						maps.put(Constants.RECLINER, eachScreenType.getRecliners());
						maps.put(Constants.LOUNGER, eachScreenType.getLounger());
						eachScreenSeatType.put(eachScreenType.getFormatType(), maps);
					}
				});
				eachScreenSeatType.forEach((k, v) -> {
					Map<String, Double> maps = new HashMap<>();
					maps.put(Constants.NORMAL, v.get(Constants.NORMAL) * workingDays * noOfShows);
					maps.put(Constants.RECLINER, v.get(Constants.RECLINER) * workingDays * noOfShows);
					maps.put(Constants.LOUNGER, v.get(Constants.LOUNGER) * workingDays * noOfShows);
					capacityForEachScreenSeatType.put(k, maps);
				});
			}

			
			Map<String, Double> occupancyForEachScreenType = getoccupancyForEachScreenType(projectDetails);
			
			Map<String, Double> occupancyForEachScreenTypeForSuggestedValue = getoccupancyForEachScreenTypeForSuggestedValue(projectDetails);

			Map<String, Map<String, Double>> admitsForEachScreenSeatType = getAdmitsForEachScreenSeatType(
					capacityForEachScreenSeatType, occupancyForEachScreenType);
			
			Map<String, Map<String, Double>> admitsForEachScreenSeatTypeForSuggestedValue = getAdmitsForEachScreenSeatType(
					capacityForEachScreenSeatType, occupancyForEachScreenTypeForSuggestedValue);
		

			Map<String, Double> admitsForEachScreenType = getAdmitsForEachScreenType(admitsForEachScreenSeatType);
			
			Map<String, Double> admitsForEachScreenTypeForSuggestedValue = getAdmitsForEachScreenType(admitsForEachScreenSeatTypeForSuggestedValue);
			

			Double totalAdmits = getTotalAdmits(admitsForEachScreenType);
			
			Double totalAdmitsForSuggestedValue = getTotalAdmits(admitsForEachScreenTypeForSuggestedValue);
			
			

			Double totalOccupancy = MathUtil.roundTwoDecimals((totalAdmits / totalCapacity) * 100);
			
			Double totalOccupancySuggestedValues = MathUtil.roundTwoDecimals((totalAdmitsForSuggestedValue / totalCapacity) * 100);
			
			
			

			Map<String, Map<String, Double>> atpForEachScreenSeatType = getAtpForEachScreenSeatType(projectDetails);
			
			Map<String, Map<String, Double>> atpForEachScreenSeatTypeForSuggestedValue = getAtpForEachScreenSeatTypeForSuggestedValue(projectDetails);
			
			
			

			Map<String, Double> totalAtpForEachFormat = getTotalAtpForEachFormat(admitsForEachScreenSeatType,
					atpForEachScreenSeatType);
			
			
			Map<String, Double> totalAtpForEachFormatForSuggestedValue = getTotalAtpForEachFormat(admitsForEachScreenSeatTypeForSuggestedValue,
					atpForEachScreenSeatTypeForSuggestedValue);
			
			
			

			Map<String, Double> totalAtpForEachSeatType = getTotalAtpForEachSeatType(admitsForEachScreenSeatType,
					atpForEachScreenSeatType);

			Double totalAtp = getTotalAtp(admitsForEachScreenType, totalAtpForEachFormat);
			
			Double totalAtpForSuggestedValue = getTotalAtp(admitsForEachScreenTypeForSuggestedValue, totalAtpForEachFormatForSuggestedValue);
			
			
			
			
			

			Map<String, Double> grossBoxOfficeCollectionForEachFormat = getTotalBoxOfficeCollectionForEachFormat(
					admitsForEachScreenType, totalAtpForEachFormat, fixedConstant);

			Double grossBoxOfficeCollection = getTotalGrossBoxOfficeCollection(grossBoxOfficeCollectionForEachFormat);

			Map<String, Double> gstOnTicketRevenueForEachFormat = getGstOnTicketRevenueForEachFormat(
					totalAtpForEachFormat,fixedConstant);

			Map<String, Double> netBoxOfficeCollectionForEachFormat = getNetBoxOfficeCollectionForEachFormat(
					grossBoxOfficeCollectionForEachFormat, gstOnTicketRevenueForEachFormat);

			Double totalNetBoxOfficeCollection = getTotalNetBoxOfficeCollection(netBoxOfficeCollectionForEachFormat);

			Double netGstOnBoxOffice = getNetGstOnBoxOffice(grossBoxOfficeCollection, totalNetBoxOfficeCollection);

			Map<String, Double> formatWiseFilmHireCost = getFormatWiseFilmHireCost(netBoxOfficeCollectionForEachFormat,
					fixedConstant);

			Double totalFilmHireCost = getTotalFilmHireCost(formatWiseFilmHireCost);

			Double totalFilmHireCostPercentage = getTotalFilmHireCostPercentage(totalFilmHireCost,
					totalNetBoxOfficeCollection);

			Map<String, Double> sphForEachScreeenType = getSphForEachScreeenType(projectDetails);
			
			Map<String, Double> sphForEachScreeenTypeForSuggestedValue = getSphForEachScreeenTypeForSuggestedValue(projectDetails);
			

			Double totalSph = getTotalSph(sphForEachScreeenType, admitsForEachScreenType, totalAdmits);
			
			Double totalSphForSuggestedValue = getTotalSph(sphForEachScreeenTypeForSuggestedValue, admitsForEachScreenTypeForSuggestedValue, totalAdmitsForSuggestedValue);
			

			Map<String, Double> grossFoodAndBeveragesRevenueByScreenType = getGrossFoodAndBeveragesRevenueByScreenType(
					sphForEachScreeenType, admitsForEachScreenType);

			Map<String, Double> netFoodAndBeveragesRevenueByScreenType = getNetFoodAndBeveragesRevenueByScreenType(
					grossFoodAndBeveragesRevenueByScreenType, fixedConstant);

//			Map<String, Double> cogsByScreenType = getCogsByScreenType(netFoodAndBeveragesRevenueByScreenType,
//					fixedConstant);
			Map<String, Double> cogsByScreenType = new HashMap<>();
			Double fnbCogs = !ObjectUtils.isEmpty(projectDetails.get().getCogs().getInputValues()) 
					? projectDetails.get().getCogs().getInputValues() : projectDetails.get().getCogs().getSuggestedValue();           
	
			netFoodAndBeveragesRevenueByScreenType.forEach((key, value) -> {
				Double totalCogs = MathUtil.roundTwoDecimals(value * (fnbCogs / Constants.HUNDRED));
				cogsByScreenType.put(key, totalCogs);
			});

			// till year 2024
			Double vpfIncome = getVpfIncome(noOfScreen,fixedConstant);

			Double webSalePercentageOfTotalSale = projectDetails.get().getWebSale().getInputValues();

			Double netConvenienceFee = getNetConvenienceFee(webSalePercentageOfTotalSale, totalAdmits, fixedConstant);

			Double municipalTaxesPerAnnum = getMunicipalTaxesPerAnnum(fixedConstant, projectDetails);

			projectDetails.get().getFinalSummary().getAtp().setInputValues(totalAtp);
			projectDetails.get().getFinalSummary().getOccupancy().setInputValues(totalOccupancy);
			projectDetails.get().getFinalSummary().getSph().setInputValues(totalSph);
			projectDetails.get().getFinalSummary().getTotalAdmits().setInputValues(totalAdmits);
			
			projectDetails.get().getFinalSummary().getOccupancy().setCostType(totalOccupancySuggestedValues);
			projectDetails.get().getFinalSummary().getAtp().setCostType(totalAtpForSuggestedValue);
			projectDetails.get().getFinalSummary().getSph().setCostType(totalSphForSuggestedValue);

			AssumptionsCalculationData assumptionsCalculationData = new AssumptionsCalculationData();

			assumptionsCalculationData.setTotalAdmits(totalAdmits);
			assumptionsCalculationData.setTotalAtp(totalAtp);
			assumptionsCalculationData.setTotalSph(totalSph);
			assumptionsCalculationData.setTotalCapacity(totalCapacity);
			assumptionsCalculationData.setNetGstOnBoxOffice(netGstOnBoxOffice);
			assumptionsCalculationData.setTotalAtpForEachSeatType(totalAtpForEachSeatType);

			assumptionsCalculationData.setAdmitsForEachScreenType(admitsForEachScreenType);
			assumptionsCalculationData.setAdmitsForEachScreenSeatType(admitsForEachScreenSeatType);
			assumptionsCalculationData.setTotalshow(totalshow);
			assumptionsCalculationData.setNetConvenienceFee(netConvenienceFee);
			assumptionsCalculationData.setVpfIncome(vpfIncome);
			assumptionsCalculationData.setMunicipalTaxesPerAnnum(municipalTaxesPerAnnum);
			assumptionsCalculationData.setTotalFilmHireCostPercentage(totalFilmHireCostPercentage);
			
			irrCalculation.calculateIrrDetails(projectDetails, assumptionsCalculationData);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating assumptions calculation");
		}

		return projectDetails.get();

	}

//	public Double getTotalShow(Integer noOfScreen, String city, List<FixedConstant> fixedConstant) {
//
//		Double totalShow = 0.0;
//		try {
//			Integer workingDays = fixedConstant.get(0).getWorkingDays();
//			Map<String, Double> avgShowPerDay = fixedConstant.get(0).getAvgShowPerDay();
//			Double avgshow = 0.0;
//			
//			if (avgShowPerDay.containsKey(city)) {
//				avgshow = avgShowPerDay.get(city);
//			} else {
//				avgshow = avgShowPerDay.get(Constants.AVG_FIXED_SHOW);
//			}
//
//			totalShow = noOfScreen * workingDays * avgshow;
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("error while calculating total show");
//		}
//		return totalShow;
//	}

//	public Double getTotalCapacity(Integer totalSeat, String city, List<FixedConstant> fixedConstant) {
//
//		Double totalCapacity = 0.0;
//		try {
//			Integer workingDays = fixedConstant.get(0).getWorkingDays();
//			Map<String, Double> avgShowPerDay = fixedConstant.get(0).getAvgShowPerDay();
//			Double avgshow = 0.0;
//			
//			if (avgShowPerDay.containsKey(city)) {
//				avgshow = avgShowPerDay.get(city);
//			} else {
//				avgshow = avgShowPerDay.get(Constants.AVG_FIXED_SHOW);
//			}
//
//			totalCapacity = MathUtil.roundTwoDecimals(totalSeat * workingDays * avgshow);
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("error while calculating total capacity");
//		}
//		return totalCapacity;
//
//	}

//	public Map<String, Map<String, Double>> getCapacityByEachScreenSeatType(List<FixedConstant> fixedConstant,
//			String city, List<CinemaFormat> cinemaFormat) {
//
//		Map<String, Map<String, Double>> capacityForEachScreenSeatType = new HashMap<>();
//		try {
//			Integer workingDays = fixedConstant.get(0).getWorkingDays();
//			Map<String, Double> avgShowPerDay = fixedConstant.get(0).getAvgShowPerDay();
//			Double avgshow = 0.0;
//			if (avgShowPerDay.containsKey(city)) {
//				avgshow = avgShowPerDay.get(city);
//			} else {
//				avgshow = avgShowPerDay.get(Constants.AVG_FIXED_SHOW);
//			}
//			final Double noOfshow = avgshow;
//
//			Map<String, Map<String, Integer>> eachScreenSeatType = new HashMap<>();
//
//			if (!ObjectUtils.isEmpty(cinemaFormat)) {
//
//				cinemaFormat.forEach(eachScreenType -> {
//					Map<String, Integer> maps = new HashMap<>();
//
//					if (eachScreenSeatType.containsKey(eachScreenType.getFormatType())) {
//						Map<String, Integer> temp = eachScreenSeatType.get(eachScreenType.getFormatType());
//						temp.put(Constants.NORMAL, temp.get(Constants.NORMAL) + eachScreenType.getNormal());
//						temp.put(Constants.RECLINER, temp.get(Constants.RECLINER) + eachScreenType.getRecliners());
//						temp.put(Constants.LOUNGER, temp.get(Constants.LOUNGER) + eachScreenType.getLounger());
//						eachScreenSeatType.put(eachScreenType.getFormatType(), temp);
//
//					} else {
//						maps.put(Constants.NORMAL, eachScreenType.getNormal());
//						maps.put(Constants.RECLINER, eachScreenType.getRecliners());
//						maps.put(Constants.LOUNGER, eachScreenType.getLounger());
//						eachScreenSeatType.put(eachScreenType.getFormatType(), maps);
//					}
//
//				});
//
//				eachScreenSeatType.forEach((k, v) -> {
//					Map<String, Double> maps = new HashMap<>();
//					maps.put(Constants.NORMAL, v.get(Constants.NORMAL) * workingDays * noOfshow);
//					maps.put(Constants.RECLINER, v.get(Constants.RECLINER) * workingDays * noOfshow);
//					maps.put(Constants.LOUNGER, v.get(Constants.LOUNGER) * workingDays * noOfshow);
//					capacityForEachScreenSeatType.put(k, maps);
//
//				});
//
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("error while calculating Capacity By Each Screen Seat Type");
//		}
//		return capacityForEachScreenSeatType;
//
//	}

	public Map<String, Map<String, Double>> getAdmitsForEachScreenSeatType(
			Map<String, Map<String, Double>> capacityForEachScreenSeatType,
			Map<String, Double> occupancyForEachScreenType) {

		Map<String, Map<String, Double>> admitForEachScreenSeatType = new HashMap<>();
		try {
			Set<String> key = capacityForEachScreenSeatType.keySet();

			key.forEach(k -> {

				Map<String, Double> screenSeatCapacity = capacityForEachScreenSeatType.get(k);

				Double occupancyForScreen = occupancyForEachScreenType.get(k);

				Double admitForNormal = MathUtil.roundTwoDecimals(screenSeatCapacity.get(Constants.NORMAL) * occupancyForScreen);
				Double admitForRecliners = MathUtil.roundTwoDecimals(screenSeatCapacity.get(Constants.RECLINER) * occupancyForScreen);
				Double admitForLounger = MathUtil.roundTwoDecimals(screenSeatCapacity.get(Constants.LOUNGER) * occupancyForScreen);

				Map<String, Double> admitForScreenSeatType = new HashMap<>();
				admitForScreenSeatType.put(Constants.NORMAL, admitForNormal);
				admitForScreenSeatType.put(Constants.RECLINER, admitForRecliners);
				admitForScreenSeatType.put(Constants.LOUNGER, admitForLounger);

				admitForEachScreenSeatType.put(k, admitForScreenSeatType);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Admits For Each Screen Seat Type");
		}
		return admitForEachScreenSeatType;

	}

	public Map<String, Double> getAdmitsForEachScreenType(
			Map<String, Map<String, Double>> admitsForEachScreenSeatType) {

		Map<String, Double> totalAdmitsForEachScreenType = new HashMap<>();
		try {

			admitsForEachScreenSeatType.forEach((key, value) -> {

				Map<String, Double> admitsByseat = value;

				Double admitForNormalSeat = admitsByseat.get(Constants.NORMAL);
				Double admitForReclinersSeat = admitsByseat.get(Constants.RECLINER);
				Double admitForLoungerSeat = admitsByseat.get(Constants.LOUNGER);

				Double totalAdmit = MathUtil.roundTwoDecimals(admitForNormalSeat + admitForReclinersSeat + admitForLoungerSeat);
				totalAdmitsForEachScreenType.put(key, totalAdmit);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Admits For Each Screen Type");
		}
		return totalAdmitsForEachScreenType;

	}

	public Map<String, Double> getTotalAtpForEachFormat(Map<String, Map<String, Double>> admitsForEachScreenSeatType,
			Map<String, Map<String, Double>> atpForEachScreenSeatType) {

		Map<String, Double> totalAtpForEachFormat = new HashMap<>();
		try {

			Set<String> key = admitsForEachScreenSeatType.keySet();

			key.forEach(k -> {

				Map<String, Double> admit = admitsForEachScreenSeatType.get(k);
				Double admitForNormal = admit.get(Constants.NORMAL);
				Double admitForRecliners = admit.get(Constants.RECLINER);
				Double admitForLounger = admit.get(Constants.LOUNGER);

				Map<String, Double> atp = atpForEachScreenSeatType.get(k);
				Double atpForNormal = atp.get(Constants.NORMAL);
				Double atpForRecliners = atp.get(Constants.RECLINER);
				Double atpForLounger = atp.get(Constants.LOUNGER);
				Double atpForEachFormat = 0.0;
				Double totalOfAdmit = MathUtil.roundTwoDecimals(admitForNormal + admitForRecliners + admitForLounger);
				if(totalOfAdmit != 0.0 ) {
					atpForEachFormat = MathUtil.roundTwoDecimals((admitForNormal * atpForNormal
							+ admitForRecliners * atpForRecliners + admitForLounger * atpForLounger) / totalOfAdmit);
				}
				else {
					atpForEachFormat = 0.0;
				}

				totalAtpForEachFormat.put(k, atpForEachFormat);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Total Atp For Each Format");
		}
		return totalAtpForEachFormat;

	}

	// ATP sum of all format for Single Seat (Normal, Recliners, Lounger)
	public Map<String, Double> getTotalAtpForEachSeatType(Map<String, Map<String, Double>> admitsForEachScreenSeatType,
			Map<String, Map<String, Double>> atpForEachScreenSeatType) {

		Map<String, Double> totalAtpForEachSeat = new HashMap<>();
		try {

			Set<String> key = admitsForEachScreenSeatType.keySet();
			Map<String, Double> formatTypeNormal = new HashMap<>();
			Map<String, Double> formatTypeRecliner = new HashMap<>();
			Map<String, Double> formatTypeLounger = new HashMap<>();

			List<Double> admitNormal = new ArrayList<>();
			List<Double> admitRecliner = new ArrayList<>();
			List<Double> admitlounger = new ArrayList<>();

			key.forEach(k -> {
				String format = k;

				Map<String, Double> admit = admitsForEachScreenSeatType.get(k);
				Map<String, Double> atp = atpForEachScreenSeatType.get(k);

				admit.get(Constants.NORMAL);
				admit.get(Constants.RECLINER);
				admit.get(Constants.LOUNGER);
				admitNormal.add(admit.get(Constants.NORMAL));
				admitRecliner.add(admit.get(Constants.RECLINER));
				admitlounger.add(admit.get(Constants.LOUNGER));

				Double seatNormal = MathUtil.roundTwoDecimals(admit.get(Constants.NORMAL) * atp.get(Constants.NORMAL));
				Double seatRecliner = MathUtil.roundTwoDecimals(admit.get(Constants.RECLINER) * atp.get(Constants.RECLINER));
				Double seatLounger = MathUtil.roundTwoDecimals(admit.get(Constants.LOUNGER) * atp.get(Constants.LOUNGER));

				formatTypeNormal.put(format, seatNormal);
				formatTypeRecliner.put(format, seatRecliner);
				formatTypeLounger.put(format, seatLounger);

			});

			List<Double> normal = new ArrayList<>();
			List<Double> recliner = new ArrayList<>();
			List<Double> lounger = new ArrayList<>();

			formatTypeNormal.forEach((keyType, value) -> {
				normal.add(value);
				recliner.add(formatTypeRecliner.get(keyType));
				lounger.add(formatTypeLounger.get(keyType));
			});

			Double sumOfNoraml = MathUtil.sum(normal);
			Double sumOfRecliner = MathUtil.sum(recliner);
			Double sumOfLounger = MathUtil.sum(lounger);

			Double sumOfAdmitNormal = MathUtil.sum(admitNormal);
			Double sumOfAdmitRecliner = MathUtil.sum(admitRecliner);
			Double sumOfAdmitlounger = MathUtil.sum(admitlounger);
			
			if(sumOfAdmitNormal == 0.0) {
				totalAtpForEachSeat.put(Constants.NORMAL,0.0);
			}else {
				totalAtpForEachSeat.put(Constants.NORMAL, MathUtil.roundTwoDecimals(sumOfNoraml / sumOfAdmitNormal));
			}
			
			if(sumOfAdmitRecliner == 0.0) {
				totalAtpForEachSeat.put(Constants.RECLINER, 0.0);
			}else {
				totalAtpForEachSeat.put(Constants.RECLINER, MathUtil.roundTwoDecimals(sumOfRecliner / sumOfAdmitRecliner));
			}

			if(sumOfAdmitlounger == 0.0) {
				totalAtpForEachSeat.put(Constants.LOUNGER, 0.0);
			}else {
				totalAtpForEachSeat.put(Constants.LOUNGER, MathUtil.roundTwoDecimals(sumOfLounger / sumOfAdmitlounger));
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Total Atp For Each Seat Type"+e.getMessage());
		}
		return totalAtpForEachSeat;

	}

	public Double getTotalAtp(Map<String, Double> admitsForEachScreenType, Map<String, Double> totalAtpForEachFormat) {

		Double totalAtp = 0.0;
		try {
			List<Double> totalAdmitForEachFormat = new ArrayList<Double>();
			List<Double> atpForEachFormat = new ArrayList<Double>();

			Map<String, Double> atpSum = new HashMap<>();
			admitsForEachScreenType.forEach((key, value) -> {

				totalAdmitForEachFormat.add(value);
				Double atpValue = value * totalAtpForEachFormat.get(key);
				atpSum.put(key, atpValue);
			});

			atpSum.forEach((key, value) -> {
				atpForEachFormat.add(value);
			});

			Double totalAdmitFormatWise = MathUtil.sum(totalAdmitForEachFormat);
			Double totalAtpFormatWise = MathUtil.sum(atpForEachFormat);

			totalAtp = MathUtil.roundTwoDecimals(totalAtpFormatWise / totalAdmitFormatWise);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Total Atp");
		}
		return totalAtp;

	}

	public Double getTotalGrossBoxOfficeCollection(Map<String, Double> grossBoxOfficeCollectionForEachFormat) {

		Double totalGrossBoxOfficeCollection = 0.0;
		try {
			List<Double> totalGrossBoxOfficeCollectionForEachFormat = new ArrayList<Double>();
			grossBoxOfficeCollectionForEachFormat.forEach((key, value) -> {
				totalGrossBoxOfficeCollectionForEachFormat.add(value);

			});

			totalGrossBoxOfficeCollection = MathUtil.roundTwoDecimals(MathUtil.sum(totalGrossBoxOfficeCollectionForEachFormat));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Total Gross Box Office Collection");
		}
		return totalGrossBoxOfficeCollection;
	}

	public Map<String, Double> getTotalBoxOfficeCollectionForEachFormat(Map<String, Double> admitsForEachScreenType,
			Map<String, Double> totalAtpForEachFormat, List<FixedConstant> fixedConstant) {

		Map<String, Double> totalGrossBoxOfficeCollectionForEachFormat = new HashMap<>();
		try {

			Double factor = fixedConstant.get(0).getFactor();
			admitsForEachScreenType.forEach((key, value) -> {

				Double gboc = (value * totalAtpForEachFormat.get(key)) / factor;
				totalGrossBoxOfficeCollectionForEachFormat.put(key, gboc);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Total Box Office Collection For Each Format");
		}
		return totalGrossBoxOfficeCollectionForEachFormat;

	}

	public Map<String, Double> getGstOnTicketRevenueForEachFormat(Map<String, Double> totalAtpForEachFormat,List<FixedConstant> fixedConstants) {

		
		Map<String, Double> gstOnTicketForEachFormat = fixedConstants.get(0).getGstOnTicketForEachFormat();
		try {
			totalAtpForEachFormat.forEach((k, v) -> {

				if (v > 100.0) {
					gstOnTicketForEachFormat.put(k, gstOnTicketForEachFormat.get("Greater_than_100")/100);// divide by 100
				} else {
					gstOnTicketForEachFormat.put(k, gstOnTicketForEachFormat.get("Less_than_100")/100);// divide by 100
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Gst On Ticket Revenue For Each Format");
		}
		return gstOnTicketForEachFormat;

	}

	public Map<String, Double> getNetBoxOfficeCollectionForEachFormat(
			Map<String, Double> grossBoxOfficeCollectionForEachFormat,
			Map<String, Double> gstOnTicketRevenueForEachFormat) {

		Map<String, Double> netBoxOfficeCollectionForEachFormat = new HashMap<>();
		try {
			Set<String> key = grossBoxOfficeCollectionForEachFormat.keySet();

			key.forEach(k -> {

				Double grossValue = grossBoxOfficeCollectionForEachFormat.get(k);
				Double gst = gstOnTicketRevenueForEachFormat.get(k);
				Double netGrossTotal = MathUtil.roundTwoDecimals(grossValue * (Constants.ONE - (gst) / (Constants.ONE + gst)));
				netBoxOfficeCollectionForEachFormat.put(k, netGrossTotal);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Net Box Office Collection For Each Format");
		}
		return netBoxOfficeCollectionForEachFormat;
	}

	public Double getTotalNetBoxOfficeCollection(Map<String, Double> netBoxOfficeCollectionForEachFormat) {

		Double totalNetBoxOfficeCollection = 0.0;
		try {
			List<Double> listOfNetBoxOfficeCollection = new ArrayList<Double>();
			netBoxOfficeCollectionForEachFormat.forEach((k, v) -> {
				listOfNetBoxOfficeCollection.add(v);
			});

			totalNetBoxOfficeCollection = MathUtil.roundTwoDecimals(MathUtil.sum(listOfNetBoxOfficeCollection));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Total Net Box Office Collection");
		}

		return totalNetBoxOfficeCollection;

	}

	public Double getNetGstOnBoxOffice(Double grossBoxOfficeCollection, Double totalNetBoxOfficeCollection) {
		Double netGstOnBoxOffice = 0.0;
		try {
			netGstOnBoxOffice = MathUtil
					.roundTwoDecimals(((grossBoxOfficeCollection / totalNetBoxOfficeCollection) - Constants.ONE) * Constants.HUNDRED);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Net Gst On Box Office");
		}
		return netGstOnBoxOffice;
	}

	public Map<String, Double> getFormatWiseFilmHireCost(Map<String, Double> netBoxOfficeCollectionForEachFormat,
			List<FixedConstant> fixedConstant) {

		Map<String, Double> formatWiseFilmHire = new HashMap<String, Double>();
		try {

			netBoxOfficeCollectionForEachFormat.forEach((key, value) -> {
				Double filmHire = MathUtil
						.roundTwoDecimals(value * (fixedConstant.get(0).getFilmHire().get(key)) / Constants.HUNDRED);
				formatWiseFilmHire.put(key, filmHire);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Format Wise Film Hire Cost");
		}
		return formatWiseFilmHire;
	}

	public Double getTotalFilmHireCost(Map<String, Double> formatWiseFilmHireCost) {
		Double totalFilmHireCost = 0.0;
		try {
			List<Double> listOfFilmHireCost = new ArrayList<Double>();
			formatWiseFilmHireCost.forEach((key, value) -> {
				listOfFilmHireCost.add(value);
			});

			totalFilmHireCost = MathUtil.roundTwoDecimals(MathUtil.sum(listOfFilmHireCost));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Total Film Hire Cost");
		}
		return totalFilmHireCost;

	}

	public Double getTotalFilmHireCostPercentage(Double totalFilmHireCost, Double totalNetBoxOfficeCollection) {

		Double totalFilmHireCostPercentage = 0.0;
		try {
			totalFilmHireCostPercentage = MathUtil
					.roundTwoDecimals((totalFilmHireCost / totalNetBoxOfficeCollection) * Constants.HUNDRED);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Total Film Hire Cost Percentage");
		}
		return totalFilmHireCostPercentage;

	}

	public Map<String, Double> getGrossFoodAndBeveragesRevenueByScreenType(Map<String, Double> sphForEachScreeenType,
			Map<String, Double> admitsForEachScreenType) {

		Map<String, Double> totalGrossFoodAndBeveragesRevenueByScreenType = new HashMap<>();
		try {
			Set<String> key = sphForEachScreeenType.keySet();

			key.forEach(k -> {

				Double sphByScreen = sphForEachScreeenType.get(k);
				Double admitsByScreen = admitsForEachScreenType.get(k);

				Double totalFoodAndBeverage = MathUtil.roundTwoDecimals((sphByScreen * admitsByScreen) / Constants.FACTOR);
				totalGrossFoodAndBeveragesRevenueByScreenType.put(k, totalFoodAndBeverage);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Gross Food And Beverages Revenue By Screen Type");
		}
		return totalGrossFoodAndBeveragesRevenueByScreenType;
	}

	public Map<String, Double> getNetFoodAndBeveragesRevenueByScreenType(
			Map<String, Double> grossFoodAndBeveragesRevenueByScreenType, List<FixedConstant> fixedConstant) {

		Map<String, Double> netFoodAndBeveragesRevenueByScreenType = new HashMap<>();
		try {

			grossFoodAndBeveragesRevenueByScreenType.forEach((k, v) -> {
				Double netFoodAndBeverageRevenue = MathUtil.roundTwoDecimals(
						v * (Constants.ONE - (fixedConstant.get(0).getFnbGst() / Constants.HUNDRED) / (Constants.ONE + (fixedConstant.get(0).getFnbGst() / Constants.HUNDRED))));
				netFoodAndBeveragesRevenueByScreenType.put(k, netFoodAndBeverageRevenue);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Net Food And Beverages Revenue By Screen Type");
		}
		return netFoodAndBeveragesRevenueByScreenType;
	}

//	public Map<String, Double> getCogsByScreenType(Map<String, Double> netFoodAndBeveragesRevenueByScreenType,
//			List<FixedConstant> fixedConstant) {
//
//		Map<String, Double> totalCogsByScreenType = new HashMap<>();
//		Double fnbCogs = fixedConstant.get(0).getFnbCogs();
//		
//		try {
//
//			netFoodAndBeveragesRevenueByScreenType.forEach((key, value) -> {
//				Double totalCogs = MathUtil.roundTwoDecimals(value * (fnbCogs / Constants.HUNDRED));
//				totalCogsByScreenType.put(key, totalCogs);
//			});
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("error while calculating Cogs By Screen Type");
//		}
//		return totalCogsByScreenType;
//	}

	public Double getVpfIncome(Integer noOfScreen, List<FixedConstant> fixedConstant) {

		Double vpfIncome = 0.0;
		try {
			Double perScreenIncome = 0.0;

			if (noOfScreen <= 5) {
				perScreenIncome = 5.0;
			} else if (noOfScreen <= 10.0) {
				perScreenIncome = 4.5;
			} else if (noOfScreen <= 15) {
				perScreenIncome = 4.0;
			} else {
				perScreenIncome = 3.5;
			}

			vpfIncome = MathUtil.roundTwoDecimals((perScreenIncome * noOfScreen) * Constants.FACTOR);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Vpf Income");
		}
		return vpfIncome;
	}

	public Double getNetConvenienceFee(Double webSalePercentageOfTotalSale, Double totalAdmits,
			List<FixedConstant> fixedConstant) {

		Double netConvenienceFee = 0.0;
		try {
			Double netConvenienceFeePerTicket = fixedConstant.get(0).getNetConvenienceFeePerTicket();
			netConvenienceFee = MathUtil.roundTwoDecimals((webSalePercentageOfTotalSale / Constants.HUNDRED) * netConvenienceFeePerTicket * totalAdmits);
		} catch (Exception e) {
			log.error("error while calculating Net Convenience Fee");
		}
		return netConvenienceFee;
	}

	public Map<String, Double> getoccupancyForEachScreenType(Optional<ProjectDetails> projectDetails) {

		Map<String, Double> getoccupancyForEachScreenType = new HashMap<>();
		try {

			List<AssumptionsValues> occupancy = projectDetails.get().getOccupancy();
			occupancy.forEach(occupancyForEachScreenType -> {
				getoccupancyForEachScreenType.put(occupancyForEachScreenType.getFormatType(),
						occupancyForEachScreenType.getInputValues() / Constants.HUNDRED);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating occupancy For Each Screen Type");
		}
		return getoccupancyForEachScreenType;
	}
	
	
	public Map<String, Double> getoccupancyForEachScreenTypeForSuggestedValue(Optional<ProjectDetails> projectDetails) {

		Map<String, Double> getoccupancyForEachScreenType = new HashMap<>();
		try {

			List<AssumptionsValues> occupancy = projectDetails.get().getOccupancy();
			occupancy.forEach(occupancyForEachScreenType -> {
				getoccupancyForEachScreenType.put(occupancyForEachScreenType.getFormatType(),
						(double)occupancyForEachScreenType.getCostType() / Constants.HUNDRED);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating occupancy For Each Screen Type");
		}
		return getoccupancyForEachScreenType;
	}
	
	
	
	

	public Double getTotalAdmits(Map<String, Double> admitsForEachScreenType) {

		Double totalAdmits = 0.0;
		try {
			List<Double> listOfAdmitsByScreenType = new ArrayList<>();
			admitsForEachScreenType.forEach((key, value) -> {
				listOfAdmitsByScreenType.add(value);
			});

			totalAdmits = MathUtil.sum(listOfAdmitsByScreenType);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Total Admits");
		}
		return totalAdmits;

	}

	Map<String, Map<String, Double>> getAtpForEachScreenSeatTypeForSuggestedValue(Optional<ProjectDetails> projectDetails) {

		Map<String, Map<String, Double>> getAtpForEachScreenSeatType = new HashMap<>();
		try {
			List<AtpAssumptions> atpValue = projectDetails.get().getAtp();
			atpValue.forEach(atp -> {

				String format = atp.getCinemaFormatType();
				Double normal = (double) atp.getNormal().getCostType();
				Double recliner = (double) atp.getRecliner().getCostType();
				Double lounger = (double) atp.getLounger().getCostType();

				Map<String, Double> atpValueByScreenType = new HashMap<>();
				atpValueByScreenType.put(Constants.NORMAL, normal);
				atpValueByScreenType.put(Constants.RECLINER, recliner);
				atpValueByScreenType.put(Constants.LOUNGER, lounger);

				getAtpForEachScreenSeatType.put(atp.getCinemaFormatType(), atpValueByScreenType);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Atp For Each Screen Seat Type");
		}
		return getAtpForEachScreenSeatType;

	}
	
	
	Map<String, Map<String, Double>> getAtpForEachScreenSeatType(Optional<ProjectDetails> projectDetails) {

		Map<String, Map<String, Double>> getAtpForEachScreenSeatType = new HashMap<>();
		try {
			List<AtpAssumptions> atpValue = projectDetails.get().getAtp();
			atpValue.forEach(atp -> {

				String format = atp.getCinemaFormatType();
				Double normal =  atp.getNormal().getInputValues();
				Double recliner = atp.getRecliner().getInputValues();
				Double lounger =  atp.getLounger().getInputValues();

				Map<String, Double> atpValueByScreenType = new HashMap<>();
				atpValueByScreenType.put(Constants.NORMAL, normal);
				atpValueByScreenType.put(Constants.RECLINER, recliner);
				atpValueByScreenType.put(Constants.LOUNGER, lounger);

				getAtpForEachScreenSeatType.put(atp.getCinemaFormatType(), atpValueByScreenType);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Atp For Each Screen Seat Type");
		}
		return getAtpForEachScreenSeatType;

	}
	
	
	
	
	
	
	
	

	public Map<String, Double> getSphForEachScreeenType(Optional<ProjectDetails> projectDetails) {

		Map<String, Double> getSphForEachScreeenType = new HashMap<>();
		try {
			List<AssumptionsValues> sph = projectDetails.get().getSph();
			sph.forEach(sphForEachScreenType -> {
				getSphForEachScreeenType.put(sphForEachScreenType.getFormatType(),
						sphForEachScreenType.getInputValues());

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  Sph For Each Screeen Type");
		}
		return getSphForEachScreeenType;
	}
	
	public Map<String, Double> getSphForEachScreeenTypeForSuggestedValue(Optional<ProjectDetails> projectDetails) {

		Map<String, Double> getSphForEachScreeenType = new HashMap<>();
		try {
			List<AssumptionsValues> sph = projectDetails.get().getSph();
			sph.forEach(sphForEachScreenType -> {
				getSphForEachScreeenType.put(sphForEachScreenType.getFormatType(),
						(double) sphForEachScreenType.getCostType());

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  Sph For Each Screeen Type");
		}
		return getSphForEachScreeenType;
	}
	
	
	
	

	public Double getTotalSph(Map<String, Double> sphForEachScreeenType, Map<String, Double> admitsForEachScreenType,
			Double totalAdmits) {

		Double getTotalSph = 0.0;
		try {
			List<Double> listOfSphAtp = new ArrayList<>();
			sphForEachScreeenType.forEach((key, value) -> {

				Double sphAtp = MathUtil.roundTwoDecimals(value * admitsForEachScreenType.get(key));
				listOfSphAtp.add(sphAtp);
			});

			getTotalSph = MathUtil.roundTwoDecimals(MathUtil.sum(listOfSphAtp) / totalAdmits);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  Sph For Each Screeen Type");
		}
		return getTotalSph;
	}

	public Double getMunicipalTaxesPerAnnum(List<FixedConstant> fixedConstant,
			Optional<ProjectDetails> projectDetails) {

		Double municipalTaxesPerAnnum = 0.0;
		try {
			Double tax = projectDetails.get().getLesserLesse().getPropertyTax();
			Double leasableArea = projectDetails.get().getLesserLesse().getLeasableArea();

			municipalTaxesPerAnnum = MathUtil.roundTwoDecimals(tax * Constants.TWELVE * leasableArea);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  municipal taxes Per Annum");
		}
		return municipalTaxesPerAnnum;
	}

//	Map<String, Map<String, Double>> getAdmitsForEachScreenSeatType(List<CinemaFormat> cinemaFormat,
//			Map<String, Double> occupancyForEachScreenType, Double totalshow, List<FixedConstant> fixedConstant,
//			String city) {
//
//		Map<String, Map<String, Double>> admitsForEachScreenSeatType = new HashMap<>();
//
//		try {
//
//			Integer workingDays = fixedConstant.get(0).getWorkingDays();
//			Map<String, Double> avgShowPerDay = fixedConstant.get(0).getAvgShowPerDay();
//			Double avgshow = 0.0;
//			
//			if (avgShowPerDay.containsKey(city)) {
//				avgshow = avgShowPerDay.get(city);
//			} else {
//				avgshow = avgShowPerDay.get(Constants.AVG_FIXED_SHOW);
//			}
//			final Double noOfshow = avgshow;
//
//			cinemaFormat.forEach(cinema -> {
//
//				Map<String, Double> admitFormatType = new HashMap<>();
//				String seatType = cinema.getFormatType();
//
//				Double admitsForNoramSeatType = MathUtil.roundTwoDecimals(occupancyForEachScreenType.get(seatType) * cinema.getNormal() * noOfshow * workingDays);
//				Double admitsForReclinerSeatType = MathUtil.roundTwoDecimals(occupancyForEachScreenType.get(seatType) * cinema.getRecliners() * noOfshow * workingDays);
//				Double admitsForLoungerSeatType = MathUtil.roundTwoDecimals(occupancyForEachScreenType.get(seatType) * cinema.getLounger() * noOfshow * workingDays);
//
//				admitFormatType.put(Constants.NORMAL, admitsForNoramSeatType);
//				admitFormatType.put(Constants.RECLINER, admitsForReclinerSeatType);
//				admitFormatType.put(Constants.LOUNGER, admitsForLoungerSeatType);
//
//				admitsForEachScreenSeatType.put(seatType, admitFormatType);
//
//			});
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("error while calculating  Admits For Each Screen Seat Type");
//		}
//		return admitsForEachScreenSeatType;
//
//	}

}
