package com.api.services;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.common.mappers.BenchmarkCinema;
import com.common.models.AssumptionsValues;
import com.common.models.AtpAssumptions;
import com.common.models.CinemaMaster;
import com.common.models.Navision;
import com.common.models.ProjectDetails;
import com.common.models.RevenueExpense;
import com.common.models.SeatType;
import com.common.sql.models.CinemaMasterSql;

public interface ProjectRevenueAnalysisService {

	double getProjectOccupancyBenchMarkCinemas(String projectId,Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal);

	List<RevenueExpense> getProjectUpcomingCinemasOccupancy(String projectId,Optional<ProjectDetails> upComingCinemaOccupancy, List<Navision> lastQuarterNonCompDataFinal);

	Map<Integer, Double> mainstreamOccupancyQuaterlyPerHopk(List<Integer> hopk);

	Double getMainstreamBeanchmarkAtp(String projectId,Optional<ProjectDetails> projectDetails,List<Navision> lastQuarterNonCompDataFinal);

	List<AtpAssumptions> getUpcomingCinemaAtp(String projectId,Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal);

	List<AssumptionsValues> getUpcomingCinemaSph(String projectId,Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal);
	
	Map<Integer,Double> getMainstreamAtpNationalLevel(List<Integer> hopk);
	
	long getAdvertisingRevenue(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	
	Map<Integer,Double> getMainstreamOccupancyBenchmarkCinemas(List<Integer> hopk);
	
	double getProjectWebSaleDetails(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	
	long getProjectTotalAdmitsDetails(String projectId,Optional<ProjectDetails> projectDetails);
	
//	Set<String> getProjectTestAlgo(String projectId);

	Double getMainstreamBeanchmarkAtpReclinerOrLounger(String projectId,Optional<ProjectDetails> projectDetails,String areaCategory,List<Navision> lastQuarterNonCompDataFinal);
	
	List<CinemaMaster> getAllKeyRevenueAssumptionOccupancy(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getRevenueBenchmarkCinemas(ProjectDetails projectDetails);
}