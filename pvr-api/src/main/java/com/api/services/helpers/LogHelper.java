package com.api.services.helpers;

import lombok.Data;

@Data
public class LogHelper {

	private String id;
	private long requestTime;
	private String requestURI;
	private String requestIP;
	private String requestBrowser;
	private String requestStatus;
	private String exceptionInCase;
}
