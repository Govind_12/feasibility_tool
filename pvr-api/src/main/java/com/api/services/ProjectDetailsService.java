package com.api.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.api.dto.Filter;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.CinemaMaster;
import com.common.models.FinalSummary;
import com.common.models.FixedConstant;
import com.common.models.IrrCalculationData;
import com.common.models.Notification;
import com.common.models.ProjectDetails;
import com.common.models.ProjectLocation;

public interface ProjectDetailsService extends FeasibilityService<ProjectDetails> {

	ProjectDetails step1(ProjectDetails projectDetail);

	ProjectDetails step2(ProjectDetails projectDetail);

	ProjectDetails step3(ProjectDetails projectDetail);

	ProjectDetails step4(ProjectDetails projectDetail);

	ProjectDetails step5(ProjectDetails projectDetail);

	ProjectDetails step6(ProjectDetails projectDetail);

	ProjectDetails step7(ProjectDetails projectDetail);

	List<ProjectDetails> fetchAllProjectDetails(Pageable pageable, List<Filter> filters);

	List<ProjectDetails> fetchPreSigningProjectByStatus(Pageable pageable, List<Filter> filters);

	Notification createNotification(ProjectDetails projectDetails);

	ProjectDetails approveProject(String projectId);

	ProjectDetails rejectProject(String projectId);

	List<ProjectDetails> getProjectByStatus(Integer status);

	List<ProjectDetails> getProjectByInStatus(List<Integer> status);

	ProjectDetails getProjectDetailsById(String id);

	List<ProjectDetails> getProjectGreaterThanEqualStatus(Integer status);

	List<ProjectDetails> getProjectGreaterThanEqualStatusAndEqualFeasibilityState(Integer status,
			String feasibilityState);

	FinalSummary getQueryByProjectId(String projectId);

	QueryMapper saveQuery(MsgMapper msgMapper);

	/*
	 * Boolean deleteProject(String projectId);
	 * 
	 * Boolean archiveProject(String projectId);
	 */

	Boolean updateProjectAndNotificationStatus(String projectId, int status);

	ProjectDetails getLatestProject(String reportType, String reportId);

	List<ProjectDetails> fetchArchievedProjectDetails(Pageable pageable, List<Filter> filters);

	ProjectDetails updateProjectDetailsById(String projectId);

	List<ProjectDetails> fetchAcessOldReports(Pageable pageable, List<Filter> filters);

	ProjectDetails updateProjectOperationFlags(String projectId);

	ProjectDetails submitOperatingAssumption(ProjectDetails projectDetails);

	ProjectDetails rejectProjectDetails(MsgMapper msgMapper);

	ProjectDetails updateProject(String projectId);

	List<CinemaMaster> getAllCinemaMasterDetails();

	List<ProjectDetails> getExistingProject();

	ProjectDetails saveCloseStep5(ProjectDetails projectDetail);

	ProjectDetails getPreSigningProject(Integer integer);

	List<ProjectDetails> fetchAllProjectDetails();

	FixedConstant getRevenueAssumptionConstant(String projectId, String projectLocation);

	ProjectDetails saveExecutiveNote(ProjectDetails projectDetail);
	
	List<ProjectDetails> fetchAllExistingProjectDetailsForCopy(Pageable paramPageable, List<Filter> paramList);
	
	Boolean copyFromExistingReport(String paramString);
	
	List<ProjectDetails> fetchAllProjectDetails2(Pageable paramPageable, List<Filter> paramList, String paramString);
	
	List<ProjectDetails> fetchAllOpsOrExtProjectDetails(String paramString);

	IrrCalculationData getPAndLTabData(String projectId);

}
