package com.api.services;

import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.CinemaMasterDao;
import com.api.dao.CityTierDao;
import com.api.dao.CompitionMasterDao;
import com.api.dao.FixedConstantDao;
import com.api.dao.IrrCalculationDao;
import com.api.dao.MasterTemplateByDistanceDao;
import com.api.dao.NavisionDao;
import com.api.dao.NotificationDao;
import com.api.dao.ProjectCostSummaryDao;
import com.api.dao.ProjectDetailsDao;
import com.api.dao.UserDao;
import com.api.dao.sql.CinemaMasterSqlDao;
import com.api.dto.Filter;
import com.api.exception.FeasibilityException;
import com.api.utils.ChangeCinemaFormat;
import com.api.utils.MathUtil;
import com.api.utils.QueryBuilder;
import com.api.utils.RoleUtil;
import com.api.utils.StateAndCityUtil;
import com.common.constants.Constants;
import com.common.constants.MessageConstants;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.AssumptionsValues;
import com.common.models.AtpAssumptions;
import com.common.models.AtpCapDetials;
import com.common.models.CinemaFormat;
import com.common.models.CinemaMaster;
import com.common.models.CityTier;
import com.common.models.CompitionMaster;
import com.common.models.CostAssumptionsGrowthRate;
import com.common.models.FeasibilityUser;
import com.common.models.FieldValue;
import com.common.models.FinalSummary;
import com.common.models.FixedConstant;
import com.common.models.IrrCalculationData;
import com.common.models.LesserLesse;
import com.common.models.MasterTemplateByDistance;
import com.common.models.Navision;
import com.common.models.Notification;
import com.common.models.OtherParameter;
import com.common.models.ProjectCostSummary;
import com.common.models.ProjectDetails;
import com.common.models.ProjectLocation;
import com.common.models.PropertyDetail;
import com.common.models.RevenueAssumptionsGrowthRate;
import com.common.models.RevenueExpense;
import com.common.models.User;
import com.common.sql.models.CinemaMasterSql;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("projectDetailsService")
public class ProjectDetailsServiceImpl implements ProjectDetailsService {

	@Autowired
	private ProjectDetailsDao projectDetailsDao;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private ProjectAnalysisService projectAnalysisService;

	@Autowired
	private CityTierDao cityTierDao;

	@Autowired
	private CinemaMasterDao cinemaMasterDao;

	@Autowired
	private NotificationDao notificationDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private ProjectRevenueAnalysisService projectRevenueService;

	@Autowired
	private AssumptionsCalculation assumptionsCalculation;

	@Autowired
	private FixedConstantDao fixedConstantDao;

	@Autowired
	private MailService mailService;

	@Autowired
	private CompitionMasterDao compitionMasterDao;

	@Autowired
	private MasterTemplateByDistanceDao masterTemplatebyDistanceDao;

	@Autowired
	private CinemaMasterSqlDao cinemaMasterSqlDao;

	@Autowired
	private ProjectCostSummaryDao projectCostSummaryDao;

	@Autowired
	private IrrCalculationDao irrCalculationDao;
	
	@Autowired
	private NavisionDao navisionDao;
	
	@Autowired
	private CommonAnalysisService commonAnalysisService;

	@Value("${google.api.key}")
	private String googleKey;

	@Override
	public ProjectDetails step1(ProjectDetails entity) {

		if (ObjectUtils.isEmpty(entity)) {
			log.error("Error while saving Project Details.");
			throw new FeasibilityException("Error while saving Project Details.");
		}

		// city-tier

		String tier = "";
		CityTier cityTier = cityTierDao.findByCity(StateAndCityUtil.getCity(entity.getProjectLocation().getCity()));
		if (!ObjectUtils.isEmpty(cityTier)) {
			tier = cityTier.getTier();
		} else {
			tier = "";
		}

		ProjectLocation projectLocation = new ProjectLocation();
		projectLocation.setCityTier(tier);
		projectLocation.setCity(StateAndCityUtil.getCity(entity.getProjectLocation().getCity()));
		projectLocation.setLatitude(entity.getProjectLocation().getLatitude());
		projectLocation.setLongitude(entity.getProjectLocation().getLongitude());
		projectLocation.setLocality(entity.getProjectLocation().getLocality());
		projectLocation.setState(entity.getProjectLocation().getState());
		entity.setProjectLocation(projectLocation);

		entity.setCreatedDate(new Date().getTime());
		entity.setStatus(Constants.STEP_1);
		Set<Integer> checkStatus = new HashSet<>();
		checkStatus.add(Constants.STEP_1);
		entity.setCheckStatus(checkStatus);
		entity.createdBy(RoleUtil.getCurrentUserID());

		entity.setIsOperationSubmit(false);
		entity.setIsProjectSubmit(false);
		entity.setProjectBdId(RoleUtil.getCurrentUserID());
		
		//No Of Shows Values Logic:: 
		AssumptionsValues noOfShowsAssumptionValue = new AssumptionsValues();
		List<FixedConstant> fixConstant = (List<FixedConstant>) fixedConstantDao.findAll();
		log.info("fixed constant during no of shows: " + fixConstant.get(0));
		
		String currentProjectLocation = entity.getProjectLocation().getState();
		log.info("current project location : " + currentProjectLocation);
		
		double noOfShows = 0;
		if(fixConstant.get(0).getAvgShowPerDay().containsKey(currentProjectLocation)) {
			noOfShows = fixConstant.get(0).getAvgShowPerDay().get(currentProjectLocation);
		}else {
			noOfShows = fixConstant.get(0).getAvgShowPerDay().get("fixed");
		}
		
		noOfShowsAssumptionValue.setSuggestedValue(noOfShows);
		entity.setNoOfShows(noOfShowsAssumptionValue);

		ProjectDetails projectDetails = null;
		if (Constants.REPORT_TYPE_NEW.equals(entity.getReportType())) {

			if (ObjectUtils.isEmpty(entity.getId())) {
				entity.setId(null);
				synchronized (this) {
					// generate report id and report versions
					ProjectDetails latestProjectDetails = getLatestProject(entity.getReportType(), null);

					entity.setReportId(latestProjectDetails.getReportId());
					entity.setReportIdVersion(latestProjectDetails.getReportIdVersion());
					entity.setReportIdSubVersion(latestProjectDetails.getReportIdSubVersion());
					entity.setUpdatedDate(new Date().getTime());

					log.debug("Saved Project Details.");
					projectDetails = projectDetailsDao.save(entity);
					createNotification(projectDetails);

				}
			} else {
				ProjectDetails existingProjectDetails = this.get(entity.getId());
				existingProjectDetails.setProjectLocation(projectLocation);
				existingProjectDetails.setProjectName(entity.getProjectName());
				existingProjectDetails.setTotalDeveloperSize(entity.getTotalDeveloperSize());
				existingProjectDetails.setRetailAreaOfProject(entity.getRetailAreaOfProject());
				existingProjectDetails.setProjectType(entity.getProjectType());
				existingProjectDetails.setTenancyMix(entity.getTenancyMix());
				existingProjectDetails
						.setUpcomingInfrastructureDevlopment(entity.getUpcomingInfrastructureDevlopment());
				existingProjectDetails.setDateOfHandover(entity.getDateOfHandover());
				existingProjectDetails.setDateOfOpening(entity.getDateOfOpening());
				if (!ObjectUtils.isEmpty(entity.getFileURLStep1())) {
					existingProjectDetails.setFileURLStep1(entity.getFileURLStep1());
				}
				existingProjectDetails.setNoOfShows(noOfShowsAssumptionValue);
				existingProjectDetails.setUpdatedDate(new Date().getTime());
				existingProjectDetails.setLeaseModel(entity.isLeaseModel());
				existingProjectDetails.setSaleModel(entity.isSaleModel());

				projectDetails = projectDetailsDao.save(existingProjectDetails);
			}

		}
		if (Constants.REPORT_TYPE_EXISTING.equals(entity.getReportType())) {
			synchronized (this) {
				ProjectDetails existingProjectDetails = this.get(entity.getId());
				existingProjectDetails.setId(null);

				existingProjectDetails.setReportId(entity.getReportId());
				existingProjectDetails.setReportIdVersion(entity.getReportIdVersion());
				existingProjectDetails.setReportIdSubVersion(entity.getReportIdSubVersion());

				existingProjectDetails.setReportType(Constants.REPORT_TYPE_NEW);

				existingProjectDetails.setProjectLocation(projectLocation);
				existingProjectDetails.setProjectName(entity.getProjectName());
				existingProjectDetails.setTotalDeveloperSize(entity.getTotalDeveloperSize());
				existingProjectDetails.setRetailAreaOfProject(entity.getRetailAreaOfProject());
				existingProjectDetails.setProjectType(entity.getProjectType());
				existingProjectDetails.setTenancyMix(entity.getTenancyMix());
				existingProjectDetails
						.setUpcomingInfrastructureDevlopment(entity.getUpcomingInfrastructureDevlopment());
				existingProjectDetails.setDateOfHandover(entity.getDateOfHandover());
				existingProjectDetails.setDateOfOpening(entity.getDateOfOpening());
				if (!ObjectUtils.isEmpty(entity.getFileURLStep1())) {
					existingProjectDetails.setFileURLStep1(entity.getFileURLStep1());
				}

				existingProjectDetails.setCreatedDate(new Date().getTime());
				existingProjectDetails.setStatus(Constants.STEP_1);
				existingProjectDetails.createdBy(RoleUtil.getCurrentUserID());
				existingProjectDetails.setIsOperationSubmit(false);
				existingProjectDetails.setIsProjectSubmit(false);
				existingProjectDetails.setProjectBdId(RoleUtil.getCurrentUserID());
				existingProjectDetails.setDateOfSendForApproval(null);
				existingProjectDetails.setRejectedBy(null);
				existingProjectDetails.setApproverId(null);
				existingProjectDetails.setRejectReasonMsg(null);
				existingProjectDetails.setMsgMapper(null);
				this.emptyQueryDataForBDApprover(existingProjectDetails);
				existingProjectDetails.setNoOfShows(noOfShowsAssumptionValue);
				existingProjectDetails.setUpdatedDate(new Date().getTime());
				
				existingProjectDetails.setLeaseModel(entity.isLeaseModel());
				existingProjectDetails.setSaleModel(entity.isSaleModel());

				log.debug("Saved Project Details.");
				projectDetails = projectDetailsDao.save(existingProjectDetails);

				ProjectCostSummary projectCostSummary = projectCostSummaryDao.findByParentId(entity.getId());
				if (!ObjectUtils.isEmpty(projectCostSummary)) {
					projectCostSummary.setId(null);
					projectCostSummary.setParentId(projectDetails.getId());
					projectCostSummaryDao.save(projectCostSummary);
				}

				IrrCalculationData irrCalculationData = irrCalculationDao.findByProjectId(entity.getId());
				if (!ObjectUtils.isEmpty(irrCalculationData)) {
					irrCalculationData.setId(null);
					irrCalculationData.setProjectId(projectDetails.getId());
					irrCalculationDao.save(irrCalculationData);
				}
				createNotification(projectDetails);
			}

		}
		return projectDetails;

	}

	@Override
	public ProjectDetails step2(ProjectDetails entity) {

		if (ObjectUtils.isEmpty(entity) || ObjectUtils.isEmpty(entity.getId())) {
			log.error("Empty survey can not assined.");
			throw new FeasibilityException("Empty Project Details can not assined.");
		}
		try {

			ProjectDetails projectDetails = this.get(entity.getId());

			projectDetails.setDeveloper(entity.getDeveloper());
			projectDetails.setDeveloperOwner(entity.getDeveloperOwner());
			projectDetails.setGrossTurnOver(entity.getGrossTurnOver());
			projectDetails.setProjectDelivered1(entity.getProjectDelivered1());
			projectDetails.setProjectDelivered2(entity.getProjectDelivered2());
			projectDetails.setProjectDelivered3(entity.getProjectDelivered3());
			projectDetails.setRetailProjectDelivered(entity.getRetailProjectDelivered());
			projectDetails.setTotalDevlopedAreDelivered(entity.getTotalDevlopedAreDelivered());
			if (entity.getPvrProjectDelivered().contains(null)) {
				entity.getPvrProjectDelivered().removeIf(a -> a == null);
			}
			if (!ObjectUtils.isEmpty(entity.getPvrProjectDelivered())) {
				projectDetails.setPvrProjectDelivered(entity.getPvrProjectDelivered());
			} else {
				projectDetails.setPvrProjectDelivered(null);
			}
			
			if (entity.getPvrProjectPipeline().contains("")) {
				entity.getPvrProjectPipeline().removeIf(a -> a.trim() == "");
			}
			
			projectDetails.setPvrProjectPipeline(entity.getPvrProjectPipeline());
			projectDetails.setCurrentProjectFinancing(entity.getCurrentProjectFinancing());

			if (!ObjectUtils.isEmpty(entity.getFileURLStep2())) {
				projectDetails.setFileURLStep2(entity.getFileURLStep2());
			}

			projectDetails.setStatus(Constants.STEP_2);
			Set<Integer> checkStatus = projectDetails.getCheckStatus();
			if (!ObjectUtils.isEmpty(checkStatus)) {
				checkStatus.add(Constants.STEP_2);
				entity.setCheckStatus(checkStatus);
			}
			projectDetails.createdBy(RoleUtil.getCurrentUserID());
			// projectDetails.setReportId(entity.getReportId());
			projectDetails.setUpdatedDate(new Date().getTime());
			projectDetails.setDeveloperWebsite(entity.getDeveloperWebsite());
			projectDetails.setArchitectNameOfMall(entity.getArchitectNameOfMall());

			ProjectDetails savedProjectDetails = projectDetailsDao.save(projectDetails);

			savedProjectDetails.setReportId(projectDetails.getReportId());
			createNotification(savedProjectDetails);

			return savedProjectDetails;

		} catch (Exception e) {
			log.debug("Exception in saving step 2.");
			e.printStackTrace();
			throw new FeasibilityException("Exception in saving step 2");
		}

	}

	@Override
	public ProjectDetails step3(ProjectDetails entity) {

		if (ObjectUtils.isEmpty(entity) || ObjectUtils.isEmpty(entity.getId())) {
			log.error("Empty survey can not assined.");
			throw new FeasibilityException("Empty Project Details can not assined.");
		}
		try {

			ProjectDetails projectDetails = this.get(entity.getId());

			List<PropertyDetail> newPvrproperties = new ArrayList<>();

			List<PropertyDetail> pvrProperties = entity.getPvrProperties();

			if (!ObjectUtils.isEmpty(pvrProperties)) {
				pvrProperties.forEach(x -> {
					if (!ObjectUtils.isEmpty(x)) {
						log.info(x.toString());
						PropertyDetail propertyDetails = new PropertyDetail();
						/*
						 * if (x.getAddress() != null && x.getAddress().contains(","))
						 * propertyDetails.setAddress(x.getAddress().substring(0,
						 * x.getAddress().indexOf(","))); else
						 */
						propertyDetails.setAddress(x.getAddress());
						propertyDetails.setNoOfScreen(x.getNoOfScreen());
						propertyDetails.setPropertyName(x.getPropertyName());
						propertyDetails.setSeatCapacity(x.getSeatCapacity());
						propertyDetails.setTenatativeOpeningDate(x.getTenatativeOpeningDate());
						// propertyDetails.setProjectLocation(x.getProjectLocation());
						newPvrproperties.add(propertyDetails);
					}
				});
			}

			List<PropertyDetail> newCompetitiveProperties = new ArrayList<>();

			List<PropertyDetail> competitiveProperties = entity.getCompetitveProperties();

			if (!ObjectUtils.isEmpty(competitiveProperties)) {
				competitiveProperties.forEach(x -> {
					PropertyDetail propertyDetails = new PropertyDetail();
					/*
					 * if (x.getAddress() != null && x.getAddress().contains(","))
					 * propertyDetails.setAddress(x.getAddress().substring(0,
					 * x.getAddress().indexOf(","))); else
					 */
					propertyDetails.setAddress(x.getAddress());
					propertyDetails.setNoOfScreen(x.getNoOfScreen());
					propertyDetails.setPropertyName(x.getPropertyName());
					propertyDetails.setSeatCapacity(x.getSeatCapacity());
					propertyDetails.setTenatativeOpeningDate(x.getTenatativeOpeningDate());

					newCompetitiveProperties.add(propertyDetails);

				});
			}
			// Gson gson = new Gson();
			// System.out.println(gson.toJson(newPvrproperties));

			projectDetails.setCompetitveProperties(newCompetitiveProperties);
			projectDetails.setPvrProperties(newPvrproperties);
			projectDetails.setStatus(Constants.STEP_3);
			Set<Integer> checkStatus = projectDetails.getCheckStatus();
			checkStatus.add(Constants.STEP_3);
			entity.setCheckStatus(checkStatus);
			projectDetails.createdBy(RoleUtil.getCurrentUserID());
			projectDetails.setUpdatedDate(new Date().getTime());

			ProjectDetails savedProjectDetails = projectDetailsDao.save(projectDetails);

			savedProjectDetails.setReportId(projectDetails.getReportId());
			createNotification(savedProjectDetails);

			return savedProjectDetails;

			/*
			 * projectDetails.setIncomeApplicable(entity.getIncomeApplicable());
			 * projectDetails.setServiceCharge(entity.getServiceCharge());
			 * projectDetails.setLbtCharge(entity.getLbtCharge());
			 */

		} catch (Exception e) {
			log.debug("Exception in saving step 4.");
			e.printStackTrace();
			throw new FeasibilityException("Exception in saving step 4");
		}

		// return null;

	}

	@Override
	public ProjectDetails step4(ProjectDetails entity) {

		if (ObjectUtils.isEmpty(entity) || ObjectUtils.isEmpty(entity.getId())) {
			log.error("Empty survey can not assined.");
			throw new FeasibilityException("Empty Project Details can not assined.");
		}
		try {

			ProjectDetails projectDetails = this.get(entity.getId());

			projectDetails.setLesserLesse(entity.getLesserLesse());
			projectDetails.setStatus(Constants.STEP_4);
			Set<Integer> checkStatus = projectDetails.getCheckStatus();
			checkStatus.add(Constants.STEP_4);
			entity.setCheckStatus(checkStatus);
			projectDetails.createdBy(RoleUtil.getCurrentUserID());
//			projectDetails.setRentTerm(entity.getRentTerm());
			projectDetails.setCamTerm(entity.getCamTerm());
			
//			int lessePeriod = !ObjectUtils.isEmpty(entity.getLesserLesse().getLeasePeriod()) ? 
//					              entity.getLesserLesse().getLeasePeriod() : 0;
					              
            if(!ObjectUtils.isEmpty(entity.getLesserLesse().getYearWiseAdvanceRentAdjustment())) {
  				HashMap<Integer,Double> yearWiseAdvanceRent = entity.getLesserLesse().getYearWiseAdvanceRentAdjustment();
  				yearWiseAdvanceRent.forEach((key, value)->{
  					yearWiseAdvanceRent.put(key, (ObjectUtils.isEmpty(value) ? 0 : value));
  				});
  				entity.getLesserLesse().setYearWiseAdvanceRentAdjustment(yearWiseAdvanceRent);
  			}
					              
//			HashMap<Integer, Double> adjustmentDetail = new HashMap<>();
//			if(!ObjectUtils.isEmpty(entity.getRentTerm().getYearWiseRent())) {
//			for(int i=1;i<=lessePeriod;i++) {
//					double rent = entity.getRentTerm().getYearWiseRent().get(i);
//					double adjustmentAmount = 0;
//					if(!ObjectUtils.isEmpty(entity.getLesserLesse().getYearWiseAdvanceRentAdjustment()) && 
//							i <= entity.getLesserLesse().getYearWiseAdvanceRentAdjustment().size()) {
//						adjustmentAmount = entity.getLesserLesse().getYearWiseAdvanceRentAdjustment().get(i) != null ? 
//								entity.getLesserLesse().getYearWiseAdvanceRentAdjustment().get(i) : 0;
//					}else {
//						adjustmentAmount = 0;
//					}
//					
//					double rentAfterAdjustment = rent - adjustmentAmount;
//					adjustmentDetail.put(i, rentAfterAdjustment);					
//				}
//			}
//			entity.getRentTerm().setYearWiseRentAfterAdjustment(adjustmentDetail);
			projectDetails.setRentTerm(entity.getRentTerm());
			projectDetails.setUpdatedDate(new Date().getTime());

			ProjectDetails savedProjectDetails = projectDetailsDao.save(projectDetails);

			savedProjectDetails.setReportId(projectDetails.getReportId());
			createNotification(savedProjectDetails);

			return savedProjectDetails;

			/*
			 * projectDetails.setIncomeApplicable(entity.getIncomeApplicable());
			 * projectDetails.setServiceCharge(entity.getServiceCharge());
			 * projectDetails.setLbtCharge(entity.getLbtCharge());
			 */

		} catch (Exception e) {
			log.debug("Exception in saving step 4.");
			e.printStackTrace();
			throw new FeasibilityException("Exception in saving step 4");
		}

		// return null;

	}

	@Override
	public ProjectDetails step5(ProjectDetails entity) {

		if (ObjectUtils.isEmpty(entity) || ObjectUtils.isEmpty(entity.getId())) {
			log.error("Empty survey can not assined.");
			throw new FeasibilityException("Empty Project Details can not assined.");
		}
		try {

			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(entity.getId());
			List<CinemaFormat> cinemaformats = new ArrayList<>();

			// projectDetails.get().setProjectInfraStructure(entity.getProjectInfraStructure());
			List<CinemaFormat> newCinemaFormat = entity.getCinemaFormat();
			List<CinemaFormat> screenDimensions = new ArrayList<>();

			if (!ObjectUtils.isEmpty(newCinemaFormat)) {
				newCinemaFormat.forEach(x -> {
					CinemaFormat cinemaFormat = new CinemaFormat();
					CinemaFormat screenDimension = new CinemaFormat();

					screenDimension.setFormatType(x.getFormatType());

					screenDimensions.add(screenDimension);
					cinemaFormat.setFormatType(x.getFormatType());
					cinemaFormat.setNormal(x.getNormal());
					cinemaFormat.setRecliners(x.getRecliners());
					cinemaFormat.setLounger(x.getLounger());
					cinemaFormat.setTotal(x.getTotal());
					cinemaformats.add(cinemaFormat);

				});
			}

			/* List<CinemaFormat> screenDimensions = new ArrayList<>(); */

			List<CinemaFormat> newscreenDimension = entity.getSeatDimension();
			for(int i =0;i<newscreenDimension.size();i++) {
				newscreenDimension.get(i).setFormatType(cinemaformats.get(i).getFormatType());
			}

			/*
			 * cinemaformats.forEach(cinemaFormat -> { newscreenDimension.forEach(seatMatrix
			 * -> { screenDimensions.forEach(screen -> {
			 * screen.setNormalSeatDimension(seatMatrix.getNormalSeatDimension());
			 * screen.setReclinerSeatDimension(seatMatrix.getReclinerSeatDimension());
			 * screen.setLoungerSeatDimension(seatMatrix.getLoungerSeatDimension()); }); });
			 * });
			 */
			
			/* To be reviewed later
			 * for (int i = 0; i < cinemaformats.size(); i++) { CinemaFormat format =
			 * cinemaformats.get(i); CinemaFormat screenDim = newscreenDimension.get(i);
			 * screenDim.setFormatType(format.getFormatType());
			 * 
			 * }
			 */
			
			projectDetails.get().setSeatDimension(newscreenDimension);

			projectDetails.get().setNormalsValSum(entity.getNormalsValSum());
			projectDetails.get().setReclinerValSum(entity.getReclinerValSum());
			projectDetails.get().setLoungersValSum(entity.getLoungersValSum());
			projectDetails.get().setTotalColumnValSum(entity.getTotalColumnValSum());
			projectDetails.get().setNoOfScreens(entity.getNoOfScreens());
			projectDetails.get().setCinemaCategory(entity.getCinemaCategory());
			// projectDetails.get().setSeatDimension(screenDimensions);

//			if (!ObjectUtils.isEmpty(entity.getFileURLStep5())) {
//				projectDetails.get().setFileURLStep5(entity.getFileURLStep5());
//			}

			if (!ObjectUtils.isEmpty(entity.getMultipleFileURLStep5()))
				projectDetails.get().setMultipleFileURLStep5(entity.getMultipleFileURLStep5()); 
			      
			projectDetails.get().setCinemaFormat(cinemaformats);
			projectDetails.get().setStatus(Constants.STEP_5);
			Set<Integer> checkStatus = projectDetails.get().getCheckStatus();
			if (!ObjectUtils.isEmpty(checkStatus)) {
				checkStatus.add(Constants.STEP_5);
				entity.setCheckStatus(checkStatus);

			}
			projectDetails.get().createdBy(RoleUtil.getCurrentUserID());

			long start = System.currentTimeMillis();
		
			//Fetching all non comp of last quarter :::::
			final List<Navision> lastQuarterNonCompDataFinal = commonAnalysisService.getAllNavisionByComp();
			
			List<String> costType = MathUtil.getAllCostTypeAlgo();
			costType.forEach(cost -> {
				if (cost.equals(Constants.COMMUNICATION)) {
					log.info("communication algo started...");
					AssumptionsValues as = new AssumptionsValues();

					as.setCostType(projectAnalysisService.getProjectCommunicationExpense(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setCommunicationExpense(as);

				} else if (cost.equals(Constants.WATERANDELECTRICITY)) {
					log.info("water and electricity algo started...");
					AssumptionsValues as = new AssumptionsValues();

					as.setCostType(projectAnalysisService.getWaterAndElectricity(null, projectDetails, lastQuarterNonCompDataFinal));

					projectDetails.get().setElectricityWaterBill(as);

				}else if (cost.equals(Constants.LEGAL_EXPENSE)) {

					log.info("legal expense algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getProjectLegalExpense(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setLegalFee(as);
				} else if (cost.equals(Constants.PERSONNAL_COST)) {
					log.info("personnal cost  algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getProjectPersonnalExpanses(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setPersonalExpenses(as);
				} else if (cost.equals(Constants.TRAVELLING_CONVEYANCE)) {
					log.info("travelling and conyeance algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getProjectTravellingConveyance(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setTravellingAndConveyanceExpense(as);
				} else if (cost.equals(Constants.MISCELLANEOUS)) {
					log.info("miscellaneous algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getProjectMiscellaneousExpense(null, projectDetails,lastQuarterNonCompDataFinal));
					projectDetails.get().setMiscellaneousExpenses(as);
				} else if (cost.equals(Constants.MARKETING_COST)) {
					log.info("marketing algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getProjectMarketingExpense(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setMarketingExpense(as);
				} else if (cost.equals(Constants.SECURITY)) {
					log.info("security algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getProjectSecurityExpense(null, projectDetails,lastQuarterNonCompDataFinal));
					projectDetails.get().setSecurityExpense(as);
				} else if (cost.equals(Constants.INTERNET_CHARGES)) {
					log.info("internet charges algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getProjectInternetCharges(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setInternetExpense(as);
				} else if (cost.equals(Constants.INSURANCE_EXPENSE)) {
					log.info("insurance charges algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getProjectInsuranceExpense(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setInsuranceExpense(as);
				} else if (cost.equals(Constants.PRINTINGANDSTATIONARY)) {
					log.info("printing and stationary  algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getPrintingAndStationary(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setPrintingAndStationaryExpense(as);
				} else if (cost.equals(Constants.RANDM_COST)) {
					log.info("R&m algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getProjectRepairAndMaintainceExpense(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setTotalRMExpenses(as);
				} else if (cost.equals(Constants.HOUSE_KEEPING)) {
					log.info("Housekeeping expense algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectAnalysisService.getProjectHousekeepingExpense(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setHouseKeepingExpense(as);
				} else if (cost.equals(Constants.AD_REVENUE)) {
					log.info("ad revenue algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(projectRevenueService.getAdvertisingRevenue(null, projectDetails, lastQuarterNonCompDataFinal));
					projectDetails.get().setAdRevenue(as);

				} else if (cost.equals(Constants.WEB_SALE)) {
					log.info("web sale algo started...");
					AssumptionsValues as = new AssumptionsValues();
					as.setCostType(Math.round(projectRevenueService.getProjectWebSaleDetails(null, projectDetails, lastQuarterNonCompDataFinal)));
					projectDetails.get().setWebSale(as);
				}
				if (cost.equals(Constants.OCCUPANCY)) {
					log.info("occupancy algo started...");
					projectDetails.get().setOccupancy(getOccupancyForUpcomingCinemas(projectDetails, lastQuarterNonCompDataFinal));

				} else if (cost.equals(Constants.ATP)) {
					log.info("atp algo started...");

					projectDetails.get().setAtp(projectRevenueService.getUpcomingCinemaAtp(null, projectDetails,lastQuarterNonCompDataFinal));

				}

			});

			projectDetails.get().setSph(projectRevenueService.getUpcomingCinemaSph(null, projectDetails,lastQuarterNonCompDataFinal));
			
			//Cogs % Values Logic:: 
			if(ObjectUtils.isEmpty(projectDetails.get().getCogs())){
				AssumptionsValues cogsAssumptionValue = new AssumptionsValues();
				cogsAssumptionValue.setSuggestedValue(28.0);
				projectDetails.get().setCogs(cogsAssumptionValue);
			}

			long seconds = (int) ((start - System.currentTimeMillis() / 1000) % 60);
			System.out.println("totalSeconds " + seconds);

			if (projectDetails.get().getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
				projectDetails.get().setMiscIncome(new OtherParameter());
				projectDetails.get().setShowTax(new OtherParameter());
				projectDetails.get().setIsSendForOpsAndProject(true);
				projectDetails.get().setIsOperationSubmit(false);
				projectDetails.get().setIsProjectSubmit(false);
					
			}
			projectDetails.get().setUpdatedDate(new Date().getTime());

			ProjectDetails savedProjectDetails = projectDetailsDao.save(projectDetails.get());

			createNotification(savedProjectDetails);

			return savedProjectDetails;
		} catch (Exception e) {
			e.printStackTrace();
			log.debug("Exception in saving step 5.");
			throw new FeasibilityException("Exception in saving step 5");
		}

	}

	@Override
	public ProjectDetails step6(ProjectDetails entity) {

		if (ObjectUtils.isEmpty(entity) || ObjectUtils.isEmpty(entity.getId())) {
			log.error("Empty survey can not assined.");
			throw new FeasibilityException("Empty Project Details can not assined.");
		}
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(entity.getId());
			if (ObjectUtils.isEmpty(entity)) {
				log.error("Project Details not found.");
				throw new FeasibilityException("Project Details not found.");
			}

			if (RoleUtil.isCurrentUserHasBDRole() || RoleUtil.isCurrentUserHasOpeartionRole()) {
				projectDetails.get().setIncome3dApplicable(entity.isIncome3dApplicable());
			}

			List<AssumptionsValues> newOccupancies = new ArrayList<>();
			List<AssumptionsValues> admits = null;
			if (!ObjectUtils.isEmpty(entity.getOccupancy())) {
				admits = ChangeCinemaFormat.reformatOccupancy(entity.getOccupancy());
			}

			if (!ObjectUtils.isEmpty(admits)) {
				admits.forEach(x -> {
					AssumptionsValues assuptionValuesOccupancy = new AssumptionsValues();
					assuptionValuesOccupancy.setComments(x.getComments());
					assuptionValuesOccupancy.setFormatType(x.getFormatType());
					assuptionValuesOccupancy.setInputValues(x.getInputValues());
					assuptionValuesOccupancy.setCostType(x.getCostType());
					assuptionValuesOccupancy.setAdmits(x.getAdmits());
					newOccupancies.add(assuptionValuesOccupancy);

				});
			}

			List<AtpAssumptions> newATP = new ArrayList<>();
			List<AtpAssumptions> ATP = null;
			if (!ObjectUtils.isEmpty(entity.getAtp())) {
				ATP = ChangeCinemaFormat.reformatAtp(entity.getAtp());
			}
			if (!ObjectUtils.isEmpty(ATP)) {
				ATP.forEach(x -> {
					AtpAssumptions assuptionValuesATP = new AtpAssumptions();
					assuptionValuesATP.setCinemaFormatType(x.getCinemaFormatType());
					assuptionValuesATP.setNormal(x.getNormal());
					assuptionValuesATP.setRecliner(x.getRecliner());
					assuptionValuesATP.setLounger(x.getLounger());
					newATP.add(assuptionValuesATP);

				});
			}

			List<AssumptionsValues> newSPH = new ArrayList<>();
			List<AssumptionsValues> SPH = null;
			if (!ObjectUtils.isEmpty(entity.getSph())) {
				SPH = ChangeCinemaFormat.reformatSph(entity.getSph());
			}
			if (!ObjectUtils.isEmpty(SPH)) {
				SPH.forEach(x -> {
					AssumptionsValues assuptionValuesSPH = new AssumptionsValues();
					assuptionValuesSPH.setComments(x.getComments());
					assuptionValuesSPH.setFormatType(x.getFormatType());
					assuptionValuesSPH.setInputValues(x.getInputValues());
					assuptionValuesSPH.setCostType(x.getCostType());
					newSPH.add(assuptionValuesSPH);

				});
			}

			if (RoleUtil.isCurrentUserHasProjectTeamRole()) {
				projectDetails.get().setProjectCost(entity.getProjectCost());
				projectDetails.get().setDepreciation(entity.getDepreciation());
				projectDetails.get().setProjectCostPerScreen(entity.getProjectCostPerScreen());
				// projectDetails.get().setOperatingProjectquery(entity.getOperatingProjectquery());
			} else {

				projectDetails.get().setOccupancy(newOccupancies);
				projectDetails.get().setAtp(newATP);
				projectDetails.get().setSph(newSPH);

				if (Constants.PRE_HANDOVER.equalsIgnoreCase((projectDetails.get().getFeasibilityState()))) {
					entity.getWebSale().setQuery(projectDetails.get().getWebSale().getQuery());
					entity.getWebSale().setLastQueryUserId(projectDetails.get().getWebSale().getLastQueryUserId());
					entity.getMiscIncome().setQuery(projectDetails.get().getMiscIncome().getQuery());
					entity.getMiscIncome()
							.setLastQueryUserId(projectDetails.get().getMiscIncome().getLastQueryUserId());
					entity.getPersonalExpenses().setQuery(projectDetails.get().getPersonalExpenses().getQuery());
					entity.getPersonalExpenses()
							.setLastQueryUserId(projectDetails.get().getPersonalExpenses().getLastQueryUserId());
					entity.getElectricityWaterBill()
							.setQuery(projectDetails.get().getElectricityWaterBill().getQuery());
					entity.getElectricityWaterBill()
							.setLastQueryUserId(projectDetails.get().getElectricityWaterBill().getLastQueryUserId());
					entity.getTotalRMExpenses().setQuery(projectDetails.get().getTotalRMExpenses().getQuery());
					entity.getTotalRMExpenses()
							.setLastQueryUserId(projectDetails.get().getTotalRMExpenses().getLastQueryUserId());
					entity.getHouseKeepingExpense().setQuery(projectDetails.get().getHouseKeepingExpense().getQuery());
					entity.getHouseKeepingExpense()
							.setLastQueryUserId(projectDetails.get().getHouseKeepingExpense().getLastQueryUserId());
					entity.getSecurityExpense().setQuery(projectDetails.get().getSecurityExpense().getQuery());
					entity.getSecurityExpense()
							.setLastQueryUserId(projectDetails.get().getSecurityExpense().getLastQueryUserId());
					entity.getCommunicationExpense()
							.setQuery(projectDetails.get().getCommunicationExpense().getQuery());
					entity.getCommunicationExpense()
							.setLastQueryUserId(projectDetails.get().getCommunicationExpense().getLastQueryUserId());
					entity.getTravellingAndConveyanceExpense()
							.setQuery(projectDetails.get().getTravellingAndConveyanceExpense().getQuery());
					entity.getTravellingAndConveyanceExpense().setLastQueryUserId(
							projectDetails.get().getTravellingAndConveyanceExpense().getLastQueryUserId());
					entity.getLegalFee().setQuery(projectDetails.get().getLegalFee().getQuery());
					entity.getLegalFee().setLastQueryUserId(projectDetails.get().getLegalFee().getLastQueryUserId());
					entity.getInsuranceExpense().setQuery(projectDetails.get().getInsuranceExpense().getQuery());
					entity.getInsuranceExpense()
							.setLastQueryUserId(projectDetails.get().getInsuranceExpense().getLastQueryUserId());
					entity.getInternetExpense().setQuery(projectDetails.get().getInternetExpense().getQuery());
					entity.getInternetExpense()
							.setLastQueryUserId(projectDetails.get().getInternetExpense().getLastQueryUserId());
					entity.getPrintingAndStationaryExpense()
							.setQuery(projectDetails.get().getPrintingAndStationaryExpense().getQuery());
					entity.getPrintingAndStationaryExpense().setLastQueryUserId(
							projectDetails.get().getPrintingAndStationaryExpense().getLastQueryUserId());
					entity.getMarketingExpense().setQuery(projectDetails.get().getMarketingExpense().getQuery());
					entity.getMarketingExpense()
							.setLastQueryUserId(projectDetails.get().getMarketingExpense().getLastQueryUserId());
					entity.getMiscellaneousExpenses()
							.setQuery(projectDetails.get().getMiscellaneousExpenses().getQuery());
					entity.getMiscellaneousExpenses()
							.setLastQueryUserId(projectDetails.get().getMiscellaneousExpenses().getLastQueryUserId());
					entity.getShowTax().setQuery(projectDetails.get().getShowTax().getQuery());
					entity.getShowTax().setLastQueryUserId(projectDetails.get().getShowTax().getLastQueryUserId());
					entity.getAdRevenue().setQuery(projectDetails.get().getAdRevenue().getQuery());
					entity.getAdRevenue().setLastQueryUserId(projectDetails.get().getAdRevenue().getLastQueryUserId());

				}

				projectDetails.get().setWebSale(entity.getWebSale());

				projectDetails.get().setMiscIncome(entity.getMiscIncome());

				projectDetails.get().setPersonalExpenses(entity.getPersonalExpenses());

				projectDetails.get().setElectricityWaterBill(entity.getElectricityWaterBill());

				projectDetails.get().setTotalRMExpenses(entity.getTotalRMExpenses());

				projectDetails.get().setHouseKeepingExpense(entity.getHouseKeepingExpense());

				projectDetails.get().setSecurityExpense(entity.getSecurityExpense());

				projectDetails.get().setCommunicationExpense(entity.getCommunicationExpense());

				projectDetails.get().setTravellingAndConveyanceExpense(entity.getTravellingAndConveyanceExpense());

				projectDetails.get().setLegalFee(entity.getLegalFee());

				projectDetails.get().setInsuranceExpense(entity.getInsuranceExpense());

				projectDetails.get().setInternetExpense(entity.getInternetExpense());

				projectDetails.get().setPrintingAndStationaryExpense(entity.getPrintingAndStationaryExpense());

				projectDetails.get().setMarketingExpense(entity.getMarketingExpense());

				projectDetails.get().setMiscellaneousExpenses(entity.getMiscellaneousExpenses());

				projectDetails.get().setShowTax(entity.getShowTax());

				// entity.getTotalOverhead().setQuery(projectDetails.get().getTotalOverhead().getQuery());
				// entity.getTotalOverhead().setLastQueryUserId(projectDetails.get().getTotalOverhead().getLastQueryUserId());
				projectDetails.get().setTotalOverhead(entity.getTotalOverhead());

				projectDetails.get().setAdRevenue(entity.getAdRevenue());

				Double totalOverheadSuggestedSum = (double) ((entity.getHouseKeepingExpense().getCostType() != null ? entity.getHouseKeepingExpense().getCostType() : 0)
						+ (entity.getSecurityExpense().getCostType() != null ? entity.getSecurityExpense().getCostType() : 0) 
						+ (entity.getCommunicationExpense().getCostType() != null ? entity.getCommunicationExpense().getCostType() : 0)		
						+ (entity.getTravellingAndConveyanceExpense().getCostType() != null ? entity.getTravellingAndConveyanceExpense().getCostType() : 0)
						+ (entity.getLegalFee().getCostType() != null ? entity.getLegalFee().getCostType() : 0)
						+ (entity.getInsuranceExpense().getCostType() != null ? entity.getInsuranceExpense().getCostType() : 0)
						+ (entity.getInternetExpense().getCostType() != null ? entity.getInternetExpense().getCostType() : 0)
						+ (entity.getPrintingAndStationaryExpense().getCostType() != null ? entity.getPrintingAndStationaryExpense().getCostType() : 0)
						+ (entity.getMarketingExpense().getCostType() != null ? entity.getMarketingExpense().getCostType() : 0)
						+ (entity.getMiscellaneousExpenses().getCostType() != null ? entity.getMiscellaneousExpenses().getCostType() : 0)
						+ (entity.getPersonalExpenses().getCostType() != null ? entity.getPersonalExpenses().getCostType() : 0)
						+ (entity.getElectricityWaterBill().getCostType() != null ? entity.getElectricityWaterBill().getCostType() : 0)
						+ (entity.getTotalRMExpenses().getCostType() != null ? entity.getTotalRMExpenses().getCostType() : 0));
				
				projectDetails.get().setTotalOverheadSuggestedSum(totalOverheadSuggestedSum);

				Double totalOverheadInputSum = (entity.getHouseKeepingExpense().getInputValues() != null
						? entity.getHouseKeepingExpense().getInputValues()
						: 0)
						+ (entity.getSecurityExpense().getInputValues() != null
								? entity.getSecurityExpense().getInputValues()
								: 0)
						+ (entity.getCommunicationExpense().getInputValues() != null
								? entity.getCommunicationExpense().getInputValues()
								: 0)
						+ (entity.getTravellingAndConveyanceExpense().getInputValues() != null
								? entity.getTravellingAndConveyanceExpense().getInputValues()
								: 0)
						+ (entity.getLegalFee().getInputValues() != null ? entity.getLegalFee().getInputValues() : 0)
						+ (entity.getInsuranceExpense().getInputValues() != null
								? entity.getInsuranceExpense().getInputValues()
								: 0)
						+ (entity.getInternetExpense().getInputValues() != null
								? entity.getInternetExpense().getInputValues()
								: 0)
						+ (entity.getPrintingAndStationaryExpense().getInputValues() != null
								? entity.getPrintingAndStationaryExpense().getInputValues()
								: 0)
						+ (entity.getMarketingExpense().getInputValues() != null
								? entity.getMarketingExpense().getInputValues()
								: 0)
						+ (entity.getMiscellaneousExpenses().getInputValues() != null
								? entity.getMiscellaneousExpenses().getInputValues()
								: 0)
						+ (entity.getPersonalExpenses().getInputValues() != null
						        ? entity.getPersonalExpenses().getInputValues()
						        : 0)
						+ (entity.getElectricityWaterBill().getInputValues() != null
						        ? entity.getElectricityWaterBill().getInputValues()
						        : 0)
						+ (entity.getTotalRMExpenses().getInputValues() != null
						        ? entity.getTotalRMExpenses().getInputValues()
						        : 0);

				projectDetails.get().setTotalOverheadInputSum(totalOverheadInputSum);
				if (RoleUtil.isCurrentUserHasBDRole()) {
					projectDetails.get().setProjectCost(entity.getProjectCost());
					projectDetails.get().setDepreciation(entity.getDepreciation());
					projectDetails.get().setProjectCostPerScreen(entity.getProjectCostPerScreen());
				}
				// projectDetails.get().setFileURLStep1(entity.getFileURLStep1());

				// projectDetails.setSecurityDepositAssumptions(entity.getSecurityDepositAssumptions());
				FinalSummary finalSummary = new FinalSummary();

				FieldValue totalAdmits = new FieldValue();
				/* totalAdmits.setCostType(getTotalAdmits(projectDetails)); */
				// System.out.println(projectDetails.get().getOccupancy());
				if (projectDetails.get().getFinalSummary() != null
						&& projectDetails.get().getFinalSummary().getTotalAdmits() != null) {
					totalAdmits.setQuery(projectDetails.get().getFinalSummary().getTotalAdmits().getQuery());
					totalAdmits.setLastQueryUserId(
							projectDetails.get().getFinalSummary().getTotalAdmits().getLastQueryUserId());
				}
				totalAdmits.setCostType(getTotalAdmits(projectDetails));
				finalSummary.setTotalAdmits(totalAdmits);

				FieldValue totalOccupancy = new FieldValue();
				/* totalOccupancy.setCostType(getTotalOccupancy(projectDetails)); */
				Double asocc = 25d;
				// totalOccupancy.setCostType(getTotalOccupancy(projectDetails));

				if(!ObjectUtils.isEmpty(projectDetails.get().getOccupancy()))
				 totalOccupancy.setComments(projectDetails.get().getOccupancy().get(0).getComments());

				if (projectDetails.get().getFinalSummary() != null
						&& projectDetails.get().getFinalSummary().getOccupancy() != null) {
					totalOccupancy.setQuery(projectDetails.get().getFinalSummary().getOccupancy().getQuery());
					totalOccupancy.setLastQueryUserId(
							projectDetails.get().getFinalSummary().getOccupancy().getLastQueryUserId());
				}

				finalSummary.setOccupancy(totalOccupancy);

				FieldValue totalAtp = new FieldValue();
				/* totalAtp.setCostType(getTotalAtp(projectDetails)); */
				Double asAtp = 25d;
				// totalAtp.setCostType(getTotalAtp(projectDetails));

				if (projectDetails.get().getFinalSummary() != null
						&& projectDetails.get().getFinalSummary().getAtp() != null) {
					totalAtp.setQuery(projectDetails.get().getFinalSummary().getAtp().getQuery());
					totalAtp.setLastQueryUserId(projectDetails.get().getFinalSummary().getAtp().getLastQueryUserId());
				}

				if(!ObjectUtils.isEmpty(projectDetails.get().getAtp()))
				  totalAtp.setComments(projectDetails.get().getAtp().get(0).getNormal().getComments());
				
				finalSummary.setAtp(totalAtp);

				FieldValue totalSph = new FieldValue();
				/* totalSph.setCostType(getTotalSph(projectDetails)); */
				Double as = 25d;
				// totalSph.setCostType(getTotalSph(projectDetails));

				if (projectDetails.get().getFinalSummary() != null
						&& projectDetails.get().getFinalSummary().getSph() != null) {
					totalSph.setQuery(projectDetails.get().getFinalSummary().getSph().getQuery());
					totalSph.setLastQueryUserId(projectDetails.get().getFinalSummary().getSph().getLastQueryUserId());
				}

				totalSph.setComments(!ObjectUtils.isEmpty(projectDetails.get().getSph())
						? projectDetails.get().getSph().get(0).getComments()
						: "");

				finalSummary.setSph(totalSph);

				FieldValue adRevenue = new FieldValue();
				if (projectDetails.get().getFinalSummary() != null
						&& projectDetails.get().getFinalSummary().getAdRevenue() != null) {
					adRevenue.setQuery(projectDetails.get().getFinalSummary().getAdRevenue().getQuery());
					adRevenue.setLastQueryUserId(
							projectDetails.get().getFinalSummary().getAdRevenue().getLastQueryUserId());
				}

				adRevenue.setComments(projectDetails.get().getAdRevenue().getComments());
				adRevenue.setCostType((double) (projectDetails.get().getAdRevenue().getCostType() != null ?
						projectDetails.get().getAdRevenue().getCostType() : 0));
			  
				finalSummary.setAdRevenue(adRevenue);

				FieldValue personalExpense = new FieldValue();
				if (projectDetails.get().getFinalSummary() != null
						&& projectDetails.get().getFinalSummary().getPersonalExpense() != null) {
					personalExpense.setQuery(projectDetails.get().getFinalSummary().getPersonalExpense().getQuery());
					personalExpense.setLastQueryUserId(
							projectDetails.get().getFinalSummary().getPersonalExpense().getLastQueryUserId());
				}
				
				personalExpense.setComments(projectDetails.get().getPersonalExpenses().getComments());
				personalExpense.setCostType((double) (projectDetails.get().getPersonalExpenses().getCostType() != null ?
						projectDetails.get().getPersonalExpenses().getCostType() : 0));
			
				finalSummary.setPersonalExpense(personalExpense);

				FieldValue electricityAndWater = new FieldValue();
				if (projectDetails.get().getFinalSummary() != null
						&& projectDetails.get().getFinalSummary().getElectricityWaterExpanse() != null) {
					electricityAndWater
							.setQuery(projectDetails.get().getFinalSummary().getElectricityWaterExpanse().getQuery());
					electricityAndWater.setLastQueryUserId(
							projectDetails.get().getFinalSummary().getElectricityWaterExpanse().getLastQueryUserId());
				}

				electricityAndWater.setComments(projectDetails.get().getElectricityWaterBill().getComments());
				electricityAndWater.setCostType((double) (projectDetails.get().getElectricityWaterBill().getCostType() != null ?
						projectDetails.get().getElectricityWaterBill().getCostType() : 0));
			
				finalSummary.setElectricityWaterExpanse(electricityAndWater);

				FieldValue totalRAndExpense = new FieldValue();
				if (projectDetails.get().getFinalSummary() != null
						&& projectDetails.get().getFinalSummary().getTotalRmExpenses() != null) {
					totalRAndExpense.setQuery(projectDetails.get().getFinalSummary().getTotalRmExpenses().getQuery());
					totalRAndExpense.setLastQueryUserId(
							projectDetails.get().getFinalSummary().getTotalRmExpenses().getLastQueryUserId());
				}

				totalRAndExpense.setComments(projectDetails.get().getTotalRMExpenses().getComments());
				totalRAndExpense.setCostType((double) (projectDetails.get().getTotalRMExpenses().getCostType() != null ?
						projectDetails.get().getTotalRMExpenses().getCostType() : 0));
				
				FieldValue projectCost = new FieldValue();
				if (projectDetails.get().getFinalSummary() != null
						&& projectDetails.get().getFinalSummary().getTotalProjectCost() != null) {
					projectCost.setQuery(projectDetails.get().getFinalSummary().getTotalProjectCost().getQuery());
					projectCost.setLastQueryUserId(
							projectDetails.get().getFinalSummary().getTotalProjectCost().getLastQueryUserId());
				}
				projectCost.setCostType(projectDetails.get().getProjectCost());
				finalSummary.setTotalProjectCost(projectCost);

				Integer leasePeroid = projectDetails.get().getLesserLesse().getLeasePeriod() != null ? 
						projectDetails.get().getLesserLesse().getLeasePeriod() : 0;
				finalSummary.setLeasePeriod(leasePeroid);
			
				projectDetails.get().setFinalSummary(finalSummary);

				finalSummary.setTotalRmExpenses(totalRAndExpense);

				if (Constants.PRE_HANDOVER.equals(projectDetails.get().getFeasibilityState())) {
					if (projectDetails.get().getIsOperationSubmit() && projectDetails.get().getIsProjectSubmit()) {
						projectDetails.get().setStatus(Constants.STEP_6);
					} else {
						   projectDetails.get().setStatus(Constants.STEP_5);
					}
				} else {
					projectDetails.get().setStatus(Constants.STEP_6);
				}

				Set<Integer> checkStatus = projectDetails.get().getCheckStatus();
				if (!ObjectUtils.isEmpty(checkStatus)) {
					checkStatus.add(Constants.STEP_6);
					entity.setCheckStatus(checkStatus);
				}

				projectDetails.get().createdBy(RoleUtil.getCurrentUserID());
			}
			
           if(!ObjectUtils.isEmpty(entity.getRevenueAssumptionsGrowthRate())) {
				
				RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate = new RevenueAssumptionsGrowthRate();
				
				Map<Integer,Double> admitsGrowth = new TreeMap<>(entity.getRevenueAssumptionsGrowthRate().getAdmitsGrowth());    
				Map<Integer,Double> atpGrowth = new TreeMap<>(entity.getRevenueAssumptionsGrowthRate().getAtpGrowth());
		        Map<Integer,Double> sphGrowth = new TreeMap<>(entity.getRevenueAssumptionsGrowthRate().getSphGrowth());
				Map<Integer,Double> sponsorshipIncomeGrowth = new TreeMap<>(entity.getRevenueAssumptionsGrowthRate().getSponsorshipIncomeGrowth());
				Map<Integer,Double> convenienceFeeGrowth = new TreeMap<>(entity.getRevenueAssumptionsGrowthRate().getConvenienceFeeGrowth());
				Map<Integer,Double> otherIncomeGrowth = new TreeMap<>(entity.getRevenueAssumptionsGrowthRate().getOtherIncomeGrowth());
				
				revenueAssumptionsGrowthRate.setAdmitsGrowth(admitsGrowth);
				revenueAssumptionsGrowthRate.setAtpGrowth(atpGrowth);
				revenueAssumptionsGrowthRate.setSphGrowth(sphGrowth);
				revenueAssumptionsGrowthRate.setSponsorshipIncomeGrowth(sponsorshipIncomeGrowth);
				revenueAssumptionsGrowthRate.setConvenienceFeeGrowth(convenienceFeeGrowth);
				revenueAssumptionsGrowthRate.setOtherIncomeGrowth(otherIncomeGrowth);
				
			   projectDetails.get().setRevenueAssumptionsGrowthRate(revenueAssumptionsGrowthRate);
			}
			if(!ObjectUtils.isEmpty(entity.getCostAssumptionsGrowthRate())) {
				
				CostAssumptionsGrowthRate costAssumptionsGrowthRate = new CostAssumptionsGrowthRate();
				
				Map<Integer,Double> personnelExpenses = new TreeMap<>(entity.getCostAssumptionsGrowthRate().getPersonnelExpenses());
				Map<Integer,Double> electricityAndAirconditioning = new TreeMap<>(entity.getCostAssumptionsGrowthRate().getElectricityAndAirconditioning());
				Map<Integer,Double> repairAndMaintenance = new TreeMap<>(entity.getCostAssumptionsGrowthRate().getRepairAndMaintenance());
				Map<Integer,Double> otherOverheadExpenses = new TreeMap<>(entity.getCostAssumptionsGrowthRate().getOtherOverheadExpenses());
				Map<Integer,Double> propertyTax = new TreeMap<>(entity.getCostAssumptionsGrowthRate().getPropertyTax());
				
				costAssumptionsGrowthRate.setPersonnelExpenses(personnelExpenses);
				costAssumptionsGrowthRate.setElectricityAndAirconditioning(electricityAndAirconditioning);
				costAssumptionsGrowthRate.setRepairAndMaintenance(repairAndMaintenance);
				costAssumptionsGrowthRate.setOtherOverheadExpenses(otherOverheadExpenses);
				costAssumptionsGrowthRate.setPropertyTax(propertyTax);
				
			   projectDetails.get().setCostAssumptionsGrowthRate(costAssumptionsGrowthRate);
			}

			
			//Saving Cogs % Values:::
			if(!ObjectUtils.isEmpty(entity.getCogs())) {
				AssumptionsValues cogsAssumptionValue = new AssumptionsValues();
				cogsAssumptionValue.setSuggestedValue(entity.getCogs().getSuggestedValue());
				cogsAssumptionValue.setInputValues(entity.getCogs().getInputValues());
				cogsAssumptionValue.setComments(entity.getCogs().getComments());
				projectDetails.get().setCogs(cogsAssumptionValue);
			}
		
			//Saving No Of Shows Values:::
			if(!ObjectUtils.isEmpty(entity.getNoOfShows())) {
				AssumptionsValues noOfShowsAssumptionValue = new AssumptionsValues();
				noOfShowsAssumptionValue.setSuggestedValue(entity.getNoOfShows().getSuggestedValue());
				noOfShowsAssumptionValue.setInputValues(entity.getNoOfShows().getInputValues());
				noOfShowsAssumptionValue.setComments(entity.getNoOfShows().getComments());
				projectDetails.get().setNoOfShows(noOfShowsAssumptionValue);
			}
			
			projectDetails.get().setYearOneFootfallAssumedLowerBy(entity.getYearOneFootfallAssumedLowerBy());
			projectDetails.get().setYearOneAdIncomeLowerByPercent(entity.getYearOneAdIncomeLowerByPercent());
			
			projectDetails.get().setTotalBlendedAtp(entity.getTotalBlendedAtp());
			
			ProjectDetails details = projectDetails.get();

			if (RoleUtil.isCurrentUserHasBDRole()) {
				details = assumptionsCalculation.calculateAssumptionsDetails(projectDetails);
			} else {
				details = projectDetails.get();
			}

			// submit by project/operation
			if (RoleUtil.isCurrentUserHasOpeartionRole() && !entity.isAllowSaveAndClose()) {
				details.setIsOperationSubmit(true);
				projectDetails.get().setStatus(Constants.STEP_5);
				details.setOperationReasonMsg(null);

			} else if (RoleUtil.isCurrentUserHasProjectTeamRole()) {
				details.setIsProjectSubmit(true);
				projectDetails.get().setStatus(Constants.STEP_5);
				details.setProjectReasonMsg(null);
			}

			details.setMsgMapper(null);
			details.setUpdatedDate(new Date().getTime());
			if(!ObjectUtils.isEmpty(details.getLesserLesse().getYearWiseAdvanceRentAdjustment())
					&& details.getLesserLesse().getYearWiseAdvanceRentAdjustment().containsKey(0)) {
				details.getLesserLesse().getYearWiseAdvanceRentAdjustment().remove(0);
			}

			projectDetailsDao.save(details);
			createNotification(details);
			/* reportService.HtmlToPdfConversion(null, projectDetails); */

			return details;
		} catch (Exception e) {
			log.debug("Exception in saving step 6.");
			e.printStackTrace();
			throw new FeasibilityException("Exception in saving step 6");
		}

	}

	@Override
	public ProjectDetails step7(ProjectDetails entity) {

		if (ObjectUtils.isEmpty(entity) || ObjectUtils.isEmpty(entity.getId())) {
			log.error("Empty survey can not assined.");
			throw new FeasibilityException("Empty Project Details can not assined.");
		}
		List<String> ccList = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate1 = sdf.format(new Date());
		FeasibilityUser currentUser = RoleUtil.getCurrentUseInfo();
		
		String message = " has been submitted for approval by " + currentUser.getFullName() + " on " + currentDate1 + ".";
		
		try {
			ProjectDetails projectDetails = this.get(entity.getId());
			if (ObjectUtils.isEmpty(entity)) {
				log.error("Project Details not found.");
				throw new FeasibilityException("Project Details not found.");
			}
			projectDetails.setIsRejected(entity.getIsRejected());
			projectDetails.setStatus(Constants.STATUS_SEND_FOR_APPROVAL);
			projectDetails.createdBy(RoleUtil.getCurrentUserID());
			projectDetails.setMsgMapper(null);
			LocalDate currentDate = LocalDate.now();
			projectDetails.setDateOfSendForApproval(currentDate.toString());
			projectDetails.setUpdatedDate(new Date().getTime());
			projectDetails.setProjectBdName(currentUser.getFullName());

			ProjectDetails savedProjectDetails = projectDetailsDao.save(projectDetails);

			String toEmail = userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getId();
			createNotification(savedProjectDetails);
			message = "Report: " + projectDetails.getReportId() + " " + projectDetails.getProjectName() + message;
			mailService.mailService(toEmail, MessageConstants.MAIL_SUBJECT_APPROVAL_PENDING, message, ccList);

			return savedProjectDetails;
		} catch (Exception e) {
			log.debug("Exception in saving step 7.");
			e.printStackTrace();
			throw new FeasibilityException("Exception in saving step 7");
		}

	}

	@Override
	public List<ProjectDetails> fetchAllProjectDetails(Pageable pageable, List<Filter> filters) {
		try {
			Query query = QueryBuilder.createQuery(filters, pageable);
			query.getSortObject();
			List<Integer> status = new ArrayList<>();
			status.add(Constants.STATUS_DELETE);
			status.add(Constants.STATUS_ACCEPTED);
			status.add(Constants.STATUS_ARCHIVED);
			status.add(Constants.POST_STATUS_ARCHIVED);
			status.add(Constants.PRE_HAND_STATUS_ARCHIVED);

			if (RoleUtil.isCurrentUserHasBDRole()) {
				query.addCriteria(Criteria.where("status").not().in(status).and("projectBdId")
						.is(RoleUtil.getCurrentUseInfo().getId()));

			} else {
				query.addCriteria(Criteria.where("status").not().in(status));
			}

			List<ProjectDetails> surveyResultList = mongoTemplate.find(query, ProjectDetails.class);

			surveyResultList.sort((obj1, obj2) -> {
				return (obj1.getCreatedDate() > obj2.getCreatedDate()) ? 1
						: (obj1.getCreatedDate() < obj2.getCreatedDate()) ? 1 : 0;
			});

			return surveyResultList;
		} catch (Exception e) {
			log.error("error while fetching Project Details.", e);
			throw new FeasibilityException("error while fetching Project Details");
		}
	}

	@Override
	public List<ProjectDetails> fetchPreSigningProjectByStatus(Pageable pageable, List<Filter> filters) {
		try {
			Query query = QueryBuilder.createQuery(filters, pageable);

			List<Integer> status = new ArrayList<>();
			status.add(Constants.STATUS_ACCEPTED);
			status.add(Constants.STATUS_ARCHIVED);

			query.addCriteria(Criteria.where("feasibilityState").is(Constants.PRE_SIGNING).and("status").in(status));
			query.getSortObject();

			List<ProjectDetails> surveyResultList = mongoTemplate.find(query, ProjectDetails.class);

			surveyResultList.sort((obj1, obj2) -> {
				return (obj1.getCreatedDate() > obj2.getCreatedDate()) ? 1
						: (obj1.getCreatedDate() < obj2.getCreatedDate()) ? 1 : 0;
			});

			return surveyResultList;
		} catch (Exception e) {
			log.error("error while fetching Project Details.", e);
			throw new FeasibilityException("error while fetching Project Details");
		}
	}

	@Override
	public ProjectDetails update(String id, ProjectDetails entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProjectDetails get(String id) {
		Optional<ProjectDetails> pD = projectDetailsDao.findById(id);
		return pD.isPresent() ? pD.get() : null;
	}

	@Override
	public ProjectDetails save(ProjectDetails entity) {
		entity.setUpdatedDate(new Date().getTime());

		return projectDetailsDao.save(entity);
	}

	@Override
	public Notification createNotification(ProjectDetails projectDetails) {
		// TODO Auto-generated method stub

		Notification notification = new Notification();
		notification.setCreatedBy(projectDetails.getCreatedBy());
		notification.setApproverId(projectDetails.getApproverId());
		notification.setProjectId(projectDetails.getId());
		notification.setProjectType(projectDetails.getProjectType());
		notification.setCreatedDate(new Date().getTime());
		notification.setProjectStatus(projectDetails.getStatus());
		notification.setReportId(projectDetails.getReportId());
		notification.setProjectName(projectDetails.getProjectName());
		notification.setFeasibilityState(projectDetails.getFeasibilityState());

		notification.setIsProjectSubmit(projectDetails.getIsProjectSubmit());
		notification.setIsOperationSubmit(projectDetails.getIsOperationSubmit());

		notification.setProjectBdId(projectDetails.getProjectBdId());
		notification.setReportStatus(projectDetails.getReportStatus());

		if (RoleUtil.getCurrentUseInfo().getAuthorities().stream()
				.anyMatch(predicate -> predicate.getAuthority().equals(Constants.ROLE_APPROVER))) {

			if (notification.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
				notification.setActionByType(RoleUtil.getCurrentUseInfo().getPreHandOverApproverType());
			} else
				notification.setActionByType(RoleUtil.getCurrentUseInfo().getApproverType());
		}

		if (projectDetails.getMsgMapper() != null)
			notification.setMsgMapper(projectDetails.getMsgMapper());

		try {
			notification = notificationDao.save(notification);

		} catch (Exception e) {
			throw new FeasibilityException("Error in saving Notification");
		}
		return notification;
	}

	@Override
	public ProjectDetails approveProject(String projectId) {
		if (ObjectUtils.isEmpty(projectId)) {
			throw new FeasibilityException("Project id cannot be null or empty");
		}
		List<String> ccList = new ArrayList<>();
//		String message = "Approved" + MessageConstants.APPROVAL_STATUS_1;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = sdf.format(new Date());
		FeasibilityUser currentUser = RoleUtil.getCurrentUseInfo();
		String message = " has been approved by " + currentUser.getFullName() + " today on " + currentDate + ".";
		
		ProjectDetails projectDetails = null;
		try {
			ProjectDetails existingProject = projectDetailsDao.findById(projectId).get();

			if (!ObjectUtils.isEmpty(existingProject)) {

				if (RoleUtil.isCurrentUserHasApproverRole()
						&& existingProject.getFeasibilityState().equals(Constants.PRE_SIGNING)) {

					int type = RoleUtil.getCurrentUseInfo().getApproverType();
					if (type == Constants.STATUS_APPROVER_1) {
						User user = userDao.findOneByApproverType(type + 1);
						if (!ObjectUtils.isEmpty(user)) {
							existingProject.setStatus(type);
							existingProject.setApproverId(currentUser.getId());

							message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;
							ccList.add(user.getEmail());
						}
					} else {
						if (type == Constants.STATUS_APPROVER_2) {
							existingProject.setStatus(Constants.STATUS_ACCEPTED);
							existingProject.setApproverId(currentUser.getId());

//							message = "Report: " + existingProject.getReportId() + " "
//									+ MessageConstants.APPROVAL_ACCEPTED;
							message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;

//							ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getEmail());
						}

					}

					if (RoleUtil.getCurrentUseInfo().getApproverType() == Constants.STATUS_APPROVER_1)
						existingProject.setCreatedBy(RoleUtil.getCurrentUseInfo().getId());

				} else if (RoleUtil.isCurrentUserHasApproverRole()
						&& existingProject.getFeasibilityState().equals(Constants.POST_SIGNING)) {

					int type = RoleUtil.getCurrentUseInfo().getApproverType();
					if (type == Constants.STATUS_APPROVER_1) {
						User user = userDao.findOneByApproverType(type + 1);
						if (!ObjectUtils.isEmpty(user)) {
							existingProject.setStatus(type);
							existingProject.setApproverId(currentUser.getId());

//							message = "Report: " + existingProject.getReportId() + " " + message;
							message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;
							ccList.add(user.getEmail());
						}
						existingProject.setCreatedBy(RoleUtil.getCurrentUseInfo().getId());
					} else if (type == Constants.STATUS_APPROVER_2) {

//						User user = userDao.findOneByApproverType(type);
						User user1 = userDao.findOneByApproverType(type - 1);
						User user2 = userDao.findOneByApproverType(type + 1);

						existingProject.setStatus(type);
						existingProject.setApproverId(currentUser.getId());

						message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;
						ccList.add(user1.getEmail());
						ccList.add(user2.getEmail());

						existingProject.setCreatedBy(RoleUtil.getCurrentUseInfo().getId());
					} else if (type == Constants.STATUS_APPROVER_3) {
						User user = userDao.findOneByApproverType(type - 2);
						User user1 = userDao.findOneByApproverType(type - 1);
						User user2 = userDao.findOneByApproverType(type + 1);
						existingProject.setStatus(type);
						existingProject.setApproverId(currentUser.getId());

						message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;
						ccList.add(user.getEmail());
						ccList.add(user1.getEmail());
						ccList.add(user2.getEmail());

						existingProject.setCreatedBy(RoleUtil.getCurrentUseInfo().getId());
					} else if (type == Constants.STATUS_APPROVER_4) {
						
						User user = userDao.findOneByApproverType(type - 3);
						User user1 = userDao.findOneByApproverType(type - 2);
						User user2 = userDao.findOneByApproverType(type - 1);

						existingProject.setStatus(Constants.STATUS_ACCEPTED);
						existingProject.setApproverId(currentUser.getId());
//						message = "Report: " + existingProject.getReportId() + " " + MessageConstants.APPROVAL_ACCEPTED;
						message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;
						ccList.add(user.getEmail());
						ccList.add(user1.getEmail());
						ccList.add(user2.getEmail());
						existingProject.setCreatedBy(currentUser.getId());

					}
				} else if (RoleUtil.isCurrentUserHasApproverRole()
						&& existingProject.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
					int type = RoleUtil.getCurrentUseInfo().getApproverType();
					if (type == Constants.STATUS_APPROVER_1) {
						User user = userDao.findOneByPreHandOverApproverType(type + 1);
						if (!ObjectUtils.isEmpty(user)) {
							existingProject.setStatus(type);
							existingProject.setApproverId(currentUser.getId());

							message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;
							ccList.add(user.getEmail());
						}
						existingProject.setCreatedBy(RoleUtil.getCurrentUseInfo().getId());
					} else if (type == Constants.STATUS_APPROVER_2) {

						User user = userDao.findOneByPreHandOverApproverType(type - 1);
						User user1 = userDao.findOneByPreHandOverApproverType(type + 1);

						existingProject.setStatus(type);
						existingProject.setApproverId(currentUser.getId());

						message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;
						ccList.add(user.getEmail());
						ccList.add(user1.getEmail());

						existingProject.setCreatedBy(RoleUtil.getCurrentUseInfo().getId());
					} else if (type == Constants.STATUS_APPROVER_5) {
						User user = userDao.findOneByApproverType(type + 1);
						existingProject.setStatus(user.getPreHandOverApproverType() - 1);
						existingProject.setApproverId(currentUser.getId());
//						message = "Report: " + existingProject.getReportId() + " " + MessageConstants.APPROVAL_ACCEPTED;
						message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;

						ccList.add(userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_4).getEmail());
						
						User user1 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_1);
						User user2 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_2);
						ccList.add(user1.getEmail());
						ccList.add(user2.getEmail());
						existingProject.setCreatedBy(currentUser.getId());
						
					} else if (type == Constants.STATUS_APPROVER_6) {
						User user = userDao.findOneByApproverType(Constants.STATUS_APPROVER_3);
						existingProject.setStatus(user.getPreHandOverApproverType() - 1);
						existingProject.setApproverId(currentUser.getId());
//						message = "Report: " + existingProject.getReportId() + " " + MessageConstants.APPROVAL_ACCEPTED;
						message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;
						ccList.add(userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_5).getEmail());
						
						User user1 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_1);
						User user2 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_2);
						User user3 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_3);
						ccList.add(user1.getEmail());
						ccList.add(user2.getEmail());
						ccList.add(user3.getEmail());
						existingProject.setCreatedBy(currentUser.getId());

					} else if (type == Constants.STATUS_APPROVER_3) {
						User user = userDao.findOneByApproverType(Constants.STATUS_APPROVER_4);
						existingProject.setStatus(user.getPreHandOverApproverType() - 1);
						existingProject.setApproverId(currentUser.getId());

						message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;
//						ccList.add(user.getEmail());
						
						User user1 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_1);
						User user2 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_2);
						User user3 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_3);
						User user4 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_4);
						User user5 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_6);

						ccList.add(user1.getEmail());
						ccList.add(user2.getEmail());
						ccList.add(user3.getEmail());
						ccList.add(user4.getEmail());
						ccList.add(user5.getEmail());

						existingProject.setCreatedBy(currentUser.getId());
						
					} else if (type == Constants.STATUS_APPROVER_4) {
						existingProject.setStatus(Constants.STATUS_ACCEPTED);
						existingProject.setApproverId(currentUser.getId());
						message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + "" + message;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_6).getEmail());
						
						User user1 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_1);
						User user2 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_2);
						User user3 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_3);
						User user4 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_4);
						User user5 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_5);

						ccList.add(user1.getEmail());
						ccList.add(user2.getEmail());
						ccList.add(user3.getEmail());
						ccList.add(user4.getEmail());
						ccList.add(user5.getEmail());
						existingProject.setCreatedBy(currentUser.getId());

					}
				}
				existingProject.setUpdatedDate(new Date().getTime());

				projectDetails = projectDetailsDao.save(existingProject);
			}

			mailService.mailService(projectDetails.getCreatedBy(), MessageConstants.MAIL_SUBJECT_APPROVE, message,
					ccList);

			// make mesgMapper blank before approving in notification
			existingProject.setMsgMapper(null);
			createNotification(existingProject);

		} catch (Exception e) {
			throw new FeasibilityException("Error while saving approved project details");
		}

		return projectDetails;
	}

	@Override
	public ProjectDetails rejectProject(String projectId) {
		if (ObjectUtils.isEmpty(projectId)) {
			throw new FeasibilityException("Project id cannot be null or empty");
		}
		List<String> ccList = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = sdf.format(new Date());
		FeasibilityUser currentUser = RoleUtil.getCurrentUseInfo();		
		String message = "";
		ProjectDetails projectDetails = null;
		
		try {
			ProjectDetails existingProject = projectDetailsDao.findById(projectId).get();

			if (!ObjectUtils.isEmpty(existingProject)) {
				String approvalDate[] = existingProject.getDateOfSendForApproval().split("-");
				
				message = "Report: " + existingProject.getReportId() + " " + existingProject.getProjectName() + " submitted for approval by " + existingProject.getProjectBdName()
				      + " on " + approvalDate[2]+"-"+approvalDate[1]+"-"+approvalDate[0] + " has been rejected by ";

				String bdUserMailId = userDao.findOneById(existingProject.getProjectBdId()).getEmail();
				ccList.add(bdUserMailId);
				
				if (RoleUtil.isCurrentUserHasApproverRole()
						&& existingProject.getFeasibilityState().equals(Constants.PRE_SIGNING)) {

					existingProject.setStatus(Constants.STATUS_REJECTED);
					existingProject.setRejectedBy(currentUser.getId());
					existingProject.setIsRejected(true);
					message += currentUser.getFullName();
					
					int type = RoleUtil.getCurrentUseInfo().getApproverType();
					if (type == Constants.STATUS_APPROVER_1) {
						User user = userDao.findOneByApproverType(type);
						if (!ObjectUtils.isEmpty(user)) {	
//							ccList.add(user.getEmail());
						}
					} else {
						if (type == Constants.STATUS_APPROVER_2) {
//							User user1 = userDao.findOneByApproverType(type);
							User user2 = userDao.findOneByApproverType(type - 1);
                            if(!ObjectUtils.isEmpty(user2)) {
    							ccList.add(user2.getEmail());
                            }
						}
					}

				} else if (RoleUtil.isCurrentUserHasApproverRole()
						&& existingProject.getFeasibilityState().equals(Constants.POST_SIGNING)) {

					existingProject.setStatus(Constants.POST_STATUS_REJECTED);
					existingProject.setRejectedBy(currentUser.getId());
					existingProject.setIsRejected(true);
					message += currentUser.getFullName();
					
					int type = RoleUtil.getCurrentUseInfo().getApproverType();
					if (type == Constants.STATUS_APPROVER_1) {
						User user = userDao.findOneByApproverType(type);
						if (!ObjectUtils.isEmpty(user)) {
//							ccList.add(user.getEmail());
						}
					} else if (type == Constants.STATUS_APPROVER_2) {
						User user = userDao.findOneByApproverType(type);
						User user1 = userDao.findOneByApproverType(type - 1);
                        if(!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user1)) {
//                        	ccList.add(user.getEmail());
    						ccList.add(user1.getEmail());
                        }	

					} else if (type == Constants.STATUS_APPROVER_3) {
						User user = userDao.findOneByApproverType(type - 2);
						User user1 = userDao.findOneByApproverType(type - 1);
						User user2 = userDao.findOneByApproverType(type);
                        if(!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user1) && !ObjectUtils.isEmpty(user2)) {
                        	ccList.add(user.getEmail());
    						ccList.add(user1.getEmail());
//    						ccList.add(user2.getEmail());
                        }

					} else if (type == Constants.STATUS_APPROVER_4) {
						User user = userDao.findOneByApproverType(type - 3);
						User user1 = userDao.findOneByApproverType(type - 2);
						User user2 = userDao.findOneByApproverType(type - 1);
						User user3 = userDao.findOneByApproverType(type);
						if (!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user1) && !ObjectUtils.isEmpty(user2)
								&& !ObjectUtils.isEmpty(user3)) {
							ccList.add(user.getEmail());
							ccList.add(user1.getEmail());
							ccList.add(user2.getEmail());
//							ccList.add(user3.getEmail());
                        }
					}
				} else if (RoleUtil.isCurrentUserHasApproverRole()
						&& existingProject.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
					
					existingProject.setStatus(Constants.PRE_HAND_STATUS_REJECTED);
					existingProject.setRejectedBy(currentUser.getId());
					existingProject.setIsRejected(true);
					message += currentUser.getFullName();
					
					int type = RoleUtil.getCurrentUseInfo().getApproverType();
					if (type == Constants.STATUS_APPROVER_1) {
						User user = userDao.findOneByPreHandOverApproverType(type);
						if (!ObjectUtils.isEmpty(user)) {
//							ccList.add(user.getEmail());
						}
						
					} else if (type == Constants.STATUS_APPROVER_2) {
						User user = userDao.findOneByPreHandOverApproverType(type);
						User user1 = userDao.findOneByPreHandOverApproverType(type - 1);
						if (!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user1)) {
//							ccList.add(user.getEmail());
							ccList.add(user1.getEmail());
						}

					} else if (type == Constants.STATUS_APPROVER_5) {
						User user = userDao.findOneByPreHandOverApproverType(type);
						User user1 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_1);
						User user2 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_2);
						if (!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user1)&& !ObjectUtils.isEmpty(user2)) {
//							ccList.add(user.getEmail());
							ccList.add(user1.getEmail());
							ccList.add(user2.getEmail());
						}

					} else if (type == Constants.STATUS_APPROVER_6) {
						User user = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_5);
						User user1 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_1);
						User user2 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_2);
						User user3 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_3);
						if (!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user1) && !ObjectUtils.isEmpty(user2)
								&& !ObjectUtils.isEmpty(user3)) {
//							ccList.add(user.getEmail());
							ccList.add(user1.getEmail());
							ccList.add(user2.getEmail());
							ccList.add(user3.getEmail());

						}

					} else if (type == Constants.STATUS_APPROVER_3) {
						User user = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_5);
						User user1 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_1);
						User user2 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_2);
						User user3 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_3);
						User user4 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_4);
						if (!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user1) && !ObjectUtils.isEmpty(user2)
								&& !ObjectUtils.isEmpty(user3) && !ObjectUtils.isEmpty(user4)) {
//							ccList.add(user.getEmail());
							ccList.add(user1.getEmail());
							ccList.add(user2.getEmail());
							ccList.add(user3.getEmail());
							ccList.add(user4.getEmail());
						}

					} else if (type == Constants.STATUS_APPROVER_4) {
						User user = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_5);
						User user1 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_1);
						User user2 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_2);
						User user3 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_3);
						User user4 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_4);
						User user5 = userDao.findOneByPreHandOverApproverType(Constants.STATUS_APPROVER_5);
						if (!ObjectUtils.isEmpty(user) && !ObjectUtils.isEmpty(user1) && !ObjectUtils.isEmpty(user2)
								&& !ObjectUtils.isEmpty(user3) && !ObjectUtils.isEmpty(user4) && !ObjectUtils.isEmpty(user5)) {
//							ccList.add(user.getEmail());
							ccList.add(user1.getEmail());
							ccList.add(user2.getEmail());
							ccList.add(user3.getEmail());
							ccList.add(user4.getEmail());
							ccList.add(user5.getEmail());
						}
					}
					
					existingProject.setIsSendForOpsAndProject(true);
					existingProject.setOperationReasonMsg(null);
					existingProject.setProjectReasonMsg(null);
				}
				
				message += " on " + currentDate + ".";
				
				existingProject.setUpdatedDate(new Date().getTime());
				projectDetails = projectDetailsDao.save(existingProject);
			}

			mailService.mailService(currentUser.getId(), MessageConstants.MAIL_SUBJECT_REJECT, message,
					ccList);

			// make mesgMapper blank before approving in notification
			existingProject.setMsgMapper(null);
			createNotification(existingProject);

		} catch (Exception e) {
			log.info(e.getMessage());
			throw new FeasibilityException("Error while saving rejected project details");
		}

		return projectDetails;
//		if (ObjectUtils.isEmpty(projectId)) {
//			throw new FeasibilityException("Project id cannot be null or empty");
//		}
//
//		List<String> ccList = new ArrayList<>();
//		String message = MessageConstants.REJECTED_BY;
//
//		ProjectDetails projectDetails = null;
//		try {
//			ProjectDetails existingProject = projectDetailsDao.findById(projectId).get();
//
//			if (!ObjectUtils.isEmpty(existingProject)) {
//				if (existingProject.getFeasibilityState().equals(Constants.PRE_SIGNING)) {
//					existingProject.setStatus(Constants.STATUS_REJECTED);
//					existingProject.setRejectedBy(RoleUtil.getCurrentUserID());
//					existingProject.setUpdatedDate(new Date().getTime());
//
//					projectDetails = projectDetailsDao.save(existingProject);
//
//					int type = RoleUtil.getCurrentUseInfo().getApproverType();
//					if (type == Constants.STATUS_APPROVER_1) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.APPROVAL_STATUS_1;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).getEmail());
//					} else if (type == Constants.STATUS_APPROVER_2) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.APPROVAL_STATUS_2;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getEmail());
//					}
//
//				} else if (existingProject.getFeasibilityState().equals(Constants.POST_SIGNING)) {
//					existingProject.setStatus(Constants.POST_STATUS_REJECTED);
//					existingProject.setRejectedBy(RoleUtil.getCurrentUserID());
//					existingProject.setUpdatedDate(new Date().getTime());
//
//					projectDetails = projectDetailsDao.save(existingProject);
//
//					int type = RoleUtil.getCurrentUseInfo().getApproverType();
//					if (type == Constants.STATUS_APPROVER_1) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.POST_APPROVAL_STATUS_1;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).getEmail());
//					} else if (type == Constants.STATUS_APPROVER_2) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.APPROVAL_STATUS_2;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_3).getEmail());
//					} else if (type == Constants.STATUS_APPROVER_3) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.POST_APPROVAL_STATUS_3;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_4).getEmail());
//					} else if (type == Constants.STATUS_APPROVER_4) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.POST_APPROVAL_STATUS_4;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getEmail());
//					}
//				} else if (existingProject.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
//					existingProject.setStatus(Constants.PRE_HAND_STATUS_REJECTED);
//					existingProject.setRejectedBy(RoleUtil.getCurrentUserID());
//					existingProject.setUpdatedDate(new Date().getTime());
//
//					projectDetails = projectDetailsDao.save(existingProject);
//
//					int type = RoleUtil.getCurrentUseInfo().getApproverType();
//					if (type == Constants.STATUS_APPROVER_1) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.PRE_APPROVAL_STATUS_1;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).getEmail());
//					} else if (type == Constants.STATUS_APPROVER_2) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.PRE_APPROVAL_STATUS_2;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_3).getEmail());
//					} else if (type == Constants.STATUS_APPROVER_3) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.PRE_APPROVAL_STATUS_3;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_4).getEmail());
//					} else if (type == Constants.STATUS_APPROVER_4) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.PRE_APPROVAL_STATUS_4;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_5).getEmail());
//					} else if (type == Constants.STATUS_APPROVER_5) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.PRE_APPROVAL_STATUS_5;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_6).getEmail());
//					} else if (type == Constants.STATUS_APPROVER_6) {
//						message = "Report: " + existingProject.getReportId() + " " + message
//								+ MessageConstants.PRE_APPROVAL_STATUS_6;
//						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getEmail());
//					}
//
//					// status updated changes in case of reject for send ops and project team
//					existingProject.setIsSendForOpsAndProject(true);
//					existingProject.setOperationReasonMsg(null);
//					existingProject.setProjectReasonMsg(null);
//					existingProject.setUpdatedDate(new Date().getTime());
//
//					projectDetails = projectDetailsDao.save(existingProject);
//				}
//
//			}
//
//			mailService.mailService(projectDetails.getCreatedBy(), MessageConstants.MAIL_SUBJECT_REJECT, message,
//					ccList);
//			projectDetails.setCreatedBy(RoleUtil.getCurrentUseInfo().getId());
//			projectDetails.setMsgMapper(null);
//			createNotification(projectDetails);
//
//		} catch (Exception e) {
//			throw new FeasibilityException("Error while saving reject project details");
//		}
//
//		return projectDetails;
	}

	@Override
	public List<ProjectDetails> getProjectByStatus(Integer status) {
		List<ProjectDetails> projectDetails = projectDetailsDao.getProjectByStatus(status);
		return projectDetails;
	}

	@Override
	public List<ProjectDetails> getProjectByInStatus(List<Integer> status) {
		List<ProjectDetails> projectDetails = projectDetailsDao.getProjectByInStatus(status);
		return projectDetails;
	}

	@Override
	public ProjectDetails getProjectDetailsById(String id) {

		if (ObjectUtils.isEmpty(id)) {
			throw new FeasibilityException("Project id cannot be null or empty");
		}

		Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(id);
		
		if(!ObjectUtils.isEmpty(projectDetails.get().getLesserLesse())){
			if(ObjectUtils.isEmpty(projectDetails.get().getLesserLesse().getRentSecurityDepositForFixedZeroMg())){
				projectDetails.get().getLesserLesse().setRentSecurityDepositForFixedZeroMg(0.0);
			}
			else {
				Double security = projectDetails.get().getLesserLesse().getRentSecurityDepositForFixedZeroMg();
				projectDetails.get().getLesserLesse().setRentSecurityDepositForFixedZeroMg(security);
				
			}
			if(ObjectUtils.isEmpty(projectDetails.get().getLesserLesse().getAdvanceRent())){
				projectDetails.get().getLesserLesse().setAdvanceRent(0.0);
			}
			else {
				Double advanceRent = projectDetails.get().getLesserLesse().getAdvanceRent();
				projectDetails.get().getLesserLesse().setAdvanceRent(advanceRent);
			}
			if(ObjectUtils.isEmpty(projectDetails.get().getLesserLesse().getAdvanceRentPeriod())){
				projectDetails.get().getLesserLesse().setAdvanceRentPeriod(0);
			}
			else {
				Integer advanceRentPeriod = projectDetails.get().getLesserLesse().getAdvanceRentPeriod();
				projectDetails.get().getLesserLesse().setAdvanceRentPeriod(advanceRentPeriod);
			}
		}
		else {
			projectDetails.get().setLesserLesse(new LesserLesse());
			projectDetails.get().getLesserLesse().setRentSecurityDepositForFixedZeroMg(0.0);
			projectDetails.get().getLesserLesse().setAdvanceRent(0.0);
		}
		projectDetails.get().setProjectCost(!ObjectUtils.isEmpty(projectDetails.get().getProjectCost()) ? projectDetails.get().getProjectCost() : 0.0);
		
		projectDetails.get().setProjectCostPerScreen(!ObjectUtils.isEmpty(projectDetails.get().getProjectCostPerScreen())
				? projectDetails.get().getProjectCostPerScreen()
				: 0.0);
		projectDetails.get().setNoOfScreens(
				!ObjectUtils.isEmpty(projectDetails.get().getNoOfScreens()) ? projectDetails.get().getNoOfScreens() : 1);

		FeasibilityUser user = RoleUtil.getCurrentUseInfo();
		projectDetails.get().setUserDesignation(user.getDesignation());
		
		if (RoleUtil.isCurrentUserHasApproverRole()) {
			int type = RoleUtil.getCurrentUseInfo().getApproverType();

			if (type == Constants.STATUS_APPROVER_1
					&& projectDetails.get().getStatus() >= Constants.STATUS_APPROVER_1) {
				projectDetails.get().setFlag(false);
			} else if (type == Constants.STATUS_APPROVER_2
					&& projectDetails.get().getStatus() >= Constants.STATUS_APPROVER_2) {
				projectDetails.get().setFlag(false);
			} else if (type == Constants.STATUS_APPROVER_3
					&& projectDetails.get().getStatus() >= Constants.STATUS_APPROVER_3) {
				projectDetails.get().setFlag(false);
			} else if (type == Constants.STATUS_APPROVER_4
					&& projectDetails.get().getStatus() >= Constants.STATUS_APPROVER_4) {
				projectDetails.get().setFlag(false);
			} else {
				projectDetails.get().setFlag(true);
			}

			if (projectDetails.get().getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
				int preHandtype = RoleUtil.getCurrentUseInfo().getApproverType();

				if (preHandtype == Constants.STATUS_APPROVER_1
						&& projectDetails.get().getStatus() >= Constants.STATUS_APPROVER_1) {
					projectDetails.get().setFlag(false);
				} else if (preHandtype == Constants.STATUS_APPROVER_2
						&& projectDetails.get().getStatus() >= Constants.STATUS_APPROVER_2) {
					projectDetails.get().setFlag(false);
				} else if (preHandtype == Constants.STATUS_APPROVER_3
						&& projectDetails.get().getStatus() >= Constants.STATUS_APPROVER_5) {
					projectDetails.get().setFlag(false);
				} else if (preHandtype == Constants.STATUS_APPROVER_4
						&& projectDetails.get().getStatus() >= Constants.STATUS_APPROVER_6) {
					projectDetails.get().setFlag(false);
				} else if (preHandtype == Constants.STATUS_APPROVER_5
						&& projectDetails.get().getStatus() >= Constants.STATUS_APPROVER_3) {
					projectDetails.get().setFlag(false);
				} else if (preHandtype == Constants.STATUS_APPROVER_6
						&& projectDetails.get().getStatus() >= Constants.STATUS_APPROVER_4) {
					projectDetails.get().setFlag(false);
				} else {
					projectDetails.get().setFlag(true);
				}
			}

		}
		if (RoleUtil.isCurrentUserHasBDRole()) { 
			projectDetails.get().setFlag(true);
		}

		return projectDetails.isPresent() ? projectDetails.get() : null;
	}

	@Override
	public List<ProjectDetails> getProjectGreaterThanEqualStatus(Integer status) {
		List<ProjectDetails> projectDetails = projectDetailsDao.getProjectGreaterThanEqualStatus(status);
		return projectDetails;
	}

	@Override
	public FinalSummary getQueryByProjectId(String projectId) {

		FinalSummary finalSummary = null;

		if (ObjectUtils.isEmpty(projectId)) {
			throw new FeasibilityException("Project id cannot be null or empty");
		}

		ProjectDetails existingProject = projectDetailsDao.findById(projectId).get();
		if (!ObjectUtils.isEmpty(existingProject)) {
			finalSummary = existingProject.getFinalSummary();
		}

		return finalSummary;
	}

	@Override
	public QueryMapper saveQuery(MsgMapper msgMapper) {

		if (ObjectUtils.isEmpty(msgMapper.getProjectId())) {
			throw new FeasibilityException("Project id cannot be null or empty");
		}

		List<String> ccList = new ArrayList<>();
		String message = "query asked";
		String subject = "";
		String mailTo = "";

		List<QueryMapper> queryMapper = new ArrayList<QueryMapper>();
		QueryMapper mapper = new QueryMapper();

		User currentUser = userDao.findOneById(RoleUtil.getCurrentUserID());
		mapper.setUserFullName(currentUser.getFullName());
		mapper.setUserId(RoleUtil.getCurrentUserID());
		mapper.setMsg(msgMapper.getMsg());
		mapper.setDate_time(new Date().getTime());

		ProjectDetails existingProject = projectDetailsDao.findById(msgMapper.getProjectId()).get();
		if (!ObjectUtils.isEmpty(existingProject)) {
			FieldValue fieldValue = null;

			switch (msgMapper.getQueryType()) {
			case "atp":
				fieldValue = existingProject.getFinalSummary().getAtp();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());
					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setAtp(fieldValue);
				}
				break;
			case "occupancy":
				fieldValue = existingProject.getFinalSummary().getOccupancy();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());
					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setOccupancy(fieldValue);
				}
				break;
			case "sph":
				fieldValue = existingProject.getFinalSummary().getSph();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());
					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setSph(fieldValue);
				}
				break;
			case "adRevenue":
				fieldValue = existingProject.getFinalSummary().getAdRevenue();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());
					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setAdRevenue(fieldValue);
				}
				break;
			case "personalExpense":
				fieldValue = existingProject.getFinalSummary().getPersonalExpense();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());
					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setPersonalExpense(fieldValue);
				}
				break;
			case "electricityWaterExpanse":
				fieldValue = existingProject.getFinalSummary().getElectricityWaterExpanse();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());
					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setElectricityWaterExpanse(fieldValue);
				}
				break;
			case "totalRmExpenses":
				fieldValue = existingProject.getFinalSummary().getTotalRmExpenses();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());
					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setTotalRmExpenses(fieldValue);
				}
				break;
			case "totalOverheadExpenses":
				fieldValue = existingProject.getFinalSummary().getTotalOverheadExpenses();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());
					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setTotalOverheadExpenses(fieldValue);
				}
				break;
			case "totalAdmits":
				fieldValue = existingProject.getFinalSummary().getTotalAdmits();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					/*
					 * mapper.setUserId(RoleUtil.getCurrentUserID());
					 * mapper.setMsg(msgMapper.getMsg()); mapper.setDate_time(new Date().getTime());
					 */
					fieldValue.setLastQueryUserId(mapper.getUserId());
					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setTotalAdmits(fieldValue);
				}
				break;

			case "totalProjectCost":
				fieldValue = existingProject.getFinalSummary().getTotalProjectCost();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());
					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setTotalProjectCost(fieldValue);
				}
				break;

			default:
				fieldValue = null;
				break;
			}

			// save notification
			msgMapper.setMsgById(RoleUtil.getCurrentUserID());
			msgMapper.setMsgByRole(RoleUtil.getCurrentUserSingleRoleString());
			// msgMapper.setMsgByRole(RoleUtil.getCurrentUseInfo().getAuthorities());
			existingProject.setMsgMapper(msgMapper);
			createNotification(existingProject);

			if (existingProject.getFeasibilityState().equals(Constants.PRE_SIGNING)) {// mail
				if (RoleUtil.isCurrentUserHasApproverRole()) {
					int type = RoleUtil.getCurrentUseInfo().getApproverType();
					subject = MessageConstants.MAIL_SUBJECT_QUERY;
					mailTo = existingProject.getCreatedBy();

					if (type == Constants.STATUS_APPROVER_1) {

						message = "Report: " + existingProject.getReportId() + " " + message;
						// ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).getEmail());
					} else if (type == Constants.STATUS_APPROVER_2) {

						message = "Report: " + existingProject.getReportId() + " " + message;
						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getEmail());
					}

				} else if (RoleUtil.isCurrentUserHasBDRole()) {
					message = "query answered";
					subject = MessageConstants.MAIL_SUBJECT_ANSWER;

					// pending
					mailTo = "";

				}
			} else if (existingProject.getFeasibilityState().equals(Constants.POST_SIGNING)) {

			}
			mailService.mailService(existingProject.getCreatedBy(), MessageConstants.MAIL_SUBJECT_QUERY, message,
					ccList);
			existingProject.setUpdatedDate(new Date().getTime());

			projectDetailsDao.save(existingProject);

		}

		return mapper;
	}

	public List<AssumptionsValues> getOccupancyForUpcomingCinemas(Optional<ProjectDetails> upcomingProjectDetails, List<Navision> lastQuarterNonCompDataFinal) { 
		List<AssumptionsValues> assumptionValues = new ArrayList<>();
		List<RevenueExpense> occupancy = projectRevenueService.getProjectUpcomingCinemasOccupancy(null,
				upcomingProjectDetails, lastQuarterNonCompDataFinal);

		occupancy.forEach(revenue -> {

			AssumptionsValues as = new AssumptionsValues();
			as.setFormatType(revenue.getCinemaFormatType());
			as.setCostType(Math.round(revenue.getOccupancy()));
			as.setAdmits(revenue.getAdmits());
			assumptionValues.add(as);

		});

		return assumptionValues;
	}

	@Override
	public Boolean updateProjectAndNotificationStatus(String projectId, int status) {

		try {
			// update project details
			Query query1 = new Query();
			query1.addCriteria(Criteria.where("_id").is(projectId));
			Update update1 = new Update();
			update1.set("status", status);
			mongoTemplate.updateMulti(query1, update1, ProjectDetails.class);

			// update notification
			Query query = new Query();
			query.addCriteria(Criteria.where("projectId").is(projectId));
			Update update = new Update();
			update.set("status", status);
			mongoTemplate.updateMulti(query, update, Notification.class);

			ProjectDetails existingProject = projectDetailsDao.findById(projectId).get();

			if (!ObjectUtils.isEmpty(existingProject)) {
				createNotification(existingProject);
			}

		} catch (Exception ex) {
			throw new FeasibilityException("Error in updating Project status: " + ex.getMessage());
		}

		return true;
	}

	public Double getTotalAdmits(Optional<ProjectDetails> upcomingCinemaProject) {
		double totalAdmits = 0;
		if (upcomingCinemaProject.isPresent()) {
			totalAdmits = projectRevenueService.getProjectTotalAdmitsDetails(null, upcomingCinemaProject);

		}
		return totalAdmits;
	}

	public Double getTotalOccupancy(Optional<ProjectDetails> upcomingProjectDetails) {
		double totalOccupancy = 0;
		try {

			if (upcomingProjectDetails.isPresent()) {
				List<FixedConstant> fixConstant = (List<FixedConstant>) fixedConstantDao.findAll();
				log.info("city " + fixConstant.get(0).getAvgShowPerDay().get("chennai"));
				/*
				 * double fixNoOfShows = !ObjectUtils.isEmpty(fixConstant) ? (double)
				 * fixConstant.get(0).getAvgShowPerDay()
				 * .get(upcomingProjectDetails.get().getProjectLocation().getCity()) : (double)
				 * 5;
				 */
				double fixNoOfShows = 0;
				if (!ObjectUtils.isEmpty(fixConstant)) {
					if (upcomingProjectDetails.get().getProjectLocation().getCity().equalsIgnoreCase("chennai"))
						fixNoOfShows = fixConstant.get(0).getAvgShowPerDay().get("chennai");
					else {
						fixNoOfShows = 5;
					}
				}

				double totalAdmits = projectRevenueService.getProjectTotalAdmitsDetails(null, upcomingProjectDetails);
				double totalCapacity = upcomingProjectDetails.get().getNoOfScreens() * (double) Constants.YEAR
						* fixNoOfShows;
				totalOccupancy = totalAdmits / totalCapacity;
			}
		} catch (Exception e) {
			log.error("error while fetching total occupancy ", e);
		}
		return totalOccupancy;
	}

	public Double getTotalAtp(Optional<ProjectDetails> upcomingProjectDetails) {
		double totalAtp = 0;
		List<Long> totalAtps = new ArrayList<>();
		try {
			if (upcomingProjectDetails.isPresent()) {

				List<AtpAssumptions> atp = upcomingProjectDetails.get().getAtp();
				atp.forEach(atps -> {
					AssumptionsValues normal = atps.getNormal();
					AssumptionsValues lounger = atps.getLounger();
					AssumptionsValues recliner = atps.getLounger();
					if (!ObjectUtils.isEmpty(normal) && !ObjectUtils.isEmpty(lounger)
							&& !ObjectUtils.isEmpty(recliner)) {
						totalAtps.add(normal.getCostType() + lounger.getCostType() + recliner.getCostType());
					}

				});
				double totalAdmits = projectRevenueService.getProjectTotalAdmitsDetails(null, upcomingProjectDetails);
				double totalAtpProject = totalAtps.stream().mapToLong(mapper -> mapper).sum();
				totalAtp = totalAdmits / totalAtpProject;

			}

		} catch (Exception e) {
			log.error("error while fetching total atp ", e);
		}
		return totalAtp;
	}

	public Double getTotalSph(Optional<ProjectDetails> upcomingProjectDetails) {
		double totalsph = 0;
		List<Long> totalSph = new ArrayList<>();
		try {
			if (upcomingProjectDetails.isPresent()) {

				List<AssumptionsValues> atp = upcomingProjectDetails.get().getSph();
				atp.forEach(sph -> {
					totalSph.add(sph.getCostType());
				});
				double totalAdmits = projectRevenueService.getProjectTotalAdmitsDetails(null, upcomingProjectDetails);
				double totalSphProject = totalSph.stream().mapToLong(mapper -> mapper).sum();
				totalsph = totalAdmits / totalSphProject;

			}

		} catch (Exception e) {
			log.error("error while fetching total sph ", e);
		}
		return totalsph;
	}

	@Override
	public ProjectDetails getLatestProject(String reportType, String reportId) {

		ProjectDetails latestProjectDetails = null;
		if (ObjectUtils.isEmpty(reportId) || reportId.equals("undefined")) {
			latestProjectDetails = projectDetailsDao.findTopByOrderByReportIdVersionDesc();
		} else {
			latestProjectDetails = projectDetailsDao.findByReportId(reportId);

			SortOperation sort = Aggregation.sort(Sort.Direction.DESC, "createdDate");
			MatchOperation matchStage = Aggregation
					.match(Criteria.where("reportIdVersion").is(latestProjectDetails.getReportIdVersion()));

			Aggregation aggregation = Aggregation.newAggregation(matchStage, sort);

			AggregationResults<ProjectDetails> output = mongoTemplate.aggregate(aggregation, "project_details",
					ProjectDetails.class);
			List<ProjectDetails> list = output.getMappedResults();

			if (list.size() > 0)
				latestProjectDetails = list.get(0);

		}

		latestProjectDetails = generateReportId(reportType, latestProjectDetails);

		return latestProjectDetails;

	}

	public ProjectDetails generateReportId(String reportType, ProjectDetails latestProjectDetails) {
		String reportId = "default";
		Integer reportIdVersion = 1;
		Integer reportIdSubVersion = 1;

		// setup report id version
		if (latestProjectDetails != null && latestProjectDetails.getReportIdVersion() != null) {

			if (reportType.equals(Constants.REPORT_TYPE_NEW)) {
				reportIdVersion =  latestProjectDetails.getReportIdVersion() + 1;
				reportId = "N-" + reportIdVersion + "." + reportIdSubVersion;
			} else if (reportType.equals(Constants.REPORT_TYPE_EXISTING)) {
				reportIdVersion = latestProjectDetails.getReportIdVersion();
				reportIdSubVersion =  latestProjectDetails.getReportIdSubVersion() + 1;
				reportId = "N-" + reportIdVersion + "." + reportIdSubVersion;
			}

		} else if (latestProjectDetails == null) {
			latestProjectDetails = new ProjectDetails();
			reportId = "N-" + reportIdVersion + "." + reportIdSubVersion;
		}

		else {
			// for existing data which has old reportId

			if (reportType.equals(Constants.REPORT_TYPE_NEW)) {
				// reportIdVersion = (byte) (latestProjectDetails.getReportIdVersion() + 1);
				latestProjectDetails = new ProjectDetails();
				reportId = "N-" + reportIdVersion + "." + reportIdSubVersion;
			} else if (reportType.equals(Constants.REPORT_TYPE_EXISTING) && latestProjectDetails != null
					&& latestProjectDetails.getReportIdVersion() == null) {

				reportIdSubVersion++;
				reportId = latestProjectDetails.getReportId() + "." + reportIdSubVersion;

			}

		}

		latestProjectDetails.setReportId(reportId);
		latestProjectDetails.setReportIdVersion(reportIdVersion);
		latestProjectDetails.setReportIdSubVersion(reportIdSubVersion);

		return latestProjectDetails;
	}

	@Override
	public List<ProjectDetails> fetchArchievedProjectDetails(Pageable pageable, List<Filter> filters) {
		try {
			Query query = new Query();
			query.addCriteria(new Criteria().orOperator(Criteria.where("status").is(Constants.STATUS_ACCEPTED),
					Criteria.where("status").is(Constants.STATUS_ARCHIVED)));
			List<ProjectDetails> surveyResultList = mongoTemplate.find(query, ProjectDetails.class);
			return surveyResultList;
		} catch (Exception e) {
			log.error("error while fetching Project Details.", e);
			throw new FeasibilityException("error while fetching Project Details");
		}
	}

	void compitionOrCinemaMaster(ProjectDetails projectDetails) {

		MasterTemplateByDistance masterDetails = new MasterTemplateByDistance();
		masterDetails.setProjectId(projectDetails.getId());
		masterDetails.setTemplateType(Constants.COMPITION);
		masterDetails.setCompitionMaster(getCompitionMasterWithinTenKilometer(projectDetails));
		/*
		 * masterDetails.setCinemaMaster(getCinemaMasterWithinTenKilometer(
		 * projectDetails));
		 */

		masterTemplatebyDistanceDao.save(masterDetails);
	}

	public List<CompitionMaster> getCompitionMasterWithinTenKilometer(ProjectDetails projectDetails) {
		List<CompitionMaster> updatedCompition = new ArrayList<>();

		List<CompitionMaster> compitionMaster = (List<CompitionMaster>) compitionMasterDao.findAll();

		try {
			double upcomingProjectLogitude = projectDetails.getProjectLocation().getLongitude();
			double upcomingProjectLatitude = projectDetails.getProjectLocation().getLatitude();

			compitionMaster.parallelStream().distinct().forEach(cinema -> {
				String cinemaName = cinema.getCompititionName();
				try {
					if (!ObjectUtils.isEmpty(cinemaName)) {

						Map<String, Double> latLng = getAddressByLatLong(cinemaName);

						double distanceBetween = MathUtil.distanceInKm(upcomingProjectLatitude, upcomingProjectLogitude,
								latLng.get(Constants.LATITUDE), latLng.get(Constants.LONGITUDE));
						cinema.setDistance(distanceBetween);

						if (distanceBetween <= 10) {
							updatedCompition.add(cinema);
						}

					}

				} catch (IOException | ParseException e) {
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			log.error("error while fetching compition master within 10 kilomter...");
		}
		return updatedCompition;
	}

	public List<CinemaMasterSql> getCinemaMasterWithinTenKilometer(ProjectDetails projectDetails) {
		List<CinemaMasterSql> updatedCinema = new ArrayList<>();

		List<CinemaMasterSql> cinemaMaster = (List<CinemaMasterSql>) cinemaMasterSqlDao.findAll();

		try {
			double upcomingProjectLogitude = projectDetails.getProjectLocation().getLongitude();
			double upcomingProjectLatitude = projectDetails.getProjectLocation().getLatitude();

			cinemaMaster.parallelStream().distinct().forEach(cinema -> {
				String cinemaName = cinema.getCinema_Name();
				try {
					if (!ObjectUtils.isEmpty(cinemaName)) {

						Map<String, Double> latLng = getAddressByLatLong(cinemaName);

						double distanceBetween = MathUtil.distanceInKm(upcomingProjectLatitude, upcomingProjectLogitude,
								latLng.get(Constants.LATITUDE), latLng.get(Constants.LONGITUDE));
						cinema.setDistance(distanceBetween);

						if (distanceBetween <= 10) {
							updatedCinema.add(cinema);
						}
						/* updatedCinema.add(cinema); */
					}

				} catch (IOException | ParseException e) {
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			log.error("error while fetching compition master within 10 kilomter...");
		}
		return updatedCinema;
	}

	public Map<String, Double> getAddressByLatLong(String fullAddress) throws IOException, ParseException {
		Map<String, Double> map = new HashMap<>();
		double lat = 0;
		double lng = 0;
		try {
			synchronized (this) {
				URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json" + "?address="
						+ URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false" + "&" + "key=" + googleKey + " ");
				URLConnection conn = url.openConnection();
				ByteArrayOutputStream output = new ByteArrayOutputStream(1024);

				IOUtils.copy(conn.getInputStream(), output);
				output.close();

				ObjectMapper mapper = new ObjectMapper();
				JsonNode array = mapper.readValue(output.toString(), JsonNode.class);

				JsonNode object = array.get("results").get(0);

				JsonNode geometry = object.get("geometry");
				JsonNode location = geometry.get("location");

				lat = location.get("lat").asDouble();
				lng = location.get("lng").asDouble();

			}
			map.put(Constants.LATITUDE, lat);
			map.put(Constants.LONGITUDE, lng);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching google api geocoding api");
		}
		return map;
	}

	@Override
	public List<ProjectDetails> getProjectGreaterThanEqualStatusAndEqualFeasibilityState(Integer status,
			String feasibilityState) {
		return projectDetailsDao.getProjectGreaterThanEqualStatusAndEqualFeasibilityState(status, feasibilityState);
	}

	@Override
	public ProjectDetails updateProjectDetailsById(String projectId) {
		ProjectDetails project = null;
		try {
			updateProjectAndNotificationStatus(projectId, Constants.STATUS_DELETE);
		} catch (Exception e) {
			log.error("error while deleting project details..");
		}

		return project;
	}

	@Override
	public List<ProjectDetails> fetchAcessOldReports(Pageable pageable, List<Filter> filters) {
		try {
			Query query = QueryBuilder.createQuery(filters, pageable);
			query.getSortObject();
			List<Integer> status = new ArrayList<>();

			status.add(Constants.STATUS_ACCEPTED);
			status.add(Constants.STATUS_ARCHIVED);
			status.add(Constants.POST_STATUS_ARCHIVED);
			status.add(Constants.PRE_HAND_STATUS_ARCHIVED);
			List<ProjectDetails> resultList = null;
			if (RoleUtil.isCurrentUserHasBDRole()) {
				resultList = projectDetailsDao.findByStatusInAndProjectBdId(status,
						RoleUtil.getCurrentUseInfo().getId());

			} else if (RoleUtil.isCurrentUserHasApproverRole()) {
				resultList = projectDetailsDao.findByStatusIn(status);

			} else {
				query.addCriteria(Criteria.where("status").in(status));
			}

			if (RoleUtil.isCurrentUserHasApproverRole()) {
				int type = RoleUtil.getCurrentUseInfo().getApproverType();
				int preHandoverApproverType = RoleUtil.getCurrentUseInfo().getPreHandOverApproverType();
				List<ProjectDetails> approverList = new ArrayList<>();

				if (!ObjectUtils.isEmpty(resultList)) {
					for (int i = 0; i < resultList.size(); i++) {
						User user = null;
						if (!ObjectUtils.isEmpty(resultList.get(i).getRejectedBy())) {
							String rejectedBy = resultList.get(i).getRejectedBy();
							user = userDao.findOneById(rejectedBy);
						}
						if (Constants.PRE_SIGNING.equals(resultList.get(i).getFeasibilityState())
								&& (Constants.STATUS_ARCHIVED == resultList.get(i).getStatus()
										|| Constants.STATUS_ACCEPTED == resultList.get(i).getStatus())) {

							if (resultList.get(i).getStatus() == Constants.STATUS_ARCHIVED) {
								if (user != null && user.getApproverType() < type) {
									continue;
								} else {
									approverList.add(resultList.get(i));
									continue;
								}

							} else {
								if (type == Constants.STATUS_APPROVER_1 || type == Constants.STATUS_APPROVER_2) {
									approverList.add(resultList.get(i));
								}
							}

						}

						if (Constants.POST_SIGNING.equals(resultList.get(i).getFeasibilityState())
								&& (Constants.POST_STATUS_ARCHIVED == resultList.get(i).getStatus()
										|| Constants.STATUS_ACCEPTED == resultList.get(i).getStatus())) {

							if (resultList.get(i).getStatus() == Constants.POST_STATUS_ARCHIVED) {
								if (user != null && user.getApproverType() < type) {
									continue;
								} else {
									approverList.add(resultList.get(i));
									continue;
								}

							} else {
								if (type == Constants.STATUS_APPROVER_1 || type == Constants.STATUS_APPROVER_2
										|| type == Constants.STATUS_APPROVER_3 || type == Constants.STATUS_APPROVER_4) {
									approverList.add(resultList.get(i));
								}

							}

						}

						if (Constants.PRE_HANDOVER.equals(resultList.get(i).getFeasibilityState())
								&& (Constants.PRE_HAND_STATUS_ARCHIVED == resultList.get(i).getStatus()
										|| Constants.STATUS_ACCEPTED == resultList.get(i).getStatus())) {
							if (resultList.get(i).getStatus() == Constants.PRE_HAND_STATUS_ARCHIVED) {
								if (user != null && user.getPreHandOverApproverType() < preHandoverApproverType) {
									continue;
								} else {
									approverList.add(resultList.get(i));
									continue;
								}

							}

							else {
								if (preHandoverApproverType == Constants.STATUS_APPROVER_1
										|| preHandoverApproverType == Constants.STATUS_APPROVER_2
										|| preHandoverApproverType == Constants.STATUS_APPROVER_3
										|| preHandoverApproverType == Constants.STATUS_APPROVER_4
										|| preHandoverApproverType == Constants.STATUS_APPROVER_5
										|| preHandoverApproverType == Constants.STATUS_APPROVER_6) {
									approverList.add(resultList.get(i));
								}
							}
						}

					}

				}

				resultList.clear();
				resultList = approverList;

			}

			if (RoleUtil.isCurrentUserHasBDRole()) {

				List<ProjectDetails> bdList = new ArrayList<>();

				if (!ObjectUtils.isEmpty(resultList)) {
					for (int i = 0; i < resultList.size(); i++) {
						if (resultList.get(i).getFeasibilityState().equals(Constants.PRE_SIGNING)
								&& (resultList.get(i).getStatus() == Constants.STATUS_ARCHIVED
										|| resultList.get(i).getStatus() == Constants.STATUS_ACCEPTED)) {
							bdList.add(resultList.get(i));
						}

						if (resultList.get(i).getFeasibilityState().equals(Constants.POST_SIGNING)
								&& (resultList.get(i).getStatus() == Constants.POST_STATUS_ARCHIVED
										|| resultList.get(i).getStatus() == Constants.STATUS_ACCEPTED)) {
							bdList.add(resultList.get(i));
						}

						if (resultList.get(i).getFeasibilityState().equals(Constants.PRE_HANDOVER)
								&& (resultList.get(i).getStatus() == Constants.PRE_HAND_STATUS_ARCHIVED
										|| resultList.get(i).getStatus() == Constants.STATUS_ACCEPTED)) {
							bdList.add(resultList.get(i));
						}

					}
				}

				resultList.clear();
				resultList = bdList;

			}

			resultList.sort((obj1, obj2) -> {
				return (obj1.getCreatedDate() > obj2.getCreatedDate()) ? 1
						: (obj1.getCreatedDate() < obj2.getCreatedDate()) ? 1 : 0;
			});

			return resultList;
		} catch (Exception e) {
			log.error("error while fetching old reports..", e);
			throw new FeasibilityException("error while fetching old reports..");
		}
	}

	@Override
	public ProjectDetails updateProjectOperationFlags(String projectId) {

		/*
		 * try {
		 * 
		 * // update project details Query query1 = new Query();
		 * query1.addCriteria(Criteria.where("_id").is(projectId)); Update update1 = new
		 * Update(); update1.set("isProjectSubmit", ); "isProjectSubmit" : false,
		 * "isOperationSubmit" : false,
		 * 
		 * 
		 * mongoTemplate.updateMulti(query1, update1, ProjectDetails.class);
		 * 
		 * List<ProjectDetails> surveyResultList = mongoTemplate.find(query,
		 * ProjectDetails.class);
		 * 
		 * 
		 * surveyResultList.sort((obj1, obj2) -> { return (obj1.getCreatedDate() >
		 * obj2.getCreatedDate()) ? 1 : (obj1.getCreatedDate() < obj2.getCreatedDate())
		 * ? 1 : 0; });
		 * 
		 * 
		 * return surveyResultList; } catch (Exception e) {
		 * log.error("error while fetching old reports..", e); throw new
		 * FeasibilityException("error while fetching old reports.."); }
		 */

		return null;
	}

	@Override
	public ProjectDetails submitOperatingAssumption(ProjectDetails projectDetails) {
		try {
			ProjectDetails entity = get(projectDetails.getId());
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(entity.getId()));
			Update update = new Update().set("reportStatus", projectDetails.getReportStatus());

			createNotification(entity);

			mongoTemplate.updateFirst(query, update, ProjectDetails.class);
		} catch (Exception ex) {
			log.error("error while updating report status");
		}

		return projectDetails;
	}

	@Override
	public ProjectDetails rejectProjectDetails(MsgMapper msgMapper) {

		if (ObjectUtils.isEmpty(msgMapper) || ObjectUtils.isEmpty(msgMapper.getProjectId())) {
			log.error("Empty survey can not assined.");
			throw new FeasibilityException("Empty Project Details can not assined.");
		}
		try {

			ProjectDetails existingProject = projectDetailsDao.findById(msgMapper.getProjectId()).get();
			ProjectDetails details = null;
			if (!ObjectUtils.isEmpty(existingProject)) {
				ProjectDetails projectDetails = this.rejectProject(existingProject.getId());
				projectDetails.setRejectReasonMsg(msgMapper.getMsg());
				if (Constants.PRE_HANDOVER.equalsIgnoreCase(projectDetails.getFeasibilityState())) {
					projectDetails.setIsRejected(true);
				}
				// projectDetails.setIsSendForOpsAndProject(false);
				// existingProject.setOperationReasonMsg("");
				// existingProject.setProjectReasonMsg("");
				/*
				 * projectDetails.setIsProjectSubmit(false);
				 * projectDetails.setIsOperationSubmit(false);
				 */
				projectDetails.setUpdatedDate(new Date().getTime());

				details = projectDetailsDao.save(projectDetails);
			}
			return details;

		} catch (Exception e) {
			log.debug("Exception in saving step 4.");
			e.printStackTrace();
			throw new FeasibilityException("Exception in reject");
		}

	}

	@Override
	public ProjectDetails updateProject(String projectId) {
		if (ObjectUtils.isEmpty(projectId)) {
			throw new FeasibilityException("Project id cannot be null or empty");
		}

		ProjectDetails projectDetails = null;
		try {
			ProjectDetails existingProject = projectDetailsDao.findById(projectId).get();
			if (!ObjectUtils.isEmpty(existingProject)) {
				existingProject.setStatus(Constants.STEP_1);
				existingProject.setRejectedBy(null);
				existingProject.setUpdatedDate(new Date().getTime());

				projectDetails = projectDetailsDao.save(existingProject);

				projectDetails.setMsgMapper(null);
				createNotification(projectDetails);
			}

		} catch (Exception e) {
			throw new FeasibilityException("Error while saving reject project details");
		}

		return projectDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CinemaMaster> getAllCinemaMasterDetails() {
		List<CinemaMaster> updatedCinemaMasters = new ArrayList<>();
		try {
			List<CinemaMaster> cinemaMasters = (List<CinemaMaster>) cinemaMasterDao.findAll();
			for(CinemaMaster cinema : cinemaMasters) {
				cinema.setCinema_name(cinema.getCinema_name().replaceAll("\\W+", " "));
				updatedCinemaMasters.add(cinema);
			}
			Collections.sort(updatedCinemaMasters, new Comparator<CinemaMaster>() {
				@Override
				public int compare(CinemaMaster o1, CinemaMaster o2) {
					return o1.getCinema_name().compareTo(o2.getCinema_name());
				}
			});
			return updatedCinemaMasters;
		} catch (Exception e) {
			log.debug("Exception while fetching cinema master details");
			e.printStackTrace();
			throw new FeasibilityException("Exception cinema master details");
		}

	}

	@Override
	public List<ProjectDetails> getExistingProject() {
		List<ProjectDetails> projectDetails = new ArrayList<>();
		try {

			GroupOperation group = Aggregation.group(Fields.fields("reportIdVersion"));
			/*
			 * GroupOperation group =
			 * Aggregation.group("reportIdVersion").count().as("reportId");
			 */
			MatchOperation matchStage = Aggregation.match(Criteria.where("feasibilityState").is(Constants.PRE_SIGNING)
					.and("projectBdId").is(RoleUtil.getCurrentUserID()));

			Aggregation aggregation = Aggregation.newAggregation(matchStage, group);
			AggregationResults<ProjectDetails> output = mongoTemplate.aggregate(aggregation, "project_details",
					ProjectDetails.class);
			List<ProjectDetails> resultList = output.getMappedResults();

			if (!ObjectUtils.isEmpty(resultList)) {
				for (int i = 0; i < resultList.size(); i++) {

					Integer reportIdVersion = Integer.parseInt(resultList.get(i).getId());

					SortOperation sort = Aggregation.sort(Sort.Direction.DESC, "reportIdSubVersion");
					MatchOperation match = Aggregation.match(Criteria.where("reportIdVersion").is(reportIdVersion)
							.and("projectBdId").is(RoleUtil.getCurrentUserID()));

					Aggregation agg = Aggregation.newAggregation(match, sort);

					AggregationResults<ProjectDetails> result = mongoTemplate.aggregate(agg, "project_details",
							ProjectDetails.class);
					List<ProjectDetails> list = result.getMappedResults();

					for (int j = 0; j < list.size(); j++) {
						ProjectDetails details = list.get(j);
						if (details.getStatus() == Constants.STATUS_DELETE
								&& (details.getFeasibilityState().equals(Constants.PRE_SIGNING)
										|| details.getFeasibilityState().equals(Constants.POST_SIGNING)
										|| details.getFeasibilityState().equals(Constants.PRE_HANDOVER))) {
							continue;
						} else {

							if ((details.getStatus() == Constants.STATUS_ARCHIVED
									&& details.getFeasibilityState().equals(Constants.PRE_SIGNING))
									|| (details.getStatus() == Constants.POST_STATUS_ARCHIVED
											&& details.getFeasibilityState().equals(Constants.POST_SIGNING))
									|| (details.getStatus() == Constants.PRE_HAND_STATUS_ARCHIVED
											&& details.getFeasibilityState().equals(Constants.PRE_HANDOVER))) {
								break;
							}
							if (details.getStatus() != Constants.STATUS_ACCEPTED
									&& details.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
								break;
							}
							if (details.getStatus() == Constants.STATUS_ACCEPTED
									&& details.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
								break;
							} else if (details.getStatus() != Constants.STATUS_ACCEPTED
									&& details.getFeasibilityState().equals(Constants.POST_SIGNING)) {
								break;
							} else if (details.getStatus() == Constants.STATUS_ACCEPTED
									&& details.getFeasibilityState().equals(Constants.POST_SIGNING)) {
								break;
							} else if (details.getStatus() != Constants.STATUS_ACCEPTED
									&& details.getFeasibilityState().equals(Constants.PRE_SIGNING)) {
								break;
							} else if (details.getStatus() == Constants.STATUS_ACCEPTED
									&& details.getFeasibilityState().equals(Constants.PRE_SIGNING)) {
								projectDetails.add(details);
								break;
							}

						}

					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching existing projectDetails...");
		}
		return projectDetails;
	}

	@Override
	public ProjectDetails saveCloseStep5(ProjectDetails entity) {

		if (ObjectUtils.isEmpty(entity) || ObjectUtils.isEmpty(entity.getId())) {
			log.error("Empty survey can not assined.");
			throw new FeasibilityException("Empty Project Details can not assined.");
		}
		try {

			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(entity.getId());
			List<CinemaFormat> cinemaformats = new ArrayList<>();
			List<CinemaFormat> newCinemaFormat = entity.getCinemaFormat();
			List<CinemaFormat> screenDimensions = new ArrayList<>();

			if (!ObjectUtils.isEmpty(newCinemaFormat)) {
				newCinemaFormat.forEach(x -> {
					CinemaFormat cinemaFormat = new CinemaFormat();
					CinemaFormat screenDimension = new CinemaFormat();

					screenDimension.setFormatType(x.getFormatType());

					screenDimensions.add(screenDimension);
					cinemaFormat.setFormatType(x.getFormatType());
					cinemaFormat.setNormal(x.getNormal());
					cinemaFormat.setRecliners(x.getRecliners());
					cinemaFormat.setLounger(x.getLounger());
					cinemaFormat.setTotal(x.getTotal());
					cinemaformats.add(cinemaFormat);

				});
			}

			List<CinemaFormat> newscreenDimension = entity.getSeatDimension();
			for (int i = 0; i < cinemaformats.size(); i++) {
				CinemaFormat format = cinemaformats.get(i);
				CinemaFormat screenDim = newscreenDimension.get(i);
				screenDim.setFormatType(format.getFormatType());

			}
			projectDetails.get().setSeatDimension(newscreenDimension);
			/*
			 * cinemaformats.forEach(cinemaFormat -> { newscreenDimension.forEach(seatMatrix
			 * -> { screenDimensions.forEach(screen -> {
			 * screen.setNormalSeatDimension(seatMatrix.getNormalSeatDimension());
			 * screen.setReclinerSeatDimension(seatMatrix.getReclinerSeatDimension());
			 * screen.setLoungerSeatDimension(seatMatrix.getLoungerSeatDimension()); }); });
			 * });
			 */

			projectDetails.get().setNormalsValSum(entity.getNormalsValSum());
			projectDetails.get().setReclinerValSum(entity.getReclinerValSum());
			projectDetails.get().setLoungersValSum(entity.getLoungersValSum());
			projectDetails.get().setTotalColumnValSum(entity.getTotalColumnValSum());
			projectDetails.get().setNoOfScreens(entity.getNoOfScreens());
			projectDetails.get().setCinemaCategory(entity.getCinemaCategory());
			// projectDetails.get().setSeatDimension(screenDimensions);

//			if (!ObjectUtils.isEmpty(entity.getFileURLStep5())) {
//				projectDetails.get().setFileURLStep5(entity.getFileURLStep5());
//			}
			
			projectDetails.get().setMultipleFileURLStep5(entity.getMultipleFileURLStep5()); 

			projectDetails.get().setCinemaFormat(cinemaformats);
			projectDetails.get().setStatus(Constants.STEP_5);
			projectDetails.get().createdBy(RoleUtil.getCurrentUserID());
			projectDetails.get().setUpdatedDate(new Date().getTime());

			ProjectDetails savedProjectDetails = projectDetailsDao.save(projectDetails.get());

			createNotification(savedProjectDetails);

			return savedProjectDetails;
		} catch (Exception e) {
			e.printStackTrace();
			log.debug("Exception in saving step 5.");
			throw new FeasibilityException("Exception in saving step 5");
		}

	}

	@Override
	public ProjectDetails getPreSigningProject(Integer reportIdVersion) {
		ProjectDetails projectDetails = new ProjectDetails();
		try {

			MatchOperation matchStage = Aggregation.match(Criteria.where("feasibilityState").is(Constants.PRE_SIGNING)
					.and("reportIdVersion").is(reportIdVersion).and("status").is(Constants.STATUS_ACCEPTED));

			SortOperation sort = Aggregation.sort(Sort.Direction.DESC, "reportIdSubVersion");

			Aggregation aggregation = Aggregation.newAggregation(matchStage, sort);

			AggregationResults<ProjectDetails> output = mongoTemplate.aggregate(aggregation, "project_details",
					ProjectDetails.class);
			List<ProjectDetails> resultList = output.getMappedResults();
			if (!ObjectUtils.isEmpty(resultList)) {
				projectDetails = resultList.get(0);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching existing projectDetails...");
		}
		return projectDetails;
	}

	public void emptyQueryDataForBDApprover(ProjectDetails projectDetails) {
		if (!ObjectUtils.isEmpty(projectDetails)) {
			if (projectDetails.getFinalSummary() != null && projectDetails.getFinalSummary().getTotalAdmits() != null) {
				projectDetails.getFinalSummary().getTotalAdmits().setQuery(null);
				projectDetails.getFinalSummary().getTotalAdmits().setLastQueryUserId(null);
			}
			if (projectDetails.getFinalSummary() != null && projectDetails.getFinalSummary().getOccupancy() != null) {
				projectDetails.getFinalSummary().getOccupancy().setQuery(null);
				projectDetails.getFinalSummary().getOccupancy().setLastQueryUserId(null);
			}
			if (projectDetails.getFinalSummary() != null && projectDetails.getFinalSummary().getAtp() != null) {
				projectDetails.getFinalSummary().getAtp().setQuery(null);
				projectDetails.getFinalSummary().getAtp().setLastQueryUserId(null);
			}
			if (projectDetails.getFinalSummary() != null && projectDetails.getFinalSummary().getSph() != null) {
				projectDetails.getFinalSummary().getSph().setQuery(null);
				projectDetails.getFinalSummary().getSph().setLastQueryUserId(null);
			}
			if (projectDetails.getFinalSummary() != null && projectDetails.getFinalSummary().getAdRevenue() != null) {
				projectDetails.getFinalSummary().getAdRevenue().setQuery(null);
				projectDetails.getFinalSummary().getAdRevenue().setLastQueryUserId(null);
			}

			if (projectDetails.getFinalSummary() != null
					&& projectDetails.getFinalSummary().getPersonalExpense() != null) {
				projectDetails.getFinalSummary().getPersonalExpense().setQuery(null);
				projectDetails.getFinalSummary().getPersonalExpense().setLastQueryUserId(null);
			}

			if (projectDetails.getFinalSummary() != null
					&& projectDetails.getFinalSummary().getElectricityWaterExpanse() != null) {
				projectDetails.getFinalSummary().getElectricityWaterExpanse().setQuery(null);
				projectDetails.getFinalSummary().getElectricityWaterExpanse().setLastQueryUserId(null);
			}

			if (projectDetails.getFinalSummary() != null
					&& projectDetails.getFinalSummary().getTotalRmExpenses() != null) {
				projectDetails.getFinalSummary().getTotalRmExpenses().setQuery(null);
				projectDetails.getFinalSummary().getTotalRmExpenses().setLastQueryUserId(null);
			}

			if (projectDetails.getFinalSummary() != null
					&& projectDetails.getFinalSummary().getTotalProjectCost() != null) {
				projectDetails.getFinalSummary().getTotalProjectCost().setQuery(null);
				projectDetails.getFinalSummary().getTotalProjectCost().setLastQueryUserId(null);
			}

		}

	}

	@Override
	public List<ProjectDetails> fetchAllProjectDetails() {
		Query query = QueryBuilder.createQuery(null, null);
		query.getSortObject();
		List<Integer> status = new ArrayList<>();
		status.add(Constants.STATUS_ACCEPTED);
		status.add(Constants.STATUS_ARCHIVED);
		status.add(Constants.POST_STATUS_ARCHIVED);
		status.add(Constants.PRE_HAND_STATUS_ARCHIVED);

		query.addCriteria(Criteria.where("status").in(status));

		List<ProjectDetails> surveyResultList = mongoTemplate.find(query, ProjectDetails.class);

		return surveyResultList;
	}

	@Override
	public FixedConstant getRevenueAssumptionConstant(String projectId, String projectLocation) {
		
		List<FixedConstant> list = (List<FixedConstant>) (fixedConstantDao.findAll());
		ProjectDetails existingProject = projectDetailsDao.findById(projectId).get();
		
		FixedConstant fixedConstant = new FixedConstant();
		
		if(!ObjectUtils.isEmpty(list)) {
			fixedConstant.setAdminLeasePeriod(list.get(0).getAdminLeasePeriod());
			fixedConstant.setProjectLocation(projectLocation);
			fixedConstant.setAdminRevenueAssumptionsGrowthRate(list.get(0).getRevenueAssumptionsGrowthRate());
			
			if(!ObjectUtils.isEmpty(existingProject) && !ObjectUtils.isEmpty(existingProject.getRevenueAssumptionsGrowthRate())){   
				fixedConstant.setRevenueAssumptionsGrowthRate(existingProject.getRevenueAssumptionsGrowthRate());
			}else {
				fixedConstant.setRevenueAssumptionsGrowthRate(list.get(0).getRevenueAssumptionsGrowthRate());
			}
			
			if(!ObjectUtils.isEmpty(existingProject) && !ObjectUtils.isEmpty(existingProject.getCostAssumptionsGrowthRate())){   
				fixedConstant.setCostAssumptionsGrowthRate(existingProject.getCostAssumptionsGrowthRate());
			}else {
				fixedConstant.setCostAssumptionsGrowthRate(list.get(0).getCostAssumptionsGrowthRate());
			}
			
			if(!ObjectUtils.isEmpty(existingProject) && !ObjectUtils.isEmpty(existingProject.getYearOneFootfallAssumedLowerBy())
					&& !ObjectUtils.isEmpty(existingProject.getYearOneAdIncomeLowerByPercent()) ) {
				fixedConstant.setYearOneFootfallAssumedLowerBy(existingProject.getYearOneFootfallAssumedLowerBy());
				fixedConstant.setYearOneAdIncomeLowerByPercent(existingProject.getYearOneAdIncomeLowerByPercent());
			}else {
				fixedConstant.setYearOneFootfallAssumedLowerBy(list.get(0).getYearOneFootfallAssumedLowerBy());
				fixedConstant.setYearOneAdIncomeLowerByPercent(list.get(0).getYearOneAdIncomeLowerByPercent());
			}
		}
		List<AtpCapDetials> atpCapDetails = list.get(0).getAtpCapDetails().stream()
				.filter(p -> p.getState().equalsIgnoreCase(projectLocation)).collect(Collectors.toList());
		
		if(!ObjectUtils.isEmpty(atpCapDetails)) {
			Map<String,Map<String,Double>> atpDetails = atpCapDetails.get(0).getAtpDetails();
			
			atpDetails.forEach((key, value)->{
				fixedConstant.setAtpGrowthRateNormalYearOne(value.get("normal"));
				fixedConstant.setAtpGrowthRateReclinerYearOne(value.get("recliners"));
				fixedConstant.setAtpGrowthRateLoungerYearOne(value.get("lounger"));
			});
		}
					
		return fixedConstant;
	}

	@Override
	public ProjectDetails saveExecutiveNote(ProjectDetails entity) {
		if (ObjectUtils.isEmpty(entity) || ObjectUtils.isEmpty(entity.getId())) {
			log.error("Empty survey can not assined.");
			throw new FeasibilityException("Empty Project Details can not assined.");
		}
		try {
			Map<String, String> executeNoteMap = new HashMap<>();
			FeasibilityUser user = RoleUtil.getCurrentUseInfo();
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(entity.getId());
			projectDetails.get().setUpdatedDate(new Date().getTime());
//			projectDetails.get().setExecutiveNote(entity.getExecutiveNote());
			executeNoteMap.put(user.getDesignation()+ " Comment", entity.getExecutiveNote());
			
			if(!ObjectUtils.isEmpty(projectDetails.get().getExecutiveNoteMap())) {
				executeNoteMap = projectDetails.get().getExecutiveNoteMap();
				executeNoteMap.put(user.getDesignation()+ " Comment",entity.getExecutiveNote());
			}else {
				projectDetails.get().setExecutiveNoteMap(executeNoteMap);
			}
		
			ProjectDetails savedProjectDetails = projectDetailsDao.save(projectDetails.get());
			return savedProjectDetails;
			
		} catch (Exception e) {
			e.printStackTrace();
			log.debug("Exception in saving executive note.");
			throw new FeasibilityException("Exception in saving executive note.");
		}
	}

	public List<ProjectDetails> fetchAllExistingProjectDetailsForCopy(Pageable pageable, List<Filter> filters) {
		try {
			Query query = new Query();
			query.getSortObject();
			List<Integer> status = new ArrayList<>();
			List<ProjectDetails> surveyResultList = null;
			status.add(Integer.valueOf(200));
			status.add(Integer.valueOf(410));
			status.add(Integer.valueOf(210));
			status.add(Integer.valueOf(310));
			status.add(Integer.valueOf(91));
			status.add(Integer.valueOf(92));
			status.add(Integer.valueOf(93));
			status.add(Integer.valueOf(94));
			status.add(Integer.valueOf(95));
			status.add(Integer.valueOf(96));
			if (RoleUtil.isCurrentUserHasBDRole()) {
				query.addCriteria((CriteriaDefinition) Criteria.where("status").in(status).and("projectBdId")
						.is(RoleUtil.getCurrentUseInfo().getId()));
			} else {
				query.addCriteria((CriteriaDefinition) Criteria.where("status").not().in(status));
			}
			if (!RoleUtil.isCurrentUserHasViewerRole())
				surveyResultList = this.mongoTemplate.find(query, ProjectDetails.class);

			surveyResultList
					.sort((obj1, obj2) -> (obj1.getCreatedDate().longValue() > obj2.getCreatedDate().longValue()) ? 1
							: ((obj1.getCreatedDate().longValue() < obj2.getCreatedDate().longValue()) ? 1 : 0));
			return surveyResultList;
		} catch (Exception e) {
			log.error("error while fetching Project Details.", e);
			throw new FeasibilityException("error while fetching Project Details");
		}
	}

	public Boolean copyFromExistingReport(String projectId) {
		Optional<ProjectDetails> projDetails = null;
		ProjectDetails existingProj = null;
		ProjectDetails newProj = null;
		try {
			projDetails = this.projectDetailsDao.findById(projectId);
			if (projDetails == null) {
				log.info("error occured while fetching projectDetails for copy");
				return Boolean.valueOf(false);
			}
			existingProj = projDetails.get();
			newProj = getLatestProject("new", null);
			existingProj.setFeasibilityState("pre_signing");
			existingProj.setReportType("new");
			existingProj.setReportId(newProj.getReportId());
			existingProj.setReportIdVersion(newProj.getReportIdVersion());
			existingProj.setReportIdSubVersion(newProj.getReportIdSubVersion());
			existingProj.setDateOfSendForApproval(null);
			existingProj.setStatus(91);
			existingProj.createdBy(RoleUtil.getCurrentUserID());
			existingProj.setApproverId(null);
			existingProj.setMsgMapper(null);
			emptyQueryDataForBDApprover(newProj);
			existingProj.setCreatedDate(Long.valueOf((new Date()).getTime()));
			existingProj.setIsOperationSubmit(Boolean.valueOf(false));
			existingProj.setIsProjectSubmit(Boolean.valueOf(false));
			existingProj.setRejectedBy(null);
			existingProj.setRejectReasonMsg(null);
//			existingProj.setExecutivenote(null);
//			existingProj.setApprovehistory(null);
			ProjectCostSummary projectCostSummary = this.projectCostSummaryDao.findByParentId(existingProj.getId());
			if (projectCostSummary != null)
				projectCostSummary.setId(null);
			IrrCalculationData irrCalculationData = this.irrCalculationDao.findByProjectId(existingProj.getId());
			if (irrCalculationData != null)
				irrCalculationData.setId(null);
			log.debug("Saved Project Details.");
			existingProj.setId(null);
			newProj = (ProjectDetails) this.projectDetailsDao.save(existingProj);
			if (irrCalculationData != null)
				irrCalculationData.setProjectId(newProj.getId());
			if (projectCostSummary != null)
				projectCostSummary.setParentId(newProj.getId());
			if (projectCostSummary != null)
				this.projectCostSummaryDao.save(projectCostSummary);
			if (irrCalculationData != null)
				this.irrCalculationDao.save(irrCalculationData);
			
			createNotification(newProj);
			
		} catch (Exception ex) {
			log.info("error in copy report "+ ex.getMessage());
			throw new FeasibilityException("Error in copying Project : " + ex.getMessage());
		}
		return Boolean.valueOf(true);
	}

	public List<ProjectDetails> fetchAllProjectDetails2(Pageable pageable, List<Filter> filters, String userid) {
		try {
			Query query = QueryBuilder.createQuery(filters, pageable);
			query.getSortObject();
			List<Integer> status = new ArrayList<>();
			status.add(Integer.valueOf(0));
			status.add(Integer.valueOf(208));
			status.add(Integer.valueOf(108));
			status.add(Integer.valueOf(308));
			if (RoleUtil.isCurrentUserHasBDRole()) {
				query.addCriteria(
						(CriteriaDefinition) Criteria.where("status").not().in(status).and("projectBdId").is(userid));
			} else {
				query.addCriteria((CriteriaDefinition) Criteria.where("status").not().in(status));
			}
			System.out.println(query);
			List<ProjectDetails> surveyResultList = this.mongoTemplate.find(query, ProjectDetails.class);
			surveyResultList
					.sort((obj1, obj2) -> (obj1.getCreatedDate().longValue() > obj2.getCreatedDate().longValue()) ? 1
							: ((obj1.getCreatedDate().longValue() < obj2.getCreatedDate().longValue()) ? 1 : 0));
			return surveyResultList;
		} catch (Exception e) {
			log.error("error while fetching Project Details.", e);
			throw new FeasibilityException("error while fetching Project Details");
		}
	}

	public List<ProjectDetails> fetchAllOpsOrExtProjectDetails(String misType) {
		Query query = QueryBuilder.createQuery(null, null);
		query.getSortObject();
		if ("ops".equals(misType)) {
			query.addCriteria((CriteriaDefinition) Criteria.where("isOperational").in(new Object[] { "1" }));
		} else {
			query.addCriteria((CriteriaDefinition) Criteria.where("isExit").in(new Object[] { "1" }));
		}
		List<ProjectDetails> surveyResultList = this.mongoTemplate.find(query, ProjectDetails.class);
		return surveyResultList;
	}

	@Override
	public IrrCalculationData getPAndLTabData(String projectId) {
		try {
			IrrCalculationData irr = irrCalculationDao.findByProjectId(projectId);
			if(!ObjectUtils.isEmpty(irr)) {
				Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);
				Integer leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
				List<Integer> leasePeriodList = new ArrayList<>();
	
				for (int i = 1; i <= leasePeriod; i++) {
					leasePeriodList.add(i);
				}
				log.info(leasePeriodList.size() + "");
				if (!ObjectUtils.isEmpty(leasePeriodList))
					irr.setLeasePeriod(leasePeriodList);
	
				Map<Integer, Double> grossMarginForEachYear = new HashMap<>();
				irr.getGrossMarginForEachYear().forEach((k, v) -> {
					grossMarginForEachYear.put(k, v / 100000);
				});
	
				Map<Integer, Double> rentRevRatio = new HashMap<>();
	
				irr.getRentEachYear().forEach((k1, v1) -> {
					irr.getTotalRevenueForEachYear().forEach((k2, v2) -> {
						if (k1 == k2)
							rentRevRatio.put(k1, (v1 / v2) * 100);
					});
				});
				Map<Integer, Double> rentCamRevenueRatioDetails = new HashMap<>();
	
				irr.getRentEachYear().forEach((k1, v1) -> {
					irr.getCamEachYear().forEach((k2, v2) -> {
						irr.getTotalRevenueForEachYear().forEach((k3, v3) -> {
							if (k1 == k2 && k1 == k3)
								rentCamRevenueRatioDetails.put(k1, ((v1 + v2) / v3) * 100);
						});
					});
				});
	
				irr.setRentRationDetails(rentRevRatio);
				irr.setRentCamRevenueRatioDetails(rentCamRevenueRatioDetails);
				irr.setGrossMarginForEachYear(grossMarginForEachYear);
			}

			return irr;

		} catch (Exception e) {
			log.info("exception in getting p and l tab info " + e.getMessage());
			throw new FeasibilityException("error while fetching Irr Details");
		}
	}
}
