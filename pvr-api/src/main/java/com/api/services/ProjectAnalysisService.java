package com.api.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.common.mappers.BenchmarkCinema;
import com.common.mappers.ProjectAnalysisMapper;
import com.common.models.Navision;
import com.common.models.ProjectDetails;
import com.common.sql.models.SeatCategory;

public interface ProjectAnalysisService {

	SeatCategory getSeatCategoryMasterDetails();
	ProjectAnalysisMapper getProjectAnalysisById(String projectId);
	long getProjectLegalExpense(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectInsuranceExpense(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectInternetCharges(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectTravellingConveyance(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getWaterAndElectricity(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getPrintingAndStationary(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectPersonnalExpanses(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectHousekeepingExpense(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectSecurityExpense(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectRepairAndMaintainceExpense(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectMarketingExpense(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectCommunicationExpense(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectMiscellaneousExpense(String projectId,Optional<ProjectDetails> projectDetails, List<Navision> lastQuarterNonCompDataFinal);
	long getProjectCamDetails(String projectId,Optional<ProjectDetails> projectDetails);
	long getProjectTestAlgo(String projectId);
	
	Set<BenchmarkCinema> getPersonalExpenseBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getEletricityDetailsBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getRepairAndMaintainceBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getHousekeepingBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getMiscellaneousBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getTravellingAndConveyanceBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getSecurityBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getLegalFeeBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getCommunicationBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getMarketingBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getInternetFeeBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getinsuranceBenchmarkCinemas(ProjectDetails projectDetails);
	Set<BenchmarkCinema> getPrintingStationaryBenchmarkCinemas(ProjectDetails projectDetails);
}
