package com.api.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.CityTierDao;
import com.api.dao.IrrCalculationDao;
import com.api.dao.ProjectCostSummaryDao;
import com.api.dao.ProjectDetailsDao;
import com.api.dao.UserDao;
import com.api.exception.FeasibilityException;
import com.api.utils.RoleUtil;
import com.api.utils.StateAndCityUtil;
import com.common.constants.Constants;
import com.common.constants.MessageConstants;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.AssumptionsValues;
import com.common.models.CityTier;
import com.common.models.FieldValue;
import com.common.models.IrrCalculationData;
import com.common.models.Notification;
import com.common.models.OtherParameter;
import com.common.models.ProjectCostSummary;
import com.common.models.ProjectDetails;
import com.common.models.ProjectLocation;
import com.common.models.User;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PreHandoverServiceImpl implements PreHandoverService {

	@Autowired
	private ProjectDetailsDao projectDetailsDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private MailService mailService;

	@Autowired
	private CityTierDao cityTierDao;

	@Autowired
	private ProjectDetailsServiceImpl projectDetailsServiceImpl;

	@Autowired
	private ProjectCostSummaryDao projectCostSummaryDao;

	@Autowired
	private IrrCalculationDao irrCalculationDao;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	

	@Autowired
	private ProjectDetailsService projectDetailsService;


	@Override
	public ProjectDetails saveStep1(ProjectDetails projectDetails) {
		return null;
	}

	@Override
	public ProjectDetails save(ProjectDetails entity) {
		return null;
	}

	@Override
	public ProjectDetails update(String id, ProjectDetails entity) {
		return null;
	}

	@Override
	public ProjectDetails get(String id) {
		Optional<ProjectDetails> pD = projectDetailsDao.findById(id);
		return pD.isPresent() ? pD.get() : null;
	}

	@Override
	public List<ProjectDetails> getAllExistingProjects() {

		List<ProjectDetails> projectDetails = new ArrayList<>();
		try {
			
			GroupOperation group = Aggregation.group(Fields.fields("reportIdVersion"));
			/*GroupOperation group = Aggregation.group("reportIdVersion").count().as("reportId");*/
			MatchOperation matchStage = Aggregation
					.match(Criteria.where("feasibilityState").is(Constants.POST_SIGNING).and("projectBdId").is(RoleUtil.getCurrentUserID()));

			Aggregation aggregation = Aggregation.newAggregation(matchStage,group);
			AggregationResults<ProjectDetails> output = mongoTemplate.aggregate(aggregation, "project_details",
					ProjectDetails.class);
			List<ProjectDetails> resultList = output.getMappedResults();
			
			
			
			if(!ObjectUtils.isEmpty(resultList)) {
				for(int i=0;i<resultList.size();i++) {
					
					Integer reportIdVersion = Integer.parseInt(resultList.get(i).getId());
					
					SortOperation sort = Aggregation.sort(Sort.Direction.DESC, "reportIdSubVersion");
					MatchOperation match = Aggregation
							.match(Criteria.where("reportIdVersion").is(reportIdVersion).and("projectBdId").is(RoleUtil.getCurrentUserID()));
					
					Aggregation agg = Aggregation.newAggregation(match, sort);

					AggregationResults<ProjectDetails> result = mongoTemplate.aggregate(agg, "project_details",
							ProjectDetails.class);
					List<ProjectDetails> list = result.getMappedResults();
					
					for(int j=0; j<list.size(); j++) {
						ProjectDetails details = list.get(j);
						if(details.getStatus() == Constants.STATUS_DELETE && (details.getFeasibilityState().equals(Constants.PRE_SIGNING) || details.getFeasibilityState().equals(Constants.POST_SIGNING) || details.getFeasibilityState().equals(Constants.PRE_HANDOVER))) {
							continue;
						}
						else {
							if((details.getStatus() == Constants.STATUS_ARCHIVED && details.getFeasibilityState().equals(Constants.PRE_SIGNING)) ||
									(details.getStatus() == Constants.POST_STATUS_ARCHIVED && details.getFeasibilityState().equals(Constants.POST_SIGNING)) ||
									(details.getStatus() == Constants.PRE_HAND_STATUS_ARCHIVED && details.getFeasibilityState().equals(Constants.PRE_HANDOVER))) {
								break;
							}
							if(details.getStatus() != Constants.STATUS_ACCEPTED && details.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
								break;
							}else if(details.getStatus() == Constants.STATUS_ACCEPTED && details.getFeasibilityState().equals(Constants.PRE_HANDOVER)) {
								projectDetails.add(details);
								break;
							}
							else if(details.getStatus() != Constants.STATUS_ACCEPTED && details.getFeasibilityState().equals(Constants.POST_SIGNING)) {
								break;
							}else if(details.getStatus() == Constants.STATUS_ACCEPTED && details.getFeasibilityState().equals(Constants.POST_SIGNING)) {
								projectDetails.add(details);
								break;
							}
						}
					}
					
				}
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching existing projectDetails.");
		}
		return projectDetails;
	}

	@Override
	public ProjectDetails saveDevelopmentDetails(ProjectDetails entity) {

		if (ObjectUtils.isEmpty(entity)) {
			log.error("Error while saving Project Details.");
			throw new FeasibilityException("Error while saving Project Details.");
		}

		// city-tier

		String tier = "";
		CityTier cityTier = cityTierDao.findByCity(StateAndCityUtil.getCity(entity.getProjectLocation().getCity()));
		if (!ObjectUtils.isEmpty(cityTier)) {
			tier = cityTier.getTier();
		} else {
			tier = "";
		}

		ProjectLocation projectLocation = new ProjectLocation();
		projectLocation.setCityTier(tier);
		projectLocation.setCity(StateAndCityUtil.getCity(entity.getProjectLocation().getCity()));
		projectLocation.setLatitude(entity.getProjectLocation().getLatitude());
		projectLocation.setLongitude(entity.getProjectLocation().getLongitude());
		projectLocation.setLocality(entity.getProjectLocation().getLocality());
		projectLocation.setState(entity.getProjectLocation().getState());

		ProjectDetails existingProject = this.get(entity.getId());
		ProjectDetails projectDetails = null;

		if (Constants.REPORT_TYPE_EXISTING.equals(entity.getReportType()) && (Constants.POST_SIGNING.equals(existingProject.getFeasibilityState()) || Constants.PRE_HANDOVER.equals(existingProject.getFeasibilityState()))) {
			synchronized (this) {
				ProjectDetails existingProjectDetails = this.get(entity.getId());
				existingProjectDetails.setId(null);
				existingProjectDetails.setReportType(Constants.REPORT_TYPE_EXISTING);

				ProjectDetails latestProjectDetails = projectDetailsServiceImpl
						.getLatestProject(existingProjectDetails.getReportType(), existingProjectDetails.getReportId());

				existingProjectDetails.setFeasibilityState(Constants.PRE_HANDOVER);
				existingProjectDetails.setReportId(latestProjectDetails.getReportId());
				existingProjectDetails.setReportIdVersion(latestProjectDetails.getReportIdVersion());
				existingProjectDetails.setReportIdSubVersion(latestProjectDetails.getReportIdSubVersion());
				existingProjectDetails.setReportType(Constants.REPORT_TYPE_NEW);

				existingProjectDetails.setProjectLocation(projectLocation);
				existingProjectDetails.setTotalDeveloperSize(entity.getTotalDeveloperSize());
				existingProjectDetails.setRetailAreaOfProject(entity.getRetailAreaOfProject());
				existingProjectDetails.setTenancyMix(entity.getTenancyMix());
				existingProjectDetails
						.setUpcomingInfrastructureDevlopment(entity.getUpcomingInfrastructureDevlopment());
				existingProjectDetails.setDateOfHandover(entity.getDateOfHandover());
				existingProjectDetails.setDateOfOpening(entity.getDateOfOpening());
				existingProjectDetails.setProjectType(entity.getProjectType());
				existingProjectDetails.setDateOfSendForApproval(null);
				existingProjectDetails.setStatus(Constants.STEP_1);
				existingProjectDetails.createdBy(RoleUtil.getCurrentUserID());
				existingProjectDetails.setApproverId(null);
				existingProjectDetails.setMsgMapper(null);
				this.emptyQueryDataForBDApprover(existingProjectDetails);
				this.emptyQueryDataForOperatingAssumption(existingProjectDetails);
				existingProjectDetails.setCreatedDate(new Date().getTime());
				existingProjectDetails.createdBy(RoleUtil.getCurrentUserID());
				existingProjectDetails.setIsOperationSubmit(false);
				existingProjectDetails.setIsProjectSubmit(false);
				existingProjectDetails.setIsSendForOpsAndProject(entity.getIsSendForOpsAndProject());
				existingProjectDetails.setProjectBdId(RoleUtil.getCurrentUserID());
				existingProjectDetails.setRejectedBy(null);
				existingProjectDetails.setRejectReasonMsg(null);

				if (!ObjectUtils.isEmpty(entity.getFileURLStep1())) {
					existingProjectDetails.setFileURLStep1(entity.getFileURLStep1());
				}

				log.debug("Saved Project Details.");
				projectDetails = projectDetailsDao.save(existingProjectDetails);

				ProjectCostSummary projectCostSummary = projectCostSummaryDao.findByParentId(existingProject.getId());

				projectCostSummary.setId(null);
				projectCostSummary.setParentId(projectDetails.getId());
				projectCostSummaryDao.save(projectCostSummary);

				IrrCalculationData irrCalculationData = irrCalculationDao.findByProjectId(existingProject.getId());
				irrCalculationData.setId(null);
				irrCalculationData.setProjectId(projectDetails.getId());
				irrCalculationDao.save(irrCalculationData);

			}

		}

		if (Constants.REPORT_TYPE_NEW.equals(entity.getReportType()) && Constants.PRE_HANDOVER.equals(existingProject.getFeasibilityState())) {

			synchronized (this) {

				ProjectDetails existingProjectDetails = this.get(entity.getId());
				existingProjectDetails.setProjectLocation(projectLocation);
				existingProjectDetails.setProjectName(entity.getProjectName());
				existingProjectDetails.setTotalDeveloperSize(entity.getTotalDeveloperSize());
				existingProjectDetails.setRetailAreaOfProject(entity.getRetailAreaOfProject());
				existingProjectDetails.setProjectType(entity.getProjectType());
				existingProjectDetails.setTenancyMix(entity.getTenancyMix());
				existingProjectDetails
						.setUpcomingInfrastructureDevlopment(entity.getUpcomingInfrastructureDevlopment());
				existingProjectDetails.setDateOfHandover(entity.getDateOfHandover());
				existingProjectDetails.setDateOfOpening(entity.getDateOfOpening());
				existingProjectDetails.setIsSendForOpsAndProject(entity.getIsSendForOpsAndProject());
				if (!ObjectUtils.isEmpty(entity.getFileURLStep1())) {
					existingProjectDetails.setFileURLStep1(entity.getFileURLStep1());
				}

				projectDetails = projectDetailsDao.save(existingProjectDetails);

			}

		}

		projectDetailsServiceImpl.createNotification(projectDetails);
		return projectDetails;

	}

	@Override
	public QueryMapper saveQuery(MsgMapper msgMapper) {

		if (ObjectUtils.isEmpty(msgMapper.getProjectId())) {
			throw new FeasibilityException("Project id cannot be null or empty");
		}

		List<String> ccList = new ArrayList<>();
		String message = "query asked";
		String subject = "";
		String mailTo = "";

		List<QueryMapper> queryMapper = new ArrayList<QueryMapper>();
		QueryMapper mapper = new QueryMapper();

		User currentUser = userDao.findOneById(RoleUtil.getCurrentUserID());
		mapper.setUserFullName(currentUser.getFullName());
		mapper.setUserId(RoleUtil.getCurrentUserID());
		mapper.setMsg(msgMapper.getMsg());
		mapper.setDate_time(new Date().getTime());

		ProjectDetails existingProject = projectDetailsDao.findById(msgMapper.getProjectId()).get();
		if (!ObjectUtils.isEmpty(existingProject)) {
			FieldValue fieldValue = null;

			switch (msgMapper.getQueryType()) {
			case "atp":
				fieldValue = existingProject.getFinalSummary().getAtp();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());

					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setAtp(fieldValue);
				}
				break;
			case "occupancy":
				fieldValue = existingProject.getFinalSummary().getOccupancy();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());

					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setOccupancy(fieldValue);
				}
				break;
			case "sph":
				fieldValue = existingProject.getFinalSummary().getSph();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());

					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setSph(fieldValue);
				}
				break;
			case "adRevenue":
				fieldValue = existingProject.getFinalSummary().getAdRevenue();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());

					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setAdRevenue(fieldValue);
				}
				break;
			case "personalExpense":
				fieldValue = existingProject.getFinalSummary().getPersonalExpense();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());

					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setPersonalExpense(fieldValue);
				}
				break;
			case "electricityWaterExpanse":
				fieldValue = existingProject.getFinalSummary().getElectricityWaterExpanse();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());

					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setElectricityWaterExpanse(fieldValue);
				}
				break;
			case "totalRmExpenses":
				fieldValue = existingProject.getFinalSummary().getTotalRmExpenses();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());

					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setTotalRmExpenses(fieldValue);
				}
				break;
			case "totalOverheadExpenses":
				fieldValue = existingProject.getFinalSummary().getTotalOverheadExpenses();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());

					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setTotalOverheadExpenses(fieldValue);
				}
				break;
			case "totalAdmits":
				fieldValue = existingProject.getFinalSummary().getTotalAdmits();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					/*
					 * mapper.setUserId(RoleUtil.getCurrentUserID());
					 * mapper.setMsg(msgMapper.getMsg()); mapper.setDate_time(new Date().getTime());
					 */
					fieldValue.setLastQueryUserId(mapper.getUserId());

					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setTotalAdmits(fieldValue);
				}
				break;

			case "totalProjectCost":
				fieldValue = existingProject.getFinalSummary().getTotalProjectCost();
				if (!ObjectUtils.isEmpty(fieldValue)) {
					fieldValue.setLastQueryUserId(mapper.getUserId());

					List<QueryMapper> listMapper = fieldValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						fieldValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						fieldValue.setQuery(queryMapper);
					}

					existingProject.getFinalSummary().setTotalProjectCost(fieldValue);
				}
				break;

			default:
				fieldValue = null;
				break;
			}

			// save notification
			msgMapper.setMsgById(RoleUtil.getCurrentUserID());
			msgMapper.setMsgByRole(RoleUtil.getCurrentUserSingleRoleString());
			// msgMapper.setMsgByRole(RoleUtil.getCurrentUseInfo().getAuthorities());
			existingProject.setMsgMapper(msgMapper);

			projectDetailsServiceImpl.createNotification(existingProject);

			if (existingProject.getFeasibilityState().equals(Constants.PRE_SIGNING)) {// mail
				if (RoleUtil.isCurrentUserHasApproverRole()) {
					int type = RoleUtil.getCurrentUseInfo().getApproverType();
					subject = MessageConstants.MAIL_SUBJECT_QUERY;
					mailTo = existingProject.getCreatedBy();

					if (type == Constants.STATUS_APPROVER_1) {

						message = "Report: " + existingProject.getReportId() + " " + message;
						// ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).getEmail());
					} else if (type == Constants.STATUS_APPROVER_2) {

						message = "Report: " + existingProject.getReportId() + " " + message;
						ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).getEmail());
					}

				} else if (RoleUtil.isCurrentUserHasBDRole()) {
					message = "query answered";
					subject = MessageConstants.MAIL_SUBJECT_ANSWER;

					// pending
					mailTo = "";

				}
			} else if (existingProject.getFeasibilityState().equals(Constants.POST_SIGNING)) {

			}
			mailService.mailService(existingProject.getCreatedBy(), MessageConstants.MAIL_SUBJECT_QUERY, message,
					ccList);
			projectDetailsDao.save(existingProject);

		}

		return mapper;
	}

	@Override
	public QueryMapper saveQueryAtOperatingAssumptions(MsgMapper msgMapper) {

		if (ObjectUtils.isEmpty(msgMapper.getProjectId())) {
			throw new FeasibilityException("Project id cannot be null or empty");
		}

		List<String> ccList = new ArrayList<>();
		String message = "query asked";
		String subject = "";
		String mailTo = "";

		List<QueryMapper> queryMapper = new ArrayList<QueryMapper>();
		QueryMapper mapper = new QueryMapper();

		User currentUser = userDao.findOneById(RoleUtil.getCurrentUserID());
		mapper.setUserFullName(currentUser.getFullName());
		mapper.setUserId(RoleUtil.getCurrentUserID());
		mapper.setMsg(msgMapper.getMsg());
		mapper.setDate_time(new Date().getTime());

		ProjectDetails existingProject = projectDetailsDao.findById(msgMapper.getProjectId()).get();
		if (!ObjectUtils.isEmpty(existingProject)) {
			AssumptionsValues assumptionValue = null;
			OtherParameter otherParam = null;

			msgMapper.setStep(Constants.STEP6);
			switch (msgMapper.getQueryType()) {

			case "operatingOccupancyquery":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getOperatingOccupancyquery();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setOperatingOccupancyquery(assumptionValue);
				} else {
					assumptionValue = new AssumptionsValues();
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setOperatingOccupancyquery(assumptionValue);
				}
				break;

			case "operatingAtpquery":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getOperatingAtpquery();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setOperatingAtpquery(assumptionValue);
				} else {
					assumptionValue = new AssumptionsValues();
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setOperatingAtpquery(assumptionValue);
				}
				break;
				
			case "operatingSphquery":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getOperatingSphquery();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setOperatingSphquery(assumptionValue);
				} else {
					assumptionValue = new AssumptionsValues();
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setOperatingSphquery(assumptionValue);
				}
				break;
			case "operatingProjectquery":
				msgMapper.setProjectChat(true);
				assumptionValue = existingProject.getOperatingProjectquery();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setOperatingProjectquery(assumptionValue);
				} else {
					assumptionValue = new AssumptionsValues();
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setOperatingProjectquery(assumptionValue);
				}
				break;

			case "adRevenue":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getAdRevenue();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setAdRevenue(assumptionValue);
				}
				break;
			case "webSale":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getWebSale();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setWebSale(assumptionValue);
				}
				break;
			case "personalExpenses":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getPersonalExpenses();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setPersonalExpenses(assumptionValue);
				}
				break;

			case "electricityWaterBill":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getElectricityWaterBill();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setElectricityWaterBill(assumptionValue);
				}
				break;

			case "totalRMExpenses":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getTotalRMExpenses();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setTotalRMExpenses(assumptionValue);
				}
				break;

			case "houseKeepingExpense":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getHouseKeepingExpense();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setHouseKeepingExpense(assumptionValue);
				}
				break;
			case "securityExpense":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getSecurityExpense();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setSecurityExpense(assumptionValue);
				}
				break;
			case "communicationExpense":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getCommunicationExpense();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setCommunicationExpense(assumptionValue);
				}
				break;

			case "travellingAndConveyanceExpense":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getTravellingAndConveyanceExpense();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setTravellingAndConveyanceExpense(assumptionValue);
				}
				break;
			case "legalFee":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getLegalFee();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setLegalFee(assumptionValue);
				}
				break;
			case "insuranceExpense":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getInsuranceExpense();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setInsuranceExpense(assumptionValue);
				}
				break;
			case "internetExpense":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getInternetExpense();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setInternetExpense(assumptionValue);
				}
				break;
			case "printingAndStationaryExpense":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getPrintingAndStationaryExpense();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setPrintingAndStationaryExpense(assumptionValue);
				}
				break;
			case "marketingExpense":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getMarketingExpense();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setMarketingExpense(assumptionValue);
				}
				break;
			case "miscellaneousExpenses":
				msgMapper.setOperationChat(true);
				assumptionValue = existingProject.getMiscellaneousExpenses();
				if (!ObjectUtils.isEmpty(assumptionValue)) {
					List<QueryMapper> listMapper = assumptionValue.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						assumptionValue.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						assumptionValue.setQuery(queryMapper);
					}
					assumptionValue.setLastQueryUserId(mapper.getUserId());
					existingProject.setMiscellaneousExpenses(assumptionValue);
				}
				break;
			case "miscIncome":
				msgMapper.setOperationChat(true);
				otherParam = existingProject.getMiscIncome();
				if (!ObjectUtils.isEmpty(otherParam)) {
					List<QueryMapper> listMapper = otherParam.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						otherParam.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						otherParam.setQuery(queryMapper);
					}
					otherParam.setLastQueryUserId(mapper.getUserId());
					existingProject.setMiscIncome(otherParam);
				}
				break;
			case "showTax":
				msgMapper.setOperationChat(true);
				otherParam = existingProject.getShowTax();
				if (!ObjectUtils.isEmpty(otherParam)) {
					List<QueryMapper> listMapper = otherParam.getQuery();
					if (!ObjectUtils.isEmpty(listMapper)) {
						listMapper.add(mapper);
						otherParam.setQuery(listMapper);
					} else {
						queryMapper.add(mapper);
						otherParam.setQuery(queryMapper);
					}
					otherParam.setLastQueryUserId(mapper.getUserId());
					existingProject.setShowTax(otherParam);
				}
				break;

			default:
				assumptionValue = null;
				break;
			}

			// save notification
			msgMapper.setMsgById(RoleUtil.getCurrentUserID());
			msgMapper.setMsgByRole(RoleUtil.getCurrentUserSingleRoleString());
			// msgMapper.setMsgByRole(RoleUtil.getCurrentUseInfo().getAuthorities());
			existingProject.setMsgMapper(msgMapper);

			projectDetailsServiceImpl.createNotification(existingProject);

			/*
			 * if (existingProject.getFeasibilityState().equals(Constants.PRE_SIGNING)) {//
			 * mail if (RoleUtil.isCurrentUserHasApproverRole()) { int type =
			 * RoleUtil.getCurrentUseInfo().getApproverType(); subject =
			 * MessageConstants.MAIL_SUBJECT_QUERY; mailTo = existingProject.getCreatedBy();
			 * 
			 * if (type == Constants.STATUS_APPROVER_1) {
			 * 
			 * message = "Report: " + existingProject.getReportId() + " " + message; //
			 * ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_2).
			 * getEmail()); } else if (type == Constants.STATUS_APPROVER_2) {
			 * 
			 * message = "Report: " + existingProject.getReportId() + " " + message;
			 * ccList.add(userDao.findOneByApproverType(Constants.STATUS_APPROVER_1).
			 * getEmail()); }
			 * 
			 * } else if (RoleUtil.isCurrentUserHasBDRole()) { message = "query answered";
			 * subject = MessageConstants.MAIL_SUBJECT_ANSWER;
			 * 
			 * // pending mailTo = "";
			 * 
			 * } } else if
			 * (existingProject.getFeasibilityState().equals(Constants.POST_SIGNING)) {
			 * 
			 * }
			 */
			mailService.mailService(existingProject.getCreatedBy(), MessageConstants.MAIL_SUBJECT_QUERY, message,
					ccList);
			projectDetailsDao.save(existingProject);

		}

		return mapper;
	}

	@Override
	public String updateProjectDetails(MsgMapper msgMapper) {
		try {
			ProjectDetails entity = projectDetailsService.get(msgMapper.getProjectId());
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(entity.getId()));

			Update update = null;
			if (msgMapper.getIsProjectOrOperation().equals(Constants.OPERATION)) {
				update = new Update();
						update.set("isOperationSubmit", false);
						update.set("operationReasonMsg", msgMapper.getMsg());
						entity.setIsOperationSubmit(false);
			} else {
				update = new Update();
				update.set("isProjectSubmit", false);
				update.set("projectReasonMsg", msgMapper.getMsg());
				entity.setIsProjectSubmit(false);
			}
			
			mongoTemplate.updateMulti(query, update, ProjectDetails.class);
			entity.setMsgMapper(null);
			projectDetailsService.createNotification(entity);
		} catch (Exception ex) {
			log.error("error while updating report status");
		}
		return "";
	}
	
	@Override
	public boolean updateNotificationStatus(String projectId,String queryType,String step) 
	{
		boolean updateFlag=true;
		try {
			  int stepId=Integer.parseInt(step.trim());
			  Query query = new Query();
			  
			  if(stepId==Constants.STEP6)
			  {
				  query.addCriteria(
						  Criteria.where("projectId").is(projectId).
						  andOperator(Criteria.where("msgMapper.queryType").is(queryType).
								  andOperator(Criteria.where("msgMapper.step").is(stepId)))
						  );
			  }
			  else if(stepId==Constants.STEP7)
			  {
				  query.addCriteria(
						  Criteria.where("projectId").is(projectId).
						  andOperator(Criteria.where("msgMapper.queryType").is(queryType).
						  andOperator(Criteria.where("msgMapper.step").is(stepId)
						  //.andOperator(Criteria.where("msgMapper.msgByRole").is(Constants.ROLE_BD))
						  )));
						  
			  }
			  
			  Update update =new Update();
			  update.set("status", 0);
			  mongoTemplate.updateMulti(query, update, Notification.class);
			
		}catch (Exception e) {
			updateFlag=false;
			e.printStackTrace();
			log.error("error while updating Notification status");
		}
		return updateFlag;
	}
	
	public void  emptyQueryDataForBDApprover(ProjectDetails projectDetails)
	{
		if(!ObjectUtils.isEmpty(projectDetails)) {
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getTotalAdmits()!=null)
			{
				projectDetails.getFinalSummary().getTotalAdmits().setQuery(null);
				projectDetails.getFinalSummary().getTotalAdmits().setLastQueryUserId(null);
			}
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getOccupancy()!=null)
			{
				projectDetails.getFinalSummary().getOccupancy().setQuery(null);
				projectDetails.getFinalSummary().getOccupancy().setLastQueryUserId(null);
			}
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getAtp()!=null)
			{
				projectDetails.getFinalSummary().getAtp().setQuery(null);
				projectDetails.getFinalSummary().getAtp().setLastQueryUserId(null);
			}
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getSph()!=null)
			{
				projectDetails.getFinalSummary().getSph().setQuery(null);
				projectDetails.getFinalSummary().getSph().setLastQueryUserId(null);
			}
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getAdRevenue()!=null)
			{
				projectDetails.getFinalSummary().getAdRevenue().setQuery(null);
				projectDetails.getFinalSummary().getAdRevenue().setLastQueryUserId(null);
			}
			
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getPersonalExpense()!=null)
			{
				projectDetails.getFinalSummary().getPersonalExpense().setQuery(null);
				projectDetails.getFinalSummary().getPersonalExpense().setLastQueryUserId(null);
			}
			
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getElectricityWaterExpanse()!=null)
			{
				projectDetails.getFinalSummary().getElectricityWaterExpanse().setQuery(null);
				projectDetails.getFinalSummary().getElectricityWaterExpanse().setLastQueryUserId(null);
			}
			
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getTotalRmExpenses()!=null)
			{
				projectDetails.getFinalSummary().getTotalRmExpenses().setQuery(null);
				projectDetails.getFinalSummary().getTotalRmExpenses().setLastQueryUserId(null);
			}
			
			if(projectDetails.getFinalSummary()!=null&&projectDetails.getFinalSummary().getTotalProjectCost()!=null)
			{
				projectDetails.getFinalSummary().getTotalProjectCost().setQuery(null);
				projectDetails.getFinalSummary().getTotalProjectCost().setLastQueryUserId(null);
			}
		}
	}
	
	public void  emptyQueryDataForOperatingAssumption(ProjectDetails projectDetails)
	{
		if(!ObjectUtils.isEmpty(projectDetails)) {
			if(projectDetails.getOperatingAtpquery()!=null)
			{
				projectDetails.getOperatingAtpquery().setLastQueryUserId(null);
				projectDetails.getOperatingAtpquery().setQuery(null);
			}
			if(projectDetails.getOperatingOccupancyquery()!=null)
			{
				projectDetails.getOperatingOccupancyquery().setLastQueryUserId(null);
				projectDetails.getOperatingOccupancyquery().setQuery(null);
			}
			if(projectDetails.getOperatingSphquery()!=null)
			{
				projectDetails.getOperatingSphquery().setLastQueryUserId(null);
				projectDetails.getOperatingSphquery().setQuery(null);
			}
			if(projectDetails.getOperatingProjectquery()!=null)
			{
				projectDetails.getOperatingProjectquery().setLastQueryUserId(null);
				projectDetails.getOperatingProjectquery().setQuery(null);
			}
			if(projectDetails.getAdRevenue()!=null)
			{
				projectDetails.getAdRevenue().setLastQueryUserId(null);
				projectDetails.getAdRevenue().setQuery(null);
			}
			if(projectDetails.getWebSale()!=null)
			{
				projectDetails.getWebSale().setLastQueryUserId(null);
				projectDetails.getWebSale().setQuery(null);
			}
			if(projectDetails.getPersonalExpenses()!=null)
			{
				projectDetails.getPersonalExpenses().setLastQueryUserId(null);
				projectDetails.getPersonalExpenses().setQuery(null);
			}
			if(projectDetails.getElectricityWaterBill()!=null)
			{
				projectDetails.getElectricityWaterBill().setLastQueryUserId(null);
				projectDetails.getElectricityWaterBill().setQuery(null);
			}
			if(projectDetails.getTotalRMExpenses()!=null)
			{
				projectDetails.getTotalRMExpenses().setLastQueryUserId(null);
				projectDetails.getTotalRMExpenses().setQuery(null);
			}
			
			if(projectDetails.getHouseKeepingExpense()!=null)
			{
				projectDetails.getHouseKeepingExpense().setLastQueryUserId(null);
				projectDetails.getHouseKeepingExpense().setQuery(null);
			}
			
			if(projectDetails.getSecurityExpense()!=null)
			{
				projectDetails.getSecurityExpense().setLastQueryUserId(null);
				projectDetails.getSecurityExpense().setQuery(null);
			}
			
			if(projectDetails.getCommunicationExpense()!=null)
			{
				projectDetails.getCommunicationExpense().setLastQueryUserId(null);
				projectDetails.getCommunicationExpense().setQuery(null);
			}
			
			if(projectDetails.getTravellingAndConveyanceExpense()!=null)
			{
				projectDetails.getTravellingAndConveyanceExpense().setLastQueryUserId(null);
				projectDetails.getTravellingAndConveyanceExpense().setQuery(null);
			}
			
			if(projectDetails.getLegalFee()!=null)
			{
				projectDetails.getLegalFee().setLastQueryUserId(null);
				projectDetails.getLegalFee().setQuery(null);
			}
			
			if(projectDetails.getInsuranceExpense()!=null)
			{
				projectDetails.getInsuranceExpense().setLastQueryUserId(null);
				projectDetails.getInsuranceExpense().setQuery(null);
			}
			
			if(projectDetails.getInternetExpense()!=null)
			{
				projectDetails.getInternetExpense().setLastQueryUserId(null);
				projectDetails.getInternetExpense().setQuery(null);
			}
			
			if(projectDetails.getPrintingAndStationaryExpense()!=null)
			{
				projectDetails.getPrintingAndStationaryExpense().setLastQueryUserId(null);
				projectDetails.getPrintingAndStationaryExpense().setQuery(null);
			}
			
			if(projectDetails.getMarketingExpense()!=null)
			{
				projectDetails.getMarketingExpense().setLastQueryUserId(null);
				projectDetails.getMarketingExpense().setQuery(null);
			}
			
			if(projectDetails.getMiscellaneousExpenses()!=null)
			{
				projectDetails.getMiscellaneousExpenses().setLastQueryUserId(null);
				projectDetails.getMiscellaneousExpenses().setQuery(null);
			}
			
			if(projectDetails.getMiscIncome()!=null)
			{
				projectDetails.getMiscIncome().setLastQueryUserId(null);
				projectDetails.getMiscIncome().setQuery(null);
			}
			
			if(projectDetails.getShowTax()!=null)
			{
				projectDetails.getShowTax().setLastQueryUserId(null);
				projectDetails.getShowTax().setQuery(null);
			}
			
			
			
		}
	}
	
	
}
