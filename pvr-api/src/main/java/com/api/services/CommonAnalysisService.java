package com.api.services;

import java.util.List;

import com.common.models.CinemaFormat;
import com.common.models.Navision;

public interface CommonAnalysisService {

	List<Navision> filterBycategoryAndIcon(List<Navision> listNavision, String categoryType);

	double electricityAndWaterMultiplier(List<Navision> allNavision, int noOfScreen, double averageYearly,
			List<CinemaFormat> allCinema);

	long personnalCostMultiplier(List<Navision> allNavision, int noOfScreen, long averageYearly,
			List<CinemaFormat> allCinema, String categoryType);

	double houseKeepingMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema,String cinemaCategory,boolean flag);

	List<Navision> getGoldScreenFilter(List<Navision> listNavision);

	List<Navision> getByCityFilter(List<Navision> navision, String cityTier);

	List<Navision> getAllNavisionByComp();

	double securityMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema);

	double randmMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly, List<CinemaFormat> allCinema,
			String categoryType);

	double communicationMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema);

	double marketingMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema, String Category);

	double printingAndStationayMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema);

	double travellingAndConveyanceMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema);

	double legalFeeMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema);

	double insuranceExpenseMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema);

	double internetExpanseMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema);

	double miscellaneousExpenseMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema);

	List<String> getRelatedCategoryByInput(String category);

	List<Navision> getAllVistaCompNonComp();

	public List<Navision> filterBycategory(List<Navision> listNavision, String categoryType);

	public List<Navision> filterRevenueCategory(List<Navision> listNavision, String categoryType);
	
	List<String> getCinemaFormatlist(String CategoryType);
	
	List<String> getAreaCategoryBySeatlist(String seatList);
	
	List<Navision> getImaxScreenFilter(List<Navision> listNavision);
	
	List<Navision> get4dxScreenFilter(List<Navision> listNavision);
	
	double camExpenseMultiplier(List<Navision> allNavision, int noOfScreen, long averageyearly,
			List<CinemaFormat> allCinema);
	
	List<Navision> filterOutByIcon(List<Navision> listNavision);
	
	List<Navision> getByCityFilterInSecurity(List<Navision> navision, String cityTier);
	
	List<Navision> getByCityFilterInMiscellaneous(List<Navision> navision, String cityTier);
	
	List<Navision> getByCityFilterInMarketing(List<Navision> navision, String cityTier);
	
	List<Navision> getByCityFilterHouseKeeping(List<Navision> navision, String cityTier);
	
	List<Navision> getByCityFilterPersonnalExpense(List<Navision> navision, String cityTier);
	
	List<Navision> getByCityFilterRevenueExpense(List<Navision> navision, String cityTier);
	
}
