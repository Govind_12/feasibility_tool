package com.api.services;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.api.dao.FixedConstantDao;
import com.api.dao.IrrCalculationDao;
import com.api.dao.ProjectCostSummaryDao;
import com.api.dao.ProjectDetailsDao;
import com.api.utils.MathUtil;
import com.common.constants.Constants;
import com.common.models.AssumptionsCalculationData;
import com.common.models.AtpCapDetials;
import com.common.models.CostAssumptionsGrowthRate;
import com.common.models.FixedConstant;
import com.common.models.GstType;
import com.common.models.IrrCalculationData;
import com.common.models.ProjectCostData;
import com.common.models.ProjectCostSummary;
import com.common.models.ProjectDetails;
import com.common.models.RevenueAssumptionsGrowthRate;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class IrrCalculation {

	@Autowired
	private FixedConstantDao constantDao;

	@Autowired
	private ProjectAnalysisService projectAnalysisService;

	@Autowired
	private AssumptionsCalculation assumptionsCalculation;

	@Autowired
	private IrrCalculationDao irrCalculationDao;

	@Autowired
	private ProjectDetailsDao projectDetailsDao;

	@Autowired
	private ProjectCostSummaryDao projectCostSummaryDao;

	public ProjectDetails calculateIrrDetails(Optional<ProjectDetails> projectDetails,
			AssumptionsCalculationData assumptionsCalculationData) {
		ProjectDetails details = null;

		try {
			List<FixedConstant> fixedConstant = (List<FixedConstant>) constantDao.findAll();

			Double totalAdmits = assumptionsCalculationData.getTotalAdmits();
			String city = projectDetails.get().getProjectLocation().getCity();
			String state = projectDetails.get().getProjectLocation().getState();
			Double totalCapacity = assumptionsCalculationData.getTotalCapacity();
			Double totalAtp = assumptionsCalculationData.getTotalAtp();
			Double totalSph = assumptionsCalculationData.getTotalSph();
			Double totalShow = assumptionsCalculationData.getTotalshow();
			Double netGstOnBoxOffice = assumptionsCalculationData.getNetGstOnBoxOffice();
			Map<String, Double> admitsForEachScreenType = assumptionsCalculationData.getAdmitsForEachScreenType();
			Map<String, Map<String, Double>> admitsForEachScreenSeatType = assumptionsCalculationData
					.getAdmitsForEachScreenSeatType();
			
			
			
			
			

			// row 103 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			// 1
			Map<Integer, Double> admitsYearWise = getAdmitsForEachYears(totalAdmits, fixedConstant,projectDetails);

			// 2
			Map<Integer, Double> occupancyForEachYear = getOccupancyForEachYear(admitsYearWise, totalCapacity);

			// 3
			Map<Integer, Double> atpForEachYear = getAtpForEachYear(fixedConstant, totalAtp, projectDetails,
					admitsForEachScreenType, admitsForEachScreenSeatType);

			// 4
			Map<Integer, Double> grossTicketSalesForEachYear = getGrossTicketSalesForEachYear(atpForEachYear,admitsYearWise);

			// 5
			Map<Integer, Double> gstForEachYear = getGstForEachYear(netGstOnBoxOffice, grossTicketSalesForEachYear);

			// 6
			Double showTax = projectDetails.get().getShowTax().getInputValues();
			Map<Integer, Double> showtaxForEachYear = getshowtaxForEachYear(showTax, totalShow, projectDetails,
					fixedConstant);

			// 7
			Map<Integer, Double> healthCess = getHealthCess(fixedConstant, admitsYearWise, state);

			// 8
			Map<Integer, Double> serviceChargePerYear = getServiceChargePerYear(fixedConstant, admitsYearWise,
					projectDetails);

			// 9
			Map<Integer, Double> lbtExpenseForEachYear = getLbtExpenseForEachYear(fixedConstant,
					grossTicketSalesForEachYear, gstForEachYear, projectDetails);

			// 10
			Map<Integer, Double> netTicketSalesForFhcForEachYear = getNetTicketSalesForFhcForEachYear(
					grossTicketSalesForEachYear, gstForEachYear, showtaxForEachYear, serviceChargePerYear,
					lbtExpenseForEachYear);

			// 11
			Map<Integer, Double> net3dSales = getNet3dSales(fixedConstant, grossTicketSalesForEachYear,
					netGstOnBoxOffice, projectDetails);

			// 12
			Map<Integer, Double> netComplimentaryTickets = getNetComplimentaryTicketsForEachYear(fixedConstant,
					grossTicketSalesForEachYear, netGstOnBoxOffice);

			// 13
			Map<Integer, Double> netTicketSales = getNetTicketSalesForEachYear(serviceChargePerYear,
					netTicketSalesForFhcForEachYear, net3dSales, netComplimentaryTickets);

			// 14
			Map<Integer, Double> sphForEachYear = getSphForEachYear(totalSph, fixedConstant, atpForEachYear,projectDetails);

			// 15
			Map<Integer, Double> grossFnBForEachYear = getGrossFnBForEachYear(sphForEachYear, admitsYearWise);

			// 16
			Map<Integer, Double> gstFoodForEachYear = getGstFoodForEachYear(grossFnBForEachYear, fixedConstant);

			// 17
			Map<Integer, Double> netFnBRevenueForCogs = getNetFnBRevenueForCogs(grossFnBForEachYear,
					gstFoodForEachYear);

			// 18
			Map<Integer, Double> netComplimentaryFnBForEachYear = getNetComplimentaryFnBForEachYear(grossFnBForEachYear,
					fixedConstant);

			// 19
			Map<Integer, Double> netFnBSalesForEachYear = getNetFnBSalesForEachYear(netFnBRevenueForCogs,
					netComplimentaryFnBForEachYear);

			// 20
			Map<Integer, Double> adRevenueForEachYear = getAdRevenueForEachYear(projectDetails, fixedConstant);

			// 21
			Map<Integer, Double> conveniencefeeForEachYear = getConveniencefeeForEachYear(fixedConstant,
					assumptionsCalculationData,projectDetails);

			// 22
			Map<Integer, Double> vpfIncomeYearWise = getVpfIncomeYearWise(assumptionsCalculationData.getVpfIncome(),projectDetails,fixedConstant);

			Double miscellaneousIncome = projectDetails.get().getMiscIncome().getInputValues();

			// 23
			Map<Integer, Double> miscellaneousIncomeForEachYear = getMiscellaneousIncomeForEachYear(miscellaneousIncome,
					fixedConstant,projectDetails);

			// 24
			Map<Integer, Double> housekeepingChargesForEachYear = gethousekeepingChargesForEachYear(projectDetails,
					fixedConstant);

			// 25

			Map<Integer, Double> securityChargesForEachYear = getSecurityCharges(fixedConstant, projectDetails);

			// 26
			Map<Integer, Double> marketingExpenseForEachYear = getMarketingExpense(fixedConstant, projectDetails);

			// 27
			Map<Integer, Double> communicationChargesForEachYear = getCommuncationChargesExpense(fixedConstant,
					projectDetails);

			// 28
			Map<Integer, Double> internetChargesForEachYear = getInternetChargesExpense(fixedConstant, projectDetails);

			// 29
			Map<Integer, Double> legalChargesForEachYear = getLegalChargesExpense(fixedConstant, projectDetails);

			// 30
			Map<Integer, Double> travellingChargesForEachYear = getTravellingChargeChargesExpense(fixedConstant,
					projectDetails);

			// 31
			Map<Integer, Double> insuranceChargesForEachYear = getInsuranceChargeChargesExpense(fixedConstant,
					projectDetails);

			// 32
			Map<Integer, Double> miscellaneousCharges = getMiscellaneousCharges(fixedConstant, projectDetails);

			// 33
			Map<Integer, Double> printingAndStationaryChargesForEachYear = getPrintingAndStationaryCharge(fixedConstant,
					projectDetails);

			// 34
			Map<Integer, Double> rateAndTaxes = getRateAndTaxes(showtaxForEachYear, healthCess);

			Double municipalTaxesPerAnnum = assumptionsCalculationData.getMunicipalTaxesPerAnnum();

			// 35
			Map<Integer, Double> municipalTaxForEachYear = getMunicipalTaxForEachYear(municipalTaxesPerAnnum,fixedConstant,projectDetails);

			// 36
			Map<Integer, Double> totalOverheadExpensesForEachYear = getTotalOverheadExpensesForEachYear(
					housekeepingChargesForEachYear,municipalTaxForEachYear, securityChargesForEachYear, marketingExpenseForEachYear,
					communicationChargesForEachYear, internetChargesForEachYear, legalChargesForEachYear,
					travellingChargesForEachYear, insuranceChargesForEachYear, miscellaneousCharges,
					printingAndStationaryChargesForEachYear, rateAndTaxes);


			// 37
			Map<String, Map<Integer, Double>> rentYearDetail = rentYearDetail(projectDetails, fixedConstant,
					netTicketSales, netFnBSalesForEachYear, adRevenueForEachYear, miscellaneousIncomeForEachYear,
					vpfIncomeYearWise,conveniencefeeForEachYear);

			Map<Integer, Double> rentForEachYear = rentYearDetail.get("rent");

			Map<Integer, Double> mgForEachYear = rentYearDetail.get("mg");

			Map<Integer, Double> mgForEachYearWithoutDiscount = rentYearDetail.get("mgWithoutDiscount");

			// 38
			Map<Integer, Double> camForEachYear = camForEachYear(projectDetails, fixedConstant);

			//

			// WACC calculations >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			Double costOfEquityPostTax = fixedConstant.get(0).getCostOfEquity();

			Double costOfDebtPostTax = getCostOfDebtPostTax(fixedConstant);

			// 40
			Double wacc = getWacc(fixedConstant, costOfDebtPostTax);

			// row 4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			// 3
			Map<Integer, Double> sponsorshipRevenueForEachYear = adRevenueForEachYear;

			// 4
			Map<Integer, Double> vasIncomeForEachYear = conveniencefeeForEachYear;

			// 5
			Map<Integer, Double> otherOperatingIncome = getOtherOperatingIncome(vpfIncomeYearWise,
					miscellaneousIncomeForEachYear);

			// 6
			Map<Integer, Double> totalRevenueForEachYear = getTotalRevenueForEachYear(netTicketSales,
					netFnBSalesForEachYear, sponsorshipRevenueForEachYear, conveniencefeeForEachYear,
					otherOperatingIncome);

			// Depreciation workings >>>>>>>>>>>>>>>>>

			// 7
			Map<Integer, Double> securityDepositForEachYear = getSecurityDepositForEachYear(projectDetails,mgForEachYearWithoutDiscount,fixedConstant,camForEachYear);

			Map<String, Double> projectCostSummaryData = getProjectCostSummaryData(projectDetails);

			Double totalProjectCost = MathUtil.roundTwoDecimals(projectCostSummaryData.get("totalProjectCost"));
			Double salvageFromDisposalAssets = MathUtil
					.roundTwoDecimals(projectCostSummaryData.get("salvageFromDisposalAssets"));

			Double securityDepositForFirstYear = securityDepositForEachYear.get(0);
			Double grandTotal = MathUtil
					.roundTwoDecimals(projectCostSummaryData.get("totalProjectCostAfterContingencies") + securityDepositForFirstYear);
			Double depRate = MathUtil.roundTwoDecimals(projectCostSummaryData.get("depRate"));
			Double totalProjectCostAfterContingencies = MathUtil
					.roundTwoDecimals(projectCostSummaryData.get("totalProjectCostAfterContingencies"));

			// 39


			Map<String, Map<Integer, Double>> depricationWorkingsData = getDepricationWorkingsData(fixedConstant,
					projectDetails, totalRevenueForEachYear, totalProjectCostAfterContingencies, depRate);

			Map<Integer, Double> openingGrossBlock = depricationWorkingsData.get("openingGrossBlock");
			Map<Integer, Double> closingGrossBlock = depricationWorkingsData.get("closingGrossBlock");
			Map<Integer, Double> additions = depricationWorkingsData.get("additions");
			Map<Integer, Double> depreciationForEachYear = depricationWorkingsData.get("depreciation");
			Map<Integer, Double> netGrossForEachYear = depricationWorkingsData.get("netGrossForEachYear");

			// ------------------------------------------------------------------------------------------------------------

			// 7
			Map<Integer, Double> filmDistributorShare = getFilmDistributorShare(netTicketSalesForFhcForEachYear,
					assumptionsCalculationData.getTotalFilmHireCostPercentage());

			// 8
			Map<Integer, Double> consumptionOfFnBForEachYear = getConsumptionOfFnBForEachYear(netFnBRevenueForCogs,
					projectDetails);

			// 9
			Map<Integer, Double> grossMarginForEachYear = getGrossMarginForEachYear(totalRevenueForEachYear,
					filmDistributorShare, consumptionOfFnBForEachYear);

			// 10
			Map<Integer, Double> grossMarginPercent = getGrossMarginPercent(grossMarginForEachYear,
					totalRevenueForEachYear);

			// 11
			Map<Integer, Double> personnelExpenseForEachYear = getPersonnelExpenseForEachYear(projectDetails,
					fixedConstant);

			// 12
			Map<Integer, Double> electricityAndWaterExpenseForEachYear = getElectricityAndWaterExpenseForEachYear(
					projectDetails, fixedConstant);

			// 13
			Map<Integer, Double> repairAndMaintenanceExpenseExpenseForEachYear = getRepairAndMaintenanceExpenseExpenseForEachYear(
					projectDetails, fixedConstant);

			// 14
			Map<Integer, Double> otherOperatingExpensesForEachYear = getOtherOperatingExpensesForEachYear(
					totalOverheadExpensesForEachYear, fixedConstant,projectDetails);

			// 15
			Map<Integer, Double> expenditureExRentAndCamForEachYear = getExpenditureExRentAndCamForEachYear(
					personnelExpenseForEachYear, electricityAndWaterExpenseForEachYear,
					repairAndMaintenanceExpenseExpenseForEachYear, otherOperatingExpensesForEachYear);

			// 16
			Map<Integer, Double> ebitdarForEachYear = getEBITDARForEachYear(grossMarginForEachYear,
					expenditureExRentAndCamForEachYear);

			// 17
			Map<Integer, Double> ebitdarMarginPercentForEachYear = getEBITDARMarginPercentForEachYear(
					ebitdarForEachYear, totalRevenueForEachYear);

			// 18
			Map<Integer, Double> rentEachYear = getRent(rentForEachYear, fixedConstant);

			// 19
			Map<Integer, Double> camEachYear = getCam(camForEachYear, fixedConstant);

			// 20
			Map<Integer, Double> getEBITDAForEachYear = getEBITDAForEachYear(ebitdarForEachYear, rentEachYear,
					camEachYear);

			// 21
			Map<Integer, Double> getEBITDAMarginForEachYear = getEBITDAMarginForEachYear(getEBITDAForEachYear,
					totalRevenueForEachYear);

			// 23
			Map<Integer, Double> getEBITForEachYear = getEBITForEachYear(getEBITDAForEachYear, depreciationForEachYear);

			// row 76 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			// 2
			Map<Integer, Double> cumulativeEBITDAForEachYear = getCumulativeEBITDAForEachYear(getEBITDAForEachYear);

			// 3
			Double getEBITDAPayback = getEBITDAPayback(cumulativeEBITDAForEachYear, grandTotal, getEBITDAForEachYear);

			// 4
			Map<Integer, Double> taxForEachYear = getTaxForEachYear(getEBITForEachYear, fixedConstant);

			// 5
			Map<Integer, Double> cashInflowAfterTaxForEachYear = getCashInflowAfterTaxForEachYear(getEBITDAForEachYear,
					taxForEachYear);

			// 6
			Map<Integer, Double> capexForEachYear = getCapexForEachYear(additions);

			// 8
			Map<Integer, Double> disposalOfAssets = getDisposalOfAssets(salvageFromDisposalAssets, fixedConstant,projectDetails);

			// 9
			Map<Integer, Double> totalOutflowsForEachYear = getTotalOutflows(capexForEachYear,
					securityDepositForEachYear, disposalOfAssets, projectDetails);

			// 10
			Map<Integer, Double> freeCashFlowPreTaxForEachYear = getFreeCashFlowPreTaxForEachYear(getEBITDAForEachYear,
					totalOutflowsForEachYear);

			// 11
			Map<Integer, Double> freeCashFlowPostTaxForEachYear = getFreeCashFlowPostTaxForEachYear(
					cashInflowAfterTaxForEachYear, totalOutflowsForEachYear);

			// 12
			Map<Integer, Double> cumulativeFCFPostTaxForEachYear = getCumulativeFCFPostTaxForEachYear(
					freeCashFlowPostTaxForEachYear);

			// 13
			Double getFCFPayback = getFCFPayback(cumulativeFCFPostTaxForEachYear, grandTotal);

			// row 190 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			// 2,3,4
			Map<Integer, Double> drawdownForEachYear = getDrawdownForEachYear(totalOutflowsForEachYear, fixedConstant);

			Map<String, Map<Integer, Double>> debtSchedule = getDebtSchedule(drawdownForEachYear, fixedConstant,
					grandTotal);

			// 5,9
			Map<Integer, Double> repaymentForEachYear = debtSchedule.get("repayment");

			// 6
			Map<Integer, Double> closingDebtForEachYear = debtSchedule.get("closingDebt");

			// 7
			Map<Integer, Double> interestUnderDebtScheduleForEachYear = debtSchedule.get("interest");

			// 1,8
			Map<Integer, Double> openingDebt = debtSchedule.get("openingDebt");

			// row 27 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			// 1
			Map<Integer, Double> interestForEachYear = getInterest(interestUnderDebtScheduleForEachYear);

			// 2
			Map<Integer, Double> getPBTForEachYear = getPBTForEachYear(getEBITForEachYear, interestForEachYear);

			// 3
			Map<Integer, Double> getTaxUnderPATForEachYear = getTaxUnderPATForEachYear(getPBTForEachYear,
					fixedConstant);

			// 4
			Map<Integer, Double> getPATForEachYear = getPATForEachYear(getPBTForEachYear, getTaxUnderPATForEachYear);

			// row 97 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			// 4,5
			Map<Integer, Double> lessCapexForEachYear = getLessCapexForEachYear(totalOutflowsForEachYear,
					fixedConstant);

			// 6
			Map<Integer, Double> equityCashFlowsForEachYear = getEquityCashFlowsForEachYear(getPATForEachYear,
					depreciationForEachYear, repaymentForEachYear, lessCapexForEachYear);

			// row 39 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			// 1
			Double projectIRRPreTax = MathUtil.roundTwoDecimals(getprojectIRRPreTax(freeCashFlowPreTaxForEachYear));

			// 2
			Double projectIRRPostTax =  MathUtil.roundTwoDecimals(getprojectIRRPostTax(freeCashFlowPostTaxForEachYear));

			// 3
			Double equityIRR = MathUtil.roundTwoDecimals(getEquityIRR(equityCashFlowsForEachYear));

			// 4
			Double npv = MathUtil.roundTwoDecimals(getNpv(freeCashFlowPostTaxForEachYear, wacc));

			// 5
			Double getROCE = MathUtil.roundTwoDecimals(getROCE(getEBITDAForEachYear, grandTotal));

			// row 46 – common size P&L >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			// 1
			Map<Integer, Double> netSaleOfTicket = getNetSaleOfTicket(netTicketSales, totalRevenueForEachYear);

			// 2
			Map<Integer, Double> saleOfFandB = getSaleOfFAndbForEachYear(netFnBSalesForEachYear,
					totalRevenueForEachYear);

			// 3
			Map<Integer, Double> sponsorshipRevenue = getSponsorshipRevenue(sponsorshipRevenueForEachYear,
					totalRevenueForEachYear);

			// 4
			Map<Integer, Double> vasIncome = conveniencefeeForEachYear;
			Map<Integer, Double> vasIncomeNextYear = getVasIncomeRowFoutySix(vasIncome, totalRevenueForEachYear);

			// 5
			Map<Integer, Double> otherOperatingRevenue = getOtherOperatingRevenue(otherOperatingIncome,
					totalRevenueForEachYear);

			// 6
			Map<Integer, Double> totalRevenue = getTotalRevenue(totalRevenueForEachYear);

			// 7
			Map<Integer, Double> filmDistributorShares = getFilmDistributorShareDetails(filmDistributorShare,
					totalRevenueForEachYear);

			// 8
			Map<Integer, Double> consumptionFandB = getConsumptionOfFAndBDetails(consumptionOfFnBForEachYear,
					totalRevenueForEachYear);

			// 9
			Map<Integer, Double> personnelExpenses = getpersonnelExpenses(personnelExpenseForEachYear,
					totalRevenueForEachYear);

			// 10
			Map<Integer, Double> electricityAndWaterCharges = getElectricityAndWaterDetails(
					electricityAndWaterExpenseForEachYear, totalRevenueForEachYear);

			// 11
			Map<Integer, Double> repairAndMaintaince = getRepairAndMaintanceDetails(
					repairAndMaintenanceExpenseExpenseForEachYear, totalRevenueForEachYear);

			// 12
			Map<Integer, Double> otherOperatingExpense = getOtherOperatingExpenseDetails(
					otherOperatingExpensesForEachYear, totalRevenueForEachYear);

			// 13
			Map<Integer, Double> expenditureExRentAndCamp = getExpenditureExrentAndCampDetails(
					expenditureExRentAndCamForEachYear, totalRevenueForEachYear);

			// 14
			Map<Integer, Double> ebitDar = getEbitDarDetails(ebitdarForEachYear, totalRevenueForEachYear);

			// 15
			Map<Integer, Double> rentDetails = getRentDetails(rentEachYear, totalRevenueForEachYear);

			// 16
			Map<Integer, Double> camDetails = getCamDetails(camEachYear, totalRevenueForEachYear);

			// 17
			Map<Integer, Double> ebitdaDetails = getEbitDaDetails(getEBITDAForEachYear, totalRevenueForEachYear);

			IrrCalculationData irrCalculationData = irrCalculationDao.findByProjectId(projectDetails.get().getId());

			if (ObjectUtils.isEmpty(irrCalculationData)) {

				irrCalculationData = new IrrCalculationData();
				irrCalculationData.setProjectId(projectDetails.get().getId());
			}

			projectDetails.get().getFinalSummary().getAdRevenue().setInputValues(projectDetails.get().getAdRevenue().getInputValues());

			projectDetails.get().getFinalSummary().getPersonalExpense().setInputValues(projectDetails.get().getPersonalExpenses().getInputValues());

			projectDetails.get().getFinalSummary().getElectricityWaterExpanse().setInputValues(projectDetails.get().getElectricityWaterBill().getInputValues());
			projectDetails.get().getFinalSummary().getTotalRmExpenses().setInputValues(projectDetails.get().getTotalRMExpenses().getInputValues());

			// projectDetails.get().getTotalOverhead().setInputValues(totalOverheadExpensesForEachYear.get(2));;

			projectDetails.get().getFinalSummary().setAnnualCam(MathUtil.roundTwoDecimals(camEachYear.get(2)));
			projectDetails.get().getFinalSummary().setAnnualRent(MathUtil.roundTwoDecimals(rentEachYear.get(2)));

			projectDetails.get().getFinalSummary().setPayBackPBDIT(getEBITDAPayback);
			projectDetails.get().getFinalSummary().setIrr(projectIRRPostTax);
			projectDetails.get().getFinalSummary().setLeasePeriod(projectDetails.get().getLesserLesse().getLeasePeriod());

			irrCalculationData.setAdmitsYearWise(admitsYearWise);
			irrCalculationData.setOccupancyForEachYear(occupancyForEachYear);
			irrCalculationData.setAtpForEachYear(atpForEachYear);
			irrCalculationData.setGrossTicketSalesForEachYear(grossTicketSalesForEachYear);
			irrCalculationData.setGstForEachYear(gstForEachYear);
			irrCalculationData.setShowtaxForEachYear(showtaxForEachYear);
			irrCalculationData.setHealthCess(healthCess);
			irrCalculationData.setServiceChargePerYear(serviceChargePerYear);
			irrCalculationData.setLbtExpenseForEachYear(lbtExpenseForEachYear);
			irrCalculationData.setNetTicketSalesForFhcForEachYear(netTicketSalesForFhcForEachYear);
			irrCalculationData.setNet3dSales(net3dSales);
			irrCalculationData.setNetComplimentaryTickets(netComplimentaryTickets);
			irrCalculationData.setNetTicketSales(netTicketSales);
			irrCalculationData.setSphForEachYear(sphForEachYear);
			irrCalculationData.setGrossFnBForEachYear(grossFnBForEachYear);
			irrCalculationData.setGstFoodForEachYear(gstFoodForEachYear);
			irrCalculationData.setNetFnBRevenueForCogs(netFnBRevenueForCogs);
			irrCalculationData.setNetComplimentaryFnBForEachYear(netComplimentaryFnBForEachYear);
			irrCalculationData.setNetFnBSalesForEachYear(netFnBSalesForEachYear);
			irrCalculationData.setAdRevenueForEachYear(adRevenueForEachYear);
			irrCalculationData.setConveniencefeeForEachYear(conveniencefeeForEachYear);
			irrCalculationData.setVpfIncomeYearWise(vpfIncomeYearWise);
			irrCalculationData.setMiscellaneousIncomeForEachYear(miscellaneousIncomeForEachYear);
			irrCalculationData.setHousekeepingChargesForEachYear(housekeepingChargesForEachYear);
			irrCalculationData.setSecurityChargesForEachYear(securityChargesForEachYear);
			irrCalculationData.setMarketingExpenseForEachYear(marketingExpenseForEachYear);
			irrCalculationData.setCommunicationChargesForEachYear(communicationChargesForEachYear);
			irrCalculationData.setInternetChargesForEachYear(internetChargesForEachYear);
			irrCalculationData.setLegalChargesForEachYear(legalChargesForEachYear);
			irrCalculationData.setTravellingChargesForEachYear(travellingChargesForEachYear);
			irrCalculationData.setInsuranceChargesForEachYear(insuranceChargesForEachYear);
			irrCalculationData.setMiscellaneousCharges(miscellaneousCharges);
			irrCalculationData.setPrintingAndStationaryChargesForEachYear(printingAndStationaryChargesForEachYear);
			irrCalculationData.setRateAndTaxes(rateAndTaxes);
			irrCalculationData.setMunicipalTaxForEachYear(municipalTaxForEachYear);
			irrCalculationData.setTotalOverheadExpensesForEachYear(totalOverheadExpensesForEachYear);
			irrCalculationData.setRentForEachYear(rentForEachYear);
			irrCalculationData.setCamForEachYear(camForEachYear);
			irrCalculationData.setWacc(wacc);

			irrCalculationData.setSponsorshipRevenueForEachYear(sponsorshipRevenueForEachYear);
			irrCalculationData.setOtherOperatingIncome(otherOperatingIncome);

			irrCalculationData.setVasIncomeForEachYear(vasIncomeForEachYear);
			irrCalculationData.setTotalRevenueForEachYear(totalRevenueForEachYear);
			irrCalculationData.setOpeningGrossBlock(openingGrossBlock);
			irrCalculationData.setClosingGrossBlock(closingGrossBlock);
			irrCalculationData.setAdditions(additions);
			irrCalculationData.setDepreciationForEachYear(depreciationForEachYear);
			irrCalculationData.setNetGrossForEachYear(netGrossForEachYear);
			irrCalculationData.setFilmDistributorShare(filmDistributorShare);
			irrCalculationData.setConsumptionOfFnBForEachYear(consumptionOfFnBForEachYear);
			irrCalculationData.setGrossMarginForEachYear(grossMarginForEachYear);
			irrCalculationData.setGrossMarginPercent(grossMarginPercent);
			irrCalculationData.setPersonnelExpenseForEachYear(personnelExpenseForEachYear);
			irrCalculationData.setElectricityAndWaterExpenseForEachYear(electricityAndWaterExpenseForEachYear);
			irrCalculationData
					.setRepairAndMaintenanceExpenseExpenseForEachYear(repairAndMaintenanceExpenseExpenseForEachYear);
			irrCalculationData.setOtherOperatingExpensesForEachYear(otherOperatingExpensesForEachYear);
			irrCalculationData.setExpenditureExRentAndCamForEachYear(expenditureExRentAndCamForEachYear);
			irrCalculationData.setEbitdarForEachYear(ebitdarForEachYear);
			irrCalculationData.setEbitdarMarginPercentForEachYear(ebitdarMarginPercentForEachYear);
			irrCalculationData.setRentEachYear(rentEachYear);
			irrCalculationData.setCamEachYear(camEachYear);
			irrCalculationData.setGetEBITDAForEachYear(getEBITDAForEachYear);
			irrCalculationData.setGetEBITDAMarginForEachYear(getEBITDAMarginForEachYear);
			irrCalculationData.setGetEBITForEachYear(getEBITForEachYear);

			irrCalculationData.setCumulativeEBITDAForEachYear(cumulativeEBITDAForEachYear);
			irrCalculationData.setGetEBITDAPayback(getEBITDAPayback);
			irrCalculationData.setTaxForEachYear(taxForEachYear);
			irrCalculationData.setCashInflowAfterTaxForEachYear(cashInflowAfterTaxForEachYear);
			irrCalculationData.setCapexForEachYear(capexForEachYear);
			irrCalculationData.setSecurityDepositForEachYear(securityDepositForEachYear);
			irrCalculationData.setDisposalOfAssets(disposalOfAssets);
			irrCalculationData.setTotalOutflowsForEachYear(totalOutflowsForEachYear);
			irrCalculationData.setFreeCashFlowPreTaxForEachYear(freeCashFlowPreTaxForEachYear);
			irrCalculationData.setFreeCashFlowPostTaxForEachYear(freeCashFlowPostTaxForEachYear);
			irrCalculationData.setCumulativeFCFPostTaxForEachYear(cumulativeFCFPostTaxForEachYear);
			irrCalculationData.setGetFCFPayback(getFCFPayback);

			irrCalculationData.setDrawdownForEachYear(drawdownForEachYear);
			irrCalculationData.setRepaymentForEachYear(repaymentForEachYear);
			irrCalculationData.setClosingDebtForEachYear(closingDebtForEachYear);
			irrCalculationData.setInterestUnderDebtScheduleForEachYear(interestUnderDebtScheduleForEachYear);
			irrCalculationData.setOpeningDebt(openingDebt);

			irrCalculationData.setInterestForEachYear(interestForEachYear);
			irrCalculationData.setGetPBTForEachYear(getPBTForEachYear);
			irrCalculationData.setGetTaxUnderPATForEachYear(getTaxUnderPATForEachYear);
			irrCalculationData.setGetPATForEachYear(getPATForEachYear);

			irrCalculationData.setLessCapexForEachYear(lessCapexForEachYear);
			irrCalculationData.setEquityCashFlowsForEachYear(equityCashFlowsForEachYear);

			irrCalculationData.setProjectIRRPreTax(projectIRRPreTax);
			irrCalculationData.setProjectIRRPostTax(projectIRRPostTax);

			irrCalculationData.setEquityIRR(equityIRR);
			irrCalculationData.setNpv(npv);
			irrCalculationData.setGetROCE(getROCE);

			irrCalculationData.setNetSaleOfTicket(netSaleOfTicket);
			irrCalculationData.setSaleOfFandB(saleOfFandB);
			irrCalculationData.setSponsorshipRevenue(sponsorshipRevenue);
			irrCalculationData.setVasIncomeNextYear(vasIncomeNextYear);
			irrCalculationData.setOtherOperatingRevenue(otherOperatingRevenue);
			irrCalculationData.setTotalRevenue(totalRevenue);
			irrCalculationData.setFilmDistributorShares(filmDistributorShares);
			irrCalculationData.setConsumptionFandB(consumptionFandB);
			irrCalculationData.setPersonnelExpenses(personnelExpenses);
			irrCalculationData.setElectricityAndWaterCharges(electricityAndWaterCharges);
			irrCalculationData.setRepairAndMaintaince(repairAndMaintaince);
			irrCalculationData.setOtherOperatingExpense(otherOperatingExpense);
			irrCalculationData.setExpenditureExRentAndCamp(expenditureExRentAndCamp);
			irrCalculationData.setEbitDar(ebitDar);
			irrCalculationData.setRentDetails(rentDetails);
			irrCalculationData.setCamDetails(camDetails);
			irrCalculationData.setEbitdaDetails(ebitdaDetails);
			irrCalculationData.setTotalFilmHireCostPercentage(assumptionsCalculationData.getTotalFilmHireCostPercentage());
			irrCalculationData.setVpfIncome(assumptionsCalculationData.getVpfIncome());
			
			details = projectDetailsDao.save(projectDetails.get());
			irrCalculationDao.save(irrCalculationData);

		} catch (Exception e) {
			log.error("error while calculating irr calculation");
		}

		return details;

	}

	public Map<Integer, Double> getAdmitsForEachYears(Double totalAdmits, List<FixedConstant> fixedConstant,Optional<ProjectDetails> projectDetails) {
		Map<Integer, Double> admitsForEachYears = new LinkedHashMap<Integer, Double>();
		try {
//			RevenueAssumptionsGrowthRate revenueAssumptions = fixedConstant.get(0).getRevenueAssumptionsGrowthRate();
			RevenueAssumptionsGrowthRate revenueAssumptions = projectDetails.get().getRevenueAssumptionsGrowthRate();

			Double yearOneFootfallAssumedLowerBy = !ObjectUtils.isEmpty(projectDetails.get().getYearOneFootfallAssumedLowerBy()) ? 
					projectDetails.get().getYearOneFootfallAssumedLowerBy() : fixedConstant.get(0).getYearOneFootfallAssumedLowerBy();
			
			Map<Integer, Double> admitGrowthByYearWise = revenueAssumptions.getAdmitsGrowth();
			
			int size = admitGrowthByYearWise.size() + 1;
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}
			 
			
			int growth = rate;

			Double admitsForFirstYear = totalAdmits * (1 - (yearOneFootfallAssumedLowerBy / 100));
			admitsForEachYears.put(1, admitsForFirstYear);
			admitsForEachYears.put(2, totalAdmits);

			admitGrowthByYearWise.forEach((key, value) -> {
				
				if(growth >= key) {
					if (!(key == 2)) {
						Double previousYearValue = !ObjectUtils.isEmpty(admitsForEachYears.get(key - 1)) ?
								          admitsForEachYears.get(key - 1) : new Double(1);
						Double admits = MathUtil
								.roundTwoDecimals(previousYearValue * (1 + (admitGrowthByYearWise.get(key) / 100)));
						admitsForEachYears.put(key, admits);

					}
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Admits For Each Years");
		}

		return admitsForEachYears;

	}

	public Map<Integer, Double> getOccupancyForEachYear(Map<Integer, Double> admitsYearWise, Double totalCapacity) {

		Map<Integer, Double> occupancyForEachYear = new LinkedHashMap<>();
		try {
			admitsYearWise.forEach((key, value) -> {

				Double occupancy = MathUtil.roundTwoDecimals((value / totalCapacity) * 100);
				occupancyForEachYear.put(key, occupancy);
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating occupancy for each year");
		}

		return occupancyForEachYear;
	}

	/*public Map<Integer, Double> getAtpForEachYear(List<FixedConstant> fixedConstants, Double firstYearTotalAtp,
			Optional<ProjectDetails> projectDetails, Map<String, Double> admitsForEachScreenType,
			Map<String, Map<String, Double>> admitsForEachScreenSeatType) {

		Map<Integer, Double> atpForEachYear = new LinkedHashMap<>();
		try {

			// Any year value= Previous year value*(1+growth rate for that year)
			Map<Integer, Double> growthRateOfAtp = fixedConstants.get(0).getRevenueAssumptionsGrowthRate()
					.getAtpGrowth();
			String state = projectDetails.get().getProjectLocation().getState();

			atpForEachYear.put(1, firstYearTotalAtp);
			Map<String, Map<String, Double>> atpForEachScreenSeatTypeForFirstYear = assumptionsCalculation
					.getAtpForEachScreenSeatType(projectDetails);

			Map<String, Map<String, Double>> newAtpForEachScreenSeatType = atpForEachScreenSeatTypeForFirstYear;

			List<AtpCapDetials> listOfAtpCapDetails = fixedConstants.get(0).getAtpCapDetails();
			Optional<AtpCapDetials> matchingObject = null;
			boolean flag = false;
			if (!ObjectUtils.isEmpty(listOfAtpCapDetails)) {
				matchingObject = listOfAtpCapDetails.stream().filter(p -> p.getState().equals(state)).findFirst();

				flag = matchingObject.isPresent();
			}
			Double atpCapGrowthRate = 0.0;
			Map<String, Map<String, Double>> atpDetails = null;
			if (flag) {

				atpDetails = matchingObject.get().getAtpDetails();
				 atpCapGrowthRate = matchingObject.get().getAtpGrowthRate() / 100;
			}

			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			Map<String, Map<String, Integer>> count = new LinkedHashMap<>();
			int countVal = 0;
			Double totalAtp = null;
			
			int size = growthRateOfAtp.size() + 1;
			
			
			int rate = 0;
			
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			
			
			

			for (int n = 2; n <= growth; n++) {
				if (flag) {

					Set<String> screenFormat = newAtpForEachScreenSeatType.keySet();
					List<String> screenFormatList = new ArrayList<>(screenFormat);

					Set<String> screenFormatInCap = atpDetails.keySet();
					List<String> screenFormatInCapList = new ArrayList<>(screenFormatInCap);

					for (int i = 0; i < screenFormatList.size(); i++) {
						Map<String, Double> atpValueBySeatType = newAtpForEachScreenSeatType
								.get(screenFormatList.get(i));
						Set<String> atpSeatType = atpValueBySeatType.keySet();
						List<String> atpSeatTypeList = new ArrayList<>(atpSeatType);

						// Any year value= Previous year value*(1+growth rate for that year)

						boolean isPresent = screenFormatInCapList.contains(screenFormatList.get(i));
						Map<String, Double> capAtpValueBySeatType = null;
						List<String> capSeatTypeList = null;
						if (isPresent) {
							capAtpValueBySeatType = atpDetails.get(screenFormatList.get(i));
							Set<String> capSeatType = capAtpValueBySeatType.keySet();
							capSeatTypeList = new ArrayList<>(capSeatType);
							
						}
						
						

						Map<String, Double> atpSeatTypeData = new LinkedHashMap<>();
						for (int j = 0; j < atpSeatTypeList.size(); j++) {

							Double atp = atpValueBySeatType.get(atpSeatTypeList.get(j))
									* (1 + (growthRateOfAtp.get(n) / 100));
							boolean seatPresent = false;
							if (isPresent) {
								seatPresent = capAtpValueBySeatType.containsKey(atpSeatTypeList.get(j));
							}

							if (seatPresent) {
								if (atp >= capAtpValueBySeatType.get(atpSeatTypeList.get(j))) {

									if (count.containsKey(screenFormatList.get(i))) {

										Map<String, Integer> countCap = count.get(screenFormatList.get(i));
										if (countCap.containsKey(atpSeatTypeList.get(j))) {

											Map<String, Integer> countSeat = count.get(screenFormatList.get(i));
											Integer val = countSeat.get(atpSeatTypeList.get(j));
											countSeat.put(atpSeatTypeList.get(j), val + 1);
											count.put(screenFormatList.get(i), countSeat);

										} else {
											countCap.put(atpSeatTypeList.get(j), 1);
											count.put(screenFormatList.get(i), countCap);
											countVal = 0;
										}

									} else {
										Map<String, Integer> countCap = new LinkedHashMap<>();
										countCap.put(atpSeatTypeList.get(j), 1);
										count.put(screenFormatList.get(i), countCap);
										countVal = 0;
									}

									Map<String, Integer> counterCap = count.get(screenFormatList.get(i));
									int val = counterCap.get(atpSeatTypeList.get(j));
									atpSeatTypeData.put(atpSeatTypeList.get(j),
											capAtpValueBySeatType.get(atpSeatTypeList.get(j)));
						

								} else {
									atpSeatTypeData.put(atpSeatTypeList.get(j), atp);
								}
							} else {
								atpSeatTypeData.put(atpSeatTypeList.get(j), atp);
							}

						}
						
							if (isPresent) {
							
							if ((n % 5 ) == 0) {

								Map<String, Double> capData = atpDetails.get(screenFormatList.get(i));
								
								for(int k=0;k<capData.size();k++) {
									
									Double capDataValue = capData.get(atpSeatTypeList.get(k));

									Double growthRate = capDataValue * atpCapGrowthRate;
									Double capWithGrowthRate = capDataValue + growthRate;

									capData.put(atpSeatTypeList.get(k), capWithGrowthRate);
									atpDetails.put(screenFormatList.get(i), capData);

									Map<String, Integer> countSeat = count.get(screenFormatList.get(i));
									if(!ObjectUtils.isEmpty(countSeat)) {
										countSeat.put(atpSeatTypeList.get(k), 0);
										count.put(screenFormatList.get(i), countSeat);
									}
									
								}
								

							}
							
							
						}
						
				
						newAtpForEachScreenSeatType.put(screenFormatList.get(i), atpSeatTypeData);
					}
					
					
					
					
					

					Map<String, Double> totalAtpForEachFormat = assumptionsCalculation
							.getTotalAtpForEachFormat(admitsForEachScreenSeatType, newAtpForEachScreenSeatType);
					totalAtp = assumptionsCalculation.getTotalAtp(admitsForEachScreenType, totalAtpForEachFormat);
					atpForEachYear.put(n, totalAtp);

				} else {
					totalAtp = MathUtil
							.roundTwoDecimals(atpForEachYear.get(n - 1) * (1 + (growthRateOfAtp.get(n) / 100)));
					atpForEachYear.put(n, totalAtp);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Atp For Each Year");
		}
		return atpForEachYear;

	}*/
	
	
	public Map<Integer, Double> getAtpForEachYear(List<FixedConstant> fixedConstants, Double firstYearTotalAtp,
			Optional<ProjectDetails> projectDetails, Map<String, Double> admitsForEachScreenType,
			Map<String, Map<String, Double>> admitsForEachScreenSeatType) {

		Map<Integer, Double> atpForEachYear = new LinkedHashMap<>();
		try {

			// Any year value= Previous year value*(1+growth rate for that year)
//			Map<Integer, Double> growthRateOfAtp = fixedConstants.get(0).getRevenueAssumptionsGrowthRate()
//					.getAtpGrowth();
			Map<Integer, Double> growthRateOfAtp = projectDetails.get().getRevenueAssumptionsGrowthRate()
					.getAtpGrowth();
			
			String state = projectDetails.get().getProjectLocation().getState();

			atpForEachYear.put(1, firstYearTotalAtp);
			Map<String, Map<String, Double>> atpForEachScreenSeatTypeForFirstYear = assumptionsCalculation
					.getAtpForEachScreenSeatType(projectDetails);
			
			Map<String, Map<String, Double>> atpForEachScreenSeatTypeForYear = atpForEachScreenSeatTypeForFirstYear;
			
			Map<String, Map<String, Double>> atpForEachScreenSeatTypeForNextYear = null;

			Map<String, Map<String, Double>> newAtpForEachScreenSeatType = atpForEachScreenSeatTypeForFirstYear;

			List<AtpCapDetials> listOfAtpCapDetails = fixedConstants.get(0).getAtpCapDetails();
			Optional<AtpCapDetials> matchingObject = null;
			boolean flag = false;
			if (!ObjectUtils.isEmpty(listOfAtpCapDetails)) {
				matchingObject = listOfAtpCapDetails.stream().filter(p -> p.getState().equals(state)).findFirst();

				flag = matchingObject.isPresent();
			}
			Double atpCapGrowthRate = 0.0;
			Map<String, Map<String, Double>> atpDetails = null;
			if (flag) {

				atpDetails = matchingObject.get().getAtpDetails();
				 atpCapGrowthRate = matchingObject.get().getAtpGrowthRate() / 100;
			}

			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			Map<String, Map<String, Integer>> count = new LinkedHashMap<>();
			int countVal = 0;
			Double totalAtp = null;
			
			int size = growthRateOfAtp.size() + 1;
			
			
			int rate = 0;
			
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			
			
			

			for (int n = 2; n <= growth; n++) {
				if (flag) {
					
					 atpForEachScreenSeatTypeForNextYear = getAtpForEachScreenSeatTypeForNextYear(atpForEachScreenSeatTypeForYear,growthRateOfAtp.get(n));
					 Iterator<String> itr=atpForEachScreenSeatTypeForNextYear.keySet().iterator();
					 while(itr.hasNext()) {
						 String key=itr.next();
						 atpForEachScreenSeatTypeForYear.put(key, atpForEachScreenSeatTypeForNextYear.get(key));
					 }
					 
					 newAtpForEachScreenSeatType = atpForEachScreenSeatTypeForNextYear;
					 atpForEachScreenSeatTypeForNextYear = null;

					Set<String> screenFormat = newAtpForEachScreenSeatType.keySet();
					List<String> screenFormatList = new ArrayList<>(screenFormat);

					Set<String> screenFormatInCap = atpDetails.keySet();
					List<String> screenFormatInCapList = new ArrayList<>(screenFormatInCap);

					for (int i = 0; i < screenFormatList.size(); i++) {
						Map<String, Double> atpValueBySeatType = newAtpForEachScreenSeatType
								.get(screenFormatList.get(i));
						Set<String> atpSeatType = atpValueBySeatType.keySet();
						List<String> atpSeatTypeList = new ArrayList<>(atpSeatType);

						// Any year value= Previous year value*(1+growth rate for that year)

						boolean isPresent = screenFormatInCapList.contains(screenFormatList.get(i));
						Map<String, Double> capAtpValueBySeatType = null;
						List<String> capSeatTypeList = null;
						if (isPresent) {
							capAtpValueBySeatType = atpDetails.get(screenFormatList.get(i));
							Set<String> capSeatType = capAtpValueBySeatType.keySet();
							capSeatTypeList = new ArrayList<>(capSeatType);
							
						}
						
						

						Map<String, Double> atpSeatTypeData = new LinkedHashMap<>();
						for (int j = 0; j < atpSeatTypeList.size(); j++) {

							Double atp = atpValueBySeatType.get(atpSeatTypeList.get(j));
							boolean seatPresent = false;
							if (isPresent) {
								seatPresent = capAtpValueBySeatType.containsKey(atpSeatTypeList.get(j));
							}

							if (seatPresent) {
								if (atp >= capAtpValueBySeatType.get(atpSeatTypeList.get(j))) {

									if (count.containsKey(screenFormatList.get(i))) {

										Map<String, Integer> countCap = count.get(screenFormatList.get(i));
										if (countCap.containsKey(atpSeatTypeList.get(j))) {

											Map<String, Integer> countSeat = count.get(screenFormatList.get(i));
											Integer val = countSeat.get(atpSeatTypeList.get(j));
											countSeat.put(atpSeatTypeList.get(j), val + 1);
											count.put(screenFormatList.get(i), countSeat);

										} else {
											countCap.put(atpSeatTypeList.get(j), 1);
											count.put(screenFormatList.get(i), countCap);
											countVal = 0;
										}

									} else {
										Map<String, Integer> countCap = new LinkedHashMap<>();
										countCap.put(atpSeatTypeList.get(j), 1);
										count.put(screenFormatList.get(i), countCap);
										countVal = 0;
									}

									Map<String, Integer> counterCap = count.get(screenFormatList.get(i));
									int val = counterCap.get(atpSeatTypeList.get(j));
									atpSeatTypeData.put(atpSeatTypeList.get(j),
											capAtpValueBySeatType.get(atpSeatTypeList.get(j)));
						

								} else {
									atpSeatTypeData.put(atpSeatTypeList.get(j), atp);
								}
							} else {
								atpSeatTypeData.put(atpSeatTypeList.get(j), atp);
							}

						}
						
							if (isPresent) {
							
							if ((n % 5 ) == 0) {

								Map<String, Double> capData = atpDetails.get(screenFormatList.get(i));
								
								for(int k=0;k<capData.size();k++) {
									
									Double capDataValue = capData.get(atpSeatTypeList.get(k));

									Double growthRate = capDataValue * atpCapGrowthRate;
									Double capWithGrowthRate = capDataValue + growthRate;

									capData.put(atpSeatTypeList.get(k), capWithGrowthRate);
									atpDetails.put(screenFormatList.get(i), capData);

									Map<String, Integer> countSeat = count.get(screenFormatList.get(i));
									if(!ObjectUtils.isEmpty(countSeat)) {
										countSeat.put(atpSeatTypeList.get(k), 0);
										count.put(screenFormatList.get(i), countSeat);
									}
									
								}
								

							}
							
							
						}
						
				
						newAtpForEachScreenSeatType.put(screenFormatList.get(i), atpSeatTypeData);
					}
					
					
					
					
					

					Map<String, Double> totalAtpForEachFormat = assumptionsCalculation
							.getTotalAtpForEachFormat(admitsForEachScreenSeatType, newAtpForEachScreenSeatType);
					totalAtp = assumptionsCalculation.getTotalAtp(admitsForEachScreenType, totalAtpForEachFormat);
					atpForEachYear.put(n, totalAtp);

				} else {
					totalAtp = MathUtil
							.roundTwoDecimals(atpForEachYear.get(n - 1) * (1 + (growthRateOfAtp.get(n) / 100)));
					atpForEachYear.put(n, totalAtp);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Atp For Each Year");
		}
		return atpForEachYear;

	}
	
	
	
	public Map<String, Map<String, Double>> getAtpForEachScreenSeatTypeForNextYear(Map<String, Map<String, Double>> atpForEachScreenSeatTypeForFirstYear,Double growthRateOfAtp) {
		
		
		Map<String, Map<String, Double>> nextYearAtpForEachScreenSeatType = new HashMap<>();
		
		atpForEachScreenSeatTypeForFirstYear.forEach((key,value)->{
			
			Double normal =  value.get(Constants.NORMAL) + (value.get(Constants.NORMAL) * (growthRateOfAtp / Constants.HUNDRED));
			Double recliner = value.get(Constants.RECLINER) + (value.get(Constants.RECLINER) * (growthRateOfAtp / Constants.HUNDRED));
			Double lounger = value.get(Constants.LOUNGER) + (value.get(Constants.LOUNGER)  * (growthRateOfAtp / Constants.HUNDRED));

			Map<String, Double> atpValueByScreenType = new HashMap<>();
			atpValueByScreenType.put(Constants.NORMAL, MathUtil.roundTwoDecimals(normal));
			atpValueByScreenType.put(Constants.RECLINER,  MathUtil.roundTwoDecimals(recliner));
			atpValueByScreenType.put(Constants.LOUNGER,  MathUtil.roundTwoDecimals(lounger));
			
			nextYearAtpForEachScreenSeatType.put(key, atpValueByScreenType);
			
		});
		
		return nextYearAtpForEachScreenSeatType;
	}
	

	public Map<Integer, Double> getGrossTicketSalesForEachYear(Map<Integer, Double> atpForEachYear,
			Map<Integer, Double> admitsYearWise) {

		Map<Integer, Double> grossTicketSalesForEachYear = new LinkedHashMap<>();
		try {
			atpForEachYear.forEach((key, value) -> {

				Double grossTicketSale = MathUtil.roundTwoDecimals(value * admitsYearWise.get(key));
				grossTicketSalesForEachYear.put(key, grossTicketSale);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating gross ticket sales for each year");
		}

		return grossTicketSalesForEachYear;
	}

	public Map<Integer, Double> getGstForEachYear(Double netGstOnBoxOffice,
			Map<Integer, Double> grossTicketSalesForEachYear) {

		Map<Integer, Double> gstForEachYear = new LinkedHashMap<>();
		try {
			grossTicketSalesForEachYear.forEach((key, value) -> {
				// Gross ticket sales*(ticket GST rate/(1+ticket GST rate))
				Double gst = MathUtil
						.roundTwoDecimals(value * ((netGstOnBoxOffice / 100) / (1 + (netGstOnBoxOffice / 100))));
				gstForEachYear.put(key, gst);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating gst for each year");
		}

		return gstForEachYear;
	}

	public Map<Integer, Double> getHealthCess(List<FixedConstant> fixedConstant, Map<Integer, Double> admitsYearWise,
			String state) {
		// fixed, only for kerela=3 else 0)
		Map<Integer, Double> healthCessYearWise = new LinkedHashMap<>();
		try {
			admitsYearWise.forEach((key, value) -> {

				Map<String, Double> healthCessRate = fixedConstant.get(0).getHealthCess();

				boolean flag = healthCessRate.containsKey(state);

				Double fixedRate = 0.0;
				if (flag) {
					fixedRate = healthCessRate.get(state);
				}

				Double healthCess = MathUtil.roundTwoDecimals(fixedRate * value);
				healthCessYearWise.put(key, healthCess);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating health cess for each year");
		}

		return healthCessYearWise;
	}

	public Map<Integer, Double> getServiceChargePerYear(List<FixedConstant> fixedConstant,
			Map<Integer, Double> admitsYearWise, Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> serviceChargePerYear = new LinkedHashMap<>();
		try {
			String state = projectDetails.get().getProjectLocation().getState();
			Map<String, Double> serviceChargePerTicket = fixedConstant.get(0).getServiceChargePerTicket();
			Double serviceChargeOfState = 0.0;
			if (serviceChargePerTicket.containsKey(state)) {
				serviceChargeOfState = serviceChargePerTicket.get(state);
			} else {
				serviceChargeOfState = 0.0;
			}

			Double charge = serviceChargeOfState;
			admitsYearWise.forEach((key, value) -> {
				Double serviceCharge = MathUtil.roundTwoDecimals(charge * value);
				serviceChargePerYear.put(key, serviceCharge);
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating service charge for each year");
		}

		return serviceChargePerYear;
	}

	public Map<Integer, Double> getLbtExpenseForEachYear(List<FixedConstant> fixedConstant,
			Map<Integer, Double> grossTicketSalesForEachYear, Map<Integer, Double> gstForEachYear,
			Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> lbtExpenseForEachYear = new LinkedHashMap<>();
		try {
			Map<String, Double> lbtAverageRate = fixedConstant.get(0).getLbtAverageRate();
			String state = projectDetails.get().getProjectLocation().getState();
			Double rateValue = 0.0;
			if (lbtAverageRate.containsKey(state)) {
				rateValue = lbtAverageRate.get(state);
			} else {
				rateValue = 0.0;
			}
			Double lbtRate = rateValue;
			grossTicketSalesForEachYear.forEach((key, value) -> {
				Double gst = gstForEachYear.get(key);
				Double lbtExpense = MathUtil
						.roundTwoDecimals((value - gst) * ((lbtRate / 100) / (1 + (lbtRate / 100))));
				lbtExpenseForEachYear.put(key, lbtExpense);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating lbt expense for each year");
		}

		return lbtExpenseForEachYear;
	}

	public Map<Integer, Double> getNetTicketSalesForFhcForEachYear(Map<Integer, Double> grossTicketSalesForEachYear,
			Map<Integer, Double> gstForEachYear, Map<Integer, Double> showTax,
			Map<Integer, Double> serviceChargePerYear, Map<Integer, Double> lbtExpenseForEachYear) {

		Map<Integer, Double> netTicketSalesForFhcForEachYear = new LinkedHashMap<>();

		try {
	
			grossTicketSalesForEachYear.forEach((key, value) -> {

				Double gst = gstForEachYear.get(key);
				Double serviceCharge = serviceChargePerYear.get(key);
				Double lbtExpense = lbtExpenseForEachYear.get(key);
				Double value1 = value - gst - showTax.get(key) - serviceCharge - lbtExpense;
				Double netTicketSalesForFhc = MathUtil.roundTwoDecimals(value1);
				netTicketSalesForFhcForEachYear.put(key, netTicketSalesForFhc);
			});
			//System.out.println(netTicketSalesForFhcForEachYear);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating net ticket sales for each year", e);
		}

		return netTicketSalesForFhcForEachYear;
	}

	public Map<Integer, Double> getNet3dSales(List<FixedConstant> fixedConstant,
			Map<Integer, Double> grossTicketSalesForEachYear, Double netGstOnBoxOffice,
			Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> net3dsalesForEach = new LinkedHashMap<>();
		try {

			boolean flag = projectDetails.get().isIncome3dApplicable();
			grossTicketSalesForEachYear.forEach((key, value) -> {

				Double income3DAsPercentOfGboc = 0.0;
				if (flag) {
					income3DAsPercentOfGboc = fixedConstant.get(0).getIncome3DAsPercentOfGboc();
				}
				Double net3dsales = MathUtil.roundTwoDecimals((income3DAsPercentOfGboc / 100) * value
						* ((1 - (netGstOnBoxOffice / 100) / (1 + (netGstOnBoxOffice / 100)))));
				net3dsalesForEach.put(key, net3dsales);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating 3D net sales");
		}

		return net3dsalesForEach;
	}

	public Map<Integer, Double> getNetComplimentaryTicketsForEachYear(List<FixedConstant> fixedConstant,
			Map<Integer, Double> grossTicketSalesForEachYear, Double netGstOnBoxOffice) {

		Map<Integer, Double> netComplimentaryTicketsForEachYear = new LinkedHashMap<>();
		try {
			grossTicketSalesForEachYear.forEach((key, value) -> {

				Double complimentaryTicketsPercent = fixedConstant.get(0).getComplimentaryTicketsPercent();
				Double netComplimentaryTickets = MathUtil.roundTwoDecimals((-complimentaryTicketsPercent / 100) * value
						* (1 - (netGstOnBoxOffice / 100) / (1 + (netGstOnBoxOffice / 100))));
				netComplimentaryTicketsForEachYear.put(key, netComplimentaryTickets);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating net complimentary  Tickets");
		}

		return netComplimentaryTicketsForEachYear;
	}

	public Map<Integer, Double> getNetTicketSalesForEachYear(Map<Integer, Double> serviceChargePerYear,
			Map<Integer, Double> netTicketSalesForFhcForEachYear, Map<Integer, Double> net3dSales,
			Map<Integer, Double> netComplimentaryTickets) {

		Map<Integer, Double> netTicketSalesForEachYear = new LinkedHashMap<>();

		try {
			netTicketSalesForFhcForEachYear.forEach((key, value) -> {

				Double serviceCharge = serviceChargePerYear.get(key);
				Double net3dSale = net3dSales.get(key);
				Double complimentaryTickets = netComplimentaryTickets.get(key);

				Double ticketSalesForEachYear = MathUtil
						.roundTwoDecimals(serviceCharge + value + net3dSale + complimentaryTickets);
				netTicketSalesForEachYear.put(key, ticketSalesForEachYear);

			});
			//System.out.println(netTicketSalesForFhcForEachYear);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating net ticket sales for each year");
		}

		return netTicketSalesForEachYear;

	}

	public Map<Integer, Double> getSphForEachYear(Double totalSph, List<FixedConstant> fixedConstant,
			Map<Integer, Double> atpForEachYear,Optional<ProjectDetails> projectDetails) {

		Double sphToAtpRatio = fixedConstant.get(0).getSphToAtpRatio();
		
		// Any year value= Previous year value*(1+growth rate for that year)
		Map<Integer, Double> sphForEachYear = new LinkedHashMap<>();
		try {

			sphForEachYear.put(1, totalSph);
//			RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate = fixedConstant.get(0)
//					.getRevenueAssumptionsGrowthRate();
			RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate = projectDetails.get()
					.getRevenueAssumptionsGrowthRate();
			
			Map<Integer, Double> sphGrowth = revenueAssumptionsGrowthRate.getSphGrowth();

			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = sphGrowth.size() + 1;
			
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}
			
			int growth = rate;
			
			
			
			sphGrowth.forEach((key, value) -> {

				if(growth >= key) {
					Double sphWithGrowth = MathUtil.roundTwoDecimals((!ObjectUtils.isEmpty(sphForEachYear.get(key - 1)) 
							? sphForEachYear.get(key - 1) : 1) * (1 + (value / 100)));

					Double ratioPercent = (sphWithGrowth / atpForEachYear.get(key)) * 100;
					if (ratioPercent > sphToAtpRatio) {
						Double sphForYear = atpForEachYear.get(key) * sphToAtpRatio;
						sphForEachYear.put(key, sphForYear / 100);

					} else {
						sphForEachYear.put(key, sphWithGrowth);
					}
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating sph for each year");
		}
		return sphForEachYear;

	}

	public Map<Integer, Double> getGrossFnBForEachYear(Map<Integer, Double> sphForEachYear,
			Map<Integer, Double> admitsYearWise) {

		// SPH (calculated) * Admits (calculated) for each year
		Map<Integer, Double> grossFnBForEachYear = new LinkedHashMap<>();
		try {
			sphForEachYear.forEach((key, value) -> {

				Double sphByYear = MathUtil.roundTwoDecimals(value * admitsYearWise.get(key));
				grossFnBForEachYear.put(key, sphByYear);
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating gross f&B for each year");
		}

		return grossFnBForEachYear;
	}

	public Map<Integer, Double> getGstFoodForEachYear(Map<Integer, Double> grossFnBForEachYear,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> gstFoodForEachYear = new LinkedHashMap<>();
		Double gstPercentFood = fixedConstant.get(0).getFnbGst();
		try {
			// Gross F&B * (GST% food/1+GST% food)
			grossFnBForEachYear.forEach((key, value) -> {

				Double gstFood = MathUtil
						.roundTwoDecimals(value * ((gstPercentFood / 100) / (1 + (gstPercentFood / 100))));
				gstFoodForEachYear.put(key, gstFood);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating gst food for each year");
		}

		return gstFoodForEachYear;
	}

	public Map<Integer, Double> getNetFnBRevenueForCogs(Map<Integer, Double> grossFnBForEachYear,
			Map<Integer, Double> gstFoodForEachYear) {

		Map<Integer, Double> netFnBRevenueForCogsForEachYear = new LinkedHashMap<>();

		try {
			grossFnBForEachYear.forEach((key, value) -> {
				// Gross F&B – GST (food) for each year
				Double fnbRevenueForCogs = MathUtil.roundTwoDecimals(value - gstFoodForEachYear.get(key));
				netFnBRevenueForCogsForEachYear.put(key, fnbRevenueForCogs);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating net f&b revenue cogs for each year");
		}

		return netFnBRevenueForCogsForEachYear;
	}

	public Map<Integer, Double> getNetComplimentaryFnBForEachYear(Map<Integer, Double> grossFnBForEachYear,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> netComplimentaryFnBForEachYear = new LinkedHashMap<>();
		Double complimentaryFnBPercent = fixedConstant.get(0).getComplimentaryFnBPercent();
		try {
			// Gross F&B * complimentary F&B % (Fixed)
			grossFnBForEachYear.forEach((key, value) -> {

				Double netComplimentaryFnB = MathUtil.roundTwoDecimals(-value * (complimentaryFnBPercent / 100));
				netComplimentaryFnBForEachYear.put(key, netComplimentaryFnB);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating net complimentary F&B for each year");
		}

		return netComplimentaryFnBForEachYear;
	}

	public Map<Integer, Double> getNetFnBSalesForEachYear(Map<Integer, Double> netFnBRevenueForCogs,
			Map<Integer, Double> netComplimentaryFnBForEachYear) {

		Map<Integer, Double> netFnBSalesForEachYear = new LinkedHashMap<>();

		try {
			// Net F&B revenue for COGS +complimentary F&B (net)
			netComplimentaryFnBForEachYear.forEach((key, value) -> {

				Double netFnBSales = MathUtil.roundTwoDecimals(netFnBRevenueForCogs.get(key) + value);
				netFnBSalesForEachYear.put(key, netFnBSales);

			});
		} catch (Exception e) {
			log.error("error while calculating net F&B sales for each year");
		}

		return netFnBSalesForEachYear;
	}

	public Map<Integer, Double> getConveniencefeeForEachYear(List<FixedConstant> fixedConstant,
			AssumptionsCalculationData assumptionsCalculationData,Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> convenienceFeeForEachYear = new LinkedHashMap<>();

		try {
//			RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate = fixedConstant.get(0)
//					.getRevenueAssumptionsGrowthRate();
			RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate = projectDetails.get()
					.getRevenueAssumptionsGrowthRate();
			
			convenienceFeeForEachYear.put(1, assumptionsCalculationData.getNetConvenienceFee());

			Map<Integer, Double> convenienceFeeGrowth = revenueAssumptionsGrowthRate.getConvenienceFeeGrowth();
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = convenienceFeeGrowth.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;

			convenienceFeeGrowth.forEach((key, value) -> {
				// Any year value= Previous year value*(1+growth rate for that year)
				if(growth >= key) {
					Double convenienceFee = MathUtil
							.roundTwoDecimals((!ObjectUtils.isEmpty(convenienceFeeForEachYear.get(key - 1)) 
									? convenienceFeeForEachYear.get(key - 1) : 1) * (1 + (value / 100)));
					convenienceFeeForEachYear.put(key, convenienceFee);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating convenience fee for each year");
		}

		return convenienceFeeForEachYear;
	}

	public Map<Integer, Double> getRateAndTaxes(Map<Integer, Double> showTax, Map<Integer, Double> healthCess) {

		Map<Integer, Double> rateAndTaxesForEachYear = new LinkedHashMap<>();

		try {
			healthCess.forEach((key, value) -> {
				// Show tax + health cess for each year
				Double rateAndTax = MathUtil.roundTwoDecimals(showTax.get(key) + value);
				rateAndTaxesForEachYear.put(key, rateAndTax);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating rate and tax for each year");
		}

		return rateAndTaxesForEachYear;
	}

	public Map<Integer, Double> getMunicipalTaxForEachYear(Double municipalTaxesPerAnnum,
			List<FixedConstant> fixedConstant,Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> municipalTaxForEachYear = new LinkedHashMap<>();

		try {
//			CostAssumptionsGrowthRate costAssumptionsGrowthRate = fixedConstant.get(0).getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumption = projectDetails.get()
					.getCostAssumptionsGrowthRate();
			
			Map<Integer, Double> propertyTax = costAssumption.getPropertyTax();
			municipalTaxForEachYear.put(1, municipalTaxesPerAnnum);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = propertyTax.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			propertyTax.forEach((year, growthRate) -> {
				
				if(growth >= year) {
					// Any year value= Previous year value*(1+growth rate for that year)

					Double municipalTax = MathUtil
							.roundTwoDecimals((!ObjectUtils.isEmpty(municipalTaxForEachYear.get(year - 1)) 
									? municipalTaxForEachYear.get(year - 1) : 1) * (1 + (growthRate / 100)));
					municipalTaxForEachYear.put(year, municipalTax);
				}

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating municipal tax for each year");
		}

		return municipalTaxForEachYear;
	}

	/*
	 * public Map<Integer, Double> getHousekeepingFee(List<FixedConstant>
	 * fixedConstant, Optional<ProjectDetails> projectDetails) { Map<Integer,
	 * Double> housekeeping = new LinkedHashMap<>(); try { CostAssumptionsGrowthRate
	 * costAssumption = fixedConstant.get(Constants.INDEX_0)
	 * .getCostAssumptionsGrowthRate(); Map<Integer, Double> otherOverheadExpenses =
	 * costAssumption.getOtherOverheadExpenses(); housekeeping.put(Constants.ONE,
	 * (double) projectAnalysisService.getProjectHousekeepingExpense(null,
	 * projectDetails)); otherOverheadExpenses.forEach((year, growthRate) -> {
	 * Double housekeppingForeachYear = housekeeping.get(year - Constants.ONE) *
	 * (Constants.ONE + growthRate); housekeeping.put(year,
	 * housekeppingForeachYear); }); } catch (Exception e) {
	 * log.error("error while fetching housekeeping fee for each years"); }
	 * 
	 * return housekeeping; }
	 */

	public Double getCostOfDebtPostTax(List<FixedConstant> fixedConstant) {

		Double costOfDebtPostTax = 0.0;
		try {
			// cost of debt in fixed value *(1-Income tax)
			Double costOfDebt = fixedConstant.get(0).getCostOfDebt();
			Double incomeTax = fixedConstant.get(0).getIncomeTax() / 100;
			costOfDebtPostTax = MathUtil.roundTwoDecimals(costOfDebt * (1 - incomeTax));
		} catch (Exception e) {
			log.error("error while calculating cost of debt post tax");
		}

		return costOfDebtPostTax;
	}

	public Double getWacc(List<FixedConstant> fixedConstant, Double costOfDebtPostTax) {

		Double wacc = 0.0;
		try {
			Double costOfEquity = fixedConstant.get(0).getCostOfEquity() / 100;
			Double debtProportion = fixedConstant.get(0).getDebtProportion() / 100;
			Double equityProportion = fixedConstant.get(0).getEquityProportion() / 100;

			// (cost of equity * equity proportion + cost of debt post tax*debt proportion)
			wacc = MathUtil.roundTwoDecimals(
					((costOfEquity * equityProportion) + ((costOfDebtPostTax / 100) * debtProportion)) * 100);

		} catch (Exception e) {
			log.error("error while calculating wacc");
		}

		return wacc;
	}

	public Map<Integer, Double> getVpfIncomeYearWise(Double vpfIncome, Optional<ProjectDetails> projectDetails,List<FixedConstant> fixedConstants) {

		Map<Integer, Double> vpfIncomeYearWise = new LinkedHashMap<>();

		try {
//			Map<Integer,Double>  growthRate  =  fixedConstants.get(0).getRevenueAssumptionsGrowthRate().getAdmitsGrowth();
			Map<Integer,Double>  growthRate  =  projectDetails.get().getRevenueAssumptionsGrowthRate().getAdmitsGrowth();

			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			String dateOfOpening = projectDetails.get().getDateOfOpening();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
			LocalDate dateTime = LocalDate.parse(dateOfOpening, formatter);
			int currentYear = dateTime.getYear();

			int fixedYear = 2024;
			int count = fixedYear - currentYear;
			int size = growthRate.size() + 1;
			
			int rate = 0;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}
			
			for (int i = 1; i <= rate; i++) {

				if (count <= 0) {
					vpfIncomeYearWise.put(i, 0.0);
				} else {
					vpfIncomeYearWise.put(i, vpfIncome);
				}
				count--;
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating vpf income");
		}

		return vpfIncomeYearWise;
	}

	public Map<Integer, Double> getOtherOperatingIncome(Map<Integer, Double> vpfIncomeYearWise,
			Map<Integer, Double> miscellaneousIncomeForEachYear) {

		Map<Integer, Double> otherOperatingIncome = new LinkedHashMap<>();
		try {
			// sum of VPF and misc. income
			miscellaneousIncomeForEachYear.forEach((year, value) -> {
				Double otherIncome = MathUtil.roundTwoDecimals(value + vpfIncomeYearWise.get(year));
				otherOperatingIncome.put(year, otherIncome);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating other operating income");
		}

		return otherOperatingIncome;
	}

	public Map<Integer, Double> getTotalRevenueForEachYear(Map<Integer, Double> netTicketSales,
			Map<Integer, Double> netFnBSalesForEachYear, Map<Integer, Double> sponsorshipRevenueForEachYear,
			Map<Integer, Double> conveniencefeeForEachYear, Map<Integer, Double> otherOperatingIncome) {

		Map<Integer, Double> totalRevenueForEachYear = new LinkedHashMap<>();

		try {
			netTicketSales.forEach((year, value) -> {

				Double totalRevenu = MathUtil.roundTwoDecimals(
						value + netFnBSalesForEachYear.get(year) + sponsorshipRevenueForEachYear.get(year)
								+ conveniencefeeForEachYear.get(year) + otherOperatingIncome.get(year));
				totalRevenueForEachYear.put(year, totalRevenu);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating total revenu");
		}

		return totalRevenueForEachYear;
	}

	public Map<Integer, Double> getFilmDistributorShare(Map<Integer, Double> netTicketSalesForFhcForEachYear,
			Double totalFilmHireCostPercentage) {

		// Net ticket sales for FHC * Total FHC %
		Map<Integer, Double> filmDistributorShareForEachYear = new LinkedHashMap<>();

		try {
			netTicketSalesForFhcForEachYear.forEach((year, value) -> {

				Double filmDistributorShare = MathUtil.roundTwoDecimals(value * (totalFilmHireCostPercentage / 100));
				filmDistributorShareForEachYear.put(year, filmDistributorShare);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating film Distributor Share");
		}

		return filmDistributorShareForEachYear;
	}

	public Map<Integer, Double> getConsumptionOfFnBForEachYear(Map<Integer, Double> netFnBRevenueForCogs,
			Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> consumptionOfFnBForEachYear = new LinkedHashMap<>();

		try {
			// Net F&B revenue for COGS (step 17)* COGS(fixed)
			Double fnbCogs = !ObjectUtils.isEmpty(projectDetails.get().getCogs().getInputValues())
					? projectDetails.get().getCogs().getInputValues() : projectDetails.get().getCogs().getSuggestedValue();
					
			netFnBRevenueForCogs.forEach((key, value) -> {

				Double consumptionOfFnB = MathUtil.roundTwoDecimals(value * (fnbCogs / 100));
				consumptionOfFnBForEachYear.put(key, consumptionOfFnB);
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating consumption Of FnB");
		}

		return consumptionOfFnBForEachYear;
	}

	public Map<Integer, Double> getGrossMarginForEachYear(Map<Integer, Double> totalRevenueForEachYear,
			Map<Integer, Double> filmDistributorShare, Map<Integer, Double> consumptionOfFnBForEachYear) {

		Map<Integer, Double> grossMarginForEachYear = new LinkedHashMap<>();
		try {
			// Total revenue – Film distributor’s share – Consumption of F&B
			totalRevenueForEachYear.forEach((key, value) -> {

				Double grossMargin = MathUtil
						.roundTwoDecimals(value - filmDistributorShare.get(key) - consumptionOfFnBForEachYear.get(key));
				grossMarginForEachYear.put(key, grossMargin);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating gross Margin for each year");
		}

		return grossMarginForEachYear;
	}

	public Map<Integer, Double> getGrossMarginPercent(Map<Integer, Double> grossMarginForEachYear,
			Map<Integer, Double> totalRevenueForEachYear) {

		Map<Integer, Double> getGrossMarginPercent = new LinkedHashMap<>();

		try {
			// Gross Margin/Total revenue
			grossMarginForEachYear.forEach((key, value) -> {

				Double grossMarginPercent = MathUtil.roundTwoDecimals((value / totalRevenueForEachYear.get(key)) * 100);
				getGrossMarginPercent.put(key, grossMarginPercent);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating gross Margin Percent for each year");
		}

		return getGrossMarginPercent;
	}

	public Map<Integer, Double> getPersonnelExpenseForEachYear(Optional<ProjectDetails> projectDetails,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> personnelExpenseForEachYear = new LinkedHashMap<>();

		try {

			// Double personnelExpense = (double)
			// projectAnalysisService.getProjectPersonnalExpanses(null, projectDetails);
			Double personnelExpense = projectDetails.get().getPersonalExpenses().getInputValues();

			// Personnel expense for year 1 = Personnel expense calculated above *(
			// 1+Effective GST input credit disallowance)
			// Any year value= Previous year value*(1+growth rate for that year)

			GstType personnel = fixedConstant.get(0).getPersonnel();

			Double personnelExpenseForFirstYear = MathUtil
					.roundTwoDecimals(personnelExpense * (1 + (personnel.getEffectiveGstCost() / 100)));
//			CostAssumptionsGrowthRate costAssumptionsGrowthRate = fixedConstant.get(0).getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumptionsGrowthRate = projectDetails.get().getCostAssumptionsGrowthRate();

			Map<Integer, Double> personnelExpensesGrowthRate = costAssumptionsGrowthRate.getPersonnelExpenses();

			personnelExpenseForEachYear.put(1, personnelExpenseForFirstYear);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = personnelExpensesGrowthRate.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}


			int growth = rate;
			personnelExpensesGrowthRate.forEach((key, value) -> {
				
				if(growth >= key) {
					Double personnelExpenseByYear = MathUtil
							.roundTwoDecimals((!ObjectUtils.isEmpty(personnelExpenseForEachYear.get(key - 1)) 
									? personnelExpenseForEachYear.get(key - 1) : 1) * (1 + (value / 100)));
					personnelExpenseForEachYear.put(key, personnelExpenseByYear);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating personnel Expense for each year");
		}

		return personnelExpenseForEachYear;

	}

	public Map<Integer, Double> getElectricityAndWaterExpenseForEachYear(Optional<ProjectDetails> projectDetails,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> electricityAndWaterExpenseForEachYear = new LinkedHashMap<>();

		try {

			// Double electricityAndWaterExpense = (double)
			// projectAnalysisService.getWaterAndElectricity(null,projectDetails);
			Double electricityAndWaterExpense = projectDetails.get().getElectricityWaterBill().getInputValues();
			// Electricity and water expense for year 1 = E&W expense calculated above *(
			// 1+Effective GST input credit disallowance)
			// Any year value= Previous year value*(1+growth rate for that year)

			GstType electricityAndWater = fixedConstant.get(0).getElectricityAndWater();

			Double electricityAndWaterExpenseForFirstYear = MathUtil.roundTwoDecimals(
					electricityAndWaterExpense * (1 + (electricityAndWater.getEffectiveGstCost() / 100)));
//			CostAssumptionsGrowthRate costAssumptionsGrowthRate = fixedConstant.get(0).getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumptionsGrowthRate = projectDetails.get().getCostAssumptionsGrowthRate();

			Map<Integer, Double> electricityAndWaterExpensesGrowthRate = costAssumptionsGrowthRate
					.getElectricityAndAirconditioning();

			electricityAndWaterExpenseForEachYear.put(1, electricityAndWaterExpenseForFirstYear);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = electricityAndWaterExpensesGrowthRate.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			electricityAndWaterExpensesGrowthRate.forEach((key, value) -> {
				
				if(growth >= key) {
					Double electricityAndWaterExpenseByYear = MathUtil
							.roundTwoDecimals((!ObjectUtils.isEmpty(electricityAndWaterExpenseForEachYear.get(key - 1)) 
									? electricityAndWaterExpenseForEachYear.get(key - 1) : 1) * (1 + (value / 100)));
					electricityAndWaterExpenseForEachYear.put(key, electricityAndWaterExpenseByYear);
				}

			});

		} catch (Exception e) {
			log.error("error while calculating electricity And Water Expense for each year");
		}

		return electricityAndWaterExpenseForEachYear;
	}

	public Map<Integer, Double> getRepairAndMaintenanceExpenseExpenseForEachYear(
			Optional<ProjectDetails> projectDetails, List<FixedConstant> fixedConstant) {

		Map<Integer, Double> RepairAndMaintenanceExpenseForEachYear = new LinkedHashMap<>();

		try {

			// Double repairAndMaintenanceExpense = (double)
			// projectAnalysisService.getProjectRepairAndMaintainceExpense(null,
			// projectDetails);
			Double repairAndMaintenanceExpense = projectDetails.get().getTotalRMExpenses().getInputValues();

			// R&M expense for year 1 = R&M expense calculated above *( 1+Effective GST
			// input credit disallowance)
			// Any year value= Previous year value*(1+growth rate for that year)

			GstType repairAndMaintenance = fixedConstant.get(0).getRepairAndMaintenance();

			Double repairAndMaintenanceExpenseForFirstYear = MathUtil.roundTwoDecimals(
					repairAndMaintenanceExpense * (1 + (repairAndMaintenance.getEffectiveGstCost() / 100)));
//			CostAssumptionsGrowthRate costAssumptionsGrowthRate = fixedConstant.get(0).getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumptionsGrowthRate = projectDetails.get().getCostAssumptionsGrowthRate();

			Map<Integer, Double> repairAndMaintenanceExpensesGrowthRate = costAssumptionsGrowthRate
					.getRepairAndMaintenance();

			RepairAndMaintenanceExpenseForEachYear.put(1, repairAndMaintenanceExpenseForFirstYear);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = repairAndMaintenanceExpensesGrowthRate.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}


			int growth = rate;
			repairAndMaintenanceExpensesGrowthRate.forEach((key, value) -> {
				
				if(growth >= key) {
					Double repairAndMaintenanceExpensesGrowthRateExpenseByYear = MathUtil
							.roundTwoDecimals((!ObjectUtils.isEmpty(RepairAndMaintenanceExpenseForEachYear.get(key - 1)) 
									? RepairAndMaintenanceExpenseForEachYear.get(key - 1) : 1) * (1 + (value / 100)));
					RepairAndMaintenanceExpenseForEachYear.put(key, repairAndMaintenanceExpensesGrowthRateExpenseByYear);
				}

			});

		} catch (Exception e) {
			log.error("error while calculating repair And Maintenance Expense for each year");
		}

		return RepairAndMaintenanceExpenseForEachYear;

	}

	public Map<Integer, Double> getOtherOperatingExpensesForEachYear(Map<Integer, Double> totalOverheadExpenses,
			List<FixedConstant> fixedConstant,Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> otherOperatingExpensesForEachYear = new LinkedHashMap<>();

		try {
			// Overhead expenses for each year already calculated above.
			// Multiply with (1+Effective GST input credit disallowance) for each year to
			// get other operating expenses

			GstType other = fixedConstant.get(0).getOther();
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = totalOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}


			int growth = rate;
			totalOverheadExpenses.forEach((key, value) -> {
				
				if(growth >= key) {
					Double otherOperatingExpenses = MathUtil
							.roundTwoDecimals(value * (1 + (other.getEffectiveGstCost() / 100)));
					otherOperatingExpensesForEachYear.put(key, otherOperatingExpenses);
				}

			});

		} catch (Exception e) {
			log.error("error while calculating other Operating Expense for each year");
		}

		return otherOperatingExpensesForEachYear;
	}

	public Map<Integer, Double> getExpenditureExRentAndCamForEachYear(Map<Integer, Double> personnelExpenseForEachYear,
			Map<Integer, Double> electricityAndWaterExpenseForEachYear,
			Map<Integer, Double> repairAndMaintenanceExpenseExpenseForEachYear,
			Map<Integer, Double> otherOperatingExpensesForEachYear) {

		Map<Integer, Double> expenditureExRentAndCamForEachYear = new LinkedHashMap<>();

		try {
			personnelExpenseForEachYear.forEach((key, value) -> {

				Double expenditureExRentAndCam = MathUtil
						.roundTwoDecimals(value + electricityAndWaterExpenseForEachYear.get(key)
								+ repairAndMaintenanceExpenseExpenseForEachYear.get(key)
								+ otherOperatingExpensesForEachYear.get(key));
				expenditureExRentAndCamForEachYear.put(key, expenditureExRentAndCam);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating expenditure Ex-Rent And Cam for each year");
		}

		return expenditureExRentAndCamForEachYear;
	}

	public Map<Integer, Double> getEBITDARForEachYear(Map<Integer, Double> grossMarginForEachYear,
			Map<Integer, Double> expenditureExRentAndCamForEachYear) {

		Map<Integer, Double> getEBITDARForEachYear = new LinkedHashMap<>();

		try {
			grossMarginForEachYear.forEach((key, value) -> {
				// Gross margin – Expenditure (ex-rent and CAM)
				Double getEBITDAR = MathUtil.roundTwoDecimals(value - expenditureExRentAndCamForEachYear.get(key));
				getEBITDARForEachYear.put(key, getEBITDAR);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating EBITDAR for each year");
		}

		return getEBITDARForEachYear;
	}

	public Map<Integer, Double> getEBITDARMarginPercentForEachYear(Map<Integer, Double> ebitdarForEachYear,
			Map<Integer, Double> totalRevenueForEachYear) {

		Map<Integer, Double> getEBITDARMarginPercentForEachYear = new LinkedHashMap<>();

		try {
			ebitdarForEachYear.forEach((key, value) -> {

				Double getEBITDARMarginPercent = MathUtil
						.roundTwoDecimals((value / totalRevenueForEachYear.get(key)) * 100);
				getEBITDARMarginPercentForEachYear.put(key, getEBITDARMarginPercent);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating EBITDAR margin percent for each year");
		}

		return getEBITDARMarginPercentForEachYear;
	}

	public Map<Integer, Double> getEBITDAForEachYear(Map<Integer, Double> ebitdarForEachYear,
			Map<Integer, Double> rentEachYear, Map<Integer, Double> camEachYear) {

		Map<Integer, Double> getEBITDAForEachYear = new LinkedHashMap<>();
		try {
			ebitdarForEachYear.forEach((key, value) -> {

				// EBITDAR – Rent – CAM
				Double getEBITDA = MathUtil.roundTwoDecimals(value - rentEachYear.get(key) - camEachYear.get(key));
				getEBITDAForEachYear.put(key, getEBITDA);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating EBITDA for each year");
		}

		return getEBITDAForEachYear;
	}

	public Map<Integer, Double> getEBITDAMarginForEachYear(Map<Integer, Double> getEBITDAForEachYear,
			Map<Integer, Double> totalRevenueForEachYear) {

		Map<Integer, Double> getEBITDAMarginForEachYear = new LinkedHashMap<>();

		try {
			// EBITDA/Revenue
			getEBITDAForEachYear.forEach((key, value) -> {

				Double getEBITDAMargin = MathUtil.roundTwoDecimals((value / totalRevenueForEachYear.get(key)) * 100);
				getEBITDAMarginForEachYear.put(key, getEBITDAMargin);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating EBITDA margin for each year");
		}

		return getEBITDAMarginForEachYear;
	}

	public Map<Integer, Double> getEBITForEachYear(Map<Integer, Double> getEBITDAForEachYear,
			Map<Integer, Double> depreciationForEachYear) {

		Map<Integer, Double> getEBITForEachYear = new LinkedHashMap<>();

		try {
			// EBITDA – Depreciation
			getEBITDAForEachYear.forEach((key, value) -> {

				Double getEBIT = MathUtil.roundTwoDecimals(value - depreciationForEachYear.get(key));
				getEBITForEachYear.put(key, getEBIT);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating EBIT for each year");
		}

		return getEBITForEachYear;
	}

	public Map<Integer, Double> getCumulativeEBITDAForEachYear(Map<Integer, Double> getEBITDAForEachYear) {

		Map<Integer, Double> cumulativeEBITDAForEachYear = new LinkedHashMap<>();

		try {
			// 2. Cumulative EBITDA for year 1 = EBITDA for year 1
			// Cumulative EBITDA for any other year = EBITDA for that year + cumulative
			// EBITDA for previous year
			cumulativeEBITDAForEachYear.put(0, 0.0);
			getEBITDAForEachYear.forEach((key, value) -> {

				if (key == 1) {
					cumulativeEBITDAForEachYear.put(key, value);
				} else {

					Double cumulativeEBITDA = MathUtil
							.roundTwoDecimals(value + cumulativeEBITDAForEachYear.get(key - 1));
					cumulativeEBITDAForEachYear.put(key, cumulativeEBITDA);
				}

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating cumulative EBITDA for each year");
		}

		return cumulativeEBITDAForEachYear;
	}

	public Map<Integer, Double> getTaxForEachYear(Map<Integer, Double> getEBITForEachYear,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> getTaxForEachYear = new LinkedHashMap<>();

		try {
			Double incomeTaxRate = fixedConstant.get(0).getIncomeTax() / 100;

			// EBIT calculated above * Income tax rate (fixed value)
			getEBITForEachYear.forEach((key, value) -> {

				Double taxForEachYear = MathUtil.roundTwoDecimals(value * incomeTaxRate);
				getTaxForEachYear.put(key, taxForEachYear);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating tax for each year");
		}

		return getTaxForEachYear;
	}

	public Map<Integer, Double> getCashInflowAfterTaxForEachYear(Map<Integer, Double> getEBITDAForEachYear,
			Map<Integer, Double> taxForEachYear) {

		Map<Integer, Double> getCashInflowAfterTaxForEachYear = new LinkedHashMap<>();

		try {
			// Cash inflow after tax = EBITDA – Tax for each year
			getEBITDAForEachYear.forEach((key, value) -> {

				Double getCashInflowAfterTax = MathUtil.roundTwoDecimals(value - taxForEachYear.get(key));
				getCashInflowAfterTaxForEachYear.put(key, getCashInflowAfterTax);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  Cash inflow after tax for each year");
		}

		return getCashInflowAfterTaxForEachYear;
	}

	public Map<Integer, Double> getTotalOutflows(Map<Integer, Double> capexForEachYear,
			Map<Integer, Double> securityDepositForEachYear, Map<Integer, Double> disposalOfAssets, Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> getTotalOutflows = new LinkedHashMap<>();

		try {
			// Total outflows = Capex + security deposit + disposal of assets + advance rent
			Map<Integer, Double> yearWiseAdvanceRent = projectDetails.get().getLesserLesse().getYearWiseAdvanceRentAdjustment();
			Double totalAdvanceRent = !ObjectUtils.isEmpty(projectDetails.get().getLesserLesse().getAdvanceRent()) ?
					projectDetails.get().getLesserLesse().getAdvanceRent() : 0.0;		
			if(!ObjectUtils.isEmpty(yearWiseAdvanceRent)) {
				yearWiseAdvanceRent.put(0, totalAdvanceRent);
			}
			
			disposalOfAssets.forEach((key, value) -> {
				
				Double totalOutflows = 0.0;
				if(key == 0) {
					totalOutflows = MathUtil
							.roundTwoDecimals(capexForEachYear.get(key) + securityDepositForEachYear.get(key) + value
									+ ((!ObjectUtils.isEmpty(yearWiseAdvanceRent) && !ObjectUtils.isEmpty(yearWiseAdvanceRent.get(key))) 
											? yearWiseAdvanceRent.get(key) : 0.0));
				}else {
					totalOutflows = MathUtil
							.roundTwoDecimals(capexForEachYear.get(key) + securityDepositForEachYear.get(key) + value
									- ((!ObjectUtils.isEmpty(yearWiseAdvanceRent) && !ObjectUtils.isEmpty(yearWiseAdvanceRent.get(key))) 
											? yearWiseAdvanceRent.get(key) : 0.0));
				}
				getTotalOutflows.put(key, totalOutflows);

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating total Out flows for each year");
		}

		return getTotalOutflows;
	}

	public Map<Integer, Double> getFreeCashFlowPreTaxForEachYear(Map<Integer, Double> getEBITDAForEachYear,
			Map<Integer, Double> totalOutflows) {

		Map<Integer, Double> getFreeCashFlowPreTaxForEachYear = new LinkedHashMap<>();

		try {
			// Free cash flow pre tax = EBITDA – total outflows for each year starting year
			// 0
			totalOutflows.forEach((key, value) -> {

				if (key == 0) {
					getFreeCashFlowPreTaxForEachYear.put(key, (0 - value));

				} else {

					Double getFreeCashFlowPreTax = MathUtil.roundTwoDecimals(getEBITDAForEachYear.get(key) - value);
					getFreeCashFlowPreTaxForEachYear.put(key, getFreeCashFlowPreTax);
				}

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Free Cash Flow Pre Tax for each year");
		}

		return getFreeCashFlowPreTaxForEachYear;
	}

	public Map<Integer, Double> getFreeCashFlowPostTaxForEachYear(Map<Integer, Double> cashInflowAfterTaxForEachYear,
			Map<Integer, Double> totalOutflowsForEachYear) {

		Map<Integer, Double> getFreeCashFlowPostTaxForEachYear = new LinkedHashMap<>();

		try {
			totalOutflowsForEachYear.forEach((key, value) -> {
				// Free cash flow post tax = Cash inflow after tax – Total outflows for each
				// year starting year 0
				if (key == 0) {
					getFreeCashFlowPostTaxForEachYear.put(key, (0 - value));

				} else {

					Double getFreeCashFlowPostTax = MathUtil
							.roundTwoDecimals(cashInflowAfterTaxForEachYear.get(key) - value);
					getFreeCashFlowPostTaxForEachYear.put(key, getFreeCashFlowPostTax);
				}

			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Free Cash Flow Post Tax for each year");
		}

		return getFreeCashFlowPostTaxForEachYear;
	}

	public Map<Integer, Double> getCumulativeFCFPostTaxForEachYear(
			Map<Integer, Double> freeCashFlowPostTaxForEachYear) {

		Map<Integer, Double> getCumulativeFCFPostTaxForEachYear = new LinkedHashMap<>();

		try {
			// Cumulative FCF post tax for year 1 = FCF post tax for year 1
			// Cumulative FCF post tax for any other year = FCF post tax for that year +
			// cumulative FCF post tax for previous year

			freeCashFlowPostTaxForEachYear.forEach((key, value) -> {
				if (key == 0) {
					getCumulativeFCFPostTaxForEachYear.put(0, value);
				} else {

					Double cumulativeFCFPostTax = MathUtil
							.roundTwoDecimals(value + getCumulativeFCFPostTaxForEachYear.get(key - 1));
					getCumulativeFCFPostTaxForEachYear.put(key, cumulativeFCFPostTax);

				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating cumulative Free Cash Flow Post Tax for each year");
		}

		return getCumulativeFCFPostTaxForEachYear;
	}

	public Map<Integer, Double> getDrawdownForEachYear(Map<Integer, Double> totalOutflowsForEachYear,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> getDrawdownForEachYear = new LinkedHashMap<>();

		try {
			// Drawdown for year 0 = Total outflows in year 0 * debt proportion
			// Drawdown for year 1 = Total outflows in year 1 * debt proportion
			// Drawdown for other years 0

			Double debtProportion = fixedConstant.get(0).getDebtProportion() / 100;

			totalOutflowsForEachYear.forEach((key, value) -> {
				Double drawdown = null;
				if (key == 0 || key == 1) {
					drawdown = MathUtil.roundTwoDecimals(value * debtProportion);
					getDrawdownForEachYear.put(key, drawdown);
				} else {
					getDrawdownForEachYear.put(key, 0.0);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating drawdown for each year");
		}

		return getDrawdownForEachYear;
	}

	public Map<String, Map<Integer, Double>> getDebtSchedule(Map<Integer, Double> drawdownForEachYear,
			List<FixedConstant> fixedConstant, Double grandTotal) {

		Map<String, Map<Integer, Double>> debtSchedule = new LinkedHashMap<>();

		try {

			Map<Integer, Double> repaymentForEachYear = new LinkedHashMap<>();
			Map<Integer, Double> openingDebtForEachYear = new LinkedHashMap<>();
			Map<Integer, Double> closingDebtForEachYear = new LinkedHashMap<>();
			Map<Integer, Double> interestForEachYear = new LinkedHashMap<>();

			Double costOfDebt = fixedConstant.get(0).getCostOfDebt() / 100;
			Double debtProportion = fixedConstant.get(0).getDebtProportion() /100;
			Double debtTenure = fixedConstant.get(0).getDebtTenure();

			drawdownForEachYear.forEach((key, value) -> {

				Double closingDebt = 0.0;
				Double interest = 0.0;

				if (key == 0) {

					repaymentForEachYear.put(key, 0.0);
					openingDebtForEachYear.put(key, 0.0);
					closingDebt = MathUtil
							.roundTwoDecimals(openingDebtForEachYear.get(key) + repaymentForEachYear.get(key) + value);
					closingDebtForEachYear.put(key, closingDebt);

				} else {

					openingDebtForEachYear.put(key, closingDebtForEachYear.get(key - 1));
					Double repayment = 0.0;
					if (openingDebtForEachYear.get(key) > 1) {
						repayment = MathUtil.roundTwoDecimals(-(grandTotal * debtProportion / debtTenure));
						repaymentForEachYear.put(key, repayment);
					} else {
						repayment = 0.0;
						repaymentForEachYear.put(key, repayment);
					}
					closingDebt = MathUtil
							.roundTwoDecimals(openingDebtForEachYear.get(key) + repaymentForEachYear.get(key) + value);
					closingDebtForEachYear.put(key, closingDebt);

				}

				interest = MathUtil.roundTwoDecimals(
						((closingDebtForEachYear.get(key) + openingDebtForEachYear.get(key)) / 2) * costOfDebt);
				interestForEachYear.put(key, interest);
			});

			debtSchedule.put("repayment", repaymentForEachYear);
			debtSchedule.put("openingDebt", openingDebtForEachYear);
			debtSchedule.put("closingDebt", closingDebtForEachYear);
			debtSchedule.put("interest", interestForEachYear);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating debt Schedule");
		}
		return debtSchedule;
	}

	public Map<Integer, Double> getInterest(Map<Integer, Double> interestForEachYear) {

		Map<Integer, Double> getInterest = new LinkedHashMap<>();
		try {
			interestForEachYear.forEach((key, value) -> {

				if (key != 0) {

					if (key == 1) {
						Double interest = MathUtil
								.roundTwoDecimals(interestForEachYear.get(0) + interestForEachYear.get(1));
						getInterest.put(key, interest);
					} else {
						getInterest.put(key, value);
					}

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating interest each year");
		}
		return getInterest;

	}

	public Map<Integer, Double> getPBTForEachYear(Map<Integer, Double> getEBITForEachYear,
			Map<Integer, Double> interestForEachYear) {

		Map<Integer, Double> getPBTForEachYear = new LinkedHashMap<>();
		try {

			interestForEachYear.forEach((key, value) -> {
				Double pbt = MathUtil.roundTwoDecimals(getEBITForEachYear.get(key) - interestForEachYear.get(key));
				getPBTForEachYear.put(key, pbt);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating PBT each year");
		}
		return getPBTForEachYear;
	}

	public Map<Integer, Double> getTaxUnderPATForEachYear(Map<Integer, Double> getPBTForEachYear,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> getTaxUnderPATForEachYear = new LinkedHashMap<>();
		try {

			Double incomeTaxRate = fixedConstant.get(0).getIncomeTax() / 100;

			getPBTForEachYear.forEach((key, value) -> {

				if (value > 0) {
					Double tax = MathUtil.roundTwoDecimals(value * incomeTaxRate);
					getTaxUnderPATForEachYear.put(key, tax);
				} else {
					getTaxUnderPATForEachYear.put(key, 0.0);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating tax in PAT each year");
		}
		return getTaxUnderPATForEachYear;

	}

	public Map<Integer, Double> getPATForEachYear(Map<Integer, Double> getPBTForEachYear,
			Map<Integer, Double> getTaxUnderPATForEachYear) {

		Map<Integer, Double> getPATForEachYear = new LinkedHashMap<>();
		try {
			getPATForEachYear.put(0, 0.0);
			getPBTForEachYear.forEach((key, value) -> {

				Double pat = MathUtil.roundTwoDecimals(value - getTaxUnderPATForEachYear.get(key));
				getPATForEachYear.put(key, pat);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  PAT each year");
		}
		return getPATForEachYear;
	}

	public Map<Integer, Double> getLessCapexForEachYear(Map<Integer, Double> totalOutflowsForEachYear,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> getLessCapexForEachYear = new LinkedHashMap<>();

		try {
			Double equityProportion = fixedConstant.get(0).getEquityProportion() / 100;

			totalOutflowsForEachYear.forEach((key, value) -> {
				Double capex = null;
				if (key == 0 || key == 1) {
					capex =  - value * equityProportion;
					getLessCapexForEachYear.put(key, capex);
				} else {
					capex = - value;
				}
				getLessCapexForEachYear.put(key, capex);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  less capex each year");
		}
		return getLessCapexForEachYear;
	}

	public Map<Integer, Double> getEquityCashFlowsForEachYear(Map<Integer, Double> getPATForEachYear,
			Map<Integer, Double> depreciationForEachYear, Map<Integer, Double> repaymentForEachYear,
			Map<Integer, Double> lessCapexForEachYear) {

		Map<Integer, Double> getEquityCashFlowsForEachYear = new LinkedHashMap<>();
		try {

			getPATForEachYear.forEach((key, value) -> {

				Double equityCashFlows = value + depreciationForEachYear.get(key) + repaymentForEachYear.get(key)
						+ lessCapexForEachYear.get(key);
				getEquityCashFlowsForEachYear.put(key, equityCashFlows);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  equity Cash Flows each year");
		}
		return getEquityCashFlowsForEachYear;
	}

	public Double getprojectIRRPostTax(Map<Integer, Double> freeCashFlowPostTaxForEachYear) {

		Double irrValue = 0.0;

		try {
			double[] cf = new double[freeCashFlowPostTaxForEachYear.size()];

			for (int i = 0; i < freeCashFlowPostTaxForEachYear.size(); i++) {
				cf[i] = freeCashFlowPostTaxForEachYear.get(i);
			}

			//irrValue = Irr.irr(cf);
			irrValue = getIRR(cf);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  project IRR Post Tax");
		}
		
		if(irrValue.isInfinite() || irrValue.isNaN()){
			irrValue = 0.0;
		}

		return irrValue;
	}

	public Double getprojectIRRPreTax(Map<Integer, Double> freeCashFlowPreTaxForEachYear) {

		Double irrValue = 0.0;
		try {
			double[] cf = new double[freeCashFlowPreTaxForEachYear.size()];

			for (int i = 0; i < freeCashFlowPreTaxForEachYear.size(); i++) {
				cf[i] =  Math.round(freeCashFlowPreTaxForEachYear.get(i));
			}
		
			//irrValue = Irr.irr(cf);
			irrValue = getIRR(cf);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  project IRR Pre Tax");
		}
		
		if(irrValue.isInfinite() || irrValue.isNaN()){
			irrValue = 0.0;
		}
		
		
		return irrValue;
	}
	
	public static double getIRR(final double[] cashFlows) {
		final int MAX_ITER = 20;
		double EXCEL_EPSILON = 0.0000001;

		double x = 0.1;
		int iter = 0;
		while (iter++ < MAX_ITER) {

			final double x1 = 1.0 + x;
			double fx = 0.0;
			double dfx = 0.0;
			for (int i = 0; i < cashFlows.length; i++) {
				final double v = cashFlows[i];
				final double x1_i = Math.pow(x1, i);
				fx += v / x1_i;
				final double x1_i1 = x1_i * x1;
				dfx += -i * v / x1_i1;
			}
			final double new_x = x - fx / dfx;
			final double epsilon = Math.abs(new_x - x);

			if (epsilon <= EXCEL_EPSILON) {
				if (x == 0.0 && Math.abs(new_x) <= EXCEL_EPSILON) {
					return 0.0; // OpenOffice calc does this
				} else {
					return new_x * 100;
				}
			}
			x = new_x;
		}
		return x;
	}


	public Double getEquityIRR(Map<Integer, Double> equityCashFlowsForEachYear) {

		Double irrValue = 0.0;
		try {
			double[] cf = new double[equityCashFlowsForEachYear.size()];

			for (int i = 0; i < equityCashFlowsForEachYear.size(); i++) {
				cf[i] = equityCashFlowsForEachYear.get(i);
			}

			//irrValue = Irr.irr(cf);
			irrValue = getIRR(cf);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Equity IRR");
		}
		
		if(irrValue.isInfinite() || irrValue.isNaN()){
			irrValue = 0.0;
		}
		
		return irrValue;
	}

	public Map<Integer, Double> getAdRevenueForEachYear(Optional<ProjectDetails> projectDetails,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> getAdRevenueForEachYear = new LinkedHashMap<>();

		try {

			Double adRevenueForYearTwo = projectDetails.get().getAdRevenue().getInputValues();

			Double yearOneAdIncomeLowerByPercent = !ObjectUtils.isEmpty(projectDetails.get().getYearOneAdIncomeLowerByPercent()) ? 
					projectDetails.get().getYearOneAdIncomeLowerByPercent() : fixedConstant.get(0).getYearOneAdIncomeLowerByPercent();
			Double adRevenueForYearOne = MathUtil
					.roundTwoDecimals(adRevenueForYearTwo * (1 - (yearOneAdIncomeLowerByPercent / 100)));

//			RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate = fixedConstant.get(0)
//					.getRevenueAssumptionsGrowthRate();
			RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate = projectDetails.get()
					.getRevenueAssumptionsGrowthRate();
			
			Map<Integer, Double> sponsorshipIncomeGrowth = revenueAssumptionsGrowthRate.getSponsorshipIncomeGrowth();

			getAdRevenueForEachYear.put(1, adRevenueForYearOne);
			getAdRevenueForEachYear.put(2, adRevenueForYearTwo);
			
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = sponsorshipIncomeGrowth.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;

			sponsorshipIncomeGrowth.forEach((key, value) -> {
				
				if(growth >= key) {
					if (key != 2) {
						// Any year value= Previous year value*(1+growth rate for that year)
						Double adRevenueForEachYear = MathUtil.roundTwoDecimals((!ObjectUtils.isEmpty(getAdRevenueForEachYear.get(key - 1)) 
										? getAdRevenueForEachYear.get(key - 1) : 1 )* (1 + (value / 100)));
						getAdRevenueForEachYear.put(key, adRevenueForEachYear);

					}
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating get Ad Revenue For Each Year");
		}
		return getAdRevenueForEachYear;

	}

	public Map<Integer, Double> getMiscellaneousIncomeForEachYear(Double miscellaneousIncome,
			List<FixedConstant> fixedConstant,Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> getMiscellaneousIncomeForEachYear = new LinkedHashMap<>();
		try {

//			RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate = fixedConstant.get(0)
//					.getRevenueAssumptionsGrowthRate();
			RevenueAssumptionsGrowthRate revenueAssumptionsGrowthRate = projectDetails.get()
					.getRevenueAssumptionsGrowthRate();
			
			Map<Integer, Double> otherIncomeGrowth = revenueAssumptionsGrowthRate.getOtherIncomeGrowth();
			getMiscellaneousIncomeForEachYear.put(1, miscellaneousIncome);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int size = otherIncomeGrowth.size() + 1;
			int rate = 0;
			
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}


			int growth = rate;
			otherIncomeGrowth.forEach((key, value) -> {
				
				if(growth >= key) {
					// Any year value= Previous year value*(1+growth rate for that year)
					Double miscellaneousIncomeForEachYear = MathUtil
							.roundTwoDecimals((!ObjectUtils.isEmpty(getMiscellaneousIncomeForEachYear.get(key - 1)) 
									? getMiscellaneousIncomeForEachYear.get(key - 1) : 1)* (1 + (value / 100)));
					getMiscellaneousIncomeForEachYear.put(key, miscellaneousIncomeForEachYear);
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating miscellaneous Income For Each Year");
		}
		return getMiscellaneousIncomeForEachYear;
	}

	public Double getEBITDAPayback(Map<Integer, Double> cumulativeEBITDAForEachYear, Double grandTotal, Map<Integer, Double> getEBITDAForEachYear) {

		Double getEBITDAPayback = 0.0;
		Map<Integer, Double> ebitdaLocalValues = new HashMap<>();
		try {
			
			if(!ObjectUtils.isEmpty(cumulativeEBITDAForEachYear)) {
				
				if(grandTotal > cumulativeEBITDAForEachYear.get(1)) {
					getEBITDAPayback = 1.0;
					
				}else {
					getEBITDAPayback = MathUtil
							.roundTwoDecimals(cumulativeEBITDAForEachYear.get(1) > 0.0 
									? (grandTotal/cumulativeEBITDAForEachYear.get(1)) : 0.0);	
				}
				ebitdaLocalValues.put(1, getEBITDAPayback);
				
				//Checking from year 2 onwards:::
				for (int i = 2; i < cumulativeEBITDAForEachYear.size(); i++) {
					Double calculateEbitTda = 0.0;

					if(grandTotal > cumulativeEBITDAForEachYear.get(i)) {
						calculateEbitTda = 1.0;
						
					}else {		
						if(ebitdaLocalValues.get(i-1) < 1) {
							calculateEbitTda = 0.0;
							
						}else {
							calculateEbitTda = MathUtil.roundTwoDecimals(
									    (getEBITDAForEachYear.get(i) > 0 ?
										 ((grandTotal- cumulativeEBITDAForEachYear.get(i-1))/ getEBITDAForEachYear.get(i))
										: 0.0));							 		    
						}			
					}
					ebitdaLocalValues.put(i, calculateEbitTda);		    
					getEBITDAPayback += calculateEbitTda;
				}
				
			}

//			for (int i = 0; i < cumulativeEBITDAForEachYear.size(); i++) {
//
//				if (cumulativeEBITDAForEachYear.get(i) > grandTotal) {
//
//					// EBITDA payback = (x-1) + (Grand Total – Cumulative EBITDA in year
//					// (x-1))/(cumulative EBITDA in year (x) – cumulative EBITDA in year (x-1))
//					getEBITDAPayback = MathUtil
//							.roundTwoDecimals((i - 1) + (grandTotal - cumulativeEBITDAForEachYear.get(i - 1))
//									/ (cumulativeEBITDAForEachYear.get(i) - cumulativeEBITDAForEachYear.get(i - 1)));
//					break;
//				}
//
//			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating EBITDA Payback");
		}
		return getEBITDAPayback;

	}

	public Double getFCFPayback(Map<Integer, Double> cumulativeFCFPostTaxForEachYear, Double grandTotal) {

		Double getFCFPayback = 0.0;
		try {

			for (int i = 0; i < cumulativeFCFPostTaxForEachYear.size(); i++) {

				if (cumulativeFCFPostTaxForEachYear.get(i) > grandTotal) {

					// EBITDA payback = (x-1) + (Grand Total – Cumulative EBITDA in year
					// (x-1))/(cumulative EBITDA in year (x) – cumulative EBITDA in year (x-1))
					getFCFPayback = MathUtil.roundTwoDecimals((i - 1) + (0
							- cumulativeFCFPostTaxForEachYear.get(i - 1))
							/ (cumulativeFCFPostTaxForEachYear.get(i) - cumulativeFCFPostTaxForEachYear.get(i - 1)));
					break;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating FCF Payback");
		}
		return getFCFPayback;
	}

	public Map<Integer, Double> getDisposalOfAssets(Double salvageFromDisposalOfAssets,List<FixedConstant> fixedConstants,Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> getDisposalOfAssets = new LinkedHashMap<>();
		try {

//			Map<Integer,Double>  growthRate  =  fixedConstants.get(0).getRevenueAssumptionsGrowthRate().getAdmitsGrowth();
			Map<Integer,Double>  growthRate  =  projectDetails.get().getRevenueAssumptionsGrowthRate().getAdmitsGrowth();

			getDisposalOfAssets.put(0, salvageFromDisposalOfAssets);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int size= growthRate.size() + 1;
			int rate = 0;
			
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			for (int i = 1; i <= rate; i++) {
				getDisposalOfAssets.put(i, 0.0);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Disposal Of Assets");
		}
		return getDisposalOfAssets;

	}

	public Map<Integer, Double> getCapexForEachYear(Map<Integer, Double> additions) {

		Map<Integer, Double> getCapexForEachYear = new LinkedHashMap<>();
		try {

			additions.forEach((key, value) -> {
				getCapexForEachYear.put(key, value);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Capex For Each Year");
		}
		return getCapexForEachYear;

	}

	public Map<Integer, Double> gethousekeepingChargesForEachYear(Optional<ProjectDetails> projectDetails,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> gethousekeepingChargesForEachYear = new LinkedHashMap<>();
		try {

			Double houseKeepingExpense = projectDetails.get().getHouseKeepingExpense().getInputValues();

//			CostAssumptionsGrowthRate costAssumptionsGrowthRate = fixedConstant.get(0).getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumptionsGrowthRate = projectDetails.get().getCostAssumptionsGrowthRate();

			Map<Integer, Double> otherOverheadExpenses = costAssumptionsGrowthRate.getOtherOverheadExpenses();
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			gethousekeepingChargesForEachYear.put(1, houseKeepingExpense);
			int rate = 0;
			int size = otherOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			otherOverheadExpenses.forEach((key, value) -> {
				
				if(growth >= key) {
					// Any year value= Previous year value*(1+growth rate for that year)
					Double housekeepingCharges = MathUtil
							.roundTwoDecimals((!ObjectUtils.isEmpty(gethousekeepingChargesForEachYear.get(key - 1)) 
									? gethousekeepingChargesForEachYear.get(key - 1) : 1) * (1 + (value / 100)));
					gethousekeepingChargesForEachYear.put(key, housekeepingCharges);
				}
			
			});

		} catch (Exception e) {
			log.error("error while calculating housekeeping Charges For Each Year");
		}
		return gethousekeepingChargesForEachYear;

	}

	public Map<Integer, Double> getTotalOverheadExpensesForEachYear(Map<Integer, Double> housekeepingChargesForEachYear,
			Map<Integer, Double> municipalTaxForEachYear, Map<Integer, Double> securityChargesForEachYear,
			Map<Integer, Double> marketingExpenseForEachYear, Map<Integer, Double> communicationChargesForEachYear,
			Map<Integer, Double> internetChargesForEachYear, Map<Integer, Double> legalChargesForEachYear,
			Map<Integer, Double> travellingChargesForEachYear, Map<Integer, Double> insuranceChargesForEachYear,
			Map<Integer, Double> miscellaneousCharges, Map<Integer, Double> printingAndStationaryChargesForEachYear,
			Map<Integer, Double> rateAndTaxes) {

		Map<Integer, Double> getTotalOverheadExpensesForEachYear = new LinkedHashMap<>();
		try {

			housekeepingChargesForEachYear.forEach((key, value) -> {

				Double totalOverheadExpenses = MathUtil.roundTwoDecimals(value + municipalTaxForEachYear.get(key)
						+ securityChargesForEachYear.get(key) + marketingExpenseForEachYear.get(key)
						+ communicationChargesForEachYear.get(key) + internetChargesForEachYear.get(key)
						+ legalChargesForEachYear.get(key) + travellingChargesForEachYear.get(key)
						+ insuranceChargesForEachYear.get(key) + miscellaneousCharges.get(key)
						+ printingAndStationaryChargesForEachYear.get(key) + rateAndTaxes.get(key));
				getTotalOverheadExpensesForEachYear.put(key, totalOverheadExpenses);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating total Overhead Expenses For Each Year");
		}
		return getTotalOverheadExpensesForEachYear;

	}

	public Double getNpv(Map<Integer, Double> freeCashFlowPostTaxForEachYear, Double wacc) {

		Double getNpv = 0.0;
		try {

			for (int i = 0; i < freeCashFlowPostTaxForEachYear.size(); i++) {

				if (i == 0) {
					getNpv = MathUtil.roundTwoDecimals(getNpv + freeCashFlowPostTaxForEachYear.get(i));
				} else {
					getNpv = MathUtil.roundTwoDecimals(getNpv + ((freeCashFlowPostTaxForEachYear.get(i)) / (Math.pow((1 + (wacc/100)), i))));

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  Npv");
		}
		return getNpv;
	}

	Map<String, Map<Integer, Double>> getDepricationWorkingsData(List<FixedConstant> fixedConstant,
			Optional<ProjectDetails> projectDetails, Map<Integer, Double> totalRevenueForEachYear,
			Double totalProjectCostAfterContingencies, Double depRate) {

		Map<String, Map<Integer, Double>> getDepricationWorkingsData = new HashMap<>();

		try {

			Map<Integer, Double> openingGrossBlockForEachYear = new LinkedHashMap<>();
			Map<Integer, Double> closingGrossBlockForEachYear = new LinkedHashMap<>();
			Map<Integer, Double> additionsForEachYear = new LinkedHashMap<>();
			Map<Integer, Double> depreciationForEachYear = new LinkedHashMap<>();
			Map<Integer, Double> netGrossForEachYear = new LinkedHashMap<>();

			Double initailCapexInZeroYear = fixedConstant.get(0).getInitailCapexInZeroYear() / 100;
			Double initailCapexInFirstYear = fixedConstant.get(0).getInitailCapexInOneYear() / 100;
			Double initailCapexInSevenYear = fixedConstant.get(0).getInitailCapexInSevenYear() / 100;
			Double overDepreciationPercent = depRate / 100; // hard code
			Integer leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
//			Map<Integer,Double>  growthRate  =  fixedConstant.get(0).getRevenueAssumptionsGrowthRate().getAdmitsGrowth();
			Map<Integer,Double>  growthRate  =  projectDetails.get().getRevenueAssumptionsGrowthRate().getAdmitsGrowth();

			Double maintenanceCapexOfRevenueForAllYearTillLastRenovation = fixedConstant.get(0)
					.getMaintenanceCapexOfRevenueForAllYearTillLastRenovation() / 100;
			Double maintenanceCapexOfRevenueForYearAfterLastRenovation = fixedConstant.get(0)
					.getMaintenanceCapexOfRevenueForYearAfterLastRenovation() / 100;

			openingGrossBlockForEachYear.put(0, 0.0);
			depreciationForEachYear.put(0, 0.0);

			// Additions in year 0: Project cost after contingencies (from project cost
			// excel uploaded)*% initial capex in year 0
			Double additionsForYearZero = MathUtil
					.roundTwoDecimals(totalProjectCostAfterContingencies * initailCapexInZeroYear);
			// Additions in year 1: Project cost after contingencies (from project cost
			// excel uploaded)*% initial capex in year 1
			Double additionsForYearOne = MathUtil
					.roundTwoDecimals(totalProjectCostAfterContingencies * initailCapexInFirstYear);

			additionsForEachYear.put(0, additionsForYearZero);
			additionsForEachYear.put(1, additionsForYearOne);

			Double closingGrossBlock = openingGrossBlockForEachYear.get(0) + additionsForEachYear.get(0);
			closingGrossBlockForEachYear.put(0, closingGrossBlock);

			// Net gross block in any year = closing gross block in that year – depreciation
			// in that year
			Double netGrossForZeroYear = closingGrossBlockForEachYear.get(0) - depreciationForEachYear.get(0);
			netGrossForEachYear.put(0, netGrossForZeroYear);

			int x = leasePeriod / 7;

			Double renovationCapex = MathUtil
					.roundTwoDecimals(totalProjectCostAfterContingencies * initailCapexInSevenYear);

			int noOfyear = x - 1;
			int count = 0;
			int flag = 0;
			int size = growthRate.size() + 1;
			int rate = 0;
			
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			for (int i = 1; i <= rate; i++) {

				int renovationCapexYear = 7 * (x - 1);
				Double addition = null;
				if (i != 1) {

					if (noOfyear == count) {
						if (flag < 3) {
							flag++;
							addition = MathUtil.roundTwoDecimals(maintenanceCapexOfRevenueForAllYearTillLastRenovation
									* totalRevenueForEachYear.get(i));
							additionsForEachYear.put(i, addition);
						} else {
							addition = MathUtil.roundTwoDecimals(maintenanceCapexOfRevenueForYearAfterLastRenovation
									* totalRevenueForEachYear.get(i));
							additionsForEachYear.put(i, addition);
						}

					} else {
						if (renovationCapexYear >= i  && i % 7 == 0) {
							count++;
							// addition =
							// MathUtil.roundTwoDecimals(maintenanceCapexOfRevenueForYearAfterLastRenovation
							// * initailCapexInSevenYear);
							additionsForEachYear.put(i, renovationCapex);
						} else {
							// Maintenance capex as % of revenue * revenue for that year
							addition = MathUtil.roundTwoDecimals(maintenanceCapexOfRevenueForAllYearTillLastRenovation
									* totalRevenueForEachYear.get(i));
							additionsForEachYear.put(i, addition);
						}
					}

				}

				// Opening gross block in any year = net gross block in previous year
				openingGrossBlockForEachYear.put(i, netGrossForEachYear.get(i - 1));

				// Closing gross bock in any year = opening gross block in that year + addition
				// in that year
				Double closingGrossBlockforYear = MathUtil
						.roundTwoDecimals(openingGrossBlockForEachYear.get(i) + additionsForEachYear.get(i));
				closingGrossBlockForEachYear.put(i, closingGrossBlockforYear);

				// Depreciation in any year = Closing gross block * over depreciation % from
				Double depreciationAnyYear = MathUtil
						.roundTwoDecimals(closingGrossBlockForEachYear.get(i) * overDepreciationPercent);
				depreciationForEachYear.put(i, depreciationAnyYear);

				// Net gross block in any year = closing gross block in that year – depreciation
				// in that year
				Double netGrossForAnyYear = MathUtil
						.roundTwoDecimals(closingGrossBlockForEachYear.get(i) - depreciationForEachYear.get(i));
				netGrossForEachYear.put(i, netGrossForAnyYear);

			}

			getDepricationWorkingsData.put("openingGrossBlock", openingGrossBlockForEachYear);
			getDepricationWorkingsData.put("closingGrossBlock", closingGrossBlockForEachYear);
			getDepricationWorkingsData.put("additions", additionsForEachYear);
			getDepricationWorkingsData.put("depreciation", depreciationForEachYear);
			getDepricationWorkingsData.put("netGrossForEachYear", netGrossForEachYear);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  Deprication Workings Data");
		}
		return getDepricationWorkingsData;

	}

	// Only for fixed rent
	public Double rentDiscount(Double rent, Double discountRate, Optional<ProjectDetails> projectDetails) {

		Double rentDiscount = 0.0;
		try {
			// Rent = Fixed rent/(1+discount rate)^(Opening date – today) 2019-01-07
			String openingDateValue = projectDetails.get().getDateOfOpening();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
			LocalDate today = LocalDate.now();

			LocalDate openingDate = LocalDate.parse(openingDateValue, formatter);
			Double months = (double) today.until(openingDate, ChronoUnit.MONTHS);
			Double val = months / 12;

			rentDiscount = MathUtil.roundTwoDecimals(rent / (Math.pow((1 + discountRate), val)));

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  Deprication rent Discount");
		}
		return rentDiscount;

	}

	// Only for cam rent
	public Double camDiscount(Double cam, Double discountRate, Optional<ProjectDetails> projectDetails) {

		Double camDiscount = 0.0;
		try {
			// Rent = Fixed rent/(1+discount rate)^(Opening date – today) 2019-01-07
			String openingDateValue = projectDetails.get().getDateOfOpening();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
			LocalDate today = LocalDate.now();
			formatter.format(today);

			LocalDate openingDate = LocalDate.parse(openingDateValue, formatter);
			Double months = (double) today.until(openingDate, ChronoUnit.MONTHS);
			Double val = months / 12;

			camDiscount = MathUtil.roundTwoDecimals(cam / Math.pow((1 + discountRate), val));

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  Deprication cam Discount");
		}
		return camDiscount;

	}

	public Map<String, Map<Integer, Double>> rentYearDetail(Optional<ProjectDetails> projectDetails,
			List<FixedConstant> fixedConstant, Map<Integer, Double> netTicketSales,
			Map<Integer, Double> netFnBSalesForEachYear, Map<Integer, Double> adRevenueForEachYear,
			Map<Integer, Double> miscellaneousIncomeForEachYear, Map<Integer, Double> vpfIncome,Map<Integer, Double> conveniencefeeForEachYear) {

		Map<String, Map<Integer, Double>> rentYearDetail = new HashMap<>();
		try {

			Map<Integer, Double> rentYearWise = projectDetails.get().getRentTerm().getYearWiseRent();
			Double rentAndCamDiscountRate = fixedConstant.get(0).getRentAndCamDiscountRate() / 100;
//			Map<Integer,Double>  growthRate  =  fixedConstant.get(0).getRevenueAssumptionsGrowthRate().getAdmitsGrowth();
			Map<Integer,Double>  growthRate  =  projectDetails.get().getRevenueAssumptionsGrowthRate().getAdmitsGrowth();
			
			Integer leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();

			Map<Integer, Double> rent = new LinkedHashMap<>();
			Map<Integer, Double> mgForEachYear = new LinkedHashMap<>();
			Map<Integer, Double> mgForEachYearWithoutDiscount = new LinkedHashMap<>();
			Map<Integer, Double> otherPercent = new LinkedHashMap<>();
			Map<Integer, Double> revenueShare = new LinkedHashMap<>();
			
		/*	Double boxOfficePercentage = 0.0;
			Double fAndBPercentage = 0.0;
			Double adSalePercentage = 0.0;
			Double othersPercentage = 0.0;
			
			if(!ObjectUtils.isEmpty(projectDetails.get().rentTerm.getBoxOfficePercentage())) {
				 boxOfficePercentage = projectDetails.get().rentTerm.getBoxOfficePercentage() / 100;
			}
			
			if(!ObjectUtils.isEmpty(projectDetails.get().rentTerm.getFAndBPercentage())) {
				fAndBPercentage = projectDetails.get().rentTerm.getFAndBPercentage() / 100;
			}
			
			if(!ObjectUtils.isEmpty(projectDetails.get().rentTerm.getAdSalePercentage() )) {
				 adSalePercentage = projectDetails.get().rentTerm.getAdSalePercentage() / 100;
			}
			
			if(!ObjectUtils.isEmpty(projectDetails.get().rentTerm.getOthersPercentage() )) {
				 othersPercentage = projectDetails.get().rentTerm.getOthersPercentage() / 100;
			}*/
			 
			
			
			Double fixedRent = 0.0;
			Double avgRent =0.0;
			boolean avgRentFlag = true;
		
			int count = 1;
			
			
			
			int size = growthRate.size() + 1;
			int rate = 0;
			
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			
			for (int i = 1; i <= rate; i++) {
				
//				double advanceRentAmount = 0;
//				if(!ObjectUtils.isEmpty(projectDetails.get().getLesserLesse().getYearWiseAdvanceRentAdjustment()) && 
//						projectDetails.get().getLesserLesse().getYearWiseAdvanceRentAdjustment().size() > 0) {
//					advanceRentAmount = !ObjectUtils.isEmpty(projectDetails.get().getLesserLesse().getYearWiseAdvanceRentAdjustment().get(i)) ? 
//							projectDetails.get().getLesserLesse().getYearWiseAdvanceRentAdjustment().get(i) : 0;
//							
//				}

				if (projectDetails.get().getLesserLesse().getRentPaymentTerm().equals(Constants.FIXED)) {
					Double rentDiscount = MathUtil.roundTwoDecimals(
							rentDiscount(rentYearWise.get(i), rentAndCamDiscountRate, projectDetails));
					rent.put(i, rentDiscount);
					mgForEachYear.put(i, rentDiscount);
					mgForEachYearWithoutDiscount.put(i, MathUtil.roundTwoDecimals(rentYearWise.get(i)));

				}

				else if (projectDetails.get().getLesserLesse().getRentPaymentTerm().equals(Constants.REVENUE_SHARE)) {
					
					Map<Integer,Double> boxOfficePercentage = projectDetails.get().rentTerm.getYearWiseBoxOfficePercentage();
					Map<Integer,Double> fAndBPercentage = projectDetails.get().rentTerm.getYearWiseFAndBPercentage();
					Map<Integer,Double> adSalePercentage = projectDetails.get().rentTerm.getYearWiseAdSalePercentage();
					Map<Integer,Double> othersPercentage = projectDetails.get().rentTerm.getYearWiseOthersPercentage();
					
					
					Double boxOffice =  netTicketSales.get(i) * (boxOfficePercentage.get(i)/100);
					Double fnb = netFnBSalesForEachYear.get(i) *  (fAndBPercentage.get(i)/100);
					Double ad = adRevenueForEachYear.get(i) * (adSalePercentage.get(i)/100);
					
					Double otherVal = conveniencefeeForEachYear.get(i) + miscellaneousIncomeForEachYear.get(i) + vpfIncome.get(i);
					Double other = otherVal * (othersPercentage.get(i)/100);

					Double revenue = MathUtil.roundTwoDecimals(boxOffice + fnb + ad + other);
			
					revenueShare.put(i, revenue);
					rent.put(i, revenue);
					mgForEachYear.put(i, revenue);
					mgForEachYearWithoutDiscount.put(i, revenue);

				}

				else if (projectDetails.get().getLesserLesse().getRentPaymentTerm().equals(Constants.MAX)) {

					
					Double rentDiscount = MathUtil.roundTwoDecimals(rentDiscount(rentYearWise.get(i), rentAndCamDiscountRate, projectDetails));
					
					Map<Integer,Double> boxOfficePercentage = projectDetails.get().rentTerm.getYearWiseBoxOfficePercentage();
					Map<Integer,Double> fAndBPercentage = projectDetails.get().rentTerm.getYearWiseFAndBPercentage();
					Map<Integer,Double> adSalePercentage = projectDetails.get().rentTerm.getYearWiseAdSalePercentage();
					Map<Integer,Double> othersPercentage = projectDetails.get().rentTerm.getYearWiseOthersPercentage();
					
					
					Double boxOffice =  netTicketSales.get(i) * (boxOfficePercentage.get(i)/100);
					Double fnb = netFnBSalesForEachYear.get(i) *  (fAndBPercentage.get(i)/100);
					Double ad = adRevenueForEachYear.get(i) * (adSalePercentage.get(i)/100);
					
					Double otherVal = conveniencefeeForEachYear.get(i) + miscellaneousIncomeForEachYear.get(i) + vpfIncome.get(i);
					Double other = otherVal * (othersPercentage.get(i)/100);
					Double revenue = MathUtil.roundTwoDecimals(boxOffice + fnb + ad + other);
					
					revenueShare.put(i, revenue);
					if (rentDiscount  >= revenue) {
						rent.put(i, rentDiscount);
						mgForEachYear.put(i, rentDiscount);
						mgForEachYearWithoutDiscount.put(i, rentDiscount);
					} else {
						rent.put(i, revenue);
						mgForEachYear.put(i, revenue);
						mgForEachYearWithoutDiscount.put(i, revenue);
					}
					

				/*	Double rentDiscount = MathUtil.roundTwoDecimals(
							rentDiscount(rentYearWise.get(i), rentAndCamDiscountRate, projectDetails));
					rentFixed.put(i, rentDiscount);

					Double other = adRevenueForEachYear.get(i) + miscellaneousIncomeForEachYear.get(i)
							+ vpfIncome.get(i);
					otherPercent.put(i, other);

					Double revenue = MathUtil.roundTwoDecimals(netTicketSales.get(i) + netFnBSalesForEachYear.get(i)
							+ adRevenueForEachYear.get(i) + otherPercent.get(i));
					revenueShare.put(i, revenue);
					rentRevenue.put(i, revenue);

					if (rentFixed.get(i) >= rentRevenue.get(i)) {
						rent.put(i, rentFixed.get(i));
						mgForEachYear.put(i, rentFixed.get(i));
					} else {
						rent.put(i, rentRevenue.get(i));
						mgForEachYear.put(i, rentRevenue.get(i));
					}*/

				} else if (projectDetails.get().getLesserLesse().getRentPaymentTerm().equals(Constants.OTHER_1)) {
					
					Map<Integer, Double> rentFixed = new LinkedHashMap<>();
					Map<Integer, Double> rentRevenue = new LinkedHashMap<>();

					int startingYear = projectDetails.get().getLesserLesse().getStartingYearForMG();
					String paymentTerm = projectDetails.get().getRentTerm().getPaymentTerm();
					
					if(Constants.FIXED.equals(paymentTerm)) {
						Map<Integer,Double> boxOfficePercentage = projectDetails.get().rentTerm.getYearWiseBoxOfficePercentage();
						Map<Integer,Double> fAndBPercentage = projectDetails.get().rentTerm.getYearWiseFAndBPercentage();
						Map<Integer,Double> adSalePercentage = projectDetails.get().rentTerm.getYearWiseAdSalePercentage();
						Map<Integer,Double> othersPercentage = projectDetails.get().rentTerm.getYearWiseOthersPercentage();
						
						
						Double boxOffice =  netTicketSales.get(i) * (boxOfficePercentage.get(i)/100);
						Double fnb = netFnBSalesForEachYear.get(i) *  (fAndBPercentage.get(i)/100);
						Double ad = adRevenueForEachYear.get(i) * (adSalePercentage.get(i)/100);
						
						Double otherVal = conveniencefeeForEachYear.get(i) + miscellaneousIncomeForEachYear.get(i) + vpfIncome.get(i);
						Double other = otherVal * (othersPercentage.get(i)/100);

						Double revenue = MathUtil.roundTwoDecimals(boxOffice + fnb + ad + other);
						
						
						if (i < startingYear) {
							rent.put(i, revenue);
							mgForEachYear.put(i, revenue);
							mgForEachYearWithoutDiscount.put(i, revenue);

						} else {
							

							Double rentDiscount = MathUtil.roundTwoDecimals(rentDiscount(rentYearWise.get(i), rentAndCamDiscountRate, projectDetails));
							rentFixed.put(i, rentDiscount);
							
							rent.put(i, rentFixed.get(i));
							mgForEachYear.put(i, rentFixed.get(i));
							mgForEachYearWithoutDiscount.put(i, rentFixed.get(i));
						}
						
					}
					
					if(Constants.AVERAGE.equals(paymentTerm)) {
						
						
						Map<Integer,Double> boxOfficePercentage = projectDetails.get().rentTerm.getYearWiseBoxOfficePercentage();
						Map<Integer,Double> fAndBPercentage = projectDetails.get().rentTerm.getYearWiseFAndBPercentage();
						Map<Integer,Double> adSalePercentage = projectDetails.get().rentTerm.getYearWiseAdSalePercentage();
						Map<Integer,Double> othersPercentage = projectDetails.get().rentTerm.getYearWiseOthersPercentage();
						
						
						Double boxOffice =  netTicketSales.get(i) * (boxOfficePercentage.get(i)/100);
						Double fnb = netFnBSalesForEachYear.get(i) *  (fAndBPercentage.get(i)/100);
						Double ad = adRevenueForEachYear.get(i) * (adSalePercentage.get(i)/100);
						
						Double otherVal = conveniencefeeForEachYear.get(i) + miscellaneousIncomeForEachYear.get(i) + vpfIncome.get(i);
						Double other = otherVal * (othersPercentage.get(i)/100);

						Double revenue = MathUtil.roundTwoDecimals(boxOffice + fnb + ad + other);
						
						
						if (i < startingYear) {
							rent.put(i, revenue);
							mgForEachYear.put(i, revenue);
							mgForEachYearWithoutDiscount.put(i, revenue);
							fixedRent +=  revenue;
							

						} else {
							
							Double rentFrequencyOfEscaltionYears_fixed = projectDetails.get().rentTerm.getRentFrequencyOfEscaltionYears_fixed();
							Double percentageEscalationInRent_fixed = projectDetails.get().rentTerm.getPercentageEscalationInRent_fixed();
							
							if(avgRentFlag) {
								avgRent = fixedRent / (startingYear - 1);
								avgRentFlag = false;
							}
							
							
							
							rent.put(i, avgRent);
							mgForEachYear.put(i, avgRent);
							mgForEachYearWithoutDiscount.put(i, avgRent);
							
							
							
							if(count % rentFrequencyOfEscaltionYears_fixed == 0)
								avgRent+=(avgRent * percentageEscalationInRent_fixed/100);
							count ++;
							
						}
						
					}
					

				} else if (projectDetails.get().getLesserLesse().getRentPaymentTerm().equals(Constants.OTHER_2)) {

					
					Map<Integer,Double> boxOfficePercentage = projectDetails.get().rentTerm.getYearWiseBoxOfficePercentage();
					Map<Integer,Double> fAndBPercentage = projectDetails.get().rentTerm.getYearWiseFAndBPercentage();
					Map<Integer,Double> adSalePercentage = projectDetails.get().rentTerm.getYearWiseAdSalePercentage();
					Map<Integer,Double> othersPercentage = projectDetails.get().rentTerm.getYearWiseOthersPercentage();
					
					Double rentDiscount = MathUtil.roundTwoDecimals(rentDiscount(rentYearWise.get(i), rentAndCamDiscountRate, projectDetails));
					
					
					Double boxOffice =  netTicketSales.get(i) * (boxOfficePercentage.get(i)/100);
					Double fnb = netFnBSalesForEachYear.get(i) *  (fAndBPercentage.get(i)/100);
					Double ad = adRevenueForEachYear.get(i) * (adSalePercentage.get(i)/100);
					
					Double otherVal = conveniencefeeForEachYear.get(i) + miscellaneousIncomeForEachYear.get(i) + vpfIncome.get(i);
					Double other = otherVal * (othersPercentage.get(i)/100);
					
					
					
					Double totalRent = rentDiscount + boxOffice + fnb + ad + other;
					
					rent.put(i, totalRent);
					mgForEachYear.put(i, totalRent);
					mgForEachYearWithoutDiscount.put(i, totalRent);
					
					
					
					
				/*	
					Map<Integer, Double> rentFixed = new LinkedHashMap<>();
					Map<Integer, Double> rentRevenue = new LinkedHashMap<>();

				
					rentFixed.put(i, rentDiscount);

					Double other = adRevenueForEachYear.get(i) + miscellaneousIncomeForEachYear.get(i)
							+ vpfIncome.get(i);
					otherPercent.put(i, other);

					Double revenue = MathUtil.roundTwoDecimals(netTicketSales.get(i) + netFnBSalesForEachYear.get(i)
							+ adRevenueForEachYear.get(i) + otherPercent.get(i));
					revenueShare.put(i, revenue);
					rentRevenue.put(i, revenue);

					if (rentFixed.get(i) == 0) {
						rent.put(i, rentRevenue.get(i));
						mgForEachYear.put(i, rentRevenue.get(i));
						mgForEachYearWithoutDiscount.put(i, rentRevenue.get(i));

					} else {
						rent.put(i, rentFixed.get(i));
						mgForEachYear.put(i, rentFixed.get(i));
						mgForEachYearWithoutDiscount.put(i,rentRevenue.get(i));
					}*/

				}

			}

			rentYearDetail.put("rent", rent);
			rentYearDetail.put("mg", mgForEachYear);
			rentYearDetail.put("mgWithoutDiscount", mgForEachYearWithoutDiscount);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  rent For Each Year");
		}
		return rentYearDetail;

	}

	public Map<Integer, Double> camForEachYear(Optional<ProjectDetails> projectDetails,
			List<FixedConstant> fixedConstant) {

		Map<Integer, Double> cam = new LinkedHashMap<>();
		try {

			Map<Integer, Double> camYearWise = projectDetails.get().getCamTerm().getYearWiseCamRent();
			Double rentAndCamDiscountRate = fixedConstant.get(0).getRentAndCamDiscountRate() / 100;
//			Map<Integer,Double>  growthRate  =  fixedConstant.get(0).getRevenueAssumptionsGrowthRate().getAdmitsGrowth();
			Map<Integer,Double>  growthRate  =  projectDetails.get().getRevenueAssumptionsGrowthRate().getAdmitsGrowth();

			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int size = growthRate.size() + 1;
			int rate = 0;
			
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			
			for (int i = 1; i <= rate; i++) {
				

				if (projectDetails.get().getCamTerm().getCamPaymentTerm().equals(Constants.FIXED)) {
					Double camDiscount = MathUtil
							.roundTwoDecimals(camDiscount(camYearWise.get(i), rentAndCamDiscountRate, projectDetails));
					cam.put(i, camDiscount);
				} else if (projectDetails.get().getCamTerm().getCamPaymentTerm().equals(Constants.ACTUALS)) {

					Double camForYearOne = (double) projectAnalysisService.getProjectCamDetails(null, projectDetails);

				/*	Integer camfrequencyOfEscaltionYears = projectDetails.get().getCamTerm()
							.getFrequencyOfEscaltionYears();
					Double camPercentageEscalation = projectDetails.get().getCamTerm().getPercentageEscalation();
					Double cam_rent = camForYearOne;
					if (i % camfrequencyOfEscaltionYears == 0) {
						cam_rent += (camYearWise.get(i) * camPercentageEscalation / 100);
						cam.put(i, cam_rent);
					} else {
						cam.put(i, cam_rent);
					}*/
					cam.put(i, camForYearOne);

				} else if (projectDetails.get().getCamTerm().getCamPaymentTerm().equals(Constants.INCLUDED)) {

					if (camYearWise.get(i) > 0) {
						Double camDiscount = MathUtil.roundTwoDecimals(
								camDiscount(camYearWise.get(i), rentAndCamDiscountRate, projectDetails));
						cam.put(i, camDiscount);

					} else {
						cam.put(i, 0.0);
					}

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  cam For Each Year");
		}

		return cam;

	}

	public Map<Integer, Double> getRent(Map<Integer, Double> rentForEachYear, List<FixedConstant> fixedConstant) {

		Map<Integer, Double> getRent = new LinkedHashMap<>();
		try {

			// Rent for each year = Rent computed above * (1+Effective GST input credit
			// allowance for rent)
			Double gstCreditDisallowed = fixedConstant.get(0).getRent().getEffectiveGstCost() / 100;

			rentForEachYear.forEach((key, value) -> {
				Double rent = MathUtil.roundTwoDecimals(rentForEachYear.get(key) * (1 + gstCreditDisallowed));
				getRent.put(key, rent);
			});

		} catch (Exception e) {
			log.error("error while calculating  rent");
		}
		return getRent;
	}

	public Map<Integer, Double> getCam(Map<Integer, Double> camForEachYear, List<FixedConstant> fixedConstant) {

		Map<Integer, Double> getCam = new LinkedHashMap<>();
		try {

			// Rent for each year = Rent computed above * (1+Effective GST input credit
			// allowance for rent)
			Double gstCreditDisallowed = fixedConstant.get(0).getCam().getEffectiveGstCost() / 100;

			camForEachYear.forEach((key, value) -> {
				Double rent = MathUtil.roundTwoDecimals(camForEachYear.get(key) * (1 + gstCreditDisallowed));
				getCam.put(key, rent);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  cam");
		}
		return getCam;
	}

	public Map<Integer, Double> getMiscellaneousCharges(List<FixedConstant> fixedConstant,
			Optional<ProjectDetails> projectDetails) {

		Map<Integer, Double> getMiscellaneousCharges = new LinkedHashMap<>();
		try {

//			CostAssumptionsGrowthRate costAssumptionsGrowthRate = fixedConstant.get(0).getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumption = projectDetails.get().getCostAssumptionsGrowthRate();
			
			Map<Integer, Double> otherOverheadExpenses = costAssumption.getOtherOverheadExpenses();
			Double miscellaneousChargesForYearOne = projectDetails.get().getMiscellaneousExpenses().getInputValues();
			getMiscellaneousCharges.put(1, miscellaneousChargesForYearOne);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = otherOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			otherOverheadExpenses.forEach((key, value) -> {

				if(growth >= key) {
					// Any year value= Previous year value*(1+growth rate for that year)
					Double miscellaneousCharges = MathUtil
							.roundTwoDecimals((!ObjectUtils.isEmpty(getMiscellaneousCharges.get(key - 1)) 
									? getMiscellaneousCharges.get(key - 1) :1) * (1 + value / 100));
					getMiscellaneousCharges.put(key, miscellaneousCharges);
				}
				
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating  Miscellaneous Charges");
		}
		return getMiscellaneousCharges;

	}

	public Double getROCE(Map<Integer, Double> getEBITDAForEachYear, Double grandTotal) {
		Double getROCE = 0.0;
		try {
			getROCE = MathUtil.roundTwoDecimals((getEBITDAForEachYear.get(2) / grandTotal) *  100);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating ROCE");
		}
		return getROCE;

	}

	public Map<Integer, Double> getSecurityDepositForEachYear(Optional<ProjectDetails> projectDetails,
			Map<Integer, Double> mgForEachYearWithoutDiscount, List<FixedConstant> fixedConstant,Map<Integer,Double> camForEachYear) {

		Map<Integer, Double> getSecurityDepositForEachYear = new LinkedHashMap<>();

		try {

			// security deposit for one year = cam + rent + utility
			String rentType = projectDetails.get().getLesserLesse().getRentType();
			String camType = projectDetails.get().getLesserLesse().getCamType();
			
			String rentPaymentTerm = projectDetails.get().getLesserLesse().getRentPaymentTerm();

			Integer leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
//			Map<Integer, Double> growthRate = fixedConstant.get(0).getRevenueAssumptionsGrowthRate().getAdmitsGrowth();
			Map<Integer, Double> growthRate = projectDetails.get().getRevenueAssumptionsGrowthRate().getAdmitsGrowth();

			Double utilitiesDeposit = projectDetails.get().getLesserLesse().getUtilitySecurityDeposit();

			Integer camNoOfMonth = projectDetails.get().getLesserLesse().getSecurityDepositForMonthCam();
			Integer rentNoOfMonth = projectDetails.get().getLesserLesse().getSecurityDepositForMonthRent();

			Map<Integer, Double> camYearWise = camForEachYear;
			Map<Integer, Double> rentYearWise = mgForEachYearWithoutDiscount;

			Integer additionDepositOnEscaltion = projectDetails.get().getLesserLesse().getAdditionDepositOnEscaltion();

			Double cam = 0.0;
			Double rent = 0.0;
			Double securityDepositForZeroYear = 0.0;
			Map<Integer, Double> yearWiseRentInFixed = null;

			if ("fixed".equals(camType)) {
				cam = projectDetails.get().getLesserLesse().getCamSecurityDepositForFixedZeroMg();

			}
			if ("fixed".equals(rentType)) {
				rent = projectDetails.get().getLesserLesse().getRentSecurityDepositForFixedZeroMg();
			}

			if ("month".equals(camType)) {
				cam = MathUtil.roundTwoDecimals(camYearWise.get(1) * camNoOfMonth / 12);
			}

			if ("month".equals(rentType)) {
				
				if(rentPaymentTerm.equals("mg_revenue_share")) { 
//					yearWiseRentInFixed = projectDetails.get().getRentTerm().getYearWiseRent();
					yearWiseRentInFixed = projectDetails.get().getRentTerm().getYearWiseRentAfterAdjustment();
					rent = MathUtil.roundTwoDecimals(yearWiseRentInFixed.get(1) * rentNoOfMonth / 12);
				}else {
					rent = MathUtil.roundTwoDecimals(rentYearWise.get(1) * rentNoOfMonth / 12);
				}
				
			}

			securityDepositForZeroYear = MathUtil.roundTwoDecimals(cam + rent + utilitiesDeposit);
			getSecurityDepositForEachYear.put(0, securityDepositForZeroYear);

			Double securityDepositTotal = securityDepositForZeroYear;

			int rate = 0;
			int size = growthRate.size() + 1;
			if (leasePeriod >= size) {
				rate = size;
			} else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			for (int i = 1; i <= rate; i++) {

				double camVal = 0.0;
				double rentVal = 0.0;

				if (i < leasePeriod) {

					if (additionDepositOnEscaltion == 1) {
						if ("fixed".equals(camType)) {

							camVal = 0.0;
						}
						if ("fixed".equals(rentType)) {

							rentVal = 0.0;
						}

						if ("month".equals(camType)) {
							if (i == 1) {
								camVal = MathUtil.roundTwoDecimals(
										(camYearWise.get(1) - camYearWise.get(i)) * camNoOfMonth / 12);
							} else {
								camVal = MathUtil.roundTwoDecimals(
										(camYearWise.get(i) - camYearWise.get(i - 1)) * camNoOfMonth / 12);
							}

						}

						if ("month".equals(rentType)) {
							if (i == 1) {
								if(rentPaymentTerm.equals("mg_revenue_share")) {
									rentVal = MathUtil.roundTwoDecimals(
											(yearWiseRentInFixed.get(1) - yearWiseRentInFixed.get(i)) * rentNoOfMonth / 12);
								}else {
									rentVal = MathUtil.roundTwoDecimals(
											(rentYearWise.get(1) - rentYearWise.get(i)) * rentNoOfMonth / 12);
								}
								
							} else {
								if(rentPaymentTerm.equals("mg_revenue_share")) {
									rentVal = MathUtil.roundTwoDecimals(
											(yearWiseRentInFixed.get(i) - yearWiseRentInFixed.get(i - 1)) * rentNoOfMonth / 12);
								}else {
									rentVal = MathUtil.roundTwoDecimals(
											(rentYearWise.get(i) - rentYearWise.get(i - 1)) * rentNoOfMonth / 12);
								}
								
							}

						}
					}

					Double securityDeposit = MathUtil.roundTwoDecimals(camVal + rentVal);
					getSecurityDepositForEachYear.put(i, securityDeposit);
					securityDepositTotal += securityDeposit;

				} else {

					Double lastYear = -securityDepositTotal;
					getSecurityDepositForEachYear.put(i, lastYear);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while calculating Security Deposit For Each Year");
		}
		return getSecurityDepositForEachYear;

	}

	public Map<Integer, Double> getSecurityCharges(List<FixedConstant> fixedConstants,
			Optional<ProjectDetails> projectDetails) {
		Map<Integer, Double> securityCharges = new LinkedHashMap<>();
		try {
//			CostAssumptionsGrowthRate costAssumption = fixedConstants.get(Constants.INDEX_0)
//					.getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumption = projectDetails.get()
					.getCostAssumptionsGrowthRate();
			
			Double securityChargesForOneYear = projectDetails.get().getSecurityExpense().getInputValues();
			Map<Integer, Double> otherOverheadExpenses = costAssumption.getOtherOverheadExpenses();
			securityCharges.put(Constants.ONE, securityChargesForOneYear);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = otherOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			
			otherOverheadExpenses.forEach((year, growthRate) -> {
				
				if(growth >= year) {
					Double securityChargesForEachYear = MathUtil.roundTwoDecimals(
							(!ObjectUtils.isEmpty(securityCharges.get(year - Constants.ONE)) 
									? securityCharges.get(year - Constants.ONE) : 1) * (Constants.ONE + (growthRate / 100)));
					securityCharges.put(year, securityChargesForEachYear);
				}
				
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching security charges fee for each years");
		}
		return securityCharges;
	}

	public Map<Integer, Double> getMarketingExpense(List<FixedConstant> fixedConstants,
			Optional<ProjectDetails> projectDetails) {
		Map<Integer, Double> marketingExpense = new LinkedHashMap<>();
		try {
//			CostAssumptionsGrowthRate costAssumption = fixedConstants.get(Constants.INDEX_0)
//					.getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumption = projectDetails.get()
					.getCostAssumptionsGrowthRate();
			
			Double marketingExpenseForOneYear = projectDetails.get().getMarketingExpense().getInputValues();
			Double marketingExpenseLaunch = fixedConstants.get(0).getMarketingExpenseLaunch()
					* projectDetails.get().getNoOfScreens();
			Map<Integer, Double> otherOverheadExpenses = costAssumption.getOtherOverheadExpenses();
			Double totalMarketingExpense = MathUtil
					.roundTwoDecimals(marketingExpenseForOneYear + marketingExpenseLaunch);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			marketingExpense.put(Constants.ONE, totalMarketingExpense);
			int rate = 0;
			int size = otherOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			otherOverheadExpenses.forEach((year, growthRate) -> {
				
				if(growth >= year) {
					Double marketingExpenseForEachYear = null;
					if (year == 2) {
						marketingExpenseForEachYear = MathUtil
								.roundTwoDecimals(((!ObjectUtils.isEmpty(marketingExpense.get(year - Constants.ONE)) 
										? marketingExpense.get(year - Constants.ONE) : 1 ) - marketingExpenseLaunch)
										* (Constants.ONE + (growthRate / 100)));
					} else {

						marketingExpenseForEachYear = MathUtil.roundTwoDecimals(
								(!ObjectUtils.isEmpty(marketingExpense.get(year - Constants.ONE)) 
										? marketingExpense.get(year - Constants.ONE) : 1) * (Constants.ONE + (growthRate / 100)));
					}

					marketingExpense.put(year, marketingExpenseForEachYear);
				}
				
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching marketing expense fee for each years");
		}
		return marketingExpense;
	}

	public Map<Integer, Double> getCommuncationChargesExpense(List<FixedConstant> fixedConstants,
			Optional<ProjectDetails> projectDetails) {
		Map<Integer, Double> communicationCharge = new LinkedHashMap<>();
		try {
//			CostAssumptionsGrowthRate costAssumption = fixedConstants.get(Constants.INDEX_0)
//					.getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumption = projectDetails.get()
					.getCostAssumptionsGrowthRate();
			
			Double communcationChargesExpenseForOneYear = projectDetails.get().getCommunicationExpense()
					.getInputValues();

			Map<Integer, Double> otherOverheadExpenses = costAssumption.getOtherOverheadExpenses();
			communicationCharge.put(Constants.ONE, communcationChargesExpenseForOneYear);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = otherOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}


			int growth = rate;
			otherOverheadExpenses.forEach((year, growthRate) -> {
				
				if(growth >= year) {
					Double communicationChargeForEachYear = MathUtil.roundTwoDecimals(
							(!ObjectUtils.isEmpty(communicationCharge.get(year - Constants.ONE)) 
									? communicationCharge.get(year - Constants.ONE) : 1) * (Constants.ONE + (growthRate / 100)));

					communicationCharge.put(year, communicationChargeForEachYear);
				}
			
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching communication expense fee for each years");
		}
		return communicationCharge;
	}

	public Map<Integer, Double> getInternetChargesExpense(List<FixedConstant> fixedConstants,
			Optional<ProjectDetails> projectDetails) {
		Map<Integer, Double> internetCharges = new LinkedHashMap<>();
		try {
//			CostAssumptionsGrowthRate costAssumption = fixedConstants.get(Constants.INDEX_0)
//					.getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumption = projectDetails.get()
					.getCostAssumptionsGrowthRate();
			
			Double internetChargesExpenseForOneYear = projectDetails.get().getInternetExpense().getInputValues();

			Map<Integer, Double> otherOverheadExpenses = costAssumption.getOtherOverheadExpenses();
			internetCharges.put(Constants.ONE, internetChargesExpenseForOneYear);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = otherOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}


			int growth = rate;
			otherOverheadExpenses.forEach((year, growthRate) -> {
				
				if(growth >= year) {
					Double internetChargeForEachYear = MathUtil
							.roundTwoDecimals(MathUtil.roundTwoDecimals((!ObjectUtils.isEmpty(internetCharges.get(year - Constants.ONE)) ? internetCharges.get(year - Constants.ONE) : 1)
									* (Constants.ONE + (growthRate / 100))));
					internetCharges.put(year, internetChargeForEachYear);
				}
				
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching internet charges fee for each years");
		}
		return internetCharges;
	}

	public Map<Integer, Double> getLegalChargesExpense(List<FixedConstant> fixedConstants,
			Optional<ProjectDetails> projectDetails) {
		Map<Integer, Double> legalCharges = new LinkedHashMap<>();
		try {
//			CostAssumptionsGrowthRate costAssumption = fixedConstants.get(Constants.INDEX_0)
//					.getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumption = projectDetails.get()
					.getCostAssumptionsGrowthRate();
			
			Double legalChargesExpenseForOneYear = projectDetails.get().getLegalFee().getInputValues();

			Map<Integer, Double> otherOverheadExpenses = costAssumption.getOtherOverheadExpenses();
			legalCharges.put(Constants.ONE, legalChargesExpenseForOneYear);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = otherOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			otherOverheadExpenses.forEach((year, growthRate) -> {
				
				if(growth >= year) {
					Double legalChargeForEachYear = MathUtil.roundTwoDecimals(
							(!ObjectUtils.isEmpty(legalCharges.get(year - Constants.ONE)) 
									? legalCharges.get(year - Constants.ONE) : 1) * (Constants.ONE + (growthRate / 100)));
					legalCharges.put(year, legalChargeForEachYear);	
				}
				
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching legal charges fee for each years");
		}
		return legalCharges;
	}

	public Map<Integer, Double> getTravellingChargeChargesExpense(List<FixedConstant> fixedConstants,
			Optional<ProjectDetails> projectDetails) {
		Map<Integer, Double> travellingCharges = new LinkedHashMap<>();
		try {
//			CostAssumptionsGrowthRate costAssumption = fixedConstants.get(Constants.INDEX_0)
//					.getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumption = projectDetails.get()
					.getCostAssumptionsGrowthRate();

			Double travellingChargesExpenseForOneYear = projectDetails.get().getTravellingAndConveyanceExpense()
					.getInputValues();

			Map<Integer, Double> otherOverheadExpenses = costAssumption.getOtherOverheadExpenses();
			travellingCharges.put(Constants.ONE, travellingChargesExpenseForOneYear);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = otherOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}


			int growth = rate;
			
			otherOverheadExpenses.forEach((year, growthRate) -> {
				if(growth >= year) {
					Double travellingChargeForEachYear = MathUtil.roundTwoDecimals(
							(!ObjectUtils.isEmpty(travellingCharges.get(year - Constants.ONE)) 
									? travellingCharges.get(year - Constants.ONE) : 1) * (Constants.ONE + (growthRate / 100)));
					travellingCharges.put(year, travellingChargeForEachYear);
				}
				
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching travelling charges fee for each years");
		}
		return travellingCharges;
	}

	public Map<Integer, Double> getInsuranceChargeChargesExpense(List<FixedConstant> fixedConstants,
			Optional<ProjectDetails> projectDetails) {
		Map<Integer, Double> insuranceCharges = new LinkedHashMap<>();
		try {
//			CostAssumptionsGrowthRate costAssumption = fixedConstants.get(Constants.INDEX_0)
//					.getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumption = projectDetails.get()
					.getCostAssumptionsGrowthRate();
			
			Double insuranceChargesExpenseForOneYear = projectDetails.get().getInsuranceExpense().getInputValues();

			Map<Integer, Double> otherOverheadExpenses = costAssumption.getOtherOverheadExpenses();
			insuranceCharges.put(Constants.ONE, insuranceChargesExpenseForOneYear);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = otherOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			otherOverheadExpenses.forEach((year, growthRate) -> {
				
				if(growth >= year) {
					Double insuranceChargeForEachYear = MathUtil.roundTwoDecimals(
							(!ObjectUtils.isEmpty(insuranceCharges.get(year - Constants.ONE)) 
									? insuranceCharges.get(year - Constants.ONE) : 1) * (Constants.ONE + (growthRate / 100)));
					insuranceCharges.put(year, insuranceChargeForEachYear);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching insurance charges fee for each years");
		}
		return insuranceCharges;
	}

	public Map<Integer, Double> getMiscellaneousChargesExpense(List<FixedConstant> fixedConstants,
			Optional<ProjectDetails> projectDetails) {
		Map<Integer, Double> miscellaneousCharges = new LinkedHashMap<>();
		try {
			CostAssumptionsGrowthRate costAssumption = fixedConstants.get(Constants.INDEX_0)
					.getCostAssumptionsGrowthRate();
			Double miscellaneousExpenseForOneYear = projectDetails.get().getMiscellaneousExpenses().getInputValues();
			Double factor = fixedConstants.get(0).getFactor();
			Map<Integer, Double> otherOverheadExpenses = costAssumption.getOtherOverheadExpenses();
			miscellaneousCharges.put(Constants.ONE, miscellaneousExpenseForOneYear);
			otherOverheadExpenses.forEach((year, growthRate) -> {
				Double miscellaneousChargeForEachYear = MathUtil.roundTwoDecimals(
						miscellaneousCharges.get(year - Constants.ONE) * (Constants.ONE + growthRate));
				miscellaneousCharges.put(year, miscellaneousChargeForEachYear);
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching micellaneous charges fee for each years");
		}
		return miscellaneousCharges;
	}

	public Map<Integer, Double> getPrintingAndStationaryCharge(List<FixedConstant> fixedConstants,
			Optional<ProjectDetails> projectDetails) {
		Map<Integer, Double> printingAndStationaryCharges = new LinkedHashMap<>();
		try {
//			CostAssumptionsGrowthRate costAssumption = fixedConstants.get(Constants.INDEX_0)
//					.getCostAssumptionsGrowthRate();
			CostAssumptionsGrowthRate costAssumption = projectDetails.get()
					.getCostAssumptionsGrowthRate();
			
			Double printingAndStationaryChargeExpenseForOneYear = projectDetails.get().getPrintingAndStationaryExpense()
					.getInputValues();
			Map<Integer, Double> otherOverheadExpenses = costAssumption.getOtherOverheadExpenses();
			printingAndStationaryCharges.put(Constants.ONE, printingAndStationaryChargeExpenseForOneYear);
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
			int size = otherOverheadExpenses.size() + 1;
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}

			int growth = rate;
			otherOverheadExpenses.forEach((year, growthRate) -> {
				
				if(growth >= year) {
					Double printingAndStationaryChargeForEachYear = MathUtil.roundTwoDecimals(
							(!ObjectUtils.isEmpty(printingAndStationaryCharges.get(year - Constants.ONE)) 
									? printingAndStationaryCharges.get(year - Constants.ONE) : 1) * (Constants.ONE + (growthRate / 100)));
					printingAndStationaryCharges.put(year, printingAndStationaryChargeForEachYear);
				}
			
			});
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching printing and stationary charges fee for each years");
		}
		return printingAndStationaryCharges;
	}


	public Map<Integer, Double> getNetSaleOfTicket(Map<Integer, Double> netTotalSale,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> netSaleOfticket = new LinkedHashMap<>();
		try {
			netTotalSale.forEach((key, value) -> {
				double netSale = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				netSaleOfticket.put(key, netSale);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching net sale  for each years");
		}
		
		return netSaleOfticket;
	}

	public Map<Integer, Double> getSaleOfFAndbForEachYear(Map<Integer, Double> saleOfFAndB,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> saleFnB = new LinkedHashMap<>();
		try {
			saleOfFAndB.forEach((key, value) -> {
				double fandb = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				saleFnB.put(key, fandb);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching sale of f and b  for each years");
		}
		return saleFnB;
	}

	public Map<Integer, Double> getSponsorshipRevenue(Map<Integer, Double> sponsorshipDetails,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> sponsorship = new LinkedHashMap<>();
		try {
			sponsorshipDetails.forEach((key, value) -> {
				double sponsorships = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				sponsorship.put(key, sponsorships);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching  sponsorship revenue  for each years");

		}
		return sponsorship;
	}

	public Map<Integer, Double> getVasIncomeRowFoutySix(Map<Integer, Double> vasIncome,
			Map<Integer, Double> totalRevnue) {
		Map<Integer, Double> vasIncomeForEachYear = new LinkedHashMap<>();
		try {
			vasIncome.forEach((key, value) -> {
				double vasIncomeDetails = MathUtil.roundTwoDecimals((value / totalRevnue.get(key)) * 100);
				vasIncomeForEachYear.put(key, vasIncomeDetails);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching  sponsorship revenue  for each years");

		}
		return vasIncomeForEachYear;
	}

	public Map<Integer, Double> getOtherOperatingRevenue(Map<Integer, Double> otherRevenueDetails,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> otherOperatingReveue = new LinkedHashMap<>();
		try {
			otherRevenueDetails.forEach((key, value) -> {
				double otherRevenue = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				otherOperatingReveue.put(key, otherRevenue);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching  other operating revenue  for each years");
		}
		return otherOperatingReveue;
	}

	public Map<Integer, Double> getTotalRevenue(Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> totalRevenueYearWise = new LinkedHashMap<>();
		try {

			totalRevenue.forEach((key, value) -> {
				double total = MathUtil.roundTwoDecimals((value / value) * 100);
				totalRevenueYearWise.put(key, total);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching total revenue  for each years");

		}
		return totalRevenueYearWise;
	}

	public Map<Integer, Double> getFilmDistributorShareDetails(Map<Integer, Double> filmDistributorShare,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> filmDistributors = new LinkedHashMap<>();
		try {
			filmDistributorShare.forEach((key, value) -> {
				double filmDistributor = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				filmDistributors.put(key, filmDistributor);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching film distributors share for each years");
		}
		return filmDistributors;
	}

	public Map<Integer, Double> getConsumptionOfFAndBDetails(Map<Integer, Double> consumptionsOfFandBDetails,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> consumptionsOfFandB = new LinkedHashMap<>();
		try {
			consumptionsOfFandBDetails.forEach((key, value) -> {
				double consumptionsFandB = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				consumptionsOfFandB.put(key, consumptionsFandB);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching consumption of f and b for each years");
		}
		return consumptionsOfFandB;
	}

	public Map<Integer, Double> getpersonnelExpenses(Map<Integer, Double> personnelExpenseForEachYear,
			Map<Integer, Double> totalRevenueForEachYear) {

		Map<Integer, Double> personnelExpenses = new LinkedHashMap<>();
		try {

			personnelExpenseForEachYear.forEach((key, value) -> {

				Double personnelExpensesTotal = MathUtil
						.roundTwoDecimals((value / totalRevenueForEachYear.get(key)) * 100);
				personnelExpenses.put(key, personnelExpensesTotal);

			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching personnel Expenses Total");

		}
		return personnelExpenses;
	}

	public Map<Integer, Double> getElectricityAndWaterDetails(Map<Integer, Double> electricityandWaterDetails,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> waterAndElectricityForEach = new LinkedHashMap<>();
		try {
			electricityandWaterDetails.forEach((key, value) -> {
				double waterandElectricity = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				waterAndElectricityForEach.put(key, waterandElectricity);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching water and electricity for each years");
		}
		return waterAndElectricityForEach;
	}

	public Map<Integer, Double> getRepairAndMaintanceDetails(Map<Integer, Double> repairAndMaintainceDetails,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> repairAndMaintaince = new LinkedHashMap<>();
		try {
			repairAndMaintainceDetails.forEach((key, value) -> {
				double repairMaintaince = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				repairAndMaintaince.put(key, repairMaintaince);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching repair and maintance for each years");
		}
		return repairAndMaintaince;
	}

	public Map<Integer, Double> getOtherOperatingExpenseDetails(Map<Integer, Double> netSaleOfTicketDetails,

			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> otherOperating = new LinkedHashMap<>();
		try {
			netSaleOfTicketDetails.forEach((key, value) -> {
				double otherOperatingTotal = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				otherOperating.put(key, otherOperatingTotal);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching other operating expense for each years");

		}
		return otherOperating;
	}

	public Map<Integer, Double> getExpenditureExrentAndCampDetails(Map<Integer, Double> expenditureExRentAndCamp,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> expenditureExRentEach = new LinkedHashMap<>();
		try {
			expenditureExRentAndCamp.forEach((key, value) -> {
				double expenditureExRent = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				expenditureExRentEach.put(key, expenditureExRent);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching expenditure ex rent for each years");
		}
		return expenditureExRentEach;
	}

	public Map<Integer, Double> getEbitDarDetails(Map<Integer, Double> ebitDarDetails,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> ebitDarDetailsForEach = new LinkedHashMap<>();
		try {
			ebitDarDetails.forEach((key, value) -> {
				double ebitdar = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				ebitDarDetailsForEach.put(key, ebitdar);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching ebit dar details for each years");

		}
		return ebitDarDetailsForEach;
	}

	public Map<Integer, Double> getRentDetails(Map<Integer, Double> rentDetails, Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> rent = new LinkedHashMap<>();
		try {
			rentDetails.forEach((key, value) -> {
				double rentExpense = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				rent.put(key, rentExpense);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching rent details for each years");
		}
		return rent;
	}

	public Map<Integer, Double> getCamDetails(Map<Integer, Double> camDetails, Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> camExpense = new LinkedHashMap<>();
		try {
			camDetails.forEach((key, value) -> {
				double cam = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				camExpense.put(key, cam);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching cam details for each years");
		}
		return camExpense;
	}

	public Map<Integer, Double> getEbitDaDetails(Map<Integer, Double> ebitDaDetails,
			Map<Integer, Double> totalRevenue) {
		Map<Integer, Double> ebitda = new LinkedHashMap<>();
		try {
			ebitDaDetails.forEach((key, value) -> {
				double ebitd = MathUtil.roundTwoDecimals((value / totalRevenue.get(key)) * 100);
				ebitda.put(key, ebitd);
			});

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching ebitda  details for each years");
		}
		return ebitda;
	}



	public Map<Integer, Double> getshowtaxForEachYear(Double showTax, Double totalShow,
			Optional<ProjectDetails> projectDetails, List<FixedConstant> fixedConstant) {

		Map<Integer, Double> getshowtaxForEachYear = new LinkedHashMap<>();
		try {
//			Map<Integer,Double>  growthRate  =  fixedConstant.get(0).getRevenueAssumptionsGrowthRate().getAdmitsGrowth();
			Map<Integer,Double>  growthRate  =  projectDetails.get().getRevenueAssumptionsGrowthRate().getAdmitsGrowth();

			int size = growthRate.size() + 1;
			int  leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			int rate = 0;
						
			if(leasePeriod >= size  ) {
				rate = size;
			}else {
				int val = size - leasePeriod;
				rate = size - val;
			}
			
			for (int i = 1; i <= rate; i++) {

				Double tax = MathUtil.roundTwoDecimals(showTax * totalShow);
				getshowtaxForEachYear.put(i, tax);

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching security show tax For Each Year");

		}
		return getshowtaxForEachYear;

	}

	public Map<String, Double> getProjectCostSummaryData(Optional<ProjectDetails> projectDetails) {

		Double factor = 100000.0;

		Map<String, Double> projectCostSummaryData = new HashMap<>();
		Double totalProjectCost = 0.0;
		Double salvageFromDisposalAssets = 0.0;
		Double grandTotal = 0.0;
		Double depRate = 0.0;
		Double totalProjectCostAfterContingencies = 0.0;

		try {
			ProjectCostSummary projectCost = projectCostSummaryDao.findByParentId(projectDetails.get().getId());

			List<ProjectCostData> projectCostDataList = projectCost.getProjectCostDataList();

			List<ProjectCostData> projectCostData = projectCostDataList.stream().filter(Objects::nonNull)
					.collect(Collectors.toList());

			for (int i = 0; i < projectCostData.size(); i++) {

				if (!ObjectUtils.isEmpty(projectCostDataList.get(i).getDescription())) {
					if (projectCostDataList.get(i).getDescription().equalsIgnoreCase(Constants.TOTAL_PROJECT_COST)) {
						if (!ObjectUtils.isEmpty(projectCostDataList.get(i).getCost())) {
							totalProjectCost = MathUtil.roundTwoDecimals(projectCostDataList.get(i).getCost() * factor);
						}
					}

					if (projectCostDataList.get(i).getDescription()
							.equalsIgnoreCase(Constants.SALVAGE_DISPOSAL_ASSETS)) {
						if (!ObjectUtils.isEmpty(projectCostDataList.get(i).getCost())) {
							salvageFromDisposalAssets = projectCostDataList.get(i).getCost() * Constants.FACTOR;
						}
					}

					if (projectCostDataList.get(i).getDescription().equalsIgnoreCase(Constants.GRAND_TOTAL)) {
						if (!ObjectUtils.isEmpty(projectCostDataList.get(i).getCost())) {
							grandTotal = MathUtil.roundTwoDecimals(projectCostDataList.get(i).getCost() * factor);
						}
					}

					if (projectCostDataList.get(i).getDescription().equalsIgnoreCase(Constants.TOTAL_PROJECT_COST)) {
						if (!ObjectUtils.isEmpty(projectCostDataList.get(i).getDepRate())) {
							depRate = projectCostDataList.get(i).getDepRate();
						}
					}

					if (projectCostDataList.get(i).getDescription()
							.equalsIgnoreCase(Constants.TOTAL_PROJECT_COST_AFTER_CONTINGENCIES)) {
						if (!ObjectUtils.isEmpty(projectCostDataList.get(i).getCost())) {
							totalProjectCostAfterContingencies = MathUtil
									.roundTwoDecimals(projectCostDataList.get(i).getCost() * factor);
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching project cost summary data");

		}
		projectCostSummaryData.put("totalProjectCost", totalProjectCost);
		projectCostSummaryData.put("salvageFromDisposalAssets", salvageFromDisposalAssets);
		projectCostSummaryData.put("grandTotal", grandTotal);
		projectCostSummaryData.put("depRate", depRate);
		projectCostSummaryData.put("totalProjectCostAfterContingencies", totalProjectCostAfterContingencies);

		return projectCostSummaryData;
	}

}
