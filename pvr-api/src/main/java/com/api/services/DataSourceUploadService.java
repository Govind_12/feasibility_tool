package com.api.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.api.dto.Filter;
import com.common.models.BbdoMaster;
import com.common.models.CategoryDataSource;
import com.common.models.CinemaMaster;
import com.common.models.CityTier;
import com.common.models.CompitionMaster;
import com.common.models.Navision;
import com.common.models.ProjectCostSummary;
import com.common.sql.models.SeatCategory;

public interface DataSourceUploadService {

	List<CinemaMaster> save(List<CinemaMaster> cinemaMasterMapper);

	List<CompitionMaster> saveCompitionMaster(List<CompitionMaster> compitionMaster);

	List<BbdoMaster> saveBbdoMaster(List<BbdoMaster> bbdoMaster);

	List<CityTier> saveCityTierMaster(List<CityTier> cityTier);

	List<Navision> saveNavisionMaster(List<Navision> navision);
	
	List<CategoryDataSource> saveCategoryMaster(List<CategoryDataSource> categoryMaster);
	
	List<SeatCategory> saveSeatCategoryMaster(List<SeatCategory> SeatcategoryMaster);

	ProjectCostSummary saveProjectCostSummary(ProjectCostSummary projectCostSummary);

	ProjectCostSummary fetchAllProjectCost(String projectId);

}
