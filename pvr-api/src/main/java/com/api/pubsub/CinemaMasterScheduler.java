/*package com.api.pubsub;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.api.dao.CinemaMasterDao;
import com.api.dao.sql.CinemaMasterSqlDao;
import com.common.constants.Constants;
import com.common.models.CinemaMaster;
import com.common.sql.models.CinemaMasterSql;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CinemaMasterScheduler {

	@Autowired
	private CinemaMasterSqlDao cinemaMasterSqlDao;

	@Autowired
	private CinemaMasterDao cinemaMasterDao;

	@Value("${google.api.key}")
	private String googleKey;


	@Scheduled(cron = "0 02 19 1/1 * ?")
	public void create() throws ParseException {
		log.info("cinema master scheduler started....");

		try {
			List<CinemaMasterSql> cinemaMasterSql = cinemaMasterSqlDao.findAllCinemaMaster();
			cinemaMasterDao.deleteAll();
			cinemaMasterSql.stream().forEach(cinema -> {
				CinemaMaster cinemaDetails = new CinemaMaster();
				//CinemaMaster exist = cinemaMasterDao.findByHopk(Integer.valueOf(cinema.getHOPK()));

				//if (ObjectUtils.isEmpty(exist)) {
					cinemaDetails.setHopk(Integer.valueOf(cinema.getHOPK()));
					cinemaDetails.setCinema_strCode(cinema.getCinema_strCode());
					cinemaDetails.setCinema_name(cinema.getCinema_Name());
					cinemaDetails.setVista_info_Cinema_Name(cinema.getVista_info_Cinema_Name());
					cinemaDetails.setCinema_location(cinema.getCinema_Location());
					cinemaDetails.setState_strName(cinema.getState_strName());
					cinemaDetails.setNavCinemaCode(cinema.getNav_cinemaCode());
					cinemaDetails.setOwner_strCompany(cinema.getOWNER_strCompany());
					cinemaDetails.setCinema_Resign(cinema.getCinema_Region());
					cinemaDetails.setNav_cinemadesc(cinema.getNav_cinemaCode());
					cinemaDetails.setCinema_str_Run(cinema.getCinema_strRun());
					cinemaDetails.setInfoworks_cin_str(cinema.getVista_info_Cinema_Name());
					cinemaDetails.setNoOfScreen(cinema.getNo_of_Audi());
					cinemaDetails.setGold(cinema.getGold());
					cinemaDetails.setPxl(cinema.getPXL());
					cinemaDetails.setUltraPremium(cinema.getUltraPremium());
					cinemaDetails.setDx(cinemaDetails.getDx());
					cinemaDetails.setPremier(cinema.getPremier());
					cinemaDetails.setImax(cinema.getIMAX());
					cinemaDetails.setPlayHouse(cinema.getPlayhouse());
					cinemaDetails.setOnxy(cinema.getONYX());
					cinemaDetails.setMainStream(cinema.getMainstream());
					cinemaDetails.setLatitude(Double.valueOf(cinema.getLatitude()));
					cinemaDetails.setLongitude(Double.valueOf(cinema.getLongitude()));
					Map<String, Double> latLng;
					try {
						latLng = getAddressByLatLong(cinema.getCinema_Name());
						cinemaDetails.setLatitude(latLng.get(Constants.LATITUDE));
						cinemaDetails.setLongitude(latLng.get(Constants.LONGITUDE));
					} catch (IOException | ParseException e) {
						e.printStackTrace();
					}
					cinemaMasterDao.save(cinemaDetails);
				//}

			});
		} catch (Exception e) {
			log.error("error while fetching cinema master in scheduler...");
		}
		log.info("cinema master scheduler  finished....");
	}

	public Map<String, Double> getAddressByLatLong(String fullAddress) throws IOException, ParseException {
		Map<String, Double> map = new HashMap<>();
		double lat = 0;
		double lng = 0;
		try {
			synchronized (this) {
				URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json" + "?address="
						+ URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false" + "&" + "key=" + googleKey + " ");
				URLConnection conn = url.openConnection();
				ByteArrayOutputStream output = new ByteArrayOutputStream(1024);

				IOUtils.copy(conn.getInputStream(), output);
				output.close();

				ObjectMapper mapper = new ObjectMapper();
				JsonNode array = mapper.readValue(output.toString(), JsonNode.class);

				JsonNode object = array.get("results").get(0);

				JsonNode geometry = object.get("geometry");
				JsonNode location = geometry.get("location");

				lat = location.get("lat").asDouble();
				lng = location.get("lng").asDouble();

			}
			map.put(Constants.LATITUDE, lat);
			map.put(Constants.LONGITUDE, lng);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching google api geocoding api");
		}
		return map;
	}
}
*/