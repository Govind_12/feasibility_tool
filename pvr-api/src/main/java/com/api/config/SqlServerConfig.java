package com.api.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.api.dao.sql.MsSqlDaoPackage;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

@Configuration
@EnableJpaRepositories(basePackageClasses = MsSqlDaoPackage.class)
/*@EntityScan("com.common.models")*/
@EntityScan("com.common.sql.models")
public class SqlServerConfig {

	@Value("${spring.datasource.url}")
	private String url;

	@Value("${spring.datasource.username}")
	private String username;

	@Value("${spring.datasource.password}")
	private String password;

	@Bean
	DataSource dataSource() throws SQLException {

		SQLServerDataSource dataSource = new SQLServerDataSource();
		dataSource.setUser(username);
		dataSource.setPassword(password);
		dataSource.setURL(url);
		return dataSource;
	}
}
