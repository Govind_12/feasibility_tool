package com.api.exception;

/**
 * @author RITESH SINGH
 * @since JDK 1.8
 * @version 1.0
 *
 */
public class FeasibilityException extends RuntimeException {

	private static final long serialVersionUID = 2771174581631905388L;

	public FeasibilityException() {
	}

	public FeasibilityException(String message) {
		super(message);
	}

	public FeasibilityException(Throwable cause) {
		super(cause);
	}

	public FeasibilityException(String message, Throwable cause) {
		super(message, cause);
	}

	public FeasibilityException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
