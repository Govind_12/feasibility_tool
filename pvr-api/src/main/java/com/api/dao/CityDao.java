package com.api.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.common.models.City;

public interface CityDao extends PagingAndSortingRepository<City, String> {

}
