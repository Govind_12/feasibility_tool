package com.api.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.common.models.SeatTypeMaster;

public interface SeatTypeMasterDao extends PagingAndSortingRepository<SeatTypeMaster, String> {

	List<SeatTypeMaster> findAll();
	
	SeatTypeMaster findOneById(String id);
	
	SeatTypeMaster findOneBySeatType(String seatType);
	
}
