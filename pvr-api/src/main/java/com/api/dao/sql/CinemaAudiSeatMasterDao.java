package com.api.dao.sql;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.common.sql.models.CinemaAudiSeatMaster;

@Repository
public interface CinemaAudiSeatMasterDao extends PagingAndSortingRepository<CinemaAudiSeatMaster, String>{

	@Query(value = "SELECT (Area_intNumSeatsTot) as Area_intNumSeatsTot from Cinema_audi_seat_Master_v1 where Cinema_HOPK = :hopk", nativeQuery = true)
	List<String> findAllByCinemaHopkAndDate(String hopk);
	
}
