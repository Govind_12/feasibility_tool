package com.api.dao.sql;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.sql.models.CinemaPerformance;

@Repository
public interface CinemaPerformanceDao extends PagingAndSortingRepository<CinemaPerformance, Integer> {

	@Query(value = "select top 5 (Cinema_hopk) as Cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_State) as Cinema_State,(Cinema_City) as Cinema_City,(Business_Date) as Business_Date,(Admits) as Admits,(GBOC) as GBOC,(Concession_Net_Revenue) as Concession_Net_Revenue,(Total_Seats_Available) as Total_Seats_Available,(BO_No_of_Trans) as BO_No_of_Trans,(Conc_No_of_Trans) as Conc_No_of_Trans  from tbl_Cinema_performance", nativeQuery = true)
	List<CinemaPerformance> findAllCinemaPerformance();

	/*@Query(value = "select  (Cinema_hopk) as Cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_State) as Cinema_State,(Cinema_City) as Cinema_City,(Business_Date) as Business_Date,(Admits) as Admits,(GBOC) as GBOC,(Concession_Net_Revenue) as Concession_Net_Revenue,(Total_Seats_Available) as Total_Seats_Available,(BO_No_of_Trans) as BO_No_of_Trans,(Conc_No_of_Trans) as Conc_No_of_Trans  from tbl_Cinema_performance where Cinema_hopk IN (:hopk) and Business_Date BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Object[]> findAllCinemaPerformanceByHopkInAndDate(List<Integer> hopk,Date d1,Date d2);*/
	
	
	@Query(value = "select  (Cinema_hopk) as Cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_State) as Cinema_State,(Cinema_City) as Cinema_City,(Business_Date) as Business_Date,(Admits) as Admits,(GBOC) as GBOC,(Concession_Net_Revenue) as Concession_Net_Revenue,(Total_Seats_Available) as Total_Seats_Available,(BO_No_of_Trans) as BO_No_of_Trans,(Conc_No_of_Trans) as Conc_No_of_Trans  from tbl_Cinema_performance where Cinema_hopk IN (:hopk) and  Business_Date<'2018-10-1'", nativeQuery = true)
	List<Object[]> findAllCinemaPerformanceByHopkInAndDate(List<Integer> hopk);
	
	/*@Query(value = "SELECT SUM(Concession_Net_Revenue) as totalConscessionNetRevenue,SUM(Admits) as totalAdmits from tbl_Cinema_performance where cinema_hopk = :hopk and Business_Date BETWEEN (:	) And (:d2)", nativeQuery = true)
	List<Object[]> findAllCinemaPerformanceByHopkAndDate(Integer hopk,Date d1,Date d2);*/
	
	@Query(value = "SELECT SUM(Concession_Net_Revenue) as totalConscessionNetRevenue,SUM(Admits) as totalAdmits from tbl_Cinema_performance where cinema_hopk = :hopk and  Business_Date<'2018-10-1'", nativeQuery = true)
	List<Object[]> findAllCinemaPerformanceByHopkAndDate(Integer hopk);
	
}
