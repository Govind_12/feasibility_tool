package com.api.dao.sql;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.sql.models.SeatCategory;

@Repository
public interface SeatCategoryDao extends PagingAndSortingRepository<SeatCategory, Integer> {

	@Query(value = "select top 5 (cinemaHopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category", nativeQuery = true)
	List<SeatCategory> findAll();

	// @Query("SELECT u FROM SeatCategory u where u.cinema_hopk IN :hopk AND
	// u.Movie_show_Time BETWEEN :d1 And :d2")
	/*@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Movie_show_Time BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Object[]> findAllByCinemaHopkIn(List<Integer> hopk, Date d1, Date d2);*/
	
	@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Movie_show_Time > '2018-10-1'", nativeQuery = true)
	List<Object[]> findAllByCinemaHopkIn(List<Integer> hopk);
	

	/*@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Cinema_Format IN (:cinemaFormat) and Movie_show_Time BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Object[]> findAllByCinemaHopkInAndCinemaFormat(List<Integer> hopk, List<String> cinemaFormat, Date d1,
			Date d2);*/
	
	/*
	 * @Query(value =
	 * "SELECT (cinema_hopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Cinema_Format IN (:cinemaFormat)"
	 * , nativeQuery = true) List<Object[]>
	 * findAllByCinemaHopkInAndCinemaFormat(List<Integer> hopk, List<String>
	 * cinemaFormat);
	 */

	@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(admits) as admits,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Cinema_Format IN (:cinemaFormat)", nativeQuery = true)
	List<Object[]> findAllByCinemaHopkInAndCinemaFormat(List<Integer> hopk, List<String> cinemaFormat);
/*	@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Cinema_Format IN (:cinemaFormat) and Area_category IN(:areaCategory) and Movie_show_Time BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Object[]> findAllByCinemaHopkInAndCinemaFormatInAndAreaCategory(List<Integer> hopk, List<String> cinemaFormat,
			List<String> areaCategory, Date d1, Date d2);*/
	
	@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Cinema_Format IN (:cinemaFormat) and Area_category IN(:areaCategory) and Movie_show_Time > '2018-10-1'", nativeQuery = true)
	List<Object[]> findAllByCinemaHopkInAndCinemaFormatInAndAreaCategory(List<Integer> hopk, List<String> cinemaFormat,
			List<String> areaCategory);
	
	

	/*@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Cinema_Format IN (:cinemaFormat) and Area_category NOT IN(:areaCategory) and Movie_show_Time BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Object[]> findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(List<Integer> hopk,
			List<String> cinemaFormat, List<String> areaCategory, Date d1, Date d2);*/
	
	
	
	/*@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Cinema_Format IN (:cinemaFormat) and Area_category IN(:areaCategory) and Movie_show_Time BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Object[]> findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(List<Integer> hopk,
			List<String> cinemaFormat, List<String> areaCategory, Date d1, Date d2);*/

	@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Cinema_Format IN (:cinemaFormat) and Area_category IN(:areaCategory)  and Movie_show_Time > '2018-10-1'", nativeQuery = true)
	List<Object[]> findAllByCinemaHopkInAndCinemaFormatInAndAreaCategoryNormal(List<Integer> hopk,
			List<String> cinemaFormat, List<String> areaCategory);

	
	
	/*@Query(value = "SELECT SUM(admits) as admitsSum,SUM(GBOC) as sumGboc from temp_table_seat_Category where cinema_hopk = :hopk and Movie_show_Time BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Object[]> findAllByGbocAndAdmitsHopk(Integer hopk, Date d1, Date d2);*/
	
	@Query(value = "SELECT SUM(admits) as admitsSum,SUM(GBOC) as sumGboc from temp_table_seat_Category where cinema_hopk = :hopk and Movie_show_Time > '2018-10-1'", nativeQuery = true)
	List<Object[]> findAllByGbocAndAdmitsHopk(Integer hopk);
	

	/*@Query(value = "SELECT SUM(admits) as admitsSum,SUM(Total_Seats) as totalSeats from temp_table_seat_Category where cinema_hopk = :hopk and Movie_show_Time BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Object[]> findAllByOccupancysHopkAndAdmit(Integer hopk, Date d1, Date d2);*/
	
	
	@Query(value = "SELECT SUM(admits) as admitsSum,SUM(Total_Seats) as totalSeats from temp_table_seat_Category where cinema_hopk = :hopk and Movie_show_Time > '2018-10-1' ", nativeQuery = true)
	List<Object[]> findAllByOccupancysHopkAndAdmit(Integer hopk);
	
	@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Cinema_Name) as Cinema_Name,(Cinema_state) as Cinema_state,(finyear) as finyear, (QTR) as QTR, (Cinema_City) as Cinema_City, (Cinema_Format) as Cinema_Format, (Movie_show_Time) as Movie_show_Time, (CinOperator_strCode) as cin_operator_str_code,(Audi_Number) as Audi_Number,(Area_category) as Area_category,(area_cat_Code) as area_cat_Code,(Session_lngSessionId) as session_lng_session_id,(Film_strCode) as film_str_code,(Movie_Name) as Movie_Name,(admits) as admits,(GBOC) as GBOC,(Total_Seats) as Total_Seats from temp_table_seat_Category where cinema_hopk IN (:hopk) and Cinema_Format IN (:cinemaFormat) and Movie_show_Time > '2018-10-1' and  finyear  IN (  SELECT MAX ((finyear)) as finyear  FROM temp_table_seat_Category)", nativeQuery = true)
	List<SeatCategory> findAllByCinemaHopkInAndCinemaFormatAndQuater(List<Integer> hopk, List<String> cinemaFormat);
	
}
