package com.api.dao.sql;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.common.sql.models.TicketOfflineOnline;

@Repository
public interface TicketOnlineOfflineDao extends PagingAndSortingRepository<TicketOfflineOnline, Integer> {
	
	/*@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Session_Business_Date) as Session_Business_Date,(Channel_Group) as Channel_Group, (Session_no) as Session_no, (Audi_number) as Audi_number, (Admits) as Admits from Ticket_online_Offline where cinema_hopk IN (:hopks) and Session_Business_Date BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<TicketOfflineOnline> findAllByDateAndhopksOnline(List<Integer> hopks,Date d1,Date d2);*/
	
	
	@Query(value = "SELECT (cinema_hopk) as cinema_hopk,(Session_Business_Date) as Session_Business_Date,(Channel_Group) as Channel_Group, (Session_no) as Session_no, (Audi_number) as Audi_number, (Admits) as Admits from Ticket_online_Offline where cinema_hopk IN (:hopks)", nativeQuery = true)
	List<TicketOfflineOnline> findAllByDateAndhopksOnline(List<Integer> hopks);
	
/*	@Query(value = "SELECT AVG(Admits) as avg, SUM(Admits) as sum from Ticket_online_Offline where cinema_hopk IN (:hopks) and Channel_Group = :channelsGroup  and Session_Business_Date BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Object[]> avgAdmitsOnline(List<Integer> hopks,Date d1,Date d2,String channelsGroup);*/
	
	
	
	//this is the latest
	/*@Query(value = "SELECT Admits as Admits from Ticket_online_Offline where cinema_hopk IN (:hopks) and Channel_Group = :channelsGroup  and Session_Business_Date BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Integer> avgAdmitsOnline(List<Integer> hopks,Date d1,Date d2,String channelsGroup);*/
	
	@Query(value = "SELECT Admits as Admits from Ticket_online_Offline where cinema_hopk IN (:hopks) and Channel_Group = :channelsGroup and Session_Business_Date>'2018-10-1' ", nativeQuery = true)
	List<Integer> avgAdmitsOnline(List<Integer> hopks,String channelsGroup);
	
	
	/*@Query(value = "SELECT Admits as Admits from Ticket_online_Offline where cinema_hopk IN (:hopks) and Channel_Group = :channelsGroup  and Session_Business_Date BETWEEN (:d1) And (:d2)", nativeQuery = true)
	List<Integer> avgAdmitsOnlineHopks(Integer hopks,Date d1,Date d2,String channelsGroup);*/
	
	@Query(value = "SELECT Admits as Admits from Ticket_online_Offline where cinema_hopk IN (:hopks) and Channel_Group = :channelsGroup  and Session_Business_Date>'2018-10-1' ", nativeQuery = true)
	List<Integer> avgAdmitsOnlineHopks(Integer hopks,String channelsGroup);
	
}
