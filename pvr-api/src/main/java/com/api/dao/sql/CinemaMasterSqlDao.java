package com.api.dao.sql;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.sql.models.CinemaMasterSql;

@Repository
public interface CinemaMasterSqlDao extends PagingAndSortingRepository<CinemaMasterSql, String> {

	// @Query("SELECT u from CinemaMasterSql u")
	@Query(value = "select  (HOPK) as HOPK,(Cinema_strCode) as cinema_str_code,(Cinema_Name) as Cinema_Name,(Vista_info_Cinema_Name) as Vista_info_Cinema_Name,(Cinema_Location) as Cinema_Location,(State_strName) as state_str_name,(Nav_cinemaCode) as nav_cinema_code,(OWNER_strCompany) as owner_str_company,(Cinema_Region) as Cinema_Region,(Nav_cinemadesc) as Nav_cinemadesc,(Cinema_strRun) as cinema_str_run,(latitude) as latitude,(longitude) as longitude,(loyalty_complex_id) as loyalty_complex_id,(No_of_Audi) as No_of_Audi,(Gold) as Gold,(PXL) as PXL,(UltraPremium) as ultra_premium,(DX) as DX,(Premier) as Premier,(IMAX) as IMAX,(Playhouse) as Playhouse,(ONYX) as ONYX,(Mainstream) as Mainstream  from Cinema_master_Data_v1", nativeQuery = true)
	List<CinemaMasterSql> findAllCinemaMaster();

	@Query(value = "select (HOPK) as HOPK WHERE Nav_cinemadesc=?", nativeQuery = true)
	List<String> findAllCinemaMasterByNavCinemaCode(List<String> list);

	@Query(value = "select  (HOPK) as HOPK,(Cinema_strCode) as cinema_str_code,(Cinema_Name) as Cinema_Name,(Vista_info_Cinema_Name) as Vista_info_Cinema_Name,(Cinema_Location) as Cinema_Location,(State_strName) as state_str_name,(Nav_cinemaCode) as nav_cinema_code,(OWNER_strCompany) as owner_str_company,(Cinema_Region) as Cinema_Region,(Nav_cinemadesc) as Nav_cinemadesc,(Cinema_strRun) as cinema_str_run,(latitude) as latitude,(longitude) as longitude,(loyalty_complex_id) as loyalty_complex_id,(No_of_Audi) as No_of_Audi,(Gold) as Gold,(PXL) as PXL,(UltraPremium) as ultra_premium,(DX) as DX,(Premier) as Premier,(IMAX) as IMAX,(Playhouse) as Playhouse,(ONYX) as ONYX,(Mainstream) as Mainstream  from Cinema_master_Data_v1 where Nav_cinemaCode IN (:navCode)", nativeQuery = true)
	List<CinemaMasterSql> findAllCinemaMasterByNavCode(List<String> navCode);

	@Query(value = "select  (HOPK) as HOPK,(Cinema_strCode) as cinema_str_code,(Cinema_Name) as Cinema_Name,(Vista_info_Cinema_Name) as Vista_info_Cinema_Name,(Cinema_Location) as Cinema_Location,(State_strName) as state_str_name,(Nav_cinemaCode) as nav_cinema_code,(OWNER_strCompany) as owner_str_company,(Cinema_Region) as Cinema_Region,(Nav_cinemadesc) as Nav_cinemadesc,(Cinema_strRun) as cinema_str_run,(latitude) as latitude,(longitude) as longitude,(loyalty_complex_id) as loyalty_complex_id,(No_of_Audi) as No_of_Audi,(Gold) as Gold,(PXL) as PXL,(UltraPremium) as ultra_premium,(DX) as DX,(Premier) as Premier,(IMAX) as IMAX,(Playhouse) as Playhouse,(ONYX) as ONYX,(Mainstream) as Mainstream  from Cinema_master_Data_v1 where Cinema_Location = :cityName", nativeQuery = true)
	List<CinemaMasterSql> findAllCinemaMasterByCityName(String cityName);

}
