package com.api.dao;

import java.util.List;

import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.CinemaMaster;

@Repository
public interface CinemaMasterDao  extends PagingAndSortingRepository<CinemaMaster,String>{
	
	List<CinemaMaster> findByLocationNear(Point point,Distance distance);
	List<CinemaMaster> findAllByCityTier(String cityTier);
	List<CinemaMaster> findAllByCinemaCategory(String cinemaCategory);
	List<CinemaMaster> findAllByNavCinemaCodeIn(List<String> navCinemaCode);
	CinemaMaster findByHopk(Integer hopk);
	CinemaMaster findByNavCinemaCode(String navCode);
}
