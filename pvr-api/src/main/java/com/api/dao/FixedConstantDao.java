package com.api.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.FixedConstant;

@Repository
public interface FixedConstantDao extends PagingAndSortingRepository<FixedConstant, String>{
	
	
	
	
}
