package com.api.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.CityTier;

@Repository
public interface CityTierDao extends PagingAndSortingRepository<CityTier, String>{
	CityTier findByCity(String city);
	List<CityTier> findAllByTierIn(List<String> cityTier);
}
