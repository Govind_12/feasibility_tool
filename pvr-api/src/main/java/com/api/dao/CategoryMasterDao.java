package com.api.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.CategoryDataSource;

@Repository
public interface CategoryMasterDao extends PagingAndSortingRepository<CategoryDataSource, String>{

List<CategoryDataSource> findAllByCinemaNavCodeIn(List<String> listnavCode);
CategoryDataSource findByCinemaNavCode(String cityName);
}
