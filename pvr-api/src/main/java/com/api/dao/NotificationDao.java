package com.api.dao;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.Notification;

@Repository
/*public interface NotificationDao extends JpaRepository<Notification, String>{*/
	public interface NotificationDao extends PagingAndSortingRepository<Notification, String>{
	
	List<Notification> findAllByType(String type);
	
	List<Notification> findAllByTypeAndApproverId(String type, String approverId);
	List<Notification> findAllByTypeAndCreatedBy(String type, String id);
	
	
	@Query("[{$sort: {'_id': 1 }},{$group: { \"_id\": \"$projectId\",\"projectId\": {\"$last\":\"$projectId\"}, msg: {\"$last\": '$msg'},status: {\"$last\": '$status'},type: {\"$last\": '$type'},approverId: {\"$last\": '$approverId'},projectType: {\"$last\": '$projectType'}, createdBy: {\"$last\": '$createdBy'}}},{$match: {type: ?0, createdBy: ?1}}]")
	List<Notification> test(String type, String id);
	
	/*@Query("{$sort: {'_id': 1 }},{$group: { \"_id\": \"$projectId\",\"projectId\": {\"$last\":\"$projectId\"}, msg: {\"$last\": '$msg'},status: {\"$last\": '$status'},type: {\"$last\": '$type'},approverId: {\"$last\": '$approverId'},projectType: {\"$last\": '$projectType'}, createdBy: {\"$last\": '$createdBy'}}},")
	List<Notification> test();*/
	
}
