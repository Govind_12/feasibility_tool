package com.api.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.common.models.CinemaFormatMaster;

public interface CinemaFormatMasterDao extends PagingAndSortingRepository<CinemaFormatMaster, String> {

	List<CinemaFormatMaster> findAll();
	
	CinemaFormatMaster findOneById(String id);
	
	CinemaFormatMaster findOneByCinemaCategory(String cinemaCategory);
}
