package com.api.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.BbdoMaster;

@Repository
public interface BbdoMasterDao extends PagingAndSortingRepository<BbdoMaster,String>{

	@Query(value = "select b.popn from bbdo_master b where b.town= :cityName", nativeQuery = true)
	float findPopnByTown(String cityName);

	BbdoMaster findByTown(String city);

}
