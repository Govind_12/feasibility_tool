package com.api.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.ProjectCostSummary;

@Repository
public interface ProjectCostSummaryDao extends PagingAndSortingRepository<ProjectCostSummary, String> {

	ProjectCostSummary findByParentId(String projectId);
	List<ProjectCostSummary> findAllByParentId(String projectId);
}
