package com.api.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.LogDetail;

@Repository
public interface LogDetailDao extends PagingAndSortingRepository<LogDetail, String>{
	
	List<LogDetail> findAllByOrderByCreatedDateDesc();

	List<LogDetail> findAllByUsernameOrderByCreatedDateDesc(String username);
	
}
