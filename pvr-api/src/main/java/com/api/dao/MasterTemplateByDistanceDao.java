package com.api.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.MasterTemplateByDistance;
import com.common.models.ProjectCostSummary;

@Repository
public interface MasterTemplateByDistanceDao extends PagingAndSortingRepository<MasterTemplateByDistance, String> {

	MasterTemplateByDistance findByProjectId(String projectId);
}
