package com.api.dao;

import java.util.List;

import java.util.Optional;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.common.models.ProjectDetails;

public interface ProjectDetailsDao extends PagingAndSortingRepository<ProjectDetails, String> {

	@Query(value = "{ 'status' : ?0 }")
	List<ProjectDetails> getProjectByStatus(Integer status);
	
	@Query(value = "{ 'status' : {'$in': ?0} }")
	List<ProjectDetails> getProjectByInStatus(List<Integer> status);
	
	@Query(value = "{ 'status' : {'$gte': ?0} }")
	List<ProjectDetails> getProjectGreaterThanEqualStatus(Integer status);
	
	@Query(value = "{ 'status' : {'$gte': ?0}, 'feasibilityState': ?1 }")
	List<ProjectDetails> getProjectGreaterThanEqualStatusAndEqualFeasibilityState(Integer status, String feasibilityState);
	
	ProjectDetails findTopByOrderByReportIdVersionDesc();
	
	ProjectDetails findByReportId(String reportId);
	
	Optional<List<ProjectDetails>> findByFeasibilityStateAndStatusAndProjectBdId(String feasibilityState, int status,String createdBy);
	
	Optional<List<ProjectDetails>> findByReportIdVersion(Integer reportIdVersion);
	
	List<ProjectDetails> findByStatusIn(List<Integer> status);
	
	List<ProjectDetails> findByStatusInAndProjectBdId(List<Integer> status,String projectBdId);

	ProjectDetails findByReportIdVersionAndReportIdSubVersion(Integer reportIdVersion, Integer reportIdSubVersion);

	List<ProjectDetails> findAllByFeasibilityStateAndStatus(String feasibilityState, int status);

	List<ProjectDetails> findAllByReportIdVersion(Integer projectId);


}
