package com.api.dao;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.MultipliersMaster;

@Repository
public interface MultiplierMasterDao extends PagingAndSortingRepository<MultipliersMaster, String> {
	MultipliersMaster findOneById(String id, int status);
	Optional<MultipliersMaster> findByCostType(String costType);
}
