package com.api.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.RevenueExpense;

@Repository
public interface RevenueExpenseDao extends PagingAndSortingRepository<RevenueExpense, String>{
	
	List<RevenueExpense> findAllByProjectId(String id);
}
