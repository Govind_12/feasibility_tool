package com.api.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.ExistingCinemaLandScape;

@Repository
public interface CinemaLandScapeDao extends PagingAndSortingRepository<ExistingCinemaLandScape,String>{
	
	ExistingCinemaLandScape findByProjectId(String projectId);

}
