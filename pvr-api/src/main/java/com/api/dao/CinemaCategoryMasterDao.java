package com.api.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.common.models.CinemaCategoryMaster;

public interface CinemaCategoryMasterDao extends PagingAndSortingRepository<CinemaCategoryMaster, String> {

	List<CinemaCategoryMaster> findAll();
	
	CinemaCategoryMaster findOneById(String id);
	
	CinemaCategoryMaster findOneByCinemaCategory(String cinemaCategory);
}
