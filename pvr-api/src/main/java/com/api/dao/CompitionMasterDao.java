package com.api.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.CompitionMaster;

@Repository
public interface CompitionMasterDao extends PagingAndSortingRepository<CompitionMaster, String>{

	List<CompitionMaster> findAllByCompititionName(String compitionName);
}
