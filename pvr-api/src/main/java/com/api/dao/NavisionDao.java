package com.api.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.common.models.Navision;

@Repository
public interface NavisionDao extends PagingAndSortingRepository<Navision, String>{

	List<Navision> findAllByCompType(String compType);

	List<Navision> findAllByYearInAndQuaterIn(List<String> years, List<String> allQuater);

	List<Navision> findAllByNavCinemaCodeIn(List<String> navCinemaCode);

}
