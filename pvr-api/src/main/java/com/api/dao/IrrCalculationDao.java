package com.api.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.common.models.IrrCalculationData;

public interface IrrCalculationDao  extends PagingAndSortingRepository<IrrCalculationData, String>{
	
	
	IrrCalculationData findByProjectId(String projectId);

}
