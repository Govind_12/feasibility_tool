package com.api.service.popup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.IrrCalculationDao;
import com.api.dao.ProjectDetailsDao;
import com.api.exception.FeasibilityException;
import com.api.service.report.TemplateDataService;
import com.api.services.ProjectAnalysisService;
import com.api.services.ProjectRevenueAnalysisService;
import com.api.utils.ChangeCinemaFormat;
import com.api.utils.MathUtil;
import com.common.constants.Constants;
import com.common.models.AssumptionsValues;
import com.common.models.IrrCalculationData;
import com.common.models.ProjectDetails;

import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ViewModalServiceImpl implements ViewModalService {

	@Autowired
	private TemplateDataService templateDataService;
	@Autowired
	private ProjectDetailsDao projectDetailsDao;
	@Autowired
	private ProjectAnalysisService projectAnalysisService;
	@Autowired
	private ProjectRevenueAnalysisService projectRevenueAnalysisService;
	
	@Autowired
	private IrrCalculationDao irrCalculationDao;

	@SuppressWarnings("rawtypes")
	public Map<String, ? super ArrayList> getTotalAdmits(String projectId) {

		if (ObjectUtils.isEmpty(projectId)) {
			throw new FeasibilityException("Project id cannot be null or empty");
		}
		Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);
		Map<String, ? super ArrayList> map = new HashMap<>();
		map.put(Constants.OCCUPANCY_DETAILS, (ArrayList) templateDataService
				.getKeyRevenueAssumptionsOccupancy(projectDetails.get()).get("benchmarkCinemas"));
		map.put(Constants.COMPITION, (ArrayList) templateDataService
				.getCompitionMasterByExistingCinemaMaster(projectDetails.get()).get(Constants.COMPITION));
		map.put("projectDetails", (ArrayList) projectDetails.get().getCinemaFormat());
		map.put("admits", (ArrayList) ChangeCinemaFormat.changeOccupancyFormat(projectDetails.get().getOccupancy()));
		map.put("atp", (ArrayList) ChangeCinemaFormat.changeAtpFormat(projectDetails.get().getAtp(),projectDetails.get()));  
    	List<AssumptionsValues> list = ChangeCinemaFormat.changeSphFormat(projectDetails.get().getSph(),projectDetails.get());
    	list = list.stream().filter(p-> !p.getFormatType().equalsIgnoreCase("BLENDED SPH")).collect(Collectors.toList());
		map.put("sph", (ArrayList)list);
		return map;
	}

	@Override
	public Map<String, ? super Object> getPersonalExpense(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("personalExpenseBenchmark",
					projectAnalysisService.getPersonalExpenseBenchmarkCinemas(projectDetails.get()));
			map.put("personalExpense", projectDetails.get().getPersonalExpenses());
		} catch (Exception e) {
			log.error("error while fetching personal expense details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getAdRevenue(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("adrevenue", templateDataService.getKeyRevenueAssumptionsOccupancy(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getAdRevenue());
		} catch (Exception e) {
			log.error("error while fetching personal expense details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getElectricityExpense(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);
			map.put("electicityAndWater",
					projectAnalysisService.getEletricityDetailsBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getElectricityWaterBill());
		} catch (Exception e) {
			log.error("error while fetching eletricity and expense details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getRepairAndMaintaince(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);
			map.put("repairAndMaintaince",
					projectAnalysisService.getRepairAndMaintainceBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getTotalRMExpenses());
		} catch (Exception e) {
			log.error("error while fetching repair and maintaince details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getOccupancy(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("occupancyBenchmark",
					projectRevenueAnalysisService.getRevenueBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getOccupancy());
		} catch (Exception e) {
			log.error("error while fetching occupancy details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getAtpDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("atpBenchmark", projectRevenueAnalysisService.getRevenueBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getAtp());
		} catch (Exception e) {
			log.error("error while fetching atp  details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getSphDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("sphBenchmark", projectRevenueAnalysisService.getRevenueBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getAtp());
		} catch (Exception e) {
			log.error("error while fetching sph  details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getWebsaleDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("websaleBenchmark", templateDataService.getKeyRevenueAssumptionsOccupancy(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getWebSale());
		} catch (Exception e) {
			log.error("error while fetching web sale  details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getHousekeepingDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("housekeepingBenchmark",
					projectAnalysisService.getHousekeepingBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getHouseKeepingExpense());
		} catch (Exception e) {
			log.error("error while fetching house keeping expense  details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getMiscellaneousDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("miscellaneousBenchmark",
					projectAnalysisService.getMiscellaneousBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getMiscellaneousExpenses());
		} catch (Exception e) {
			log.error("error while fetching miscellaneous expense  details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getTravellingAndConyenceDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("travellingAndConyenceBenchmark",
					projectAnalysisService.getTravellingAndConveyanceBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getTravellingAndConveyanceExpense());
		} catch (Exception e) {
			log.error("error while fetching travelling and conyence details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getSecurityDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("securityBenchmark", projectAnalysisService.getSecurityBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getSecurityExpense());
		} catch (Exception e) {
			log.error("error while fetching security details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getLegalfeeDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("legalBenchmark", projectAnalysisService.getLegalFeeBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getLegalFee());
		} catch (Exception e) {
			log.error("error while fetching legal details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getCommunicationFeeDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("communicationBenchmark",
					projectAnalysisService.getCommunicationBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getCommunicationExpense());
		} catch (Exception e) {
			log.error("error while fetching communication details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getMarketingDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("marketingBenchmark", projectAnalysisService.getMarketingBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getMarketingExpense());
		} catch (Exception e) {
			log.error("error while fetching marketing details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getInternetDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("internetBenchmark", projectAnalysisService.getInternetFeeBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getInternetExpense());
		} catch (Exception e) {
			log.error("error while fetching internet details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getInsuranceDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("insuranceBenchmark", projectAnalysisService.getinsuranceBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getInsuranceExpense());
		} catch (Exception e) {
			log.error("error while fetching insurance details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getPrintingAndStationaryDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);

			map.put("printingAndStationaryBenchmark",
					projectAnalysisService.getPrintingStationaryBenchmarkCinemas(projectDetails.get()));
			map.put("projectDetails", projectDetails.get().getPrintingAndStationaryExpense());
		} catch (Exception e) {
			log.error("error while fetching printing and stationary details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getAnnualRentDetails(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);
			map.put("projectDetails", projectDetails.get().getRentTerm());
			map.put("lesserLesse", projectDetails.get().getLesserLesse());
		} catch (Exception e) {
			log.error("error while fetching annual rate details for view details..");
		}
		return map;
	}

	@Override
	public Map<String, ? super Object> getViewCalculation(String projectId) {
		Map<String, ? super Object> map = new HashMap<>();
		try {
			Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectId);
			IrrCalculationData irr = irrCalculationDao.findByProjectId(projectId);
			Integer leasePeriod = projectDetails.get().getLesserLesse().getLeasePeriod();
			List<String> leasePeriodList = new ArrayList<>();

			for (int i = 1; i <= leasePeriod; i++) {

				leasePeriodList.add("yr-"+i);
			}
			Map<Integer, Double> rentRevRatio = new HashMap<>();

			irr.getRentEachYear().forEach((k1, v1) -> {
				irr.getTotalRevenueForEachYear().forEach((k2, v2) -> {
					if (k1 == k2)
						rentRevRatio.put(k1, MathUtil.roundTwoDecimals((v1 / v2) * 100));
				});
			});
			
			Map<Integer, Double> rentCamRevenueRatioDetails = new HashMap<>();

			irr.getRentEachYear().forEach((k1, v1) -> {
				irr.getCamEachYear().forEach((k2, v2) -> {
					irr.getTotalRevenueForEachYear().forEach((k3, v3) -> {
						if (k1 == k2 && k1 == k3)
							rentCamRevenueRatioDetails.put(k1, MathUtil.roundTwoDecimals(((v1 + v2) / v3) * 100));
					});
				});
			});
			
			Map<Integer, Double> grossMarginForEachYear = new HashMap<>();
			irr.getGrossMarginForEachYear().forEach((k, v) -> {
				grossMarginForEachYear.put(k, v / 100000);
			});
			
			irr.setRentRationDetails(rentRevRatio);
			irr.setRentCamRevenueRatioDetails(rentCamRevenueRatioDetails);
			irr.setGrossMarginForEachYear(grossMarginForEachYear);
			map.put("summary", irr);
			map.put("leasePeriod", leasePeriodList);	
			} catch (Exception e) {
			log.error("error while fetching view calculation details..");
		}
		return map;
	}
}
