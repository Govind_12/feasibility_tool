package com.api.service.popup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface ViewModalService {

	Map<String, ? super ArrayList> getTotalAdmits(String projectId);
	Map<String, ? super Object> getPersonalExpense(String projectId);
	Map<String, ? super Object> getAdRevenue(String projectId);
	Map<String, ? super Object> getElectricityExpense(String projectId);
	Map<String, ? super Object> getRepairAndMaintaince(String projectId);
	Map<String, ? super Object> getOccupancy(String projectId);
	Map<String, ? super Object> getAtpDetails(String projectId);
	Map<String, ? super Object> getSphDetails(String projectId);
	Map<String, ? super Object> getWebsaleDetails(String projectId);
	Map<String, ? super Object> getHousekeepingDetails(String projectId);
	Map<String, ? super Object> getMiscellaneousDetails(String projectId);
	Map<String, ? super Object> getTravellingAndConyenceDetails(String projectId);
	Map<String, ? super Object> getSecurityDetails(String projectId);
	Map<String, ? super Object> getLegalfeeDetails(String projectId);
	Map<String, ? super Object> getCommunicationFeeDetails(String projectId);
	Map<String, ? super Object> getMarketingDetails(String projectId);
	Map<String, ? super Object> getInternetDetails(String projectId);
	Map<String, ? super Object> getInsuranceDetails(String projectId);
	Map<String, ? super Object> getPrintingAndStationaryDetails(String projectId);
	Map<String, ? super Object> getAnnualRentDetails(String projectId);
	Map<String, ? super Object> getViewCalculation(String projectId);
}
