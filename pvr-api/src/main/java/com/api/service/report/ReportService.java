package com.api.service.report;

import java.util.Optional;

import com.common.models.ProjectDetails;

public interface ReportService {

	ProjectDetails HtmlToPdfConversion(String projectId,Optional<ProjectDetails> projectDetails);
}