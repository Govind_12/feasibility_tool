package com.api.service.report;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.api.utils.Format;
import com.common.constants.Constants;
import com.common.models.CinemaMaster;
import com.common.models.CompitionMaster;
import com.common.models.ProjectDetails;
import com.common.models.PropertyDetail;

import de.pentabyte.googlemaps.StaticMap;
import de.pentabyte.googlemaps.StaticMap.Maptype;
import de.pentabyte.googlemaps.StaticMarker;
import de.pentabyte.googlemaps.StaticPath;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CreateImagesServiceImpl implements CreateImagesService {

	@Value("${google.api.key}")
	private String googleKey;

	@Value("${images.google.path}")
	private String imagesGooglePath;
	private static String API_URL = "https://maps.googleapis.com/maps/api/staticmap";

	@Autowired
	private TemplateDataService templateDataService;

	@Override
	public void createImagesDevelopmentDetails(ProjectDetails projectDetails, String pdfImagePath) {
		try {
			synchronized (this) {

				StaticMap googleMap = new StaticMap(512, 512, googleKey);

				String location = projectDetails.getProjectLocation().getLocality() + ","
						+ projectDetails.getProjectLocation().getCity();
				System.out.println(location);
				StaticMarker notreDame = new StaticMarker(projectDetails.getProjectLocation().getLatitude(),
						projectDetails.getProjectLocation().getLongitude());
				notreDame.setLabel('N');
				notreDame.setZoom(12);
				googleMap.setScale(3);
				notreDame.setColor("orange");
				googleMap.addMarker(notreDame);
				googleMap.setMaptype(Maptype.hybrid);
				log.info(googleMap.toString());
				getImages(googleMap.toString(), pdfImagePath + "/" + Constants.CATCHMENT_DETAILS);
			}

		} catch (Exception e) {
			log.error("error while fetching images for developement details");
		}
	}

	public BufferedImage getImages(String urlImages, String imagePath) throws IOException {
		BufferedImage image = null;

		URL url = new URL(urlImages);
		HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
		httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");
		image = ImageIO.read(url.openStream());
		File fileOutPut = new File(imagePath);

		ImageIO.write(image, "jpg", fileOutPut);

		return image;
	}

	public void createImagesExistingCinemaLandScape(ProjectDetails projectDetails, String pdfImagePath,
			Map<String, List<CinemaMaster>> cinemaMaster, Map<String, List<CompitionMaster>> compititionMaster) {
		try {

			List<CinemaMaster> cinemaLocation = new ArrayList<>();
			List<CompitionMaster> competitionList = new ArrayList<>();

			cinemaMaster.forEach((key, value) -> {
				value.forEach(cinema -> {
					cinemaLocation.add(cinema);
				});
			});
			compititionMaster.forEach((key, value) -> {
				value.forEach(cinema -> {
					competitionList.add(cinema);
				});
			});

			// StaticMarker m1 = null;
			Format format = Format.PNG;
			int width = 512, height = 512;
			String apiKey = googleKey;
			Maptype maptype = Maptype.roadmap;
			int scale = 3;
			// private Location center;
			Integer zoom = 12;
			List<StaticMarker> markers = new ArrayList<>();
			Character count = 65;
			for (CinemaMaster cinema : cinemaLocation) {
				StaticMarker m1 = new StaticMarker(cinema.getLatitude(), cinema.getLongitude());
				m1.setColor("green");
				m1.setLabel(count);
				m1.setZoom(15);
				markers.add(m1);
				count++;
			}
			Character count1 = 65;
			for (CompitionMaster cinema : competitionList) {
				StaticMarker m1 = new StaticMarker(cinema.getLatitude(), cinema.getLongitude());
				m1.setColor("red");
				m1.setLabel(count1);
				m1.setZoom(15);
				markers.add(m1);
				count1++;
			}

			List<StaticPath> paths;
			URIBuilder builder = new URIBuilder(API_URL);

			// Dimensionen
			builder.addParameter("size", width + "x" + height);
			if (zoom != null) {
				StaticMarker m1 = new StaticMarker(projectDetails.getProjectLocation().getLatitude(),
						projectDetails.getProjectLocation().getLongitude());
				builder.addParameter("center", m1.toString());
				m1.setColor("brown");
				builder.addParameter("markers", m1.toString());
				builder.addParameter("zoom", zoom.toString());
			}
			if (scale > 0)
				builder.addParameter("scale", String.valueOf(scale));
			if (maptype != null)
				builder.addParameter("maptype", maptype.name());

			if (format != null && Format.PNG != format)
				builder.addParameter("format", format.getValue());

			if (markers != null) {
				for (StaticMarker marker : markers) {
					builder.addParameter("markers", marker.toString());
				}
			}
			if (apiKey != null)
				builder.addParameter("key", apiKey);

			URL url = builder.build().toURL();
			log.info("EXISTING_CINEMA_LANDSACPE url : " + url.toString());

			getImages(url.toString(), pdfImagePath + "/" + Constants.EXISTING_CINEMA_LANDSACPE);

			System.out.println("");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching existing cinema landscape..");
		}

	}

	@Override
	public void createImagesUpcomingCinemaLandScape(ProjectDetails projectDetails, String projectId,
			String pdfImagePath) {
		try {

			List<PropertyDetail> pvrCinemaList = new ArrayList<>();
			List<PropertyDetail> competitionList = new ArrayList<>();

			projectDetails.getPvrProperties().forEach(a -> {
				pvrCinemaList.add(a);
			});

			projectDetails.getCompetitveProperties().forEach(a -> {
				competitionList.add(a);
			});

			// StaticMarker m1 = null;
			Format format = Format.PNG;
			int width = 512, height = 512;
			String apiKey = googleKey;
			Maptype maptype = Maptype.roadmap;
			int scale = 3;
			// private Location center;
			Integer zoom = 12;
			List<StaticMarker> markers = new ArrayList<>();
			Character count = 65;

			for (PropertyDetail cinema : pvrCinemaList) {
				Map<String, Double> latLong = templateDataService.getAddressByLatLong(cinema.getAddress());
				StaticMarker m1 = new StaticMarker(latLong.get(Constants.LATITUDE), latLong.get(Constants.LONGITUDE));
				m1.setColor("orange");
				m1.setLabel(count);
				markers.add(m1);
				count++;
			}
			Character count1 = 65;
			for (PropertyDetail cinema : competitionList) {
				Map<String, Double> latLong = templateDataService.getAddressByLatLong(cinema.getAddress());
				StaticMarker m1 = new StaticMarker(latLong.get(Constants.LATITUDE), latLong.get(Constants.LONGITUDE));
				m1.setColor("blue");
				m1.setLabel(count1);
				markers.add(m1);
				count1++;
			}

			List<StaticPath> paths;
			URIBuilder builder = new URIBuilder(API_URL);

			// Dimensionen
			builder.addParameter("size", width + "x" + height);

			if (zoom != null) {
				StaticMarker m1 = new StaticMarker(projectDetails.getProjectLocation().getLatitude(),
						projectDetails.getProjectLocation().getLongitude());
				builder.addParameter("center", m1.toString());
				m1.setColor("brown");
				builder.addParameter("markers", m1.toString());
				builder.addParameter("zoom", zoom.toString());
			}

			if (scale > 0)
				builder.addParameter("scale", String.valueOf(scale));
			if (maptype != null)
				builder.addParameter("maptype", maptype.name());

			if (format != null && Format.PNG != format)
				builder.addParameter("format", format.getValue());

			if (markers != null) {
				for (StaticMarker marker : markers) {
					builder.addParameter("markers", marker.toString());
				}
			}
			if (apiKey != null)
				builder.addParameter("key", apiKey);

			URL url = builder.build().toURL();
			log.info("UPCOMING_CINEMA_LANDSCAPE url : " + url.toString());

			getImages(url.toString(), pdfImagePath + "/" + Constants.UPCOMING_CINEMA_LANDSCAPE);

			System.out.println("");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching upcoming cinema landscape..");
		}

	}

	@Override
	public void createImagesCinemaLandScapeInNext5Years(ProjectDetails projectDetails, String pdfImagePath,
			Map<String, List<CinemaMaster>> cinemaMaster, Map<String, List<CompitionMaster>> compititionMaster) {
		try {

			List<CinemaMaster> cinemaLocation = new ArrayList<>();
			List<CompitionMaster> competitionList = new ArrayList<>();

			List<PropertyDetail> pvrCinemaList = new ArrayList<>();
			List<PropertyDetail> upcomingCompetitionList = new ArrayList<>();

			projectDetails.getPvrProperties().forEach(a -> {
				pvrCinemaList.add(a);
			});

			projectDetails.getCompetitveProperties().forEach(a -> {
				upcomingCompetitionList.add(a);
			});

			cinemaMaster.forEach((key, value) -> {
				value.forEach(cinema -> {
					cinemaLocation.add(cinema);
				});
			});
			compititionMaster.forEach((key, value) -> {
				value.forEach(cinema -> {
					competitionList.add(cinema);
				});
			});

			// StaticMarker m1 = null;
			Format format = Format.PNG;
			int width = 512, height = 512;
			String apiKey = googleKey;
			Maptype maptype = Maptype.roadmap;
			int scale = 3;
			// private Location center;
			Integer zoom = 12;
			List<StaticMarker> markers = new ArrayList<>();
			Character count = 65;
			for (CinemaMaster cinema : cinemaLocation) {
				StaticMarker m1 = new StaticMarker(cinema.getLatitude(), cinema.getLongitude());
				m1.setColor("green");
				// m1.setLabel(count);
				markers.add(m1);
				// count++;
			}
			Character count1 = 65;
			for (CompitionMaster cinema : competitionList) {
				StaticMarker m1 = new StaticMarker(cinema.getLatitude(), cinema.getLongitude());
				m1.setColor("red");
				// m1.setLabel(count1);
				markers.add(m1);
				// count1++;
			}
			for (PropertyDetail cinema : pvrCinemaList) {
				Map<String, Double> latLong = templateDataService.getAddressByLatLong(cinema.getAddress());
				StaticMarker m1 = new StaticMarker(latLong.get(Constants.LATITUDE), latLong.get(Constants.LONGITUDE));
				m1.setColor("orange");
				// m1.setLabel(count);
				markers.add(m1);
				// count++;
			}
			for (PropertyDetail cinema : upcomingCompetitionList) {
				Map<String, Double> latLong = templateDataService.getAddressByLatLong(cinema.getAddress());
				StaticMarker m1 = new StaticMarker(latLong.get(Constants.LATITUDE), latLong.get(Constants.LONGITUDE));
				m1.setColor("blue");
				// m1.setLabel(count1);
				markers.add(m1);
				// count1++;
			}

			List<StaticPath> paths;
			URIBuilder builder = new URIBuilder(API_URL);

			// Dimensionen
			builder.addParameter("size", width + "x" + height);
			if (zoom != null) {
				StaticMarker m1 = new StaticMarker(projectDetails.getProjectLocation().getLatitude(),
						projectDetails.getProjectLocation().getLongitude());
				builder.addParameter("center", m1.toString());
				m1.setColor("brown");
				builder.addParameter("markers", m1.toString());
				builder.addParameter("zoom", zoom.toString());
			}
			if (scale > 0)
				builder.addParameter("scale", String.valueOf(scale));
			if (maptype != null)
				builder.addParameter("maptype", maptype.name());

			if (format != null && Format.PNG != format)
				builder.addParameter("format", format.getValue());

			if (markers != null) {
				for (StaticMarker marker : markers) {
					builder.addParameter("markers", marker.toString());
				}
			}
			if (apiKey != null)
				builder.addParameter("key", apiKey);

			URL url = builder.build().toURL();
			log.info("CINEMA_LANDSCAPE_IN_NEXT_5_YEAR url : " + url.toString());

			getImages(url.toString(), pdfImagePath + "/" + Constants.CINEMA_LANDSCAPE_IN_NEXT_5_YEAR);

			System.out.println("");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching existing cinema landscape..");
		}
	}
}
