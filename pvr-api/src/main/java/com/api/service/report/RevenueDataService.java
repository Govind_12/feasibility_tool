package com.api.service.report;

public interface RevenueDataService {
	
Long getAllSeatCapacityByHopk(String hopk);
Long getAllAtpByHopk(Integer hopk);
Long getAllAdmitsByHopk(Integer hopk);
Double getAllOccupancyByHopk(Integer hopk);
Long getAllSphByHopk(Integer hopk);
Double getAllWebSaleByHopk(Integer hopk);
}
