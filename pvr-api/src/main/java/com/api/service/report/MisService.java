package com.api.service.report;

import java.util.List;

import com.common.mappers.MisMapper;

public interface MisService {

	List<MisMapper> getMisData();
	  
	List<MisMapper> getOpsOrExtMisData(String paramString);

}
