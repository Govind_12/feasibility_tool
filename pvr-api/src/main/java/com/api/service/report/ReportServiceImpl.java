package com.api.service.report;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.FileSystems;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.layout.SharedContext;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.api.dao.CinemaLandScapeDao;
import com.api.dao.CinemaMasterDao;
import com.api.dao.ProjectDetailsDao;
import com.api.dao.UserDao;
import com.api.services.PostSigningService;
import com.api.services.ProjectDetailsService;
import com.api.utils.ChangeCinemaFormat;
import com.api.utils.ReportUtil;
import com.api.utils.RoleUtil;
import com.common.constants.Constants;
import com.common.mappers.KeyOperatingCostAssumtionsMapper;
import com.common.mappers.QueryMapper;
import com.common.mappers.VpfIncomeMapper;
import com.common.models.AssumptionsValues;
import com.common.models.AtpAssumptions;
import com.common.models.CinemaMaster;
import com.common.models.CompitionMaster;
import com.common.models.ExistingCinemaLandScape;
import com.common.models.ProjectDetails;
import com.common.models.User;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPage;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSmartCopy;
import com.itextpdf.text.pdf.PdfWriter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ReportServiceImpl implements ReportService {

	@Autowired
	private TemplateEngine templateEngine;

	@Autowired
	private TemplateDataService templateDataService;

	@Autowired
	private ProjectDetailsDao projectDetailsDao;

	@Autowired
	private CinemaLandScapeDao cinemaMasterScapeDao;

	@Autowired
	private CinemaMasterDao cinemaMasterDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private PostSigningService postSigningService;

	private static final String UTF_8 = "UTF-8";

	@Value("${htmlinput.path}")
	private String htmlPath;

	@Value("${pdfoutput.path}")
	private String pdfPath;

	@Value("${google.api.key}")
	private String googleKey;

	@Value("${pdfImage.path}")
	private String pdfImagePath;

	@Value("${images.google.path}")
	private String imagesGooglePath;

	@Value("${pdfoutput.path}")
	private String pdfTrimPath;

	@Value("${images.google.path}")
	private String imagesTrimGooglePath;

	@Value("${reportsImagePath}")
	private String reportsImagePath;

	@Autowired
	private CreateImagesService createImageService;

	@Autowired
	private ExternalFileuploadService externalFileuploadService;
	@Autowired
	private ProjectDetailsService detailsService;

	@Override
	public ProjectDetails HtmlToPdfConversion(String projectId, Optional<ProjectDetails> upcomingProjectDetails) {

		String projectIds = projectId == null ? upcomingProjectDetails.get().getId() : projectId;
		StringBuilder stringBuilder = new StringBuilder();
		// String trimPath = pdfPath.substring(0, 29);
		String trimPath = pdfTrimPath;
		stringBuilder.append(trimPath);
		stringBuilder.append("-");
		stringBuilder.append(projectIds);

		File dir = new File(stringBuilder.toString());
		boolean exists = dir.exists();

		if (!exists) {
			dir.mkdir();
		} else {
			deleteFolder(dir);
			dir.mkdir();
		}

		pdfPath = dir.getAbsolutePath();

		StringBuilder stringBuilderimages = new StringBuilder();
		String trimPathimages = imagesTrimGooglePath;// imagesGooglePath.substring(0, 29);
		stringBuilderimages.append(trimPathimages);
		stringBuilderimages.append("-");
		stringBuilderimages.append("google");
		File imageFilePath = new File(stringBuilderimages.toString());
		boolean existsPath = imageFilePath.exists();
		if (existsPath) {
			imageFilePath.delete();
			imageFilePath.mkdir();
		} else {
			imageFilePath.mkdir();
		}
		imagesGooglePath = imageFilePath.getAbsolutePath();
		try {

			List<String> imagesGenerate = ReportUtil.getReportHtmlImages();
			imagesGenerate.parallelStream().forEach(action -> {

				Optional<ProjectDetails> projectDetails = null;

				if (!ObjectUtils.isEmpty(projectId)) {
					projectDetails = projectDetailsDao.findById(projectId);
				} else {
					projectDetails = upcomingProjectDetails;
				}

				if (action.equals(Constants.CATCHMENT_DETAILS)) {

					createImageService.createImagesDevelopmentDetails(projectDetails.get(),
							stringBuilderimages.toString());

				} else if (action.equals(Constants.EXISTING_CINEMA_LANDSACPE)) {

					createImageService.createImagesExistingCinemaLandScape(projectDetails.get(),
							stringBuilderimages.toString(),
							templateDataService.getExistingCinemaLandScape(projectDetails.get()),
							templateDataService.getCompitionMasterByProjectId(projectDetails.get()));

				} else if (action.equals(Constants.UPCOMING_CINEMA_LANDSCAPE)) {
					createImageService.createImagesUpcomingCinemaLandScape(projectDetails.get(), projectId,
							stringBuilderimages.toString());

				} else if (action.equals(Constants.CINEMA_LANDSCAPE_IN_NEXT_5_YEAR)) {
					createImageService.createImagesCinemaLandScapeInNext5Years(projectDetails.get(),
							stringBuilderimages.toString(),
							templateDataService.getExistingCinemaLandScape(projectDetails.get()),
							templateDataService.getCompitionMasterByProjectId(projectDetails.get()));
				}

			});

			List<String> htmlPathPdfGenerate = ReportUtil.getReportHtmlTemplate();
					
			for (String html : htmlPathPdfGenerate) {
				
				Optional<ProjectDetails> projectDetails = null;
				if (!ObjectUtils.isEmpty(projectId)) {
					projectDetails = projectDetailsDao.findById(projectId);
				} else {
					projectDetails = upcomingProjectDetails;
				}

				if (html.equals(Constants.HOME)) {

					try {
						generatePdf(Constants.HOME, Constants.HOME,
								templateDataService.getProjectDetails(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.INDEX)) {
					try {
						generatePdf(Constants.INDEX, Constants.INDEX,
								templateDataService.getProjectDetails(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.CATCHMENT_DETAILS)) {

					try {
						generateCatchmentDetails(Constants.CATCHMENT_DETAILS, Constants.CATCHMENT_DETAILS,
								templateDataService.getCatchmentDetailsId(projectDetails.get()), projectId,
								templateDataService.getProjectDetails(projectDetails.get()));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.DEVELOPMENT_DETAILS)) {

					try {
						generatePdfDevelopmentDetails(Constants.DEVELOPMENT_DETAILS, Constants.DEVELOPMENT_DETAILS,
								templateDataService.getProjectDetails(projectDetails.get()), projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.DEVELOPER_DETAILS)) {

					try {
						generatePdfDeveloperDetails(Constants.DEVELOPER_DETAILS, Constants.DEVELOPER_DETAILS,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.EXISTING_CINEMA_LANDSACPE)) {

					try {
						generatePdfExitinglanScape(Constants.EXISTING_CINEMA_LANDSACPE,
								Constants.EXISTING_CINEMA_LANDSACPE,
								templateDataService.getExistingCinemaLandScape(projectDetails.get()),
								templateDataService.getProjectDetails(projectDetails.get()),
								projectDetails.get().getId(),
								templateDataService.getCompitionMasterByProjectId(projectDetails.get()));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}

				else if (html.equals(Constants.UPCOMING_CINEMA_LANDSCAPE)) {

					try {
						generatePdfUpcomingCinemalandScape(Constants.UPCOMING_CINEMA_LANDSCAPE,
								Constants.UPCOMING_CINEMA_LANDSCAPE,
								templateDataService.getProjectDetails(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.CINEMA_LANDSCAPE_IN_NEXT_5_YEAR)) {

					try {
						generatePdfCinemalandScapeInNext5Years(Constants.CINEMA_LANDSCAPE_IN_NEXT_5_YEAR,
								Constants.CINEMA_LANDSCAPE_IN_NEXT_5_YEAR,
								templateDataService.getPvrFootprintrByProjectId(projectDetails.get()), projectId,
								templateDataService.getProjectDetails(projectDetails.get()));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.UPCOMING_CINEMA_PROPOSITION)) {

					externalFileuploadService.fileUploadProposition(projectDetails.get(), pdfPath);

				} else if (html.equals(Constants.CINEMA_SPECS)) {

					try {
						generatePdf(Constants.CINEMA_SPECS, Constants.CINEMA_SPECS,
								templateDataService.getCinemaSpecs(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.CINEMA_FORMAT_SEATDIMENSION)) {

					try {
						generatePdf(Constants.CINEMA_FORMAT_SEATDIMENSION, Constants.CINEMA_FORMAT_SEATDIMENSION,
								templateDataService.getCinemaSpecs(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_LEASE_TERMS)) {

					try {
						generatePdf(Constants.KEY_LEASE_TERMS, Constants.KEY_LEASE_TERMS,
								templateDataService.getProjectDetails(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} 
				else if (html.equals(Constants.KEY_LEASE_TERMS_FIXED)) {

					try {
						generatePdfFixedRent(Constants.KEY_LEASE_TERMS_FIXED, Constants.KEY_LEASE_TERMS_FIXED,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_LEASE_TERMS_RENT_REVENUE)) {

					try {
						generatePdfRentRevenue(Constants.KEY_LEASE_TERMS_RENT_REVENUE,
								Constants.KEY_LEASE_TERMS_RENT_REVENUE,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_LEASE_TERMS_RENT_MAX)) {

					try {
						generatePdfRentMax(Constants.KEY_LEASE_TERMS_RENT_MAX, Constants.KEY_LEASE_TERMS_RENT_MAX,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_LEASE_TERMS_RENT_OTHER_ONE_FIXED)) {

					try {
						generatePdfRentOtherOneFixed(Constants.KEY_LEASE_TERMS_RENT_OTHER_ONE_FIXED,
								Constants.KEY_LEASE_TERMS_RENT_OTHER_ONE_FIXED,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_LEASE_TERMS_RENT_OTHER_ONE_AVERAGE)) {

					try {
						generatePdfRentOtherOneAverage(Constants.KEY_LEASE_TERMS_RENT_OTHER_ONE_AVERAGE,
								Constants.KEY_LEASE_TERMS_RENT_OTHER_ONE_AVERAGE,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_LEASE_TERMS_RENT_OTHER_TWO)) {

					try {
						generatePdfRentOtherTwo(Constants.KEY_LEASE_TERMS_RENT_OTHER_TWO,
								Constants.KEY_LEASE_TERMS_RENT_OTHER_TWO,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_LEASE_TERMS_CAM_FIXED)) {

					try {
						generatePdfCamFixed(Constants.KEY_LEASE_TERMS_CAM_FIXED, Constants.KEY_LEASE_TERMS_CAM_FIXED,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					

				} else if (html.equals(Constants.KEY_LEASE_TERMS_CAM_ACTUALS)) {

					try {
						generatePdfCamActuals(Constants.KEY_LEASE_TERMS_CAM_ACTUALS,
								Constants.KEY_LEASE_TERMS_CAM_ACTUALS,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_LEASE_TERMS_CAM_INCLUDEDINRENT)) {

					try {
						generatePdfCamIncluded(Constants.KEY_LEASE_TERMS_CAM_INCLUDEDINRENT,
								Constants.KEY_LEASE_TERMS_CAM_INCLUDEDINRENT,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_LEASE_TERMS_SECURITY_DEPOSITE)) {

					try {
						generatePdf(Constants.KEY_LEASE_TERMS_SECURITY_DEPOSITE,
								Constants.KEY_LEASE_TERMS_SECURITY_DEPOSITE,
								templateDataService.getProjectDetails(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.DEVELOPER_SCOPE_OF_WORK)) {

					try {
						generatePdf(Constants.DEVELOPER_SCOPE_OF_WORK, Constants.DEVELOPER_SCOPE_OF_WORK,
								templateDataService.getProjectDetails(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.FEASIBIITY_SUMMARY_AND_PAYBACKS_ANALYSIS)) {

					try {
						generateFeasibilitySummaryPdf(Constants.FEASIBIITY_SUMMARY_AND_PAYBACKS_ANALYSIS,
								Constants.FEASIBIITY_SUMMARY_AND_PAYBACKS_ANALYSIS,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								templateDataService.getIrrCalulationById(projectDetails.get()), projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				}

				else if (html.equals(Constants.SUMMARY_PANDL)) {

					try {
						generateSummaryPandLPdf(Constants.SUMMARY_PANDL, Constants.SUMMARY_PANDL,
								templateDataService.getIrrCalulationById(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.COMMON_SIZE_PL_AND_L)) {

					try {
						generateSummaryPandLPdf(Constants.COMMON_SIZE_PL_AND_L, Constants.COMMON_SIZE_PL_AND_L,
								templateDataService.getIrrCalulationById(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				}

				else if (html.equals(Constants.KEY_REVENUE_ASSUMPTION_OCCUPANCY)) {

					try {
						generatePdfKeyRevenueAssumptionOccupancy(Constants.KEY_REVENUE_ASSUMPTION_OCCUPANCY,
								Constants.KEY_REVENUE_ASSUMPTION_OCCUPANCY,
								templateDataService.getKeyRevenueAssumptionsOccupancy(projectDetails.get()),
								templateDataService.getCompitionMasterByExistingCinemaMaster(projectDetails.get()),
								projectId, templateDataService.getProjectDetailsOccupancy(projectDetails.get()),
								templateDataService.getProjectDetails(projectDetails.get()));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_REVENUE_ASSUMPTION_ADMITS)) {

					try {
						generatePdfKeyRevenueAssumptionAdmits(Constants.KEY_REVENUE_ASSUMPTION_ADMITS,
								Constants.KEY_REVENUE_ASSUMPTION_ADMITS,
								templateDataService.getKeyRevenueAssumptionsOccupancy(projectDetails.get()),
								templateDataService.getCompitionMasterByExistingCinemaMaster(projectDetails.get()),
								projectId, templateDataService.getProjectDetailsOccupancy(projectDetails.get()),
								templateDataService.getProjectDetails(projectDetails.get()));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_REVENUE_ASSUMPTION_ATP)) {
					try {
						generatePdfKeyRevenueAssumptionAtp(Constants.KEY_REVENUE_ASSUMPTION_ATP,
								Constants.KEY_REVENUE_ASSUMPTION_ATP,
								templateDataService.getKeyRevenueAssumptionsOccupancy(projectDetails.get()),
								templateDataService.getCompitionMasterByExistingCinemaMaster(projectDetails.get()),
								projectId, templateDataService.getKeyRevenueAssumptionsAtp(projectDetails.get()),
								templateDataService.getProjectDetails(projectDetails.get()));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_REVENUE_ASSUMPTION_SPH)) {

					try {
						generatePdfKeyRevenueAssumptionSph(Constants.KEY_REVENUE_ASSUMPTION_SPH,
								Constants.KEY_REVENUE_ASSUMPTION_SPH,
								templateDataService.getKeyRevenueAssumptionsOccupancy(projectDetails.get()), projectId,
								templateDataService.getProjectDetailsSph(projectDetails.get()),
								templateDataService.getProjectDetails(projectDetails.get()));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_REVENUE_ASSUMPTION_AD_SALE_AND_OTHERS)) {

					try {
						generatePdfKeyRevenueAssumptionAdSale(Constants.KEY_REVENUE_ASSUMPTION_AD_SALE_AND_OTHERS,
								Constants.KEY_REVENUE_ASSUMPTION_AD_SALE_AND_OTHERS,
								templateDataService.getKeyRevenueAssumptionsOccupancy(projectDetails.get()), projectId,
								templateDataService.getProjectDetails(projectDetails.get()),
								templateDataService.getVpfInComeDetails(projectDetails.get()));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.KEY_OPERATING_COST_ASSUMPTION)) {

					try {
						generatePdfKeyOperatingAssumption(Constants.KEY_OPERATING_COST_ASSUMPTION,
								Constants.KEY_OPERATING_COST_ASSUMPTION,
								templateDataService.getProjectDetailsOperatingAssumtion(projectDetails.get()),
								projectId, templateDataService.getProjectDetails(projectDetails.get()));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();

					}

				} else if (html.equals(Constants.KEY_PROJECT_COST_ASSUMPTION)) {

					try {
						generateKeyProjectCostAssumptions(Constants.KEY_PROJECT_COST_ASSUMPTION,
								Constants.KEY_PROJECT_COST_ASSUMPTION,
								templateDataService.getKeyProjectCostAssumptionById(projectDetails.get().getId()),
								projectId, templateDataService.getProjectDetails(projectDetails.get()));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.COMMENTS_SECTION)) {

					try {
						Map<String, Object> operatingProjectQueryMap = new HashMap<>();
						if (!ObjectUtils.isEmpty(projectDetails.get().getOperatingProjectquery()) && 
								!ObjectUtils.isEmpty(projectDetails.get().getOperatingProjectquery().getQuery())) {
							
							String currentUser = RoleUtil.getCurrentUserID();
							
							List<QueryMapper> queryList = new ArrayList<>();
							String commentDate = "";
							for(QueryMapper queryLists : projectDetails.get().getOperatingProjectquery().getQuery()) {
								QueryMapper query = new QueryMapper();
								query.setMsg(queryLists.getMsg());
								commentDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(queryLists.getDate_time()));
								query.setDate(commentDate);
								query.setUserFullName(queryLists.getUserFullName());
								query.setUserId(queryLists.getUserId());
								query.setUserRole(currentUser);
								
								queryList.add(query);
							}
							operatingProjectQueryMap.put("operatingProjectQuery",queryList);
							
							generateCommentSection(Constants.COMMENTS_SECTION, Constants.COMMENTS_SECTION,
									operatingProjectQueryMap, projectId,
									templateDataService.getProjectDetails(projectDetails.get()));		
						}
					
						
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				}  else if (Constants.POST_SIGNING.equals(projectDetails.get().getFeasibilityState()) &&
						html.equals(Constants.POST_SIGNING_COST_COMPARISON)) {

					try {
						generatePostSigningCostComparison(Constants.POST_SIGNING_COST_COMPARISON, Constants.POST_SIGNING_COST_COMPARISON,
								templateDataService.getPostSigningCostComparison(projectDetails.get()),
                                projectId,templateDataService.getProjectDetails(projectDetails.get()));
						
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (Constants.PRE_HANDOVER.equals(projectDetails.get().getFeasibilityState()) &&
						html.equals(Constants.PRE_HANDOVER_COST_COMPARISON)) {

					try {
						generatePreHandoverCostComparison(Constants.PRE_HANDOVER_COST_COMPARISON, Constants.PRE_HANDOVER_COST_COMPARISON,
								templateDataService.getPreHandoverCostComparison(projectDetails.get()),
                                projectId,templateDataService.getProjectDetails(projectDetails.get()));
						
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				}else if (html.equals(Constants.KEY_ASSUMPTIONS)) {

					try {
						generatePdfkeyAssumption(Constants.KEY_ASSUMPTIONS, Constants.KEY_ASSUMPTIONS,
								templateDataService.getKeyAssumption(projectDetails.get()),
								templateDataService.getProjectDetails(projectDetails.get()), projectId);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.PROJECT_SYNOPSIS)) {

					try {
						generateProjectSynopsisPdf(Constants.PROJECT_SYNOPSIS, Constants.PROJECT_SYNOPSIS,
								templateDataService.getProjectDetails(projectDetails.get()), projectId,
								templateDataService.getIrrCalulationById(projectDetails.get()), projectDetails.get());
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}

				} else if (html.equals(Constants.APPENDIX_PROJECT_PRESENTATION)) {

					externalFileuploadService.fileUploadPropositionStep2(projectDetails.get(), pdfPath);

				} else if (html.equals(Constants.APPENDIX_DEVELOPER_CREDIT_RATING_REPORT)) {

					externalFileuploadService.fileUploadPropositionStep5(projectDetails.get(), pdfPath);
				} else if (html.equals(Constants.APPENDIX)) {

					generatePdfAppendix(Constants.APPENDIX, Constants.APPENDIX,
							templateDataService.getProjectDetails(projectDetails.get()), projectId,
							projectDetails.get());
				} else if (html.equals(Constants.StepOne)) {

					generatePdfStepOne(Constants.StepOne, Constants.StepOne,
							templateDataService.getProjectDetails(projectDetails.get()), projectId,
							projectDetails.get());
				} else if (html.equals(Constants.StepTwo)) {

					generatePdfStepTwo(Constants.StepTwo, Constants.StepTwo,
							templateDataService.getProjectDetails(projectDetails.get()), projectId,
							projectDetails.get());
				} else if (html.equals(Constants.StepFive)) {

					generatePdfStepFive(Constants.StepFive, Constants.StepFive,
							templateDataService.getProjectDetails(projectDetails.get()), projectId,
							projectDetails.get());
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while parsing report ...");
		}
		Optional<ProjectDetails> projectDetails = projectDetailsDao.findById(projectIds);
		try {
			synchronized (projectDetails) {
				mergePDF(pdfPath, stringBuilder + "/" + projectDetails.get().getProjectName() + ".pdf",
						projectDetails.get());
			}
			return projectDetails.get();
		} catch (DocumentException | IOException e) {
			log.error("error while MERGING PDF report ....", e);
		}
		return projectDetails.get();
	}

	private void generatePdfDeveloperDetails(String pdfTemplate, String htmlTemplateName,
			Map<String, ProjectDetails> map, String projectId, ProjectDetails projectDetails)
			throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;
		List<Integer> pvrProjects = projectDetails.getPvrProjectDelivered();
		List<CinemaMaster> pvrProjectsDeliver = new ArrayList<>();
		List<String> cinema = new ArrayList<>();

		if (!ObjectUtils.isEmpty(pvrProjects)) {
			if (pvrProjects.size() > 0) {
				for (Integer integer : pvrProjects) {
					pvrProjectsDeliver.add(cinemaMasterDao.findByHopk(integer));
				}
			}

			pvrProjectsDeliver.forEach(a -> {
				cinema.add(a.getCinema_name());
			});

		}

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		ctx.setVariable("pvrProjectsDeliver", cinema);

		ctx.setVariable("rowspan", cinema.size());

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);

			ITextRenderer renderer = new ITextRenderer();
			SharedContext sharedContext = renderer.getSharedContext();
			sharedContext.setPrint(true);
			sharedContext.setInteractive(false);
			sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
			sharedContext.getTextRenderer().setSmoothingThreshold(0);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();
			renderer.getSharedContext().setDPI(600);
			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}

	}

	public void generatePdf(String htmlTemplateName, String pdfTemplate, Map<String, ProjectDetails> map,
			String projectId) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);

			if (!ObjectUtils.isEmpty(value.getOccupancy())) {
				value.setOccupancy(ChangeCinemaFormat.changeOccupancyFormat(value.getOccupancy()));
				log.info(value.getSph().toString());
				value.setSph(ChangeCinemaFormat.changeSphFormat(value.getSph(),value));
				value.setAtp(ChangeCinemaFormat.changeAtpFormat(value.getAtp(), value));
			}
			ctx.setVariable("projectDetails", value);
			System.out.println(value.getReportSignature());
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);

			ITextRenderer renderer = new ITextRenderer();
			SharedContext sharedContext = renderer.getSharedContext();
			sharedContext.setPrint(true);
			sharedContext.setInteractive(false);
			sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
			sharedContext.getTextRenderer().setSmoothingThreshold(0);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();
			renderer.getSharedContext().setDPI(600);
			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfExitinglanScape(String htmlTemplateName, String pdfTemplate,
			Map<String, List<CinemaMaster>> map, Map<String, ProjectDetails> projectDetails, String projectId,
			Map<String, List<CompitionMaster>> compitionMaster) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);

		ExistingCinemaLandScape exist = cinemaMasterScapeDao.findByProjectId(projectId);
		List<CinemaMaster> cinemaMaster = map.get(Constants.EXISTING_CINEMA_LANDSACPE);
		List<CompitionMaster> compitionMasters = compitionMaster.get("compitionMaster");
		// System.out.println(map.get(Constants.EXISTING_CINEMA_LANDSACPE));
		if (ObjectUtils.isEmpty(exist)) {
			ExistingCinemaLandScape existingCinemaLandScape = new ExistingCinemaLandScape();
			existingCinemaLandScape.setProjectId(projectId);
			existingCinemaLandScape.setCinemaMaster(cinemaMaster);
			existingCinemaLandScape.setCompitionMaster(compitionMasters);
			cinemaMasterScapeDao.save(existingCinemaLandScape);
		} else {
			cinemaMasterScapeDao.delete(exist);
			ExistingCinemaLandScape existingCinemaLandScape = new ExistingCinemaLandScape();
			existingCinemaLandScape.setProjectId(projectId);
			existingCinemaLandScape.setCinemaMaster(cinemaMaster);
			existingCinemaLandScape.setCompitionMaster(compitionMasters);
			cinemaMasterScapeDao.save(existingCinemaLandScape);
		}

		map.forEach((key, value) -> {
			ctx.setVariable("cinemaLandScape", value);
		});

		projectDetails.forEach((key, value) -> {
			ctx.setVariable("icon", value);
		});

		compitionMaster.forEach((key, value) -> {
			ctx.setVariable("compitionMaster", value);
		});

		projectDetails.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			value.setGoogleImagePath(imagesTrimGooglePath + "-google");
			ctx.setVariable("projectDetail", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			System.out.println("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public static File createTempDirectory() throws IOException {
		final File temp;

		temp = File.createTempFile("temp", Long.toString(System.nanoTime()));

		if (!(temp.delete())) {
			throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
		}

		if (!(temp.mkdir())) {
			throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
		}

		return (temp);
	}

	public void generateProjectSynopsisPdf(String htmlTemplateName, String pdfTemplate, Map<String, ProjectDetails> map,
			String projectId, Map<String, Object> feasibilitySummary, ProjectDetails details)
			throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Optional<User> user = userDao.findById(details.getCreatedBy());
		User userDetails = user.get();

		Context ctx = new Context(Locale.FRANCE);
		ctx.setVariable("userDetails", userDetails);
		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		feasibilitySummary.forEach((key, value) -> {

			if (key.equals("feasibilitySummaryAndPaybackAnalysis")) {
				ctx.setVariable("irrCalcuation", value);
			} else if (key.equals("ebitda")) {
				ctx.setVariable("ebitda", value);
			} else if (key.equals("ebitdaMargin")) {
				ctx.setVariable("ebitdaMargin", value);
			} else if (key.equals("revenue")) {
				ctx.setVariable("revenue", value);
			} else if (key.equals("occupancy")) {
				ctx.setVariable("occupancy", value);
			} else if (key.equals("atp")) {
				ctx.setVariable("atp", value);
			} else if (key.equals("sph")) {
				ctx.setVariable("sph", value);
			} else if (key.equals("adSale")) {
				ctx.setVariable("adSale", value);
			} else if (key.equals("projectIrrPostTax")) {
				ctx.setVariable("projectIrrPostTax", value);
			} else if (key.equals("equityIrr")) {
				ctx.setVariable("equityIrr", value);
			} else if (key.equals("paybackPeriod")) {
				ctx.setVariable("paybackPeriod", value);
			} else if (key.equals("roce")) {
				ctx.setVariable("roce", value);
			} else if (key.equals("admits")) {
				ctx.setVariable("admits", value);
			} else if (key.equals("projectCost")) {
				ctx.setVariable("projectCost", value);
			} else if (key.equals("rent")) {
				ctx.setVariable("rent", value);
			} else if (key.equals("leasePeroid")) {
				ctx.setVariable("leasePeroid", value);
			} else if (key.equals("cam")) {
				ctx.setVariable("cam", value);
			} else if (key.equals("capexPerSeat")) {
				ctx.setVariable("capexPerSeat", value);
			} else if (key.equals("gla")) {
				ctx.setVariable("gla", value);
			} else if (key.equals("capexPerSqCarpetArea")) {
				ctx.setVariable("capexPerSqCarpetArea", value);
			} else if (key.equals("carpetAreaPerSeat")) {
				ctx.setVariable("carpetAreaPerSeat", value);
			} else if (key.equals("rentRevenueRation")) {
				ctx.setVariable("rentRevenueRation", value);
			} else if (key.equals("capexPerScreen")) {
				ctx.setVariable("capexPerScreen", value);
			}

		});
		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();
			renderer.getSharedContext().setDPI(600);
			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generateFeasibilitySummaryPdf(String htmlTemplateName, String pdfTemplate,
			Map<String, ProjectDetails> map, String projectId, Map<String, Object> feasibilitySummary,
			ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);
		Boolean showPrehandover = false;
		Boolean showPostsigning = false;
		Boolean hidePreTheader = false;
		Boolean hidePostTheader = false;
		ProjectDetails preHandoverDetails = new ProjectDetails();

		if (Constants.PRE_SIGNING.equals(projectDetails.getFeasibilityState())) {
			map.forEach((key, value) -> {
				ctx.setVariable("preSigningDetails", value);
			});
			generatePreSigningFeasibilitySummary(projectDetails, ctx);
			hidePreTheader = true;
			ctx.setVariable("hidePreTheader", hidePreTheader);
		}

		if (Constants.POST_SIGNING.equals(projectDetails.getFeasibilityState())) {
			ProjectDetails project = detailsService.getPreSigningProject(projectDetails.getReportIdVersion());
			if (!ObjectUtils.isEmpty(project)) {
				ctx.setVariable("preSigningDetails", project);
				generatePreSigningFeasibilitySummary(project, ctx);
			}
			ctx.setVariable("postSigningDetails", projectDetails);
			generatePostSigningFeasibilitySummary(projectDetails, ctx);
			showPostsigning = true;
			ctx.setVariable("showPostsigning", showPostsigning);
			ctx.setVariable("hidePreTheader", hidePreTheader);
			hidePostTheader = true;
			ctx.setVariable("hidePostTheader", hidePostTheader);
		}

		if (Constants.PRE_HANDOVER.equals(projectDetails.getFeasibilityState())) {
			ProjectDetails postSigning = postSigningService.getPostSigningProject(projectDetails.getReportIdVersion());
			if (!ObjectUtils.isEmpty(postSigning)) {
				ctx.setVariable("postSigningDetails", postSigning);
				generatePostSigningFeasibilitySummary(postSigning, ctx);
			}

			ProjectDetails project = detailsService.getPreSigningProject(projectDetails.getReportIdVersion());
			if (!ObjectUtils.isEmpty(project)) {
				ctx.setVariable("preSigningDetails", project);
				generatePreSigningFeasibilitySummary(project, ctx);
			}

			preHandoverDetails = projectDetails;
			ctx.setVariable("preHandoverDetails", preHandoverDetails);
			generatePreHandoverFeasibilitySummary(preHandoverDetails, ctx);
			showPostsigning = true;
			ctx.setVariable("showPostsigning", showPostsigning);
			showPrehandover = true;
			ctx.setVariable("showPrehandover", showPrehandover);
			ctx.setVariable("hidePreTheader", hidePreTheader);
			ctx.setVariable("hidePostTheader", hidePostTheader);
		}

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();
			renderer.getSharedContext().setDPI(600);
			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	private void generatePreHandoverFeasibilitySummary(ProjectDetails preHandoverDetails, Context ctx) {
		Map<String, Object> feasibilitySummary = templateDataService.getIrrCalulationById(preHandoverDetails);

		if (!ObjectUtils.isEmpty(feasibilitySummary)) {
			feasibilitySummary.forEach((key, value) -> {

				if (key.equals("feasibilitySummaryAndPaybackAnalysis")) {
					ctx.setVariable("irrCalcuation", value);
				} else if (key.equals("ebitda")) {
					ctx.setVariable("ebitda", value);
				} else if (key.equals("ebitdaMargin")) {
					ctx.setVariable("ebitdaMargin", value);
				} else if (key.equals("revenue")) {
					ctx.setVariable("revenue", value);
				} else if (key.equals("occupancy")) {
					ctx.setVariable("occupancy", value);
				} else if (key.equals("atp")) {
					ctx.setVariable("atp", value);
				} else if (key.equals("sph")) {
					ctx.setVariable("sph", value);
				} else if (key.equals("adSale")) {
					ctx.setVariable("adSale", value);
				} else if (key.equals("projectIrrPostTax")) {
					ctx.setVariable("projectIrrPostTax", value);
				} else if (key.equals("equityIrr")) {
					ctx.setVariable("equityIrr", value);
				} else if (key.equals("paybackPeriod")) {
					ctx.setVariable("paybackPeriod", value);
				} else if (key.equals("roce")) {
					ctx.setVariable("roce", value);
				} else if (key.equals("admits")) {
					ctx.setVariable("admits", value);
				} else if (key.equals("projectCost")) {
					ctx.setVariable("projectCost", value);
				} else if (key.equals("rent")) {
					ctx.setVariable("rent", value);
				} else if (key.equals("leasePeroid")) {
					ctx.setVariable("leasePeroid", value);
				} else if (key.equals("cam")) {
					ctx.setVariable("cam", value);
				} else if (key.equals("capexPerSeat")) {
					ctx.setVariable("capexPerSeat", value);
				} else if (key.equals("gla")) {
					ctx.setVariable("gla", value);
				} else if (key.equals("capexPerSqCarpetArea")) {
					ctx.setVariable("capexPerSqCarpetArea", value);
				} else if (key.equals("carpetAreaPerSeat")) {
					ctx.setVariable("carpetAreaPerSeat", value);
				} else if (key.equals("rentRevenueRation")) {
					ctx.setVariable("rentRevenueRation", value);
				}
			});
		}

	}

	private void generatePreSigningFeasibilitySummary(ProjectDetails project, Context ctx) {

		Map<String, Object> feasibilitySummary = templateDataService.getIrrCalulationById(project);

		if (!ObjectUtils.isEmpty(feasibilitySummary)) {
			feasibilitySummary.forEach((key, value) -> {

				if (key.equals("feasibilitySummaryAndPaybackAnalysis")) {
					ctx.setVariable("pre_irrCalcuation", value);
				} else if (key.equals("ebitda")) {
					ctx.setVariable("pre_ebitda", value);
				} else if (key.equals("ebitdaMargin")) {
					ctx.setVariable("pre_ebitdaMargin", value);
				} else if (key.equals("revenue")) {
					ctx.setVariable("pre_revenue", value);
				} else if (key.equals("occupancy")) {
					ctx.setVariable("pre_occupancy", value);
				} else if (key.equals("atp")) {
					ctx.setVariable("pre_atp", value);
				} else if (key.equals("sph")) {
					ctx.setVariable("pre_sph", value);
				} else if (key.equals("adSale")) {
					ctx.setVariable("pre_adSale", value);
				} else if (key.equals("projectIrrPostTax")) {
					ctx.setVariable("pre_projectIrrPostTax", value);
				} else if (key.equals("equityIrr")) {
					ctx.setVariable("pre_equityIrr", value);
				} else if (key.equals("paybackPeriod")) {
					ctx.setVariable("pre_paybackPeriod", value);
				} else if (key.equals("roce")) {
					ctx.setVariable("pre_roce", value);
				} else if (key.equals("admits")) {
					ctx.setVariable("pre_admits", value);
				} else if (key.equals("projectCost")) {
					ctx.setVariable("pre_projectCost", value);
				} else if (key.equals("rent")) {
					ctx.setVariable("pre_rent", value);
				} else if (key.equals("leasePeroid")) {
					ctx.setVariable("pre_leasePeroid", value);
				} else if (key.equals("cam")) {
					ctx.setVariable("pre_cam", value);
				} else if (key.equals("capexPerSeat")) {
					ctx.setVariable("pre_capexPerSeat", value);
				} else if (key.equals("gla")) {
					ctx.setVariable("pre_gla", value);
				} else if (key.equals("capexPerSqCarpetArea")) {
					ctx.setVariable("pre_capexPerSqCarpetArea", value);
				} else if (key.equals("carpetAreaPerSeat")) {
					ctx.setVariable("pre_carpetAreaPerSeat", value);
				} else if (key.equals("rentRevenueRation")) {
					ctx.setVariable("pre_rentRevenueRation", value);
				}
			});
		}

	}

	private Context generatePostSigningFeasibilitySummary(ProjectDetails project, Context ctx) {

		Map<String, Object> feasibilitySummary = templateDataService.getIrrCalulationById(project);

		if (!ObjectUtils.isEmpty(feasibilitySummary)) {
			feasibilitySummary.forEach((key, value) -> {

				if (key.equals("feasibilitySummaryAndPaybackAnalysis")) {
					ctx.setVariable("post_irrCalcuation", value);
				} else if (key.equals("ebitda")) {
					ctx.setVariable("post_ebitda", value);
				} else if (key.equals("ebitdaMargin")) {
					ctx.setVariable("post_ebitdaMargin", value);
				} else if (key.equals("revenue")) {
					ctx.setVariable("post_revenue", value);
				} else if (key.equals("occupancy")) {
					ctx.setVariable("post_occupancy", value);
				} else if (key.equals("atp")) {
					ctx.setVariable("post_atp", value);
				} else if (key.equals("sph")) {
					ctx.setVariable("post_sph", value);
				} else if (key.equals("adSale")) {
					ctx.setVariable("post_adSale", value);
				} else if (key.equals("projectIrrPostTax")) {
					ctx.setVariable("post_projectIrrPostTax", value);
				} else if (key.equals("equityIrr")) {
					ctx.setVariable("post_equityIrr", value);
				} else if (key.equals("paybackPeriod")) {
					ctx.setVariable("post_paybackPeriod", value);
				} else if (key.equals("roce")) {
					ctx.setVariable("post_roce", value);
				} else if (key.equals("admits")) {
					ctx.setVariable("post_admits", value);
				} else if (key.equals("projectCost")) {
					ctx.setVariable("post_projectCost", value);
				} else if (key.equals("rent")) {
					ctx.setVariable("post_rent", value);
				} else if (key.equals("leasePeroid")) {
					ctx.setVariable("post_leasePeroid", value);
				} else if (key.equals("cam")) {
					ctx.setVariable("post_cam", value);
				} else if (key.equals("capexPerSeat")) {
					ctx.setVariable("post_capexPerSeat", value);
				} else if (key.equals("gla")) {
					ctx.setVariable("post_gla", value);
				} else if (key.equals("capexPerSqCarpetArea")) {
					ctx.setVariable("post_capexPerSqCarpetArea", value);
				} else if (key.equals("carpetAreaPerSeat")) {
					ctx.setVariable("post_carpetAreaPerSeat", value);
				} else if (key.equals("rentRevenueRation")) {
					ctx.setVariable("post_rentRevenueRation", value);
				}
			});
		}

		return ctx;

	}

	public void generateSummaryPandLPdf(String htmlTemplateName, String pdfTemplate, Map<String, Object> map,
			String projectId) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
//			System.out.println(value);

			if (key.equals(Constants.FEASIBIITY_SUMMARY_AND_PAYBACKS_ANALYSIS))
				ctx.setVariable("pandlDetails", value);
		});
		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();
			renderer.getSharedContext().setDPI(600);
			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public static void mergePDF(String directory, String targetFile, ProjectDetails projectDetails)
			throws DocumentException, IOException {
		File dir = new File(directory);
		File[] filesToMerge = dir.listFiles(new FilenameFilter() {
			public boolean accept(File file, String fileName) {
				return fileName.endsWith(".pdf");
			}
		});
		Document document = new Document();
		FileOutputStream outputStream = new FileOutputStream(targetFile);
		PdfCopy copy = new PdfSmartCopy(document, outputStream);
		document.open();

		List<String> content = ReportUtil.getReportHtmlTemplate();
		LinkedList<String> linkedList = new LinkedList<String>();
		linkedList.add(0, Constants.HOME);
		linkedList.add(1, Constants.INDEX);
		linkedList.add(2, Constants.PROJECT_SYNOPSIS);
		linkedList.add(3, Constants.CATCHMENT_DETAILS);
		linkedList.add(4, Constants.EXISTING_CINEMA_LANDSACPE);
		linkedList.add(5, Constants.UPCOMING_CINEMA_LANDSCAPE);
		linkedList.add(6, Constants.CINEMA_LANDSCAPE_IN_NEXT_5_YEAR);
		linkedList.add(7, Constants.DEVELOPMENT_DETAILS);
		linkedList.add(8, Constants.DEVELOPER_DETAILS);
		linkedList.add(9, Constants.CINEMA_SPECS);
		linkedList.add(10, Constants.CINEMA_FORMAT_SEATDIMENSION);
		linkedList.add(11, Constants.SUMMARY_PANDL);
		linkedList.add(12, Constants.COMMON_SIZE_PL_AND_L);
		linkedList.add(13, Constants.KEY_LEASE_TERMS);
		linkedList.add(14, Constants.KEY_LEASE_TERMS_FIXED);
		linkedList.add(15, Constants.KEY_LEASE_TERMS_RENT_REVENUE);
		linkedList.add(16, Constants.KEY_LEASE_TERMS_RENT_MAX);
		linkedList.add(17, Constants.KEY_LEASE_TERMS_RENT_OTHER_ONE_FIXED);
		linkedList.add(18, Constants.KEY_LEASE_TERMS_RENT_OTHER_ONE_AVERAGE);
		linkedList.add(19, Constants.KEY_LEASE_TERMS_RENT_OTHER_TWO);
		linkedList.add(20, Constants.KEY_LEASE_TERMS_CAM_FIXED);
		linkedList.add(21, Constants.KEY_LEASE_TERMS_CAM_ACTUALS);
		linkedList.add(22, Constants.KEY_LEASE_TERMS_CAM_INCLUDEDINRENT);
		linkedList.add(23, Constants.KEY_LEASE_TERMS_SECURITY_DEPOSITE);
		linkedList.add(24, Constants.FEASIBIITY_SUMMARY_AND_PAYBACKS_ANALYSIS);
		linkedList.add(25, Constants.KEY_REVENUE_ASSUMPTION_OCCUPANCY);
		linkedList.add(26, Constants.KEY_REVENUE_ASSUMPTION_ADMITS);
		linkedList.add(27, Constants.KEY_REVENUE_ASSUMPTION_ATP);
		linkedList.add(28, Constants.KEY_REVENUE_ASSUMPTION_SPH);
		linkedList.add(29, Constants.KEY_REVENUE_ASSUMPTION_AD_SALE_AND_OTHERS);
		linkedList.add(30, Constants.KEY_OPERATING_COST_ASSUMPTION);
		linkedList.add(31, Constants.KEY_PROJECT_COST_ASSUMPTION);
		linkedList.add(32, Constants.COMMENTS_SECTION);
		
		int pageNo = 33;
		if (Constants.POST_SIGNING.equals(projectDetails.getFeasibilityState())) {
			linkedList.add(pageNo, Constants.POST_SIGNING_COST_COMPARISON);
			pageNo++;
		}
		
		if (Constants.PRE_HANDOVER.equals(projectDetails.getFeasibilityState())) {
			linkedList.add(pageNo, Constants.PRE_HANDOVER_COST_COMPARISON);
			pageNo++;
		}
		
		linkedList.add(pageNo, Constants.KEY_ASSUMPTIONS);
		
		pageNo++;
		linkedList.add(pageNo, Constants.DEVELOPER_SCOPE_OF_WORK);
		
		pageNo++;
		linkedList.add(pageNo, Constants.APPENDIX); 
		
		pageNo++;
		linkedList.add(pageNo, Constants.StepOne);
		
		pageNo++;
		if (!ObjectUtils.isEmpty(projectDetails.getFileURLStep1())) {
			linkedList.add(pageNo, projectDetails.getFileURLStep1());
		} else {
			linkedList.add(pageNo, "");
		}
		
		pageNo++;
		linkedList.add(pageNo, Constants.StepTwo);
		
		pageNo++;
		if (!ObjectUtils.isEmpty(projectDetails.getFileURLStep2())) {
			linkedList.add(pageNo, projectDetails.getFileURLStep2());
		} else {
			linkedList.add(pageNo, "");
		}
		
		pageNo++;
		linkedList.add(pageNo, Constants.StepFive);
		
		pageNo++;
		if (!ObjectUtils.isEmpty(projectDetails.getFileURLStep5())) {
			linkedList.add(pageNo, projectDetails.getFileURLStep5());
		} else {
			linkedList.add(pageNo, "");
		}

		for (String html : linkedList) {

			for (File inFile : filesToMerge) {

				if (html.equals(projectDetails.getFileURLStep1())
						&& inFile.getName().equals(projectDetails.getFileURLStep1())) {
					PdfReader reader = new PdfReader(inFile.getCanonicalPath());
					copy.addDocument(reader);
					reader.close();
				} else if (html.equals(projectDetails.getFileURLStep2())
						&& inFile.getName().equals(projectDetails.getFileURLStep2())) {
					PdfReader reader = new PdfReader(inFile.getCanonicalPath());
					copy.addDocument(reader);
					reader.close();
				} else if (html.equals(projectDetails.getFileURLStep5())
						&& inFile.getName().equals(projectDetails.getFileURLStep5())) {
					PdfReader reader = new PdfReader(inFile.getCanonicalPath());
					copy.addDocument(reader);
					reader.close();
				} else {
					String str = inFile.getName().replaceAll("\\d", "");
					if (str.equalsIgnoreCase(html.replaceAll("\\d", "") + ".pdf")) {
						PdfReader reader = new PdfReader(inFile.getCanonicalPath());
						copy.addDocument(reader);
						reader.close();
					}
				}
			}
		}
		document.close();
	}

	public void generateCatchmentDetails(String htmlTemplateName, String pdfTemplate, Map<String, Object> map,
			String projectId, Map<String, ProjectDetails> projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);
		map.forEach((key, value) -> {
			if (key.equals("projectDetails")) {
				ctx.setVariable("projectDetails", value);
			} else if (key.equals("catchmentDetails")) {
				ctx.setVariable("catchmentDetails", value);
			}

		});
		projectDetails.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			value.setGoogleImagePath(imagesTrimGooglePath + "-google");
			ctx.setVariable("projectDetail", value);
		});
		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generateKeyProjectCostAssumptions(String htmlTemplateName, String pdfTemplate, Map<String, Object> map,
			String projectId, Map<String, ProjectDetails> project) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
//			System.out.println(value);
			ctx.setVariable("projectCostSummary", value);
		});
		project.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});
		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfUpcomingCinemalandScape(String htmlTemplateName, String pdfTemplate,
			Map<String, ProjectDetails> projectDetails, String projectId) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);

		projectDetails.forEach((key, value) -> {
//			System.out.println(value);
			value.setReportsImagePath(reportsImagePath);
			value.setGoogleImagePath(imagesTrimGooglePath + "-google");
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			System.out.println("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfCinemalandScapeInNext5Years(String htmlTemplateName, String pdfTemplate,
			Map<String, List<Long>> upcomingProjectDetails, String projectId, Map<String, ProjectDetails> map)
			throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);

		upcomingProjectDetails.forEach((key, value) -> {

			if (key.equals(Constants.NO_OF_CINEMA)) {
				ctx.setVariable(Constants.NO_OF_CINEMA, value);
			} else if (key.equals(Constants.NO_OF_SCREEN)) {
				ctx.setVariable(Constants.NO_OF_SCREEN, value);
			} else if (key.equals(Constants.NO_OF_SEATS)) {
				ctx.setVariable(Constants.NO_OF_SEATS, value);
			} else if (key.equals(Constants.NO_OF_SEATS_COMPITION)) {
				ctx.setVariable(Constants.NO_OF_SEATS_COMPITION, value);
			} else if (key.equals(Constants.NO_OF_SCREEN_COMPITION)) {
				ctx.setVariable(Constants.NO_OF_SCREEN_COMPITION, value);
			} else if (key.equals(Constants.NO_OF_CINEMA_COMPITION)) {
				ctx.setVariable(Constants.NO_OF_CINEMA_COMPITION, value);
			} else if (key.equals(Constants.NO_OF_CINEMA_MARKET)) {
				ctx.setVariable(Constants.NO_OF_CINEMA_MARKET, value);
			} else if (key.equals(Constants.NO_OF_SCREEN_MARKET)) {
				ctx.setVariable(Constants.NO_OF_SCREEN_MARKET, value);
			} else if (key.equals(Constants.NO_OF_SEATS_MARKET)) {
				ctx.setVariable(Constants.NO_OF_SEATS_MARKET, value);
			}
		});

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			value.setGoogleImagePath(imagesTrimGooglePath + "-google");
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			System.out.println("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfKeyRevenueAssumptionOccupancy(String htmlTemplateName, String pdfTemplate,
			Map<String, List<CinemaMaster>> projectDetails, Map<String, List<CompitionMaster>> mapCompitionMaster,
			String projectId, Map<String, List<AssumptionsValues>> projectDetailsResources,
			Map<String, ProjectDetails> map) throws UnsupportedEncodingException {

		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);

		projectDetails.forEach((key, value) -> {
			ctx.setVariable("cinemaMaster", value);
		});

		mapCompitionMaster.forEach((key, value) -> {
			ctx.setVariable("compitionMaster", value);
		});

		projectDetailsResources.forEach((key, value) -> {

			ctx.setVariable("projectDetails", value);
		});

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetail", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			System.out.println("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfKeyRevenueAssumptionAdmits(String htmlTemplateName, String pdfTemplate,
			Map<String, List<CinemaMaster>> projectDetails, Map<String, List<CompitionMaster>> mapCompitionMaster,
			String projectId, Map<String, List<AssumptionsValues>> projectDetailsResources,
			Map<String, ProjectDetails> map) throws UnsupportedEncodingException {

		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);

		projectDetails.forEach((key, value) -> {
			ctx.setVariable("cinemaMaster", value);
		});

		mapCompitionMaster.forEach((key, value) -> {
			ctx.setVariable("compitionMaster", value);
		});

		projectDetailsResources.forEach((key, value) -> {
			ctx.setVariable("projectDetails", value);
		});
		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetail", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			System.out.println("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	private String convertToXhtml(String html) throws UnsupportedEncodingException {
		Tidy tidy = new Tidy();
		tidy.setInputEncoding(UTF_8);
		tidy.setOutputEncoding(UTF_8);
		tidy.setXHTML(true);
		ByteArrayInputStream inputStream = new ByteArrayInputStream(html.getBytes(UTF_8));
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		tidy.parseDOM(inputStream, outputStream);
		return outputStream.toString(UTF_8);
	}

	public BufferedImage getImages(String urlImages, String imagePath) throws IOException {
		BufferedImage image = null;

		URL url = new URL(urlImages);
		HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
		httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");
		image = ImageIO.read(url.openStream());
		File fileOutPut = new File(imagePath);
		ImageIO.write(image, "png", fileOutPut);

		return image;
	}

	String executePost(String targetURL, String urlParameters) {

		URL url;
		HttpURLConnection connection = null;
		try {
			// Create connection

			url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();

			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11");

			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// send Request
			DataOutputStream dataout = new DataOutputStream(connection.getOutputStream());
			dataout.writeBytes(pdfPath);
			dataout.flush();
			dataout.close();

			// get response
			InputStream is = connection.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();

			while ((line = br.readLine()) != null) {
				response.append(line);
				response.append('\n');

			}
			System.out.println(response.toString());
			br.close();
			return response.toString();
		} catch (Exception e) {
			System.out.println("Unable to full create connection");
			e.printStackTrace();
			return null;
		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}

	}

	public class RotateEvent extends PdfPageEventHelper {
		public void onStartPage(PdfWriter writer, Document document) {
			writer.addPageDictEntry(PdfName.ROTATE, PdfPage.LANDSCAPE);
		}
	}

	public void generatePdfKeyRevenueAssumptionAtp(String htmlTemplateName, String pdfTemplate,
			Map<String, List<CinemaMaster>> projectDetails, Map<String, List<CompitionMaster>> mapCompitionMaster,
			String projectId, Map<String, List<AtpAssumptions>> projectDetailsResources,
			Map<String, ProjectDetails> map) throws UnsupportedEncodingException {

		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);

		projectDetails.forEach((key, value) -> {
			ctx.setVariable("cinemaMaster", value);
		});

		mapCompitionMaster.forEach((key, value) -> {
			ctx.setVariable("compitionMaster", value);
		});

		projectDetailsResources.forEach((key, value) -> {
			ctx.setVariable("atpAssumptions", value);
		});

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetail", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			System.out.println("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfKeyRevenueAssumptionSph(String htmlTemplateName, String pdfTemplate,
			Map<String, List<CinemaMaster>> sph, String projectId,
			Map<String, List<AssumptionsValues>> sphProjectDetails, Map<String, ProjectDetails> map)
			throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);

		sph.forEach((key, value) -> {
			ctx.setVariable("cinemaMaster", value);
		});

		sphProjectDetails.forEach((key, value) -> {
			ctx.setVariable("projectDetails", value);
		});

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetail", value);
		});
		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			System.out.println("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfKeyOperatingAssumption(String htmlTemplateName, String pdfTemplate,
			Map<String, List<KeyOperatingCostAssumtionsMapper>> operatingAssumption, String projectId,
			Map<String, ProjectDetails> map) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);

		operatingAssumption.forEach((key, value) -> {
			ctx.setVariable("operatingAssumption", value);
		});

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			System.out.println("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfKeyRevenueAssumptionAdSale(String htmlTemplateName, String pdfTemplate,
			Map<String, List<CinemaMaster>> operatingAssumption, String projectId,
			Map<String, ProjectDetails> projectDetails, Map<String, VpfIncomeMapper> vpfIncome)
			throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;
		Context ctx = new Context(Locale.FRANCE);

		operatingAssumption.forEach((key, value) -> {
			ctx.setVariable("adSaleAssumption", value);
		});

		projectDetails.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		vpfIncome.forEach((key, value) -> {
			ctx.setVariable("vpfIncome", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {

			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			System.out.println("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfDevelopmentDetails(String htmlTemplateName, String pdfTemplate,
			Map<String, ProjectDetails> map, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});
		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);

			ITextRenderer renderer = new ITextRenderer();

			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();
			renderer.getSharedContext().setDPI(600);
			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfkeyAssumption(String htmlTemplateName, String pdfTemplate,
			Map<String, List<Double>> revenueAssumption, Map<String, ProjectDetails> map, String projectId)
			throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		revenueAssumption.forEach((key, value) -> {
			if (key.equals(Constants.ADMIT_GROWTH)) {
				ctx.setVariable(Constants.ADMIT_GROWTH, value);
			} else if (key.equals(Constants.ATP_GROWTH)) {
				ctx.setVariable(Constants.ATP_GROWTH, value);
			} else if (key.equals(Constants.SPH_GROWTH)) {
				ctx.setVariable(Constants.SPH_GROWTH, value);
			} else if (key.equals(Constants.SPONSORSHIP_GROWTH)) {
				ctx.setVariable(Constants.SPONSORSHIP_GROWTH, value);
			} else if (key.equals(Constants.CONVENIENCE_GROWTH)) {
				ctx.setVariable(Constants.CONVENIENCE_GROWTH, value);
			} else if (key.equals(Constants.OTHER_INCOME_GROWTH)) {
				ctx.setVariable(Constants.OTHER_INCOME_GROWTH, value);
			} else if (key.equals(Constants.RENT_EXPENSE)) {
				ctx.setVariable(Constants.RENT_EXPENSE, value);
			} else if (key.equals(Constants.COMMON_AREA_MAINTAINCE)) {
				ctx.setVariable(Constants.COMMON_AREA_MAINTAINCE, value);
			} else if (key.equals(Constants.PROPERTY_TAX)) {
				ctx.setVariable(Constants.PROPERTY_TAX, value);
			} else if (key.equals(Constants.MAINTENCE_CAPEX_INITIAL)) {
				ctx.setVariable(Constants.MAINTENCE_CAPEX_INITIAL, value);
			} else if (key.equals(Constants.PERSONNAL_COST)) {
				ctx.setVariable(Constants.PERSONNAL_COST, value);
			} else if (key.equals(Constants.WATERANDELECTRICITY)) {
				ctx.setVariable(Constants.WATERANDELECTRICITY, value);
			} else if (key.equals(Constants.REPAIRANDMAINTANCE)) {
				ctx.setVariable(Constants.REPAIRANDMAINTANCE, value);
			} else if (key.equals(Constants.OTHER_OVERHEAD_EXPENSE)) {
				ctx.setVariable(Constants.OTHER_OVERHEAD_EXPENSE, value);
			} else if (key.equals(Constants.F_B_COGS)) {
				ctx.setVariable(Constants.F_B_COGS, value);
			} else if (key.equals(Constants.F_H_C)) {
				ctx.setVariable(Constants.F_H_C, value);
			} else if (key.equals("leasePeriod")) {
				ctx.setVariable("leasePeriod", value);
			}
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			log.info(pdfPath + "");

			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);

			ITextRenderer renderer = new ITextRenderer();

			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();
			renderer.getSharedContext().setDPI(600);
			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public static void deleteFolder(File folder) {
		File[] files = folder.listFiles();
		if (files != null) {
			for (File f : files) {
				if (f.isDirectory()) {
					deleteFolder(f);
				} else {
					f.delete();
				}
			}
		}
		folder.delete();
	}

	public void generatePdfFixed(String htmlTemplateName, String pdfTemplate, Map<String, ProjectDetails> map,
			String projectId) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);

			ITextRenderer renderer = new ITextRenderer();
			SharedContext sharedContext = renderer.getSharedContext();
			sharedContext.setPrint(true);
			sharedContext.setInteractive(false);
			sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
			sharedContext.getTextRenderer().setSmoothingThreshold(0);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();
			renderer.getSharedContext().setDPI(600);
			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfRentOtherOneFixed(String pdfTemplate, String htmlTemplateName,
			Map<String, ProjectDetails> map, String projectId, ProjectDetails projectDetails)
			throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			// log.info(projectDetails.getRentTerm().getPaymentTerm().toString());
			if (projectDetails.getLesserLesse() != null && projectDetails.getLesserLesse().getRentPaymentTerm() != null
					&& projectDetails.getLesserLesse().getRentPaymentTerm().equalsIgnoreCase(Constants.OTHER_1)
					&& projectDetails.getRentTerm().getPaymentTerm().equalsIgnoreCase(Constants.FIXED)) {
				log.info(pdfPath + "");
				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfRentOtherTwo(String htmlTemplateName, String pdfTemplate, Map<String, ProjectDetails> map,
			String projectId) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);

			ITextRenderer renderer = new ITextRenderer();
			SharedContext sharedContext = renderer.getSharedContext();
			sharedContext.setPrint(true);
			sharedContext.setInteractive(false);
			sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
			sharedContext.getTextRenderer().setSmoothingThreshold(0);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();
			renderer.getSharedContext().setDPI(600);
			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfRentOtherOneAverage(String pdfTemplate, String htmlTemplateName,
			Map<String, ProjectDetails> map, String projectId, ProjectDetails projectDetails)
			throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			if (projectDetails.getLesserLesse() != null && projectDetails.getLesserLesse().getRentPaymentTerm() != null
					&& projectDetails.getLesserLesse().getRentPaymentTerm().equalsIgnoreCase(Constants.OTHER_1)
					&& projectDetails.getRentTerm().getPaymentTerm().equalsIgnoreCase(Constants.AVERAGE)) {
				log.info(pdfPath + "");
				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfRentOtherTwo(String pdfTemplate, String htmlTemplateName, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			if (projectDetails.getLesserLesse() != null && projectDetails.getLesserLesse().getRentPaymentTerm() != null
					&& projectDetails.getLesserLesse().getRentPaymentTerm().equalsIgnoreCase(Constants.OTHER_2)) {
				log.info(pdfPath + "");
				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfRentRevenue(String htmlTemplateName, String pdfTemplate, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			if (projectDetails.getLesserLesse() != null && projectDetails.getLesserLesse().getRentPaymentTerm() != null
					&& projectDetails.getLesserLesse().getRentPaymentTerm().equalsIgnoreCase(Constants.REVENUE_SHARE)) {
				log.info(pdfPath + "");
				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	private void generatePdfFixedRent(String htmlTemplateName, String pdfTemplate, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {

			if (projectDetails.getLesserLesse() != null && projectDetails.getLesserLesse().getRentPaymentTerm() != null
					&& projectDetails.getLesserLesse().getRentPaymentTerm().equalsIgnoreCase(Constants.FIXED)) {
				log.info(pdfPath + "");
				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}

	}

	private void generatePdfRentMax(String pdfTemplate, String htmlTemplateName, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {

			if (projectDetails.getLesserLesse() != null && projectDetails.getLesserLesse().getRentPaymentTerm() != null
					&& projectDetails.getLesserLesse().getRentPaymentTerm().equalsIgnoreCase(Constants.MAX)) {
				log.info(pdfPath + "");
				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}

	}

	private void generatePdfCamFixed(String pdfTemplate, String htmlTemplateName, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {

			if (projectDetails.getCamTerm() != null && projectDetails.getCamTerm().getCamPaymentTerm() != null
					&& projectDetails.getCamTerm().getCamPaymentTerm().equalsIgnoreCase(Constants.FIXED)) {
				log.info(pdfPath + "");
				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}

	}

	private void generatePdfCamIncluded(String pdfTemplate, String htmlTemplateName, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {

			if (projectDetails.getCamTerm() != null && projectDetails.getCamTerm().getCamPaymentTerm() != null
					&& projectDetails.getCamTerm().getCamPaymentTerm().equalsIgnoreCase(Constants.INCLUDED)) {
				log.info(pdfPath + "");
				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}

	}

	private void generatePdfCamActuals(String pdfTemplate, String htmlTemplateName, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {

			if (projectDetails.getCamTerm() != null && projectDetails.getCamTerm().getCamPaymentTerm() != null
					&& projectDetails.getCamTerm().getCamPaymentTerm().equalsIgnoreCase(Constants.ACTUALS)) {
				log.info(pdfPath + "");
				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}

	}

	public void generatePdfAppendix(String htmlTemplateName, String pdfTemplate, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			log.info(pdfPath + "");

			if (!ObjectUtils.isEmpty(projectDetails.getFileURLStep1())
					|| !ObjectUtils.isEmpty(projectDetails.getFileURLStep2())
					|| !ObjectUtils.isEmpty(projectDetails.getFileURLStep5())) {

				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfStepOne(String htmlTemplateName, String pdfTemplate, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			log.info(pdfPath + "");

			if (!ObjectUtils.isEmpty(projectDetails.getFileURLStep1())) {

				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfStepTwo(String htmlTemplateName, String pdfTemplate, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			log.info(pdfPath + "");

			if (!ObjectUtils.isEmpty(projectDetails.getFileURLStep2())) {
				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}

	public void generatePdfStepFive(String htmlTemplateName, String pdfTemplate, Map<String, ProjectDetails> map,
			String projectId, ProjectDetails projectDetails) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});

		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);

		FileOutputStream os = null;

		try {
			log.info(pdfPath + "");

			if (!ObjectUtils.isEmpty(projectDetails.getFileURLStep5())) {

				File file = new File(pdfPath);
				final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
				os = new FileOutputStream(outputFile);

				ITextRenderer renderer = new ITextRenderer();
				SharedContext sharedContext = renderer.getSharedContext();
				sharedContext.setPrint(true);
				sharedContext.setInteractive(false);
				sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
				sharedContext.getTextRenderer().setSmoothingThreshold(0);
				String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL()
						.toString();

				renderer.setDocumentFromString(xHtml, baseUrl);
				renderer.layout();
				renderer.createPDF(os);
				renderer.finishPDF();
				renderer.getSharedContext().setDPI(600);
				log.info("PDF created successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}
	
	public void generateCommentSection(String htmlTemplateName, String pdfTemplate, Map<String, Object> map,
			String projectId, Map<String, ProjectDetails> project) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);

		map.forEach((key, value) -> {
//			System.out.println(value);
			ctx.setVariable("commentSection", value);
		});
		project.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});
		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}
	
	public void generatePostSigningCostComparison(String htmlTemplateName, String pdfTemplate, Map<String, Object> map,
			String projectId, Map<String, ProjectDetails> project) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);
		ctx.setVariable("postSigningCost", map);

		project.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});
		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}
	
	public void generatePreHandoverCostComparison(String htmlTemplateName, String pdfTemplate, Map<String, Object> map,
			String projectId, Map<String, ProjectDetails> project) throws UnsupportedEncodingException {
		String ouputPdf = pdfTemplate;

		Context ctx = new Context(Locale.FRANCE);		
		ctx.setVariable("preHandoverCost", map);
		
		project.forEach((key, value) -> {
			value.setReportsImagePath(reportsImagePath);
			ctx.setVariable("projectDetails", value);
		});
		String processedHtml = templateEngine.process(htmlTemplateName, ctx);
		String xHtml = convertToXhtml(processedHtml);
		FileOutputStream os = null;
		try {
			log.info(pdfPath + "");
			File file = new File(pdfPath);
			final File outputFile = File.createTempFile(ouputPdf, ".pdf", file);
			os = new FileOutputStream(outputFile);
			String baseUrl = FileSystems.getDefault().getPath("src", "main", "resources").toUri().toURL().toString();

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(xHtml, baseUrl);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();

			log.info("PDF created successfully");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while converting file into pdf ..", e);
		}
	}
	
}