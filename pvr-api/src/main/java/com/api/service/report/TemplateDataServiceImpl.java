package com.api.service.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.BbdoMasterDao;
import com.api.dao.CategoryMasterDao;
import com.api.dao.CinemaLandScapeDao;
import com.api.dao.CinemaMasterDao;
import com.api.dao.CompitionMasterDao;
import com.api.dao.FixedConstantDao;
import com.api.dao.IrrCalculationDao;
import com.api.dao.MasterTemplateByDistanceDao;
import com.api.dao.ProjectCostSummaryDao;
import com.api.dao.ProjectDetailsDao;
import com.api.services.ProjectRevenueAnalysisService;
import com.api.utils.ChangeCinemaFormat;
import com.api.utils.DateUtil;
import com.api.utils.MathUtil;
import com.common.constants.Constants;
import com.common.mappers.KeyOperatingCostAssumtionsMapper;
import com.common.mappers.VpfIncomeMapper;
import com.common.models.AssumptionsValues;
import com.common.models.AtpAssumptions;
import com.common.models.BbdoMaster;
import com.common.models.CategoryDataSource;
import com.common.models.CinemaMaster;
import com.common.models.CompitionMaster;
import com.common.models.ExistingCinemaLandScape;
import com.common.models.FixedConstant;
import com.common.models.IrrCalculationData;
import com.common.models.LesserLesse;
import com.common.models.ProjectCostData;
import com.common.models.ProjectCostSummary;
import com.common.models.ProjectDetails;
import com.common.models.PropertyDetail;
import com.common.sql.models.CinemaMasterSql;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TemplateDataServiceImpl implements TemplateDataService {

	@Autowired
	private ProjectDetailsDao projectDetailsDao;

	@Autowired
	private BbdoMasterDao bbdoMasterDao;

	@Autowired

	private IrrCalculationDao irrCalculationDao;

	@Autowired
	private BbdoMasterDao bbdoaMaster;

	@Autowired
	private ProjectCostSummaryDao projectCostSummaryDao;

	@Autowired
	private RevenueDataService revenueDataService;

	@Autowired
	private CompitionMasterDao compitionMasterDao;

	@Autowired
	private CompitionDataService compitionDataService;

	@Autowired
	private ProjectRevenueAnalysisService projectRevenueAnalysisService;

	@Autowired
	private MasterTemplateByDistanceDao masterTemplateByDistanceDao;

	@Autowired
	private CinemaLandScapeDao cinemaLandScapeDao;

	@Autowired
	private CinemaMasterDao cinemaMasterDao;

	@Autowired
	private CategoryMasterDao categoryMasterDao;

	@Value("${google.api.key}")
	private String googleKey;

	@Autowired
	private FixedConstantDao fixedConstantDao;

	@Override
	public Map<String, ProjectDetails> getProjectDetails(ProjectDetails projectDetails) {
		Map<String, ProjectDetails> map = new HashMap<>();
		try {
			StringBuilder sft = new StringBuilder();
			sft.append(projectDetails.getProjectName());
			sft.append("-");
			sft.append(projectDetails.getProjectLocation().getCity());
			sft.append("-");
			sft.append(projectDetails.getFeasibilityState());
			sft.append("-");
			sft.append(projectDetails.getReportIdSubVersion());

			projectDetails.setReportSignature(sft.toString());

			/* MathUtil.roundTwoDecimals(projectDetails.getAreaPerSeat()); */

			Integer totalSeats = projectDetails.getCinemaFormat().stream().mapToInt(mapper -> mapper.getTotal()).sum();
			projectDetails
					.setAreaPerSeat((double) projectDetails.getLesserLesse().getLeasableArea() / (double) totalSeats);

			projectDetails.setTotalSeats(totalSeats);
			String cityName = projectDetails.getProjectLocation().getCity();
			BbdoMaster findByTown = bbdoMasterDao.findByTown(cityName);
			if (!ObjectUtils.isEmpty(findByTown)) {
				projectDetails.setCityPopulation(findByTown.getPopn());
			}

			map.put(Constants.DEVELOPER_DETAILS, projectDetails);
			if (!ObjectUtils.isEmpty(projectDetails.getCompetitveProperties())) {
				projectDetails.getCompetitveProperties().forEach(action -> {
					if (action.getSeatCapacity() == null) {
						action.setSeatCapacity(action.getNoOfScreen() != null ? 
								action.getNoOfScreen() * Constants.SEAT_CAPCITY : Constants.SEAT_CAPCITY);
					}
				});
			}
			
			if(!ObjectUtils.isEmpty(projectDetails.getLesserLesse())){
				if(ObjectUtils.isEmpty(projectDetails.getLesserLesse().getRentSecurityDepositForFixedZeroMg())){
					projectDetails.getLesserLesse().setRentSecurityDepositForFixedZeroMg(0.0);
				}
				else {
					Double security = projectDetails.getLesserLesse().getRentSecurityDepositForFixedZeroMg();
					projectDetails.getLesserLesse().setRentSecurityDepositForFixedZeroMg(security);
					
				}
				if(ObjectUtils.isEmpty(projectDetails.getLesserLesse().getAdvanceRent())){
					projectDetails.getLesserLesse().setAdvanceRent(0.0);
				}
				else {
					Double advanceRent = projectDetails.getLesserLesse().getAdvanceRent();
					projectDetails.getLesserLesse().setAdvanceRent(advanceRent);
				}
				if(ObjectUtils.isEmpty(projectDetails.getLesserLesse().getAdvanceRentPeriod())){
					projectDetails.getLesserLesse().setAdvanceRentPeriod(0);
				}
				else {
					Integer advanceRentPeriod = projectDetails.getLesserLesse().getAdvanceRentPeriod();
					projectDetails.getLesserLesse().setAdvanceRentPeriod(advanceRentPeriod);
				}
				
			}else {
				projectDetails.setLesserLesse(new LesserLesse());
				projectDetails.getLesserLesse().setRentSecurityDepositForFixedZeroMg(0.0);
				projectDetails.getLesserLesse().setAdvanceRent(0.0);
			}
			projectDetails.setProjectCost(!ObjectUtils.isEmpty(projectDetails.getProjectCost()) ? projectDetails.getProjectCost() : 0.0);
			
			projectDetails.setProjectCostPerScreen(!ObjectUtils.isEmpty(projectDetails.getProjectCostPerScreen())
					? projectDetails.getProjectCostPerScreen()
					: 0.0);
			projectDetails.setNoOfScreens(
					!ObjectUtils.isEmpty(projectDetails.getNoOfScreens()) ? projectDetails.getNoOfScreens() : 1);

			map.put("projectDetails", projectDetails);

		} catch (Exception e) {
			log.error("error while fetching project details by id...");
		}
		return map;
	}

	@Override
	public Map<String, List<CinemaMaster>> getExistingCinemaLandScape(ProjectDetails projectDetails) {
		Map<String, List<CinemaMaster>> map = new HashMap<>();

		try {
			List<CinemaMaster> cinemaMaster = getCinemaMasterWithinFifteenKilometer(projectDetails);
			char alphaCounter = 65;
			for (CinemaMaster cinema : cinemaMaster) {
				CategoryDataSource category = categoryMasterDao.findByCinemaNavCode(cinema.getNavCinemaCode());
				if (!ObjectUtils.isEmpty(category)) {
					cinema.setCategory(category.getCinemaCategory());
				}
				cinema.setSeatCapacity(revenueDataService.getAllSeatCapacityByHopk(cinema.getHopk() + ""));
				cinema.setAtp(revenueDataService.getAllAtpByHopk(Integer.valueOf(cinema.getHopk())));
				cinema.setOccupancy(revenueDataService.getAllOccupancyByHopk(Integer.valueOf(cinema.getHopk())));
				cinema.setAdmits(revenueDataService.getAllAdmitsByHopk(Integer.valueOf(cinema.getHopk())));
				cinema.setAlphaCounter(alphaCounter);
				alphaCounter++;
			}

			/*
			 * cinemaMaster.forEach(cinema -> { CategoryDataSource category =
			 * categoryMasterDao.findByCinemaNavCode(cinema.getNavCinemaCode()); if
			 * (!ObjectUtils.isEmpty(category)) {
			 * cinema.setCategory(category.getCinemaCategory()); }
			 * cinema.setSeatCapacity(revenueDataService.getAllSeatCapacityByHopk(cinema.
			 * getHopk()+""));
			 * cinema.setAtp(revenueDataService.getAllAtpByHopk(Integer.valueOf(cinema.
			 * getHopk())));
			 * cinema.setOccupancy(revenueDataService.getAllOccupancyByHopk(Integer.valueOf(
			 * cinema.getHopk())));
			 * cinema.setAdmits(revenueDataService.getAllAdmitsByHopk(Integer.valueOf(cinema
			 * .getHopk()))); });
			 */

			map.put(Constants.EXISTING_CINEMA_LANDSACPE, cinemaMaster);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching exiting cinema-land-scope by city");
		}
		return map;
	}

	@Override
	public Map<String, ProjectDetails> getCinemaSpecs(ProjectDetails projectDetails) {
		Map<String, ProjectDetails> map = new HashMap<>();
		try {
			StringBuilder sft = new StringBuilder();
			sft.append(projectDetails.getProjectName());
			sft.append("-");
			sft.append(projectDetails.getProjectLocation().getCity());
			sft.append("-");
			sft.append(projectDetails.getFeasibilityState());
			sft.append("-");
			sft.append(projectDetails.getReportIdSubVersion());
			projectDetails.setReportSignature(sft.toString());
			map.put(Constants.CINEMA_SPECS, projectDetails);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching cinema specs");

		}
		return map;
	}

	@Override
	public Map<String, Object> getIrrCalulationById(ProjectDetails projectDetails) {
		Map<String, Object> map = new HashMap<>();
		try {
			IrrCalculationData irr = irrCalculationDao.findByProjectId(projectDetails.getId());
			Integer leasePeriod = projectDetails.getLesserLesse().getLeasePeriod();
			List<Integer> leasePeriodList = new ArrayList<>();

			for (int i = 1; i <= leasePeriod; i++) {

				leasePeriodList.add(i);
			}
			log.info(leasePeriodList.size() + "");
			if (!ObjectUtils.isEmpty(leasePeriodList))
				irr.setLeasePeriod(leasePeriodList);

			Map<Integer, Double> grossMarginForEachYear = new HashMap<>();
			irr.getGrossMarginForEachYear().forEach((k, v) -> {
				grossMarginForEachYear.put(k, v / 100000);
			});

			/*
			 * Map<Integer,Double> ebitdarForEachYear = new HashMap<>();
			 * irr.getEbitdarForEachYear().forEach((k,v)->{
			 * ebitdarForEachYear.put(k,v/100000); });
			 */
			Map<Integer, Double> rentRevRatio = new HashMap<>();

			irr.getRentEachYear().forEach((k1, v1) -> {
				irr.getTotalRevenueForEachYear().forEach((k2, v2) -> {
					if (k1 == k2)
						rentRevRatio.put(k1, (v1 / v2) * 100);
				});
			});
			Map<Integer, Double> rentCamRevenueRatioDetails = new HashMap<>();

			irr.getRentEachYear().forEach((k1, v1) -> {
				irr.getCamEachYear().forEach((k2, v2) -> {
					irr.getTotalRevenueForEachYear().forEach((k3, v3) -> {
						if (k1 == k2 && k1 == k3)
							rentCamRevenueRatioDetails.put(k1, ((v1 + v2) / v3) * 100);
					});
				});
			});

			irr.setRentRationDetails(rentRevRatio);
			irr.setRentCamRevenueRatioDetails(rentCamRevenueRatioDetails);
			irr.setGrossMarginForEachYear(grossMarginForEachYear);

//			Map<Integer, Double> consumptionFandB = new HashMap<>();
//            Double cogs = !ObjectUtils.isEmpty(projectDetails.getCogs().getInputValues()) ? 
//            		projectDetails.getCogs().getInputValues() : projectDetails.getCogs().getSuggestedValue();
//			irr.getConsumptionFandB().forEach((k1, v1) -> {
//				consumptionFandB.put(k1, cogs);
//			});
//			irr.setConsumptionFandB(consumptionFandB);
			
			map.put(Constants.FEASIBIITY_SUMMARY_AND_PAYBACKS_ANALYSIS, irr);
			log.info(irr.getTotalRevenueForEachYear().get(2).toString());
			map.put("revenue", MathUtil.roundTwoDecimals(irr.getTotalRevenueForEachYear().get(2) / 100000));
			map.put("ebitda", MathUtil.roundTwoDecimals(irr.getGetEBITDAForEachYear().get(2) / 100000));
			map.put("ebitdaMargin", MathUtil.roundTwoDecimals(irr.getGetEBITDAMarginForEachYear().get(2)));
			map.put("admits", MathUtil.roundTwoDecimals(irr.getAdmitsYearWise().get(2)));

			map.put("occupancy", MathUtil.roundTwoDecimals(irr.getOccupancyForEachYear().get(2)));
			map.put("atp", MathUtil.roundTwoDecimals(irr.getAtpForEachYear().get(2)));
			map.put("sph", MathUtil.roundTwoDecimals(irr.getSphForEachYear().get(2)));
			map.put("adSale", MathUtil.roundTwoDecimals(irr.getAdRevenueForEachYear().get(2) / 100000));
			map.put("projectIrrPostTax", MathUtil.roundTwoDecimals(irr.getProjectIRRPostTax()));
			map.put("equityIrr", MathUtil.roundTwoDecimals(irr.getEquityIRR()));
			map.put("paybackPeriod", MathUtil.roundTwoDecimals(irr.getGetEBITDAPayback()));
			map.put("roce", irr.getGetROCE());
			map.put("atpSphRatio", MathUtil.roundTwoDecimals(irr.getSphForEachYear().get(2)/irr.getAtpForEachYear().get(2)));
			
			
			map.put("projectCost", projectDetails.getProjectCost());
			map.put("rent", MathUtil.roundTwoDecimals(irr.getRentEachYear().get(2) / 100000));
			map.put("leasePeroid", projectDetails.getLesserLesse().getLeasePeriod());
			map.put("cam", MathUtil.roundTwoDecimals(irr.getCamEachYear().get(2) / 100000));
			Integer totalSeats = projectDetails.getCinemaFormat().stream().mapToInt(mapper -> mapper.getTotal()).sum();
			map.put("capexPerSeat", MathUtil.roundTwoDecimals(projectDetails.getProjectCost() / totalSeats)*100000);
			map.put("gla", MathUtil.roundTwoDecimals(projectDetails.getLesserLesse().getLeasableArea() / totalSeats));
			map.put("capexPerSqCarpetArea", MathUtil.roundTwoDecimals((
					projectDetails.getProjectCost() / projectDetails.getLesserLesse().getCarpetArea())) *100000);
			map.put("carpetAreaPerSeat",
					MathUtil.roundTwoDecimals(projectDetails.getLesserLesse().getCarpetArea() / totalSeats));
			map.put("rentRevenueRation", rentRevRatio.get(Constants.YEAR_2));
			map.put("capexPerScreen",
					MathUtil.roundTwoDecimals(projectDetails.getProjectCost() / projectDetails.getNoOfScreens()));
			map.put("securityDeposit", MathUtil.roundTwoDecimals(irr.getSecurityDepositForEachYear().get(0)/100000));
			map.put("securityCapex", MathUtil.roundTwoDecimals((irr.getSecurityDepositForEachYear().get(0)+irr.getSecurityDepositForEachYear().get(1)+irr.getCapexForEachYear().get(0)+irr.getCapexForEachYear().get(1))/100000));
			map.put("netSaleOfTickets", MathUtil.roundTwoDecimals((irr.getNetTicketSales().get(2) / 100000)));
			map.put("salseOfFnB", MathUtil.roundTwoDecimals((irr.getNetFnBSalesForEachYear().get(2) / 100000)));
			map.put("adRevenue", MathUtil.roundTwoDecimals((irr.getAdRevenueForEachYear().get(2) / 100000)));
			map.put("vasIncome", MathUtil.roundTwoDecimals((irr.getVasIncomeForEachYear().get(2) / 100000)));
			map.put("otherOperatingIncome", MathUtil.roundTwoDecimals((irr.getOtherOperatingIncome().get(2) / 100000)));
			map.put("filmDistributorShare", MathUtil.roundTwoDecimals((irr.getFilmDistributorShare().get(2) / 100000)));
			map.put("fnbConsumption", MathUtil.roundTwoDecimals((irr.getConsumptionOfFnBForEachYear().get(2) / 100000)));
			map.put("grossMargin", MathUtil.roundTwoDecimals((irr.getGrossMarginPercent().get(2))));
			map.put("personnelExpenses", MathUtil.roundTwoDecimals((irr.getPersonnelExpenseForEachYear().get(2) / 100000)));
			map.put("electricityWater", MathUtil.roundTwoDecimals((irr.getElectricityAndWaterExpenseForEachYear().get(2) / 100000)));
			map.put("rm", MathUtil.roundTwoDecimals((irr.getRepairAndMaintenanceExpenseExpenseForEachYear().get(2) / 100000)));
			map.put("otherOperatingExpense", MathUtil.roundTwoDecimals((irr.getOtherOperatingExpensesForEachYear().get(2) / 100000)));
			map.put("totalFixedExp", MathUtil.roundTwoDecimals((irr.getExpenditureExRentAndCamForEachYear().get(2)) / 100000));
			map.put("ebitdaPaybackPeriod", MathUtil.roundTwoDecimals((irr.getGetEBITDAForEachYear().get(2) / 100000)));
			
		
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching irrCalculation");
		}
		return map;
	}

	@Override
	public Map<String, Object> getCatchmentDetailsId(ProjectDetails projectDetails) {
		Map<String, Object> map = new HashMap<>();
		try {

			map.put("projectDetails", projectDetails);
			map.put("catchmentDetails", bbdoaMaster.findByTown(projectDetails.getProjectLocation().getCity()));

		} catch (Exception e) {
			log.error("erorr while fetching catchment details..");
		}
		return map;
	}

	@Override
	public Map<String, Object> getKeyProjectCostAssumptionById(String projectId) {
		Map<String, Object> map = new HashMap<>();
		try {
			ProjectCostSummary project = projectCostSummaryDao.findByParentId(projectId);
			ProjectCostData advanceRentCost = new ProjectCostData();
			if(!project.getProjectCostDataList().get(37).getDescription().contains("Advance Rent")){
				advanceRentCost.setCost(0.0);
				project.getProjectCostDataList().add(37, advanceRentCost);
			}
			
			if(ObjectUtils.isEmpty(project.getProjectCostDataList().get(38).getCost())) {
				project.getProjectCostDataList().get(38).setCost(0.0);
				
			}
			if (!ObjectUtils.isEmpty(project)) {
				map.put("keyProjectCostAssumption", project.getProjectCostDataList());
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("erorr while fetching catchment details..");
		}
		return map;
	}

	public Map<String, Double> getAddress(String fullAddress) throws IOException, ParseException {
		Map<String, Double> map = new HashMap<>();
		double lat = 0;
		double lng = 0;
		try {
			synchronized (this) {
				URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json" + "?address="
						+ URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false" + "&" + "key=" + googleKey + " ");
				URLConnection conn = url.openConnection();
				ByteArrayOutputStream output = new ByteArrayOutputStream(1024);

				IOUtils.copy(conn.getInputStream(), output);
				output.close();

				ObjectMapper mapper = new ObjectMapper();
				JsonNode array = mapper.readValue(output.toString(), JsonNode.class);

				JsonNode object = array.get("results").get(0);

				JsonNode geometry = object.get("geometry");
				JsonNode location = geometry.get("location");

				lat = location.get("lat").asDouble();
				lng = location.get("lng").asDouble();

			}
			map.put(Constants.LATITUDE, lat);
			map.put(Constants.LONGITUDE, lng);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching google api geocoding api");
		}
		return map;
	}

	public List<CinemaMaster> getCinemaMasterWithinFifteenKilometer(ProjectDetails projectDetails) {
		List<CinemaMaster> updatedCinema = new ArrayList<>();

		Iterable<CinemaMaster> cinemaMaster = cinemaMasterDao.findAll();

		try {
			double upcomingProjectLogitude = projectDetails.getProjectLocation().getLongitude();
			double upcomingProjectLatitude = projectDetails.getProjectLocation().getLatitude();

			cinemaMaster.forEach(cinema -> {
				String cinemaName = cinema.getCinema_name();

				/* Map<String, Double> latLng = getAddress(cinemaName); */
				CinemaMaster cinemaMasterDetails = cinemaMasterDao.findByHopk(Integer.valueOf(cinema.getHopk()));
				if (!ObjectUtils.isEmpty(cinemaMasterDetails)) {
					double distanceBetween = MathUtil.distanceInKm(upcomingProjectLatitude, upcomingProjectLogitude,
							cinemaMasterDetails.getLatitude(), cinemaMasterDetails.getLongitude());
					cinema.setDistance(distanceBetween);

					if (distanceBetween <= 8) {
						updatedCinema.add(cinema);
					}
				}
			});
		} catch (Exception e) {
			log.error("error while fetching cinema master within 10 kilomter...");
		}
		return updatedCinema;
	}

	@Override
	public Map<String, List<CompitionMaster>> getCompitionMasterByProjectId(ProjectDetails projectDetails) {
		Map<String, List<CompitionMaster>> map = new HashMap<>();
		try {

			List<CompitionMaster> compitionMaster = getCompitionMasterWithinTenKilometer(projectDetails);
			char alphaCounter = 65;

			for (CompitionMaster cinema : compitionMaster) {
				cinema.setSeatCapacity(compitionDataService.getAllSeatCapacityByHopk(cinema.getCompititionName()));
				cinema.setAtp(compitionDataService.getAllAtpByHopk(cinema.getCompititionName()));
				cinema.setOccupancy(compitionDataService.getAllOccupancyByHopk(cinema.getCompititionName()));
				cinema.setAdmits(compitionDataService.getAllAdmitsByHopk(cinema.getCompititionName()));
				cinema.setAlphaCounter(alphaCounter);
				alphaCounter++;
			}
			map.put("compitionMaster", compitionMaster);

		} catch (Exception e) {
			log.error("error while fetching compition master within 10 kilomter...");
		}
		return map;
	}

	@Override
	public Map<String, List<Long>> getPvrFootprintrByProjectId(ProjectDetails projectDetails) {
		Map<String, List<Long>> map = new HashMap<>();

		List<Long> noOfCinemas = new ArrayList<>();
		List<Long> noOfScreens = new ArrayList<>();
		List<Long> noOfSeats = new ArrayList<>();

		List<Long> CompitionNoOfCinemas = new ArrayList<>();
		List<Long> CompitionNoOfScreens = new ArrayList<>();
		List<Long> CompitionNoOfSeats = new ArrayList<>();

		try {
			ExistingCinemaLandScape existingCinema = cinemaLandScapeDao.findByProjectId(projectDetails.getId());
			int year = Calendar.getInstance().get(Calendar.YEAR);
			int yearSize = year + 5;
			long cinemaSize = 0;
			long seatCapacity = 0;
			Integer screen = 0;
			cinemaSize = existingCinema.getCinemaMaster().size();
			screen = existingCinema.getCinemaMaster().stream().map(CinemaMaster::getNoOfScreen)
					.filter(predicate -> predicate != null).mapToInt(mapper -> mapper.intValue()).sum();
			seatCapacity = existingCinema.getCinemaMaster().stream().map(CinemaMaster::getSeatCapacity)
					.filter(predicate -> predicate != null).mapToLong(mapper -> mapper.longValue()).sum() / 2;

			for (int j = year; j < yearSize; j++) {

				List<PropertyDetail> pvrProperty = projectDetails.getPvrProperties();

				if (!ObjectUtils.isEmpty(pvrProperty)) {

					List<Integer> datesInt = new ArrayList<Integer>();
					pvrProperty.forEach(action -> {

						Date date;
						try {
							date = DateUtil.getDateBy(action.getTenatativeOpeningDate());
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(date);
							int years = calendar.get(Calendar.YEAR);
							datesInt.add(years);
							action.setDate(years);
						} catch (java.text.ParseException e) {
							e.printStackTrace();
						}

					});

					System.out.println(pvrProperty);
					int year1 = j;
					List<PropertyDetail> sortDate1 = pvrProperty.stream()
							.filter(predicate -> predicate.getDate() == year1).collect(Collectors.toList());

					if (!ObjectUtils.isEmpty(sortDate1)) {
						cinemaSize = cinemaSize + 1;
						noOfCinemas.add(cinemaSize);

						screen = (int) (screen + sortDate1.stream().filter(predicate -> predicate != null)
								.mapToLong(m -> m.getNoOfScreen()).sum());
						noOfScreens.add((long) screen);

						seatCapacity = seatCapacity + sortDate1.stream().filter(predicate -> predicate != null)
								.mapToLong(m -> m.getSeatCapacity()).sum();
						noOfSeats.add(seatCapacity);
					} else {
						noOfCinemas.add(cinemaSize);
						noOfScreens.add((long) screen);
						noOfSeats.add(seatCapacity);

					}

				} else {
					noOfCinemas.add((long) existingCinema.getCinemaMaster().size());
					screen = existingCinema.getCinemaMaster().stream().map(CinemaMaster::getNoOfScreen)
							.filter(predicate -> predicate != null).mapToInt(mapper -> mapper.intValue()).sum();
					noOfScreens.add((long) screen);
					noOfSeats.add(existingCinema.getCinemaMaster().stream().map(CinemaMaster::getSeatCapacity)
							.filter(predicate -> predicate != null).mapToLong(mapper -> mapper.longValue()).sum() / 2);
				}

			}

			// for competition data
			cinemaSize = 0;
			seatCapacity = 0;
			screen = 0;
			cinemaSize = existingCinema.getCompitionMaster().size();

			screen = existingCinema.getCompitionMaster().stream().map(CompitionMaster::getNoOfScreen)
					.filter(predicate -> predicate != null)
					.mapToInt(mapper -> mapper.intValue() == 0 ? 0 : mapper.intValue()).sum();

			seatCapacity = existingCinema.getCompitionMaster().stream().map(CompitionMaster::getSeatCapacity)
					.filter(predicate -> predicate != null).mapToLong(mapper -> mapper.longValue()).sum();

			for (int j = year; j < yearSize; j++) {
				if (!ObjectUtils.isEmpty(existingCinema)) {

					List<PropertyDetail> pvrProperty = projectDetails.getCompetitveProperties();

					if (!ObjectUtils.isEmpty(pvrProperty)) {

						List<Integer> datesInt = new ArrayList<Integer>();
						pvrProperty.forEach(action -> {

							Date date;
							try {
								date = DateUtil.getDateBy(action.getTenatativeOpeningDate());
								Calendar calendar = Calendar.getInstance();
								calendar.setTime(date);
								int years = calendar.get(Calendar.YEAR);
								datesInt.add(years);
								action.setDate(years);
							} catch (java.text.ParseException e) {
								e.printStackTrace();
							}

						});

						System.out.println(pvrProperty);
						int year1 = j;
						List<PropertyDetail> sortDate1 = pvrProperty.stream()
								.filter(predicate -> predicate.getDate() == year1).collect(Collectors.toList());

						if (!ObjectUtils.isEmpty(sortDate1)) {
							cinemaSize = cinemaSize + 1;
							CompitionNoOfCinemas.add(cinemaSize);

							screen = (int) (screen + sortDate1.stream().filter(predicate -> predicate != null)
									.mapToLong(m -> m.getNoOfScreen()).sum());
							CompitionNoOfScreens.add((long) screen);

							seatCapacity = seatCapacity + sortDate1.stream()
									.mapToLong(m -> m.getSeatCapacity() != null ? m.getSeatCapacity() : 0).sum();
							CompitionNoOfSeats.add(seatCapacity);
						} else {
							CompitionNoOfCinemas.add(cinemaSize);
							CompitionNoOfScreens.add((long) screen);
							CompitionNoOfSeats.add(seatCapacity);

						}

					} else {
						CompitionNoOfCinemas.add((long) existingCinema.getCompitionMaster().size());
						int screens = existingCinema.getCompitionMaster().stream().map(CompitionMaster::getNoOfScreen)
								.filter(predicate -> predicate != null).mapToInt(mapper -> mapper.intValue()).sum();
						CompitionNoOfScreens.add((long) screens);

						CompitionNoOfSeats.add(existingCinema.getCompitionMaster().stream()
								.map(CompitionMaster::getSeatCapacity).filter(predicate -> predicate != null)
								.mapToLong(mapper -> mapper.longValue()).sum());
					}
				} else {
					CompitionNoOfCinemas.add(0l);
					CompitionNoOfScreens.add(0l);
					CompitionNoOfSeats.add(0l);
				}

			}

			List<Long> marketNoOfCinemas = new ArrayList<>();
			List<Long> marketNoOfScreens = new ArrayList<>();
			List<Long> marketNoOfSeats = new ArrayList<>();

			for (int i = 0; i < noOfCinemas.size(); i++) {
				long noOfCinema = noOfCinemas.get(i);
				long compitionOfCinema = CompitionNoOfCinemas.get(i);
				marketNoOfCinemas.add(noOfCinema + compitionOfCinema);
			}

			for (int j = 0; j < noOfScreens.size(); j++) {
				long noOfScreen = noOfScreens.get(j);
				long compitionOfScren = CompitionNoOfScreens.get(j);
				marketNoOfScreens.add(noOfScreen + compitionOfScren);
			}

			for (int k = 0; k < noOfSeats.size(); k++) {
				long noOFSeats = noOfSeats.get(k);
				long CompitionNoOFSeats = CompitionNoOfSeats.get(k);
				marketNoOfSeats.add(noOFSeats + CompitionNoOFSeats);
			}
			map.put(Constants.NO_OF_CINEMA, noOfCinemas);
			map.put(Constants.NO_OF_SCREEN, noOfScreens);
			map.put(Constants.NO_OF_SEATS, noOfSeats);

			map.put(Constants.NO_OF_CINEMA_COMPITION, CompitionNoOfCinemas);
			map.put(Constants.NO_OF_SCREEN_COMPITION, CompitionNoOfScreens);
			map.put(Constants.NO_OF_SEATS_COMPITION, CompitionNoOfSeats);

			map.put(Constants.NO_OF_CINEMA_MARKET, marketNoOfCinemas);
			map.put(Constants.NO_OF_SCREEN_MARKET, marketNoOfScreens);
			map.put(Constants.NO_OF_SEATS_MARKET, marketNoOfSeats);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching pvr footPrint ..", e);
		}
		return map;
	}

	@Override
	public Map<String, List<CinemaMaster>> getKeyRevenueAssumptionsOccupancy(ProjectDetails projectDetails) {
		Map<String, List<CinemaMaster>> pvrProperties = new HashMap<>();

		try {
			List<CinemaMaster> cinema = projectRevenueAnalysisService
					.getAllKeyRevenueAssumptionOccupancy(projectDetails);
			if (cinema.size() == 2) {
				CinemaMaster cinemaMaster = new CinemaMaster();
				cinemaMaster.setCinema_name("NA");
				cinema.add(cinemaMaster);
			} else if (cinema.size() == 1) {
				CinemaMaster cinemaMaster = new CinemaMaster();
				cinemaMaster.setCinema_name("NA");
				cinema.add(cinemaMaster);

				CinemaMaster cinemaMaster1 = new CinemaMaster();
				cinemaMaster1.setCinema_name("NA");
				cinema.add(cinemaMaster1);
			}
			pvrProperties.put("benchmarkCinemas", cinema);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching pvr key revenue assumptions ..");
		}
		return pvrProperties;
	}

	@Override
	public Map<String, List<AssumptionsValues>> getProjectDetailsOccupancy(ProjectDetails projectDetails) {
		Map<String, List<AssumptionsValues>> occupancyValues = new HashMap<>();
		try {

			List<AssumptionsValues> occpancyDetails = projectDetails.getOccupancy();
			occupancyValues.put("occupancy", occpancyDetails);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching upcoming project occupency ..");
		}
		return occupancyValues;
	}

	@Override
	public Map<String, List<CinemaMasterSql>> getKeyRevenueAssumptionsAdmits(String projectId,
			Optional<ProjectDetails> upcomingProjectDetails) {

		Optional<ProjectDetails> projectDetails = null;

		if (!ObjectUtils.isEmpty(projectId)) {
			projectDetails = projectDetailsDao.findById(projectId);
		} else {
			projectDetails = upcomingProjectDetails;
		}
		try {

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching upcoming project admits ..");
		}
		return null;
	}

	@Override
	public Map<String, List<AtpAssumptions>> getKeyRevenueAssumptionsAtp(ProjectDetails projectDetails) {
		Map<String, List<AtpAssumptions>> map = new HashMap<>();
		try {
			List<AtpAssumptions> atpValues = ChangeCinemaFormat.changeAtpFormat(projectDetails.getAtp(),projectDetails);
			map.put("atp", atpValues);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching upcoming project atp ..");
		}
		return map;
	}

	@Override
	public Map<String, List<AssumptionsValues>> getProjectDetailsSph(ProjectDetails projectDetails) {
		Map<String, List<AssumptionsValues>> map = new HashMap<>();
		try {
			List<AssumptionsValues> sphValues = ChangeCinemaFormat.changeSphFormat(projectDetails.getSph(),projectDetails);
			map.put("sph", sphValues);
		} catch (Exception e) {
			log.error("error while fetching sph by Project Details..");
		}
		return map;
	}

	@Override
	public Map<String, List<KeyOperatingCostAssumtionsMapper>> getProjectDetailsOperatingAssumtion(
			ProjectDetails projectDetails) {
		Map<String, List<KeyOperatingCostAssumtionsMapper>> map = new HashMap<>();
		List<KeyOperatingCostAssumtionsMapper> list = new ArrayList<>();
		try {

			KeyOperatingCostAssumtionsMapper assumption = new KeyOperatingCostAssumtionsMapper();
			assumption.setOpertaingCostType(projectDetails.getPersonalExpenses() != null ? "Personnel Expense" : "");
			assumption.setCostType(projectDetails.getPersonalExpenses().getCostType() != null
					? (double) projectDetails.getPersonalExpenses().getCostType() / 100000
					: 0l);
			assumption.setInputValues(projectDetails.getPersonalExpenses().getInputValues() != null
					? projectDetails.getPersonalExpenses().getInputValues() / 100000
					: 0.0);
			assumption.setComments(projectDetails.getPersonalExpenses().getComments() != null
					? projectDetails.getPersonalExpenses().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption1 = new KeyOperatingCostAssumtionsMapper();
			assumption1.setOpertaingCostType(
					projectDetails.getElectricityWaterBill() != null ? "Electricity And Water Expense" : "");
			assumption1.setCostType(projectDetails.getElectricityWaterBill().getCostType() != null
					? (double) projectDetails.getElectricityWaterBill().getCostType() / 100000
					: 0l);
			assumption1.setInputValues(projectDetails.getElectricityWaterBill().getInputValues() != null
					? projectDetails.getElectricityWaterBill().getInputValues() / 100000
					: 0.0);
			assumption1.setComments(projectDetails.getElectricityWaterBill().getComments() != null
					? projectDetails.getElectricityWaterBill().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption2 = new KeyOperatingCostAssumtionsMapper();
			assumption2
					.setOpertaingCostType(projectDetails.getTotalRMExpenses() != null ? "Repair And Maintenance" : "");
			assumption2.setCostType(projectDetails.getTotalRMExpenses().getCostType() != null
					? (double) projectDetails.getTotalRMExpenses().getCostType() / 100000
					: 0l);
			assumption2.setInputValues(projectDetails.getTotalRMExpenses().getInputValues() != null
					? projectDetails.getTotalRMExpenses().getInputValues() / 100000
					: 0.0);
			assumption2.setComments(projectDetails.getTotalRMExpenses().getComments() != null
					? projectDetails.getTotalRMExpenses().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption3 = new KeyOperatingCostAssumtionsMapper();
			assumption3.setOpertaingCostType(projectDetails.getLegalFee() != null ? "Legal Expense" : "");
			assumption3.setCostType(projectDetails.getLegalFee().getCostType() != null
					? (double) projectDetails.getLegalFee().getCostType() / 100000
					: 0l);
			assumption3.setInputValues(projectDetails.getLegalFee().getInputValues() != null
					? projectDetails.getLegalFee().getInputValues() / 100000
					: 0.0);
			assumption3.setComments(
					projectDetails.getLegalFee().getComments() != null ? projectDetails.getLegalFee().getComments()
							: "");

			KeyOperatingCostAssumtionsMapper assumption4 = new KeyOperatingCostAssumtionsMapper();
			assumption4.setOpertaingCostType(projectDetails.getInsuranceExpense() != null ? "Insurance Expense" : "");
			assumption4.setCostType(projectDetails.getInsuranceExpense().getCostType() != null
					? (double) projectDetails.getInsuranceExpense().getCostType() / 100000
					: 0l);
			assumption4.setInputValues(projectDetails.getInsuranceExpense().getInputValues() != null
					? projectDetails.getInsuranceExpense().getInputValues() / 100000
					: 0.0);
			assumption4.setComments(projectDetails.getInsuranceExpense().getComments() != null
					? projectDetails.getInsuranceExpense().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption5 = new KeyOperatingCostAssumtionsMapper();
			assumption5.setOpertaingCostType(
					projectDetails.getHouseKeepingExpense() != null ? "Housekeeping Expense" : "");
			assumption5.setCostType(projectDetails.getHouseKeepingExpense().getCostType() != null
					? (double) projectDetails.getHouseKeepingExpense().getCostType() / 100000
					: 0l);
			assumption5.setInputValues(projectDetails.getHouseKeepingExpense().getInputValues() != null
					? projectDetails.getHouseKeepingExpense().getInputValues() / 100000
					: 0.0);
			assumption5.setComments(projectDetails.getHouseKeepingExpense().getComments() != null
					? projectDetails.getHouseKeepingExpense().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption6 = new KeyOperatingCostAssumtionsMapper();
			assumption6.setOpertaingCostType(projectDetails.getSecurityExpense() != null ? "Security Expense" : "");
			assumption6.setCostType(projectDetails.getSecurityExpense().getCostType() != null
					? (double) projectDetails.getSecurityExpense().getCostType() / 100000
					: 0l);
			assumption6.setInputValues(projectDetails.getSecurityExpense().getInputValues() != null
					? projectDetails.getSecurityExpense().getInputValues() / 100000
					: 0.0);
			assumption6.setComments(projectDetails.getSecurityExpense().getComments() != null
					? projectDetails.getSecurityExpense().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption7 = new KeyOperatingCostAssumtionsMapper();
			assumption7.setOpertaingCostType(projectDetails.getMarketingExpense() != null ? "Marketing Expense" : "");
			assumption7.setCostType(projectDetails.getMarketingExpense().getCostType() != null
					? (double) projectDetails.getMarketingExpense().getCostType() / 100000
					: 0l);
			assumption7.setInputValues(projectDetails.getMarketingExpense().getInputValues() != null
					? projectDetails.getMarketingExpense().getInputValues() / 100000
					: 0.0);
			assumption7.setComments(projectDetails.getMarketingExpense().getComments() != null
					? projectDetails.getMarketingExpense().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption8 = new KeyOperatingCostAssumtionsMapper();
			assumption8.setOpertaingCostType(
					projectDetails.getPrintingAndStationaryExpense() != null ? "Printing And Stationary" : "");
			assumption8.setCostType(projectDetails.getPrintingAndStationaryExpense().getCostType() != null
					? (double) projectDetails.getPrintingAndStationaryExpense().getCostType() / 100000
					: 0l);
			assumption8.setInputValues(projectDetails.getPrintingAndStationaryExpense().getInputValues() != null
					? projectDetails.getPrintingAndStationaryExpense().getInputValues() / 100000
					: 0.0);
			assumption8.setComments(projectDetails.getPrintingAndStationaryExpense().getComments() != null
					? projectDetails.getPrintingAndStationaryExpense().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption9 = new KeyOperatingCostAssumtionsMapper();
			assumption9.setOpertaingCostType(
					projectDetails.getCommunicationExpense() != null ? "Communication Expense" : "");
			assumption9.setCostType(projectDetails.getCommunicationExpense().getCostType() != null
					? (double) projectDetails.getCommunicationExpense().getCostType() / 100000
					: 0l);
			assumption9.setInputValues(projectDetails.getCommunicationExpense().getInputValues() != null
					? projectDetails.getCommunicationExpense().getInputValues() / 100000
					: 0.0);
			assumption9.setComments(projectDetails.getCommunicationExpense().getComments() != null
					? projectDetails.getCommunicationExpense().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption10 = new KeyOperatingCostAssumtionsMapper();
			assumption10.setOpertaingCostType(projectDetails.getMiscellaneousExpenses() != null ? "Miscellaneous" : "");
			assumption10.setCostType(projectDetails.getMiscellaneousExpenses().getCostType() != null
					? (double) projectDetails.getMiscellaneousExpenses().getCostType() / 100000
					: 0l);
			assumption10.setInputValues(projectDetails.getMiscellaneousExpenses().getInputValues() != null
					? projectDetails.getMiscellaneousExpenses().getInputValues() / 100000
					: 0.0);
			assumption10.setComments(projectDetails.getMiscellaneousExpenses().getComments() != null
					? projectDetails.getMiscellaneousExpenses().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption11 = new KeyOperatingCostAssumtionsMapper();
			assumption11.setOpertaingCostType(
					projectDetails.getTravellingAndConveyanceExpense() != null ? "Travelling and Conveyance" : "");
			assumption11.setCostType(projectDetails.getTravellingAndConveyanceExpense().getCostType() != null
					? (double) projectDetails.getTravellingAndConveyanceExpense().getCostType() / 100000
					: 0l);
			assumption11.setInputValues(projectDetails.getTravellingAndConveyanceExpense().getInputValues() != null
					? projectDetails.getTravellingAndConveyanceExpense().getInputValues() / 100000
					: 0.0);
			assumption11.setComments(projectDetails.getTravellingAndConveyanceExpense().getComments() != null
					? projectDetails.getTravellingAndConveyanceExpense().getComments()
					: "");

			KeyOperatingCostAssumtionsMapper assumption12 = new KeyOperatingCostAssumtionsMapper();
			assumption12.setOpertaingCostType(projectDetails.getInternetExpense() != null ? "Internet Expense" : "");
			assumption12.setCostType(projectDetails.getInternetExpense().getCostType() != null
					? (double) projectDetails.getInternetExpense().getCostType() / 100000
					: 0l);
			assumption12.setInputValues(projectDetails.getInternetExpense().getInputValues() != null
					? projectDetails.getInternetExpense().getInputValues() / 100000
					: 0.0);
			assumption12.setComments(projectDetails.getInternetExpense().getComments() != null
					? projectDetails.getInternetExpense().getComments()
					: "");
			
			KeyOperatingCostAssumtionsMapper assumption13 = new KeyOperatingCostAssumtionsMapper();
			assumption13.setOpertaingCostType(projectDetails.getTotalOverheadSuggestedSum() != null ? "Total Other Overhead Cost" : "");
			assumption13.setCostType(projectDetails.getTotalOverheadSuggestedSum() != null
					? (double) projectDetails.getTotalOverheadSuggestedSum() / 100000
					: 0l);
			assumption13.setInputValues(projectDetails.getTotalOverheadInputSum() != null
					? projectDetails.getTotalOverheadInputSum() / 100000
					: 0.0);
			assumption13.setComments(projectDetails.getTotalOverhead() != null
					&& projectDetails.getTotalOverhead().getComments() != null
					? projectDetails.getTotalOverhead().getComments()
					: "");

			list.add(assumption);
			list.add(assumption1);
			list.add(assumption2);
			list.add(assumption3);
			list.add(assumption4);
			list.add(assumption5);
			list.add(assumption6);
			list.add(assumption7);
			list.add(assumption8);
			list.add(assumption9);
			list.add(assumption10);
			list.add(assumption11);
			list.add(assumption12);
			list.add(assumption13);

			map.put("keyOperatingAssumption", list);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching key operating cost assumptions by Project Details..");
		}
		return map;
	}

	@Override
	public Map<String, VpfIncomeMapper> getVpfInComeDetails(ProjectDetails projectDetails) {
		Map<String, VpfIncomeMapper> map = new HashMap<>();
		IrrCalculationData irr = irrCalculationDao.findByProjectId(projectDetails.getId());
		try {
			VpfIncomeMapper vpf = new VpfIncomeMapper();
			String year = projectDetails.getDateOfOpening().substring(6, 10);
			int openingYear = Integer.parseInt(year);

			int lastYear = 2024;

			if (openingYear > lastYear) {
				vpf.setAverageVpfIncome(0.0);
				vpf.setNoOfScreen(0);
				vpf.setTotalVpfincomePerAnnum(0.0);
				vpf.setVpfIncomeValidity(0);
			} else {
				Integer noOfScreen = projectDetails.getNoOfScreens();

				double perScreenIncome = 0.0;
				if (noOfScreen <= 5) {
					perScreenIncome = 5.0;
				} else if (noOfScreen <= 10.0) {
					perScreenIncome = 4.5;
				} else if (noOfScreen <= 15) {
					perScreenIncome = 4.0;
				} else {
					perScreenIncome = 3.5;
				}

				vpf.setAverageVpfIncome(perScreenIncome);
				vpf.setNoOfScreen(noOfScreen);
				vpf.setTotalVpfincomePerAnnum(irr.getVpfIncome() / 100000);
				vpf.setVpfIncomeValidity(lastYear - openingYear);
			}

			map.put("vpfIncome", vpf);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching key operating cost assumptions by Project Details..");
		}
		return map;
	}

	@Override
	public Map<String, List<Double>> getKeyAssumption(ProjectDetails projectDetails) {
		Map<String, List<Double>> map = new HashMap<>();
		try {
			List<Double> admitGwroth = new ArrayList<>();
			List<Double> atpGrowth = new ArrayList<>();
			List<Double> sphGrowth = new ArrayList<>();
			List<Double> sponsorshipGrowth = new ArrayList<>();
			List<Double> convenienceGrowth = new ArrayList<>();
			List<Double> otherIncomeGrowth = new ArrayList<>();
			List<Double> personalExpense = new ArrayList<>();
			List<Double> electricityAndExpense = new ArrayList<>();
			List<Double> repairAndMainTence = new ArrayList<>();
			List<Double> otherOverheadExpense = new ArrayList<>();
			List<Double> rentExpense = new ArrayList<>();
			List<Double> commonAreaMain = new ArrayList<>();
			List<Double> propertytax = new ArrayList<>();
			List<Double> maintenaceCapex = new ArrayList<>();
			List<Double> fnbCogs = new ArrayList<>();
			List<Double> fhc = new ArrayList<>();
			List<Double> leasePeriodList = new ArrayList<>();
			Integer leasePeriod = projectDetails.getLesserLesse().getLeasePeriod();

			List<FixedConstant> fixedConstant = (List<FixedConstant>) fixedConstantDao.findAll();
			IrrCalculationData irr = irrCalculationDao.findByProjectId(projectDetails.getId());

//			Map<Integer, Double> mapFixedConstant = fixedConstant.get(Constants.ZERO).getRevenueAssumptionsGrowthRate()
//					.getAdmitsGrowth();
			Map<Integer, Double> mapFixedConstant = projectDetails.getRevenueAssumptionsGrowthRate()
					.getAdmitsGrowth();
			mapFixedConstant.forEach((key, value) -> {
				if (key <= leasePeriod)
					admitGwroth.add(value);
			});

			/*
			 * Map<Integer, Double> mapFixedAtpGrwoth =
			 * fixedConstant.get(Constants.ZERO).getRevenueAssumptionsGrowthRate()
			 * .getAtpGrowth(); mapFixedAtpGrwoth.forEach((key, value) -> {
			 * atpGrowth.add(value); });
			 */
//			Map<Integer, Double> atpForEachYear = irr.getAtpForEachYear();
//			Map<Integer, Double> sphForEachYear = irr.getSphForEachYear();
//			Map<Integer, Double> atp = new HashMap<>();
//			Map<Integer, Double> sph = new HashMap<>();
//			atpForEachYear.forEach((key, value) -> {
//				if (key != 1) {
//					Double atpDetails = MathUtil.roundTwoDecimals(
//							((atpForEachYear.get(key) - atpForEachYear.get(key - 1)) / atpForEachYear.get(key - 1))
//									* 100);
//					atp.put(key, atpDetails);
//				}
//			});
//			atp.forEach((key, value) -> {
//				atpGrowth.add(value);
//			});
//			sphForEachYear.forEach((key, value) -> {
//				if (key != 1) {
//					Double atpDetails = MathUtil.roundTwoDecimals(
//							((sphForEachYear.get(key) - sphForEachYear.get(key - 1)) / sphForEachYear.get(key - 1))
//									* 100);
//					sph.put(key, atpDetails);
//				}
//			});
//
//			sph.forEach((key, value) -> {
//				sphGrowth.add(value);
//			});
			
			Map<Integer, Double> mapFixedConstantAtp = projectDetails.getRevenueAssumptionsGrowthRate()
					.getAtpGrowth();
			mapFixedConstantAtp.forEach((key, value) -> {
				atpGrowth.add(value);
			});
			
			Map<Integer, Double> mapFixedConstantSph = projectDetails.getRevenueAssumptionsGrowthRate()
					.getSphGrowth();
			mapFixedConstantSph.forEach((key, value) -> {
				sphGrowth.add(value);
			});

			Map<Integer, Double> maintenaceCapexPercentage = calculateCapexPercentage(projectDetails, fixedConstant);

			maintenaceCapexPercentage.forEach((key, value) -> {

				maintenaceCapex.add(value);
			});

			/*
			 * Map<Integer, Double> mapFixedsphGrwoth =
			 * fixedConstant.get(Constants.ZERO).getRevenueAssumptionsGrowthRate()
			 * .getSphGrowth(); mapFixedsphGrwoth.forEach((key, value) -> {
			 * sphGrowth.add(value); });
			 */

//			Map<Integer, Double> mapFixedsponshorshipGrwoth = fixedConstant.get(Constants.ZERO)
//					.getRevenueAssumptionsGrowthRate().getSponsorshipIncomeGrowth();
			Map<Integer, Double> mapFixedsponshorshipGrwoth = projectDetails.getRevenueAssumptionsGrowthRate()
					.getSponsorshipIncomeGrowth();
			mapFixedsponshorshipGrwoth.forEach((key, value) -> {
				if (key <= leasePeriod)
					sponsorshipGrowth.add(value);
			});

//			Map<Integer, Double> mapFixedconvenianceGrwoth = fixedConstant.get(Constants.ZERO)
//					.getRevenueAssumptionsGrowthRate().getConvenienceFeeGrowth();
			Map<Integer, Double> mapFixedconvenianceGrwoth = projectDetails.getRevenueAssumptionsGrowthRate()
					.getConvenienceFeeGrowth();
			mapFixedconvenianceGrwoth.forEach((key, value) -> {
				if (key <= leasePeriod)
					convenienceGrowth.add(value);
			});

//			Map<Integer, Double> mapFixedOtherIncomeGrwoth = fixedConstant.get(Constants.ZERO)
//					.getRevenueAssumptionsGrowthRate().getOtherIncomeGrowth();
			Map<Integer, Double> mapFixedOtherIncomeGrwoth = projectDetails.getRevenueAssumptionsGrowthRate()
					.getOtherIncomeGrowth();
			mapFixedOtherIncomeGrwoth.forEach((key, value) -> {
				if (key <= leasePeriod)
					otherIncomeGrowth.add(value);
			});

//			Map<Integer, Double> mapFixedCostGrowth = fixedConstant.get(Constants.ZERO).getCostAssumptionsGrowthRate()
//					.getPersonnelExpenses();
			Map<Integer, Double> mapFixedCostGrowth = projectDetails.getCostAssumptionsGrowthRate()
					.getPersonnelExpenses();
			mapFixedCostGrowth.forEach((key, value) -> {
				if (key <= leasePeriod)
					personalExpense.add(value);
			});

//			Map<Integer, Double> mapFixedElectricity = fixedConstant.get(Constants.ZERO).getCostAssumptionsGrowthRate()
//					.getElectricityAndAirconditioning();
			Map<Integer, Double> mapFixedElectricity = projectDetails.getCostAssumptionsGrowthRate()
					.getElectricityAndAirconditioning();
			mapFixedElectricity.forEach((key, value) -> {
				if (key <= leasePeriod)
					electricityAndExpense.add(value);
			});

//			Map<Integer, Double> mapFixedRepairAndMaintaince = fixedConstant.get(Constants.ZERO)
//					.getCostAssumptionsGrowthRate().getRepairAndMaintenance();
			Map<Integer, Double> mapFixedRepairAndMaintaince = projectDetails.getCostAssumptionsGrowthRate()
					.getRepairAndMaintenance();
			mapFixedRepairAndMaintaince.forEach((key, value) -> {
				if (key <= leasePeriod)
					repairAndMainTence.add(value);
			});

//			Map<Integer, Double> mapFixedOtherheadExpense = fixedConstant.get(Constants.ZERO)
//					.getCostAssumptionsGrowthRate().getOtherOverheadExpenses();
			Map<Integer, Double> mapFixedOtherheadExpense = projectDetails.getCostAssumptionsGrowthRate()
					.getOtherOverheadExpenses();
			mapFixedOtherheadExpense.forEach((key, value) -> {
				if (key <= leasePeriod)
					otherOverheadExpense.add(value);
			});

			Map<Integer, Double> rent = rentTerm(projectDetails);

			rent.forEach((key, value) -> {
				if (key < leasePeriod)
					rentExpense.add(value);
			});

			Map<Integer, Double> cam = camTerm(projectDetails);
			cam.forEach((key, value) -> {
				if (key < leasePeriod)
					commonAreaMain.add(value);
			});

//			Map<Integer, Double> mapFixedPropertyTax = fixedConstant.get(Constants.ZERO).getCostAssumptionsGrowthRate()
//					.getPropertyTax();
			Map<Integer, Double> mapFixedPropertyTax = projectDetails.getCostAssumptionsGrowthRate()
					.getPropertyTax();
			mapFixedPropertyTax.forEach((key, value) -> {
				if (key <= leasePeriod)
					propertytax.add(value);
			});

			for (int i = 0; i < admitGwroth.size(); i++) {
//				fnbCogs.add(fixedConstant.get(Constants.ZERO).getFnbCogs());
				fnbCogs.add(!ObjectUtils.isEmpty(projectDetails.getCogs().getInputValues()) ?  projectDetails.getCogs().getInputValues()    
						: projectDetails.getCogs().getSuggestedValue());
			}

			for (int i = 0; i < admitGwroth.size(); i++) {
				fhc.add(irr.getTotalFilmHireCostPercentage());
			}

			for (int i = 2; i <= leasePeriod; i++) {
				leasePeriodList.add((double) i);
			}
			/*
			 * Double d= new Double(leasePeriod2); leasePeriod.add(d );
			 */
			map.put("leasePeriod", leasePeriodList);
			map.put(Constants.ADMIT_GROWTH, admitGwroth);
			map.put(Constants.ATP_GROWTH, atpGrowth);
			map.put(Constants.SPH_GROWTH, sphGrowth);
			map.put(Constants.SPONSORSHIP_GROWTH, sponsorshipGrowth);
			map.put(Constants.CONVENIENCE_GROWTH, convenienceGrowth);
			map.put(Constants.OTHER_INCOME_GROWTH, otherIncomeGrowth);
			map.put(Constants.PERSONNAL_COST, personalExpense);
			map.put(Constants.WATERANDELECTRICITY, electricityAndExpense);
			map.put(Constants.REPAIRANDMAINTANCE, repairAndMainTence);
			map.put(Constants.OTHER_OVERHEAD_EXPENSE, otherOverheadExpense);
			map.put(Constants.RENT_EXPENSE, rentExpense);
			map.put(Constants.COMMON_AREA_MAINTAINCE, commonAreaMain);
			map.put(Constants.PROPERTY_TAX, propertytax);
			map.put(Constants.MAINTENCE_CAPEX_INITIAL, maintenaceCapex);
			map.put(Constants.F_B_COGS, fnbCogs);
			map.put(Constants.F_H_C, fhc);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching key assumptions..");
		}
		return map;
	}

	private Map<Integer, Double> calculateCapexPercentage(ProjectDetails projectDetails,
			List<FixedConstant> fixedConstant) {
		int leasePeriod = projectDetails.getLesserLesse().getLeasePeriod();
		Double maintenanceCapexOfRevenueForAllYearTillLastRenovation = fixedConstant.get(Constants.ZERO)
				.getMaintenanceCapexOfRevenueForAllYearTillLastRenovation();
		Double maintenanceCapexOfRevenueForYearAfterLastRenovation = fixedConstant.get(Constants.ZERO)
				.getMaintenanceCapexOfRevenueForYearAfterLastRenovation();
		int x = leasePeriod / 7;
		Double initailCapexInSevenYear = fixedConstant.get(0).getInitailCapexInSevenYear();
		Double renovationCapex = MathUtil.roundTwoDecimals(initailCapexInSevenYear);
		Map<Integer, Double> maintenaceCapexPercentage = new HashMap<>();
		int noOfyear = x - 1;
		int count = 0;
		int flag = 0;
		for (int i = 2; i <= leasePeriod; i++) {
			int renovationCapexYear = 7 * (x - 1);
			if (i != 1) {

				if (noOfyear == count) {
					if (flag < 3) {
						flag++;
						maintenaceCapexPercentage.put(i, maintenanceCapexOfRevenueForAllYearTillLastRenovation);
					} else {
						maintenaceCapexPercentage.put(i, maintenanceCapexOfRevenueForYearAfterLastRenovation);
					}

				} else {
					if (renovationCapexYear % i == 0) {
						count++;
						// addition =
						// MathUtil.roundTwoDecimals(maintenanceCapexOfRevenueForYearAfterLastRenovation
						// * initailCapexInSevenYear);
						maintenaceCapexPercentage.put(i, renovationCapex);
					} else {
						// Maintenance capex as % of revenue * revenue for that year
						maintenaceCapexPercentage.put(i, maintenanceCapexOfRevenueForAllYearTillLastRenovation);
					}
				}

			}
		}
		return maintenaceCapexPercentage;
	}

	Map<Integer, Double> rentTerm(ProjectDetails projectDetails) {
		Map<Integer, Double> map = new LinkedHashMap<>();
		Integer frequencyOfEscalation = projectDetails.getRentTerm().getFreqOfEscalation() != null
				? projectDetails.getRentTerm().getFreqOfEscalation()
				: 0;
		Double percentageOfEscalation = projectDetails.getRentTerm().getPercOfEscalation() != null
				? projectDetails.getRentTerm().getPercOfEscalation()
				: 0.0;

		for (int i = 1; i <= 15; i++) {
			if (frequencyOfEscalation == i) {
				map.put(i + Constants.ONE, percentageOfEscalation);
			}
			map.put(i, 0.0);
		}

		return map;
	}

	Map<Integer, Double> camTerm(ProjectDetails projectDetails) {
		Map<Integer, Double> map = new LinkedHashMap<>();
		Integer frequencyOfEscalation = projectDetails.getCamTerm().getFrequencyOfEscaltionYears() != null
				? projectDetails.getCamTerm().getFrequencyOfEscaltionYears()
				: 0;
		Double percentageOfEscalation = projectDetails.getCamTerm().getPercentageEscalation() != null
				? projectDetails.getCamTerm().getFrequencyOfEscaltionYears()
				: 0.0;

		for (int i = 1; i <= 15; i++) {
			if (frequencyOfEscalation == i) {
				map.put(i + Constants.ONE, percentageOfEscalation);
			}
			map.put(i, 0.0);
		}

		return map;
	}

	public List<CompitionMaster> getCompitionMasterWithinTenKilometer(ProjectDetails projectDetails) {
		List<CompitionMaster> updatedCompition = new ArrayList<>();

		List<CompitionMaster> compitionMaster = (List<CompitionMaster>) compitionMasterDao.findAll();

		try {
			double upcomingProjectLogitude = projectDetails.getProjectLocation().getLongitude();
			double upcomingProjectLatitude = projectDetails.getProjectLocation().getLatitude();

			compitionMaster.parallelStream().distinct().forEach(compition -> {
				double distanceBetween = MathUtil.distanceInKm(upcomingProjectLatitude, upcomingProjectLogitude,
						compition.getLatitude(), compition.getLongitude());
				compition.setDistance(distanceBetween);

				if (distanceBetween <= 10) {
					updatedCompition.add(compition);
				}
			});
		} catch (Exception e) {
			log.error("error while fetching compition master within 10 kilomter...");
		}
		return updatedCompition;
	}

	public Map<String, Double> getAddressByLatLong(String fullAddress) throws IOException, ParseException {
		Map<String, Double> map = new HashMap<>();
		double lat = 0;
		double lng = 0;
		try {
			synchronized (this) {
				URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json" + "?address="
						+ URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false" + "&" + "key=" + googleKey + " ");
				URLConnection conn = url.openConnection();
				ByteArrayOutputStream output = new ByteArrayOutputStream(1024);

				IOUtils.copy(conn.getInputStream(), output);
				output.close();

				ObjectMapper mapper = new ObjectMapper();
				JsonNode array = mapper.readValue(output.toString(), JsonNode.class);

				JsonNode object = array.get("results").get(0);

				JsonNode geometry = object.get("geometry");
				JsonNode location = geometry.get("location");

				lat = location.get("lat").asDouble();
				lng = location.get("lng").asDouble();

			}
			map.put(Constants.LATITUDE, lat);
			map.put(Constants.LONGITUDE, lng);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching google api geocoding api");
		}
		return map;
	}

	@Override
	public Map<String, List<CompitionMaster>> getCompitionMasterByExistingCinemaMaster(ProjectDetails projectDetails) {
		Map<String, List<CompitionMaster>> map = new HashMap<>();
		List<CompitionMaster> comp = null;
		try {
			ExistingCinemaLandScape exiast = cinemaLandScapeDao.findByProjectId(projectDetails.getId());
			if (!ObjectUtils.isEmpty(exiast)) {
				List<CompitionMaster> compitionMaster = exiast.getCompitionMaster();
				comp = compitionMaster.stream().limit(3).collect(Collectors.toList());

				/*
				 * if (comp.size() == 0) { CompitionMaster compition = new CompitionMaster();
				 * compition.setCompititionName("NA"); compition.setOccupancy(0.0);
				 * 
				 * CompitionMaster compition1 = new CompitionMaster();
				 * compition1.setCompititionName("NA"); compition1.setOccupancy(0.0);
				 * 
				 * CompitionMaster compition2 = new CompitionMaster();
				 * compition2.setCompititionName("NA"); compition2.setOccupancy(0.0);
				 * comp.add(compition2); comp.add(compition); comp.add(compition1); }
				 */

				for (int i = 1; i < 4; i++) {
					if (!(i < comp.size())) {
						CompitionMaster compition1 = new CompitionMaster();
						compition1.setCompititionName("NA");
						compition1.setOccupancy(0.0);
						comp.add(compition1);
					}

				}
			} else {
				List<CompitionMaster> master = getCompitionMasterWithinTenKilometer(projectDetails);
				comp = master.stream().limit(3).collect(Collectors.toList());

				for (int i = 1; i < 4; i++) {
					if (!(i < comp.size())) {
						CompitionMaster compition1 = new CompitionMaster();
						compition1.setCompititionName("NA");
						compition1.setOccupancy(0.0);
						comp.add(compition1);
					}

				}
				/*
				 * if (comp.size() == 0) { CompitionMaster compition = new CompitionMaster();
				 * compition.setCompititionName("NA"); compition.setOccupancy(0.0);
				 * 
				 * CompitionMaster compition1 = new CompitionMaster();
				 * compition1.setCompititionName("NA"); compition1.setOccupancy(0.0);
				 * 
				 * CompitionMaster compition2 = new CompitionMaster();
				 * compition2.setCompititionName("NA"); compition2.setOccupancy(0.0);
				 * comp.add(compition2); comp.add(compition); comp.add(compition1); }
				 */
			}
			map.put("compitionMaster", comp);

		} catch (Exception e) {
			log.error("error while fetching compition master within 10 kilomter...");
		}
		return map;
	}

	@Override
	public Map<String, Object> getPostSigningCostComparison(ProjectDetails projectDetails) {
		
		Map<String, Object> postSigningCost = new HashMap<>();
		
		ProjectDetails preSignProjectDetail = projectDetailsDao
				.findByReportIdVersionAndReportIdSubVersion(projectDetails.getReportIdVersion(), 1);
		ProjectCostSummary preSignProjectCostSummary = projectCostSummaryDao.findByParentId(preSignProjectDetail.getId());
		
		ProjectDetails postSignProjectDetail = projectDetailsDao
				.findByReportIdVersionAndReportIdSubVersion(projectDetails.getReportIdVersion(), 2);
		ProjectCostSummary postSignProjectCostSummary = projectCostSummaryDao.findByParentId(postSignProjectDetail.getId());
		
		List<ProjectCostData> preSignProjectCostData = preSignProjectCostSummary.getProjectCostDataList();
		for (ProjectCostData projectCostData : preSignProjectCostData) {
			if(projectCostData.getDescription().replaceAll("\\W", "").contains("Contingency")){
				postSigningCost.put("preSign" + projectCostData.getDescription().replaceAll("\\W", "").replaceAll("[0-9]",""), projectCostData.getCost() != null ? projectCostData.getCost() : 0);
			}else {
				postSigningCost.put("preSign" + projectCostData.getDescription().replaceAll("\\W", ""), projectCostData.getCost() != null ? projectCostData.getCost() : 0);
			}
		}
		
		List<ProjectCostData> postSignProjectCostData = postSignProjectCostSummary.getProjectCostDataList();
		for (ProjectCostData projectCostData : postSignProjectCostData) {
			if(projectCostData.getDescription().replaceAll("\\W", "").contains("Contingency")){
				postSigningCost.put("postSign" + projectCostData.getDescription().replaceAll("\\W", "").replaceAll("[0-9]",""), projectCostData.getCost() != null ? projectCostData.getCost() : 0);
			}else {
				postSigningCost.put("postSign" + projectCostData.getDescription().replaceAll("\\W", ""), projectCostData.getCost() != null ? projectCostData.getCost() : 0);
			}
		}
		
		return postSigningCost;
	}

	@Override
	public Map<String, Object> getPreHandoverCostComparison(ProjectDetails projectDetails) {
		// TODO Auto-generated method stub  
		Map<String, Object> preHandoverCost = new HashMap<>();
		
		ProjectDetails preSignProjectDetail = projectDetailsDao
				.findByReportIdVersionAndReportIdSubVersion(projectDetails.getReportIdVersion(), 1);
		ProjectCostSummary preSignProjectCostSummary = projectCostSummaryDao.findByParentId(preSignProjectDetail.getId());
		
		ProjectDetails postSignProjectDetail = projectDetailsDao
				.findByReportIdVersionAndReportIdSubVersion(projectDetails.getReportIdVersion(), 2);
		ProjectCostSummary postSignProjectCostSummary = projectCostSummaryDao.findByParentId(postSignProjectDetail.getId());
		
		ProjectDetails preHandProjectDetail = projectDetailsDao
				.findByReportIdVersionAndReportIdSubVersion(projectDetails.getReportIdVersion(), 3);
		ProjectCostSummary preHandProjectCostSummary = projectCostSummaryDao.findByParentId(preHandProjectDetail.getId());
		
		List<ProjectCostData> preSignProjectCostData = preSignProjectCostSummary.getProjectCostDataList();
		for (ProjectCostData projectCostData : preSignProjectCostData) {
//			log.info("preSign "+ projectCostData.getDescription().replaceAll("\\W", "") + " "+ projectCostData.getCost());
			if(projectCostData.getDescription().replaceAll("\\W", "").contains("Contingency")){
				preHandoverCost.put("preSign" + projectCostData.getDescription().replaceAll("\\W", "").replaceAll("[0-9]",""), projectCostData.getCost() != null ? projectCostData.getCost() : 0);
			}else { 
			    preHandoverCost.put("preSign" + projectCostData.getDescription().replaceAll("\\W", ""), projectCostData.getCost() != null ? projectCostData.getCost() : 0);
			}
		} 
		
		List<ProjectCostData> postSignProjectCostData = postSignProjectCostSummary.getProjectCostDataList();
		for (ProjectCostData projectCostData : postSignProjectCostData) {
//			log.info("postSign "+ projectCostData.getDescription().replaceAll("\\W", "") + " "+ projectCostData.getCost());
			if(projectCostData.getDescription().replaceAll("\\W", "").contains("Contingency")){
				preHandoverCost.put("postSign" + projectCostData.getDescription().replaceAll("\\W", "").replaceAll("[0-9]",""), projectCostData.getCost() != null ? projectCostData.getCost() : 0);
			}else {
				preHandoverCost.put("postSign" + projectCostData.getDescription().replaceAll("\\W", ""), projectCostData.getCost() != null ? projectCostData.getCost() : 0);
			}	
		}
		
		List<ProjectCostData> preHandProjectCostData = preHandProjectCostSummary.getProjectCostDataList();
		for (ProjectCostData projectCostData : preHandProjectCostData) {
//			log.info("preHand "+ projectCostData.getDescription().replaceAll("\\W", "") + " "+ projectCostData.getCost());
			if(projectCostData.getDescription().replaceAll("\\W", "").contains("Contingency")){
				preHandoverCost.put("preHand" + projectCostData.getDescription().replaceAll("\\W", "").replaceAll("[0-9]",""), projectCostData.getCost() != null ? projectCostData.getCost() : 0);
			}else {
				preHandoverCost.put("preHand" + projectCostData.getDescription().replaceAll("\\W", ""), projectCostData.getCost() != null ? projectCostData.getCost() : 0);
			}
		}
		
		return preHandoverCost;
	}

}
