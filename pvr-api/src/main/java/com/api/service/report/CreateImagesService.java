package com.api.service.report;

import java.util.List;
import java.util.Map;

import com.common.models.CinemaMaster;
import com.common.models.CompitionMaster;
import com.common.models.ProjectDetails;

public interface CreateImagesService {
	void createImagesDevelopmentDetails(ProjectDetails projectDetails, String pdfImagePath);

	void createImagesExistingCinemaLandScape(ProjectDetails projectDetails, String pdfImagePath,
			Map<String, List<CinemaMaster>> cinemaMaster, Map<String, List<CompitionMaster>> map);

	void createImagesUpcomingCinemaLandScape(ProjectDetails projectDetails, String projectId, String pdfImagePath);

	void createImagesCinemaLandScapeInNext5Years(ProjectDetails projectDetails, String string,
			Map<String, List<CinemaMaster>> existingCinemaLandScape,
			Map<String, List<CompitionMaster>> compitionMasterByProjectId);
}
