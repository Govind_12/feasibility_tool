package com.api.service.report;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.common.models.ProjectDetails;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExternalFileuploadServiceImpl implements ExternalFileuploadService {

	@Value("${path.fileUploadPath}")
	private String fileUploadPath;

	@Override
	public void fileUploadProposition(ProjectDetails projectDetails, String outputPath) {

		/*
		 * File afile = new
		 * File("/home/orange/Downloads/upload/step_1_N-191202014_ROHIT RAJ (SEPT'18).pdf"
		 * ); File bfile = new
		 * File("/home/orange/Documents/upload/step_1_N-191202014_ROHIT RAJ (SEPT'18).pdf"
		 * );
		 */

		File source = new File(fileUploadPath + File.separator + projectDetails.getFileURLStep1());
		File dest = new File(outputPath + File.separator + projectDetails.getFileURLStep1());
		try {
			Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching file upload proposition ..");
		}
	}

	@Override
	public void fileUploadPropositionStep2(ProjectDetails projectDetails, String outPath) {

		File source = new File(fileUploadPath + File.separator + projectDetails.getFileURLStep2());
		File dest = new File(outPath + File.separator + projectDetails.getFileURLStep2());
		try {
			Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching file upload proposition ..");
		}
	}

	@Override
	public void fileUploadPropositionStep5(ProjectDetails projectDetails, String outPath) {

		File source = new File(fileUploadPath + File.separator + projectDetails.getFileURLStep5());
		File dest = new File(outPath + File.separator + projectDetails.getFileURLStep5());
		try {
			Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching file upload proposition ..");
		}
	}

}
