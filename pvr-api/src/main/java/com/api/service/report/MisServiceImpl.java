package com.api.service.report;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.api.dao.BbdoMasterDao;
import com.api.dao.CinemaMasterDao;
import com.api.services.ProjectDetailsService;
import com.api.services.UserService;
import com.common.constants.Constants;
import com.common.mappers.MisMapper;
import com.common.models.BbdoMaster;
import com.common.models.CinemaFormat;
import com.common.models.CinemaMaster;
import com.common.models.ProjectDetails;
import com.common.models.User;

@Service("misService")
public class MisServiceImpl implements MisService {

	@Autowired
	private ProjectDetailsService detailsService;
	@Autowired
	private BbdoMasterDao bbdoaMaster;
	@Autowired
	private CinemaMasterDao cinemaMasterDao;
	@Autowired
	private TemplateDataService templateDataService;

	@Autowired
	private UserService userService;

	public List<MisMapper> getMisData() {

		List<MisMapper> mappersList = new ArrayList<>();

		List<ProjectDetails> projectDetailsList = detailsService.fetchAllProjectDetails();
		if (!ObjectUtils.isEmpty(projectDetailsList)) {
			for (ProjectDetails projectDetails : projectDetailsList) {
				MisMapper mapper = new MisMapper();
				// PROJECT DETAILS
				mapper.setReportId(projectDetails.getReportId());
				mapper.setProjectName(projectDetails.getProjectName());
				mapper.setDateOfSendForApproval(projectDetails.getDateOfSendForApproval());
				mapper.setDateOfHandover(projectDetails.getDateOfHandover());
				mapper.setFeasibilityState(projectDetails.getFeasibilityState());
				mapper.setDateOfOpening(projectDetails.getDateOfOpening());
				// DEVELOPMENT DETAILS
				mapper.setProjectLocation(projectDetails.getProjectLocation());
				mapper.setCityPopulation(getPopulation(projectDetails.getProjectLocation().getCity()) != null
						? getPopulation(projectDetails.getProjectLocation().getCity())
						: "");
				mapper.setTotalDeveloperSize(projectDetails.getTotalDeveloperSize());
				mapper.setRetailAreaOfProject(projectDetails.getRetailAreaOfProject());
				mapper.setProjectType(projectDetails.getProjectType());
				mapper.setTenancyMix(projectDetails.getTenancyMix());
				mapper.setUpcomingInfrastructureDevlopment(projectDetails.getUpcomingInfrastructureDevlopment());
				// DEVELOPER DETAILS
				mapper.setDeveloper(projectDetails.getDeveloper());
				mapper.setDeveloperOwner(projectDetails.getDeveloperOwner());
				mapper.setGrossTurnOver(projectDetails.getGrossTurnOver());
				mapper.setProjectDelivered1(projectDetails.getProjectDelivered1());
				mapper.setProjectDelivered2(projectDetails.getProjectDelivered2());
				mapper.setProjectDelivered3(projectDetails.getProjectDelivered3());
				mapper.setPvrProjectDelivered(getProjectsDelivered(projectDetails.getPvrProjectDelivered()));
				mapper.setPvrProjectPipeline(projectDetails.getPvrProjectPipeline());
				mapper.setPvrProperties(projectDetails.getPvrProperties());
				mapper.setRetailProjectDelivered(projectDetails.getRetailProjectDelivered());
				mapper.setCurrentProjectFinancing(projectDetails.getCurrentProjectFinancing());

				// CINEMA SPECS
				mapper.setNoOfScreens(projectDetails.getNoOfScreens());
				List<CinemaFormat> cinemaFormat = projectDetails.getCinemaFormat();
				int mainstreamSum = 0;
				int directorSum = 0;
				int goldSum = 0;
				int imaxSum = 0;
				int pxlSum = 0;
				int dxSum = 0;
				int plSum = 0;
				int onyxSum = 0;

				if (!ObjectUtils.isEmpty(cinemaFormat)) {

					for (CinemaFormat cinema : cinemaFormat) {
						if (cinema.getFormatType().equalsIgnoreCase(Constants.MAINSTREAM)) {
							mainstreamSum = mainstreamSum + cinema.getTotal();
						}
						if (cinema.getFormatType().equalsIgnoreCase(Constants.ULTRAPREMIUM)) {
							directorSum = directorSum + cinema.getTotal();
						}
						if (cinema.getFormatType().equalsIgnoreCase(Constants.GOLD)) {
							goldSum = goldSum + cinema.getTotal();
						}
						if (cinema.getFormatType().equalsIgnoreCase(Constants.IMAX)) {
							imaxSum = imaxSum + cinema.getTotal();
						}
						if (cinema.getFormatType().equalsIgnoreCase(Constants.PXL)) {
							pxlSum = pxlSum + cinema.getTotal();
						}
						if (cinema.getFormatType().equalsIgnoreCase(Constants.DX)) {
							dxSum = dxSum + cinema.getTotal();
						}
						if (cinema.getFormatType().equalsIgnoreCase(Constants.PLAYHOUSE)) {
							plSum = plSum + cinema.getTotal();
						}
						if (cinema.getFormatType().equalsIgnoreCase(Constants.ONYX)) {
							onyxSum = onyxSum + cinema.getTotal();
						}
					}
				}

				if (mainstreamSum > 0) {
					mapper.setMainstream(mainstreamSum);
				} else {
					mapper.setMainstream("");
				}
				if (directorSum > 0) {
					mapper.setDirectorsCut(directorSum);
				} else {
					mapper.setDirectorsCut("");
				}
				if (goldSum > 0) {
					mapper.setGold(goldSum);
				} else {
					mapper.setGold("");
				}
				if (imaxSum > 0) {
					mapper.setImax(imaxSum);
				} else {
					mapper.setImax("");
				}
				if (pxlSum > 0) {
					mapper.setPxl(pxlSum);
				} else {
					mapper.setPxl("");
				}
				if (dxSum > 0) {
					mapper.setDx(dxSum);
				} else {
					mapper.setDx("");
				}
				if (plSum > 0) {
					mapper.setPlayhouse(plSum);
				} else {
					mapper.setPlayhouse("");
				}
				if (onyxSum > 0) {
					mapper.setOnyx(onyxSum);
				} else {
					mapper.setOnyx("");
				}

				mapper.setOthers("");

				mapper.setTotalColumnValSum(projectDetails.getTotalColumnValSum());

				// CAPEX
				mapper.setProjectCost(projectDetails.getProjectCost());
				mapper.setSecurityDepositForMonthCam(
						projectDetails.getLesserLesse().getSecurityDepositForMonthCam() != null
								? projectDetails.getLesserLesse().getSecurityDepositForMonthCam()
								: "");
				mapper.setSecurityDepositForMonthRent(
						projectDetails.getLesserLesse().getSecurityDepositForMonthRent() != null
								? projectDetails.getLesserLesse().getSecurityDepositForMonthRent()
								: "");
				mapper.setCamSecurityDepositForFixedZeroMg(
						projectDetails.getLesserLesse().getCamSecurityDepositForFixedZeroMg() != null
								? projectDetails.getLesserLesse().getCamSecurityDepositForFixedZeroMg()
								: "");
				mapper.setRentSecurityDepositForFixedZeroMg(
						projectDetails.getLesserLesse().getRentSecurityDepositForFixedZeroMg() != null
								? projectDetails.getLesserLesse().getRentSecurityDepositForFixedZeroMg()
								: "");
				mapper.setProjectCostPerScreen(projectDetails.getProjectCostPerScreen());
				Map<String, Object> irr = templateDataService.getIrrCalulationById(projectDetails);
				mapper.setSecurityCapex(irr.get("securityCapex"));
				mapper.setSecurityDeposit(irr.get("securityDeposit"));

				// KEY LEASE TERMS
				mapper.setLeasePeriod(projectDetails.getLesserLesse().getLeasePeriod());
				mapper.setPvrLockInPeriod(projectDetails.getLesserLesse().getPvrLockInPeriod());
				mapper.setDevloperLockedInPeriod(projectDetails.getLesserLesse().getDevloperLockedInPeriod());
				mapper.setLeasableArea(projectDetails.getLesserLesse().getLeasableArea());
				mapper.setLoadingPercentage(projectDetails.getLesserLesse().getLoadingPercentage());

				mapper.setCarpetArea(projectDetails.getLesserLesse().getCarpetArea());
				mapper.setRentAgreement(projectDetails.getRentTerm().getExplainRentAgreement());
				mapper.setCamAgreement(projectDetails.getCamTerm().getExplainCamAgreement());
				mapper.setRentPerSqFt(
						projectDetails.getRentTerm().getRent() != null ? projectDetails.getRentTerm().getRent() : "");
				mapper.setCamPerSqFt(
						projectDetails.getCamTerm().getCamRate() != null ? projectDetails.getCamTerm().getCamRate()
								: "");
				mapper.setBoxOfficePercentage(projectDetails.getRentTerm().getBoxOfficePercentage() != null
						? projectDetails.getRentTerm().getBoxOfficePercentage()
						: "");
				mapper.setFAndBPercentage(projectDetails.getRentTerm().getFAndBPercentage() != null
						? projectDetails.getRentTerm().getFAndBPercentage()
						: "");
				mapper.setAdSalePercentage(projectDetails.getRentTerm().getAdSalePercentage() != null
						? projectDetails.getRentTerm().getAdSalePercentage()
						: "");
				mapper.setOthersPercentage(projectDetails.getRentTerm().getOthersPercentage() != null
						? projectDetails.getRentTerm().getOthersPercentage()
						: "");
				mapper.setCarpetAreaPerSeat(irr.get("carpetAreaPerSeat"));

				// OPERATING ASSUMPTIONS

				mapper.setAtp(irr.get("atp"));
				mapper.setSph(irr.get("sph"));
				mapper.setAdmits(irr.get("admits"));
				mapper.setOccupancy(irr.get("occupancy"));
				mapper.setAtpSphRatio(irr.get("atpSphRatio"));

				// FINANCIALS

				mapper.setRevenue(irr.get("revenue"));
				mapper.setNetSaleOfTickets(irr.get("netSaleOfTickets"));
				mapper.setSalseOfFnB(irr.get("salseOfFnB"));
				mapper.setAdRevenue(irr.get("adRevenue"));
				mapper.setVasIncome(irr.get("vasIncome"));
				mapper.setOtherOperatingIncome(irr.get("otherOperatingIncome"));
				mapper.setEbitda(irr.get("ebitda"));
				mapper.setEbitdaMargin(irr.get("ebitdaMargin"));
				mapper.setFilmDistributorShare(irr.get("filmDistributorShare"));
				mapper.setFnbConsumption(irr.get("fnbConsumption"));
				mapper.setGrossMargin(irr.get("grossMargin"));
				mapper.setPersonnelExpenses(irr.get("personnelExpenses"));
				mapper.setRent(irr.get("rent"));
				mapper.setCam(irr.get("cam"));
				mapper.setElectricityWater(irr.get("electricityWater"));
				mapper.setRm(irr.get("rm"));
				mapper.setOtherOperatingExpense(irr.get("otherOperatingExpense"));
				mapper.setTotalFixedExp(irr.get("totalFixedExp"));

				// RETURN METRICES
				mapper.setEbitdaPaybackPeriod(irr.get("paybackPeriod"));
				mapper.setPostTaxIrr(irr.get("projectIrrPostTax"));

				mappersList.add(mapper);
			}
		}

		return mappersList;
	}

	public Object getPopulation(String city) {
		BbdoMaster bbdoMaster = bbdoaMaster.findByTown(city);

		if (!ObjectUtils.isEmpty(bbdoMaster))
			return bbdoMaster.getPopn();
		else
			return null;
	}

	public List<String> getProjectsDelivered(List<Integer> pvrProjects) {
		List<CinemaMaster> pvrProjectsDeliver = new ArrayList<>();
		List<String> cinema = new ArrayList<>();

		if (!ObjectUtils.isEmpty(pvrProjects)) {
			if (pvrProjects.size() > 0) {
				for (Integer integer : pvrProjects) {
					pvrProjectsDeliver.add(cinemaMasterDao.findByHopk(integer));
				}
			}

			pvrProjectsDeliver.forEach(a -> {
				cinema.add(a.getCinema_name());
			});
			return cinema;
		}
		return null;
	}

	public List<MisMapper> getOpsOrExtMisData(String misType) {
		List<MisMapper> mappersList = new ArrayList<>();
		List<ProjectDetails> projectDetailsList = this.detailsService.fetchAllOpsOrExtProjectDetails(misType);
		if (!ObjectUtils.isEmpty(projectDetailsList))
			for (ProjectDetails projectDetails : projectDetailsList) {
				MisMapper mapper = new MisMapper();
				mapper.setCreatedBy(this.userService.get(String.valueOf(projectDetails.getProjectBdId())).getUsername());
				mapper.setReportId(projectDetails.getReportId());
				mapper.setProjectName(projectDetails.getProjectName());
				mapper.setDateOfSendForApproval(projectDetails.getDateOfSendForApproval());
				mapper.setDateOfHandover(projectDetails.getDateOfHandover());
				mapper.setDateOfOpening(projectDetails.getDateOfOpening());
				mapper.setFeasibilityState(projectDetails.getFeasibilityState());
				mapper.setProjectLocation(projectDetails.getProjectLocation());
				mapper.setCityPopulation((getPopulation(projectDetails.getProjectLocation().getCity()) != null)
						? getPopulation(projectDetails.getProjectLocation().getCity())
						: "");
				mapper.setTotalDeveloperSize(projectDetails.getTotalDeveloperSize());
				mapper.setRetailAreaOfProject(projectDetails.getRetailAreaOfProject());
				mapper.setProjectType(projectDetails.getProjectType());
				mapper.setTenancyMix(projectDetails.getTenancyMix());
				mapper.setUpcomingInfrastructureDevlopment(projectDetails.getUpcomingInfrastructureDevlopment());
				mapper.setDeveloper(projectDetails.getDeveloper());
				mapper.setDeveloperOwner(projectDetails.getDeveloperOwner());
				mapper.setGrossTurnOver(projectDetails.getGrossTurnOver());
				mapper.setProjectDelivered1(projectDetails.getProjectDelivered1());
				mapper.setProjectDelivered2(projectDetails.getProjectDelivered2());
				mapper.setProjectDelivered3(projectDetails.getProjectDelivered3());
				mapper.setPvrProjectDelivered(getProjectsDelivered(projectDetails.getPvrProjectDelivered()));
				mapper.setPvrProjectPipeline(projectDetails.getPvrProjectPipeline());
				mapper.setPvrProperties(projectDetails.getPvrProperties());
				mapper.setRetailProjectDelivered(projectDetails.getRetailProjectDelivered());
				mapper.setCurrentProjectFinancing(projectDetails.getCurrentProjectFinancing());
				mapper.setNoOfScreens(projectDetails.getNoOfScreens());
				List<CinemaFormat> cinemaFormat = projectDetails.getCinemaFormat();
				int mainstreamSum = 0;
				int directorSum = 0;
				int goldSum = 0;
				int imaxSum = 0;
				int pxlSum = 0;
				int dxSum = 0;
				int plSum = 0;
				int onyxSum = 0;
				if (!ObjectUtils.isEmpty(cinemaFormat))
					for (CinemaFormat cinema : cinemaFormat) {
						if (cinema.getFormatType().equalsIgnoreCase("mainstream"))
							mainstreamSum += cinema.getTotal().intValue();
						if (cinema.getFormatType().equalsIgnoreCase("ultrapremium"))
							directorSum += cinema.getTotal().intValue();
						if (cinema.getFormatType().equalsIgnoreCase("gold"))
							goldSum += cinema.getTotal().intValue();
						if (cinema.getFormatType().equalsIgnoreCase("imax"))
							imaxSum += cinema.getTotal().intValue();
						if (cinema.getFormatType().equalsIgnoreCase("pxl"))
							pxlSum += cinema.getTotal().intValue();
						if (cinema.getFormatType().equalsIgnoreCase("4dx"))
							dxSum += cinema.getTotal().intValue();
						if (cinema.getFormatType().equalsIgnoreCase("playhouse"))
							plSum += cinema.getTotal().intValue();
						if (cinema.getFormatType().equalsIgnoreCase("onyx"))
							onyxSum += cinema.getTotal().intValue();
					}
				if (mainstreamSum > 0) {
					mapper.setMainstream(Integer.valueOf(mainstreamSum));
				} else {
					mapper.setMainstream("");
				}
				if (directorSum > 0) {
					mapper.setDirectorsCut(Integer.valueOf(directorSum));
				} else {
					mapper.setDirectorsCut("");
				}
				if (goldSum > 0) {
					mapper.setGold(Integer.valueOf(goldSum));
				} else {
					mapper.setGold("");
				}
				if (imaxSum > 0) {
					mapper.setImax(Integer.valueOf(imaxSum));
				} else {
					mapper.setImax("");
				}
				if (pxlSum > 0) {
					mapper.setPxl(Integer.valueOf(pxlSum));
				} else {
					mapper.setPxl("");
				}
				if (dxSum > 0) {
					mapper.setDx(Integer.valueOf(dxSum));
				} else {
					mapper.setDx("");
				}
				if (plSum > 0) {
					mapper.setPlayhouse(Integer.valueOf(plSum));
				} else {
					mapper.setPlayhouse("");
				}
				if (onyxSum > 0) {
					mapper.setOnyx(Integer.valueOf(onyxSum));
				} else {
					mapper.setOnyx("");
				}
				mapper.setOthers("");
				mapper.setTotalColumnValSum(projectDetails.getTotalColumnValSum());
				mapper.setProjectCost(projectDetails.getProjectCost());
				mapper.setSecurityDepositForMonthCam(
						(projectDetails.getLesserLesse().getSecurityDepositForMonthCam() != null)
								? projectDetails.getLesserLesse().getSecurityDepositForMonthCam()
								: "");
				mapper.setSecurityDepositForMonthRent(
						(projectDetails.getLesserLesse().getSecurityDepositForMonthRent() != null)
								? projectDetails.getLesserLesse().getSecurityDepositForMonthRent()
								: "");
				mapper.setCamSecurityDepositForFixedZeroMg(
						(projectDetails.getLesserLesse().getCamSecurityDepositForFixedZeroMg() != null)
								? projectDetails.getLesserLesse().getCamSecurityDepositForFixedZeroMg()
								: "");
				mapper.setRentSecurityDepositForFixedZeroMg(
						(projectDetails.getLesserLesse().getRentSecurityDepositForFixedZeroMg() != null)
								? projectDetails.getLesserLesse().getRentSecurityDepositForFixedZeroMg()
								: "");
				mapper.setProjectCostPerScreen(projectDetails.getProjectCostPerScreen());
				Map<String, Object> irr = this.templateDataService.getIrrCalulationById(projectDetails);
				mapper.setSecurityCapex((irr != null) ? irr.get("securityCapex") : Double.valueOf(0.0D));
				mapper.setSecurityDeposit((irr != null) ? irr.get("securityDeposit") : Double.valueOf(0.0D));
				mapper.setLeasePeriod(projectDetails.getLesserLesse().getLeasePeriod());
				mapper.setPvrLockInPeriod(projectDetails.getLesserLesse().getPvrLockInPeriod());
				mapper.setDevloperLockedInPeriod(projectDetails.getLesserLesse().getDevloperLockedInPeriod());
				mapper.setLeasableArea(projectDetails.getLesserLesse().getLeasableArea());
				mapper.setLoadingPercentage(projectDetails.getLesserLesse().getLoadingPercentage());
				mapper.setCarpetArea(projectDetails.getLesserLesse().getCarpetArea());
				mapper.setRentAgreement(projectDetails.getRentTerm().getExplainRentAgreement());
				mapper.setCamAgreement(projectDetails.getCamTerm().getExplainCamAgreement());
				mapper.setRentPerSqFt(
						(projectDetails.getRentTerm().getRent() != null) ? projectDetails.getRentTerm().getRent() : "");
				mapper.setCamPerSqFt(
						(projectDetails.getCamTerm().getCamRate() != null) ? projectDetails.getCamTerm().getCamRate()
								: "");
				mapper.setBoxOfficePercentage((projectDetails.getRentTerm().getBoxOfficePercentage() != null)
						? projectDetails.getRentTerm().getBoxOfficePercentage()
						: "");
				mapper.setFAndBPercentage((projectDetails.getRentTerm().getFAndBPercentage() != null)
						? projectDetails.getRentTerm().getFAndBPercentage()
						: "");
				mapper.setAdSalePercentage((projectDetails.getRentTerm().getAdSalePercentage() != null)
						? projectDetails.getRentTerm().getAdSalePercentage()
						: "");
				mapper.setOthersPercentage((projectDetails.getRentTerm().getOthersPercentage() != null)
						? projectDetails.getRentTerm().getOthersPercentage()
						: "");
				mapper.setCarpetAreaPerSeat((irr != null) ? irr.get("carpetAreaPerSeat") : Integer.valueOf(0));
				mapper.setAtp((irr != null) ? irr.get("atp") : Integer.valueOf(0));
				mapper.setSph((irr != null) ? irr.get("sph") : Integer.valueOf(0));
				mapper.setAdmits((irr != null) ? irr.get("admits") : Integer.valueOf(0));
				mapper.setOccupancy((irr != null) ? irr.get("occupancy") : Integer.valueOf(0));
				mapper.setAtpSphRatio((irr != null) ? irr.get("atpSphRatio") : Integer.valueOf(0));
				mapper.setRevenue((irr != null) ? irr.get("revenue") : Integer.valueOf(0));
				mapper.setNetSaleOfTickets((irr != null) ? irr.get("netSaleOfTickets") : Integer.valueOf(0));
				mapper.setSalseOfFnB((irr != null) ? irr.get("salseOfFnB") : Integer.valueOf(0));
				mapper.setAdRevenue((irr != null) ? irr.get("adRevenue") : Integer.valueOf(0));
				mapper.setVasIncome((irr != null) ? irr.get("vasIncome") : Integer.valueOf(0));
				mapper.setOtherOperatingIncome((irr != null) ? irr.get("otherOperatingIncome") : Integer.valueOf(0));
				mapper.setEbitda((irr != null) ? irr.get("ebitda") : Integer.valueOf(0));
				mapper.setEbitdaMargin((irr != null) ? irr.get("ebitdaMargin") : Integer.valueOf(0));
				mapper.setFilmDistributorShare((irr != null) ? irr.get("filmDistributorShare") : Integer.valueOf(0));
				mapper.setFnbConsumption((irr != null) ? irr.get("fnbConsumption") : Integer.valueOf(0));
				mapper.setGrossMargin((irr != null) ? irr.get("grossMargin") : Integer.valueOf(0));
				mapper.setPersonnelExpenses((irr != null) ? irr.get("personnelExpenses") : Integer.valueOf(0));
				mapper.setRent((irr != null) ? irr.get("rent") : Integer.valueOf(0));
				mapper.setCam((irr != null) ? irr.get("cam") : Integer.valueOf(0));
				mapper.setElectricityWater((irr != null) ? irr.get("electricityWater") : Integer.valueOf(0));
				mapper.setRm((irr != null) ? irr.get("rm") : Integer.valueOf(0));
				mapper.setOtherOperatingExpense((irr != null) ? irr.get("otherOperatingExpense") : Integer.valueOf(0));
				mapper.setTotalFixedExp((irr != null) ? irr.get("totalFixedExp") : Integer.valueOf(0));
				mapper.setEbitdaPaybackPeriod((irr != null) ? irr.get("paybackPeriod") : Integer.valueOf(0));
				mapper.setPostTaxIrr((irr != null) ? irr.get("projectIrrPostTax") : Integer.valueOf(0));
				mappersList.add(mapper);
			}
		return mappersList;
	}

}
