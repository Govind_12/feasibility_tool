package com.api.service.report;

public interface CompitionDataService {

	Long getAllSeatCapacityByHopk(String compitionName);
	Integer getAllAtpByHopk(String compitionName);
	Long getAllAdmitsByHopk(String compitionName);
	Double getAllOccupancyByHopk(String compitionName);
	Long getAllSphByHopk(String compitionName);
}
