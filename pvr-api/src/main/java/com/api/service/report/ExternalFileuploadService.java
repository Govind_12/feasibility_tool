package com.api.service.report;

import com.common.models.ProjectDetails;

public interface ExternalFileuploadService {

void fileUploadProposition(ProjectDetails projectDetails,String outPath);
void fileUploadPropositionStep2(ProjectDetails projectDetails,String outPath);
void fileUploadPropositionStep5(ProjectDetails projectDetails,String outPath);
}
