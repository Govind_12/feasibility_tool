package com.api.service.report;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.dao.sql.CinemaAudiSeatMasterDao;
import com.api.dao.sql.CinemaPerformanceDao;
import com.api.dao.sql.SeatCategoryDao;
import com.api.dao.sql.TicketOnlineOfflineDao;
import com.api.utils.DateUtil;
import com.common.constants.Constants;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RevenueDataServiceImpl implements RevenueDataService {

	@Autowired
	private CinemaAudiSeatMasterDao cinemaAudiSeatMasterDao;

	@Autowired
	private SeatCategoryDao seatCategoryDao;

	@Autowired
	private RevenueDataService revenueDataService;

	@Autowired
	private CinemaPerformanceDao cinemaPerformanceDao;

	@Autowired
	private TicketOnlineOfflineDao ticketOnlineOfflineDao;

	@Override
	public Long getAllSeatCapacityByHopk(String hopk) {
		Long seatCapacity = 0l;
		try {

			List<String> seatCapacityByHopk = cinemaAudiSeatMasterDao.findAllByCinemaHopkAndDate(hopk);
			seatCapacity = seatCapacityByHopk.stream().map(Long::valueOf).mapToLong(Long::longValue).sum();
			//System.out.println(seatCapacity);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching seat capacity by hopk...");
		}
		return seatCapacity;
	}

	@Override
	public Long getAllAtpByHopk(Integer hopk) {
		Long atp = 0l;
		try {

			/*List<Object[]> seatCategory = seatCategoryDao.findAllByGbocAndAdmitsHopk(hopk,
					DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
					DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
			
			List<Object[]> seatCategory = seatCategoryDao.findAllByGbocAndAdmitsHopk(hopk);
			

			for (Object[] atpDetails : seatCategory) {

				Integer totalAdmits = (Integer) atpDetails[Constants.ZERO];
				BigDecimal totalGboc = (BigDecimal) atpDetails[Constants.ONE];

				if (totalAdmits != null && totalGboc != null) {
					atp = Math.round((double) totalGboc.doubleValue() / (double) totalAdmits);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching atp by hopk...");
		}
		return atp;
	}

	@Override
	public Long getAllAdmitsByHopk(Integer hopk) {
		Integer totalAdmits = 0;
		Long admit = 0l;
		try {

			/*List<Object[]> seatCategory = seatCategoryDao.findAllByGbocAndAdmitsHopk(hopk,
					DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
					DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
			
			
			List<Object[]> seatCategory = seatCategoryDao.findAllByGbocAndAdmitsHopk(hopk);

			for (Object[] arr : seatCategory) {
				totalAdmits = (Integer) arr[Constants.ZERO];
				if (totalAdmits != null) {
					admit = Long.valueOf(totalAdmits.longValue());
				}
			}

		} catch (Exception e) {
			log.error("error while fetching admits..");
		}
		return admit;
	}

	@Override
	public Double getAllOccupancyByHopk(Integer hopk) {
		double occupancy = 0.0;
		try {

			/*List<Object[]> occupancyByHopk = seatCategoryDao.findAllByOccupancysHopkAndAdmit(hopk,
					DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
					DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
			
			List<Object[]> occupancyByHopk = seatCategoryDao.findAllByOccupancysHopkAndAdmit(hopk);
			

			for (Object[] occupancydata : occupancyByHopk) {

				Integer totalAdmits = (Integer) occupancydata[Constants.ZERO];
				Integer totalSeats = (Integer) occupancydata[Constants.ONE];

				if (totalAdmits != null && totalSeats != null) {
					occupancy = (double) totalAdmits / (double) totalSeats;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching occupancy..");
		}

		return Math.round((occupancy * 100) * 10) / 10.0;
	}

	@Override
	public Long getAllSphByHopk(Integer hopk) {
		Long sph = 0l;
		try {
			/*List<Object[]> occupancyByHopk = cinemaPerformanceDao.findAllCinemaPerformanceByHopkAndDate(hopk,
					DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
					DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE));*/
			
			List<Object[]> occupancyByHopk = cinemaPerformanceDao.findAllCinemaPerformanceByHopkAndDate(hopk);
			

			for (Object[] occupancydata : occupancyByHopk) {

				BigDecimal totalConcession = (BigDecimal) occupancydata[Constants.ZERO];
				Integer totalAdmits = (Integer) occupancydata[Constants.ONE];

				if (totalConcession != null && totalAdmits != null) {
					sph = totalConcession.longValue() / totalAdmits;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching sph..");
		}
		return sph;
	}

	@Override
	public Double getAllWebSaleByHopk(Integer hopk) {
		Double finalAvg = 0.0;
		try {
			String channelGroupOnline = Constants.ONLINE;
			/*List<Integer> ob = ticketOnlineOfflineDao.avgAdmitsOnlineHopks(hopk,
					DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
					DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE), channelGroupOnline);*/
			
			List<Integer> ob = ticketOnlineOfflineDao.avgAdmitsOnlineHopks(hopk, channelGroupOnline);

			int onlineSum = ob.stream().mapToInt(mapper -> mapper).sum();

			String channelGroupOffline = Constants.OFFLINE;

			/*List<Integer> objectOffline = ticketOnlineOfflineDao.avgAdmitsOnlineHopks(hopk,
					DateUtil.getDateByCurrentQuater().get(Constants.STARTDATE),
					DateUtil.getDateByCurrentQuater().get(Constants.ENDDATE), channelGroupOffline);*/
			
			List<Integer> objectOffline = ticketOnlineOfflineDao.avgAdmitsOnlineHopks(hopk, channelGroupOffline);
			

			int offlineSum = objectOffline.stream().mapToInt(mapper -> mapper).sum();

			int onlineOffline = onlineSum + offlineSum;
			finalAvg = (double) onlineSum / onlineOffline;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching web sale..");
		}
		return finalAvg * 100;
	}
}
