package com.api.service.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.dao.CompitionMasterDao;
import com.common.models.CompitionMaster;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CompitionDataServiceImpl implements CompitionDataService {

	@Autowired
	private CompitionMasterDao compitionMasterDao;

	@Override
	public Long getAllSeatCapacityByHopk(String compitionName) {
		Long seatCapacity = 0l;
		try {

			List<CompitionMaster> compitionMasterByName = compitionMasterDao.findAllByCompititionName(compitionName);
			seatCapacity = compitionMasterByName.get(0).getSeatCapacity();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching seat capacity by compitionName...");
		}
		return seatCapacity;
	}

	@Override
	public Integer getAllAtpByHopk(String compitionName) {
		Integer atp = 0;
		try {
			List<CompitionMaster> compitionMasterByName = compitionMasterDao.findAllByCompititionName(compitionName);
			atp = compitionMasterByName.get(0).getAtp();
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while fetching atp by compition master...");
		}
		return atp;
	}

	@Override
	public Long getAllAdmitsByHopk(String compitionName) {
		Long admit = 0l;
		try {
			List<CompitionMaster> compitionMasterByName = compitionMasterDao.findAllByCompititionName(compitionName);
			admit = compitionMasterByName.get(0).getAdmits();
		} catch (Exception e) {
			log.error("error while fetching admits by compitionName");
		}
		return admit;
	}

	@Override
	public Double getAllOccupancyByHopk(String compitionName) {
		double occupancy = 0.0;
		try {
			List<CompitionMaster> compitionMasterByName = compitionMasterDao.findAllByCompititionName(compitionName);
			occupancy = compitionMasterByName.get(0).getOccupancy();
		} catch (Exception e) {
			log.error("error while fetching occupancy by compitionName..");
		}
		return occupancy;
	}

	@Override
	public Long getAllSphByHopk(String compitionName) {
		Long sph=0l;
		try {
			List<CompitionMaster> compitionMasterByName = compitionMasterDao.findAllByCompititionName(compitionName);
			/*sph = compitionMasterByName.get(0).gets;*/
		} catch (Exception e) {
			log.error("error while fetching sph by compitionName..");
		}
		return sph;
	}

}
