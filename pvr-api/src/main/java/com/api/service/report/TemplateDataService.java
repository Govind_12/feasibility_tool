package com.api.service.report;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.simple.parser.ParseException;

import com.common.mappers.KeyOperatingCostAssumtionsMapper;
import com.common.mappers.VpfIncomeMapper;
import com.common.models.AssumptionsValues;
import com.common.models.AtpAssumptions;
import com.common.models.CinemaMaster;
import com.common.models.CompitionMaster;
import com.common.models.ProjectDetails;
import com.common.sql.models.CinemaMasterSql;

public interface TemplateDataService {
	Map<String,ProjectDetails> getProjectDetails(ProjectDetails projectDetails);
	Map<String, List<CinemaMaster>> getExistingCinemaLandScape(ProjectDetails projectDetails);
	Map<String,ProjectDetails> getCinemaSpecs(ProjectDetails projectDetails);
	Map<String,Object> getIrrCalulationById(ProjectDetails projectDetails);
	Map<String,Object> getCatchmentDetailsId(ProjectDetails projectDetails);
	Map<String,Object> getKeyProjectCostAssumptionById(String projectId);
	Map<String,List<CompitionMaster>> getCompitionMasterByProjectId(ProjectDetails projectDetails);
	Map<String,List<Long>> getPvrFootprintrByProjectId(ProjectDetails projectDetails);
	Map<String,List<CinemaMaster>> getKeyRevenueAssumptionsOccupancy(ProjectDetails projectDetails);
	Map<String,List<AssumptionsValues>> getProjectDetailsOccupancy(ProjectDetails projectDetails);
	Map<String,List<CinemaMasterSql>> getKeyRevenueAssumptionsAdmits(String projectId,Optional<ProjectDetails> upcomingProjectDetails);
	Map<String,List<AtpAssumptions>> getKeyRevenueAssumptionsAtp(ProjectDetails projectDetails);
	Map<String,List<AssumptionsValues>> getProjectDetailsSph(ProjectDetails projectDetails);
	Map<String,List<KeyOperatingCostAssumtionsMapper>> getProjectDetailsOperatingAssumtion(ProjectDetails projectDetails);
	Map<String,VpfIncomeMapper> getVpfInComeDetails(ProjectDetails projectDetails);
	Map<String,List<Double>> getKeyAssumption(ProjectDetails projectDetails);
	Map<String,List<CompitionMaster>> getCompitionMasterByExistingCinemaMaster(ProjectDetails projectDetails);
	Map<String, Double> getAddressByLatLong(String address) throws IOException, ParseException;
	Map<String,Object> getPostSigningCostComparison(ProjectDetails projectDetails);
	Map<String,Object> getPreHandoverCostComparison(ProjectDetails projectDetails);
}
	