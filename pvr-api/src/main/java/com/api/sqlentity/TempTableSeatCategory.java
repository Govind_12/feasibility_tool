package com.api.sqlentity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "temp_table_seat_Category")
public class TempTableSeatCategory {
  @Column(name = "cinema_hopk")
  private Integer cinema_hopk;
  
  @Column(name = "Cinema_Name")
  private String Cinema_Name;
  
  @Column(name = "Cinema_state")
  private String Cinema_state;
  
  @Column(name = "Cinema_City")
  private String Cinema_City;
  
  @Column(name = "Cinema_Format")
  private String Cinema_Format;
  
  @Column(name = "Movie_show_Time")
  private Date Movie_show_Time;
  
  @Column(name = "CinOperator_strCode")
  private String CinOperator_strCode;
  
  @Column(name = "Audi_Number")
  private Integer Audi_Number;
  
  @Column(name = "Area_category")
  private String Area_category;
  
  @Column(name = "area_cat_Code")
  private String area_cat_Code;
  
  @Column(name = "Session_lngSessionId")
  private Integer Session_lngSessionId;
  
  @Column(name = "Film_strCode")
  private String Film_strCode;
  
  @Column(name = "Movie_Name")
  private String Movie_Name;
  
  @Column(name = "admits")
  private String admits;
  
  @Column(name = "GBOC")
  private String GBOC;
  
  @Column(name = "Total_Seats")
  private Integer Total_Seats;
  
  @Column(name = "finyear")
  private String finyear;
  
  @Column(name = "QTR")
  private String QTR;
  
  public void setCinema_hopk(Integer cinema_hopk) {
    this.cinema_hopk = cinema_hopk;
  }
  
  public void setCinema_Name(String Cinema_Name) {
    this.Cinema_Name = Cinema_Name;
  }
  
  public void setCinema_state(String Cinema_state) {
    this.Cinema_state = Cinema_state;
  }
  
  public void setCinema_City(String Cinema_City) {
    this.Cinema_City = Cinema_City;
  }
  
  public void setCinema_Format(String Cinema_Format) {
    this.Cinema_Format = Cinema_Format;
  }
  
  public void setMovie_show_Time(Date Movie_show_Time) {
    this.Movie_show_Time = Movie_show_Time;
  }
  
  public void setCinOperator_strCode(String CinOperator_strCode) {
    this.CinOperator_strCode = CinOperator_strCode;
  }
  
  public void setAudi_Number(Integer Audi_Number) {
    this.Audi_Number = Audi_Number;
  }
  
  public void setArea_category(String Area_category) {
    this.Area_category = Area_category;
  }
  
  public void setArea_cat_Code(String area_cat_Code) {
    this.area_cat_Code = area_cat_Code;
  }
  
  public void setSession_lngSessionId(Integer Session_lngSessionId) {
    this.Session_lngSessionId = Session_lngSessionId;
  }
  
  public void setFilm_strCode(String Film_strCode) {
    this.Film_strCode = Film_strCode;
  }
  
  public void setMovie_Name(String Movie_Name) {
    this.Movie_Name = Movie_Name;
  }
  
  public void setAdmits(String admits) {
    this.admits = admits;
  }
  
  public void setGBOC(String GBOC) {
    this.GBOC = GBOC;
  }
  
  public void setTotal_Seats(Integer Total_Seats) {
    this.Total_Seats = Total_Seats;
  }
  
  public void setFinyear(String finyear) {
    this.finyear = finyear;
  }
  
  public void setQTR(String QTR) {
    this.QTR = QTR;
  }
  
  public Integer getCinema_hopk() {
    return this.cinema_hopk;
  }
  
  public String getCinema_Name() {
    return this.Cinema_Name;
  }
  
  public String getCinema_state() {
    return this.Cinema_state;
  }
  
  public String getCinema_City() {
    return this.Cinema_City;
  }
  
  public String getCinema_Format() {
    return this.Cinema_Format;
  }
  
  public Date getMovie_show_Time() {
    return this.Movie_show_Time;
  }
  
  public String getCinOperator_strCode() {
    return this.CinOperator_strCode;
  }
  
  public Integer getAudi_Number() {
    return this.Audi_Number;
  }
  
  public String getArea_category() {
    return this.Area_category;
  }
  
  public String getArea_cat_Code() {
    return this.area_cat_Code;
  }
  
  public Integer getSession_lngSessionId() {
    return this.Session_lngSessionId;
  }
  
  public String getFilm_strCode() {
    return this.Film_strCode;
  }
  
  public String getMovie_Name() {
    return this.Movie_Name;
  }
  
  public String getAdmits() {
    return this.admits;
  }
  
  public String getGBOC() {
    return this.GBOC;
  }
  
  public Integer getTotal_Seats() {
    return this.Total_Seats;
  }
  
  public String getFinyear() {
    return this.finyear;
  }
  
  public String getQTR() {
    return this.QTR;
  }
}
