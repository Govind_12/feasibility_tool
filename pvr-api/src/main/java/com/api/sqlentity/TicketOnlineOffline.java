package com.api.sqlentity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Ticket_online_Offline")
public class TicketOnlineOffline {
  @Column(name = "cinema_hopk")
  private Integer cinema_hopk;
  
  @Column(name = "Session_Business_Date")
  private Date Session_Business_Date;
  
  @Column(name = "Channel_Group")
  private String Channel_Group;
  
  @Column(name = "Session_no")
  private Integer Session_no;
  
  @Column(name = "Audi_number")
  private Boolean Audi_number;
  
  @Column(name = "Admits")
  private Integer Admits;
  
  @Column(name = "finyear")
  private String finyear;
  
  @Column(name = "QTR")
  private String QTR;
  
  public void setCinema_hopk(Integer cinema_hopk) {
    this.cinema_hopk = cinema_hopk;
  }
  
  public void setSession_Business_Date(Date Session_Business_Date) {
    this.Session_Business_Date = Session_Business_Date;
  }
  
  public void setChannel_Group(String Channel_Group) {
    this.Channel_Group = Channel_Group;
  }
  
  public void setSession_no(Integer Session_no) {
    this.Session_no = Session_no;
  }
  
  public void setAudi_number(Boolean Audi_number) {
    this.Audi_number = Audi_number;
  }
  
  public void setAdmits(Integer Admits) {
    this.Admits = Admits;
  }
  
  public void setFinyear(String finyear) {
    this.finyear = finyear;
  }
  
  public void setQTR(String QTR) {
    this.QTR = QTR;
  }
  
  public Integer getCinema_hopk() {
    return this.cinema_hopk;
  }
  
  public Date getSession_Business_Date() {
    return this.Session_Business_Date;
  }
  
  public String getChannel_Group() {
    return this.Channel_Group;
  }
  
  public Integer getSession_no() {
    return this.Session_no;
  }
  
  public Boolean getAudi_number() {
    return this.Audi_number;
  }
  
  public Integer getAdmits() {
    return this.Admits;
  }
  
  public String getFinyear() {
    return this.finyear;
  }
  
  public String getQTR() {
    return this.QTR;
  }
}

