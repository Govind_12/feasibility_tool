package com.api.sqlentity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Cinema_master_Data_v1")
public class CinemaMasterData {
  @Column(name = "Cinema_Name")
  private String Cinema_Name;
  
  @Column(name = "Vista_info_Cinema_Name")
  private String Vista_info_Cinema_Name;
  
  @Column(name = "Cinema_Location")
  private String Cinema_Location;
  
  @Column(name = "State_strName")
  private String State_strName;
  
  @Column(name = "Nav_cinemaCode")
  private String Nav_cinemaCode;
  
  @Column(name = "OWNER_strCompany")
  private String OWNER_strCompany;
  
  @Column(name = "Nav_cinemadesc")
  private String Nav_cinemadesc;
  
  @Column(name = "Cinema_strRun")
  private String Cinema_strRun;
  
  @Column(name = "latitude")
  private String latitude;
  
  @Column(name = "longitude")
  private String longitude;
  
  @Column(name = "loyalty_complex_id")
  private Integer loyalty_complex_id;
  
  @Column(name = "No_of_Audi")
  private Integer No_of_Audi;
  
  @Column(name = "Gold")
  private Integer Gold;
  
  @Column(name = "PXL")
  private Integer PXL;
  
  @Column(name = "UltraPremium")
  private Integer UltraPremium;
  
  @Column(name = "DX")
  private Integer DX;
  
  @Column(name = "Premier")
  private Integer Premier;
  
  @Column(name = "IMAX")
  private Integer IMAX;
  
  @Column(name = "Playhouse")
  private Integer Playhouse;
  
  @Column(name = "ONYX")
  private Integer ONYX;
  
  @Column(name = "Mainstream")
  private Integer Mainstream;
  
  @Column(name = "luxe")
  private Integer luxe;
  
  @Column(name = "HOPK")
  private String HOPK;
  
  @Column(name = "Cinema_strCode")
  private String Cinema_strCode;
  
  public void setCinema_Name(String Cinema_Name) {
    this.Cinema_Name = Cinema_Name;
  }
  
  public void setVista_info_Cinema_Name(String Vista_info_Cinema_Name) {
    this.Vista_info_Cinema_Name = Vista_info_Cinema_Name;
  }
  
  public void setCinema_Location(String Cinema_Location) {
    this.Cinema_Location = Cinema_Location;
  }
  
  public void setState_strName(String State_strName) {
    this.State_strName = State_strName;
  }
  
  public void setNav_cinemaCode(String Nav_cinemaCode) {
    this.Nav_cinemaCode = Nav_cinemaCode;
  }
  
  public void setOWNER_strCompany(String OWNER_strCompany) {
    this.OWNER_strCompany = OWNER_strCompany;
  }
  
  public void setNav_cinemadesc(String Nav_cinemadesc) {
    this.Nav_cinemadesc = Nav_cinemadesc;
  }
  
  public void setCinema_strRun(String Cinema_strRun) {
    this.Cinema_strRun = Cinema_strRun;
  }
  
  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }
  
  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }
  
  public void setLoyalty_complex_id(Integer loyalty_complex_id) {
    this.loyalty_complex_id = loyalty_complex_id;
  }
  
  public void setNo_of_Audi(Integer No_of_Audi) {
    this.No_of_Audi = No_of_Audi;
  }
  
  public void setGold(Integer Gold) {
    this.Gold = Gold;
  }
  
  public void setPXL(Integer PXL) {
    this.PXL = PXL;
  }
  
  public void setUltraPremium(Integer UltraPremium) {
    this.UltraPremium = UltraPremium;
  }
  
  public void setDX(Integer DX) {
    this.DX = DX;
  }
  
  public void setPremier(Integer Premier) {
    this.Premier = Premier;
  }
  
  public void setIMAX(Integer IMAX) {
    this.IMAX = IMAX;
  }
  
  public void setPlayhouse(Integer Playhouse) {
    this.Playhouse = Playhouse;
  }
  
  public void setONYX(Integer ONYX) {
    this.ONYX = ONYX;
  }
  
  public void setMainstream(Integer Mainstream) {
    this.Mainstream = Mainstream;
  }
  
  public void setLuxe(Integer luxe) {
    this.luxe = luxe;
  }
  
  public void setHOPK(String HOPK) {
    this.HOPK = HOPK;
  }
  
  public void setCinema_strCode(String Cinema_strCode) {
    this.Cinema_strCode = Cinema_strCode;
  }
  
  public String getCinema_Name() {
    return this.Cinema_Name;
  }
  
  public String getVista_info_Cinema_Name() {
    return this.Vista_info_Cinema_Name;
  }
  
  public String getCinema_Location() {
    return this.Cinema_Location;
  }
  
  public String getState_strName() {
    return this.State_strName;
  }
  
  public String getNav_cinemaCode() {
    return this.Nav_cinemaCode;
  }
  
  public String getOWNER_strCompany() {
    return this.OWNER_strCompany;
  }
  
  public String getNav_cinemadesc() {
    return this.Nav_cinemadesc;
  }
  
  public String getCinema_strRun() {
    return this.Cinema_strRun;
  }
  
  public String getLatitude() {
    return this.latitude;
  }
  
  public String getLongitude() {
    return this.longitude;
  }
  
  public Integer getLoyalty_complex_id() {
    return this.loyalty_complex_id;
  }
  
  public Integer getNo_of_Audi() {
    return this.No_of_Audi;
  }
  
  public Integer getGold() {
    return this.Gold;
  }
  
  public Integer getPXL() {
    return this.PXL;
  }
  
  public Integer getUltraPremium() {
    return this.UltraPremium;
  }
  
  public Integer getDX() {
    return this.DX;
  }
  
  public Integer getPremier() {
    return this.Premier;
  }
  
  public Integer getIMAX() {
    return this.IMAX;
  }
  
  public Integer getPlayhouse() {
    return this.Playhouse;
  }
  
  public Integer getONYX() {
    return this.ONYX;
  }
  
  public Integer getMainstream() {
    return this.Mainstream;
  }
  
  public Integer getLuxe() {
    return this.luxe;
  }
  
  public String getHOPK() {
    return this.HOPK;
  }
  
  public String getCinema_strCode() {
    return this.Cinema_strCode;
  }
}
