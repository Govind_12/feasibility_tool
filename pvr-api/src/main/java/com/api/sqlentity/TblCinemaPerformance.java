package com.api.sqlentity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_Cinema_performance")
public class TblCinemaPerformance {
  @Column(name = "Cinema_hopk")
  private Integer Cinema_hopk;
  
  @Column(name = "Cinema_Name")
  private String Cinema_Name;
  
  @Column(name = "Cinema_State")
  private String Cinema_State;
  
  @Column(name = "Cinema_City")
  private String Cinema_City;
  
  @Column(name = "Business_Date")
  private Date Business_Date;
  
  @Column(name = "GBOC")
  private String GBOC;
  
  @Column(name = "Concession_Net_Revenue")
  private String Concession_Net_Revenue;
  
  @Column(name = "Admits")
  private Integer Admits;
  
  @Column(name = "Total_Seats_Available")
  private Integer Total_Seats_Available;
  
  @Column(name = "BO_No_of_Trans")
  private Integer BO_No_of_Trans;
  
  @Column(name = "Conc_No_of_Trans")
  private Integer Conc_No_of_Trans;
  
  @Column(name = "finyear")
  private String finyear;
  
  @Column(name = "QTR")
  private String QTR;
  
  public void setCinema_hopk(Integer Cinema_hopk) {
    this.Cinema_hopk = Cinema_hopk;
  }
  
  public void setCinema_Name(String Cinema_Name) {
    this.Cinema_Name = Cinema_Name;
  }
  
  public void setCinema_State(String Cinema_State) {
    this.Cinema_State = Cinema_State;
  }
  
  public void setCinema_City(String Cinema_City) {
    this.Cinema_City = Cinema_City;
  }
  
  public void setBusiness_Date(Date Business_Date) {
    this.Business_Date = Business_Date;
  }
  
  public void setGBOC(String GBOC) {
    this.GBOC = GBOC;
  }
  
  public void setConcession_Net_Revenue(String Concession_Net_Revenue) {
    this.Concession_Net_Revenue = Concession_Net_Revenue;
  }
  
  public void setAdmits(Integer Admits) {
    this.Admits = Admits;
  }
  
  public void setTotal_Seats_Available(Integer Total_Seats_Available) {
    this.Total_Seats_Available = Total_Seats_Available;
  }
  
  public void setBO_No_of_Trans(Integer BO_No_of_Trans) {
    this.BO_No_of_Trans = BO_No_of_Trans;
  }
  
  public void setConc_No_of_Trans(Integer Conc_No_of_Trans) {
    this.Conc_No_of_Trans = Conc_No_of_Trans;
  }
  
  public void setFinyear(String finyear) {
    this.finyear = finyear;
  }
  
  public void setQTR(String QTR) {
    this.QTR = QTR;
  }
  
  public Integer getCinema_hopk() {
    return this.Cinema_hopk;
  }
  
  public String getCinema_Name() {
    return this.Cinema_Name;
  }
  
  public String getCinema_State() {
    return this.Cinema_State;
  }
  
  public String getCinema_City() {
    return this.Cinema_City;
  }
  
  public Date getBusiness_Date() {
    return this.Business_Date;
  }
  
  public String getGBOC() {
    return this.GBOC;
  }
  
  public String getConcession_Net_Revenue() {
    return this.Concession_Net_Revenue;
  }
  
  public Integer getAdmits() {
    return this.Admits;
  }
  
  public Integer getTotal_Seats_Available() {
    return this.Total_Seats_Available;
  }
  
  public Integer getBO_No_of_Trans() {
    return this.BO_No_of_Trans;
  }
  
  public Integer getConc_No_of_Trans() {
    return this.Conc_No_of_Trans;
  }
  
  public String getFinyear() {
    return this.finyear;
  }
  
  public String getQTR() {
    return this.QTR;
  }
}
