package com.api.sqlentity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Cinema_audi_seat_Master_v1")
public class CinemaAudiSeatMaster {
  @Column(name = "Screen_bytNum")
  private String Screen_bytNum;
  
  @Column(name = "Screen_strName")
  private String Screen_strName;
  
  @Column(name = "Screen_intSeats")
  private String Screen_intSeats;
  
  @Column(name = "Area_strDescription")
  private String Area_strDescription;
  
  @Column(name = "Area_strShortDesc")
  private String Area_strShortDesc;
  
  @Column(name = "Area_intNumSeatsTot")
  private String Area_intNumSeatsTot;
  
  @Column(name = "Cinema_HOPK")
  private String Cinema_HOPK;
  
  @Column(name = "AreaCat_strCode")
  private String AreaCat_strCode;
  
  @Column(name = "AreaCat_strDesc")
  private String AreaCat_strDesc;
  
  @Column(name = "Area_cat_status")
  private String Area_cat_status;
  
  public void setScreen_bytNum(String Screen_bytNum) {
    this.Screen_bytNum = Screen_bytNum;
  }
  
  public void setScreen_strName(String Screen_strName) {
    this.Screen_strName = Screen_strName;
  }
  
  public void setScreen_intSeats(String Screen_intSeats) {
    this.Screen_intSeats = Screen_intSeats;
  }
  
  public void setArea_strDescription(String Area_strDescription) {
    this.Area_strDescription = Area_strDescription;
  }
  
  public void setArea_strShortDesc(String Area_strShortDesc) {
    this.Area_strShortDesc = Area_strShortDesc;
  }
  
  public void setArea_intNumSeatsTot(String Area_intNumSeatsTot) {
    this.Area_intNumSeatsTot = Area_intNumSeatsTot;
  }
  
  public void setCinema_HOPK(String Cinema_HOPK) {
    this.Cinema_HOPK = Cinema_HOPK;
  }
  
  public void setAreaCat_strCode(String AreaCat_strCode) {
    this.AreaCat_strCode = AreaCat_strCode;
  }
  
  public void setAreaCat_strDesc(String AreaCat_strDesc) {
    this.AreaCat_strDesc = AreaCat_strDesc;
  }
  
  public void setArea_cat_status(String Area_cat_status) {
    this.Area_cat_status = Area_cat_status;
  }
  
  public String getScreen_bytNum() {
    return this.Screen_bytNum;
  }
  
  public String getScreen_strName() {
    return this.Screen_strName;
  }
  
  public String getScreen_intSeats() {
    return this.Screen_intSeats;
  }
  
  public String getArea_strDescription() {
    return this.Area_strDescription;
  }
  
  public String getArea_strShortDesc() {
    return this.Area_strShortDesc;
  }
  
  public String getArea_intNumSeatsTot() {
    return this.Area_intNumSeatsTot;
  }
  
  public String getCinema_HOPK() {
    return this.Cinema_HOPK;
  }
  
  public String getAreaCat_strCode() {
    return this.AreaCat_strCode;
  }
  
  public String getAreaCat_strDesc() {
    return this.AreaCat_strDesc;
  }
  
  public String getArea_cat_status() {
    return this.Area_cat_status;
  }
}
