$(document).ready(function() {

	
	$(".numberLimit").inputFilter(function(value) {
		return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 1000000000);
	});
	
	
	$(function() {
		  $('.decimalLimit').on('input', function() {
		    match = (/(\d{0,9})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
		    this.value = match[1] + match[2];
		  });
		});
	
	
	$(".intLimit").inputFilter(function(value) {
		return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 12);
	});
	
	
	$(".leaseLimit").inputFilter(function(value) {
		return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 99);
	});
	
	
	

	var inputsFields = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #boxOfficePercentage , #f_bPercentage, #starting_year_mg , #mg_type');
	$(inputsFields).find(':input').attr('disabled', 'disabled');
	$('#other1_table').hide();
	$('#other2_table').hide();
	$('#revenue_share_table').hide();
	$('#mg_revenue_share_table').hide();
	$('#fixed_table').hide();

	$('#monthCam').hide();
	$('#monthRent').hide();
	$('#camZeroMg').hide();
	$('#rentZeroMg').hide();

					
					
	var inputsFields = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #boxOfficePercentage , #f_bPercentage, #starting_year_mg , #mg_type');
	$(inputsFields).find(':input').attr('disabled', 'disabled');
	$('#other1_table').hide();
	$('#other2_table').hide();
	$('#revenue_share_table').hide();
	$('#mg_revenue_share_table').hide();
	$('#fixed_table').hide();

	$('#monthCam').hide();
	$('#monthRent').hide();
	$('#camZeroMg').hide();
	$('#rentZeroMg').hide();
	
	var projectId = $("#id").val();
	if(projectId != ''){
		getRentAndCamData();
	}

});



(function($) {
	  $.fn.inputFilter = function(inputFilter) {
	    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
	      if (inputFilter(this.value)) {
	        this.oldValue = this.value;
	        this.oldSelectionStart = this.selectionStart;
	        this.oldSelectionEnd = this.selectionEnd;
	      } else if (this.hasOwnProperty("oldValue")) {
	        this.value = this.oldValue;
	        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
	      }
	    });
	  };
	}(jQuery));




function getRentAndCamData(){
	
	 var projectId = $("#id").val();
		
		$.ajax({
			url :"/project/getProjectDetailsById/"+ projectId,
			success : function(response) {
				//console.log(response);
				loadData(response);
			},
			error:function(xhr,status,error){
				console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
				}
		}); 
	
}



// edit case 
function loadData(response){
	var rentVal = $('#rentType').val();
	var camVal = $('#camType').val();
	var rentPaymentTerm = $('#rentPaymentTerm').val();
	var noOfColumns = $('#leasePeriod')
	
	if (rentVal == 'month') {
		$('#monthRent').show();
		$('#rentZeroMg').hide();
	}
	if (rentVal == 'fixed') {
		$('#rentZeroMg').show();
		$('#monthRent').hide();
	}
	
	if (camVal == 'month') {
		$('#monthCam').show();
		$('#camZeroMg').hide();

	}
	if (camVal == 'fixed') {
		$('#camZeroMg').show();
		$('#monthCam').hide();
	}
	
	var rentData = response.data.rentTerm;
	var camData = response.data.camTerm;
	//console.log(rentData);
	//console.log(camData);
	
	var lesserLesse = response.data.lesserLesse;
	var adjustmentYears = $('#lesserLesse_advanceRentPeriod').val();
	
	var CAMTermsCurrentVal = $("#cam_payment_terms").val();
    var noOfColumns = $('#leasePeriod').val();
    if (CAMTermsCurrentVal == 'choose_cam_terms') {
        var hiddenField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'none');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
    }
    else if (CAMTermsCurrentVal == 'fixed') {
        var showedField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        generateCamTableOnEdit(noOfColumns,camData);
    }
    else if (CAMTermsCurrentVal == 'actuals') {
        var hiddenField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'none');
      //  $('#camTerms_fixed_table').css('display', 'block');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
       // generateCamTableOnEdit(noOfColumns,camData);
    }
    else if (CAMTermsCurrentVal == 'included') {
        var hiddenField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'none');
        $('#camTerms_fixed_table').css('display', 'block');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
        generateCamTableOnEdit(noOfColumns,camData);
    }
		
    if (rentPaymentTerm == 'fixed') {
        var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #fixed_table').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        var hiddenField = $('#starting_year_mg , #mg_type, #boxOfficePercentage , #f_bPercentage , #revenue_share_table , #sale_percentage ,#other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'none');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
        $('#revenue_share_table').hide();
        $('#tblRevenueShare').hide();
        $('#other2_table').hide();
        $('#mg_revenue_share_table').hide();
        $('#other1_table').hide();
        generateRentTableOnEdit(noOfColumns,rentData);	
        
    }
    else if (rentPaymentTerm == 'revenue_share') {
        var showedField = $('#frequencyEscalationYear , #percentageEscalationRent ,#boxOfficePercentage , #f_bPercentage  , #revenue_share_table, #sale_percentage, #other_revenue_percentage').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        var hiddenField = $('#rentINR , #starting_year_mg , #mg_type, #fixed_table, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'none');
       
        $('#fixed_table').hide();
        $('#other1_table').hide();
        $('#other2_table').hide();
        $('#mg_revenue_share_table').hide();
        $('#revenue_share_table').show();
        $('#tblRevenueShare').show();
        generateRentTableOnEdit(noOfColumns,rentData);
    }
    else if (rentPaymentTerm == 'mg_revenue_share') {
        var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #sale_percentage, #other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        var hiddenField = $('#starting_year_mg , #mg_type, #fixed_table, #revenue_share_table').css('display', 'none');
        $('#fixed_table').hide();
        $('#other1_table').hide();
        $('#other2_table').hide();
        $('#revenue_share_table').hide();
        generateRentTableOnEdit(noOfColumns,rentData);
    }else if(rentPaymentTerm == 'other_1'){
    	  var showedField = $('#starting_year_mg , #mg_type, #rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #FixedYears, #FixedRent, #mg_revenue_share_table,#sale_percentage,#other_revenue_percentage').css('display', 'block');
          $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
          var hiddenField = $('#fixed_table, #revenue_share_table,#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #boxOfficePercentage , #sale_percentage, #other_revenue_percentage, #f_bPercentage,#FixedYears, #FixedRent').css('display', 'none');
          $('#fixed_table').hide();
          $('#mg_revenue_share_table').hide();
          $('#revenue_share_table').hide();
          $('#other2_table').hide();
          $('#other1_table').show();
          
          var mgTypeSelect = $("#mg_type_select").val();
          //alert(mgTypeSelect)
          if(mgTypeSelect == 'fixed'){
        	  $('#rentINR,#FixedYears,#FixedRent,#boxOfficePercentage,#f_bPercentage,#sale_percentage,#other_revenue_percentage,#frequencyEscalationYear,#percentageEscalationRent').css('display', 'block');  
          }
          if(mgTypeSelect == 'average'){
        	  $('#rentINR,#FixedYears,#FixedRent').css('display', 'none'); 
        	  $('#boxOfficePercentage,#f_bPercentage,#sale_percentage,#other_revenue_percentage,#frequencyEscalationYear,#percentageEscalationRent').css('display', 'block'); 
          }
          
                  
          generateRentTableOnEdit(noOfColumns,rentData);
    }
    else if(rentPaymentTerm == 'other_2'){
  	  var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #sale_percentage, #other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        var hiddenField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #sale_percentage, #other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table,#starting_year_mg , #mg_type, #fixed_table, #revenue_share_table').css('display', 'none');
        $('#fixed_table').hide();
        $('#mg_revenue_share_table').hide();
        $('#revenue_share_table').hide();
        $('#other1_table').hide();
        $('#other2_table').show();
        generateRentTableOnEdit(noOfColumns,rentData);
      
    }
	
	if(adjustmentYears > 0){
	  var showedField = $('#lesserLesse_advanceRent , #lesserLesse_advanceRentPeriod , #tblAdvanceRent').css('display', 'block');
	    $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
	    generateAdjustmentRentTableOnEdit(adjustmentYears,lesserLesse); 
	}  
	
}	


function generateRentTableOnEdit(noOfColumns,rentData) {
	
	//monthly rent to 
	const noOfMonths=12;
	var leasableArea = $('#leasableArea').val();
	var rentPaymentTerm=$('#rentPaymentTerm').val();
    var startingYearForMG = $('#startingYearForMG').val();
	var myTable;
	if (rentPaymentTerm == 'fixed'){
			myTable = $('#tblFixed');
		$('#theadFixedTable').find('tr').remove();
		$('#tbodyFixedTable').find('tr').remove();
		
		$('#theadFixedTable').append('<tr><th class="fixed-side">Lease Period (Yrs)</th></tr>');
		$('#tbodyFixedTable').append('<tr><th class="fixed-side">Rent (INR)</th></tr>');
		
		for(var i=1;i<=noOfColumns;i++){
			
			$('#tblFixed tr').append($("<td>"));
			$('#tblFixed thead tr>td:last').html(i);
			$('#tblFixed tbody tr').each(function(){
				
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" class ="fixed" id="y'+i+'" value="'+rentData.yearWiseRent[i]+'" />'))
			});
			
			
		}
	}
	else if(rentPaymentTerm == 'revenue_share'){
			myTable = $('#tblRevenueShare');
		
		$('#theadRevenueShare').find('tr').remove();
		$('#tbodyRevenueShareTable').find('tr').remove();
		
		$('#theadRevenueShare').append('<tr><th class="fixed-side">Year</th></tr>');
		$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Share of Box Office Revenue (%)</th></tr>');
		$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Share of F&B Revenue (%)</th></tr>');
		$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Share of Ad Revenue (%)</th></tr>');
		$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Share of Other Revenue (%)</th></tr>');
		
		for(var i=1;i<=noOfColumns;i++){
			$('#tblRevenueShare tr').append($("<td>"));
			$('#tblRevenueShare thead tr>td:last').html(i);
			
			var ind=1;
			$('#tblRevenueShare tbody tr').each(function(){
				if(ind==1)
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" class ="boxPercent" id="b'+i+'" value="'+rentData.yearWiseBoxOfficePercentage[i]+'" />'));
				else if(ind==2)
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" class ="fnbPercent" id="f'+i+'" value="'+rentData.yearWiseFAndBPercentage[i]+'" />'));
				else if(ind==3)
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" class ="adSalePercent" id="a'+i+'" value="'+rentData.yearWiseAdSalePercentage[i]+'" />'));
				else if(ind==4)
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" class ="otherPercent" id="o'+i+'" value="'+rentData.yearWiseOthersPercentage[i]+'" />'));
				
				ind++;
			});
			
				
			
			}
	
	}
	
	 else if (rentPaymentTerm == 'mg_revenue_share') {
		 
		 myTable = $('#tblMgRevenueShare');
		 	$('#theadMgRevenueShare').find('tr').remove();
			$('#tbodyMgRevenueShareTable').find('tr').remove();
		 
			$('#theadMgRevenueShare').append('<tr><th class="fixed-side">Year</th></tr>');
			$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Share of Box Office Revenue (%)</th></tr>');
			$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Share of F&B Revenue (%)</th></tr>');
			$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Share of Ad Revenue (%)</th></tr>');
			$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Share of Other Revenue (%)</th></tr>');
			$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Fixed Rent (INR)</th></tr>');
			
			for(var i=1;i<=noOfColumns;i++){
				$('#tblMgRevenueShare tr').append($("<td>"));
				$('#tblMgRevenueShare thead tr>td:last').html(i);
				
				var ind=1;
				$('#tblMgRevenueShare tbody tr').each(function(){
					if(ind==1)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" class ="boxPercent" id="b'+i+'" value="'+rentData.yearWiseBoxOfficePercentage[i]+'" />'));
					else if(ind==2)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" class ="fnbPercent" id="f'+i+'" value="'+rentData.yearWiseFAndBPercentage[i]+'" />'));
					else if(ind==3)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" class ="adSalePercent" id="a'+i+'" value="'+rentData.yearWiseAdSalePercentage[i]+'" />'));
					else if(ind==4)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" class ="otherPercent" id="o'+i+'" value="'+rentData.yearWiseOthersPercentage[i]+'" />'));
					else if(ind==5)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" class ="fixed" id="y'+i+'" value="'+rentData.yearWiseRent[i]+'" />'))
					
					
					ind++;
				});
				
			}
			
		 
	 }
	
	 else if (rentPaymentTerm == 'other_1') {
		 
		 myTable = $('#tblOther_1');
		 	$('#theadOther_1').find('tr').remove();
			$('#tbodyOther_1').find('tr').remove();
		 
			$('#theadOther_1').append('<tr><th class="fixed-side">Year</th></tr>');
			$('#tbodyOther_1').append('<tr><th class="fixed-side">Share of Box Office Revenue (%)</th></tr>');
			$('#tbodyOther_1').append('<tr><th class="fixed-side">Share of F&B Revenue (%)</th></tr>');
			$('#tbodyOther_1').append('<tr><th class="fixed-side">Share of Ad Revenue (%)</th></tr>');
			$('#tbodyOther_1').append('<tr><th class="fixed-side">Share of Other Revenue (%)</th></tr>');
			$('#tbodyOther_1').append('<tr><th class="fixed-side">Fixed Rent (INR)</th></tr>');
			
			var mg_type_select = $('#mg_type_select').val();
			for(var i=1;i<=noOfColumns;i++){
				$('#tblOther_1 tr').append($("<td>"));
				$('#tblOther_1 thead tr>td:last').html(i);
				
				if('average' == mg_type_select){
					rent = 0;
				}
				
				
				
				var ind=1;
				$('#tblOther_1 tbody tr').each(function(){
					if(ind==1)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" class ="boxPercent" id="b'+i+'" value="'+rentData.yearWiseBoxOfficePercentage[i]+'" />'));
					else if(ind==2)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" class ="fnbPercent" id="f'+i+'" value="'+rentData.yearWiseFAndBPercentage[i]+'" />'));
					else if(ind==3)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" class ="adSalePercent" id="a'+i+'" value="'+rentData.yearWiseAdSalePercentage[i]+'" />'));
					else if(ind==4)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" class ="otherPercent" id="o'+i+'" value="'+rentData.yearWiseOthersPercentage[i]+'" />'));
					else if(ind==5){
						if(i < startingYearForMG){
							$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']"  value=0 />')) 
						}else{
							$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" class ="fixed" id="y'+i+'" value="'+rentData.yearWiseRent[i]+'" />')) 
						}
						
					}
					
					ind++;
				});
				
				
			}
			
		 
	 }
	
	 else if (rentPaymentTerm == 'other_2') {
		 
		 myTable = $('#tblOther_2');
		 	$('#theadOther_2').find('tr').remove();
			$('#tbodyOther_2').find('tr').remove();
		 
			$('#theadOther_2').append('<tr><th class="fixed-side">Year</th></tr>');
			$('#tbodyOther_2').append('<tr><th class="fixed-side">Share of Box Office Revenue (%)</th></tr>');
			$('#tbodyOther_2').append('<tr><th class="fixed-side">Share of F&B Revenue (%)</th></tr>');
			$('#tbodyOther_2').append('<tr><th class="fixed-side">Share of Ad Revenue (%)</th></tr>');
			$('#tbodyOther_2').append('<tr><th class="fixed-side">Share of Other Revenue (%)</th></tr>');
			$('#tbodyOther_2').append('<tr><th class="fixed-side">Fixed Rent (INR)</th></tr>');
			
			
			for(var i=1;i<=noOfColumns;i++){
				$('#tblOther_2 tr').append($("<td>"));
				$('#tblOther_2 thead tr>td:last').html(i);
				
				var ind=1;
				$('#tblOther_2 tbody tr').each(function(){
					if(ind==1)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" value="'+rentData.yearWiseBoxOfficePercentage[i]+'" />'));
					else if(ind==2)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" value="'+rentData.yearWiseFAndBPercentage[i]+'" />'));
					else if(ind==3)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" value="'+rentData.yearWiseAdSalePercentage[i]+'" />'));
					else if(ind==4)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" value="'+rentData.yearWiseOthersPercentage[i]+'" />'));
					else if(ind==5)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" value="'+rentData.yearWiseRent[i]+'" />'))
					
					
					ind++;
				});
				
				
				}
			
		 
	 }
	

} 





function generateCamTableOnEdit(noOfColumns,camData) {
	
	//monthly rent to 
	const noOfMonths=12;
	var leasableArea = $('#leasableArea').val();
	var cam_payment_terms = $('#cam_payment_terms').val();
	var cam_rent= ($('#camRate').val()=='') ? 0 : leasableArea * noOfMonths*parseFloat($('#camRate').val());
	var camfrequencyOfEscaltionYears = ($('#camfrequencyOfEscaltionYears').val() == '') ? 0 : parseFloat($('#camfrequencyOfEscaltionYears').val()) ;
	var camPercentageEscalation = ($('#camPercentageEscalation').val() == '') ? 0 : parseFloat($('#camPercentageEscalation').val());
		
	var myTable;
	if (cam_payment_terms == 'fixed'){
			myTable = $('#tblCamFixed');
		
		$('#theadCamFixedTable').find('tr').remove();
		$('#tbodyCamFixedTable').find('tr').remove();
		
		$('#theadCamFixedTable').append('<tr><th class="fixed-side">Year</th></tr>');
		$('#tbodyCamFixedTable').append('<tr><th class="fixed-side">CAM (INR)</th></tr>');
		
		for(var i=1;i<=noOfColumns;i++){
			
			$('#tblCamFixed tr').append($("<td>"));
			$('#tblCamFixed thead tr>td:last').html(i);
			$('#tblCamFixed tbody tr').each(function(){
				
				$(this).children('td:last').append($('<input type="text" name="camTerm.yearWiseCamRent['+i+']" class ="camFixed" id="c'+i+'" value="'+camData.yearWiseCamRent[i]+'" />'))
			});
			
				
			}
	}
	
	if (cam_payment_terms == 'actuals'){
/*		myTable = $('#tblCamFixed');
	
	$('#theadCamFixedTable').find('tr').remove();
	$('#tbodyCamFixedTable').find('tr').remove();
	
	$('#theadCamFixedTable').append('<tr><th class="fixed-side">Year</th></tr>');
	$('#tbodyCamFixedTable').append('<tr><th class="fixed-side">CAM (INR)</th></tr>');
	
	cam_rent = 0;
	
	for(var i=1;i<=noOfColumns;i++){
		
		$('#tblCamFixed tr').append($("<td>"));
		$('#tblCamFixed thead tr>td:last').html(i);
		$('#tblCamFixed tbody tr').each(function(){
			
			$(this).children('td:last').append($('<input type="text" name="camTerm.yearWiseCamRent['+i+']" value="'+camData.yearWiseCamRent[i]+'" />'))
		});
		
		
		}*/
}
	
	if (cam_payment_terms == 'included'){
		myTable = $('#tblCamFixed');
	
	$('#theadCamFixedTable').find('tr').remove();
	$('#tbodyCamFixedTable').find('tr').remove();
	
	$('#theadCamFixedTable').append('<tr><th class="fixed-side">Year</th></tr>');
	$('#tbodyCamFixedTable').append('<tr><th class="fixed-side">CAM (INR)</th></tr>');
	
	cam_rent = 0;
	
	for(var i=1;i<=noOfColumns;i++){
		
		$('#tblCamFixed tr').append($("<td>"));
		$('#tblCamFixed thead tr>td:last').html(i);
		$('#tblCamFixed tbody tr').each(function(){
			
			$(this).children('td:last').append($('<input type="text" name="camTerm.yearWiseCamRent['+i+']" value="'+camData.yearWiseCamRent[i]+'" />'))
		});
		
		
		}
}
	
	

} 

	// Scroll Table For Fixed Option
	$(document).on('click', '#fixed_table >.viewYearDetails , #camTerms_fixed_table >.viewYearDetails ,#revenue_share_table >.viewYearDetails, #mg_revenue_share_table >.viewYearDetails, #other1_table >.viewYearDetails,#other2_table >.viewYearDetails, #advanceRent_fixed_table > .viewYearDetails', function(){
		$(this).parents().find('#fixed_table .collapse_div , #camTerms_fixed_table .collapse_div , #revenue_share_table .collapse_div , #mg_revenue_share_table .collapse_div, #other1_table .collapse_div,#other2_table .collapse_div, #advanceRent_fixed_table .collapse_div').slideToggle(), $(this).toggleClass('rotateArrow');
	});

	$('#rentPaymentTerm').on('change keyup', function () {
		var noOfColumns = $('#leasePeriod').val();
	   
	    
	    var selectCurrentVal = $(this).val();
	    if (selectCurrentVal == 'other') {
	        var showedField = $('#starting_year_mg , #mg_type').css('display', 'block');
	        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
	        var hiddenField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #boxOfficePercentage , #f_bPercentage, #fixed_table, #FixedYears, #FixedRent').css('display', 'none');
	        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
	    }
	    else if (selectCurrentVal == 'fixed') {
	    	
	        var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #fixed_table').css('display', 'block');
	        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
	        var hiddenField = $('#starting_year_mg , #mg_type, #boxOfficePercentage , #f_bPercentage , #revenue_share_table , #sale_percentage ,#other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'none');
	        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
	        $('#revenue_share_table').hide();
	        $('#tblRevenueShare').hide();
	        $('#other2_table').hide();
	        $('#mg_revenue_share_table').hide();
	        $('#other1_table').hide();
	        generateTable(noOfColumns);
	    }
	    else if (selectCurrentVal == 'revenue_share') {
	        var showedField = $('#frequencyEscalationYear , #percentageEscalationRent ,#boxOfficePercentage , #f_bPercentage  , #revenue_share_table, #sale_percentage, #other_revenue_percentage').css('display', 'block');
	        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
	        var hiddenField = $('#rentINR , #starting_year_mg , #mg_type, #fixed_table, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'none');
	       
	        $('#fixed_table').hide();
	        $('#other1_table').hide();
	        $('#other2_table').hide();
	        $('#mg_revenue_share_table').hide();
	        $('#revenue_share_table').show();
	        $('#tblRevenueShare').show();
	        
	        generateTable(noOfColumns);
	    }
	    else if (selectCurrentVal == 'mg_revenue_share') {
	        var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #sale_percentage, #other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'block');
	        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
	        var hiddenField = $('#starting_year_mg , #mg_type, #fixed_table, #revenue_share_table').css('display', 'none');
	        $('#fixed_table').hide();
	        $('#other1_table').hide();
	        $('#other2_table').hide();
	        $('#revenue_share_table').hide();
	        generateTable(noOfColumns);
	    }else if(selectCurrentVal == 'other_1'){
	    	  var showedField = $('#starting_year_mg , #mg_type, #rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #FixedYears, #FixedRent, #mg_revenue_share_table,#sale_percentage,#other_revenue_percentage').css('display', 'block');
	          $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
	          var hiddenField = $('#fixed_table, #revenue_share_table,#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #boxOfficePercentage , #sale_percentage, #other_revenue_percentage, #f_bPercentage,#FixedYears, #FixedRent').css('display', 'none');
	          $('#fixed_table').hide();
	          $('#mg_revenue_share_table').hide();
	          $('#revenue_share_table').hide();
	          $('#other2_table').hide();
	          $('#other1_table').hide();
	         
	          generateTable(noOfColumns);
	    }
	    else if(selectCurrentVal == 'other_2'){
	  	  var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #sale_percentage, #other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'block');
	        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
	        var hiddenField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #sale_percentage, #other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table,#starting_year_mg , #mg_type, #fixed_table, #revenue_share_table').css('display', 'none');
	        $('#fixed_table').hide();
	        $('#mg_revenue_share_table').hide();
	        $('#revenue_share_table').hide();
	        $('#other1_table').hide();
	        $('#other2_table').show();
	      
	        generateTable(noOfColumns);
	  }
	    
	    else if (selectCurrentVal == 'select_terms') {
	        var hiddenField = $('#starting_year_mg , #mg_type, #rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage, #fixed_table, #revenue_share_table, #mg_revenue_share_table').css('display', 'none');
	        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
	    }
	   
	});


	$('#mg_type_select').on('change keyup', function () {
		
	    var MGTypeCurrentVal = $(this).val();
	    //alert(MGTypeCurrentVal)
	    var noOfColumns = $('#leasePeriod').val();
	    $('#other1_table').show();
	    if (MGTypeCurrentVal == 'choose_mg_type') {
	        var hiddenField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #boxOfficePercentage , #f_bPercentage').css('display', 'none');
	        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
	    }
	    else if (MGTypeCurrentVal == 'fixed') {
	        var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #sale_percentage, #other_revenue_percentage, #boxOfficePercentage , #f_bPercentage,#FixedRent,#FixedYears').css('display', 'block');
	        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
	    }
	    else if (MGTypeCurrentVal == 'average') {
	    	 $('#boxOfficePercentage,#f_bPercentage,#sale_percentage,#other_revenue_percentage,#frequencyEscalationYear,#percentageEscalationRent').css('display', 'block');
	        var hiddenField = $('#rentINR').css('display', 'none');
	        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
	    }
	    
	    generateTable(noOfColumns);
	});

	$('#cam_payment_terms').on('change keyup', function () {
	    var CAMTermsCurrentVal = $(this).val();
	    var noOfColumns = $('#leasePeriod').val();
	    if (CAMTermsCurrentVal == 'choose_cam_terms') {
	        var hiddenField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'none');
	        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
	    }
	    else if (CAMTermsCurrentVal == 'fixed') {
	        var showedField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'block');
	        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
	        generateCamTable(noOfColumns);
	    }
	    else if (CAMTermsCurrentVal == 'actuals') {
	        var hiddenField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'none');
	      //  $('#camTerms_fixed_table').css('display', 'block');
	        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
	        //generateCamTable(noOfColumns);
	    }
	    else if (CAMTermsCurrentVal == 'included') {
	        var hiddenField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'none');
	        $('#camTerms_fixed_table').css('display', 'block');
	        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
	        generateCamTable(noOfColumns);
	    }
	});
	   
	$("#rent, #percentageEscalationInRent,#leasePeriod, #rentFrequencyOfEscaltionYears ,#startingYearForMG, #box_office_percentage , #f_and_b_percentage , #ad_sale_percentage, #others_percentage,#rentFrequencyOfEscaltionYears_fixed,#percentageEscalationInRent_fixed").on('keyup', function() {
		
		 var noOfColumns = $('#leasePeriod').val();
	     generateTable(noOfColumns);

	});


	$("#camRate, #camfrequencyOfEscaltionYears,#leasePeriod, #camPercentageEscalation").on('keyup', function() {
		 var noOfColumns = $('#leasePeriod').val();
		 generateCamTable(noOfColumns);

	});


	
	
/*	$(document).on('keyup', '.fixed', function() {
		 var noOfColumns = $('#leasePeriod').val();
		 var value = $(this).val();
		 var yr = $(this).attr('id');
		 var year = yr.split('').slice(1);
		 fixedRentAutoGenerate(noOfColumns,value,year);

	});*/
	
	
	
/*	function fixedRentAutoGenerate(noOfColumns,value,year){
		const noOfMonths=12;
		var leasableArea = $('#leasableArea').val();
		var rent= parseFloat(value);
		var yr =  parseInt(year);
		var rentFrequencyOfEscaltionYears=$('#rentFrequencyOfEscaltionYears').val();
		var percentageEscalationInRent=$('#percentageEscalationInRent').val();
		
			for (var i = yr; i <= noOfColumns; i++) {
				
				if (i % rentFrequencyOfEscaltionYears == 0){
					rent += (rent * percentageEscalationInRent / 100);
				}
				$("#y" + (i+1)).val(rent);
	
			}
}*/
	
/*	$(document).on('change keyup','.boxPercent', function() {
		$('.boxPercent').off('change keyup');
		 var noOfColumns = $('#leasePeriod').val();
		 var value = $(this).val();
		 var yr = $(this).attr('id');
		 var year = yr.split('').slice(1);
		 boxPercentRentAutoGenerate(noOfColumns,value,year);
	});
	*/
	
	
/*	function boxPercentRentAutoGenerate(noOfColumns,value,year){
		const noOfMonths=12;
		var leasableArea = $('#leasableArea').val();
		var yr =  parseInt(year);
		var rentFrequencyOfEscaltionYears=$('#rentFrequencyOfEscaltionYears').val();
		var percentageEscalationInRent=$('#percentageEscalationInRent').val();
		var boxOfcPerc = parseFloat(value);
		
		for(var i=yr;i<=noOfColumns;i++){
			if(i%rentFrequencyOfEscaltionYears == 0){
				boxOfcPerc = parseFloat(boxOfcPerc) + parseFloat(percentageEscalationInRent);
			}
			$("#b" + (i+1)).val(boxOfcPerc);
			
		}
		
}*/
		
	
	
/*	
	
	$(document).on('keyup', '.fnbPercent', function() {
		 var noOfColumns = $('#leasePeriod').val();
		 var value = $(this).val();
		 var yr = $(this).attr('id');
		 var year = yr.split('').slice(1);
		 fnbPercentRentAutoGenerate(noOfColumns,value,year);

	});
	*/
	
	
/*	function fnbPercentRentAutoGenerate(noOfColumns,value,year){
		const noOfMonths=12;
		var leasableArea = $('#leasableArea').val();
		var yr =  parseInt(year);
		var rentFrequencyOfEscaltionYears=$('#rentFrequencyOfEscaltionYears').val();
		var percentageEscalationInRent=$('#percentageEscalationInRent').val();
		 var fandBPerc = parseFloat(value);
		
		for(var i=yr;i<=noOfColumns;i++){
			
			if(i%rentFrequencyOfEscaltionYears == 0){
				fandBPerc = parseFloat(fandBPerc) + parseFloat(percentageEscalationInRent);
			}
			$("#f" + (i+1)).val(fandBPerc);	
			
		}
		
}*/
	

	
	
	
/*	$(document).on('keyup', '.adSalePercent', function() {
		 var noOfColumns = $('#leasePeriod').val();
		 var value = $(this).val();
		 var yr = $(this).attr('id');
		 var year = yr.split('').slice(1);
		 adSalePercentRentAutoGenerate(noOfColumns,value,year);

	});
	
	
	
	function adSalePercentRentAutoGenerate(noOfColumns,value,year){
		const noOfMonths=12;
		var leasableArea = $('#leasableArea').val();
		var yr =  parseInt(year);
		var rentFrequencyOfEscaltionYears=$('#rentFrequencyOfEscaltionYears').val();
		var percentageEscalationInRent=$('#percentageEscalationInRent').val();
		  var adSalePerc =  parseFloat(value);
		
		for(var i=yr;i<=noOfColumns;i++){
			
			if(i%rentFrequencyOfEscaltionYears == 0){
				adSalePerc = parseFloat(adSalePerc) + parseFloat(percentageEscalationInRent);
			}
			$("#a" + (i+1)).val(adSalePerc);	
			
		}
		
}*/
	
	
	
	
	
/*	
	$(document).on('keyup', '.otherPercent', function() {
		 var noOfColumns = $('#leasePeriod').val();
		 var value = $(this).val();
		 var yr = $(this).attr('id');
		 var year = yr.split('').slice(1);
		 otherPercentRentAutoGenerate(noOfColumns,value,year);

	});
	
	
	
	function otherPercentRentAutoGenerate(noOfColumns,value,year){
		const noOfMonths=12;
		var leasableArea = $('#leasableArea').val();
		var yr =  parseInt(year);
		var rentFrequencyOfEscaltionYears=$('#rentFrequencyOfEscaltionYears').val();
		var percentageEscalationInRent=$('#percentageEscalationInRent').val();
		 var othersPerc = parseFloat(value);
		
		for(var i=yr;i<=noOfColumns;i++){
			
			if(i%rentFrequencyOfEscaltionYears == 0){
				othersPerc = parseFloat(othersPerc) + parseFloat(percentageEscalationInRent);
			}
			$("#o" + (i+1)).val(othersPerc);	
			
		}
		
}
	*/
	
	

	function generateTable(noOfColumns) {
		
		//monthly rent to 
		const noOfMonths=12;
		var leasableArea = $('#leasableArea').val();
		
		var rent= ($('#rent').val()=='') ? 0 : leasableArea * noOfMonths*parseFloat($('#rent').val());
		var rentFrequencyOfEscaltionYears=$('#rentFrequencyOfEscaltionYears').val();
		var percentageEscalationInRent=$('#percentageEscalationInRent').val();
		var rentPaymentTerm=$('#rentPaymentTerm').val();
		
		var boxOfcPerc=$("#box_office_percentage").val()== '' ? 0 : parseFloat($("#box_office_percentage").val());
	    var fandBPerc=$("#f_and_b_percentage").val()== '' ? 0 : parseFloat($("#f_and_b_percentage").val());
	    var adSalePerc=$("#ad_sale_percentage").val()== '' ? 0 : parseFloat($("#ad_sale_percentage").val());
	    var othersPerc=$("#others_percentage").val()== '' ? 0 : parseFloat($("#others_percentage").val());
	    
	    var startingYearForMG = $('#startingYearForMG').val();
		var rentFrequencyOfEscaltionYears_fixed = $('#rentFrequencyOfEscaltionYears_fixed').val();
		var percentageEscalationInRent_fixed = $('#percentageEscalationInRent_fixed').val();
		
		
		var myTable;
		if (rentPaymentTerm == 'fixed'){
				myTable = $('#tblFixed');
			$('#theadFixedTable').find('tr').remove();
			$('#tbodyFixedTable').find('tr').remove();
			
			$('#theadRevenueShare').find('tr').remove();
			$('#tbodyRevenueShareTable').find('tr').remove();
			
			$('#theadMgRevenueShare').find('tr').remove();
			$('#tbodyMgRevenueShareTable').find('tr').remove();
			
			$('#theadOther_1').find('tr').remove();
			$('#tbodyOther_1').find('tr').remove();
			
			
			$('#theadFixedTable').append('<tr><th class="fixed-side">Lease Period (Yrs)</th></tr>');
			$('#tbodyFixedTable').append('<tr><th class="fixed-side">Rent (INR)</th></tr>');
			
			for(var i=1;i<=noOfColumns;i++){
				
				$('#tblFixed tr').append($("<td>"));
				$('#tblFixed thead tr>td:last').html(i);
				$('#tblFixed tbody tr').each(function(){
					
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" class ="fixed" id="y'+i+'" value="'+rent+'" />'))
				});
				
					if(i%rentFrequencyOfEscaltionYears == 0)
						rent+=(rent*percentageEscalationInRent/100);
				
				}
		}
		else if(rentPaymentTerm == 'revenue_share'){
			//alert('table for renvue')
				myTable = $('#tblRevenueShare');
			
			$('#theadRevenueShare').find('tr').remove();
			$('#tbodyRevenueShareTable').find('tr').remove();
			
			$('#theadFixedTable').find('tr').remove();
			$('#tbodyFixedTable').find('tr').remove();
			
			$('#theadMgRevenueShare').find('tr').remove();
			$('#tbodyMgRevenueShareTable').find('tr').remove();
			
			$('#theadOther_1').find('tr').remove();
			$('#tbodyOther_1').find('tr').remove();
			
			$('#theadRevenueShare').append('<tr><th class="fixed-side">Year</th></tr>');
			$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Share of Box Office Revenue (%)</th></tr>');
			$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Share of F&B Revenue (%)</th></tr>');
			$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Share of Ad Revenue (%)</th></tr>');
			$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Share of Other Revenue (%)</th></tr>');
			
			for(var i=1;i<=noOfColumns;i++){
				$('#tblRevenueShare tr').append($("<td>"));
				$('#tblRevenueShare thead tr>td:last').html(i);
				
				var ind=1;
				$('#tblRevenueShare tbody tr').each(function(){
					if(ind==1)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" class ="boxPercent" id="b'+i+'" value="'+boxOfcPerc+'" />'));
					else if(ind==2)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" class ="fnbPercent" id="f'+i+'" value="'+fandBPerc+'" />'));
					else if(ind==3)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" class ="adSalePercent" id="a'+i+'" value="'+adSalePerc+'" />'));
					else if(ind==4)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" class ="otherPercent" id="o'+i+'" value="'+othersPerc+'" />'));
					
					ind++;
				});
				
					if(i%rentFrequencyOfEscaltionYears==0){
						boxOfcPerc = parseFloat(boxOfcPerc) + parseFloat(percentageEscalationInRent);
						fandBPerc = parseFloat(fandBPerc) + parseFloat(percentageEscalationInRent);
						adSalePerc = parseFloat(adSalePerc) + parseFloat(percentageEscalationInRent);
						othersPerc = parseFloat(othersPerc) + parseFloat(percentageEscalationInRent);
						
					}
					
				
				}
		
		}
		
		 else if (rentPaymentTerm == 'mg_revenue_share') {
			 
			 myTable = $('#tblMgRevenueShare');
			 	$('#theadMgRevenueShare').find('tr').remove();
				$('#tbodyMgRevenueShareTable').find('tr').remove();
				
				$('#theadFixedTable').find('tr').remove();
				$('#tbodyFixedTable').find('tr').remove();
				
				$('#theadRevenueShare').find('tr').remove();
				$('#tbodyRevenueShareTable').find('tr').remove();
				
				$('#theadFixedTable').find('tr').remove();
				$('#tbodyFixedTable').find('tr').remove();
				
				$('#theadOther_1').find('tr').remove();
				$('#tbodyOther_1').find('tr').remove();
				
				
				
				
			 
				$('#theadMgRevenueShare').append('<tr><th class="fixed-side">Year</th></tr>');
				$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Share of Box Office Revenue (%)</th></tr>');
				$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Share of F&B Revenue (%)</th></tr>');
				$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Share of Ad Revenue (%)</th></tr>');
				$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Share of Other Revenue (%)</th></tr>');
				$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Fixed Rent (INR)</th></tr>');
				
				for(var i=1;i<=noOfColumns;i++){
					$('#tblMgRevenueShare tr').append($("<td>"));
					$('#tblMgRevenueShare thead tr>td:last').html(i);
					
					var ind=1;
					$('#tblMgRevenueShare tbody tr').each(function(){
						if(ind==1)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" class ="boxPercent" id="b'+i+'" value="'+boxOfcPerc+'" />'));
						else if(ind==2)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" class ="fnbPercent" id="f'+i+'" value="'+fandBPerc+'" />'));
						else if(ind==3)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" class ="adSalePercent" id="a'+i+'" value="'+adSalePerc+'" />'));
						else if(ind==4)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" class ="otherPercent" id="o'+i+'" value="'+othersPerc+'" />'));
						else if(ind==5)
							$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" class ="fixed" id="y'+i+'" value="'+rent+'" />'))
						
						
						ind++;
					});
					
					if(i%rentFrequencyOfEscaltionYears==0){
						boxOfcPerc = parseFloat(boxOfcPerc) + parseFloat(percentageEscalationInRent);
						fandBPerc = parseFloat(fandBPerc) + parseFloat(percentageEscalationInRent);
						adSalePerc = parseFloat(adSalePerc) + parseFloat(percentageEscalationInRent);
						othersPerc = parseFloat(othersPerc) + parseFloat(percentageEscalationInRent);
						
					}
						
						if(i%rentFrequencyOfEscaltionYears_fixed==0)
							rent+=(rent*percentageEscalationInRent_fixed/100);
						
					
					}
				
			 
		 }
		
		 else if (rentPaymentTerm == 'other_1') {
			 
			 var count = 1;
			 
			 myTable = $('#tblOther_1');
			 	$('#theadOther_1').find('tr').remove();
				$('#tbodyOther_1').find('tr').remove();
				
				$('#theadFixedTable').find('tr').remove();
				$('#tbodyFixedTable').find('tr').remove();
				
				$('#theadRevenueShare').find('tr').remove();
				$('#tbodyRevenueShareTable').find('tr').remove();
				
				$('#theadFixedTable').find('tr').remove();
				$('#tbodyFixedTable').find('tr').remove();
				
				$('#theadMgRevenueShare').find('tr').remove();
				$('#tbodyMgRevenueShareTable').find('tr').remove();
				
			 
				$('#theadOther_1').append('<tr><th class="fixed-side">Year</th></tr>');
				$('#tbodyOther_1').append('<tr><th class="fixed-side">Share of Box Office Revenue (%)</th></tr>');
				$('#tbodyOther_1').append('<tr><th class="fixed-side">Share of F&B Revenue (%)</th></tr>');
				$('#tbodyOther_1').append('<tr><th class="fixed-side">Share of Ad Revenue (%)</th></tr>');
				$('#tbodyOther_1').append('<tr><th class="fixed-side">Share of Other Revenue (%)</th></tr>');
				$('#tbodyOther_1').append('<tr><th class="fixed-side">Fixed Rent (INR)</th></tr>');
				
				var mg_type_select = $('#mg_type_select').val();
				for(var i=1;i<=noOfColumns;i++){
					$('#tblOther_1 tr').append($("<td>"));
					$('#tblOther_1 thead tr>td:last').html(i);
					
					if('average' == mg_type_select){
						rent = 0;
					}
					
					
					
					var ind=1;
					$('#tblOther_1 tbody tr').each(function(){
						if(ind==1)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" class ="boxPercent" id="b'+i+'" value="'+boxOfcPerc+'" />'));
						else if(ind==2)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" class ="fnbPercent" id="f'+i+'" value="'+fandBPerc+'" />'));
						else if(ind==3)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" class ="adSalePercent" id="a'+i+'" value="'+adSalePerc+'" />'));
						else if(ind==4)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" class ="otherPercent" id="o'+i+'" value="'+othersPerc+'" />'));
						else if(ind==5){
							if(i < startingYearForMG){
								$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']"  value=0 />')) 
							}else{
								$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" class ="fixed" id="y'+i+'" value="'+rent+'" />')) 
							}
							
							
						}
						
						
						ind++;
					});
					
					if(i >= (startingYearForMG - 1) ){
						boxOfcPerc = 0;
						fandBPerc = 0;
						adSalePerc = 0;
						othersPerc = 0;
					}else{
						if(i%rentFrequencyOfEscaltionYears == 0){
							boxOfcPerc = parseFloat(boxOfcPerc) + parseFloat(percentageEscalationInRent);
							fandBPerc = parseFloat(fandBPerc) + parseFloat(percentageEscalationInRent);
							adSalePerc = parseFloat(adSalePerc) + parseFloat(percentageEscalationInRent);
							othersPerc = parseFloat(othersPerc) + parseFloat(percentageEscalationInRent);
							
						}
					}
					
					if(i > startingYearForMG){
						if('fixed' == mg_type_select){
							count ++;
							var sum = parseInt(i) + parseInt(startingYearForMG);
							if(count % rentFrequencyOfEscaltionYears_fixed == 0)
								rent+=(rent*percentageEscalationInRent_fixed/100);
							}
					}
					
					
					
					
				}
					
					
					
				
			 
		 }
		
		 else if (rentPaymentTerm == 'other_2') {
			 
			 myTable = $('#tblOther_2');
			 	$('#theadOther_2').find('tr').remove();
				$('#tbodyOther_2').find('tr').remove();
			 
				$('#theadOther_2').append('<tr><th class="fixed-side">Year</th></tr>');
				$('#tbodyOther_2').append('<tr><th class="fixed-side">Share of Box Office Revenue (%)</th></tr>');
				$('#tbodyOther_2').append('<tr><th class="fixed-side">Share of F&B Revenue (%)</th></tr>');
				$('#tbodyOther_2').append('<tr><th class="fixed-side">Share of Ad Revenue (%)</th></tr>');
				$('#tbodyOther_2').append('<tr><th class="fixed-side">Share of Other Revenue (%)</th></tr>');
				$('#tbodyOther_2').append('<tr><th class="fixed-side">Fixed Rent (INR)</th></tr>');
				
				rent =0;
				boxOfcPerc=0;
				fandBPerc=0;
				adSalePerc=0;
				othersPerc=0;
				
				for(var i=1;i<=noOfColumns;i++){
					$('#tblOther_2 tr').append($("<td>"));
					$('#tblOther_2 thead tr>td:last').html(i);
					 
					var ind=1;
					$('#tblOther_2 tbody tr').each(function(){
						if(ind==1)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" value="'+boxOfcPerc+'" />'));
						else if(ind==2)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" value="'+fandBPerc+'" />'));
						else if(ind==3)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" value="'+adSalePerc+'" />'));
						else if(ind==4)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" value="'+othersPerc+'" />'));
						else if(ind==5)
							$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" value="'+rent+'" />'))
						
						
						ind++;
					});
					
					
					}
				
			 
		 }
		

	} 


	$(document).on('click','#accordion-toggle', function () {
	   $(this).parents().find('.innerTableParent .collapseTable').slideToggle();
	});


/*	
	$(document).on('change keyup', '.camFixed', function() {
		 var noOfColumns = $('#leasePeriod').val();
		 var value = $(this).val();
		 var yr = $(this).attr('id');
		 var year = yr.split('').slice(1);
		 fixedCamAutoGenerate(noOfColumns,value,year);

	});
	
	
	
	function fixedCamAutoGenerate(noOfColumns,value,year){
		const noOfMonths=12;
		var leasableArea = $('#leasableArea').val();
		var yr =  parseInt(year);
		var cam_rent = parseFloat(value);
		var camfrequencyOfEscaltionYears = ($('#camfrequencyOfEscaltionYears').val() == '') ? 0 : parseFloat($('#camfrequencyOfEscaltionYears').val()) ;
		var camPercentageEscalation = ($('#camPercentageEscalation').val() == '') ? 0 : parseFloat($('#camPercentageEscalation').val());
		
			for (var i = yr; i <= noOfColumns; i++) {
				
				if(i%camfrequencyOfEscaltionYears == 0){
					cam_rent+=(cam_rent*camPercentageEscalation/100);
				}
				$("#c" + (i+1)).val(cam_rent);
	
			}
	}*/
	
	
	


	function generateCamTable(noOfColumns) {
		
		//monthly rent to 
		const noOfMonths=12;
		var leasableArea = $('#leasableArea').val();
		var cam_payment_terms = $('#cam_payment_terms').val();
		var cam_rent= ($('#camRate').val()=='') ? 0 : leasableArea * noOfMonths*parseFloat($('#camRate').val());
		var camfrequencyOfEscaltionYears = ($('#camfrequencyOfEscaltionYears').val() == '') ? 0 : parseFloat($('#camfrequencyOfEscaltionYears').val()) ;
		var camPercentageEscalation = ($('#camPercentageEscalation').val() == '') ? 0 : parseFloat($('#camPercentageEscalation').val());
			
		var myTable;
		if (cam_payment_terms == 'fixed'){
				myTable = $('#tblCamFixed');
			
			$('#theadCamFixedTable').find('tr').remove();
			$('#tbodyCamFixedTable').find('tr').remove();
			
			$('#theadCamFixedTable').append('<tr><th class="fixed-side">Year</th></tr>');
			$('#tbodyCamFixedTable').append('<tr><th class="fixed-side">CAM (INR)</th></tr>');
			
			for(var i=1;i<=noOfColumns;i++){
				
				$('#tblCamFixed tr').append($("<td>"));
				$('#tblCamFixed thead tr>td:last').html(i);
				$('#tblCamFixed tbody tr').each(function(){
					
					$(this).children('td:last').append($('<input type="text" name="camTerm.yearWiseCamRent['+i+']" class ="camFixed" id="c'+i+'" value="'+cam_rent+'" />'))
				});
				
					if(i%camfrequencyOfEscaltionYears == 0)
						cam_rent+=(cam_rent*camPercentageEscalation/100);
				}
		}
		
		if (cam_payment_terms == 'actuals'){
	/*		myTable = $('#tblCamFixed');
		
		$('#theadCamFixedTable').find('tr').remove();
		$('#tbodyCamFixedTable').find('tr').remove();
		
		$('#theadCamFixedTable').append('<tr><th class="fixed-side">Year</th></tr>');
		$('#tbodyCamFixedTable').append('<tr><th class="fixed-side">CAM (INR)</th></tr>');
		
		cam_rent = 0;
		
		for(var i=1;i<=noOfColumns;i++){
			
			$('#tblCamFixed tr').append($("<td>"));
			$('#tblCamFixed thead tr>td:last').html(i);
			$('#tblCamFixed tbody tr').each(function(){
				
				$(this).children('td:last').append($('<input type="text" name="camTerm.yearWiseCamRent['+i+']" value="'+cam_rent+'" />'))
			});
			
			
			}*/
	   }
		
		if (cam_payment_terms == 'included'){
			myTable = $('#tblCamFixed');
		
		$('#theadCamFixedTable').find('tr').remove();
		$('#tbodyCamFixedTable').find('tr').remove();
		
		$('#theadCamFixedTable').append('<tr><th class="fixed-side">Year</th></tr>');
		$('#tbodyCamFixedTable').append('<tr><th class="fixed-side">CAM (INR)</th></tr>');
		
		cam_rent = 0;
		
		for(var i=1;i<=noOfColumns;i++){
			
			$('#tblCamFixed tr').append($("<td>"));
			$('#tblCamFixed thead tr>td:last').html(i);
			$('#tblCamFixed tbody tr').each(function(){
				
				$(this).children('td:last').append($('<input type="text" name="camTerm.yearWiseCamRent['+i+']" value="'+cam_rent+'" />'))
			});
			
			
			}
	   }	

	} 



	$('#camType').on('change keyup', function () {
		
	    var camVal = $(this).val();
	    
	    if (camVal == 'month') {
	        $('#monthCam').show();
	        $('#camZeroMg').hide();
	       
	    }
	    if (camVal == 'fixed') {
	        $('#camZeroMg').show();
	        $('#monthCam').hide();
	    }
	    if (camVal == '') {
	        $('#monthCam').hide();
	        $('#camZeroMg').hide();
	       
	    }

	});


	$('#rentType').on('change keyup', function () {
		
	    var camVal = $(this).val();
	    
	    if (camVal == 'month') {
	        $('#monthRent').show();
	        $('#rentZeroMg').hide();
	    }
	    if (camVal == 'fixed') {
	        $('#rentZeroMg').show();
	        $('#monthRent').hide();
	    }
	    if (camVal == '') {
	        $('#monthRent').hide();
	        $('#rentZeroMg').hide();
	       
	    }
	});				
					

	//Generate Advance Rent Adjustment Table And Period Validation::::
	$('#lesserLesse_advanceRentPeriod').on('change keyup', function () {
		
		var advanceRentPeriod = $('#lesserLesse_advanceRentPeriod').val();
		var showedField = $('#lesserLesse_advanceRent_period, #lesserLesse_advance_rent, #advanceRent_fixed_table').css('display', 'block');
		$(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
		
		$("#lesserLesse_advanceRentPeriodErr").text("");
		var advanceRent = $('#lesserLesse_advanceRent').val();
//		if ((Number(advanceRent.trim()) > 0 &&  Number(advanceRentPeriod.trim()) > 0 ) ||  
//				(advanceRent.trim() == "" && advanceRentPeriod.trim() == "")) {
//			$("#lesserLesse_advanceRentPeriodErr").text("");
//			$("#lesserLesse_advanceRentErr").text("");
//
//		}else{
//			if(Number(advanceRentPeriod.trim()) == 0 || advanceRentPeriod.trim() == ""){
//				$("#lesserLesse_advanceRentPeriodErr").text("Period must be grater than 0.");
//			}if(Number(advanceRent.trim()) == 0 || advanceRent.trim() == ""){
//				$("#lesserLesse_advanceRentErr").text("Advance rent must be greater than 0.");
//			}
//		}
		
		generateAdvanceRentTable(advanceRentPeriod);	
		
	});

	//Advance Rent Value validation:::
	$('#lesserLesse_advanceRent').on('change keyup', function () {
		
		$("#lesserLesse_advanceRentErr").text("");
		var advanceRentPeriod = $('#lesserLesse_advanceRentPeriod').val();
		var advanceRent = $('#lesserLesse_advanceRent').val();
//		if ((Number(advanceRent.trim()) > 0 &&  Number(advanceRentPeriod.trim()) > 0 ) ||  
//				(advanceRent.trim() == "" && advanceRentPeriod.trim() == "")) {
//			$("#lesserLesse_advanceRentPeriodErr").text("");
//			$("#lesserLesse_advanceRentErr").text("");
//
//		}else{
//			if(Number(advanceRentPeriod.trim()) == 0 || advanceRentPeriod.trim() == ""){
//				$("#lesserLesse_advanceRentPeriodErr").text("Period must be grater than 0.");
//			}if(Number(advanceRent.trim()) == 0 || advanceRent.trim() == ""){
//				$("#lesserLesse_advanceRentErr").text("Advance rent must be greater than 0.");
//			}
//		}
		
	});

	function generateAdvanceRentTable(noOfColumns) {
		
		if(noOfColumns == "" || Number(noOfColumns) == 0){
			$('#theadAdvanceRentTable').find('tr').remove();
			$('#tbodyAdvanceRentTable').find('tr').remove();	
			
		}else{
		    var adjustmentRate = "";
			
			$('#theadAdvanceRentTable').find('tr').remove();
			$('#tbodyAdvanceRentTable').find('tr').remove();
			
			$('#theadAdvanceRentTable').append('<tr><th class="fixed-side">Year</th></tr>');
			$('#tbodyAdvanceRentTable').append('<tr><th class="fixed-side">Advance Rent amount to be adjusted </br> from Rent on commencement of Cinema operation</th></tr>');
				
			for(var i=1;i<=noOfColumns;i++){
				
				$('#tblAdvanceRent tr').append($("<td>"));
				$('#tblAdvanceRent thead tr>td:last').html(i);
				$('#tblAdvanceRent tbody tr').each(function(){
					
					$(this).children('td:last').append($('<input type="text" name="lesserLesse.yearWiseAdvanceRentAdjustment['+i+']" id="adjustment'+i+'" class="adjustmentRate" value="'+adjustmentRate+'" placeholder="Enter value"/>'))
				});	
		    }
		}
	} 

	function generateAdjustmentRentTableOnEdit(noOfColumns,lesserLesse){
		
		$('#theadAdvanceRentTable').find('tr').remove();
		$('#tbodyAdvanceRentTable').find('tr').remove();
		
		$('#theadAdvanceRentTable').append('<tr><th class="fixed-side">Year</th></tr>');
		$('#tbodyAdvanceRentTable').append('<tr><th class="fixed-side">Advance Rent amount to be adjusted </br> from Rent on commencement of Cinema operation</th></tr>');
			
		for(var i=1;i<=noOfColumns;i++){
			
			$('#tblAdvanceRent tr').append($("<td>"));
			$('#tblAdvanceRent thead tr>td:last').html(i);
			$('#tblAdvanceRent tbody tr').each(function(){
				
				$(this).children('td:last').append($('<input type="text" name="lesserLesse.yearWiseAdvanceRentAdjustment['+i+']" value='+lesserLesse.yearWiseAdvanceRentAdjustment[i]+' id="adjustment'+i+'" class="adjustmentRate" placeholder="Enter value"/>'))
			});	
	    }
	} 

	//Year wise rent validation:::
	$('#tblAdvanceRent').on('change keyup',  '.adjustmentRate', function(){
		var advanceRentPeriod = $('#lesserLesse_advanceRentPeriod').val();
		var advanceRent = $('#lesserLesse_advanceRent').val();

		var adjustmentSum = 0;
		for(var i =1;i<= advanceRentPeriod;i++){
			adjustmentSum += Number($('#adjustment'+i).val());
		}
		if( advanceRent != adjustmentSum){
			$("#advanceRent_fixed_tableErr").text("Adjustment sum for all year must be equal to advance rent");
		}else{
			$("#advanceRent_fixed_tableErr").text("");
		}
	})					
			
			


function validate() {

	var lease = document.getElementById("leasePeriod").value;
	var pvrLock = document.getElementById("pvrLock").value;
	var developer = document.getElementById("deve1").value;
	var carpet = document.getElementById("carpetArea").value;
	var loadingPercent = document.getElementById("loadingPercentage").value;
	var leasableArea = document.getElementById("leasableArea").value;
	var coveredArea = document.getElementById("coveredArea").value;
	var height = document.getElementById("height").value;
	var rentPayTerm = document.getElementById("rentPaymentTerm").value;
	var rent = document.getElementById("rent").value;
	var rentFrequency = document
			.getElementById("rentFrequencyOfEscaltionYears_fixed").value;
	var percentageEscalations1 = document
			.getElementById("percentageEscalationInRent_fixed").value;
	var box_office = document.getElementById("box_office_percentage").value;
	var fandb = document.getElementById("f_and_b_percentage").value;
	var addsale = document.getElementById("ad_sale_percentage").value;
	var otherspercent = document.getElementById("others_percentage").value;
	var rentFrequencyOfEscaltionYears = document
			.getElementById("rentFrequencyOfEscaltionYears").value;
	// var fandb1 = document.getElementById("F&B").value;
	var explainRentAgree = document.getElementById("explainRentAgreement").value;
	var percentageEscalation = document
			.getElementById("percentageEscalationInRent").value;
	var startingYearForMG = $('#startingYearForMG').val();

	var camRate = $('#camRate').val();
	var camfrequencyOfEscaltionYears = $('#camfrequencyOfEscaltionYears').val();
	var camPercentageEscalation = $('#camPercentageEscalation').val();

	var ExplaincamAgreement = $('#camAgreement').val();
	var securitydeposite = $('#hvac').val();
	var securitydepositeNo = $('#hvac1').val();
	var camSecuritydeposit = $('#fireAndSafety').val();
	var rentsecuritydeposit = $('#rentSecuritydeposit').val();
	var electriCity = $('#electricity').val();
	var paymentschedule = $('#paymentSchedule').val();
	var fromdeveloper = $('#fromDeveloper').val();
	var todeveloper = $('#toDevloper').val();
	var propertytax = $('#propertyTax').val();
	var advanceRent = $('#lesserLesse_advanceRent').val();
	var advanceRentPeriod = $('#lesserLesse_advanceRentPeriod').val();
	var adjustmentSum = 0;
	for(var i =1;i<= advanceRentPeriod;i++){
		adjustmentSum += Number($('#adjustment'+i).val());//.yearWiseAdvanceRentAdjustment[i];
	}

	$("#leaseperiodError").text("");
	$("#pvrLockError").text("");
	$("#developer1Error").text("");
	$("#carpetAreaError").text("");
	$("#loadingPercentageError").text("");
	$("#leasableAreaError").text("");
	$("#coveredAreaError").text("");
	$("#heightMsg").text("");
	$("#paymentTermError").text("");
	$("#rentMsg").text("");
	$("#freqEscError").text("");
	$("#percEscError").text("");
	$("#box_officeErr").text("");
	$("#fandbErr").text("");
	$("#addsaleErr").text("");
	$("#otherspercentErr").text("");
	$("#percentageEscalationError").text("");
	$("#rentFrequencyYearErr").text("");
	$("#startingYearError").text("");
	$("#camRateError").text("");
	$("#camfrequencyOfEscaltionYearsErr").text("");
	$("#camPercentageEscalationErr").text("");
	$("#camAggrementErr").text("");
	$("#camTypeErr").text("");
	$("#securityDeposit").text("");
	$("#securitydepositeErr").text("");
	$("#fireandsafetyerr").text("");
	$("#electriCityerr").text("");
	$("#propertyTaxErr").text("");
	$("#paymentScheduleErr").text("");
	$("#fromdevelopererr").text("");
	$("#todevelopererr").text("");
	$("#additionalDepositeIdErr").text("");
	$("#rentsecuritydepositerr").text("");
	
	$("#lesserLesse_advanceRentErr").text("");
	$("#lesserLesse_advanceRentPeriodErr").text("");
	$("#advanceRent_fixed_tableErr").text("");

	var isValidate = true;

	if (lease.trim() == "" || lease.trim() == null) {
		$("#leaseperiodError").text("Required");
		return false;
	}
	if (lease.trim() != "" &&  pvrLock.trim() != "" &&  developer.trim() != "" ) {
		var a = parseInt(lease.trim());
		var b = parseInt(pvrLock.trim());
		var c = parseInt(developer.trim());
		console.log(a,b)
		if(b>a){
			$("#pvrLockError").text("PVR Lock in Period must be less than or equal to Lease Period");
			return false;
		}
		if(c>a){
			$("#developer1Error").text("Developer Lock in Period must be less than or equal to Lease Period");
			return false;
		}
		
	}
	
	else if (pvrLock.trim() == "" || pvrLock.trim() == null) {
		$("#pvrLockError").text("Required");
		return false;
	}
	else if (developer.trim() == "" || developer.trim() == null) {
		$("#developer1Error").text("Required");
		return false;
	}
	else if (carpet.trim() == "" || carpet.trim() == null) {
		$("#carpetAreaError").text("Required");
		return false;
	}
	else if (loadingPercent.trim() == "" || loadingPercent.trim() == null) {
		$("#loadingPercentageError").text("Required");
		return false;
	}
	else if (leasableArea.trim() == "" || leasableArea.trim() == null) {
		$("#leasableAreaError").text("Required");
		return false;
	}
	else if (coveredArea.trim() == "" || coveredArea.trim() == null) {
		$("#coveredAreaError").text("Required");
		return false;
	}
	else if (height.trim() == "" || height.trim() == null) {
		$("#heightMsg").text("Required");
		return false;
	}
	
	switch (rentPayTerm) {
	case "select_terms": {
		$("#paymentTermError").text("Required");
			return false;
			break;
	}
	case "fixed": {
		if (rent.trim() == "" || rent.trim() == null) {
			$("#rentMsg").text("Required");
			return false;
		}
		else if (rentFrequencyOfEscaltionYears.trim() == ""
				|| rentFrequencyOfEscaltionYears.trim() == null) {
			$("#rentFrequencyYearErr").text("Required");
			return false;
		}
		else if (percentageEscalation.trim() == ""
				|| percentageEscalation.trim() == null) {
			$("#percentageEscalationError").text("Required");
			return false;
		}
		break;
	}
	case "revenue_share": {
		if (box_office.trim() == "" || box_office.trim() == null) {
			$("#box_officeErr").text("Required");
			return false;
		}
		else if (fandb.trim() == "" || fandb.trim() == null) {
			$("#fandbErr").text("Required");
			return false;
		}
		else if (addsale.trim() == "" || addsale.trim() == null) {
			$("#addsaleErr").text("Required");
			return false;
		}
		else if (otherspercent.trim() == "" || otherspercent.trim() == null) {
			$("#otherspercentErr").text("Required");
			return false;
		}
		else if (rentFrequencyOfEscaltionYears.trim() == ""
				|| rentFrequencyOfEscaltionYears.trim() == null) {
			$("#rentFrequencyYearErr").text("Required");
			return false;
		}
		else if (percentageEscalation.trim() == ""
				|| percentageEscalation.trim() == null) {
			$("#percentageEscalationError").text("Required");
			return false;
		}
		break;
	}

	case "mg_revenue_share": {
		if (rent.trim() == "" || rent.trim() == null) {
			$("#rentMsg").text("Required");
			return false;
		}
		else if (rentFrequency.trim() == "" || rentFrequency.trim() == null) {
			$("#freqEscError").text("Required");
			return false;
		}
		else if (box_office.trim() == "" || box_office.trim() == null) {
			$("#box_officeErr").text("Required");
			return false;
		}
		else if (fandb.trim() == "" || fandb.trim() == null) {
			$("#fandbErr").text("Required");
			return false;
		}

		else if (addsale.trim() == "" || addsale.trim() == null) {
			$("#addsaleErr").text("Required");
			return false;
		}
		else if (otherspercent.trim() == "" || otherspercent.trim() == null) {
			$("#otherspercentErr").text("Required");
			return false;
		}

		else if (rentFrequencyOfEscaltionYears.trim() == ""
				|| rentFrequencyOfEscaltionYears.trim() == null) {
			$("#rentFrequencyYearErr").text("Required");
			return false;
		}
		else if (percentageEscalation.trim() == ""
				|| percentageEscalation.trim() == null) {
			$("#percentageEscalationError").text("Required");
			return false;
		}
		else if (percentageEscalations1.trim() == ""
				|| percentageEscalations1.trim() == null) {
			$("#percEscError").text("Required");
			return false;
		}
		break;
	}
	case "other_1": {
		if (startingYearForMG.trim() == "" || startingYearForMG.trim() == null) {
			$("#startingYearError").text("Required");
			return false;
		}
		var mg_type_select = $('#mg_type_select').find(":selected").val();
		if (mg_type_select == "fixed") {
			if (rent.trim() == "" || rent.trim() == null) {
				$("#rentMsg").text("Required");
				return false;
			}
			else if (rentFrequency.trim() == "" || rentFrequency.trim() == null) {
				$("#freqEscError").text("Required");
				return false;
			}
			else if (box_office.trim() == "" || box_office.trim() == null) {
				$("#box_officeErr").text("Required");
				return false;
			}
			else if (fandb.trim() == "" || fandb.trim() == null) {
				$("#fandbErr").text("Required");
				return false;
			}

			else if (addsale.trim() == "" || addsale.trim() == null) {
				$("#addsaleErr").text("Required");
				return false;
			}
			else if (otherspercent.trim() == "" || otherspercent.trim() == null) {
				$("#otherspercentErr").text("Required");
				return false;
			}

			else if (rentFrequencyOfEscaltionYears.trim() == ""
					|| rentFrequencyOfEscaltionYears.trim() == null) {
				$("#rentFrequencyYearErr").text("Required");
				return false;
			}
			else if (percentageEscalation.trim() == ""
					|| percentageEscalation.trim() == null) {
				$("#percentageEscalationError").text("Required");
				return false;
			}
			else if (percentageEscalations1.trim() == ""
					|| percentageEscalations1.trim() == null) {
				$("#percEscError").text("Required");
				return false;
			}
		}
		if (mg_type_select == "average") {
			if (box_office.trim() == "" || box_office.trim() == null) {
				$("#box_officeErr").text("Required");
				return false;
			}
			else if (fandb.trim() == "" || fandb.trim() == null) {
				$("#fandbErr").text("Required");
				return false;
			}

			else if (addsale.trim() == "" || addsale.trim() == null) {
				$("#addsaleErr").text("Required");
				return false;
			}
			else if (otherspercent.trim() == "" || otherspercent.trim() == null) {
				$("#otherspercentErr").text("Required");
				return false;
			}

			else if (rentFrequencyOfEscaltionYears.trim() == ""
					|| rentFrequencyOfEscaltionYears.trim() == null) {
				$("#rentFrequencyYearErr").text("Required");
				return false;
			}
			else if (percentageEscalation.trim() == ""
					|| percentageEscalation.trim() == null) {
				$("#percentageEscalationError").text("Required");
				return false;
			}
		}
		break;
	}
	}
	if (explainRentAgree.trim() == "" || explainRentAgree.trim() == null) {
		$("#rentAggrementError").text("Required");
		return false;
	}

	var camtermscurrentval = $('#cam_payment_terms').find(":selected").val();

	if (camtermscurrentval == "") {
		$("#camPay").text("Required");
		return false;
	}
	else if (camtermscurrentval == "fixed") {
		if (camRate.trim() == "") {
			$("#camRateError").text("Required");
			return false;
		}
		else if (camfrequencyOfEscaltionYears.trim() == "") {
			$("#camfrequencyOfEscaltionYearsErr").text("Required");
			return false;
		}
		else if (camPercentageEscalation.trim() == "") {
			$("#camPercentageEscalationErr").text("Required");
			return false;
		}

	}
	if (ExplaincamAgreement.trim() == "" || ExplaincamAgreement.trim() == null) {
		$("#camAggrementErr").text("Required");
		return false;
	}

	var camType = $('#camType').find(":selected").val();
	if (camType == "") {
		$("#camTypeErr").text("Required");
		return false;
	}
	if (camType == "month") {
		if (securitydeposite.trim() == "" || securitydeposite.trim() == null) {
			$("#securityDeposit").text("Required");
			return false;
		}
	}
	if (camType == "fixed") {

		if (camSecuritydeposit.trim() == ""
				|| camSecuritydeposit.trim() == null) {
			$("#fireandsafetyerr").text("Required");
			return false;
		}
	}
	var rentType = $('#rentType').find(":selected").val();
	if (rentType == "") {
		$("#rentTypeErr").text("Required");
		return false;
	}
	if (rentType == "month") {
		if (securitydepositeNo.trim() == ""
				|| securitydepositeNo.trim() == null) {
			$("#securitydepositeErr").text("Required");
			return false;
		}
	}
	if (rentType == "fixed") {

		if (rentsecuritydeposit.trim() == ""
				|| rentsecuritydeposit.trim() == null) {
			$("#rentsecuritydepositerr").text("Required");
			return false;
		}
	}
	if (electriCity.trim() == "" || electriCity.trim() == null) {
		$("#electriCityerr").text("Required");
		return false;
	}

	var additionalDepositeId = $('#additionalDepositeId').find(":selected")
			.val();
	if (additionalDepositeId == "") {
		$("#additionalDepositeIdErr").text("Required");
		return false;
	}
	if (paymentschedule.trim() == "" || paymentschedule.trim() == null) {
		$("#paymentScheduleErr").text("Required");
		return false;
	}
	if (fromdeveloper.trim() == "" || fromdeveloper.trim() == null) {
		$("#fromdevelopererr").text("Required");
		return false;
	}

	if (todeveloper.trim() == "" || todeveloper.trim() == null) {
		$("#todevelopererr").text("Required");
		return false;
	}

	if (propertytax.trim() == "" || propertytax.trim() == null) {
		$("#propertyTaxErr").text("Required");
		return false;
	}
	
	//Advance Rent Validation:::
//	if ((Number(advanceRent.trim()) > 0 &&  Number(advanceRentPeriod.trim()) > 0 ) ||  
//			(advanceRent.trim() == "" && advanceRentPeriod.trim() == "")) {
//		//do nothing::
//	}else{
//		if(Number(advanceRentPeriod.trim()) == 0 || advanceRentPeriod.trim() == ""){
//			$("#lesserLesse_advanceRentPeriodErr").text("Required");
//			return false;
//		}if(Number(advanceRent.trim()) == 0 || advanceRent.trim() == ""){
//			$("#lesserLesse_advanceRentErr").text("Required");
//			return false;
//		}
//	}
	
	if( advanceRent != adjustmentSum){
		$("#advanceRent_fixed_tableErr").text("Adjustment sum must be equal to advance rent");
		return false;
	}

	return true ;
}



$('#save').click(function() {
	if(validate()){
		form = $('#lesserLesseeForm');
		form.attr('action', '/post-signing/lessor-lessee-schedule/').trigger('submit');
	}

});

$('#save_close').click(function() {
form = $('#lesserLesseeForm');
form.attr('action', '/post-signing/lessor-lessee-schedule-save/').trigger('submit');
});



$('#lesserLesseView').click(function() {
	var option = $('#CAMPaymentTerm').find(":selected").text();
	var selected_option = $('#CAMPaymentTerm').val();
	//console.log(selected_option)
	switch (selected_option) {
	case "Option_1":
		 $("#1").html('<img src="../../images/right-arrow.png"/>');
         $("#2").html('<img src="../../images/right-arrow.png"/>');
         $("#3").html('<img src="../../images/right-arrow.png"/>');
         $("#4").html('<img src="../../images/right-arrow.png"/>');
         $("#5").html('<img src="../../images/right-arrow.png"/>');
         $("#6").html('<img src="../../images/right-arrow.png"/>');
         $("#7").html('<img src="../../images/right-arrow.png"/>');
         $("#8").html('<img src="../../images/right-arrow.png"/>');
         $("#9").html('<img src="../../images/right-arrow.png"/>');
         $("#10").html('<img src="../../images/right-arrow.png"/>');
         $("#11").html('<img src="../../images/right-arrow.png"/>');
         $("#12").html('<img src="../../images/right-arrow.png"/>');
         $("#13").html('<img src="../../images/right-arrow.png"/>');
         $("#14").html('<img src="../../images/right-arrow.png"/>');
         $("#15").html('<img src="../../images/right-arrow.png"/>');
         $("#16").html('<img src="../../images/right-arrow.png"/>');
         $("#17").html('<img src="../../images/right-arrow.png"/>');
         $("#18").html('<img src="../../images/right-arrow.png"/>');
         $("#19").html('<img src="../../images/right-arrow.png"/>');
         $("#20").html('<img src="../../images/right-arrow.png"/>');
         $("#21").html('<img src="../../images/right-arrow.png"/>');
         $("#22").html('<img src="../../images/right-arrow.png"/>');
         $("#23").html('<img src="../../images/right-arrow.png"/>');
         $("#24").html('<img src="../../images/right-arrow.png"/>');
         $("#25").html('<img src="../../images/right-arrow.png"/>');
         $("#26").html('<img src="../../images/right-arrow.png"/>');
         $("#27").html('<img src="../../images/right-arrow.png"/>');
         $("#28").html('<img src="../../images/right-arrow.png"/>');
         $("#29").html('<img src="../../images/right-arrow.png"/>');
         $("#30").html('<img src="../../images/right-arrow.png"/>');
         $("#31").html('<img src="../../images/right-arrow.png"/>');
         $("#32").html('<img src="../../images/right-arrow.png"/>');
         $("#33").html('<img src="../../images/right-arrow.png"/>');
         $("#34").html('<img src="../../images/right-arrow.png"/>');
         $("#35").html('<img src="../../images/right-arrow.png"/>');
         $("#36").html('<img src="../../images/right-arrow.png"/>');
         $("#37").html('<img src="../../images/right-arrow.png"/>');
         $("#38").html('<img src="../../images/right-arrow.png"/>');
         $("#39").html('<img src="../../images/right-arrow.png"/>');
         
         $("#40").html('<img src="../../images/right-arrow.png"/>');
         $("#41").html('<img src="../../images/right-arrow.png"/>');
         $("#42").html('<img src="../../images/right-arrow.png"/>');
         $("#43").html('<img src="../../images/right-arrow.png"/>');
         $("#44").html('<img src="../../images/right-arrow.png"/>');
         $("#45").html('<img src="../../images/right-arrow.png"/>');
         $("#46").html('<img src="../../images/right-arrow.png"/>');
         $("#47").html('<img src="../../images/right-arrow.png"/>');
         $("#48").html('<img src="../../images/right-arrow.png"/>');
         
         $("#49").html('<img src="../../images/right-arrow.png"/>');
         $("#50").html('<img src="../../images/right-arrow.png"/>');
         $("#51").html('<img src="../../images/right-arrow.png"/>');
         $("#52").html('<img src="../../images/right-arrow.png"/>');
         $("#53").html('<img src="../../images/right-arrow.png"/>');
         $("#54").html('<img src="../../images/right-arrow.png"/>');
         $("#55").html('<img src="../../images/right-arrow.png"/>');
         $("#56").html('<img src="../../images/right-arrow.png"/>');
         $("#57").html('<img src="../../images/right-arrow.png"/>');
         $("#58").html('<img src="../../images/right-arrow.png"/>');
         $("#59").html('<img src="../../images/right-arrow.png"/>');
         $("#60").html('<img src="../../images/right-arrow.png"/>');
         $("#61").html('<img src="../../images/right-arrow.png"/>');
         $("#62").html('<img src="../../images/right-arrow.png"/>');
         $("#63").html('<img src="../../images/right-arrow.png"/>');
         $("#64").html('<img src="../../images/right-arrow.png"/>');
         $("#65").html('<img src="../../images/right-arrow.png"/>');
         $("#66").html('<img src="../../images/right-arrow.png"/>');
         $("#67").html('<img src="../../images/right-arrow.png"/>');
         $("#68").html('<img src="../../images/right-arrow.png"/>');
         $("#69").html('<img src="../../images/right-arrow.png"/>');
         $("#70").html('<img src="../../images/right-arrow.png"/>');
         $("#71").html('<img src="../../images/right-arrow.png"/>');
         $("#72").html('<img src="../../images/right-arrow.png"/>');
         
         $("#73").html('<img src="../../images/right-arrow.png"/>');
         $("#74").html('<img src="../../images/right-arrow.png"/>');
		break;
	case "Option_2":
		 $("#1").html('<img src="../../images/right-arrow.png"/>');
        $("#2").html('<img src="../../images/right-arrow.png"/>');
        $("#3").html('<img src="../../images/right-arrow.png"/>');
        $("#4").html('<img src="../../images/right-arrow.png"/>');
        $("#5").html('<img src="../../images/right-arrow.png"/>');
        $("#6").html('<img src="../../images/right-arrow.png"/>');
        $("#7").html('<img src="../../images/right-arrow.png"/>');
        $("#8").html('<img src="../../images/right-arrow.png"/>');
        $("#9").html('<img src="../../images/right-arrow.png"/>');
        $("#10").html('<img src="../../images/right-arrow.png"/>');
        $("#11").html('<img src="../../images/right-arrow.png"/>');
        $("#12").html('<img src="../../images/right-arrow.png"/>');
        $("#13").html('<img src="../../images/right-arrow.png"/>');
        $("#14").html('<img src="../../images/right-arrow.png"/>');
        $("#15").html('<img src="../../images/right-arrow.png"/>');
        $("#16").html('<img src="../../images/right-arrow.png"/>');
        $("#17").html('<b>X</b>');
        $("#18").html('<b>X</b>');
        $("#19").html('<img src="../../images/right-arrow.png"/>');
        $("#20").html('<img src="../../images/right-arrow.png"/>');
        $("#21").html('<img src="../../images/right-arrow.png"/>');
        $("#22").html('<img src="../../images/right-arrow.png"/>');
        $("#23").html('<img src="../../images/right-arrow.png"/>');
        $("#24").html('<b>X</b>');
        $("#25").html('<b>X</b>');
        $("#26").html('<b>X</b>');
        $("#27").html('<img src="../../images/right-arrow.png"/>');
        $("#28").html('<img src="../../images/right-arrow.png"/>');
        $("#29").html('<b>X</b>');
        $("#30").html('<img src="../../images/right-arrow.png"/>');
        $("#31").html('<b>X</b>');
        $("#32").html('<img src="../../images/right-arrow.png"/>');
        $("#33").html('<img src="../../images/right-arrow.png"/>');
        $("#34").html('<img src="../../images/right-arrow.png"/>');
        $("#35").html('<img src="../../images/right-arrow.png"/>');
        $("#36").html('<img src="../../images/right-arrow.png"/>');
        $("#37").html('<img src="../../images/right-arrow.png"/>');
        $("#38").html('<b>X</b>');
        $("#39").html('<img src="../../images/right-arrow.png"/>');
        $("#40").html('<img src="../../images/right-arrow.png"/>');
        $("#41").html('<img src="../../images/right-arrow.png"/>');
        $("#42").html('<img src="../../images/right-arrow.png"/>');
        $("#43").html('<img src="../../images/right-arrow.png"/>');
        $("#44").html('<img src="../../images/right-arrow.png"/>');
        $("#45").html('<img src="../../images/right-arrow.png"/>');
        $("#46").html('<img src="../../images/right-arrow.png"/>');
        $("#47").html('<img src="../../images/right-arrow.png"/>');
        $("#48").html('<img src="../../images/right-arrow.png"/>');
        $("#49").html('<img src="../../images/right-arrow.png"/>');
        $("#50").html('<img src="../../images/right-arrow.png"/>');
        $("#51").html('<img src="../../images/right-arrow.png"/>');
        $("#52").html('<img src="../../images/right-arrow.png"/>');
        $("#53").html('<img src="../../images/right-arrow.png"/>');
        $("#54").html('<img src="../../images/right-arrow.png"/>');
        $("#55").html('<img src="../../images/right-arrow.png"/>');
        $("#56").html('<img src="../../images/right-arrow.png"/>');
        $("#57").html('<img src="../../images/right-arrow.png"/>');
        $("#58").html('<img src="../../images/right-arrow.png"/>');
        $("#59").html('<img src="../../images/right-arrow.png"/>');
        $("#60").html('<img src="../../images/right-arrow.png"/>');
        $("#61").html('<img src="../../images/right-arrow.png"/>');
        $("#62").html('<img src="../../images/right-arrow.png"/>');
        $("#63").html('<img src="../../images/right-arrow.png"/>');
        $("#64").html('<img src="../../images/right-arrow.png"/>');
        $("#65").html('<img src="../../images/right-arrow.png"/>');
        $("#66").html('<img src="../../images/right-arrow.png"/>');
        $("#67").html('<img src="../../images/right-arrow.png"/>');
        $("#68").html('<img src="../../images/right-arrow.png"/>');
        $("#69").html('<img src="../../images/right-arrow.png"/>');
        $("#70").html('<img src="../../images/right-arrow.png"/>');
        $("#71").html('<img src="../../images/right-arrow.png"/>');
        $("#72").html('<img src="../../images/right-arrow.png"/>');
        $("#73").html('<img src="../../images/right-arrow.png"/>');
        $("#74").html('<img src="../../images/right-arrow.png"/>');
		break;
	case "Option_3":
		 $("#1").html('<img src="../../images/right-arrow.png"/>');
       $("#2").html('<img src="../../images/right-arrow.png"/>');
       $("#3").html('<img src="../../images/right-arrow.png"/>');
       $("#4").html('<img src="../../images/right-arrow.png"/>');
       $("#5").html('<img src="../../images/right-arrow.png"/>');
       $("#6").html('<img src="../../images/right-arrow.png"/>');
       $("#7").html('<img src="../../images/right-arrow.png"/>');
       $("#8").html('<img src="../../images/right-arrow.png"/>');
       $("#9").html('<img src="../../images/right-arrow.png"/>');
       $("#10").html('<img src="../../images/right-arrow.png"/>');
       $("#11").html('<img src="../../images/right-arrow.png"/>');
       $("#12").html('<img src="../../images/right-arrow.png"/>');
       $("#13").html('<img src="../../images/right-arrow.png"/>');
       $("#14").html('<img src="../../images/right-arrow.png"/>');
       $("#15").html('<img src="../../images/right-arrow.png"/>');
       $("#16").html('<img src="../../images/right-arrow.png"/>');
       $("#17").html('<b>X</b>');
       $("#18").html('<b>X</b>');
       $("#19").html('<img src="../../images/right-arrow.png"/>');
       $("#20").html('<img src="../../images/right-arrow.png"/>');
       $("#21").html('<img src="../../images/right-arrow.png"/>');
       $("#22").html('<img src="../../images/right-arrow.png"/>');
       $("#23").html('<img src="../../images/right-arrow.png"/>');
       $("#24").html('<b>X</b>');
       $("#25").html('<b>X</b>');
       $("#26").html('<b>X</b>');
       $("#27").html('<img src="../../images/right-arrow.png"/>');
       $("#28").html('<img src="../../images/right-arrow.png"/>');
       $("#29").html('<b>X</b>');
       $("#30").html('<img src="../../images/right-arrow.png"/>');
       $("#31").html('<b>X</b>');
       $("#32").html('<img src="../../images/right-arrow.png"/>');
       $("#33").html('<img src="../../images/right-arrow.png"/>');
       $("#34").html('<img src="../../images/right-arrow.png"/>');
       $("#35").html('<img src="../../images/right-arrow.png"/>');
       $("#36").html('<img src="../../images/right-arrow.png"/>');
       $("#37").html('<img src="../../images/right-arrow.png"/>');
       $("#38").html('<b>X</b>');
       $("#39").html('<img src="../../images/right-arrow.png"/>');
       $("#40").html('<img src="../../images/right-arrow.png"/>');
       $("#41").html('<img src="../../images/right-arrow.png"/>');
       $("#42").html('<img src="../../images/right-arrow.png"/>');
       $("#43").html('<img src="../../images/right-arrow.png"/>');
       $("#44").html('<img src="../../images/right-arrow.png"/>');
       $("#45").html('<img src="../../images/right-arrow.png"/>');
       $("#46").html('<b>X</b>');
       $("#47").html('<b>X</b>');
       $("#48").html('<img src="../../images/right-arrow.png"/>');
       $("#49").html('<img src="../../images/right-arrow.png"/>');
       $("#50").html('<img src="../../images/right-arrow.png"/>');
       $("#51").html('<img src="../../images/right-arrow.png"/>');
       $("#52").html('<img src="../../images/right-arrow.png"/>');
       $("#53").html('<img src="../../images/right-arrow.png"/>');
       $("#54").html('<img src="../../images/right-arrow.png"/>');
       $("#55").html('<img src="../../images/right-arrow.png"/>');
       $("#56").html('<img src="../../images/right-arrow.png"/>');
       $("#57").html('<img src="../../images/right-arrow.png"/>');
       $("#58").html('<img src="../../images/right-arrow.png"/>');
       $("#59").html('<img src="../../images/right-arrow.png"/>');
       $("#60").html('<img src="../../images/right-arrow.png"/>');
       $("#61").html('<img src="../../images/right-arrow.png"/>');
       $("#62").html('<img src="../../images/right-arrow.png"/>');
       $("#63").html('<img src="../../images/right-arrow.png"/>');
       $("#64").html('<img src="../../images/right-arrow.png"/>');
       $("#65").html('<img src="../../images/right-arrow.png"/>');
       $("#66").html('<img src="../../images/right-arrow.png"/>');
       $("#67").html('<img src="../../images/right-arrow.png"/>');
       $("#68").html('<img src="../../images/right-arrow.png"/>');
       $("#69").html('<img src="../../images/right-arrow.png"/>');
       $("#70").html('<img src="../../images/right-arrow.png"/>');
       $("#71").html('<img src="../../images/right-arrow.png"/>');
       $("#72").html('<img src="../../images/right-arrow.png"/>');
       $("#73").html('<img src="../../images/right-arrow.png"/>');
       $("#74").html('<img src="../../images/right-arrow.png"/>');
		break;
	case "Option_4":
		 $("#1").html('<img src="../../images/right-arrow.png"/>');
       $("#2").html('<img src="../../images/right-arrow.png"/>');
       $("#3").html('<img src="../../images/right-arrow.png"/>');
       $("#4").html('<img src="../../images/right-arrow.png"/>');
       $("#5").html('<img src="../../images/right-arrow.png"/>');
       $("#6").html('<img src="../../images/right-arrow.png"/>');
       $("#7").html('<img src="../../images/right-arrow.png"/>');
       $("#8").html('<img src="../../images/right-arrow.png"/>');
       $("#9").html('<img src="../../images/right-arrow.png"/>');
       $("#10").html('<img src="../../images/right-arrow.png"/>');
       $("#11").html('<img src="../../images/right-arrow.png"/>');
       $("#12").html('<img src="../../images/right-arrow.png"/>');
       $("#13").html('<img src="../../images/right-arrow.png"/>');
       $("#14").html('<img src="../../images/right-arrow.png"/>');
       $("#15").html('<img src="../../images/right-arrow.png"/>');
       $("#16").html('<img src="../../images/right-arrow.png"/>');
       $("#17").html('<b>X</b>');
       $("#18").html('<b>X</b>');
       $("#19").html('<b>X</b>');
       $("#20").html('<b>X</b>');
       $("#21").html('<b>X</b>');
       $("#22").html('<b>X</b>');
       $("#23").html('<b>X</b>');
       $("#24").html('<b>X</b>');
       $("#25").html('<b>X</b>');
       $("#26").html('<b>X</b>');
       $("#27").html('<b>X</b>');
       $("#28").html('<b>X</b>');
       $("#29").html('<b>X</b>');
       $("#30").html('<b>X</b>');
       $("#31").html('<b>X</b>');
       $("#32").html('<b>X</b>');
       $("#33").html('<b>X</b>');
       $("#34").html('<b>X</b>');
       $("#35").html('<b>X</b>');
       $("#36").html('<b>X</b>');
       $("#37").html('<b>X</b>');
       $("#38").html('<b>X</b>');
       $("#39").html('<b>X</b>');
       $("#40").html('<b>X</b>');
       $("#41").html('<b>X</b>');
       $("#42").html('<b>X</b>');
       $("#43").html('<b>X</b>');
       $("#44").html('<b>X</b>');
       $("#45").html('<b>X</b>');
       $("#46").html('<b>X</b>');
       $("#47").html('<b>X</b>');
       $("#48").html('<b>X</b>');
       $("#49").html('<b>X</b>');
       $("#50").html('<b>X</b>');
       $("#51").html('<b>X</b>');
       $("#52").html('<b>X</b>');
       $("#53").html('<b>X</b>');
       $("#54").html('<b>X</b>');
       $("#55").html('<b>X</b>');
       $("#56").html('<b>X</b>');
       $("#57").html('<b>X</b>');
       $("#58").html('<b>X</b>');
       $("#59").html('<b>X</b>');
       $("#60").html('<b>X</b>');
       $("#61").html('<b>X</b>');
       $("#62").html('<b>X</b>');
       $("#63").html('<b>X</b>');
       $("#64").html('<b>X</b>');
       $("#65").html('<b>X</b>');
       $("#66").html('<b>X</b>');
       $("#67").html('<b>X</b>');
       $("#68").html('<b>X</b>');
       $("#69").html('<b>X</b>');
       $("#70").html('<b>X</b>');
       $("#71").html('<b>X</b>');
       $("#72").html('<b>X</b>');
       $("#73").html('<b>X</b>');
       $("#74").html('<b>X</b>');
		break;
	default:
		break;
	}
	$('.selected_option').text(option);
	$('#lesserLesseModal').modal();
	
});



















