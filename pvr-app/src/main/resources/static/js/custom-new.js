$(window).on('load', function(){
	// This script use for comma separated value
	 /*$(":input, p").each(function() {
	    var _inputs = $(this).val();
	    var _valParagraph = $(this).text();
	   $(this).val(_inputs.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	    $(this).text(_valParagraph.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	  });*/
});
var pathname;
$(document)
		.ready(
				function() { // First Document Ready Start Here
					
					  pathname = window.location.host;
					
					// This script use for comma separated value
					/*$(':input , p').on('change keyup',function(){
						$(this).each(function() {
						    var _inputs = $(this).val();
						    var _valParagraph = $(this).text();
						  /*  $(this).val(_inputs.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
						    $(this).text(_valParagraph.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
						  });*/
					/*});*/

					// Bootstrap Material Design
					$('body').bootstrapMaterialDesign();
					$(document).ready(function() {
						$('.leftmenutrigger').on('click', function(e) {
							$('.side-nav').toggleClass("open");
							e.preventDefault();
						});
					});

					$('#from').bootstrapMaterialDatePicker({
						time: false,
						weekStart : 0
					});
					$('#to').bootstrapMaterialDatePicker({
						time: false,
						weekStart : 0
					});

					$('#from1').bootstrapMaterialDatePicker({
						time: false,
						weekStart : 0
					});
					$('#to1').bootstrapMaterialDatePicker({
						time: false,
						weekStart : 0
					});
					
					// Hamburger On Responsive
					var wrapperMenu = document.querySelector('.wrapper-menu');
					var sideBar = document.querySelector('.sidebar ');
					wrapperMenu.addEventListener('click', function() {
						wrapperMenu.classList.toggle('open');
					});
					if ($(window).width() <= 767) {
						wrapperMenu.addEventListener('click', function() {
							sideBar.classList.toggle('open');
						});
					}

					// Multiplier Page Script
					/*$('#multiplierForm #submit-btn').attr('disabled',
							'disabled');
					$('#multiplierForm #costType').on(
							'change keyup',
							function() {
								var current_input = $(this).val();

								if (current_input == 'choose_cost_type') {
									$('#multiplierForm #submit-btn').attr(
											'disabled', 'disabled');
								} else {
									$('#multiplierForm #submit-btn')
											.removeAttr('disabled');
								}

							});*/

					// Custom choose file input script # Step 1
					$('#chooseFile').bind(
							'change',
							function() {
								var filename = $("#chooseFile").val();
								if (/^\s*$/.test(filename)) {
									$(".tabsParent .file-upload").removeClass(
											'active');
									$("#noFile").text("Select File");
								} else {
									$(".tabsParent .file-upload").addClass(
											'active');
									$("#noFile").text(
											filename.replace("C:\\fakepath\\",
													""));
								}
							});

					// Add Project Delivered #Step 2
					
					var nameCounter3 = 0;
					function addmoreOption() {
						nameCounter3++;
					
					 selectBox = '<div class="fieldParent">'
							+ '<select class="form-control" name="pvrProjectDelivered['+nameCounter3+']" id="project_deliverd" spellcheck="true"> '
							+ '<option value="1">Select Project</option> '
							+ '<option value="2">2</option> '
							+ '<option value="3">3</option> '
							+ '<option value="4">4</option> ' + '</select> '
							+ '<i class="fas fa-trash-alt crossIcon"></i> '
							+ '</div>';
					}
					$(document).on(
							'click',
							'#pvr_project_delivered #addMoreBtn',
							function(){ 
							addmoreOption();
								$(this).parents().find(
										'#pvr_project_delivered fieldset')
										.append(selectBox);
							});
					$(document).on('click', '.fieldParent >.crossIcon',
							function() {
								$(this).closest('.fieldParent').remove();
							});
					
					// Add Project In Pipeline #Step 2
					var nameCounter4 = 0;
					function addmoreOption1(){
						nameCounter4++;
						
						projectPipeline = '<div class="fieldParent">'
							+ '	<input type="text" class="form-control" '
							+ '	name="pvrProjectPipeline['+nameCounter4+']" id="exampleInputEmail1" '
							+ '	placeholder="Enter Project"> '
							+ '	<i class="fas fa-trash-alt crossIcon"></i> '
							+ '	</div>';
					}
					$(document).on(
							'click',
							'#pvr_project_pipeline #addMoreBtn',
							function(){ 
							addmoreOption1()
								$(this).parents().find(
										'#pvr_project_pipeline fieldset')
										.append(projectPipeline);
							});
					$(document).on('click', '.fieldParent >.crossIcon',
							function() {
								$(this).closest('.fieldParent').remove();
							});
					// Add Property #Step 3
					var nameVal = $('.property_repater_parent');
					var nameCounter = 0;
					
					var addAnotherProperty = '';
					
					function nameCounterFun() {
						nameCounter++;
						addAnotherProperty = '<div class="property_repater_parent mb-4">'
							+ '<div class="row">'
							+ '<div class="subTitle_strip my-0">'
							+ '<h5>property - <span>'+(nameCounter+1) +'</span></h5>'
							+ '<ul>'
							+ '<li><a class="resetBtn" href="javascript:void(0);"><i class="fas fa-redo"></i> Reset</a></li>'
							+ '<li><a class="deleteBtn" href="javascript:void(0);"><i class="fas fa-trash-alt"></i> Delete</a></li>'
							+ '</ul>'
							+ '</div>'
							+ '<div class="col-lg-6 col-sm-6">'
							+ '<fieldset class="form-group bmd-form-group">'
							+ '<label for="exampleInputEmail1" class="bmd-label-static">Name <span style="color:red">*</span></label> '
							+ '<input type="text" class="form-control" name="pvrProperties['+nameCounter+'].propertyName" id="exampleInputEmail1" placeholder="Enter Property Name">'
							+ '</fieldset>'
							+ '</div>'
							+ '<div class="col-lg-6 col-sm-6">'
							+ '<fieldset class="form-group bmd-form-group">'
							+ '<label for="exampleInputEmail1" class="bmd-label-static">Address <span style="color:red">*</span></label> '
							+ '<input type="text" class="form-control" name="pvrProperties['+nameCounter+'].address" id="exampleInputEmail1" placeholder="Enter Address">'
							+ '</fieldset>'
							+ '</div>'
							+ '<div class="col-lg-6 col-sm-6">'
							+ '<fieldset class="form-group bmd-form-group">'
							+ '<label for="exampleInputEmail1" class="bmd-label-static">Number of Screen <span style="color:red">*</span></label> '
							+ '<input type="text" class="form-control" name="pvrProperties['+nameCounter+'].noOfScreen" id="exampleInputEmail1" placeholder="Enter Number of Screen">'
							+ '</fieldset>'
							+ '</div>'
							+ '<div class="col-lg-6 col-sm-6">'
							+ '<fieldset class="form-group bmd-form-group">'
							+ '<label for="exampleInputEmail1" class="bmd-label-static">Seat Capacity <span style="color:red">*</span></label> '
							+ '<input type="text" class="form-control" name="pvrProperties['+nameCounter+'].seatCapacity" id="exampleInputEmail1" placeholder="Enter Number of Screen">'
							+ '</fieldset>'
							+ '</div>'
							+ '<div class="col-lg-6 col-sm-6">'
							+ '<fieldset class="form-group bmd-form-group">'
							+ '<label for="start-date" class="bmd-label-static">Tentative Opening Date <span style="color:red">*</span></label> '
							+ '<input type="text" placeholder="Select Opening Date" class="date-field form-control" data-dtp="dtp_EDQ9R" id="from1" name="pvrProperties['+nameCounter+'].tenatativeOpeningDate">'
							+ '</fieldset>' + '</div> ' + '</div>' + '</div>';
						
					}
					
					
					$(document).on(
							'click',
							'#addProperty',
							function() {
								nameCounterFun();
								$(this).parents().find(
										'#property_repater_parent').append(
										addAnotherProperty);
								
							});
					$(document).on(
							'click',
							'.subTitle_strip > .crossIcon ,  .subTitle_strip >ul li a.deleteBtn',
							function() {
								$(this).closest('.property_repater_parent')
										.remove();
							});
					
					var nameVal2 = $('.competition_properties');
					var nameCounter2 = 0;
				
					var add_competition_properties = '';
					
					function nameCounterFun1() {
							nameCounter2++;
							add_competition_properties = '<div class="competition_properties mb-4">'
							+ '<div class="row">'
							+ '<div class="subTitle_strip my-0">'
							+ '<h5>property - <span>'+ (nameCounter2+1) +'</span></h5>'
							+ '<ul>'
							+ '<li><a class="resetBtn" href="javascript:void(0);"><i class="fas fa-redo"></i> Reset</a></li>'
							+ '<li><a class="deleteBtn" href="javascript:void(0);"><i class="fas fa-trash-alt"></i> Delete</a></li>'
							+ '</ul>'
							+ '</div>'
							+ '<div class="col-lg-6 col-sm-6">'
							+ '<fieldset class="form-group bmd-form-group">'
							+ '<label for="exampleInputEmail1" class="bmd-label-static">Name <span style="color:red">*</span></label> '
							+ '<input type="text" class="form-control" name="competitveProperties['+nameCounter2+'].propertyName" id="exampleInputEmail1" placeholder="Enter Property Name">'
							+ '</fieldset>'
							+ '</div>'
							+ '<div class="col-lg-6 col-sm-6">'
							+ '<fieldset class="form-group bmd-form-group">'
							+ '<label for="exampleInputEmail1" class="bmd-label-static">Address <span style="color:red">*</span></label> '
							+ '<input type="text" class="form-control" name="competitveProperties['+nameCounter2+'].address" id="exampleInputEmail1" placeholder="Enter Address">'
							+ '</fieldset>'
							+ '</div>'
							+ '<div class="col-lg-6 col-sm-6">'
							+ '<fieldset class="form-group bmd-form-group">'
							+ '<label for="exampleInputEmail1" class="bmd-label-static">Number of Screen <span style="color:red">*</span></label> '
							+ '<input type="text" class="form-control" name="competitveProperties['+nameCounter2+'].noOfScreen" id="exampleInputEmail1" placeholder="Enter Number of Screen">'
							+ '</fieldset>'
							+ '</div>'
							+ '<div class="col-lg-6 col-sm-6">'
							+ '<fieldset class="form-group bmd-form-group">'
							+ '<label for="exampleInputEmail1" class="bmd-label-static">Seat Capacity <span style="color:red">*</span></label> '
							+ '<input type="text" class="form-control" name="competitveProperties['+nameCounter2+'].seatCapacity" id="exampleInputEmail1" placeholder="Enter Number of Screen">'
							+ '</fieldset>'
							+ '</div>'
							+ '<div class="col-lg-6 col-sm-6">'
							+ '<fieldset class="form-group bmd-form-group">'
							+ '<label for="start-date" class="bmd-label-static">Tentative Opening Date <span style="color:red">*</span></label> '
							+ '<input type="text" placeholder="Select Opening Date" class="date-field form-control" data-dtp="dtp_EDQ9R" id="to1" name="competitveProperties['+nameCounter2+'].tenatativeOpeningDate">'
							+ '</fieldset>' + '</div> ' + '</div>' + '</div>';
					}
					
					$(document).on(
							'click',
							'#addCompetitionProperties',
							function() {
								nameCounterFun1();
								$(this).parents().find(
										'#competition_properties').append(
										add_competition_properties);
								//counterFunction();
							});
					$(document).on(
							'click',
							'.subTitle_strip > .crossIcon , .subTitle_strip >ul li a.deleteBtn',
							function() {
								$(this).closest('.competition_properties')
										.remove();
							});
					
					// Scroll Table For Fixed Option
					$(document).on('click', '#fixed_table >.viewYearDetails , #camTerms_fixed_table >.viewYearDetails ,#revenue_share_table >.viewYearDetails, #mg_revenue_share_table >.viewYearDetails, #other1_table >.viewYearDetails,#other2_table >.viewYearDetails', function(){
						$(this).parents().find('#fixed_table .collapse_div , #camTerms_fixed_table .collapse_div , #revenue_share_table .collapse_div , #mg_revenue_share_table .collapse_div, #other1_table .collapse_div,#other2_table .collapse_div').slideToggle(), $(this).toggleClass('rotateArrow');
					});

				}); // First Document Ready End Here



// Navigation Active state

$(function() {
setNavigation();
});

function setNavigation() {
var path = window.location.pathname;
path = path.replace(/\/$/, "");
path = decodeURIComponent(path);
$(".sidebar ul li a").each(function() {
var href = $(this).attr('href');
if (path === href) {
$(this).addClass('active');
}
});
}


jQuery(document).ready(function() { // Second Document Ready Start Here
// fixed table
//jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone'); 
	

	
/*###########################################################################################################################################*/
//STEP-1
/*###########################################################################################################################################*/
	$(window).on('load', function(){
		var currentVal = $('#feasibility_state :input').val();
		if(currentVal == 'pre_signing'){
			$('.new_exciting, .new_exciting_blank').css('display', 'block');
		}
	});
$('#feasibility_state :input').on('change keyup', function(){
	var currentVal = $(this).val();
	if(currentVal == 'pre_signing'){
		$('.new_exciting, .new_exciting_blank').css('display', 'block');
	}else if (currentVal == 'post_signing'){
		$('.new_exciting, .new_exciting_blank').css('display', 'none');
	}else if (currentVal == 'pre_handover'){
		$('.new_exciting, .new_exciting_blank').css('display', 'none');
	}
});



$('#new_exciting :input').on('change keyup', function(){
	var currentSelectVal = $(this).val();
	if(currentSelectVal == 'new') {
	$('.projectName_blank , .projectName_field').css('display', 'block');
	$('#selectProjectName , .select_projectName_blank').css('display', 'none');
	
	}else if(currentSelectVal == 'existing') {
	$('#selectProjectName').css('display', 'block');
	$('.projectName_blank, .select_projectName_blank , .projectName_field').css('display', 'none');
	
	//if reportid=null then dont generate and update report id text in UI
	if($("#selectReportId").val()==null)
		return;
	
	}
	generateReportIdForExistingReport();
		
	});

$('#selectReportId').on('change', function(){
	generateReportIdForExistingReport();
});


 var generateReportIdForExistingReport=function(){
	
	var reportId=$('#selectReportId option:selected').val();
	var reportType=$('#reportType').val();
	var projectName=$('#selectReportId option:selected').text();
	
	if(reportType == 'new')
		$("#projectName").val("");
	else
		$("#projectName").val(projectName);
		
	
	 $.ajax({
        url: "//" + pathname + "/project/getLatestProjectWithReportId/"+reportType+"?reportId="+reportId,
        async: false,
        success: function(response) {
       	 	$("#reportIdText").html(response.reportId);
       	 	$("#reportId").val(response.reportId);
       	 
        },
        error: function(xhr, status, error) {
       	 console.log(JSON.stringify(xhr)+" "+status+" "+error);
        }
    });
	
}
	


   
/*###########################################################################################################################################*/
//STEP-4
/*###########################################################################################################################################*/
var inputsFields = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #boxOfficePercentage , #f_bPercentage, #starting_year_mg , #mg_type');
$(inputsFields).find(':input').attr('disabled', 'disabled');
$('#other1_table').hide();
$('#other2_table').hide();
$('#revenue_share_table').hide();
$('#mg_revenue_share_table').hide();
$('#fixed_table').hide();

$('#monthCam').hide();
$('#monthRent').hide();
$('#camZeroMg').hide();
$('#rentZeroMg').hide();


$('#rentPaymentTerm').on('change keyup', function () {

	var noOfColumns = $('#leasePeriod').val();
   
    
    var selectCurrentVal = $(this).val();
    if (selectCurrentVal == 'other') {
        var showedField = $('#starting_year_mg , #mg_type').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        var hiddenField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #boxOfficePercentage , #f_bPercentage, #fixed_table, #FixedYears, #FixedRent').css('display', 'none');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
    }
    else if (selectCurrentVal == 'fixed') {
    	
        var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #fixed_table').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        var hiddenField = $('#starting_year_mg , #mg_type, #boxOfficePercentage , #f_bPercentage , #revenue_share_table , #sale_percentage ,#other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'none');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
        $('#revenue_share_table').hide();
        $('#tblRevenueShare').hide();
        $('#other2_table').hide();
        $('#mg_revenue_share_table').hide();
        $('#other1_table').hide();
        generateTable(noOfColumns);
    }
    else if (selectCurrentVal == 'revenue_share') {
        var showedField = $('#frequencyEscalationYear , #percentageEscalationRent ,#boxOfficePercentage , #f_bPercentage  , #revenue_share_table, #sale_percentage, #other_revenue_percentage').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        var hiddenField = $('#rentINR , #starting_year_mg , #mg_type, #fixed_table, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'none');
       
        $('#fixed_table').hide();
        $('#other1_table').hide();
        $('#other2_table').hide();
        $('#mg_revenue_share_table').hide();
        $('#revenue_share_table').show();
        $('#tblRevenueShare').show();
        
       // $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
        
        generateTable(noOfColumns);
    }
    else if (selectCurrentVal == 'mg_revenue_share') {
        var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #sale_percentage, #other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        var hiddenField = $('#starting_year_mg , #mg_type, #fixed_table, #revenue_share_table').css('display', 'none');
        $('#fixed_table').hide();
        $('#other1_table').hide();
        $('#other2_table').hide();
        $('#revenue_share_table').hide();
        generateTable(noOfColumns);
    }else if(selectCurrentVal == 'other_1'){
    	  var showedField = $('#starting_year_mg , #mg_type, #rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #FixedYears, #FixedRent, #mg_revenue_share_table,#sale_percentage,#other_revenue_percentage').css('display', 'block');
          $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
          var hiddenField = $('#fixed_table, #revenue_share_table,#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #boxOfficePercentage , #sale_percentage, #other_revenue_percentage, #f_bPercentage,#FixedYears, #FixedRent').css('display', 'none');
          $('#fixed_table').hide();
          $('#mg_revenue_share_table').hide();
          $('#revenue_share_table').hide();
          $('#other2_table').hide();
          $('#other1_table').hide();
         
          generateTable(noOfColumns);
    }
    else if(selectCurrentVal == 'other_2'){
  	  var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #sale_percentage, #other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        var hiddenField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage , #sale_percentage, #other_revenue_percentage, #FixedYears, #FixedRent, #mg_revenue_share_table,#starting_year_mg , #mg_type, #fixed_table, #revenue_share_table').css('display', 'none');
        $('#fixed_table').hide();
        $('#mg_revenue_share_table').hide();
        $('#revenue_share_table').hide();
        $('#other1_table').hide();
        $('#other2_table').show();
      
        generateTable(noOfColumns);
  }
    
    else if (selectCurrentVal == 'select_terms') {
        var hiddenField = $('#starting_year_mg , #mg_type, #rentINR , #frequencyEscalationYear , #percentageEscalationRent , #boxOfficePercentage , #f_bPercentage, #fixed_table, #revenue_share_table, #mg_revenue_share_table').css('display', 'none');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
    }
   
});







$('#mg_type_select').on('change keyup', function () {
    var MGTypeCurrentVal = $(this).val();
    var noOfColumns = $('#leasePeriod').val();
    $('#other1_table').show();
    if (MGTypeCurrentVal == 'choose_mg_type') {
        var hiddenField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #boxOfficePercentage , #f_bPercentage').css('display', 'none');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
    }
    else if (MGTypeCurrentVal == 'fixed') {
        var showedField = $('#rentINR , #frequencyEscalationYear , #percentageEscalationRent, #sale_percentage, #other_revenue_percentage, #boxOfficePercentage , #f_bPercentage,#FixedRent,#FixedYears').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
    }
    else if (MGTypeCurrentVal == 'average') {
    	 
        var hiddenField = $('#rentINR,#FixedRent,#FixedYears ').css('display', 'none');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
    }
    
    generateTable(noOfColumns);
});

$('#cam_payment_terms').on('change keyup', function () {
    var CAMTermsCurrentVal = $(this).val();
    var noOfColumns = $('#leasePeriod').val();
    if (CAMTermsCurrentVal == 'choose_cam_terms') {
        var hiddenField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'none');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
    }
    else if (CAMTermsCurrentVal == 'fixed') {
        var showedField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'block');
        $(showedField).find(':input').removeAttr('disabled').attr('enabled', 'enabled');
        generateCamTable(noOfColumns);
    }
    else if (CAMTermsCurrentVal == 'actuals') {
        var hiddenField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'none');
        $('#camTerms_fixed_table').css('display', 'block');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
        generateCamTable(noOfColumns);
    }
    else if (CAMTermsCurrentVal == 'included') {
        var hiddenField = $('#cam_rateINR, #frequency_escalation_years, #percentage_escalation_CAM, #camTerms_fixed_table').css('display', 'none');
        $('#camTerms_fixed_table').css('display', 'block');
        $(hiddenField).find(':input').removeAttr('enabled').attr('disabled', 'disabled');
        generateCamTable(noOfColumns);
    }
});
   
$("#rent, #percentageEscalationInRent, #rentFrequencyOfEscaltionYears , #box_office_percentage , #f_and_b_percentage , #ad_sale_percentage, #others_percentage,#rentFrequencyOfEscaltionYears_fixed,#percentageEscalationInRent_fixed").on('keyup', function() {
	
	 var noOfColumns = $('#leasePeriod').val();
     generateTable(noOfColumns);

});


$("#camRate, #camfrequencyOfEscaltionYears, #camPercentageEscalation").on('keyup', function() {
	
	 var noOfColumns = $('#leasePeriod').val();
	 generateCamTable(noOfColumns);

});



function generateTable(noOfColumns) {
	
	//monthly rent to 
	const noOfMonths=12;
	var leasableArea = $('#leasableArea').val();
	
	var rent= ($('#rent').val()=='') ? 0 : leasableArea * noOfMonths*parseFloat($('#rent').val());
	var rentFrequencyOfEscaltionYears=$('#rentFrequencyOfEscaltionYears').val();
	var percentageEscalationInRent=$('#percentageEscalationInRent').val();
	var rentPaymentTerm=$('#rentPaymentTerm').val();
	
	var boxOfcPerc=$("#box_office_percentage").val()== '' ? 0 : parseFloat($("#box_office_percentage").val());
    var fandBPerc=$("#f_and_b_percentage").val()== '' ? 0 : parseFloat($("#f_and_b_percentage").val());
    var adSalePerc=$("#ad_sale_percentage").val()== '' ? 0 : parseFloat($("#ad_sale_percentage").val());
    var othersPerc=$("#others_percentage").val()== '' ? 0 : parseFloat($("#others_percentage").val());
    
    var startingYearForMG = $('#startingYearForMG').val();
	var rentFrequencyOfEscaltionYears_fixed = $('#rentFrequencyOfEscaltionYears_fixed').val();
	var percentageEscalationInRent_fixed = $('#percentageEscalationInRent_fixed').val();
	
	var myTable;
	if (rentPaymentTerm == 'fixed'){
			myTable = $('#tblFixed');
		
		$('#theadFixedTable').find('tr').remove();
		$('#tbodyFixedTable').find('tr').remove();
		
		$('#theadFixedTable').append('<tr><th class="fixed-side">Lease Period (Yrs)</th></tr>');
		$('#tbodyFixedTable').append('<tr><th class="fixed-side">Rent (INR)</th></tr>');
		
		for(var i=1;i<=noOfColumns;i++){
			
			$('#tblFixed tr').append($("<td>"));
			$('#tblFixed thead tr>td:last').html(i);
			$('#tblFixed tbody tr').each(function(){
				
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" value="'+rent+'" />'))
			});
			
				if(i%rentFrequencyOfEscaltionYears == 0)
					rent+=(rent*percentageEscalationInRent/100);
			
			}
	}
	else if(rentPaymentTerm == 'revenue_share'){
		//alert('table for renvue')
			myTable = $('#tblRevenueShare');
		
		$('#theadRevenueShare').find('tr').remove();
		$('#tbodyRevenueShareTable').find('tr').remove();
		
		$('#theadRevenueShare').append('<tr><th class="fixed-side">Year</th></tr>');
		$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Box Office %</th></tr>');
		$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">F&B %</th></tr>');
		$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Ad %</th></tr>');
		$('#tbodyRevenueShareTable').append('<tr><th class="fixed-side">Others %</th></tr>');
		
		for(var i=1;i<=noOfColumns;i++){
			$('#tblRevenueShare tr').append($("<td>"));
			$('#tblRevenueShare thead tr>td:last').html(i);
			
			var ind=1;
			$('#tblRevenueShare tbody tr').each(function(){
				if(ind==1)
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" value="'+boxOfcPerc+'" />'));
				else if(ind==2)
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" value="'+fandBPerc+'" />'));
				else if(ind==3)
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" value="'+adSalePerc+'" />'));
				else if(ind==4)
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" value="'+othersPerc+'" />'));
				
				ind++;
			});
			
				if(i%rentFrequencyOfEscaltionYears==0){
					boxOfcPerc = parseFloat(boxOfcPerc) + parseFloat(percentageEscalationInRent);
					fandBPerc = parseFloat(fandBPerc) + parseFloat(percentageEscalationInRent);
					adSalePerc = parseFloat(adSalePerc) + parseFloat(percentageEscalationInRent);
					othersPerc = parseFloat(othersPerc) + parseFloat(percentageEscalationInRent);
					
				}
				
			
			}
	
	}
	
	 else if (rentPaymentTerm == 'mg_revenue_share') {
		 
		 myTable = $('#tblMgRevenueShare');
		 	$('#theadMgRevenueShare').find('tr').remove();
			$('#tbodyMgRevenueShareTable').find('tr').remove();
		 
			$('#theadMgRevenueShare').append('<tr><th class="fixed-side">Year</th></tr>');
			$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Box Office %</th></tr>');
			$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">F&B %</th></tr>');
			$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Ad %</th></tr>');
			$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Others %</th></tr>');
			$('#tbodyMgRevenueShareTable').append('<tr><th class="fixed-side">Fixed Rent (INR)</th></tr>');
			
			for(var i=1;i<=noOfColumns;i++){
				$('#tblMgRevenueShare tr').append($("<td>"));
				$('#tblMgRevenueShare thead tr>td:last').html(i);
				
				var ind=1;
				$('#tblMgRevenueShare tbody tr').each(function(){
					if(ind==1)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" value="'+boxOfcPerc+'" />'));
					else if(ind==2)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" value="'+fandBPerc+'" />'));
					else if(ind==3)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" value="'+adSalePerc+'" />'));
					else if(ind==4)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" value="'+othersPerc+'" />'));
					else if(ind==5)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" value="'+rent+'" />'))
					
					
					ind++;
				});
				
				if(i%rentFrequencyOfEscaltionYears==0){
					boxOfcPerc = parseFloat(boxOfcPerc) + parseFloat(percentageEscalationInRent);
					fandBPerc = parseFloat(fandBPerc) + parseFloat(percentageEscalationInRent);
					adSalePerc = parseFloat(adSalePerc) + parseFloat(percentageEscalationInRent);
					othersPerc = parseFloat(othersPerc) + parseFloat(percentageEscalationInRent);
					
				}
					
					if(i%rentFrequencyOfEscaltionYears_fixed==0)
						rent+=(rent*percentageEscalationInRent_fixed/100);
					
				
				}
			
		 
	 }
	
	 else if (rentPaymentTerm == 'other_1') {
		 
		 myTable = $('#tblOther_1');
		 	$('#theadOther_1').find('tr').remove();
			$('#tbodyOther_1').find('tr').remove();
		 
			$('#theadOther_1').append('<tr><th class="fixed-side">Year</th></tr>');
			$('#tbodyOther_1').append('<tr><th class="fixed-side">Box Office %</th></tr>');
			$('#tbodyOther_1').append('<tr><th class="fixed-side">F&B %</th></tr>');
			$('#tbodyOther_1').append('<tr><th class="fixed-side">Ad %</th></tr>');
			$('#tbodyOther_1').append('<tr><th class="fixed-side">Others %</th></tr>');
			$('#tbodyOther_1').append('<tr><th class="fixed-side">Fixed Rent (INR)</th></tr>');
			
			var mg_type_select = $('#mg_type_select').val();
			for(var i=1;i<=noOfColumns;i++){
				$('#tblOther_1 tr').append($("<td>"));
				$('#tblOther_1 thead tr>td:last').html(i);
				
				if('average' == mg_type_select){
					rent = 0;
				}
				
				var ind=1;
				$('#tblOther_1 tbody tr').each(function(){
					if(ind==1)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" value="'+boxOfcPerc+'" />'));
					else if(ind==2)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" value="'+fandBPerc+'" />'));
					else if(ind==3)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" value="'+adSalePerc+'" />'));
					else if(ind==4)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" value="'+othersPerc+'" />'));
					else if(ind==5)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" value="'+rent+'" />'))
					
					
					ind++;
				});
				
				if(i >= (startingYearForMG - 1) ){
					boxOfcPerc = 0;
					fandBPerc = 0;
					adSalePerc = 0;
					othersPerc = 0;
				}else{
					if(i%rentFrequencyOfEscaltionYears == 0){
						boxOfcPerc = parseFloat(boxOfcPerc) + parseFloat(percentageEscalationInRent);
						fandBPerc = parseFloat(fandBPerc) + parseFloat(percentageEscalationInRent);
						adSalePerc = parseFloat(adSalePerc) + parseFloat(percentageEscalationInRent);
						othersPerc = parseFloat(othersPerc) + parseFloat(percentageEscalationInRent);
						
					}
				}
				
	 			
				
				if('fixed' == mg_type_select){
					if(i%rentFrequencyOfEscaltionYears_fixed == 0)
						rent+=(rent*percentageEscalationInRent/100);
					}
				
				
			}
				
				
				
			
		 
	 }
	
	 else if (rentPaymentTerm == 'other_2') {
		 
		 myTable = $('#tblOther_2');
		 	$('#theadOther_2').find('tr').remove();
			$('#tbodyOther_2').find('tr').remove();
		 
			$('#theadOther_2').append('<tr><th class="fixed-side">Year</th></tr>');
			$('#tbodyOther_2').append('<tr><th class="fixed-side">Box Office %</th></tr>');
			$('#tbodyOther_2').append('<tr><th class="fixed-side">F&B %</th></tr>');
			$('#tbodyOther_2').append('<tr><th class="fixed-side">Ad %</th></tr>');
			$('#tbodyOther_2').append('<tr><th class="fixed-side">Others %</th></tr>');
			$('#tbodyOther_2').append('<tr><th class="fixed-side">Fixed Rent (INR)</th></tr>');
			
			rent =0;
			boxOfcPerc=0;
			fandBPerc=0;
			adSalePerc=0;
			othersPerc=0;
			
			for(var i=1;i<=noOfColumns;i++){
				$('#tblOther_2 tr').append($("<td>"));
				$('#tblOther_2 thead tr>td:last').html(i);
				
				var ind=1;
				$('#tblOther_2 tbody tr').each(function(){
					if(ind==1)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseBoxOfficePercentage['+i+']" value="'+boxOfcPerc+'" />'));
					else if(ind==2)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseFAndBPercentage['+i+']" value="'+fandBPerc+'" />'));
					else if(ind==3)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseAdSalePercentage['+i+']" value="'+adSalePerc+'" />'));
					else if(ind==4)
					$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseOthersPercentage['+i+']" value="'+othersPerc+'" />'));
					else if(ind==5)
						$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" value="'+rent+'" />'))
					
					
					ind++;
				});
				
				
				}
			
		 
	 }
	

} 


$(document).on('click','#accordion-toggle', function () {
   $(this).parents().find('.innerTableParent .collapseTable').slideToggle();
});




function generateCamTable(noOfColumns) {
	
	//monthly rent to 
	const noOfMonths=12;
	var leasableArea = $('#leasableArea').val();
	var cam_payment_terms = $('#cam_payment_terms').val();
	var cam_rent= ($('#camRate').val()=='') ? 0 : leasableArea * noOfMonths*parseFloat($('#camRate').val());
	var camfrequencyOfEscaltionYears = parseFloat($('#camfrequencyOfEscaltionYears').val());
	var camPercentageEscalation = parseFloat($('#camPercentageEscalation').val());
	
		
	var myTable;
	if (cam_payment_terms == 'fixed'){
			myTable = $('#tblCamFixed');
		
		$('#theadCamFixedTable').find('tr').remove();
		$('#tbodyCamFixedTable').find('tr').remove();
		
		$('#theadCamFixedTable').append('<tr><th class="fixed-side">Year</th></tr>');
		$('#tbodyCamFixedTable').append('<tr><th class="fixed-side">CAM (INR)</th></tr>');
		
		for(var i=1;i<=noOfColumns;i++){
			
			$('#tblCamFixed tr').append($("<td>"));
			$('#tblCamFixed thead tr>td:last').html(i);
			$('#tblCamFixed tbody tr').each(function(){
				
				$(this).children('td:last').append($('<input type="text" name="camTerm.yearWiseCamRent['+i+']" value="'+cam_rent+'" />'))
			});
			
				if(i%camfrequencyOfEscaltionYears == 0)
					cam_rent+=(cam_rent*camPercentageEscalation/100);
			}
	}
	
	if (cam_payment_terms == 'actuals'){
		myTable = $('#tblCamFixed');
	
	$('#theadCamFixedTable').find('tr').remove();
	$('#tbodyCamFixedTable').find('tr').remove();
	
	$('#theadCamFixedTable').append('<tr><th class="fixed-side">Year</th></tr>');
	$('#tbodyCamFixedTable').append('<tr><th class="fixed-side">CAM (INR)</th></tr>');
	
	cam_rent = 0;
	
	for(var i=1;i<=noOfColumns;i++){
		
		$('#tblCamFixed tr').append($("<td>"));
		$('#tblCamFixed thead tr>td:last').html(i);
		$('#tblCamFixed tbody tr').each(function(){
			
			$(this).children('td:last').append($('<input type="text" name="camTerm.yearWiseCamRent['+i+']" value="'+cam_rent+'" />'))
		});
		
		
		}
}
	
	if (cam_payment_terms == 'included'){
		myTable = $('#tblCamFixed');
	
	$('#theadCamFixedTable').find('tr').remove();
	$('#tbodyCamFixedTable').find('tr').remove();
	
	$('#theadCamFixedTable').append('<tr><th class="fixed-side">Year</th></tr>');
	$('#tbodyCamFixedTable').append('<tr><th class="fixed-side">CAM (INR)</th></tr>');
	
	cam_rent = 0;
	
	for(var i=1;i<=noOfColumns;i++){
		
		$('#tblCamFixed tr').append($("<td>"));
		$('#tblCamFixed thead tr>td:last').html(i);
		$('#tblCamFixed tbody tr').each(function(){
			
			$(this).children('td:last').append($('<input type="text" name="camTerm.yearWiseCamRent['+i+']" value="'+cam_rent+'" />'))
		});
		
		
		}
}
	
	

} 



$('#camType').on('change keyup', function () {
	
    var camVal = $(this).val();
    
    if (camVal == 'month') {
        $('#monthCam').show();
        $('#camZeroMg').hide();
       
    }
    if (camVal == 'fixed') {
        $('#camZeroMg').show();
        $('#monthCam').hide();
    }
    if (camVal == '') {
        $('#monthCam').hide();
        $('#camZeroMg').hide();
       
    }
    

});


$('#rentType').on('change keyup', function () {
	
    var camVal = $(this).val();
    
    if (camVal == 'month') {
        $('#monthRent').show();
        $('#rentZeroMg').hide();
    }
    if (camVal == 'fixed') {
        $('#rentZeroMg').show();
        $('#monthRent').hide();
    }
    if (camVal == '') {
        $('#monthRent').hide();
        $('#rentZeroMg').hide();
       
    }
    

});

// Project Layout Assumptions
var typeOfList = '<div class="adminCategory_list">'+
		'<div class="row">'+
	'<div class="col-lg-4 col-sm-4"><label>Screen Format Types</label></div>'+
	
	'<div class="col-lg-4 col-sm-4">'+
		'<div class="multiSelectBox">'+
		'<select class="selectpicker" multiple data-live-search="true">'+
	  '<option>Mustard</option>'+
	 ' <option>Ketchup</option>'+
	 ' <option>Relish</option>'+
	'</select>'+
	'</div>'+
	'</div>'+
	
	 /*'<div class="col-lg-4 col-sm-4">'+
    '<div class="multiSelectBox">'+
    '<span class="bmd-form-group is-filled"><div class="dropdown bootstrap-select show-tick dropup"><select class="selectpicker" multiple="" data-live-search="true" tabindex="-98">'+
    '<option>Mustard</option>'+
    '<option>Ketchup</option>'+
    '<option>Relish</option>'+
  '</select><button type="button" class="btn dropdown-toggle btn-light bs-placeholder" data-toggle="dropdown" role="button" title="Nothing selected" aria-expanded="false"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner">Nothing selected</div></div> </div></button><div class="dropdown-menu" role="combobox" style="max-height: 260px; overflow: hidden; min-height: 43px; position: absolute; will-change: top, left; min-width: 443px; top: -133px; left: 0px;" x-placement="top-start"><div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off" role="textbox" aria-label="Search"></div><div class="inner show" role="listbox" aria-expanded="false" tabindex="-1" style="max-height: 201px; overflow-y: auto; min-height: 0px;"><ul class="dropdown-menu inner show"><li class=""><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Mustard</span></a></li><li class=""><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Ketchup</span></a></li><li class=""><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Relish</span></a></li></ul></div></div></div></span>'+
  '</div>'+
'</div>'+*/
	
	'<div class="col-lg-4 col-sm-4">'+
		'<div class="delEditBtn_btn">'+
			'<a href="javascript:void(0);" class="editBtn_btn">Edit</a>'+
			'<a href="javascript:void(0);" class="deleteBtn_btn">Delete</a>'+
		'</div>'+
	'</div>'+
'</div>'+
'</div>';


$('.adminValue_areaParent  >#addMoreBtn').on('click', function(){
	var ScreenName = prompt("Enter Name of Screen Type");
	var getValue;
	if(ScreenName != null) {
		getValue = ScreenName
	}
	var cloneList = $('.adminCategoryParent').append(typeOfList);
	$('.adminCategoryParent').children().last().find('label').text(getValue);
});
//Delete Row
$(document).on('click', '.deleteBtn_btn', function(){
	$(this).closest('.adminCategory_list').remove();
});


/*$(".adminValue_areaParent  >#addMoreBtn").click(function(){
	
	  $.getScript('/js/bootstrap-select.min.js', function() {
	     $("body").html('Javascript is loaded successful!  is loaded!');
	  });
	});*/



}); // Second Document Ready Close Here







////////////////////////////////
// this script for external bootstrap select js recall on click
//////////////////////////////////
/*function loadScript(location, runOnLoad) {
    // Check for existing script element and delete it if it exists
    var js = document.getElementById("bootstrapSelectJS");
  
    if(js) {
        document.head.removeChild(js);
        sandbox = undefined;
        console.info("---------- Script refreshed ----------");
    }
    
    // Create new script element and load a script into it
    js = document.createElement("script");
    document.head.appendChild(js);
    if(runOnLoad)
        js.onload = function() { sandbox(); };
    js.src = location;
    js.id = "bootstrapSelectJS";
}

// Make sure script has loaded and display an alert if it has not
function runScript() {
    if(typeof sandbox !== "undefined") {
        sandbox();
    } else {
       // window.alert("The script has not been loaded yet!");
    }
}
*/
