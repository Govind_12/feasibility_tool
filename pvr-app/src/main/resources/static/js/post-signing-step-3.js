var input = document.getElementsByClassName('exampleInputAddress');

function initMap() {
	
	var options = {
			 //types: ['(cities)'],
		componentRestrictions : {
			country : [ 'IN' ]
		}
	};
	var autocomplete =[];
	//console.log(" input.length "+input.length);
	for(var k=0; k < input.length; k++){
		var autocomplete = new google.maps.places.Autocomplete(input[k], options);
		
		google.maps.event.addListener(autocomplete,'place_changed',function() {
			var place = autocomplete.getPlace();
			console.log(place.address_components);
			var lat = place.geometry.location.lat();
			var lng = place.geometry.location.lng();
			//$('#latitude').val(lat);
			//$('#longitude').val(lng);
			console.log(" k "+k);	
			for (var i = 0; i < place.address_components.length; i += 1) {
				var addressObj = place.address_components[i];
				for (var j = 0; j < addressObj.types.length; j += 1) {
					if (addressObj.types[j] === 'country') {
						//console.log(addressObj.long_name);
					} else if (addressObj.types[j] === 'administrative_area1') {
						//console.log('state '+ addressObj.long_name);
					} else if (addressObj.types[j] === 'locality') {
						var index =k-1;
						$('#city'+index).val(addressObj.long_name);
					} else if (addressObj.types[j] === 'administrative_area_level_1') {
						var index =k-1;
						$('#state'+index).val(addressObj.long_name);
						//$('#stateCity').val(addressObj.long_name);
					}
				}
			}
		});
	}	
}




$(document).on('focus',".uc_datepicker", function(){
    $(this).bootstrapMaterialDatePicker({
		weekStart : 0,
		time : false,
		format : 'DD/MM/YYYY',
		minDate: new Date()
	});
});

$(document).ready(function() {
	
	$(".numberLimit").inputFilter(function(value) {
		return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 10000);
	});
	
	
	//-----------------------------------pvr property start----------------------------------------
	
	// Add Property #Step 3
	var nameVal = $('.property_repater_parent');
	var nameCounter = 1;
	var propertyCounter = 1;
	var pvr_properties_size = parseInt($('#pvr_properties_size').val());
	//console.log(pvr_properties_size)
	if(pvr_properties_size>0){
		nameCounter = pvr_properties_size-1;
	}
	var addAnotherProperty = '';
	
	function nameCounterFun() {
		nameCounter++;
		addAnotherProperty = '<div class="property_repater_parent mb-4">'
			+ '<div class="row">'
			+ '<div class="subTitle_strip my-0">'
			+ '<h5>property - <span id="pvrProperties'+(nameCounter+1)+'">'+(nameCounter+1) +'</span></h5>'
			/*+ '<h5>property - <span id="pvrProperties'+(nameCounter+1)+'">'+(propertyCounter+1) +'</span></h5>'*/
			+ '<ul>'
			+ '<li><a class="resetBtn" href="javascript:void(0);"><i class="fas fa-redo"></i> Reset</a></li>'
			+ '<li><a class="deleteBtn deletePVRBtn" pvrDeleteCount="'+ (nameCounter+1)+'" href="javascript:void(0);"><i class="fas fa-trash-alt"></i> Delete</a></li>'
			+ '</ul>'
			+ '</div>'
			+ '<div class="col-lg-6 col-sm-6">'
			+ '<fieldset class="form-group bmd-form-group">'
			+ '<label for="exampleInputEmail1" class="bmd-label-static">Name <span style="color:red">*</span></label> '
			+ '<input type="text" class="form-control" name="pvrProperties['+nameCounter+'].propertyName" id="propertName'+nameCounter+'" placeholder="Enter property name">'
			+ '</fieldset>'
			+ '</div>'
			+ '<div class="col-lg-6 col-sm-6">'
			+ '<fieldset class="form-group bmd-form-group">'
			+ '<label for="exampleInputEmail1" class="bmd-label-static">Address <span style="color:red">*</span></label> '
			+ '<input type="text" class="form-control exampleInputAddress" name="pvrProperties['+nameCounter+'].address" id="propertAddress'+nameCounter+'" placeholder="Enter address">'
			+ '</fieldset>'
			+ '</div>'
			+ '<div class="col-lg-6 col-sm-6">'
			+ '<fieldset class="form-group bmd-form-group">'
			+ '<label for="exampleInputEmail1" class="bmd-label-static">Number of screens <span style="color:red">*</span></label> '
			+ '<input type="text" class="form-control  numberLimit" name="pvrProperties['+nameCounter+'].noOfScreen" id="propertNoOfScreen'+nameCounter+'" placeholder="Enter number of screens">'
			+ '</fieldset>'
			+ '</div>'
			+ '<div class="col-lg-6 col-sm-6">'
			+ '<fieldset class="form-group bmd-form-group">'
			+ '<label for="exampleInputEmail1" class="bmd-label-static">Seat capacity <span style="color:red">*</span></label> '
			+ '<input type="text" class="form-control  numberLimit" name="pvrProperties['+nameCounter+'].seatCapacity" id="propertSeatCapacity'+nameCounter+'" placeholder="Enter seat capacity">'
			+ '</fieldset>'
			+ '</div>'
			+ '<div class="col-lg-6 col-sm-6">'
			+ '<fieldset class="form-group bmd-form-group">'
			+ '<label for="start-date" class="bmd-label-static">Tentative opening date <span style="color:red">*</span></label> '
			+ '<input type="text" placeholder="Select opening date" class="date-field form-control uc_datepicker " data-dtp="dtp_EDQ9R" id="tentativeOpeningDate'+nameCounter+'" name="pvrProperties['+nameCounter+'].tenatativeOpeningDate">'
			+ '</fieldset>' + '</div> ' + '</div>' + '</div>';
		
	}
	
	
	/*function resetCounter(noOfCount){
		
		nameCounter = nameCounter;
		console.log("before delete -"+propertyCounter);
		
		var count = 2;
		for(var i = 1; i<=propertyCounter;i++){
			
			if(i === noOfCount){
				propertyCounter ++;
			}else{
				console.log("pvrProperties"+count);
				$('#pvrProperties'+count).text(count);
				count++;
			}
			
		}
		
		nameCounter = nameCounter -1;
		propertyCounter = propertyCounter - 1;
		console.log("after delete -"+propertyCounter);
		
		
	}*/
	


	$(document).on('click','#addProperty',function() {
		//addCounter();
		nameCounterFun();
		
		$(this).parents().find('#repeaterGrandParent').append(addAnotherProperty);
		initMap();
		
		$(".numberLimit").inputFilter(function(value) {
			return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 10000);
		});
		
	});
	
	$(document).on('click','.subTitle_strip > .crossIcon ,  .subTitle_strip >ul li a.deletePVRBtn',function() {
		var  noOfCount = $(this).closest('.subTitle_strip').find('h5 span').text();
		
		$(this).closest('.property_repater_parent').remove();
		var deletedPropertyCount=$(this).attr('pvrDeleteCount');
		resuffelPvrPointer(deletedPropertyCount);
		
		nameCounter--;
		
	});
	
	$(document).on('click','#repeaterGrandParent .resetBtn', function(){
		$(this).closest('.property_repater_parent').find(':input').prop('value', '');
	});
	
	
	var resuffelPvrPointer=function(deletedPropertyCount){
		
		deletedPropertyCount=parseInt(deletedPropertyCount);
		
		if((nameCounter>1) && (nameCounter>=deletedPropertyCount)){
			for(var i=deletedPropertyCount;i<=nameCounter;i++){
				
				var idToUpdate=i+1;
				
				//change span value to show exact property no
				$('#pvrProperties'+idToUpdate).text(i);
				//change id of span
				$('#pvrProperties'+idToUpdate).attr('id', 'pvrProperties'+i);
				//change delete count
				$("[pvrDeleteCount="+idToUpdate+"]").attr('pvrDeleteCount', i);
				
			}
		}
	}
	
	
	//-----------------------------------pvr property end----------------------------------------
	
	
	
	
	
	
	
	
	//--------------------------Competetion property start-----------------------------------------
	
	
	var nameVal2 = $('.competition_properties');
	var nameCounter2 = 1;
	var pvr_competition_size = parseInt($('#pvr_competition_size').val());
	/*console.log(pvr_competition_size)*/
	if(pvr_competition_size>0){
		nameCounter2 = pvr_competition_size-1;
	}
	var add_competition_properties = '';
	
	function nameCounterFun1() {
			nameCounter2++;
			add_competition_properties = '<div class="competition_properties mb-4">'
			+ '<div class="row">'
			+ '<div class="subTitle_strip my-0">'
			+ '<h5>property - <span id="pvrCompProperty'+ (nameCounter2+1) +'" >'+ (nameCounter2+1) +'</span></h5>'
			+ '<ul>'
			+ '<li><a class="resetBtn" href="javascript:void(0);"><i class="fas fa-redo"></i> Reset</a></li>'
			+ '<li><a class="deleteBtn deleteCOMPBtn" deleteCount="'+ (nameCounter2+1) +'" href="javascript:void(0);"><i class="fas fa-trash-alt"></i> Delete</a></li>'
			+ '</ul>'
			+ '</div>'
			+ '<div class="col-lg-6 col-sm-6">'
			+ '<fieldset class="form-group bmd-form-group">'
			+ '<label for="exampleInputEmail1" class="bmd-label-static">Name <span style="color:red">*</span></label> '
			+ '<input type="text" class="form-control" name="competitveProperties['+nameCounter2+'].propertyName" id="exampleInputEmail1" placeholder="Enter property name">'
			+ '</fieldset>'
			+ '</div>'
			+ '<div class="col-lg-6 col-sm-6">'
			+ '<fieldset class="form-group bmd-form-group">'
			+ '<label for="exampleInputEmail1" class="bmd-label-static">Address <span style="color:red">*</span></label> '
			+ '<input type="text" class="form-control exampleInputAddress" name="competitveProperties['+nameCounter2+'].address" id="exampleInputEmail1" placeholder="Enter address">'
			/*+'<input type="hidden" class="form-control city" id="city'+nameCounter2+'"'
			+'name="{competitveProperties['+nameCounter2+'].projectLocation.city}">'
			+' <input type="hidden" class="form-control  state" id="state'+nameCounter2+'"'
			+'name="{competitveProperties['+nameCounter2+'].projectLocation.state}">'*/
			+ '</fieldset>'
			+ '</div>'
			+ '<div class="col-lg-6 col-sm-6">'
			+ '<fieldset class="form-group bmd-form-group">'
			+ '<label for="exampleInputEmail1" class="bmd-label-static">Number of screens <span style="color:red">*</span></label> '
			+ '<input type="text" class="form-control numberLimit" name="competitveProperties['+nameCounter2+'].noOfScreen" id="exampleInputEmail1" placeholder="Enter number of screens">'
			+ '</fieldset>'
			+ '</div>'
			+ '<div class="col-lg-6 col-sm-6">'
			+ '<fieldset class="form-group bmd-form-group">'
			+ '<label for="exampleInputEmail1" class="bmd-label-static">Seat capacity (Optional) </label> '
			+ '<input type="text" class="form-control numberLimit competetionSeatCap" name="competitveProperties['+nameCounter2+'].seatCapacity" id="exampleInputEmail1" placeholder="Enter seat capacity">'
			+ '</fieldset>'
			+ '</div>'
			+ '<div class="col-lg-6 col-sm-6">'
			+ '<fieldset class="form-group bmd-form-group">'
			+ '<label for="start-date" class="bmd-label-static">Tentative opening date <span style="color:red">*</span></label> '
			+ '<input type="text" placeholder="Select opening date" class="date-field form-control uc_datepicker" data-dtp="dtp_EDQ9R" id="to1" name="competitveProperties['+nameCounter2+'].tenatativeOpeningDate">'
			+ '</fieldset>' + '</div> ' + '</div>' + '</div>';
	}
	
	
	$(document).on('click','#addCompetitionProperties',function() {
		
		nameCounterFun1();
		
		$(this).parents().find('#repeaterGrandParent1').append(add_competition_properties);
		initMap();
		$(".numberLimit").inputFilter(function(value) {
			return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 10000);
		});
		
	});
	
	$(document).on('click','.subTitle_strip > .crossIcon , .subTitle_strip >ul li a.deleteCOMPBtn',function() {
		$(this).closest('.competition_properties').remove();
		var deletedPropertyCount=$(this).attr('deleteCount');
		resuffelCompetetionPointer(deletedPropertyCount);
		
		nameCounter2--;
		
	});
	
	$(document).on('click','#repeaterGrandParent1 .resetBtn', function(){
		$(this).closest('.competition_properties').find(':input').prop('value', '');
	});
	
	
	var resuffelCompetetionPointer=function(deletedPropertyCount){
		deletedPropertyCount=parseInt(deletedPropertyCount);
		//alert("nameCounter2: "+nameCounter2+" deletedPropertyCount:"+deletedPropertyCount);
		
		if((nameCounter2>1) && (nameCounter2>=deletedPropertyCount)){
			
			for(var i=deletedPropertyCount;i<=nameCounter2;i++){
				
				var idToUpdate=i+1;
				
				//change span value to show exact property no
				$('#pvrCompProperty'+idToUpdate).text(i);
				//change id of span
				$('#pvrCompProperty'+idToUpdate).attr('id', 'pvrCompProperty'+i);
				//change delete count
				$("[deletecount="+idToUpdate+"]").attr('deletecount', i);
				
			}
		}
	}
	
	//--------------------------Competetion property end-----------------------------------------
	
	
	//------------------------Start -Validation of either complete form filled or not--------------------
	$(document).on('keyup', '.property_repater_parent input', function () {
		validateFilledOrNotPVRProperties(this);
	});
	
	$(document).on('change', '.property_repater_parent .date-field', function(){
		validateFilledOrNotPVRProperties(this);
	});
	
	
	var validateFilledOrNotPVRProperties= function(thisObject){
		var filledFlag=false;
		//check if any filled is not blank
		$(thisObject).closest('.property_repater_parent').find(':input').each(function(){
			
			console.log($(this).val());
			if($(this).val()!=''){
				filledFlag=true;
			}
		});
		
		//if any of filled is not blank then make complete div as required else not
		if(filledFlag){
			$(thisObject).closest('.property_repater_parent').find(':input').each(function(){
				$(this).addClass('validate_required');
			});
		}
		else
		{
			$(thisObject).closest('.property_repater_parent').find(':input').each(function(){
				$(this).removeClass('validate_required');
			});
		}
	}
	
	
	$(document).on('keyup', '.competition_properties input', function () {
		validateFilledOrNotCompetetionProperties(this);
	});
	
	$(document).on('change', '.competition_properties .date-field', function(){
		validateFilledOrNotCompetetionProperties(this);
	});
	
	
	var validateFilledOrNotCompetetionProperties= function(thisObject){
		var filledFlag=false;
		//check if any filled is not blank
		$(thisObject).closest('.competition_properties').find(':input').each(function(){
			
			console.log($(this).val());
			if($(this).val()!=''){
				filledFlag=true;
			}
		});
		console.log(filledFlag);
		//if any of filled is not blank then make complete div as required else not
		if(filledFlag){
			$(thisObject).closest('.competition_properties').find(':input').each(function(){
				
				if(!$(this).hasClass("competetionSeatCap"))
					$(this).addClass('validate_required1');
					
			});
		}
		else
		{
			$(thisObject).closest('.competition_properties').find(':input').each(function(){
				$(this).removeClass('validate_required1');
			});
		}
	}
	
	
	//------------------------END  -Validation of either complete form filled or not --------------------
	
	
	
});


(function($) {
	  $.fn.inputFilter = function(inputFilter) {
	    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
	      if (inputFilter(this.value)) {
	        this.oldValue = this.value;
	        this.oldSelectionStart = this.selectionStart;
	        this.oldSelectionEnd = this.selectionEnd;
	      } else if (this.hasOwnProperty("oldValue")) {
	        this.value = this.oldValue;
	        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
	      }
	    });
	  };
	}(jQuery));

function validate() {
	
	$('.form-group').find('label span').remove();
	var flag = true;
	
	$('.validate_required').each(function(){
		
		var currentVal = $(this).val();
		console.log('.validate_required')
		if(currentVal == ''){
			$(this).closest('.form-group').find('label').append('<span style="color:red"> Required </span>');
			flag = false;
		} else if (currentVal == true){
			$('.validate_required').closest('.form-group').find('label span').remove();
			
		}
		
	});
	
	var flag1 = true;
	$('.validate_required1').each(function(){
		console.log('.validate_required1')
		var currentVal = $(this).val();
		
		if(currentVal == ''){
			$(this).closest('.form-group').find('label').append('<span style="color:red"> Required </span>');
			flag1 = false;
		} else if (currentVal == true){
			$('.validate_required1').closest('.form-group').find('label span').remove();
			
		}
		
	});
	
	if(flag && flag1)
		return true;
	else
		return false;

}


$(document).on('click','.pvrPropertiesresetBtn',function() {
	$('#step_3_name').val('');
	$('#exampleInputAddress').val('');
	$('#exampleInputscreen').val('');
	$('#exampleInputseat').val('');
	$('#from1').val('');
	
});

$(document).on('click','.compitionResetBtn',function() {
	$('#step_3_name1').val('');
	$('#exampleInputAddress1').val('');
	$('#exampleInputscreen1').val('');
	$('#exampleInputEmail1').val('');
	$('#to1').val('');
	
});  
    
$('#save').click(function() {
	if(validate()){
		form = $('#upcomingCinemaForm');
		form.attr('action', '/post-signing/upcoming-cinemas/').trigger('submit');	
	}
	
});

$('#save_close').click(function() {
	form = $('#upcomingCinemaForm');
	form.attr('action', '/post-signing/upcoming-cinemas-save/').trigger('submit');
});   

    
    
    
    
    
    
    
    
    
