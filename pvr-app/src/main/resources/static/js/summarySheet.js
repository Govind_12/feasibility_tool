$(document).ready(function(){    	  
			var pathname = window.location.host;
            populateDataOnLoad();
            
            $('#dateOfHandover').bootstrapMaterialDatePicker({
                weekStart: 0,
                time: false,
                format: 'DD/MM/YYYY'
            });
            $('#dateOfOpening').bootstrapMaterialDatePicker({
                weekStart: 0,
                time: false,
                format: 'DD/MM/YYYY'
            });
           var datepicker = $.fn.datepicker.noConflict();
           $.fn.bootstrapDP = datepicker;
            
         
           
           $(document).on('change', '[type="radio"]', function() {
        	    var currentValue = $(this).val(); 
        	    if("pre_signing" == currentValue){
        	    	  window.location.href = "//" + pathname + "/project/step-1";
        	    }
        	    
        	    if("post_signing" == currentValue){
      	    	  window.location.href = "//" + pathname + "/post-signing";
        	    }
        	    
        	    if("pre_handover" == currentValue){
        	    	  window.location.href = "//" + pathname + "/post-signing";
          	    }
        	  
        	});
           
           
         
            
   /*#####################################################################################################################*/
   						/*#STEP-1*/
   /*###################################################################################################################*/  
           

            
            $('#save_plan').on('click', function() {
                $(this).parents().find('.tabsParent .nav-tabs .step-2').click();
              //  var tab_A = $('#plan').serialize();
                var form_data = new FormData($('#plan')[0]);
                $('.loading').show();
                $.ajax({
                    url: "//" + pathname + "/project/step-1",
                    type: 'POST',
                    data: form_data,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        const surveyId = response.id;
                        step2(surveyId)
                        const reportId = response.reportId;
                        getReportId(reportId)
                       
                        $("#reportIdStep2").html(reportId);
                        $('.loading').hide();
                    },
                    error: function(xhr, status, error) {}
                });
            });
          /*#####################################################################################################################*/
          							/*save and close step-1*/
          /*#####################################################################################################################*/
  
          $('#save_close1').on('click', function() {
                var tab_A = $('#plan').serialize();
                //var form_data = new FormData($('#plan')[0]);
                window.location.href = "//" + pathname +"/dashboard";
                $('.loading').show();
                $.ajax({
                    url: "//" + pathname + "/project/saveAndClose/step-1",
                    type: 'POST',
                    data: tab_A,
                    success: function(response) {
                    	$('.loading').hide();
                    },
                    error: function(xhr, status, error) {}
                });
            });

            $("#citySelect").change(function() {
                var selectedCity = $(this).val();
                var cityState;
                $('.loading').show();
                $.ajax({
                    url: "//" + pathname + "/city/get-state/" + selectedCity,
                    method: 'GET',
                    success: function(data) {
                        for (var i = 0; i < data.length; i++) {
                            cityState = data[i].city_state;
                            $('#stateCity').val(cityState);
                        }
                        $('.loading').hide();
                    },
                    error: function(xhr, status, error) {
                        console.log('Error ' + JSON.stringify(status) + "Error : " + error + " status: " + status + " xhr: " + JSON.stringify(xhr))
                    }
                });
            });

    /*#####################################################################################################################*/
				/*#STEP-2*/
/*###################################################################################################################*/

            function step2(surveyId) {
                $('#step_2_Id').val(surveyId);
            
            $('#step_2_save').on('click', function() {
            	//alert("step_2_save")
                $(this).parents().find('.tabsParent .nav-tabs .step-3').click();
                var form_data = new FormData($('#step_2')[0]);
              // var survey = $('#step_2').serialize();
              // survey.id = surveyId;
                $('.loading').show();
                $.ajax({
                    url: "//" + pathname + "/project/step-2",
                    type: 'POST',
                    data: form_data,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                    	 $('.loading').hide();
                        step3(response.id)
                        //move to step3
                        //console.log(response);
                        $("#reportIdStep3").html(response.reportId);
                       
                    },
                    error: function(xhr, status, error) {}
                });
            });
            }

 /*#####################################################################################################################*/
			/* #STEP-2 save and close */
/*###################################################################################################################*/

            $('#save_close2').on('click', function() {
                var survey = $('#step_2').serialize();
                
                $('#assignEmp input:checkbox').each(function() {
                    (this.checked ? userId.push($(this).val()) : "")
                });
                $('#surveyId').val(surveyId)
                survey.id = surveyId;
                window.location.href = "//" + pathname +"/dashboard";
                $('.loading').show();
                $.ajax({
                    url: "//" + pathname + "/project/saveAndClose/step-2",
                    type: 'POST',
                    data: survey,
                    success: function(response) {
                    	$('.loading').hide();
                    },
                    error: function(xhr, status, error) {}
                });
            });

   /*#####################################################################################################################*/
			/* #STEP-3*/
/*###################################################################################################################*/
       	
        	function step3(surveyId){
        		$('#step_3_Id').val(surveyId);
        		$('#step_3_save').on('click',function(){
        			$(this).parents().find('.tabsParent .nav-tabs .step-4').click();
        			var survey=$('#step_3').serialize();
        			$('#surveyId').val(surveyId)
        			survey.id=surveyId;
        			$('.loading').show();
        			$.ajax({
        				url :"//" +pathname + "/project/step-3",
        				type : 'POST',
        				data: survey,
        				success : function(response) {
        					step4(response.id)
        					$("#reportIdStep4").html(response.reportId);
        					$('.loading').hide();
        				},
        				error:function(xhr,status,error){
        					}
        				});
        			
        			});
        	}
        		$('#save_close3').on('click',function(){
        			var survey=$('#step_3').serialize();
        			
        			$('#assignEmp input:checkbox').each(function () {
        				 (this.checked ? userId.push($(this).val()) : "")
        				});
        			
        			$('#surveyId').val(surveyId)
        			survey.id=surveyId;
        			
        			 window.location.href = "//" + pathname +"/dashboard";
        			 $('.loading').show();
        			$.ajax({
        				url :"//" +pathname + "/project/saveAndClose/step-3",
        				type : 'POST',
        				data: survey,
        				success : function(response) {
        					$('.loading').hide();

        				},
        				error:function(xhr,status,error){
        					}
        				});
        			});
        		
   /*###########################################################################################################################################*/
           /* STEP-4*/
/*###########################################################################################################################################*/


        		
        function step4(surveyId) {
            $('#step_4_Id').val(surveyId);
                    
     $('#step_4_save').on('click', function() {

        $(this).parents().find('.tabsParent .nav-tabs .step-5').click();
                var survey = $('#step_4').serialize();
                $('#surveyId').val(surveyId)
                survey.id = surveyId;
                $('.loading').show();
                $.ajax({
                    url: "//" + pathname + "/project/step-4",
                    type: 'POST',
                    data: survey,
                    success: function(response) {
                    	 $('.loading').hide();
                    	step5(response.id);
                    	$("#reportIdStep5").html(response.reportId);
                       
                    },
                    error: function(xhr, status, error) {}
                });
            });
        		}
            
            
            $('#save_close4').on('click',function(){
    			
    			var survey=$('#step_4').serialize();
    			
    			$('#assignEmp input:checkbox').each(function () {
    				 (this.checked ? userId.push($(this).val()) : "")
    				});
    			
    			$('#surveyId').val(surveyId)
    			survey.id=surveyId;
    			
    			 window.location.href = "//" + pathname +"/dashboard";
    			 $('.loading').show();
    			$.ajax({
    				url :"//" +pathname + "/project/saveAndClose/step-4",
    				type : 'POST',
    				data: survey,
    				success : function(response) {
    					//step3(response.id)
    					$('.loading').hide();
    				},
    				error:function(xhr,status,error){
    					//console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
    					}
    				});
    			});

  /*###########################################################################################################################################*/
            									/* STEP-5*/
 /*###########################################################################################################################################*/

            function step5(surveyId) {
                $('#step_5_Id').val(surveyId);
              
                $('#step_5_save').on('click', function() {
                	//alert("step_5_save")
                	/*$('.loading').show();
                    */
                	$(this).parents().find('.tabsParent .nav-tabs .step-6').click();
                    var form_data = new FormData($('#step_5')[0]);
                    //var screen = $('#step_5').serialize();
                    $('#assignEmp input:checkbox').each(function() {
                        (this.checked ? userId.push($(this).val()) : "")
                    });
                    
                    $('.loading').show();

                    $.ajax({
                        url: "//" + pathname + "/project/step-5",
                        type: 'POST',
                        data: form_data,
                        processData: false,
                        contentType: false,
                        async:true,
                        success: function(response) {
                        	$('.loading').hide();
                            step6(response.id)
                            $("#reportIdStep6").html(response.reportId);
                            getProject(response);
                            $('.loading').hide();


                        },
                        error: function(xhr, status, error) {}
                    });
                });
            }
            
            
            $('#save_close5').on('click',function(){
    			//$(this).parents().find('.tabsParent .nav-tabs .step-3').click();
    			var survey=$('#step_5').serialize();
    			//var userId=[];
    			$('#assignEmp input:checkbox').each(function () {
    				 (this.checked ? userId.push($(this).val()) : "")
    				});
    			
    			$('#surveyId').val(surveyId)
    			survey.id=surveyId;
    			
    			 window.location.href = "//" + pathname +"/dashboard";
    			 $('.loading').show();
    			$.ajax({
    				url :"//" +pathname + "/project/saveAndClose/step-5",
    				type : 'POST',
    				data: survey,
    				success : function(response) {
    					//step3(response.id)
    					$('.loading').hide();
    					
    				},
    				error:function(xhr,status,error){
    					//console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
    					}
    				});
    			});

                function getProject(projectdetails) {
                	var occupancyRevenue=projectdetails.occupancy;
                	var sph=projectdetails.sph;
                	console.log(occupancyRevenue);
                	var atp=projectdetails.atp;
                	 for (var i = 0; i < occupancyRevenue.length; i++) {
                		 var occupancy = occupancyRevenue[i];
                		 $('#occupancy').append('<tr class="categoryTitle"><td class="firstColumn "><input type="text" name="occupancy[' + i + '].formatType" value=\'' + occupancy.formatType + '\'></td><td><input type="text" name="occupancy.costType" placeholder="Enter Value" value=\'' + occupancy.costType +'\' ></td><td><input type="text" name="occupancy[' + i + '].inputValues" placeholder="Enter Value"></td><td><input type="text" name="occupancy[' + i + '].comments" placeholder="Enter Text"></td></tr>'); 
                	 }
                	 
                  	 for (var i = 0; i < sph.length; i++) {
                		 var sphRevnue = sph[i];
                		 $('#sph').append('<tr class="categoryTitle"><td class="firstColumn "><input type="text" name="sph[' + i + '].formatType" value=\'' + sphRevnue.formatType + '\'></td><td><input type="text" name="sph.suggestedValue" placeholder="Enter Value" value=\'' + sphRevnue.costType +'\'></td><td><input type="text" name="sph[' + i + '].inputValues" placeholder="Enter Value"></td><td><input type="text" name="sph[' + i + '].comments" placeholder="Enter Text"></td></tr>'); 

                	 }
                  	 
                 for (var i = 0; i < atp.length; i++) {
                		 var atpRevenue = atp[i];

               $('#atp').append('<tr class="categoryTitle">'+
						'<td colspan="4"><input type="text" name="atp[' + i + '].cinemaFormatType" value=\'' + atpRevenue.cinemaFormatType + '\'></td>'+
					'</tr>'+
					'<tr class="categoryTitle">'+
						'<td>Normal</td>'+
						'<td><input type="text" name="atp.normal.costType" placeholder="suggested value" value=\'' + atpRevenue.normal.costType + '\'></td>'+
						'<td><input type="text" name="atp[' + i + '].normal.inputValues" placeholder="input value"></td>'+
						'<td><input type="text" name="atp[' + i + '].normal.comments" placeholder="comments"></td>'+
					'</tr>'+
					'<tr class="categoryTitle">'+
						'<td>Recliner</td>'+
						'<td><input type="text" name="atp.recliner.costType" placeholder="suggested value" value=\'' + atpRevenue.recliner.costType + '\'></td>'+
						'<td><input type="text" name="atp[' + i + '].recliner.inputValues" placeholder="input value"></td>'+
						'<td><input type="text" name="atp[' + i + '].recliner.comments" placeholder="comments"></td>'+
					'</tr>'+
					'<tr class="categoryTitle">'+
						'<td>Loungers</td>'+
						'<td><input type="text" name="atp.lounger.costType" placeholder="suggested value" value=\'' + atpRevenue.lounger.costType + '\'></td>'+
						'<td><input type="text" name="atp[' + i + '].lounger.inputValues" placeholder="input value"></td>'+
						'<td><input type="text" name="atp[' + i + '].lounger.comments" placeholder="comments"></td>'+
					'</tr>');       
                		}
                	
             
                    var houseKeepingExpense = projectdetails.houseKeepingExpense;
                    var securityExpense = projectdetails.securityExpense;
                    var communicationExpense = projectdetails.communicationExpense;
                    var travellingAndConveyanceExpense = projectdetails.travellingAndConveyanceExpense;
                    var legalFee = projectdetails.legalFee;
                    var insuranceExpense = projectdetails.insuranceExpense;
                    var internetExpense = projectdetails.internetExpense;
                    var printingAndStationaryExpense = projectdetails.printingAndStationaryExpense;
                    var marketingExpense = projectdetails.marketingExpense;
                    var miscellaneousExpenses = projectdetails.miscellaneousExpenses;
                    
               
                    
                    $('#expenses').append('<tr class="">'+
							'<td class="firstColumn">Housekeeping expense</td>'+
							'<td><input type="text" name="houseKeepingExpense.costType"'+
								'value=\'' + houseKeepingExpense.costType + '\'></td>'+
							'<td><input type="text" name="houseKeepingExpense.inputValues"'+
								'placeholder="Enter Value"></td>'+
							'<td><input type="text" name="houseKeepingExpense.comments"'+
								'placeholder="Enter Text"></td>'+
						'</tr>'+
						'<tr class="">'+
							'<td class="firstColumn">Security expense</td>'+
							'<td><input type="text" name="securityExpense.costType"'+
								'value=\'' + securityExpense.costType + '\'></td>'+
							'<td><input type="text" name="securityExpense.inputValues"'+
								'placeholder="Enter Value"></td>'+
							'<td><input type="text" name="securityExpense.comments"'+
								'placeholder="Enter Text"></td>'+
						'</tr>'+
						'<tr class="">'+
							'<td class="firstColumn">Communication expense</td>'+
							'<td><input type="text" name="communicationExpense.costType"'+
								'value=\'' + communicationExpense.costType + '\'></td>'+
							'<td><input type="text" name="communicationExpense.inputValues"'+
								'placeholder="Enter Value"></td>'+
							'<td><input type="text" name="communicationExpense.comments"'+
								'placeholder="Enter Text"></td>'+
						'</tr>'+
						'<tr class="">'+
							'<td class="firstColumn">Travelling and conveyance'+
							'	expense</td>'+
							'<td><input type="text" name="travellingAndConveyanceExpense.costType"'+
							'	value=\'' + travellingAndConveyanceExpense.costType + '\'></td>'+
							'<td><input type="text" name="travellingAndConveyanceExpense.inputValues"'+
							'	placeholder="Enter Value"></td>'+
							'<td><input type="text" name="travellingAndConveyanceExpense.comments"'+
							'	placeholder="Enter Text"></td>'+
						'</tr>'+
						'<tr class="">'+
							'<td class="firstColumn">Legal fee</td>'+
							'<td><input type="text" name="legalFee.costType"'+
							'	value=\'' + legalFee.costType + '\'></td>'+
							'<td><input type="text" name="legalFee.inputValues"'+
							'	placeholder="Enter Value"></td>'+
							'<td><input type="text" name="legalFee.comments"'+
							'	placeholder="Enter Text"></td>'+
						'</tr>'+
						'<tr class="">'+
							'<td class="firstColumn">Insurance expense</td>'+
							'<td><input type="text" name="insuranceExpense.costType"'+
							'	value=\'' + insuranceExpense.costType + '\'></td>'+
							'<td><input type="text" name="insuranceExpense.inputValues"'+
							'	placeholder="Enter Value"></td>'+
							'<td><input type="text" name="insuranceExpense.comments"'+
							'	placeholder="Enter Text"></td>'+
						'</tr>'+
						'<tr class="">'+
							'<td class="firstColumn">Internet expenses</td>'+
							'<td><input type="text" name="internetExpense.costType"'+
							'	value=\'' + internetExpense.costType + '\'></td>'+
							'<td><input type="text" name="internetExpense.inputValues"'+
							'	placeholder="Enter Value"></td>'+
							'<td><input type="text" name="internetExpense.comments"'+
							'	placeholder="Enter Text"></td>'+
						'</tr>'+
						'<tr class="">'+
							'<td class="firstColumn">Printing and stationary'+
							'	expense</td>'+
							'<td><input type="text" name="printingAndStationaryExpense.costType"'+
							'	value=\'' + printingAndStationaryExpense.costType + '\'></td>'+
							'<td><input type="text" name="printingAndStationaryExpense.inputValues"'+
							'	placeholder="Enter Value"></td>'+
							'<td><input type="text" name="printingAndStationaryExpense.comments"'+
							'	placeholder="Enter Text"></td>'+
						'</tr>'+
						'<tr class="">'+
						'	<td class="firstColumn">Marketing expense</td>'+
							'<td><input type="text" name="marketingExpense.costType"'+
							'	value=\'' + marketingExpense.costType + '\'></td>'+
							'<td><input type="text" name="marketingExpense.inputValues"'+
							'	placeholder="Enter Value"></td>'+
							'<td><input type="text" name="marketingExpense.comments"'+
							'	placeholder="Enter Text"></td>'+
						'</tr>'+
						'<tr class="">'+
							'<td class="firstColumn">Miscellaneous expenses</td>'+
							'<td><input type="text" name="miscellaneousExpenses.costType"'+
							'	value=\'' + miscellaneousExpenses.costType + '\'></td>'+
							'<td><input type="text" name="miscellaneousExpenses.inputValues"'+
							'	placeholder="Enter Value"></td>'+
							'<td><input type="text" name="miscellaneousExpenses.comments"'+
							'	placeholder="Enter Text"></td>'+
						'</tr>');
                  //  console.log(projectdetails);
                    
                    
                    var personalExpenses = projectdetails.personalExpenses;
                    var electricityWaterBill = projectdetails.electricityWaterBill;
                    var totalRMExpenses = projectdetails.totalRMExpenses;
                    var totalOverhead = projectdetails.totalOverhead;
                    
                    
                   $('#expenses_costType').append('<input type="text"'+
					'value=\'' + personalExpenses.costType + '\' name="personalExpenses.costType"'+
					'placeholder="Enter Value">');
                    
                    $('#expenses_inputValue').append('<input type="text"'+
					'name="personalExpenses.inputValues"'+
					'placeholder="Enter Value">');
                    
                   $('#expenses_comments').append('<input type="text"'+
					'name="personalExpenses.comments"'+ 
					'placeholder="Enter Text">');
                    
                     $('#electricity_cost').append('<input type="text"'+
        			' value=\'' + electricityWaterBill.costType + '\' name="electricityWaterBill.costType"'+
        			'placeholder="Enter Value">');
                            
                    $('#electricity_inputValue').append('<input type="text"'+
        			'name="electricityWaterBill.inputValues"'+
        			'placeholder="Enter Value">');
                            
                     $('#electricity_comments').append('<input type="text"'+
        			'name="electricityWaterBill.comments"'+ 
        			'placeholder="Enter Text">');
                    
                    $('#RM_cost').append('<input type="text"'+
                	'value=\'' + totalRMExpenses.costType + '\' name="totalRMExpenses.costType"'+
                	'placeholder="Enter Value">');
                                    
                    $('#RM_inputValue').append('<input type="text"'+
                	'name="totalRMExpenses.inputValues"'+
                	'placeholder="Enter Value">');
                                    
                    $('#RM_comments').append('<input type="text"'+
                	'name="totalRMExpenses.comments"'+ 
                	'placeholder="Enter Text">');
                    

                    
                    var adRevenue = projectdetails.adRevenue;
                    var webSale = projectdetails.webSale;
                    
                    
                    $('#adRevenue_costType').append('<input type="text"'+
        			'value=\'' + adRevenue.costType + '\' name="adRevenue.costType"'+
        			'placeholder="Enter Value">');
                            
                     $('#adRevenue_inputValue').append('<input type="text"'+
        			 'name="adRevenue.inputValues"'+
        			 'placeholder="Enter Value">');
                            
                     $('#adRevenue_comments').append('<input type="text"'+
        			 'name="adRevenue.comments"'+ 
        			 'placeholder="Enter Text">');
                           
                     $('#webSale_costType').append('<input type="text"'+
               		 'value=\'' + webSale.costType + '\' name="webSale.costType"'+
               		 'placeholder="Enter Value">');
                                   
                     $('#webSale_inputValue').append('<input type="text"'+
               		 'name="webSale.inputValues"'+
               		 'placeholder="Enter Value">');
                                   
                      $('#webSale_comments').append('<input type="text"'+
               		 'name="webSale.comments"'+ 
               		  'placeholder="Enter Text">');
                    
                    
                }
                
                

                function step6(surveyId) {
                    $('#step_6_Id').val(surveyId);
                    //alert(surveyId);
                    $('#step_6_save').on('click', function() {
                     //   console.log("screen data");
                        $(this).parents().find('.tabsParent .nav-tabs .step-7').click();
                        var form_data = new FormData($('#step_6')[0]);

                        $('.loading').show();

                        $.ajax({
                            url: "//" + pathname + "/project/step-6",
                            type: 'POST',
                            data: form_data,
                            processData: false,
                            contentType: false,
                            async:false,
                            success: function(response) {
                            	  $('.loading').hide();
                                getProjectDetail(response)
                                step7(response.id)
                              
                            },
                            error: function(xhr, status, error) {}
                        });
                    });
                }
                
                $('#save_close6').on('click',function(){
        			//$(this).parents().find('.tabsParent .nav-tabs .step-3').click();
        			var survey=$('#step_6').serialize();
        			//var userId=[];
        			$('#assignEmp input:checkbox').each(function () {
        				 (this.checked ? userId.push($(this).val()) : "")
        				});
        			
        			$('#surveyId').val(surveyId)
        			survey.id=surveyId;
        			
        			 window.location.href = "//" + pathname +"/dashboard";
        			 $('.loading').show();
        			$.ajax({
        				url :"//" +pathname + "/project/saveAndClose/step-6",
        				type : 'POST',
        				data: survey,
        				success : function(response) {
        					//step3(response.id)
        					$('.loading').hide();
        				},
        				error:function(xhr,status,error){
        					}
        				});
        			});

                function getProjectDetail(projectdetails) {
                    var detail = projectdetails;
                    //console.log(projectdetails.finalSummary);
            $('#detail_project').append('<div class="heading_title_strip">' + '<h3>' + 'feasibility report <small>(' + detail.reportId + ')</small>' + '</h3>' + '</div>' + '<div class="title_strip">' + '<h5>project DETAILS</h5>' + '</div>' + '<div class="col-lg-3 col-sm-4 float-left">' + '<fieldset class="form-group bmd-form-group">' + '<label class="control-label bmd-label-static">Report' + 'ID</label> <input type="text" class="form-control" name="" value=\'' + detail.reportId + '\' placeholder="xxx" spellcheck="true">' + '</fieldset>' + '</div>' + '<div class="col-lg-3 col-sm-4 float-left">' + '<fieldset class="form-group bmd-form-group">' + '<label class="control-label bmd-label-static">Project' + 'Name</label> <input type="text" class="form-control" name="" value=\'' + detail.projectName + '\' placeholder="xxx" spellcheck="true">' + '</fieldset>' + '</div>' + '<div class="col-lg-3 col-sm-4 float-left">' + '<fieldset class="form-group bmd-form-group">' + '<label class="control-label bmd-label-static">Locality</label>' + '<input type="text" class="form-control" name="" value=\'' + detail.projectLocation.locality + '\' placeholder="xxx" spellcheck="true">' + '</fieldset>' + '</div>' + '<div class="col-lg-3 col-sm-4 float-left">' + '<fieldset class="form-group bmd-form-group">' + '<label class="control-label bmd-label-static">City</label> <input type="text" class="form-control" name="" value=\'' + detail.projectLocation.city + '\' placeholder="xxx" spellcheck="true">' + '</fieldset>' + '</div>' + '<div class="col-lg-3 col-sm-4 float-left">' + '<fieldset class="form-group bmd-form-group">' + '<label class="control-label bmd-label-static">Developer' + 'Name</label> <input type="text" class="form-control" name="" value=\'' + detail.developer + '\' placeholder="xxx" spellcheck="true">' + '</fieldset>' + '</div>' + '<div class="col-lg-3 col-sm-4 float-left">' + '<fieldset class="form-group bmd-form-group">' + '<label class="control-label bmd-label-static">Carpet' + 'Area (sq ft)</label> <input type="text" class="form-control" name="" value=\'' + detail.lesserLesse.carpetArea + '\' placeholder="xxx" spellcheck="true">' + '</fieldset>' + '</div>' + '<div class="col-lg-3 col-sm-4 float-left">' + '<fieldset class="form-group bmd-form-group">' + '<label class="control-label bmd-label-static">Date' + 'of opening</label> <input type="text" class="form-control" name="" value=\'' + detail.dateOfOpening + '\' placeholder="xxx" spellcheck="true">' + '</fieldset>' + '</div><div class="col-lg-3 col-sm-4 float-left">' + '<fieldset class="form-group bmd-form-group">' + '<label class="control-label bmd-label-static">Date' + 'of handover</label> <input type="text" class="form-control" name="" value=\'' + detail.dateOfHandover + '\' placeholder="xxx" spellcheck="true">' + '</fieldset>' + '</div>');
            
            var personalExpenses = projectdetails.personalExpenses;
            var electricityWaterBill = projectdetails.electricityWaterBill;
            var totalRMExpenses = projectdetails.totalRMExpenses;
            var totalOverhead = projectdetails.totalOverhead;
            var finalSummary = projectdetails.finalSummary;
            console.log(finalSummary)
            
            $('#pbdit_finalSummary').append('<input type="text"'+
         			'value=\'' + finalSummary.payBackPBDIT + '\' name="finalSummary.payBackPBDIT"'+
         			'placeholder="Enter Value">');
            
            $('#irr_finalSummary').append('<input type="text"'+
         			'value=\'' + finalSummary.irr + '\' name="finalSummary.irr"'+
         			'placeholder="Enter Value">');
            
            $('#expenses_costType_Final_summary').append('<input type="text"'+
 			'value=\'' + personalExpenses.costType + '\' name="finalSummary.personalExpense.costType"'+
 			'placeholder="Enter Value">');
             
             $('#expenses_inputValue_Final_summary').append('<input type="text"'+
            'value=\'' + personalExpenses.inputValues + '\' name="finalSummary.personalExpense.inputValues"'+ 
 			'placeholder="Enter Value">');
             
            $('#expenses_comments_Final_summary').append('<input type="text"'+
            'value=\'' +personalExpenses.comments + '\' name="finalSummary.personalExpense.comments"'+ 
 			'placeholder="Enter Text">');
             
            
            
            
              $('#electricity_cost_Final_summary').append('<input type="text"'+
 			' value=\'' + electricityWaterBill.costType + '\' name="finalSummary.electricityWaterExpanse.costType"'+
 			'placeholder="Enter Value">');
                     
             $('#electricity_inputValue_Final_summary').append('<input type="text"'+
            'value=\''  +electricityWaterBill.inputValues + '\' name="finalSummary.electricityWaterExpanse.inputValues"'+
 			'placeholder="Enter Value">');
                     
              $('#electricity_comments_Final_summary').append('<input type="text"'+
            'value=\''  +electricityWaterBill.comments + '\' name="finalSummary.electricityWaterExpanse.comments"'+ 
 			'placeholder="Enter Text">');
             
             $('#RM_cost_Final_summary').append('<input type="text"'+
         	'value=\'' + totalRMExpenses.costType + '\' name="finalSummary.totalRmExpenses.costType"'+
         	'placeholder="Enter Value">');
                             
             $('#RM_inputValue_Final_summary').append('<input type="text"'+
            'value=\'' + totalRMExpenses.inputValues + '\'name="finalSummary.totalRmExpenses.inputValues"'+ 
         	'placeholder="Enter Value">');
                             
             $('#RM_comments_Final_summary').append('<input type="text"'+
            'value=\'' + totalRMExpenses.comments + '\' name="finalSummary.totalRmExpenses.comments"'+ 
         	'placeholder="Enter Text">');
             
   
             
             $('#totalAdmits_finalSummary_cost').append('<input type="text"'+
         	'value=\'' + finalSummary.totalAdmits.costType + '\' name="finalSummary.totalAdmits.costType"'+
         	'placeholder="Enter Value">');
                     
             $('#totalAdmits_finalSummary_inputValue').append('<input type="text"'+
            'value=\'' + finalSummary.totalAdmits.inputValues + '\' name="finalSummary.totalAdmits.inputValues"'+ 
         	'placeholder="Enter Value">');
                     
             $('#totalAdmits_finalSummary_comments').append('<input type="text"'+
            'name="finalSummary.totalAdmits.comments"'+ 
         	'placeholder="Enter Text">');
             
             
             $('#occupancy_finalSummary_cost').append('<input type="text"'+
             'value=\'' + finalSummary.occupancy.costType + '\' name="finalSummary.occupancy.costType"'+
             'placeholder="Enter Value">');
                            
             $('#occupancy_finalSummary_inputValue').append('<input type="text"'+
             'value=\'' + finalSummary.occupancy.inputValues + '\' name="finalSummary.occupancy.inputValues"'+ 
             'placeholder="Enter Value">');
                            
             $('#occupancy_finalSummary_comments').append('<input type="text"'+
            'value=\'' + finalSummary.occupancy.comments + '\' name="finalSummary.occupancy.comments"'+ 
             'placeholder="Enter Text">');
             
             
             $('#atp_finalSummary_cost').append('<input type="text"'+
             'value=\'' + finalSummary.atp.costType + '\' name="finalSummary.atp.costType"'+
             'placeholder="Enter Value">');
                                    

             $('#atp_finalSummary_inputValue').append('<input type="text"'+
            'value=\'' + finalSummary.atp.inputValues + '\'name="finalSummary.atp.inputValues" projectdetails'+ 
             'placeholder="Enter Value">');
             
             $('#atp_finalSummary_comments').append('<input type="text"'+
            'value=\'' + finalSummary.atp.comments + '\' name="finalSummary.atp.comments" projectdetails'+ 
                      'placeholder="Enter Value">');
             
             $('#sph_finalSummary_cost').append('<input type="text"'+
             'value=\'' + finalSummary.sph.costType + '\' name="finalSummary.sph.costType"'+
             'placeholder="Enter Value">');
                                            
             $('#sph_finalSummary_inputValue').append('<input type="text"'+
              'value=\''+ finalSummary.sph.inputValues + '\'name="finalSummary.sph.inputValues"'+ 
             'placeholder="Enter Value">');
                                            
              $('#sph_finalSummary_comments').append('<input type="text"'+
            	'value=\'' + finalSummary.sph.comments + '\' name="finalSummary.sph.comments"'+ 
             'placeholder="Enter Text">');
              
              $('#adRevenue_finalSummary_cost').append('<input type="text"'+
              'value=\'' + finalSummary.adRevenue.costType + '\' name="finalSummary.adRevenue.costType"'+
              'placeholder="Enter Value">');
                                                     
              $('#adRevenue_finalSummary_inputValue').append('<input type="text"'+
            	'value=\'' + finalSummary.adRevenue.inputValues + '\' name="finalSummary.adRevenue.inputValues"'+ 
              'placeholder="Enter Value">');
                                                     
              $('#adRevenue_finalSummary_comments').append('<input type="text"'+
            'value=\'' + finalSummary.adRevenue.comments + '\' name="finalSummary.adRevenue.comments"'+ 
              'placeholder="Enter Text">');
              
           
              
              $('#final_cost_summary').append('<input type="text" class="form-control"  name="finalSummary.totalProjectCost.inputValues" value=\'' + finalSummary.totalProjectCost.costType + '\' spellcheck="true">');
                 
              $('#leasePeriodFinal').val(finalSummary.leasePeriod);
              $('#annualRent').val(finalSummary.annualRent);
              $('#annualCam').val(finalSummary.annualCam);
                 
                 
        }



                function step7(surveyId) {
                    $('#step_7_Id').val(surveyId);
                   
                    $('#step_7_save').on('click', function() {
                        var screen = $('#step_7').serialize();
                        $('#surveyId').val(surveyId)
                        screen.id = surveyId;
                        
                        window.location.href = "//" + pathname +"/dashboard";
                        $('.loading').show();
                        $.ajax({
                            url: "//" + pathname + "/project/step-7",
                            type: 'POST',
                            data: screen,
                            success: function(response) {
                            	$('.loading').hide();
                            },
                            error: function(xhr, status, error) {}
                        });
                    });
                }

                function populateDataOnLoad() {
                    if (type === "tab_B") {
                        $("#step_2_Id").val(surveyId);
                        step2(surveyId);
                        $("#tab_A").removeClass("nav-link active").addClass("nav-link");
                        $("#step-1").removeClass("tab-pane fade active show").addClass("tab-pane fade");
                        $("#tab_B").removeClass("nav-link assigned-emp").addClass("nav-link assigned-emp active show");
                        $("#step-2").removeClass("tab-pane fade active").addClass("tab-pane fade active show");
                    }
                    if (type === "tab_C") {
                        $("#step_3_Id").val(surveyId);
                        step3(surveyId);
                        $("#tab_A").removeClass("nav-link active").addClass("nav-link");
                        $("#step-1").removeClass("tab-pane fade active show").addClass("tab-pane fade");
                        $("#tab_C").removeClass("nav-link assigned-emp").addClass("nav-link assigned-emp active show");
                        $("#step-3").removeClass("tab-pane fade active").addClass("tab-pane fade active show");
                    }
                    if (type === "tab_D") {
                        $("#step_4_Id").val(surveyId);
                        step4(surveyId);
                        $("#tab_A").removeClass("nav-link active").addClass("nav-link");
                        $("#step-1").removeClass("tab-pane fade active show").addClass("tab-pane fade");
                        $("#tab_D").removeClass("nav-link assigned-emp").addClass("nav-link assigned-emp active show");
                        $("#step-4").removeClass("tab-pane fade active").addClass("tab-pane fade active show");
                    }
                    if (type === "tab_E") {
                        $("#step_5_Id").val(surveyId);
                        step4(surveyId);
                        $("#tab_A").removeClass("nav-link active").addClass("nav-link");
                        $("#step-1").removeClass("tab-pane fade active show").addClass("tab-pane fade");
                        $("#tab_E").removeClass("nav-link assigned-emp").addClass("nav-link assigned-emp active show");
                        $("#step-5").removeClass("tab-pane fade active").addClass("tab-pane fade active show");
                    }
                    if (type === "tab_F") {
                        $("#step_6_Id").val(surveyId);
                        step4(surveyId);
                        $("#tab_A").removeClass("nav-link active").addClass("nav-link");
                        $("#step-1").removeClass("tab-pane fade active show").addClass("tab-pane fade");
                        $("#tab_F").removeClass("nav-link assigned-emp").addClass("nav-link assigned-emp active show");
                        $("#step-6").removeClass("tab-pane fade active").addClass("tab-pane fade active show");
                    }
                    if (type === "tab_G") {
                        $("#step_7_Id").val(surveyId);
                        step4(surveyId);
                        $("#tab_A").removeClass("nav-link active").addClass("nav-link");
                        $("#step-1").removeClass("tab-pane fade active show").addClass("tab-pane fade");
                        $("#tab_G").removeClass("nav-link assigned-emp").addClass("nav-link assigned-emp active show");
                        $("#step-7").removeClass("tab-pane fade active").addClass("tab-pane fade active show");
                    }
                }
                
                

                $(document).on('change', '#cinemaFormatField', function(){
                	var i=0;
                	$(this).parents().find('#screenDetailTbody > #screen_detail').each(function(){
                		$(this).find('#cinemaFormatField').each(function(){
                			var cinema_val = $(this).val();
                			
                			var j=0;
                			$('.tabsParent #step3Table1 >tbody#seatDimensionTbody >tr#seat_dimension').each(function(){
                				$('.tabsParent #step3Table1 >tbody#seatDimensionTbody >tr#seat_dimension #cinemaFormatField').each(function(){
                					if(i==j){
                						$(this).val(cinema_val);
                					}
                        			j++;
                        		});
                			});
                			
                			i++
                		});
                	});
                });
               

                    $(document).on('change', '#screenNo', function() {
                        $('.tabsParent #step3Table >tbody#screenDetailTbody >tr#screen_detail').remove('');
                        $('.tabsParent #step3Table1 >tbody#seatDimensionTbody >tr#seat_dimension').remove('');
                        for (var i = 0; i < this.value; i++) {
                            var screenDetailClone= '<tr id="screen_detail" class="categoryTitle">' + ' <th scope="row" class="screenName">Screen ' + (i + 1) + '</th>' + '  <td>' + ' <select id="cinemaFormatField" name="cinemaFormat[' + i + '].formatType" class="form-control">' + '  <option value="mainstream">MainStream</option>' + ' <option value="gold">Gold</option>' + ' <option value="imax">Imax</option>' + ' <option value="pxl">PXL</option>' + ' <option value="playhouse">PlayHouse</option>' + ' <option value="ultrapremium">Ultra Premium</option>' + ' <option value="4dx">4DX</option>' + ' <option value="premier">Premier</option>' + ' <option value="onyx">Onyx</option>' + ' </select>' + '   </td>' + ' <td><input type="text" id="a" class="qty1" name="cinemaFormat[' + i + '].normal" placeholder="Enter Value"></td>' + ' <td><input type="text" id="b" class="qty1" name="cinemaFormat[' + i + '].recliners" placeholder="Enter Value"></td>' + ' <td><input type="text" id="c" class="qty1" name="cinemaFormat[' + i + '].lounger" placeholder="Enter Value"></td>' + ' <td><input type="text" id="d' + i + '" class="totalColumn" name="cinemaFormat[' + i + '].total" placeholder="0" style="width:70px;pointer-events:none;" ></td>' + ' </tr> ';
                            $('.tabsParent #step3Table >tbody#screenDetailTbody').append('<tr id="screen_detail"> ' + screenDetailClone + ' </tr>');
                            
                            var seatDimensionClone= '<tr id="seat_dimension" class="categoryTitle">' + ' <th scope="row" class="screenName">Screen ' + (i + 1) + '</th>' + '  <td>' + ' <select id="cinemaFormatField" name="seatDimension[' + i + '].formatType" class="form-control">' + '  <option value="mainstream">MainStream</option>' + ' <option value="gold">Gold</option>' + ' <option value="imax">Imax</option>' + ' <option value="pxl">PXL</option>' + ' <option value="playhouse">PlayHouse</option>' + ' <option value="ultrapremium">Ultra Premium</option>' + ' <option value="4dx">4DX</option>' + ' <option value="premier">Premier</option>' + ' <option value="onyx">Onyx</option>' + ' </select>' + '   </td>' + ' <td><input type="text" id="a" class="qty1" name="seatDimension[' + i + '].normal" placeholder="Enter Value"></td>' + ' <td><input type="text" id="b" class="qty1" name="seatDimension[' + i + '].recliners" placeholder="Enter Value"></td>' + ' <td><input type="text" id="c" class="qty1" name="seatDimension[' + i + '].lounger" placeholder="Enter Value"></td>'+ '</td>' + ' </tr> ';
                            $('.tabsParent #step3Table1 >tbody#seatDimensionTbody').append('<tr id="seat_dimension"> ' + seatDimensionClone + ' </tr>');
                        }
                        $(".qty1").on('keyup change', calculateSum);

                        
                        
                        
                        
                        function calculateSum() {
                            var $input = $(this);
                            var $row = $input.closest('#screen_detail');
                            var sum = 0;
                            $row.find(".qty1").each(function() {
                                sum += parseFloat(this.value) || 0;
                            });
                            $row.find(".totalColumn").val(sum.toFixed());
                        }
                    });

                    $(".qty1").on('keyup change', calculateSum);

                    function calculateSum() {
                        var $input = $(this);
                        var $row = $input.closest('#screen_detail');
                        var sum = 0;
                        $row.find(".qty1").each(function() {
                            sum += parseFloat(this.value) || 0;
                        });
                        $row.find(".totalColumn").val(sum.toFixed());
                    }

                    $(document).on('blur', '.qty1', regularSumCalc);

                    function regularSumCalc() {
                        $('#screenDetailTbody #screen_detail').each(function() {
                            var normalSum = 0;
                            $('#screen_detail #a').each(function() {
                                normalSum += parseFloat(this.value) || 0;
                                $('#subtotal .normalsValSum').val(normalSum.toFixed())
                            })
                        });
                    }

                    $(document).on('blur', '.qty1', reclinerSumCalc);

                    function reclinerSumCalc() {
                        $('#screenDetailTbody #screen_detail').each(function() {
                            var normalSum = 0;
                            $('#screen_detail #b').each(function() {
                                normalSum += parseFloat(this.value) || 0;
                                $('#subtotal .reclinerValSum').val(normalSum.toFixed())
                            })
                        });
                    }

                    $(document).on('blur', '.qty1', loungersSumCalculate);

                    function loungersSumCalculate() {
                        $('#screenDetailTbody #screen_detail').each(function() {
                            var normalSum = 0;
                            $('#screen_detail #c').each(function() {
                                normalSum += parseFloat(this.value) || 0;
                                $('#subtotal .loungersValSum').val(normalSum.toFixed())
                            })
                        });
                    }

                    $(document).on('blur', '.qty1', loungersSumCalc);

                    function loungersSumCalc() {
                        $('#screenDetailTbody #screen_detail').each(function() {
                            var normalSum = 0;
                            $('#screen_detail .totalColumn').each(function() {
                                normalSum += parseFloat(this.value) || 0;
                                $('#subtotal .totalColumnValSum').val(normalSum.toFixed())
                            });
                        });
                    }

                                                        
                        var file = $('[name="file"]');
                        
                        $(document).on('click','#upload', function() {
                        	var step6_id =$('#step_6_Id').attr('value');
                        	//alert(step6_id);
                        	
                             var fd = new FormData();
                             var file_data = $('#uploadfile')[0].files; // for multiple files
                                 fd.append("file", file_data[0]);
                                 fd.append("id", step6_id);
                                 $('.loading').show();
                            $.ajax({
                            	url :"//" +pathname + "/upload/projectCostSummary",
                                type: "POST",
                                data: fd,
                                enctype: 'multipart/form-data',
                                processData: false,
                                contentType: false
                              }).done(function(data) {
                            	  //console.log(data);
                            	  getProjectDetailupload(data);
                            	  $('.loading').hide();
                                  
                              }).fail(function(jqXHR, textStatus) {
                                  //alert(jqXHR.responseText);
                                  alert('File upload failed ...');
                              });
                            
                        });

                        
                        $('#btnClear').on('click', function() {
                            imgContainer.html('');
                            file.val('');

                        });
                        
                        $('#uploadfile').change(function(e){
                            var fileName = e.target.files[0].name;
                            var fileCurrentName = $(this).closest('.file-select').find('.selectFile').text(fileName);
                        });
                    
                    
                    function getProjectDetailupload(data) {
                        var detail = data;
                       // console.log(data);
                        $('#projectCost').append('<input type="text" class="form-control"  name="projectCost" value=\'' + data.totalProjectCost + '\' spellcheck="true">');
                        $('#depreciation').append('<input type="text" class="form-control"  name="depreciation" value=\'' + data.depreciation + '\' spellcheck="true">');
                    
                    }
                        
                        
                function getReportId(reportId) {
                    var ReportId = reportId;
                    //console.log(ReportId);
                    $('#reportId2').append('<div id="stickID" style="top:0;">' + '<label for="exampleInputEmail1" class="bmd-label-static">Report' + 'D</label> <input type="hidden" name="reportId" value=\'' + ReportId + '\' spellcheck="true"></div>');
                }


                function generateTable(noOfColumns, myTable) {
                    $('#theadFixedTable').find('tr').remove();
                    $('#tbodyFixedTable').find('tr').remove();
                    $('#theadFixedTable').append('<tr><th class="fixed-side">Lease Period (Yrs)</th></tr>');
                    $('#tbodyFixedTable').append('<tr><th class="fixed-side">Rent (INR)</th></tr>');
                    for (var i = 1; i <= noOfColumns; i++) {
                        $('#tblFixed tr').append($("<td>"));
                        $('#tblFixed thead tr>td:last').html(i);
                        $('#tblFixed tbody tr').each(function() {
                            $(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent[' + i + ']" placeholder="Enter Value"/>'))
                        });
                    }
                }
                
                
                $('#viewModalData').on('click', function() {
                	var projectID =$('#step_6_Id').attr('value');
                	//alert(projectID);
                	 $.ajax({
                         url: "//" + pathname + "/project/getAllProjectCostSummary/"+projectID,
                         type: 'GET',
                         success: function(response) {
                         	console.log(response);
                         	generateTableForProjectSummary(response);
                         },
                         error: function(xhr, status, error) {
                        	 console.log(xhr+"  "+ status+"  "+ error);
                         }
                     });
                });
                
                function generateTableForProjectSummary(response){
                	 $("#d1").text(response.projectCostDataList[0].cost);
                     $("#d2").text(response.projectCostDataList[0].depRate);
                     $("#d3").text(response.projectCostDataList[0].annualDep);
                     $("#d4").text(response.projectCostDataList[1].cost);
                     $("#d5").text(response.projectCostDataList[1].depRate);
                     $("#d6").text(response.projectCostDataList[1].annualDep);
                     $("#d7").text(response.projectCostDataList[3].cost);
                     $("#d8").text(response.projectCostDataList[3].depRate);
                     $("#d9").text(response.projectCostDataList[3].annualDep);
                     $("#d10").text(response.projectCostDataList[4].cost);
                     $("#d11").text(response.projectCostDataList[4].depRate);
                     $("#d12").text(response.projectCostDataList[4].annualDep);
                     $("#d13").text(response.projectCostDataList[5].cost);
                     $("#d14").text(response.projectCostDataList[5].depRate);
                     $("#d15").text(response.projectCostDataList[5].annualDep);
                     $("#d16").text(response.projectCostDataList[6].cost);
                     $("#d17").text((response.projectCostDataList[6].depRate).toFixed(2));
                     $("#d18").text(response.projectCostDataList[6].annualDep);
                     $("#d19").text(response.projectCostDataList[8].cost);
                     $("#d20").text(response.projectCostDataList[8].depRate);
                     $("#d21").text(response.projectCostDataList[8].annualDep);
                     $("#d22").text(response.projectCostDataList[9].cost);
                     $("#d23").text(response.projectCostDataList[9].depRate);
                     $("#d24").text(response.projectCostDataList[9].annualDep);
                     $("#d25").text(response.projectCostDataList[10].cost);
                     $("#d26").text(response.projectCostDataList[10].depRate);
                     $("#d27").text(response.projectCostDataList[10].annualDep);
                     $("#d28").text(response.projectCostDataList[11].cost);
                     $("#d29").text(response.projectCostDataList[11].depRate);
                     $("#d30").text(response.projectCostDataList[11].annualDep);
                     $("#d31").text(response.projectCostDataList[12].cost);
                     $("#d32").text(response.projectCostDataList[12].depRate);
                     $("#d33").text(response.projectCostDataList[12].annualDep);
                     $("#d34").text(response.projectCostDataList[13].cost);
                     $("#d35").text(response.projectCostDataList[13].depRate);
                     $("#d36").text(response.projectCostDataList[13].annualDep);
                     $("#d37").text(response.projectCostDataList[14].cost);
                     $("#d38").text(response.projectCostDataList[14].depRate);
                     $("#d39").text(response.projectCostDataList[14].annualDep);
                     
                     $("#d40").text(response.projectCostDataList[15].cost);
                     $("#d41").text(response.projectCostDataList[15].depRate);
                     $("#d42").text(response.projectCostDataList[15].annualDep);
                     $("#d43").text(response.projectCostDataList[16].cost);
                     $("#d44").text(response.projectCostDataList[16].depRate);
                     $("#d45").text(response.projectCostDataList[16].annualDep);
                     $("#d46").text(response.projectCostDataList[17].cost);
                     $("#d47").text(response.projectCostDataList[17].depRate);
                     $("#d48").text(response.projectCostDataList[17].annualDep);
                     
                     $("#d49").text(response.projectCostDataList[19].cost);
                     $("#d50").text(response.projectCostDataList[19].depRate);
                     $("#d51").text(response.projectCostDataList[19].annualDep);
                     $("#d52").text(response.projectCostDataList[20].cost);
                     $("#d53").text(response.projectCostDataList[20].depRate);
                     $("#d54").text(response.projectCostDataList[20].annualDep);
                     $("#d55").text(response.projectCostDataList[21].cost);
                     $("#d56").text(response.projectCostDataList[21].depRate);
                     $("#d57").text(response.projectCostDataList[21].annualDep);
                     $("#d58").text(response.projectCostDataList[22].cost);
                     $("#d59").text(response.projectCostDataList[22].depRate);
                     $("#d60").text(response.projectCostDataList[22].annualDep);
                     $("#d61").text(response.projectCostDataList[23].cost);
                     $("#d62").text(response.projectCostDataList[23].depRate);
                     $("#d63").text(response.projectCostDataList[23].annualDep);
                     $("#d64").text(response.projectCostDataList[24].cost);
                     $("#d65").text(response.projectCostDataList[24].depRate);
                     $("#d66").text(response.projectCostDataList[24].annualDep);
                     $("#d67").text(response.projectCostDataList[25].cost);
                     $("#d68").text(response.projectCostDataList[25].depRate);
                     $("#d69").text(response.projectCostDataList[25].annualDep);
                     $("#d70").text(response.projectCostDataList[26].cost);
                     $("#d71").text(response.projectCostDataList[26].depRate);
                     $("#d72").text(response.projectCostDataList[26].annualDep);
                     
                     $("#d73").text(response.projectCostDataList[28].cost);
                     $("#d74").text(response.projectCostDataList[28].depRate);
                     $("#d75").text(response.projectCostDataList[28].annualDep);
                     $("#d76").text(response.projectCostDataList[29].cost);
                     $("#d77").text(response.projectCostDataList[29].depRate);
                     $("#d78").text(response.projectCostDataList[29].annualDep);
                     $("#d79").text(response.projectCostDataList[30].cost);
                     $("#d80").text(response.projectCostDataList[30].depRate);
                     $("#d81").text(response.projectCostDataList[30].annualDep);
                     $("#d82").text(response.projectCostDataList[31].cost);
                     $("#d83").text(response.projectCostDataList[31].depRate);
                     $("#d84").text(response.projectCostDataList[31].annualDep);
                     $("#d85").text(response.projectCostDataList[32].cost);
                     $("#d86").text((response.projectCostDataList[32].depRate).toFixed(2));
                     $("#d87").text(response.projectCostDataList[32].annualDep);
                     $("#d88").text(response.projectCostDataList[33].cost);
                     $("#d89").text(response.projectCostDataList[33].depRate);
                     $("#d90").text(response.projectCostDataList[33].annualDep);
                     $("#d91").text(response.projectCostDataList[34].cost);
                     $("#d92").text(response.projectCostDataList[34].depRate);
                     $("#d93").text(response.projectCostDataList[34].annualDep);
                     $("#d94").text(response.projectCostDataList[35].cost);
                     $("#d95").text(response.projectCostDataList[35].depRate);
                     $("#d96").text(response.projectCostDataList[35].annualDep);
                     $("#d97").text(response.projectCostDataList[36].cost);
                     $("#d98").text(response.projectCostDataList[36].depRate);
                     $("#d99").text(response.projectCostDataList[36].annualDep);
                     $("#d100").text(response.projectCostDataList[37].cost.toFixed(2));
                     $("#d101").text(response.projectCostDataList[37].depRate);
                     $("#d102").text(response.projectCostDataList[37].annualDep);
                     $("#d103").text(response.projectCostDataList[38].cost.toFixed(2));
                     $("#d104").text(response.projectCostDataList[38].depRate);
                     $("#d105").text(response.projectCostDataList[38].annualDep);
				}
            
     });

