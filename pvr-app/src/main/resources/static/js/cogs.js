 $("#tblData").on('click','.editCogs',function(){
         // get the current row
         var currentRow=$(this).closest("tr"); 
         
         var col1=currentRow.find("td:eq(0)").text(); // get current row 1st TD value
         var col2=currentRow.find("input").val(); // get current row 2nd TD
         
         $('#edit_cogs_name').val(col1);
         $('#edit_cogs_value').val(col2);
         
         $('#editCogsModal').modal();
        
    });
	$(function() {
		  $('.decimalLimit').on('input', function() {
		    match = (/(\d{0,9})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
		    this.value = match[1] + match[2];
		  });
		});
	
 
function validateCogs() {

	var name = $('#cogs_name').val();
	var value = $('#cogs_value').val();

	$('#nameError').text("");
	$('#valueError').text("");

	if (name.trim() == '') {
		$('#nameCogsError').text("Cogs name cannot be empty.");
		$( "#cogs_name" ).focus();
		return false;
	}

	else if (value.trim() == '') {
		$('#valueCogsError').text("Value cannot be empty.");
		$( "#cogs_value" ).focus();
		return false;
	}
	else{
		
		$.ajax({
			url :"/admin/is-cogs-present/"+name,
			type:"GET",
			async: false,
			success : function(response) {
				console.log(response);
				if(!response){
					console.log(response);
					return true;
				}
				else{
					$('#nameError').text(name+" already exists.");
					return false;
				}
			},
			error:function(xhr,status,error){
				console.log('Error ' +JSON.stringify(status)+ "Error : " + error +" "+JSON.stringify(xhr))
				}
		});
		
		
		console.log(response);
	}
	return false;
}

function validateEdit() {
	var value = $('#fhc_value').val();

	$('#valueError').text("");
	if (value.trim() == '') {
		$('#valueError').text("Value cannot be empty.");
		$( "#fhc_value" ).focus();
		return false;
	}else{
		return true;
	}
	
	
}