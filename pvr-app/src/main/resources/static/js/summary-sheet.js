
//$(document).ready(function() {
//	var projectId = $("#id").val();
//	
//	$.ajax({
//		url :"/project/getProjectDetailsById/"+ projectId,
//		success : function(response) {
//			var noOfYears = response.data.lesserLesse.advanceRentPeriod != null ? Number(response.data.lesserLesse.advanceRentPeriod) : 0;
//			generateAdjustmentRentTableOnEdit(noOfYears,response.data.lesserLesse);
//		},
//		error:function(xhr,status,error){
//			console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
//		}
//	});
//	
//});
//
//function generateAdjustmentRentTableOnEdit(noOfColumns,lesserLesse){
//	
//	$('#theadAdvanceRentTable').find('tr').remove();
//	$('#tbodyAdvanceRentTable').find('tr').remove();
//	
//	$('#theadAdvanceRentTable').append('<tr><th class="fixed-side">Year</th></tr>');
//	$('#tbodyAdvanceRentTable').append('<tr><th class="fixed-side">Advance Rent amount to be adjusted </br> from Rent on commencement of Cinema operation</th></tr>');
//		
//	for(var i=1;i<=noOfColumns;i++){
//		
//		$('#tblAdvanceRent tr').append($("<td>"));
//		$('#tblAdvanceRent thead tr>td:last').html(i);
//		$('#tblAdvanceRent tbody tr').each(function(){
//			
//			$(this).children('td:last').append($('<input type="text" name="lesserLesse.yearWiseAdvanceRentAdjustment['+i+']" value='+lesserLesse.yearWiseAdvanceRentAdjustment[i]+' id="adjustment'+i+'" class="adjustmentRate" placeholder="Enter value" readonly/>'))
//		});	
//    }
//} 

$('#save').click(function() {
	form = $('#step-7');
	form.attr('action', '/project/summary-sheet/').trigger('submit');
});

$('#save_close').click(function() {
	form = $('#step-7');
	form.attr('action', '/project/summary-sheet-update/').trigger('submit');
});

$('#downloadFullReportBtn').click(function() {
	
	$('.loading').show();
	var reportId = $("#id").val();
	$.ajax({
		url :"/report/generate-file/"+ reportId,
		success : function(response) {
			console.log(response);
			if(response.status==200){
				window.location.href="/report/files/"+reportId;
				$('.loading').hide();
			}
			$('.loading').hide();
		},
		error:function(xhr,status,error){
			console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
			$('.loading').hide();
			}
	}); 
	
});



var httpPath="http://"+window.location.host
//open modal----------		
$(document).on('click','.query-modal',function(){
	
	autoScroll();
	$('#exampleModal').modal('show');
	
	var queryType = $(this).closest('td').attr('id');
	 var projectId = $("#id").val();
	 var reportId = $("#reportId").val();
	
		$.ajax({
			url :"/project/getProjectDetailsById/"+ projectId,
			success : function(response) {
				//console.log(response);
				setQuery(response,reportId,queryType);
			},
			error:function(xhr,status,error){
				console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
				}
		}); 
})



$(document).on('click', '.sendForApproval', function() {
	 var projectId = $("#id").val();
	 console.log(projectId);
		$.ajax({
			url :"/project/getProjectDetailsById/"+ projectId,
			success : function(response) {
				
				if(validate(response)){
					$("#approverModal").modal('show');	
				}
				
				
			},
			error:function(xhr,status,error){
				console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
				}
		}); 
	
		var projectId = this.id;
		$('#hiddenId').val(projectId);
		

	});

function validate(response){
	var projectDetailStatus = response.data.checkStatus;
	var statusList = [91,92,93,94,95,96];
	var flag = true;
	projectDetailStatus = projectDetailStatus.sort();
		console.log("df : "+projectDetailStatus)
		for(i=0;i<statusList.length;i++){
			
			if(statusList[i]!=projectDetailStatus[i]){
				$("#step_id").text(i+1);
				$("#warningModal").modal('show');
				flag = false;
				break;
			}
		}
		console.log(flag);
		
		return flag;
}



function setQuery(response,reportId,queryType){
var projectId = $("#id").val();
$("#modal-span").text(reportId);
$("#projectId").val(projectId);
$("#queryType").val(queryType);


switch(queryType) {
case "totalAdmits":
  $("#query-type").text("Total Admits");
break;
case "occupancy":
  $("#query-type").text("Occupancy");
  break;
break;
case "atp":
  $("#query-type").text("ATP");
break;
case "sph":
  $("#query-type").text("SPH");
break;
case "adRevenue":
  $("#query-type").text("Ad Revenue(INR)");
break;
case "personalExpense":
  $("#query-type").text("Personal Expense");
break;
case "electricityWaterExpanse":
  $("#query-type").text("Electricity Water Expanse");
break;
case "totalRmExpenses":
  $("#query-type").text("Total R&M Expenses");
break;

case "totalOverheadExpenses":
  $("#query-type").text("Total Overhead Expenses");
break; 

case "totalProjectCost":
  $("#query-type").text("Total Project Cost");
break; 


default:
  $("#query-type").text(" ");
} 

createChatBox(queryType,response);

}


$("#submitQuery").click(function(){
	
	if($('#msg').val()=="")
		return;
	
	var queryType = $("#queryType").val();
	var projectId = $("#projectId").val();
	var msg = $("#msg").val();

	var data=$('#modal-form').serialize();
	
	$.ajax({
		url :"/project/save-query",
		type:"POST",
		data: data,
		success : function(response) {
			showMsg(response)
			$('#'+queryType+'Button').attr('class','float-right greenBtnQuery query-modal');
			$('#'+queryType+'Img').attr('src','../../images/queryBtnReply-green.png');
		},
		error:function(xhr,status,error){
			console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
			}
	}); 
	$(".msg_card_body").css("overflow","auto");
	
});

function createChatBox(queryType,projectResponse){
	
	var divToAppend="";
	$('.msg_card_body').empty()
	var response=projectResponse.data.finalSummary;
	//console.log(queryType+" :queryType: , response: "+response);
	//console.log(response);
	var currentuser=$("#currentUserId").val();
	var currentuserId=currentuser.replace(/"/g, '');
	var messageLength=0;
	if(response[queryType]["query"]!=null)
	{
		messageLength=response[queryType]["query"].length;
	}
	
	$.each(response[queryType]["query"], function(key,value) {

		if(currentuserId == value["userId"]){
			
			divToAppend=divToAppend+'<div class="d-flex justify-content-end mb-3"> '+
			' <div class="msg_cotainer_send"> '+
			value["msg"]+' <span '+
			' 	class="msg_time_send">'+value["date_time"]+'</span> '+
			' </div> '+
			' <div class="img_cont_msg"> '+
			' 	<img src="'+httpPath+'/images/clientIcon_chatbox.png" '+
			' class="rounded-circle user_img_msg"> '+
				 ' <p>'+value["userFullName"]+'</p> '+
				/* ' <p>BD</p> '+ */
				' </div> '+
		' </div>';
		}
		else{
			divToAppend=divToAppend+'<div class="d-flex justify-content-start mb-3">'+
			' <div class="img_cont_msg">'+
			' <img src="'+httpPath+'/images/userIcon_chatbox.png"'+
			' 	class="rounded-circle user_img_msg">'+
			 ' <p>'+value["userFullName"]+'</p>'+ 
			/* ' <p>AP</p>'+ */
			' </div>'+
			' <div class="msg_cotainer">'+value["msg"]+' <span class="msg_time">'+value["date_time"]+'</span>'+
			' </div>'+
			' </div>';
			
		}
		
	});
	if(messageLength<=3)
	{
		$('#viewOtherComments').hide();
	}
	else
	{
		$('#viewOtherComments').show();
	}
	
	
	$('.msg_card_body').append(divToAppend);
	$(".msg_card_body").css("overflow","hidden");
	
}

function showMsg(response){
	autoScroll();
	
	var divToAppend='<div class="d-flex justify-content-end mb-3"> '+
	' <div class="msg_cotainer_send"> '+
	response["msg"]+' <span '+
	' 	class="msg_time_send">'+response["date_time"]+'</span> '+
	' </div> '+
	' <div class="img_cont_msg"> '+
	' 	<img src="'+httpPath+'/images/clientIcon_chatbox.png" '+
	' class="rounded-circle user_img_msg"> '+
		 ' <p>'+response["userFullName"]+'</p> '+
		/* ' <p>BD</p> '+ */
		' </div> '+
' </div>';

	$('.msg_card_body').append(divToAppend);
	$('#msg').val("");
	
	
}

$("#viewOtherComments").click(function(){
	$(".msg_card_body").css("overflow","auto");
});

function autoScroll() {
	setTimeout(function(){
	$('.chat .msg_card_body').animate({
	scrollTop: $('.chat .msg_card_body').get(0).scrollHeight}, 0); 
	},300);	
	}

function generateTable(response) {
	//alert("DAfdf")
	console.log("console.log(response); : "+" response"+response,JSON.stringify(response));
}


$("#update").click(function(){	
	 var projectId = $("#id").val();
		$.ajax({
			url :"/project/update/"+ projectId,
			success : function(response) {
				console.log(response);
				window.location.href="/project/developmentDetails/"+projectId;
			},
			error:function(xhr,status,error){
				console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
				}
		}); 
})

$("#saveExecutiveNote").click(function(){	
     var data=$('#step-7').serialize();
	 var projectId = $("#id").val();
		$.ajax({
			url :"/project/save-executive-note",
			type:"POST",
			data: data,
			success : function(response) {
				alert("Executive Note has been saved successfully.")
				window.location.reload();
			},
			error:function(xhr,status,error){
			  console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
			}
		}); 
})

$('#downloadFullReportXcl').click(function(){
	
	form = $('#step-7');
	form.attr('action', '/report/XcelFile/').trigger('submit');
});


