$(document).ready(function()
	{
		
		$("#chngstatus").on('click',function()
				
		{
			
			var reportIds=[];
			
			$("input[name='rptName']:checked").each(function()
			{
				reportIds.push($(this).val());
			});
			//$("#reportIds").val(reportIds);
			//var form = $('#opsReportsform');
			//form.submit();
			$.ajax({
	        	 
        	    /*type: 'POST',
        	    data:'projectId='+reportIds,
        	    url: "/admin/ops-feasibility-mis/",
        	    
        	    success: function(){
        	    	alert("BD Assigned Successfully");
        	    	
        	    	location.reload();
        	    },
        	    error: function(){
        	    	alert("error while assigning report");
        	    	
        	    	location.reload();
            }*/

	        	 
        	    type: 'POST',
        	    url: "/admin/opsFeasibilityMis/",
        	    data: { 
 				    "ids": reportIds 
        	    },
        	    success : function(response) {
        			console.log('-----------', response);
        			
        			$("#data-table").empty();
        			
        			var myTable= $("#data-table").DataTable( {
        				bDestroy:true,
        				fnDestroy:true
        	       
        			});
        			for(let i=0;i<response.length;i++){
        				let data1=response[i];
        				myTable.row.add(['<a href="@{/project/getProjectId/}+${data1.id}" th:if="${data1.reportId != null }"'
        	               +' th:text="${data1.reportId}">' + data1.reportId + '</a>',data1.projectName,"Pre-Handover","Accepted",
        					'<span th:text="${#dates.format(data1.createdDate, dd-MMM-yyyy)}"','<input type="checkbox" th:value="${data1.reportIdVersion}" name="rptName">'	]);
        				myTable.draw();
        			}
        			
        			return false;
        		},
        		error:function(xhr,status,error){
        			console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
        			
        			}
        	
        	});
		});
		
	});

