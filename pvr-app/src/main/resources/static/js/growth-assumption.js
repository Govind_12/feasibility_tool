$( document ).ready(function() {
	$("#adminLeasePeriod").on('keyup', function(key) {
		
		/*if (key.keyCode != 8 && key.keyCode != 0 && (key.keyCode < 48 || key.keyCode > 57)){
			$(this).val($(this).val().replace(/\D/g, ''));
				return;
		}*/
		
		var leasePeriod=$("#adminLeasePeriod").val();
		var fixedConstant=$("#hiddenFixedConstants").val();
		fixedConstant = JSON.parse(fixedConstant);
		
		if(leasePeriod != ''){
			generateRevenueTable(leasePeriod, fixedConstant);
			generateCostTable(leasePeriod, fixedConstant);
		}
		
		
	});
	
	/*
	$("#submitBtnGrowth").on('click', function() {
		var intRegex = /^\d+$/;
		if (! intRegex.test($("#adminLeasePeriod").val())){
			$("#adminLeasePeriod").focus();
			//$("#adminLeasePeriod").addClass('error');
			return false;
		}
		
	});*/
	
    	
    	getFixedConstants();
    	
    	
    	$("#revenueAssumtionTable tbody td").keyup(function(e) {
            var rowIndex = $(this).parent().index('#revenueAssumtionTable tbody tr');
            var tdIndex = $(this).index('#revenueAssumtionTable tbody tr:eq(' + rowIndex + ') td');
            var cellValue = $(this).closest('td').find("input").val();
            var currentClass =  $(this).closest('td').find("input").attr("class");
            console.log(cellValue)
            for(i=(tdIndex+1);i<=99;i++){
            	
            	if(currentClass.includes('admit_growth')){
            		$('#admit_growth'+i).val(cellValue);
            	}
            	if(currentClass.includes('atp_growth')){
            		$('#atp_growth'+i).val(cellValue);
            	}
            	if(currentClass.includes('sph_growth')){
            		$('#sph_growth'+i).val(cellValue);
            	}
            	if(currentClass.includes('sig_growth')){
            		$('#sig_growth'+i).val(cellValue);
            	}
            	if(currentClass.includes('cfg_growth')){
            		$('#cfg_growth'+i).val(cellValue);
            	}
            	if(currentClass.includes('other_growth')){
            		$('#other_growth'+i).val(cellValue);
            	}
    		}
        });
    	
    	
    	$("#revenueAssumtionTable tbody td").focusout(function(e) {
    		  var rowIndex = $(this).parent().index('#revenueAssumtionTable tbody tr');
              var tdIndex = $(this).index('#revenueAssumtionTable tbody tr:eq(' + rowIndex + ') td');
              var cellValue = $(this).closest('td').find("input").val();
              var currentClass =  $(this).closest('td').find("input").attr("class");
              console.log(cellValue)
              if(cellValue==''){
            	  cellValue = 0;
                  for(i=(tdIndex+1);i<=99;i++){
                  	
                  	if(currentClass.includes('admit_growth')){
                  		$('#admit_growth'+i).val(cellValue);
                  	}
                  	if(currentClass.includes('atp_growth')){
                  		$('#atp_growth'+i).val(cellValue);
                  	}
                  	if(currentClass.includes('sph_growth')){
                  		$('#sph_growth'+i).val(cellValue);
                  	}
                  	if(currentClass.includes('sig_growth')){
                  		$('#sig_growth'+i).val(cellValue);
                  	}
                  	if(currentClass.includes('cfg_growth')){
                  		$('#cfg_growth'+i).val(cellValue);
                  	}
                  	if(currentClass.includes('other_growth')){
                  		$('#other_growth'+i).val(cellValue);
                  	}
          		}
              }
            	  
    	});
    	
    	$("#costAssumtionTable tbody td").keyup(function(e) {
            var rowIndex = $(this).parent().index('#costAssumtionTable tbody tr');
            var tdIndex = $(this).index('#costAssumtionTable tbody tr:eq(' + rowIndex + ') td');
            var cellValue = $(this).closest('td').find("input").val();
            var currentClass =  $(this).closest('td').find("input").attr("id");
            console.log(cellValue)
            for(i=(tdIndex+1);i<=99;i++){
            	
            	if(currentClass.includes('per_exp')){
              		$('#per_exp'+i).val(cellValue);
              	}
              	if(currentClass.includes('elec_exp')){
              		$('#elec_exp'+i).val(cellValue);
              	}
              	if(currentClass.includes('rm_exp')){
              		$('#rm_exp'+i).val(cellValue);
              	}
              	if(currentClass.includes('other_exp')){
              		$('#other_exp'+i).val(cellValue);
              	}
              	if(currentClass.includes('prop_exp')){
              		$('#prop_exp'+i).val(cellValue);
              	}
    		}
        });
    	
    	
    	$("#costAssumtionTable tbody td").focusout(function(e) {
    		  var rowIndex = $(this).parent().index('#costAssumtionTable tbody tr');
              var tdIndex = $(this).index('#costAssumtionTable tbody tr:eq(' + rowIndex + ') td');
              var cellValue = $(this).closest('td').find("input").val();
              var currentClass =  $(this).closest('td').find("input").attr("id");
              console.log(cellValue)
              if(cellValue==''){
            	  cellValue = 0;
                  for(i=(tdIndex+1);i<=99;i++){
                  	
                  	if(currentClass.includes('per_exp')){
                  		$('#per_exp'+i).val(cellValue);
                  	}
                  	if(currentClass.includes('elec_exp')){
                  		$('#elec_exp'+i).val(cellValue);
                  	}
                  	if(currentClass.includes('rm_exp')){
                  		$('#rm_exp'+i).val(cellValue);
                  	}
                  	if(currentClass.includes('other_exp')){
                  		$('#other_exp'+i).val(cellValue);
                  	}
                  	if(currentClass.includes('prop_exp')){
                  		$('#prop_exp'+i).val(cellValue);
                  	}
          		}
              }
            	  
    	});

});


var getFixedConstants=function(){
	
	$.ajax({
	
	url : "assumption/getFixedConstants",
	async: false,
	success : function(result) {
		console.log(result);
		$('#adminLeasePeriod').val(result.adminLeasePeriod);
		
		generateRevenueTable(result.adminLeasePeriod, result);
		generateCostTable(result.adminLeasePeriod, result);
		
		
	},
	error : function(error,xhr, a) {
		console.log('Error in getFixedConstants() ajax request: '+error+' '+xhr+' '+a);
	}
	
	});
	
}

var generateRevenueTable=function(noOfColumns, fixedConstants){
	
	var value=0;
	
	myTable = $('#revenueAssumtionTable');
	
	$('#theadRevenueAssumtionTable').find('tr').remove();
	$('#tbodyRevenueAssumtionTable').find('tr').remove();
	
	$('#theadRevenueAssumtionTable').append('<tr><th class="fixed-side titleColumn">Revenue Assumptions</th></tr>');
	$('#tbodyRevenueAssumtionTable').append('<tr><th class="fixed-side">Admit Growth</th></tr>');
	$('#tbodyRevenueAssumtionTable').append('<tr><th class="fixed-side ">ATP Growth</th></tr>');
	$('#tbodyRevenueAssumtionTable').append('<tr><th class="fixed-side ">SPH Growth</th></tr>');
	$('#tbodyRevenueAssumtionTable').append('<tr><th class="fixed-side ">Sponsorship Income Growth</th></tr>');
	$('#tbodyRevenueAssumtionTable').append('<tr><th class="fixed-side ">Convenience fee growth</th></tr>');
	$('#tbodyRevenueAssumtionTable').append('<tr><th class="fixed-side ">Other income growth</th></tr>');
	
	
	if(fixedConstants!=null)
		//decrease 1 column for first year in iteration
	for(var i=1;i<=(noOfColumns-1);i++){
		var count=1;
		$('#revenueAssumtionTable tr').append($("<td>"));
		$('#revenueAssumtionTable thead tr>td:last').html("Year "+(i+1));
		$('#revenueAssumtionTable tbody tr').each(function(){
			switch(count){
			
			case 1:
				$(this).children('td:last').append($('<input type="text" class="assumption-input admit_growth" id="admit_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.admitsGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.admitsGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.admitsGrowth[i+1]: 0)  +' " />'))
				count++;
				break;
			case 2:
				$(this).children('td:last').append($('<input type="text" class="assumption-input atp_growth" id="atp_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.atpGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.atpGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.atpGrowth[i+1] : 0)+'" />'))
				count++;
				break;
			case 3:
				$(this).children('td:last').append($('<input type="text" class="assumption-input sph_growth" id="sph_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.sphGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.sphGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.sphGrowth[i+1] : 0)+'" />'))
				count++;
				break;
			case 4:
				$(this).children('td:last').append($('<input type="text" class="assumption-input sig_growth" id="sig_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth[i+1] :0)+'" />'))
				count++;
				break;
			case 5:
				$(this).children('td:last').append($('<input type="text" class="assumption-input cfg_growth" id="cfg_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.convenienceFeeGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.convenienceFeeGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.convenienceFeeGrowth[i+1] : 0)+'" />'))
				count++;
				break;
			case 6:
				$(this).children('td:last').append($('<input type="text" class="assumption-input other_growth" id="other_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.otherIncomeGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.otherIncomeGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.otherIncomeGrowth[i+1] : 0)+'" />'))
				count++;
			}
			
		});
		}
	
	else{
		
		for(var i=1;i<=noOfColumns-1;i++){
			$('#revenueAssumtionTable tr').append($("<td>"));
			$('#revenueAssumtionTable thead tr>td:last').html("Year "+(i+1));
			$('#revenueAssumtionTable tbody tr').each(function(){
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" value="0" />'))
			});
			
			}
	}
	
}


var generateCostTable=function(noOfColumns, fixedConstants){
	
	var value=0;
	
	myTable = $('#costAssumtionTable');
	
	$('#theadCostAssumtionTable').find('tr').remove();
	$('#tbodyCostAssumtionTable').find('tr').remove();
	
	$('#theadCostAssumtionTable').append('<tr><th class="fixed-side titleColumn">Cost Assumptions</th></tr>');
	$('#tbodyCostAssumtionTable').append('<tr><th class="fixed-side">Personnel Expenses</th></tr>');
	$('#tbodyCostAssumtionTable').append('<tr><th class="fixed-side ">Electricity & Air-conditioning</th></tr>');
	$('#tbodyCostAssumtionTable').append('<tr><th class="fixed-side ">Repair and Maintenance </th></tr>');
	$('#tbodyCostAssumtionTable').append('<tr><th class="fixed-side "> Other Overhead expenses</th></tr>');
	$('#tbodyCostAssumtionTable').append('<tr><th class="fixed-side "> Property tax</th></tr>');
	
	
	if(fixedConstants!=null)
		//decrease 1 column for first year in iteration
	for(var i=1;i<=(noOfColumns-1);i++){
		var count=1;
		$('#costAssumtionTable tr').append($("<td>"));
		$('#costAssumtionTable thead tr>td:last').html("Year "+(i+1));
		$('#costAssumtionTable tbody tr').each(function(){
			switch(count){
			
			case 1:
				$(this).children('td:last').append($('<input type="text" class="assumption-input" id="per_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.personnelExpenses['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.personnelExpenses[i+1]) ? fixedConstants.costAssumptionsGrowthRate.personnelExpenses[i+1] : 0)+'" />'))
				count++;
				break;
			case 2:
				$(this).children('td:last').append($('<input type="text" class="assumption-input" id="elec_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.electricityAndAirconditioning['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.electricityAndAirconditioning[i+1]) ? fixedConstants.costAssumptionsGrowthRate.electricityAndAirconditioning[i+1] : 0)+'" />'))
				count++;
				break;
			case 3:
				$(this).children('td:last').append($('<input type="text" class="assumption-input"id="rm_exp'+(i)+'"  maxlength="5" name="costAssumptionsGrowthRate.repairAndMaintenance['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.repairAndMaintenance[i+1]) ? fixedConstants.costAssumptionsGrowthRate.repairAndMaintenance[i+1] : 0)+'" />'))
				count++;
				break;
			case 4:
				$(this).children('td:last').append($('<input type="text" class="assumption-input" id="other_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.otherOverheadExpenses['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.otherOverheadExpenses[i+1]) ? fixedConstants.costAssumptionsGrowthRate.otherOverheadExpenses[i+1] : 0)+'" />'))
				count++;
				break;
			case 5:
				$(this).children('td:last').append($('<input type="text" class="assumption-input" id="prop_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.propertyTax['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.propertyTax[i+1]) ? fixedConstants.costAssumptionsGrowthRate.propertyTax[i+1] : 0)+'" />'))
				count++;
				
			}
			
		});
		}
	
	else{
		
		for(var i=1;i<=(noOfColumns-1);i++){
			$('#costAssumtionTable tr').append($("<td>"));
			$('#costAssumtionTable thead tr>td:last').html("Year "+(i+1));
			$('#costAssumtionTable tbody tr').each(function(){
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" value="0" />'))
			});
		}
	}
}


$(function() {
	  $('.assumption-input').on('input', function() {
	    match = (/(\d{0,9})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
	    this.value = match[1] + match[2];
	  });
	});

