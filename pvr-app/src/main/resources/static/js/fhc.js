 $("#tblData").on('click','.editFhc',function(){
         // get the current row
         var currentRow=$(this).closest("tr"); 
         
         var col1=currentRow.find("td:eq(0)").text(); // get current row 1st TD value
         var col2=currentRow.find("input").val(); // get current row 2nd TD
         
         $('#edit_fhc_name').val(col1);
         $('#edit_fhc_value').val(col2);
         
         $('#editModal').modal();
        
    });
	$(function() {
		  $('.decimalLimit').on('input', function() {
		    match = (/(\d{0,9})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
		    this.value = match[1] + match[2];
		  });
		});
	
 
function validate() {

	var name = $('#fhc_name').val();
	var value = $('#fhc_value').val();

	$('#nameError').text("");
	$('#valueError').text("");

	if (name.trim() == '') {
		$('#nameError').text("FHC name cannot be empty.");
		$( "#fhc_name" ).focus();
		return false;
	}

	else if (value.trim() == '') {
		$('#valueError').text("Value cannot be empty.");
		$( "#fhc_value" ).focus();
		return false;
	}
	else{
		
		$.ajax({
			url :"/admin/is-fhc-present/"+name,
			type:"GET",
			async: false,
			success : function(response) {
				console.log(response);
				if(!response){
					console.log(response);
					return true;
				}
				else{
					$('#nameError').text(name+" already exists.");
					return false;
				}
			},
			error:function(xhr,status,error){
				console.log('Error ' +JSON.stringify(status)+ "Error : " + error +" "+JSON.stringify(xhr))
				}
		});
		
		
		console.log(response);
	}
	return false;
}

function validateEdit() {
	var value = $('#fhc_value').val();

	$('#valueError').text("");
	if (value.trim() == '') {
		$('#valueError').text("Value cannot be empty.");
		$( "#fhc_value" ).focus();
		return false;
	}else{
		return true;
	}
}

$(function() {

	var table = $("#tblData").dataTable({
		"aLengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
		"autoWidth": false,
		"order" : [],
		"deferRender" : true,
		"columnDefs" : [ {
			"targets" : 2,
			"orderable" : false,
		} ]

	});

});


$("#saveScreenType").on('click', function(){
	if($('#cinemaFormatName').val() != "" && $('#cinemaFormatName').val() != undefined){
		var cinemaFormatMaster = {};
		cinemaFormatMaster.cinemaCategory = $('#cinemaFormatName').val();
		cinemaFormatMaster.cinemaSubCategory = $('#cinemaFormatSubCategory').val() !== undefined ? $('#cinemaFormatSubCategory').val().split(",") : [];
		cinemaFormatMaster.cinemaFormatMapping = $('#cinemaFormatMapping').val() !== undefined ? $('#cinemaFormatMapping').val().split(",") : [];
		cinemaFormatMaster.fhcValue = $('#filmHireCost').val() == "" ? 0 : $('#filmHireCost').val();
		
		console.log(cinemaFormatMaster);
		
		$.ajax({
			url :"/admin/save/screen/type",
			type:"POST",
			async: true,
			data: JSON.stringify(cinemaFormatMaster),
			contentType: "application/json",
			success : function(response) {
				window.location.reload();
			},
			error:function(xhr,status,error){
			  console.log('Error ' +JSON.stringify(status)+ "Error : " + error +" "+JSON.stringify(xhr))
			}
		});
	}else{
		window.alert("Cinema format name must not be empty.");
	}

	
})






