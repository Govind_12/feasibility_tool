
$('#web-sales').click(function() {
	 $('.loading').show();
	 $('#web-sales-table').empty();
	var projectID =$('#id').attr('value');
	 $.ajax({
         url: "/view-modal/getWebSales/"+projectID,
         type: 'GET',
         success: function(response) {
        	 //console.log(response)
        	 var inputValues = "";
        		var comments = "";
        		if(response.projectDetails.inputValues != null)
        			inputValues = response.projectDetails.inputValues;
        		if(response.projectDetails.comments != null)
        			comments = response.projectDetails.comments;
        		
        		 var content = '<h3>BENCHMARK CINEMAS</h3><table class="table table-bordered transparent fixed-layout">' +
	       	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px">Particulars</th>' +
								'<th colspan="3" class="p-m-0">' +
								'	<table class="p-m-0 border-hide">' +
								'		<thead>' +
								'			<tr>' +
								'				<th  colspan="3" class="text-center text-middle">PVR Properties</th>' +
								'			</tr>' +
								'			<tr>' +
								'				<th width="155" class="text-center">'+nullConversion(response.websaleBenchmark.benchmarkCinemas[0].cinema_name)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.websaleBenchmark.benchmarkCinemas[1].cinema_name)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.websaleBenchmark.benchmarkCinemas[2].cinema_name)+'</th>' +
								'			</tr>' +
								'		</thead>'+
								'	</table>'+
								'</th>'+
							'</tr>'+
						'</thead>' +
						'	<tbody>' +
						'	<tr>' +
						'		<td class="text-left">Screens (nos)</td>' +
						'		<td width="155">'+nullConversion(response.websaleBenchmark.benchmarkCinemas[0].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.websaleBenchmark.benchmarkCinemas[1].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.websaleBenchmark.benchmarkCinemas[2].noOfScreen)+'</td>' +
						'	</tr>' +
					/*	'	<tr>' +
						'		<td class="text-left">Seats (nos)</td>' +
						'		<td width="155" >'+response.websaleBenchmark.benchmarkCinemas[0].seatCapacity+'</td>' +
						'		<td width="155">'+response.websaleBenchmark.benchmarkCinemas[1].seatCapacity+'</td>' +
						'		<td width="155">'+response.websaleBenchmark.benchmarkCinemas[2].seatCapacity+'</td>' +
						'	</tr>' +*/
						'		<td class="text-left">Web sale (% of total sales)</td>' +
						'		<td width="155" >'+formatCurrenry(response.websaleBenchmark.benchmarkCinemas[0].webSale)+'</td>' +
						'		<td width="155">'+formatCurrenry(response.websaleBenchmark.benchmarkCinemas[1].webSale)+'</td>' +
						'		<td width="155">'+formatCurrenry(response.websaleBenchmark.benchmarkCinemas[2].webSale)+'</td>' +
						'	</tr>' +
						'</tbody></table>	'+
							'<h3>PROPOSED VALUES</h3>'+
							 '<table class="table table-bordered transparent fixed-layout">' +
			        	 		'<thead>' +
									'<tr>' +
										'<th class="text-center text-middle" style="200px"></th>' +
										'<th class="text-center text-middle" style="200px">System generated value</th>' +
										/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
										'<th class="text-center text-middle" style="200px">Comments</th>' +*/
									'</tr>' +
								'</thead>' +
								'	<tbody>'+
							       	'	<tr>' +
									'		<td class="text-left">Web sales (% of total sales)</td>' +
									'		<td width="155">'+response.projectDetails.costType+'</td>' +
									/*'		<td width="155">'+inputValues+'</td>' +
									'		<td width="155">'+comments+'</td>' +*/
									'	</tr>' ;
    						
    	       		content+='</tbody></table>';
        		$('#web-sales-table').append(content);
        		$('.loading').hide();
        		 $('#webSalesModal').modal();

         },
         error: function(xhr, status, error) {
        	 $('.loading').hide();
        	 console.log(xhr+"  "+ status+"  "+ error);
         }
     });
		
	
});



$('#ad-revenue').click(function() {
	 $('#ad-revenue-table').empty();
		var projectID =$('#id').attr('value');
		$('.loading').show();
		 $.ajax({
	        url: "/view-modal/getAdRevenue/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	console.log(response)
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	 var content = '<h3>BENCHMARK CINEMAS</h3><table class="table table-bordered transparent fixed-layout">' +
	       	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px">Particulars</th>' +
								'<th colspan="3" class="p-m-0">' +
								'	<table class="p-m-0 border-hide">' +
								'		<thead>' +
								'			<tr>' +
								'				<th  colspan="3" class="text-center text-middle">PVR Properties</th>' +
								'			</tr>' +
								'			<tr>' +
								'				<th width="155" class="text-center">'+nullConversion(response.adrevenue.benchmarkCinemas[0].cinema_name)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.adrevenue.benchmarkCinemas[1].cinema_name)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.adrevenue.benchmarkCinemas[2].cinema_name)+'</th>' +
								'			</tr>' +
								'		</thead>' +
								'	</table>' +
								'</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>' +
						'	<tr>' +
						'		<td class="text-left">Screens (nos)</td>' +
						'		<td width="155">'+nullConversion(response.adrevenue.benchmarkCinemas[0].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.adrevenue.benchmarkCinemas[1].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.adrevenue.benchmarkCinemas[2].noOfScreen)+'</td>' +
						'	</tr>' +
					/*	'	<tr>' +
						'		<td class="text-left">Seats (nos)</td>' +
						'		<td width="155" >'+response.adrevenue.benchmarkCinemas[0].seatCapacity+'</td>' +
						'		<td width="155">'+response.adrevenue.benchmarkCinemas[1].seatCapacity+'</td>' +
						'		<td width="155">'+response.adrevenue.benchmarkCinemas[2].seatCapacity+'</td>' +
						'	</tr>' +*/
						'	<tr>' +
						'		<td class="text-left">Ad Revenue (INR)</td>' +
						'		<td width="155" >'+formatData(response.adrevenue.benchmarkCinemas[0].adRevenue)+'</td>' +
						'		<td width="155">'+formatData(response.adrevenue.benchmarkCinemas[1].adRevenue)+'</td>' +
						'		<td width="155">'+formatData(response.adrevenue.benchmarkCinemas[2].adRevenue)+'</td>' +
						'	</tr>' +
						'</tbody></table>	'+
							'<h3>PROPOSED VALUES</h3>'+
							 '<table class="table table-bordered transparent fixed-layout">' +
			        	 		'<thead>' +
									'<tr>' +
										'<th class="text-center text-middle" style="200px"></th>' +
										'<th class="text-center text-middle" style="200px">System generated value</th>' +
										/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
										'<th class="text-center text-middle" style="200px">Comments</th>' +*/
									'</tr>' +
								'</thead>' +
								'	<tbody>'+
							       	'	<tr>' +
									'		<td class="text-left">Ad Revenue (INR)</td>' +
									'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
								/*	'		<td width="155">'+inputValues+'</td>' +
									'		<td width="155">'+comments+'</td>' +*/
									'	</tr>' ;
								
	       	 content+='</tbody></table>';
	       		$('#ad-revenue-table').append(content);
	       		$('.loading').hide();
	       		$('#AdRevenueModal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});

$('#personnel-expenses').click(function() {
	 $('#personnel-expenses-table').empty();
		var projectID =$('#id').attr('value');
		$('.loading').show();
		 $.ajax({
	        url: "/view-modal/getPersonalExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	console.log(response)
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.personalExpense.inputValues != null)
	        			inputValues = response.personalExpense.inputValues;
	        		if(response.personalExpense.comments != null)
	        			comments = response.personalExpense.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Personnel Expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
	       		if(response.personalExpenseBenchmark.length>0){
	       			for(var i=0;i<response.personalExpenseBenchmark.length;i++){
			        	  content+='<tr>' +
							'		<td width="155">'+nullConversion(response.personalExpenseBenchmark[i].cinemaName)+'</td>' +
							'		<td width="155">'+formatData(response.personalExpenseBenchmark[i].navCode)+'</td>' +
							'		<td width="155">'+nullConversion(response.personalExpenseBenchmark[i].noOfScreen)+'</td>' +
							'	</tr>';
			        }
	       		}
	       		else{
	       		 content+='<tr>' +
					'		<td class="text-center text-middle" width="155">NA</td>' +
					'		<td class="text-center text-middle" width="155">NA</td>' +
					'		<td class="text-center text-middle" width="155">NA</td>' +
					'	</tr>';
	       		}
			      
			      content+='</tbody></table>'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
								/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Personnel Expense (INR)</td>' +
							'		<td width="155">'+formatData(response.personalExpense.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#personnel-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#personnel-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});


$('#electricity-water').click(function() {
	 $('#electricity-water-table').empty();
		var projectID =$('#id').attr('value');
		$('.loading').show();
		 $.ajax({
	        url: "/view-modal/getElectricityExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Electricity & water Expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.electicityAndWater.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.electicityAndWater[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.electicityAndWater[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.electicityAndWater[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
								/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Electricity & water Expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
						/*	'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#electricity-water-table').append(content);
	       		$('.loading').hide();
	       		$('#electricity-water-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});


$('#rm-expenses').click(function() {
	 $('#rm-expenses-table').empty();
		var projectID =$('#id').attr('value');
		$('.loading').show();
		 $.ajax({
	        url: "/view-modal/getRmExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Total R&M Expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.repairAndMaintaince.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.repairAndMaintaince[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.repairAndMaintaince[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.repairAndMaintaince[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
								/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Total R&M Expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#rm-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#rm-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});



$('#housekeeping-expense').click(function() {
	 $('#housekeeping-expenses-table').empty();
	 $('.loading').show();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getHousekeeping/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Housekeeping expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.housekeepingBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+response.housekeepingBenchmark[i].cinemaName+'</td>' +
						'		<td width="155">'+formatData(response.housekeepingBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+response.housekeepingBenchmark[i].noOfScreen+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
							/*	'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Housekeeping expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#housekeeping-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#housekeeping-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});


$('#security-expense').click(function() {
	 $('#security-expenses-table').empty();
		var projectID =$('#id').attr('value');
		$('.loading').show();
		 $.ajax({
	        url: "/view-modal/getSecurityExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Security expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.securityBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.securityBenchmark[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.securityBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.securityBenchmark[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
							/*	'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Security expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#security-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#security-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});


$('#communication-expense').click(function() {
	 $('#communication-expenses-table').empty();
	 $('.loading').show();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getCommunicationExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	console.log(response)
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Communication expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.communicationBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.communicationBenchmark[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.communicationBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.communicationBenchmark[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
							/*	'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Communication expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#communication-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#communication-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});


$('#travelling-expense').click(function() {
	 $('#travelling-expenses-table').empty();
	 $('.loading').show();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getTravellingExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Travelling and conveyance expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.travellingAndConyenceBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.travellingAndConyenceBenchmark[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.travellingAndConyenceBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.travellingAndConyenceBenchmark[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
							/*	'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Travelling and conveyance expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#travelling-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#travelling-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});


$('#legal-fee').click(function() {
	 $('#legafee-expenses-table').empty();
	 $('.loading').show();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getLeegalfee/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Legal Fee (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.legalBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.legalBenchmark[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.legalBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.legalBenchmark[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
								/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Legal Fee (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#legafee-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#legafee-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});



$('#insurance-expense').click(function() {
	$('.loading').show();
	 $('#insurance-expenses-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getInsuranceExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Insurance expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.insuranceBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.insuranceBenchmark[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.insuranceBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.insuranceBenchmark[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
								/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Insurance expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#insurance-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#insurance-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});



$('#internet-expenses').click(function() {
	$('.loading').show();
	 $('#internet-expenses-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getInternetExpenses/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Internet expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.internetBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.internetBenchmark[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.internetBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.internetBenchmark[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
								/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Internet expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#internet-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#internet-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});



$('#printing-stationary').click(function() {
	$('.loading').show();
	 $('#printing-expenses-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getPrintingStationary/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Printing and stationary expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.printingAndStationaryBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.printingAndStationaryBenchmark[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.printingAndStationaryBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.printingAndStationaryBenchmark[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
							/*	'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Printing and stationary expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#printing-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#printing-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});



$('#marketing-expense').click(function() {
	$('.loading').show();
	 $('#marketing-expenses-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getMarketingExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Marketing expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.marketingBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.marketingBenchmark[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.marketingBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.marketingBenchmark[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
							/*	'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Marketing expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#marketing-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#marketing-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});



$('#miscellaneous-expenses').click(function() {
	$('.loading').show();
	 $('#miscellaneous-expenses-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getMiscellaneousExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	        	 var inputValues = "";
	        		var comments = "";
	        		if(response.projectDetails.inputValues != null)
	        			inputValues = response.projectDetails.inputValues;
	        		if(response.projectDetails.comments != null)
	        			comments = response.projectDetails.comments;
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Miscellaneous expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.miscellaneousBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.miscellaneousBenchmark[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.miscellaneousBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.miscellaneousBenchmark[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h3>PROPOSED VALUES</h3>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
							/*	'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +*/
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Miscellaneous expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							/*'		<td width="155">'+inputValues+'</td>' +
							'		<td width="155">'+comments+'</td>' +*/
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#miscellaneous-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#miscellaneous-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});

$('#occupancy-revenue').click(function() {
	$('.loading').show();
	 $('#occupancy-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getTotalAdmits/"+projectID,
	        type: 'GET',
	        success: function(response) {  
	        	console.log(response)
	        	let compOccupancy1='';
	        	let compOccupancy2='';
	        	let compOccupancy3='';
	        	if(response.compitionMaster[0].occupancy === null){
	        		compOccupancy1='NA';
	        	}else{
	        		compOccupancy1='NA';
	        	}
	        	if(response.compitionMaster[1].occupancy === null){
	        		compOccupancy2='NA';
	        	}else{
	        		compOccupancy2='NA';
	        	}
	        	if(response.compitionMaster[2].occupancy === null){
	        		compOccupancy3='NA';
	        	}else{
	        		compOccupancy3='NA';
	        	}
	           	
	          	 var content = '<h5>BENCHMARK CINEMAS</h5><table class="table table-bordered transparent fixed-layout">' +
	          	 		'<thead>' +
	   						'<tr>' +
	   							'<th class="text-center text-middle" style="200px">Particulars</th>' +
	   							'<th colspan="3" class="p-m-0">' +
	   							'	<table class="p-m-0 border-hide">' +
	   							'		<thead>' +
	   							'			<tr>' +
	   							'				<th  colspan="3" class="text-center text-middle">PVR Properties</th>' +
	   							'			</tr>' +
	   							'			<tr>' +
	   							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[0].cinema_name)+'</th>' +
	   							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[1].cinema_name)+'</th>' +
	   							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[2].cinema_name)+'</th>' +
	   							'			</tr>' +
	   							'		</thead>' +
	   							'	</table>' +
	   							'</th>' +
	   						'</tr>' +
	   					'</thead>' +
	   					'	<tbody>' +
	   					'	<tr>' +
	   					'		<td class="text-left">Screens (nos)</td>' +
	   					'		<td width="155">'+nullConversion(response.occupancy[0].noOfScreen)+'</td>' +
	   					'		<td width="155">'+nullConversion(response.occupancy[1].noOfScreen)+'</td>' +
	   					'		<td width="155">'+nullConversion(response.occupancy[2].noOfScreen)+'</td>' +
	   					'	</tr>' +
	   					/*'	<tr>' +
	   					'		<td class="text-left">Seats (nos)</td>' +
	   					'		<td width="155" >'+response.occupancy[0].seatCapacity+'</td>' +
	   					'		<td width="155">'+response.occupancy[1].seatCapacity+'</td>' +
	   					'		<td width="155">'+response.occupancy[2].seatCapacity+'</td>' +
	   					'	</tr>' +*/
	   					'	<tr>' +
	   					'		<td class="text-left">Occupancy</td>' +
	   					'		<td width="155" >'+nullConversion(response.occupancy[0].occupancy)+'%</td>' +
	   					'		<td width="155">'+nullConversion(response.occupancy[1].occupancy)+'%</td>' +
	   					'		<td width="155">'+nullConversion(response.occupancy[2].occupancy)+'%</td>' +
	   					'	</tr>' +
	   					
	   					'</tbody></table>	'+
	   					 '<table class="table table-bordered transparent fixed-layout">' +
	   	        	 		'<thead>' +
	   							'<tr>' +
	   								'<th class="text-center text-middle" style="200px">Particulars</th>' +
	   								'<th colspan="3" class="p-m-0">' +
	   								'	<table class="p-m-0 border-hide">' +
	   								'		<thead>' +
	   								'			<tr>' +
	   								'				<th  colspan="3" class="text-center text-middle">Competitor Properties</th>' +
	   								'			</tr>' +
	   								'			<tr>' +
	   								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[0].compititionName)+'</th>' +
	   								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[1].compititionName)+'</th>' +
	   								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[2].compititionName)+'</th>' +
	   								'			</tr>' +
	   								'		</thead>' +
	   								'	</table>' +
	   								'</th>' +
	   							'</tr>' +
	   						'</thead>' +
	   						'	<tbody>' +
	   						'	<tr>' +
	   						'		<td class="text-left">Screens (nos)</td>' +
	   						'		<td width="155">'+nullConversion(response.compitionMaster[0].noOfScreen)+'</td>' +
	   						'		<td width="155">'+nullConversion(response.compitionMaster[1].noOfScreen)+'</td>' +
	   						'		<td width="155">'+nullConversion(response.compitionMaster[2].noOfScreen)+'</td>' +
	   						'	</tr>' +
	   						'	<tr>' +
	   						'		<td class="text-left">Seats (nos)</td>' +
	   						'		<td width="155" >'+nullConversion(response.compitionMaster[0].seatCapacity)+'</td>' +
	   						'		<td width="155">'+nullConversion(response.compitionMaster[1].seatCapacity)+'</td>' +
	   						'		<td width="155">'+nullConversion(response.compitionMaster[2].seatCapacity)+'</td>' +
	   						'	</tr>' +
	   						'	<tr>' +
	   						'		<td class="text-left">Occupancy</td>' +
	   						'		<td width="155" >'+compOccupancy1+'</td>' +
	   						'		<td width="155">'+compOccupancy2+'</td>' +
	   						'		<td width="155">'+compOccupancy3+'</td>' +
	   						'	</tr>' +
	   						
	   						'</tbody>	</table>	'+
	   						'<h5>PROPOSED VALUES</h5>'+
	   						 '<table class="table table-bordered transparent fixed-layout">' +
	   		        	 		'<thead>' +
	   								'<tr>' +
	   									'<th class="text-center text-middle" style="200px">Occupancy</th>' +
	   									'<th class="text-center text-middle" style="200px">System generated value</th>' +
	   									/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
	   									'<th class="text-center text-middle" style="200px">Comments</th>' +*/
	   								'</tr>' +
	   							'</thead>' +
	   							'	<tbody>';
	   					          for(var i=0;i<response.admits.length;i++){
	   					        	  
	   					        var formatType='';
	   					      	var inputValues = "";
	   			        		var comments = "";
	   			        		let costType='';
	   			        		if(response.admits[i].formatType != null)
	   			        			formatType=response.admits[i].formatType;
	   			        		if(response.admits[i].costType != null)
	   			        			costType=response.admits[i].costType;
	   			        		if(response.admits[i].inputValues != null)
	   			        			inputValues = response.admits[i].inputValues;
	   			        		if(response.admits[i].comments != null)
	   			        			comments = response.admits[i].comments;
	   					        	  content+='<tr>' +
	   									'		<td width="155">'+formatType+'</td>' +
	   									'		<td width="155">'+costType+'%</td>' +
	   								/*	'		<td width="155">'+inputValues+'</td>' +
	   									'		<td width="155">'+comments+'</td>' +*/
	   									'	</tr>';
	   					            }
	   							
	          	 content+='</tbody></table>';
	          		$('#occupancy-table').append(content);
	          		$('.loading').hide();
	          		$('#occupancyModal').modal();	
	        	},
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});

function nullConversion(data) {
    var response = data ? data : "NA";
    return response;
}


$('#rev-atp').click(function() {
	$('.loading').show();
	$('#atp-table').empty();
	var projectID =$('#id').attr('value');
	$.ajax({
        url: "/view-modal/getTotalAdmits/"+projectID,
        type: 'GET',
        success: function(response) {
       	 //console.log(response);
       	
       	 var content = '<h5>BENCHMARK CINEMAS</h5><table class="table table-bordered transparent fixed-layout">' +
       	 		'<thead>' +
						'<tr>' +
							'<th class="text-center text-middle" style="200px">Particulars</th>' +
							'<th colspan="3" class="p-m-0">' +
							'	<table class="p-m-0 border-hide">' +
							'		<thead>' +
							'			<tr>' +
							'				<th  colspan="3" class="text-center text-middle">PVR Properties</th>' +
							'			</tr>' +
							'			<tr>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[0].cinema_name)+'</th>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[1].cinema_name)+'</th>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[2].cinema_name)+'</th>' +
							'			</tr>' +
							'		</thead>' +
							'	</table>' +
							'</th>' +
						'</tr>' +
					'</thead>' +
					'	<tbody>' +
					'	<tr>' +
					'		<td class="text-left">Screens (nos)</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[0].noOfScreen)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[1].noOfScreen)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[2].noOfScreen)+'</td>' +
					'	</tr>' +
					/*'	<tr>' +
					'		<td class="text-left">Seats (nos)</td>' +
					'		<td width="155" >'+response.occupancy[0].seatCapacity+'</td>' +
					'		<td width="155">'+response.occupancy[1].seatCapacity+'</td>' +
					'		<td width="155">'+response.occupancy[2].seatCapacity+'</td>' +
					'	</tr>' +*/
					'	<tr>' +
					'		<td class="text-left">ATP (INR)</td>' +
					'		<td width="155" >'+nullConversion(response.occupancy[0].atp)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[1].atp)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[2].atp)+'</td>' +
					'	</tr>' +
					
					'</tbody></table>	'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px">Particulars</th>' +
								'<th colspan="3" class="p-m-0">' +
								'	<table class="p-m-0 border-hide">' +
								'		<thead>' +
								'			<tr>' +
								'				<th  colspan="3" class="text-center text-middle">Competitor Properties</th>' +
								'			</tr>' +
								'			<tr>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[0].compititionName)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[1].compititionName)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[2].compititionName)+'</th>' +
								'			</tr>' +
								'		</thead>' +
								'	</table>' +
								'</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>' +
						'	<tr>' +
						'		<td class="text-left">Screens (nos)</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[0].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[1].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[2].noOfScreen)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">Seats (nos)</td>' +
						'		<td width="155" >'+nullConversion(response.compitionMaster[0].seatCapacity)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[1].seatCapacity)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[2].seatCapacity)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">ATP (INR)</td>' +
						'		<td width="155" >'+nullConversion(response.compitionMaster[0].atp)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[1].atp)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[2].atp)+'</td>' +
						'	</tr>' +
						
						'</tbody>	</table>	'+
						'<h5>PROPOSED VALUES</h5>'+
						
						       	 '<table class="table table-bordered transparent fixed-layout">' +
							 		'<thead>' +
										'<tr>' +
											'<th colspan="2" class="text-center text-middle" style="200px">ATP (INR)</th>' +
											'<th colspan="2" class="text-center text-middle" style="200px">System generated value</th>' +
											/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
											'<th class="text-center text-middle" style="200px">Comments</th>' +*/
										'</tr>' +
									'</thead>' +
									'	<tbody>';
						         for(var i=0;i<response.atp.length;i++){
						        	 
						        	
			   					      	let  normalInputValues = "";
			   					      	let  reclinerInputValues = "";
			   					      	let  loungerInputValues = "";
			   			        		let normalComments = "";
			   			        		let reclinerComments='';
			   			        		let loungerComments='';
			   			        		
			   			        		if(response.atp[i].normal.inputValues !=null){
			   			        			normalInputValues=response.atp[i].normal.inputValues;
			   			        		}
			   			        		if(response.atp[i].recliner.inputValues !=null){
			   			        			reclinerInputValues=response.atp[i].recliner.inputValues;
			   			        		}
			   			        		if(response.atp[i].lounger.inputValues !=null){
			   			        			loungerInputValues=response.atp[i].lounger.inputValues;
			   			        		}
			   			        		if(response.atp[i].normal.comments !=null){
			   			        			normalComments=response.atp[i].normal.comments;
			   			        		}
			   			        		if(response.atp[i].recliner.comments!=null){
			   			        			reclinerComments=response.atp[i].recliner.comments;
			   			        		}
			   			        		if(response.atp[i].lounger.comments!=null){
			   			        			loungerComments=response.atp[i].lounger.comments;
			   			        		}
			   			        		
						        	  content+='<tr class="total_row">' +
										'		<td colspan="4" width="155">'+nullConversion(response.atp[i].cinemaFormatType)+'</td>' +
										/*'		<td colspan="2" width="155"></td>' +
										'		<td width="155"></td>' +
										'		<td width="155"></td>' +*/
										'	</tr>'+
										'<tr class="">' +
										'		<td colspan="2" width="155">Normal seat	</td>' +
										'		<td colspan="2" width="155">'+nullConversion(response.atp[i].normal.costType)+'</td>' +
									/*	'		<td width="155">'+normalInputValues+'</td>' +
										'		<td width="155">'+normalComments+'</td>' +*/
										'	</tr>'+
										'<tr class="">' +
										'		<td colspan="2" width="155">Recliner seat	</td>' +
										'		<td colspan="2" width="155">'+nullConversion(response.atp[i].recliner.costType)+'</td>' +
							/*			'		<td width="155">'+reclinerInputValues+'</td>' +
										'		<td width="155">'+reclinerComments+'</td>' +*/
										'	</tr>'+
										'<tr class="">' +
										'		<td colspan="2" width="155">Lounger seat	</td>' +
										'		<td colspan="2" width="155">'+nullConversion(response.atp[i].lounger.costType)+'</td>' +
								/*		'		<td width="155">'+loungerInputValues+'</td>' +
										'		<td width="155">'+loungerComments+'</td>' +*/
										'	</tr>';
						            }
								
								'</tbody>	';
       	 
       		 content += "</table>"
       		$('#atp-table').append(content);
       		$('.loading').hide();
       		$('#atpModal').modal();	

        },
        error: function(xhr, status, error) {
        	$('.loading').hide();
       	 console.log(xhr+"  "+ status+"  "+ error);
        }
    });
	
	
});
//$('#rev-sph').click(function() {
$('.rev-sph').click(function() {
	$('.loading').show();
	 $('#sph-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getTotalAdmits/"+projectID,
	        type: 'GET',
	        success: function(response) {
	       	 //console.log(response);
	       	
	       	 var content = '<h5>BENCHMARK CINEMAS</h5><table class="table table-bordered transparent fixed-layout">' +
	       	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px">Particulars</th>' +
								'<th colspan="3" class="p-m-0">' +
								'	<table class="p-m-0 border-hide">' +
								'		<thead>' +
								'			<tr>' +
								'				<th  colspan="3" class="text-center text-middle">PVR Properties</th>' +
								'			</tr>' +
								'			<tr>' +
								'				<th width="155" class="text-center">'+nullConversion(response.occupancy[0].cinema_name)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.occupancy[1].cinema_name)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.occupancy[2].cinema_name)+'</th>' +
								'			</tr>' +
								'		</thead>' +
								'	</table>' +
								'</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>' +
						'	<tr>' +
						'		<td class="text-left">Screens (nos)</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[0].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[1].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[2].noOfScreen)+'</td>' +
						'	</tr>' +
					/*	'	<tr>' +
						'		<td class="text-left">Seats (nos)</td>' +
						'		<td width="155" >'+response.occupancy[0].seatCapacity+'</td>' +
						'		<td width="155">'+response.occupancy[1].seatCapacity+'</td>' +
						'		<td width="155">'+response.occupancy[2].seatCapacity+'</td>' +
						'	</tr>' +*/
						'	<tr>' +
						'		<td class="text-left">ATP (INR)</td>' +
						'		<td width="155" >'+nullConversion(response.occupancy[0].atp)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[1].atp)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[2].atp)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">SPH (INR)</td>' +
						'		<td width="155" >'+nullConversion(response.occupancy[0].sph)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[1].sph)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[2].sph)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">SPH/ATP</td>' +
						'		<td width="155" >'+nullConversion(response.occupancy[0].sphAtp)+'%</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[1].sphAtp)+'%</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[2].sphAtp)+'%</td>' +
						'	</tr>' +
						
						'</tbody></table>	'+
							'<h5>PROPOSED VALUES</h5>'+
							 '<table class="table table-bordered transparent fixed-layout">' +
			        	 		'<thead>' +
									'<tr>' +
										'<th class="text-center text-middle" style="200px">SPH (INR)</th>' +
										'<th class="text-center text-middle" style="200px">System generated value</th>' +
										/*'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
										'<th class="text-center text-middle" style="200px">Comments</th>' +*/
									'</tr>' +
								'</thead>' +
								'	<tbody>';
						          for(var i=0;i<response.sph.length;i++){
						        	  let inputType='';
						        	  let comments='';
						        	  if(response.sph[i].inputValues != null){
						        		  inputType=response.sph[i].inputValues;
						        	  }
						        	  if(response.sph[i].comments != null){
						        		  comments=response.sph[i].comments;
						        	  }
						        	  content+='<tr>' +
										'		<td width="155">'+nullConversion(response.sph[i].formatType)+'</td>' +
										'		<td width="155">'+nullConversion(response.sph[i].costType)+'</td>' +
										/*'		<td width="155">'+inputType+'</td>' +
										'		<td width="155">'+comments+'</td>' +*/
										'	</tr>';
						            }
								
	       	 content+='</tbody></table>';
	       		$('#sph-table').append(content);
	       		$('.loading').hide();
	       		$('#sphModal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
	
	
});

function formatCurrenry(rent){
	if(rent!=null){
		var x=rent.toFixed(2);
		x=x.toString();
		var afterPoint = '';
		if(x.indexOf('.') > 0)
		   afterPoint = x.substring(x.indexOf('.'),x.length);
		x = Math.floor(x);
		x=x.toString();
		var lastThree = x.substring(x.length-3);
		var otherNumbers = x.substring(0,x.length-3);
		if(otherNumbers != '')
		    lastThree = ',' + lastThree;
		var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;	
	}
	else{
		res = 'NA';
	}
	return res;
}

function formatData(rent){
	if(rent!=null){
		var x=rent.toFixed(0);
		x=x.toString();
		var afterPoint = '';
		if(x.indexOf('.') > 0)
		   afterPoint = x.substring(x.indexOf('.'),x.length);
		x = Math.floor(x);
		x=x.toString();
		var lastThree = x.substring(x.length-3);
		var otherNumbers = x.substring(0,x.length-3);
		if(otherNumbers != '')
		    lastThree = ',' + lastThree;
		var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
	}
	
	else{
		res = 'NA';
	}
	return res;
}
