function validate() {
	var person1 = $('#pergstApplicable').val();
	var person2 = $('#pergstCreditDisallowed').val();
	var person3 = $('#pereffectiveGstCost').val();
	var repaire1 = $('#repaire1').val();
	var repaire2 = $('#repaire2').val();
	var repaire3 = $('#repaire3').val();
	var rnt1 = $('#rnt1').val();
	var rnt2 = $('#rnt2').val();
	var rnt3 = $('#rnt3').val();
	var cam1 = $('#cam1').val();
	var cam2 = $('#cam2').val();
	var cam3 = $('#cam3').val();
	var elec1 = $('#elec1').val();
	var elec2 = $('#elec2').val();
	var elec3 = $('#elec3').val();
	var oth1 = $('#oth1').val();
	var oth2 = $('#oth2').val();
	var oth3 = $('#oth3').val();
	var cred1 = $('#cred1').val();
	var cred2 = $('#cred2').val();
	var cred3 = $('#cred3').val();
	var incred1 = $('#incred1').val();
	var incred2 = $('#incred2').val();
	var incred3 = $('#incred3').val();
	var fullcredit1 = $('#fullcredit1').val();
	var fullcredit2 = $('#fullcredit2').val();
	var fullcredit3 = $('#fullcredit3').val();

	if (!person1.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)) 
	{
		return false;
	}
	else if (!person2.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!person3.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!repaire1.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!repaire2.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!repaire3.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!rnt1.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!rnt2.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!rnt3.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!cam1.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!cam2.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!cam3.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!elec1.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!elec2.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!elec3.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	
	else if (!oth1.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!oth2.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!oth3.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	
	else if (!cred1.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!cred2.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!cred3.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	
	else if (!incred1.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!incred2.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!incred3.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!fullcredit1.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!fullcredit2.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!fullcredit3.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	
	else {
		return true;
	     }
}
