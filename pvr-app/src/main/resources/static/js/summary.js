var httpPath="http://"+window.location.host


$(document).ready(function(){
	$(function() {
		  $('a[href*="#"]:not([href="#"])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html, body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});	
});


$(".query-modal").click(function(){
	
	autoScroll();
	
	$('#exampleModal').modal('show');
	
	 var queryType = $(this).closest('td').attr('id');
	 var projectId = $("#id").val();
	 var reportId = $("#reportId").val();
	 
		$.ajax({
			url :"/project/getProjectDetailsById/"+projectId,
			success : function(response) {
				console.log(response);
				setQuery(response,reportId,queryType);
				
				
			},
			error:function(xhr,status,error){
				console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
				}
		});
})


function setQuery(response,reportId,queryType){
	var projectId = $("#id").val();
	$("#modal-span").text(reportId);
	$("#projectId").val(projectId);
	$("#queryType").val(queryType);
	
	switch(queryType) {
	  case "totalAdmits":
		  $("#query-type").text("Total Admits");
	    break;
	  case "occupancy":
		  $("#query-type").text("Occupancy");
	    break;
	  case "atp":
		  $("#query-type").text("ATP");
	    break;
	  case "sph":
		  $("#query-type").text("SPH");
	    break;
	  case "adRevenue":
		  $("#query-type").text("Ad Revenue(INR)");
	    break;
	  case "personalExpense":
		  $("#query-type").text("Personal Expense");
	    break;
	  case "electricityWaterExpanse":
		  $("#query-type").text("Electricity Water Expanse");
	    break;
	  case "totalRmExpenses":
		  $("#query-type").text("Total R&M Expenses");
	    break;
	    
	  case "totalOverheadExpenses":
		  $("#query-type").text("Total Overhead Expenses");
	    break; 
	    
	  case "totalProjectCost":
		  $("#query-type").text("Total Project Cost");
	    break; 
	    
	    
	  default:
		  $("#query-type").text(" ");
	}
	
	//	console.log(response[queryType]["query"][0]);
	createChatBox(queryType,response);

	
}

$("#submitQuery").click(function(){
	
	
	if($('#msg').val()=="")
		return;
	
	var queryType = $("#queryType").val();
	var projectId = $("#projectId").val();
	var msg = $("#msg").val();

	
	var data=$('#modal-form').serialize();
	
	$.ajax({
		url :"/project/save-query",
		type:"POST",
		data: data,
		success : function(response) {
			showMsg(response);
			console.log('Button id:'+queryType+'Button')
			$('#'+queryType+'Button').attr('class','float-right queryBtn query-modal blueQueryBtn');
			//console.log(data);
		},
		error:function(xhr,status,error){
			console.log('Error ' +JSON.stringify(status)+ "Error : " + error +" "+JSON.stringify(xhr))
			}
	});
	
	$(".msg_card_body").css("overflow","auto");
});


function createChatBox(queryType,projectResponse){
	
	var divToAppend="";
	$('.msg_card_body').empty();
	var projectId=projectResponse.data.id;
	var feasibilityState=projectResponse.data.feasibilityState;
	var response=projectResponse.data.finalSummary;
	
	var currentuser=$("#currentUserId").val();
	
	var currentuserId=currentuser.replace(/"/g, '');
	
	
	
	var lastUserId=response[queryType].lastQueryUserId;
	
	
	//alert(currentuserId);
	var messageLength=0;
	if(response[queryType]["query"]!=null)
	{
		messageLength=response[queryType]["query"].length;
	}
 
 //if(projectResponse.data.feasibilityState=='post_signing')
	{
	 $.each(response[queryType]["query"], function(key,value) {

		//++i;	
	  // console.log("createdBy:"+projectResponse.data.createdBy+"   user id:"+value["userId"]+"  current userId:"+currentuserId);
		 roleOfLastMessage=value["userRole"];
		 if(currentuserId == value["userId"]){
			console.log("matched");
			divToAppend=divToAppend+'<div class="d-flex justify-content-end mb-3"> '+
			' <div class="msg_cotainer_send"> '+
			value["msg"]+' <span '+
			' 	class="msg_time_send">'+value["date_time"]+'</span> '+
			' </div> '+
			' <div class="img_cont_msg"> '+
			' 	<img src="'+httpPath+'/images/clientIcon_chatbox.png" '+
			' class="rounded-circle user_img_msg"> '+
				' <p>'+value["userFullName"]+'</p> '+
				' </div> '+
		' </div>';
		}
		else{
			console.log("not matched");
			divToAppend=divToAppend+'<div class="d-flex justify-content-start mb-3">'+
			' <div class="img_cont_msg">'+
			' <img src="'+httpPath+'/images/userIcon_chatbox.png"'+
			' 	class="rounded-circle user_img_msg">'+
			' <p>'+value["userFullName"]+'</p>'+
			' </div>'+
			' <div class="msg_cotainer">'+value["msg"]+' <span class="msg_time">'+value["date_time"]+'</span>'+
			' </div>'+
			' </div>';
			
		}
	});
	
}	
 
	if(messageLength<=3)
	{
		$('#viewOtherComments').hide();
	}
	else
	{
		$('#viewOtherComments').show();
	}
	//console.log('feasibilityState:'+feasibilityState);
	//console.log('projectId:'+projectId);
	if(feasibilityState=='pre_handover' && lastUserId.trim()!=currentuserId.trim())
	{
		//console.log('Updating status');
		updateNotificationStatus(projectId,queryType);
	}
	$('.msg_card_body').append(divToAppend);
	$(".msg_card_body").css("overflow","hidden");
	
}


function showMsg(response){
	autoScroll();
	
	var divToAppend='<div class="d-flex justify-content-end mb-3"> '+
	' <div class="msg_cotainer_send"> '+
	response["msg"]+' <span '+
	' 	class="msg_time_send">'+response["date_time"]+'</span> '+
	' </div> '+
	' <div class="img_cont_msg"> '+
	' 	<img src="'+httpPath+'/images/clientIcon_chatbox.png" '+
	' class="rounded-circle user_img_msg"> '+
		 ' <p>'+response["userFullName"]+'</p> '+
		/* ' <p>BD</p> '+ */
		' </div> '+
' </div>';

	$('.msg_card_body').append(divToAppend);
	$('#msg').val("");
	
	
}

/*function showMsg(response){
	
	autoScroll();
	var divToAppend='<div class="d-flex justify-content-start mb-3">'+
	' <div class="img_cont_msg">'+
	' <img src="'+httpPath+'/images/userIcon_chatbox.png"'+
	' 	class="rounded-circle user_img_msg">'+
	' <p>'+response["userFullName"]+'</p>'+
	' </div>'+
	' <div class="msg_cotainer">'+response["msg"]+' <span class="msg_time">'+response["date_time"]+'</span>'+
	' </div>'+
	' </div>';
	
	$('.msg_card_body').append(divToAppend);
	$('#msg').val("");
	
}
*/

$("#viewOtherComments").click(function(){
	
	$(".msg_card_body").css("overflow","auto");
	
})

function autoScroll() {
	setTimeout(function(){
	$('.chat .msg_card_body').animate({
	scrollTop: $('.chat .msg_card_body').get(0).scrollHeight}, 0); 
	},300);	
}


$(document).on('click', '#submitApproved', function() {
	var projectId = this.id;
	$('#hiddenId').val(projectId);
	$("#approverModal").modal('show');

});



$(document).on('click', '#submitReject', function() {
	var projectId = $('#projectId');
	$('#hiddenId').val(projectId);
	$("#rejectModal").modal('show');

});




function validateReject(){
	var flag = true
	var reason = $("#rejectResaon").val();
	 $("#rejectError").text("");
	 if (reason.length < 1) {
		  $("#rejectError").text("Required");
          flag = false;
      }
	
	return flag;
}

function updateNotificationStatus(projectId,queryType)
{
	$.ajax({
		url :"/pre-handover/updateNotificationStatus/"+projectId+"/"+queryType+"/7",
		async: "true",
		type:"GET",
		success : function(response) {
			console.log('notification status update'+response);		
		},
		error:function(xhr,status,error){
			console.log('Error ' +JSON.stringify(status)+ "Error  updating notification status: " + error )
			}
	});
}



$('#saveReject').click(function() {
	if(validateReject()){
		form = $('#rejectForm');
		form.attr('action', '/project/reject/').trigger('submit');
	}
	
	});

$('#downloadFullReportBtn').click(function() {
	
	$('.loading').show();
	var reportId = $("#id").val();
	$.ajax({
		url :"/report/generate-file/"+ reportId,
		success : function(response) {
			console.log(response);
			if(response.status==200){
				window.location.href="/report/files/"+reportId;
				$('.loading').hide();
			}
			$('.loading').hide();
		},
		error:function(xhr,status,error){
			console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
			$('.loading').hide();
			}
	}); 
	
});

$("#saveExecutiveNote").click(function(){	
    var data=$('#step-5').serialize();
	 var projectId = $("#id").val();
		$.ajax({
			url :"/project/save-executive-note",
			type:"POST",
			data: data,
			success : function(response) {
				alert("Executive Note has been saved successfully.")
				window.location.reload();
			},
			error:function(xhr,status,error){
			  console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
			}
		}); 
})



