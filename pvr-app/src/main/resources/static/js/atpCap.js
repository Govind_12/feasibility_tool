
function validate() {
	var atpstate = $('#atpstate').val().trim();
	$('#nameError').text("");
	if(atpstate ==''){
		$('#nameError').text("Please enter State name");
		 $('#atpstate').focus();
		 return false;
	}else{
		return true;
	}

}


$(document).ready(function() {
  $(window).keydown(function(event){
    if( (event.keyCode == 13) && (validate() == false) ) {
      event.preventDefault();
      return false;
    }
  });
  
  //showTable
  $('#addFhc').on('click', function(){
	  $(this).hide();  
	  $('#atpList').show();
	  $('#addAtpDiv').toggleClass('showTable');
  	  $('#tableDiv').hide();
  });
  $('#atpList').on('click', function(){
	  $(this).hide();
	  $('#addAtpDiv').hide();
	  $('#addAtpDiv').removeAttr('class');
	  $('#addFhc').show();
	  //$('#tableDiv').hide();
  });
  
});



/*$('#addFhc').on('click',function(){
	$('#addAtpDiv').show();
	$('#tableDiv').hide();
	$('#addFhc').hide();
	$('#atpList').show();
	
	
});*/

$('#atpList').on('click',function(){
	$('#addFhc').show();
	$('#tableDiv').show();
	$('#addAtpDiv').hide();
	$('#atpList').hide();
});

$('.deleteAtp').on('click',function(){
	 var currentRow=$(this).closest("tr"); 
	 var state=currentRow.find("td:eq(0)").text().trim();
	 var screenType=currentRow.find("td:eq(1)").text().trim();
	 if(getConfirmation()){
		 window.location.href="/admin/atpcap/"+state+"/"+screenType;	 
	 }
	 
});

function getConfirmation() {
    var retVal = confirm("Do you want to continue ?");
    if( retVal == true ) {
       return true;
    } else {
       return false;
    }
 }
$("#atpCapTable").on('click','.editAtp',function(){
	
    // get the current row
    var currentRow=$(this).closest("tr"); 
    var state=currentRow.find("td:eq(0)").text().trim();
    var screenType=currentRow.find("td:eq(1)").text().trim();
    var normal=currentRow.find(".seatTypeCap_0").text().trim();
    var recliners=currentRow.find(".seatTypeCap_1").text().trim();
    var lounger=currentRow.find(".seatTypeCap_2").text().trim();
    var growthRate=currentRow.find("td:eq(4)").text().trim(); 
    var growthYear=currentRow.find("td:eq(5)").text().trim(); 
    
    
    $('#hiddenState').val(state);
    $('#hiddenScreenType').val(screenType);
    $('#edit_normal').val(normal);
    $('#edit_recliners').val(recliners);
    $('#edit_lounger').val(lounger);
    $('#edit_atpgrowthRate').val(growthRate);
    $('#edit_growthyear').val(growthYear);
    
    $('#editModal').modal();
   
});

$(function() {
	  $('.decimalLimit').on('input', function() {
	    match = (/(\d{0,9})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
	    this.value = match[1] + match[2];
	  });

	var table = $("#atpCapTable").dataTable({
		"aLengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
		"autoWidth": false,
		"columnDefs" : [ {
			"targets" : 7,
			"orderable" : false,
		} ]

	});

});