function initMap() {
	var input = document.getElementById('pac-input');
	var options = {
		/* types: ['(cities)'], */
		componentRestrictions : {
			country : [ 'IN' ]
		}
	};

	var autocomplete = new google.maps.places.Autocomplete(input, options);
	
	google.maps.event.addListener(autocomplete,'place_changed',function() {
		var place = autocomplete.getPlace();
		console.log(place.address_components);
		var lat = place.geometry.location.lat();
		var lng = place.geometry.location.lng();
		$('#latitude').val(lat);
		$('#longitude').val(lng);
	
		for (var i = 0; i < place.address_components.length; i += 1) {
			var addressObj = place.address_components[i];
			for (var j = 0; j < addressObj.types.length; j += 1) {
				if (addressObj.types[j] === 'country') {
					//console.log(addressObj.long_name);
				} else if (addressObj.types[j] === 'administrative_area1') {
					//console.log('state '+ addressObj.long_name);
				} else if (addressObj.types[j] === 'locality') {
					console.log('city '+ addressObj.long_name);
					$('#city').val(addressObj.long_name);
					//$('#city').attr('disabled','disabled');
				} else if (addressObj.types[j] === 'administrative_area_level_1') {
					console.log(addressObj.long_name)
					$('#stateCity').val(addressObj.long_name);
				}
			}
		}
	});
}





$(document).ready(function() {

	var pathname = window.location.host;
	$('#dateOfHandover').bootstrapMaterialDatePicker({
	weekStart : 0,
	time : false,
	format : 'DD/MM/YYYY',
	minDate: new Date(),
	});
	$('#dateOfOpening').bootstrapMaterialDatePicker({
	weekStart : 0,
	time : false,
	format : 'DD/MM/YYYY',
	minDate: new Date(),
	});



	$(".intLimit").inputFilter(function(value) {
	return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 1000000000);
	});

	
	$(function() {
		  $('.decimalLimit').on('input', function() {
		    match = (/(\d{0,9})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
		    this.value = match[1] + match[2];
		  });
	});

  if($("#saleModel").val() == "true"){
   	$("#saleModel").attr('checked', '');
   }
   if($("#leaseModel").val() == "true"){
	$("#leaseModel").attr('checked', '');
   }
   if($("#saleModel").val() == '' && $("#leaseModel").val() == ''){
	   $("#leaseModel").attr('checked', '');
	   $("#leaseModel").attr('value', true);
   }

});

	(function($) {
	  $.fn.inputFilter = function(inputFilter) {
	    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
	      if (inputFilter(this.value)) {
	        this.oldValue = this.value;
	        this.oldSelectionStart = this.selectionStart;
	        this.oldSelectionEnd = this.selectionEnd;
	      } else if (this.hasOwnProperty("oldValue")) {
	        this.value = this.oldValue;
	        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
	      }
	    });
	  };
	}(jQuery));

function validate() {
	var regForNumber = /^\d+$/;
	var selectVal = $("#projectName option:selected").val();
	var proName =  $("#projectNameInput").val();
	var pacinput = document.getElementById("pac-input").value;
	var exampleinput1 = document.getElementById("exampleInputEmail1").value;
	var exampleinput2 = document.getElementById("exampleInputEmail2").value;
	var exampleinput3 = document.getElementById("exampleInputEmail3").value;
	var exampleinput4 = document.getElementById("exampleInputEmail4").value;
	var dateOfHandover = document.getElementById("dateOfHandover").value;
	var dateOfOpening = document.getElementById("dateOfOpening").value;
	var myfile= $( '#uploadfile' ).val();
	
	var initial = dateOfHandover.split(/\//);
	var d =( [ initial[1], initial[0], initial[2] ].join('/')); //=> 'mm/dd/yyyy'
	var handOverDate = (new Date(d));
	
	var opening = dateOfOpening.split(/\//);
	var d1 =( [ opening[1], opening[0], opening[2] ].join('/')); //=> 'mm/dd/yyyy'
	var openingDate = (new Date(d1));
	
	
	
	$("#projectListError").text("");
	$("#pac-inputError").text("");
	$("#exampleInputEmail1Error").text("");
	$("#exampleInputEmail2Error").text("");
	$("#exampleInputEmail3Error").text("");
	$("#exampleInputEmail4Error").text("");
	$("#fromError").text("");
	$("#toError").text("");
	
	console.log(proName)

	if (selectVal !== undefined && selectVal == "") {
		$("#projectListError").text("Required");
		return false;
	}
	else if (proName !== undefined && (proName.trim() == "" || proName.trim() == null)) {
		$("#projectListError").text("Required");
		return false;
	}
	else if (pacinput.trim() == "" || pacinput.trim() == null) {
		$("#pac-inputError").text("Required");
		return false;
	}
	else if (exampleinput1.trim() == "" || exampleinput1.trim() == null) {
		$("#exampleInputEmail1Error").text("Required");
		return false;
	}
	else if (exampleinput2.trim() == "" || exampleinput2.trim() == null) {
		$("#exampleInputEmail2Error").text("Required");
		return false;
	}
	else if (exampleinput3.trim() == "" || exampleinput3.trim() == null) {
		$("#exampleInputEmail3Error").text("Required");
		return false;
	}
	else if (exampleinput4.trim() == "" || exampleinput4.trim() == null) {
		$("#exampleInputEmail4Error").text("Required");
		return false;
	}
	else if (dateOfHandover.trim() == "" || dateOfHandover.trim() == null) {
		$("#fromError").text("Required");
		return false;
	}
	else if (dateOfOpening.trim() == "" || dateOfOpening.trim() == null) {
		$("#toError").text("Required");
		return false;
	}
	else if((handOverDate).getTime() > (openingDate).getTime()) {
		$("#toError").text("Date of opening should be greater than handover date");
		document.getElementById("dateOfOpening").value = "";
		 return false;
	}
	else if(myfile!=""){
		var ext = myfile.split('.').pop();
	   if(ext=="pdf"){
	       return true;
	   } else{
		   $("#fileError").attr('style', 'color: red');
		   return false;
	   }
	}
	else {
		return true;
	}
	return false;
}







$('#projectName').on('change', function () {
    var selectVal = $("#projectName option:selected").val();
    if(selectVal != ''){
    	 window.location.href = "//" + pathname + "/post-signing/"+selectVal;
    }
    
	
});

	
	
	
	var pathname = window.location.host;

	
	   
$('#save_plan').click(function() {
	if(validate()){
		form = $('#plan');
		form.attr('action', '/post-signing/development-details/').trigger('submit');
	}
	
});

    $('#save_close').click(function() {
	form = $('#plan');
	form.attr('action', '/post-signing/development-details-save/').trigger('submit');
});

    
    
    

$(document).on('change', '[type="radio"]', function() {
    var currentValue = $(this).val(); 
    if("pre_signing" == currentValue){
    	  window.location.href = "//" + pathname + "/project";
    }
    
    if("post_signing" == currentValue){
  	  window.location.href = "//" + pathname + "/post-signing";
    }
    
    if("pre_handover" == currentValue){
    	  window.location.href = "//" + pathname + "/pre-handover";
	    }
  
});

$('#uploadfile').change(function(e){
    var fileName = e.target.files[0].name;
    var fileCurrentName = $(this).closest('.file-select').find('.selectFile').text(fileName);
});

$('#projectNameInput').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9 ]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
});

$('.isMallRadio').on('click', function (e) {
    if(this.outerText == 'Sale Model'){
    	$("#saleModel").attr('checked', '');
    	$("#saleModel").attr('value', true);
    	$("#leaseModel").removeAttr('checked');
    	$("#leaseModel").attr('value', false);
    }
    if(this.outerText == 'Lease Model'){
    	$("#leaseModel").attr('checked', '');
    	$("#leaseModel").attr('value', true);
    	$("#saleModel").removeAttr('checked');
    	$("#saleModel").attr('value', false);
    }
	
});



