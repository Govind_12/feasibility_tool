$(document).ready(function(){
	$("#approverUsers").css('display','none');
	
	$('#authorities').on('change',function(){
		
		
		if(this.value === 'ROLE_APPROVER'){
			$("#approverUsers").css('display','block');
			$("#designation").attr('required','required');
			$("#designation").val('');
		}else{
			$("#approverUsers").css('display','none');
			$("#designation").val('');
			$("#designation").removeAttr('required');
			
			
		}
	})
	
});