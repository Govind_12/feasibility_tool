$(document).ready(function() {
	
 $(document).on('click','#add-btn',function(){ 
		var rowLength=$('#valueTable tr').length;
		var rowno=parseInt(rowLength)+1;
		var newtr='<tr >'+
		'<td id="sn'+rowLength+'"><span>'+rowno+'</span></td>'+
		'<td ><input id="value'+rowLength+'" required="required" type="text" class="form-control" name="cinemaSubCategory['+rowLength+']"> '+
		'<span style="color: red"'+
        'class="text-danger" id="error'+rowLength+'"></span>'+
		'</td>'+		
		'<td class="text-center">'+
		'<img src="../../images/deleteIconImg.png" style="cursor: pointer;" class="deleteValue"'+
		'alt="delete" data-toggle="tooltip" data-placement="bottom" id="'+rowLength +'" title="Delete">'+
		'</td>'+
		'</tr>';
		$('#valueTable').append(newtr);
	});
 $(document).on('click','.deleteValue',function(){ 
	 var id = parseInt(this.id);
	 $(this).parent().parent().remove();
	 var rowLength=$('#valueTable tr').length;
	 for(var i=(id+1);i<=rowLength;i++)
	 {
		 var j=i-1;
		 $('#sn'+i).attr("id",'sn'+j);
		 $('#value'+i).attr("id",'value'+j);
		 $('#error'+i).attr("id",'error'+j);
		 $('#value'+j).attr("name",'cinemaSubCategory['+j+']');
         $('#'+i).attr("id",j);
		 $('#sn'+j).html(i);
		 
	 }
	 
	});

 
 
});

function submitCinemaCategory()
{
	 var returnFlag=true;
	 var rowLength=$('#valueTable tr').length;
	 for(var i=0;i<rowLength;i++)
	  {
		 var value=$('#value'+i).val().trim();
		 console.log(value);
		 if(value=='')
		 {
			 $('#error'+i).text('Cannot be blank');
			 $('#value'+i).focus();
			 returnFlag=false;
			 return returnFlag;
		 }
	  }
	
	 return returnFlag;
}