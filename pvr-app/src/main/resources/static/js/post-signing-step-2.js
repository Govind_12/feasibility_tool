$(document).ready(function() {

	$(".grossTurnOverLimit").inputFilter(function(value) {
		return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 1000000000);
	});
	$(".projectDeliverdLimit").inputFilter(function(value) {
		return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500);
	});
	
	var nameCounter3 = 0;
	var delivered_projects_size = $("#delivered_projects_size").val();
	if(delivered_projects_size>0){
		nameCounter3 = parseInt(delivered_projects_size)-1;
	}
	function addmoreOption() {
		nameCounter3++;
		
		var selectOption;	
		
		 selectBox = '<div class="fieldParent">'
				+ '<select class="form-control project_deliverd" name="pvrProjectDelivered['+nameCounter3+']" id="project_deliverd'+nameCounter3+'" spellcheck="true"> '
				+ '</select> '
				+ '<i class="fas fa-trash-alt crossIcon"></i> '
				+ '</div>';
		
		$.ajax({
			url :"/project/get-cinema-master-details/",
			success : function(response) {
			//console.log(response);
			
			  for(var i=0;i<response.data.length;i++){
				  $('#project_deliverd'+nameCounter3).append("<option value=\"" +  response.data[i].hopk + "\">" + response.data[i].cinema_name + "</option>");
			  }
			
			},
			error:function(xhr,status,error){
				console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
				}
		}); 	
			

	}
	
	
	
	
	
	$(document).on('click','#pvr_project_delivered #addMoreBtn',function(){ 
		addmoreOption();
		$(this).parents().find('#pvr_project_delivered fieldset').append(selectBox);
	});
	$(document).on('click', '.fieldParent >.crossIcon',function() {
		$(this).closest('.fieldParent').remove();
	});
	
	// Add Project In Pipeline #Step 2
	var nameCounter4 = 0;
	var pipeline_projects_size = $("#pipeline_projects_size").val();
	if(pipeline_projects_size>0){
		nameCounter4 = parseInt(pipeline_projects_size)-1;
	}
	function addmoreOption1(){
		nameCounter4++;
		
		projectPipeline = '<div class="fieldParent">'
			+ '	<input type="text" class="form-control" '
			+ '	name="pvrProjectPipeline['+nameCounter4+']" id="exampleInputEmail1" '
			+ '	placeholder="Enter Project"> '
			+ '	<i class="fas fa-trash-alt crossIcon"></i> '
			+ '	</div>';
	}
	$(document).on(
			'click',
			'#pvr_project_pipeline #addMoreBtn',
			function(){ 
			addmoreOption1()
				$(this).parents().find(
						'#pvr_project_pipeline fieldset')
						.append(projectPipeline);
			});
	$(document).on('click', '.fieldParent >.crossIcon',
			function() {
				$(this).closest('.fieldParent').remove();
			});

});
(function($) {
	  $.fn.inputFilter = function(inputFilter) {
	    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
	      if (inputFilter(this.value)) {
	        this.oldValue = this.value;
	        this.oldSelectionStart = this.selectionStart;
	        this.oldSelectionEnd = this.selectionEnd;
	      } else if (this.hasOwnProperty("oldValue")) {
	        this.value = this.oldValue;
	        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
	      }
	    });
	  };
	}(jQuery));
function validate() {
	var regForNumber = /^\d+$/;
	var developerName = document.getElementById("developerName").value;
	var nameOfOwner = document.getElementById("nameOfOwner").value;
	var myfile= $( '#uploadfile' ).val();
	$("#developerNameError").text("");
	$("#nameOfOwnerError").text("");
//	$("#fileError").text("");
	if (developerName.trim() == "" || developerName.trim() == null) {
		$("#developerNameError").text("Required");
		return false;
	}

	else if (nameOfOwner.trim() == "" || nameOfOwner.trim() == null) {
		$("#nameOfOwnerError").text("Required");
		return false;
	}
	else if(myfile!=""){
		var ext = myfile.split('.').pop();
	   if(ext=="pdf"){
	       return true;
	   } else{
		   $("#fileError").attr('style', 'color: red');
		   return false;
	   }
	}
	else {
		return true;
	}
	return false;
}


    
    $('#save').click(function() {
    	if(validate()){
    		form = $('#developerDetailForm');
        	form.attr('action', '/post-signing/developer-details/').trigger('submit');
    	}
    	
    	
    });

    $('#save_close').click(function() {
    	form = $('#developerDetailForm');
    	form.attr('action', '/post-signing/developer-details-save/').trigger('submit');
    });

    $('#uploadfile').change(function(e){
        var fileName = e.target.files[0].name;
        var fileCurrentName = $(this).closest('.file-select').find('.selectFile').text(fileName);
    });
   
    
    
    
    $(function() {
  	  $('.decimalLimit').on('input', function() {
  	    match = (/(\d{0,9})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
  	    this.value = match[1] + match[2];
  	  });
  	});

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
