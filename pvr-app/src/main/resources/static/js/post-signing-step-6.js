var OldFixedConstants = "";
var projectDetails = {};
$(document).ready(function(){    	  
	var pathname = window.location.host;
	var file = $('[name="file"]');

	$(document).on('click','#upload', function() {
		var step6_id =$('#id').attr('value');
		var filename = $("#uploadfile").val();
		if(filename != "")
		{
			 var fd = new FormData();
		     var file_data = $('#uploadfile')[0].files; // for multiple files
		         fd.append("file", file_data[0]);
		         fd.append("id", step6_id);
		         $('.loading').show();
		    $.ajax({
		    	url :"//" +pathname + "/upload/projectCostSummary",
		        type: "POST",
		        data: fd,
		        enctype: 'multipart/form-data',
		        processData: false,
		        contentType: false
		      }).done(function(data) {
		    	  getProjectDetailupload(data);
		    	  $('.loading').hide();
		          
		      }).fail(function(jqXHR, textStatus) {
		          alert('File upload failed ...');
		      });
	
		}
		else{
			alert('Please select a file to upload');
		}
	   	    
	});
	
	 function getProjectDetailupload(data) {
        $('#projectCost').val((data.totalProjectCost).toFixed(2));
		$('#projectCostPerScreen').val((data.projectCostPerScreen).toFixed(2));
     }
	/* $(".occupancyLimit").inputFilter(function(value) {
      	  return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 1000000000); });*/
	 
	 $(function() {
		  $('.occupancyLimit').on('input', function() {
		    match = (/(\d{0,9})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
		    this.value = match[1] + match[2];
		  });
		});
	 
	 $(function(){
	       $("input").prop('required',true);
	       $("#uploadfile").removeAttr('required');
	});
	 
   var projectId = $("#id").val();	
	 $.ajax({
		url :"/project/getProjectDetailsById/"+ projectId,
		success : function(response) {
			// console.log(response);
			projectDetails = response;
		},
		error:function(xhr,status,error){
			console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
			}
	});
	 
	 $(function() {
		  $('.occupancyLimit').on('input', function() {
		    match = (/(\d{0,9})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
		    this.value = match[1] + match[2];
		    
		    if(this.id.includes("changeBlendedInputSph")){
				var size = Number($("#changeBlendedInputSph").attr("size4")) - Number(1);
				var inputValue = 0;
				var suggestedValue = 0;
				
				var totalScreen = 0;
				var screenArray = [];
				var cinemaFormat = projectDetails.data.cinemaFormat;
				console.log(cinemaFormat)
                
                $('table tr.blendedInputSph').each(function(index,value){
                	console.log(index);
					inputValue += Number($(this).find('.input_value').val()) * Number(cinemaFormat[index].total);	
					suggestedValue += Number(cinemaFormat[index].total);
				})
				
//				$('table tr.blendedInputSph').each(function(){
//					inputValue += Number($(this).find('.input_value').val()) * Number($(this).find('.suggested_input_value').val());
//					suggestedValue += Number($(this).find('.suggested_input_value').val());
//
//				})
				var average = Number(suggestedValue) > 0 ? inputValue/suggestedValue : 0;
				$("#blendedInputValueSph").attr('value', average.toFixed(2));
			}
		    
		    if(this.id.includes("normalAtp") || this.id.includes("reclinerAtp") || this.id.includes("loungerAtp")){
				var id = this.id.split("_");
				var inputValue = 0;
				var suggestedValue = 0;
				
				var totalScreen = 0;
				var screenArray = [];
				var cinemaFormat = projectDetails.data.cinemaFormat;
				console.log(cinemaFormat)

                for(var i=0;i< cinemaFormat.length;i++){
                	if(cinemaFormat[i].formatType === id[1].toLowerCase()){
                		screenArray[0] = cinemaFormat[i].normal;
                		screenArray[1] = cinemaFormat[i].recliners;
                		screenArray[2] = cinemaFormat[i].lounger;
                		totalScreen = cinemaFormat[i].total;
                	}
				}
                
                $('table tr.'+id[1]).each(function(index, value){
                	console.log(index);
					inputValue += Number($(this).find('.input_value').val()) * Number(screenArray[index]);
				})
				
				var average = Number(totalScreen) > 0 ? inputValue/totalScreen : 0;
				$("#blendedAtp"+id[1]).attr('value', average.toFixed(2));
				
				var totalBlendedInput = 0;
				var totalScreenFormat = 0;
				for(var i=0;i< cinemaFormat.length;i++){
					var id = cinemaFormat[i].formatType.toUpperCase();
					
					if(id == "MAINSTREAM" || id == "GOLD" || id == "PLAYHOUSE" || id == "ONYX"){
						id = id.charAt(0).toUpperCase() + id.substring(1, id.length).toLowerCase();
					}
				
					totalBlendedInput += Number(cinemaFormat[i].total) * Number($("#blendedAtp"+id).val()); 
					totalScreenFormat += Number(cinemaFormat[i].total);
			    }
			    
			    var averageTotal = Number(totalScreenFormat) > 0 ? totalBlendedInput/totalScreenFormat : 0;
				$("#totalBlendedAtp").attr('value', averageTotal.toFixed(2));
				
//				$('table tr.'+id[1]).each(function(){
//					inputValue += Number($(this).find('.input_value').val()) * Number($(this).find('.suggested_input_value').val());
//					suggestedValue += Number($(this).find('.suggested_input_value').val());
//				})
//				var average = Number(suggestedValue) > 0 ? inputValue/suggestedValue : 0;
//				$("#blendedAtp"+id[1]).attr('value', average.toFixed(2));
			}
		    
		  });
	 });
	 
	 getRevenueAssumptionConstant($('#id').val());
	 
//	 $('.assumption-input').on('change keyup', function() {
//			var currentIndex = Number(this.attributes.index.value) + Number(1);
//			$('.assumption-input-error').text("");
//			this.className = this.className.replace(' assumption-input-error','');	
//			var previousFixedConstant = 0;    
//			
//			if(this.className.includes('admit_growth')){
//				previousFixedConstant = OldFixedConstants.revenueAssumptionsGrowthRate.admitsGrowth[currentIndex];
//	    	}
//	    	if(this.className.includes('atp_growth')){
//	    		previousFixedConstant = OldFixedConstants.revenueAssumptionsGrowthRate.atpGrowth[currentIndex];
//	    		
//	    	}
//	    	if(this.className.includes('sph_growth')){
//	    		previousFixedConstant = OldFixedConstants.revenueAssumptionsGrowthRate.sphGrowth[currentIndex];
//	    	}
//	    	if(this.className.includes('sig_growth')){
//	    		previousFixedConstant = OldFixedConstants.revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth[currentIndex];
//	    	}
//	    	
//	    	if(this.value == '' || Number(this.value) > Number(previousFixedConstant)){
//	    		this.value = "";
//	    		this.className = this.className + " assumption-input-error";
//	    		$('.assumption-input-error').text("Please Enter value less than or equal to the Max. Cap. value");
//			}
//		
//	});
	 
	 //Validation for ATP Growth::: 
	 $('.assumption-input').on('keyup', function() {
		 
		 if(this.className.includes('atp_growth')){
			 var cinemaFormat = projectDetails.data.cinemaFormat;
			 var normalInput = 0;
			 var totalNormalAtp = 0;
			 var finalNormalAtpYearOne = 0;
			 
			 var reclinerInput = 0;
			 var totalReclinerAtp = 0;
			 var finalReclinerAtpYearOne = 0;
			 
			 var loungerInput = 0;
			 var totalLoungerAtp = 0;
			 var finalLoungerAtpYearOne = 0;
			 
			 $('table tr.normal').each(function(index,value){
//         	    console.log(index +" "+Number($(this).find('.input_value').val()));
         	    normalInput += Number($(this).find('.input_value').val()) * cinemaFormat[index].normal;
         	    totalNormalAtp += cinemaFormat[index].normal;
		
			 })	 
			 finalNormalAtpYearOne = (Number(totalNormalAtp) > 0 ? Number(normalInput/totalNormalAtp) : 0).toFixed(2);
			 
			 $('table tr.recliner').each(function(index,value){
//				console.log(index +" "+Number($(this).find('.input_value').val()));
				reclinerInput += Number($(this).find('.input_value').val()) * cinemaFormat[index].recliners;
				totalReclinerAtp += cinemaFormat[index].recliners;
		
			 })	 
			 finalReclinerAtpYearOne = (Number(totalReclinerAtp) > 0 ? Number(reclinerInput/totalReclinerAtp) : 0).toFixed(2);
			 
			 $('table tr.lounger').each(function(index,value){
//				console.log(index +" "+Number($(this).find('.input_value').val()));
				loungerInput += Number($(this).find('.input_value').val()) * cinemaFormat[index].lounger;
				totalLoungerAtp += cinemaFormat[index].lounger;
		
			 })	 
			 finalLoungerAtpYearOne = (Number(totalLoungerAtp) > 0 ? Number(loungerInput/totalLoungerAtp) : 0).toFixed(2);
			 
			 var currentIndex = Number(this.attributes.index.value);
			 
			 $('#revenueAssumtionGrowthTable tbody tr').each(function(index){
//				console.log(index+" "+Number($(this).find('.atp_growth').val()))
				if(Number($(this).find('.atp_growth').length) === 98){
					
					$(this).find('td').each (function(indexx) {
						if($(this)[0].firstChild !== null){
							console.log("atp value in each column "+Number($(this)[0].firstChild.value));
							
							var currentPercentage = Number($(this)[0].firstChild.value);
							var normalAtp = Number($(this)[0].firstChild.attributes.normalAtp.value);
							var reclinersAtp = Number($(this)[0].firstChild.attributes.reclinersAtp.value);
							var loungerAtp = Number($(this)[0].firstChild.attributes.loungerAtp.value);
							
							finalNormalAtpYearOne = Number(finalNormalAtpYearOne) + ((Number(finalNormalAtpYearOne)*currentPercentage)/100);
							finalReclinerAtpYearOne = Number(finalReclinerAtpYearOne) + ((Number(finalReclinerAtpYearOne)*currentPercentage)/100);
							finalLoungerAtpYearOne = Number(finalLoungerAtpYearOne) + ((Number(finalLoungerAtpYearOne)*currentPercentage)/100);
							
//							ATP assumed for Normal & Recliner seats is exceeding the ATP Cap defined for these seats. Do you wish to continue with your figures ?
//							ATP assumed for Normal & Lounger seats is exceeding the ATP Cap defined for these seats. Do you wish to continue with your figures ?
//							ATP assumed for Recliner & Lounger seats is exceeding the ATP Cap defined for these seats. Do you wish to continue with your figures ?
//							ATP assumed for Normal, Recliner & Lounger seats is exceeding the ATP Cap defined for these seats. Do you wish to continue with your figures ?
							
							if(indexx === currentIndex){
								if(finalNormalAtpYearOne > normalAtp)
									alert("ATP assumed for Normal seat is exceeding the ATP Cap defined for the seats. Do you wish to continue with your figures ?");
								if(finalReclinerAtpYearOne > reclinersAtp)
									alert("ATP assumed for Recliner seat is exceeding the ATP Cap defined for these seats. Do you wish to continue with your figures ?");
								if(finalLoungerAtpYearOne > loungerAtp)
									alert("ATP assumed for Lounger seat is exceeding the ATP Cap defined for these seats. Do you wish to continue with your figures ?");
								
								return false;
							}
						}
					});
				} 	 
			 });
		 }
			
//		if(this.className.includes('atp_growth') && this.attributes.oldValue !== undefined){
//			var newValue = Number(this.value);
//			var adminValue = Number(this.attributes.oldValue.value);
//			if(newValue > adminValue){
//				alert("ATP assumed is exceeding the ATP Cap defined for the state.");
//			}
//    	}	
	});
	 
	//value updating for year 2 in revenu assumption::
	$('#yearOneFootfallAssumedLowerBy').on('change keyup', function() {
		   console.log("ji")
		   var currValue = Number(this.value);
		   $('#admit_growth1').attr('value', (currValue/(1- currValue/100)).toFixed(1));
	});
	
	//value updating for year 2 in revenu assumption::
	$('#yearOneAdIncomeLowerByPercent').on('change keyup', function() {
		   console.log("ji")
		   var currValue = Number(this.value);
		   $('#sig_growth1').attr('value', (currValue/(1- currValue/100)).toFixed(1));
	});
	 
});	

var getRevenueAssumptionConstant = function(projectId){	
	$.ajax({
		url : "getRevenueAssumptionConstant/"+projectId,
		async: false,
		success : function(result) {
			generateRevenueAssumptionTable(result.adminLeasePeriod, result);
			generateCostTable(result.adminLeasePeriod, result);

		},
		error : function(error,xhr, a) {
			console.log('Error in getFixedConstants() ajax request: '+error+' '+xhr+' '+a);
		}
	
	});	
}


var generateRevenueAssumptionTable = function(noOfColumns, fixedConstants){
	var value=0;	
	myTable = $('#revenueAssumtionGrowthTable');
	OldFixedConstants = fixedConstants;
	
	$('#theadRevenueAssumtionGrowthTable').find('tr').remove();
	$('#tbodyRevenueAssumtionGrowthTable').find('tr').remove();
	
	$('#theadRevenueAssumtionGrowthTable').append('<tr><th class="fixed-side titleColumn">Revenue Assumptions</th></tr>');
	$('#tbodyRevenueAssumtionGrowthTable').append('<tr><th class="fixed-side">Admit Growth (%) *</th></tr>');
	$('#tbodyRevenueAssumtionGrowthTable').append('<tr><th class="fixed-side ">ATP Growth (%)</th></tr>');
	$('#tbodyRevenueAssumtionGrowthTable').append('<tr><th class="fixed-side ">SPH Growth (%)</th></tr>');
	$('#tbodyRevenueAssumtionGrowthTable').append('<tr><th class="fixed-side ">Sponsorship Income Growth (%) *</th></tr>');
	$('#tbodyRevenueAssumtionGrowthTable').append('<tr><th class="fixed-side ">Convenience Fee Growth (%)</th></tr>');
	$('#tbodyRevenueAssumtionGrowthTable').append('<tr><th class="fixed-side ">Other Income Growth (%)</th></tr>');
	
	if(fixedConstants!=null){
		for(var i=0;i<=(noOfColumns-1);i++){
			var count=1;
			$('#revenueAssumtionGrowthTable tr').append($("<td>"));
			$('#revenueAssumtionGrowthTable thead tr>td:last').html("Year"+"'"  +(i+1)).css("background-color", "#a2a2a2");;
			
			if(i <= 3){
				if(i == 0){
					$('#revenueAssumtionGrowthTable tbody tr').each(function(){
						switch(count){
							case 1:
								$(this).children('td:last').append($('<input type="text" id="yearOneFootfallAssumedLowerBy" class="assumption-input validate_required" name="yearOneFootfallAssumedLowerBy" value="'+((fixedConstants.yearOneFootfallAssumedLowerBy) ? fixedConstants.yearOneFootfallAssumedLowerBy: 0)  +' "/>'))
								count++;
								break;
							case 2:
								$(this).children('td:last').css("background-color", "#f5f5f5")
								count++;
								break;
							case 3:
								$(this).children('td:last').css("background-color", "#f5f5f5")
								count++;
								break;
							case 4:
								$(this).children('td:last').append($('<input type="text" id="yearOneAdIncomeLowerByPercent" class="assumption-input validate_required" name="yearOneAdIncomeLowerByPercent" value="'+((fixedConstants.yearOneAdIncomeLowerByPercent) ? fixedConstants.yearOneAdIncomeLowerByPercent :0)+'" />'))
								count++;
							    break;
							case 5:
								$(this).children('td:last').css("background-color", "#f5f5f5")
								count++;
								break;
							case 6:
								$(this).children('td:last').css("background-color", "#f5f5f5")
								count++;
								break;
						}	
					});
					
				}else{
					$('#revenueAssumtionGrowthTable tbody tr').each(function(){
						switch(count){
							case 1:
								if(i == 1)
								  $(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text"  style="background-color:#f5f5f5" index="'+i+'" class="validate_required" id="admit_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.admitsGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.admitsGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.admitsGrowth[i+1]: 0)  +' "  readonly/>'))
								else
								  $(this).children('td:last').append($('<input type="text"  index="'+i+'" class="assumption-input validate_required" id="admit_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.admitsGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.admitsGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.admitsGrowth[i+1]: 0)  +' "  />'))
								count++;
								break;
							case 2:
								if(fixedConstants.projectLocation == 'Tamil Nadu' || fixedConstants.projectLocation == 'Andhra Pradesh' || fixedConstants.projectLocation == 'Telangana')
								   $(this).children('td:last').append($('<input type="text" index="'+i+'" class="validate_required assumption-input atp_growth" id="atp_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.atpGrowth['+(i+1)+']" normalAtp="'+fixedConstants.atpGrowthRateNormalYearOne+'" reclinersAtp="'+fixedConstants.atpGrowthRateReclinerYearOne+'" loungerAtp="'+fixedConstants.atpGrowthRateLoungerYearOne+'" oldValue="'+((fixedConstants.adminRevenueAssumptionsGrowthRate.atpGrowth[i+1]) ? fixedConstants.adminRevenueAssumptionsGrowthRate.atpGrowth[i+1] : 0)+'" value="'+((fixedConstants.revenueAssumptionsGrowthRate.atpGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.atpGrowth[i+1] : 0)+'" />'))
	                            else
								   $(this).children('td:last').append($('<input type="text" index="'+i+'" class="assumption-input validate_required" id="atp_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.atpGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.atpGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.atpGrowth[i+1] : 0)+'" />'))
								count++;
								break;
							case 3:
								$(this).children('td:last').append($('<input type="text" index="'+i+'" class="assumption-input validate_required" id="sph_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.sphGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.sphGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.sphGrowth[i+1] : 0)+'" />'))
								count++;
								break;
							case 4:
								if(i == 1)
								   $(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" index="'+i+'" class="assumption-input sig_growth" id="sig_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth[i+1] :0)+'" readonly/>'))
                                else
								   $(this).children('td:last').append($('<input type="text" index="'+i+'" class="assumption-input validate_required" id="sig_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth[i+1] :0)+'" />'))
								count++;
							    break;
							case 5:
								$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" index="'+i+'" class="assumption-input cfg_growth" id="cfg_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.convenienceFeeGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.convenienceFeeGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.convenienceFeeGrowth[i+1] : 0)+'" readonly/>'))
								count++;
								break;
							case 6:
								$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" index="'+i+'" class="assumption-input other_growth" id="other_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.otherIncomeGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.otherIncomeGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.otherIncomeGrowth[i+1] : 0)+'" readonly/>'))
								count++;
								break;
						}	
					});
				}
			
				
			}else{
				
				$('#revenueAssumtionGrowthTable tbody tr').each(function(){
					switch(count){
						case 1:
							$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" index="'+i+'" class="assumption-input admit_growth" id="admit_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.admitsGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.admitsGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.admitsGrowth[i+1]: 0)  +' "  readonly/>'))
							count++;
							break;
						case 2:
							if(fixedConstants.projectLocation == 'Tamil Nadu' || fixedConstants.projectLocation == 'Andhra Pradesh' || fixedConstants.projectLocation == 'Telangana')
							   $(this).children('td:last').append($('<input type="text" index="'+i+'" class="validate_required assumption-input atp_growth" id="atp_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.atpGrowth['+(i+1)+']" normalAtp="'+fixedConstants.atpGrowthRateNormalYearOne+'" reclinersAtp="'+fixedConstants.atpGrowthRateReclinerYearOne+'" loungerAtp="'+fixedConstants.atpGrowthRateLoungerYearOne+'" oldValue="'+((fixedConstants.adminRevenueAssumptionsGrowthRate.atpGrowth[i+1]) ? fixedConstants.adminRevenueAssumptionsGrowthRate.atpGrowth[i+1] : 0)+'" value="'+((fixedConstants.revenueAssumptionsGrowthRate.atpGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.atpGrowth[i+1] : 0)+'" />'))
                            else
							   $(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" index="'+i+'" class="assumption-input atp_growth" id="atp_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.atpGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.atpGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.atpGrowth[i+1] : 0)+'" readonly/>'))
							count++;
							break;
						case 3:
							$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" index="'+i+'" class="assumption-input sph_growth" id="sph_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.sphGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.sphGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.sphGrowth[i+1] : 0)+'" readonly/>'))
							count++;
							break;
						case 4:
							$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" index="'+i+'" class="assumption-input sig_growth" id="sig_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.sponsorshipIncomeGrowth[i+1] :0)+'" readonly/>'))
							count++;
						    break;
						case 5:
							$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" index="'+i+'" class="assumption-input cfg_growth" id="cfg_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.convenienceFeeGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.convenienceFeeGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.convenienceFeeGrowth[i+1] : 0)+'" readonly/>'))
							count++;
							break;
						case 6:
							$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" index="'+i+'" class="assumption-input other_growth" id="other_growth'+(i)+'" maxlength="5" name="revenueAssumptionsGrowthRate.otherIncomeGrowth['+(i+1)+']" value="'+((fixedConstants.revenueAssumptionsGrowthRate.otherIncomeGrowth[i+1]) ? fixedConstants.revenueAssumptionsGrowthRate.otherIncomeGrowth[i+1] : 0)+'" readonly/>'))
							count++;
							break;
					}	
				});
			}	
		}
    }
	else{	
		for(var i=1;i<=noOfColumns-1;i++){
			$('#revenueAssumtionGrowthTable tr').append($("<td>"));
			$('#revenueAssumtionGrowthTable thead tr>td:last').html("Year"+"'"  +(i+1));
			$('#revenueAssumtionGrowthTable tbody tr').each(function(){
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" value="0" />'))
			});
		}
	}
}

var generateCostTable=function(noOfColumns, fixedConstants){
	
	var value=0;
	
	myTable = $('#costAssumtionGrowthTable');
	
	$('#theadCostAssumtionTable').find('tr').remove();
	$('#tbodyCostAssumtionTable').find('tr').remove();
	
	$('#theadCostAssumtionTable').append('<tr><th class="fixed-side titleColumn">Cost Assumptions</th></tr>');
	$('#tbodyCostAssumtionTable').append('<tr><th class="fixed-side">Personnel Expenses (%)</th></tr>');
	$('#tbodyCostAssumtionTable').append('<tr><th class="fixed-side ">Electricity & Air-conditioning (%)</th></tr>');
	$('#tbodyCostAssumtionTable').append('<tr><th class="fixed-side ">Repair and Maintenance (%) </th></tr>');
	$('#tbodyCostAssumtionTable').append('<tr><th class="fixed-side "> Other Overhead expenses (%)</th></tr>');
	$('#tbodyCostAssumtionTable').append('<tr><th class="fixed-side "> Property tax (%)</th></tr>');
	
	if(fixedConstants!=null){
		
		for(var i=1;i<=(noOfColumns-1);i++){
			var count=1;
			$('#costAssumtionGrowthTable tr').append($("<td>"));
			$('#costAssumtionGrowthTable thead tr>td:last').html("Year"+"'"  +(i+1)).css("background-color", "#a2a2a2");;
			
			if(i <= 3){
				$('#costAssumtionGrowthTable tbody tr').each(function(){
					switch(count){
						case 1:
							$(this).children('td:last').append($('<input type="text" class="assumption-input validate_required" id="per_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.personnelExpenses['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.personnelExpenses[i+1]) ? fixedConstants.costAssumptionsGrowthRate.personnelExpenses[i+1] : 0)+'" />'))
							count++;
							break;
						case 2:
							$(this).children('td:last').append($('<input type="text" class="assumption-input validate_required" id="elec_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.electricityAndAirconditioning['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.electricityAndAirconditioning[i+1]) ? fixedConstants.costAssumptionsGrowthRate.electricityAndAirconditioning[i+1] : 0)+'" />'))
							count++;
							break;
						case 3:
							$(this).children('td:last').append($('<input type="text" class="assumption-input validate_required" id="rm_exp'+(i)+'"  maxlength="5" name="costAssumptionsGrowthRate.repairAndMaintenance['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.repairAndMaintenance[i+1]) ? fixedConstants.costAssumptionsGrowthRate.repairAndMaintenance[i+1] : 0)+'" />'))
							count++;
							break;
						case 4:
							$(this).children('td:last').append($('<input type="text" class="assumption-input validate_required" id="other_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.otherOverheadExpenses['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.otherOverheadExpenses[i+1]) ? fixedConstants.costAssumptionsGrowthRate.otherOverheadExpenses[i+1] : 0)+'" />'))
							count++;
							break;
						case 5:
							$(this).children('td:last').append($('<input type="text" class="assumption-input validate_required" id="prop_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.propertyTax['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.propertyTax[i+1]) ? fixedConstants.costAssumptionsGrowthRate.propertyTax[i+1] : 0)+'" />'))
							count++;
					}	
				});
				
			}else{
				
				$('#costAssumtionGrowthTable tbody tr').each(function(){
					switch(count){
						case 1:
							$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" class="assumption-input" id="per_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.personnelExpenses['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.personnelExpenses[i+1]) ? fixedConstants.costAssumptionsGrowthRate.personnelExpenses[i+1] : 0)+'" readonly/>'))
							count++;
							break;
						case 2:
							$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" class="assumption-input" id="elec_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.electricityAndAirconditioning['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.electricityAndAirconditioning[i+1]) ? fixedConstants.costAssumptionsGrowthRate.electricityAndAirconditioning[i+1] : 0)+'" readonly/>'))
							count++;
							break;
						case 3:
							$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" class="assumption-input"id="rm_exp'+(i)+'"  maxlength="5" name="costAssumptionsGrowthRate.repairAndMaintenance['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.repairAndMaintenance[i+1]) ? fixedConstants.costAssumptionsGrowthRate.repairAndMaintenance[i+1] : 0)+'" readonly/>'))
							count++;
							break;
						case 4:
							$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" class="assumption-input" id="other_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.otherOverheadExpenses['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.otherOverheadExpenses[i+1]) ? fixedConstants.costAssumptionsGrowthRate.otherOverheadExpenses[i+1] : 0)+'" readonly/>'))
							count++;
							break;
						case 5:
							$(this).children('td:last').css("background-color", "#f5f5f5").append($('<input type="text" style="background-color:#f5f5f5" class="assumption-input" id="prop_exp'+(i)+'" maxlength="5" name="costAssumptionsGrowthRate.propertyTax['+(i+1)+']" value="'+((fixedConstants.costAssumptionsGrowthRate.propertyTax[i+1]) ? fixedConstants.costAssumptionsGrowthRate.propertyTax[i+1] : 0)+'" readonly/>'))
							count++;
					}	
				});
			}	
		 }
	
       }else{
		
		for(var i=1;i<=(noOfColumns-1);i++){
			$('#costAssumtionGrowthTable tr').append($("<td>"));
			$('#costAssumtionGrowthTable thead tr>td:last').html("Year"+"'"  +(i+1));
			$('#costAssumtionGrowthTable tbody tr').each(function(){
				$(this).children('td:last').append($('<input type="text" name="rentTerm.yearWiseRent['+i+']" value="0" />'))
			});
		}
	}
}

$(function() {
	  $('.assumption-input').on('input', function() {
	    match = (/(\d{0,9})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
	    this.value = match[1] + match[2];
	  });
});

function validate() {
	//console.log('recall')
	var flag = true;
	
	$('.validate_required').each(function(){
		var currentVal = $(this).val();
		if(currentVal == ''){
			$(this).css('border-bottom','1px solid red');
			flag = false;
		}
		else if(currentVal !== '' || currentVal !== 0){
			$(this).css('border-bottom','1px solid transparent');
			//flag = false;
		}
	});
	if(!flag){
		return false;
	}
	
	$('.operatingAssumptionTableParent table tr.validationCondtionTR').each(function(){
		var getValues1 = $(this).find('.suggested_input_value').val();
		var getValues2 = $(this).find('.input_value').val();
		var getTextarea = $(this).find('.input_comment').val().trim();
		var textarea = $(this).find('.input_comment');
		console.log(getTextarea.length)
		if(parseFloat(getValues1) !== parseFloat(getValues2) ) {
			if(getTextarea.trim().length>0){
				$(textarea).css('border-bottom', '1px solid transparent');
				$(textarea).removeAttr('required',true);
				flag = true;
				console.log('if')
			}
			else{
				console.log('else')
				$(textarea).css('border-bottom', '1px solid red');
				$(textarea).prop('required',true);
				flag = false;
			}
			
		}
		else{
			$(textarea).css('border-bottom', '1px solid transparent');
			$(textarea).removeAttr('required',true);
			flag = true;
		}
		if(!flag)
			return false;
	});
	
	
	return flag;
}

$('.no_of_shows_input_value').on('change keyup', function() {
	$('#noOfShowsError').text('');
	var suggestedValue = $('.no_of_shows_suggested_value').val();
    if(Number(this.value) > Number(suggestedValue)){
    	this.value = '';
    	$('#noOfShowsError').text('Value must be less than equal to suggested value');
    }
    
    if(this.value == ''){
    	$('#noOfShows_comments').removeAttr('class');
    	$('#noOfShows_comments').removeAttr('required');
    	$('#noOfShows_comments').removeAttr('style');
    	$('#noOfShows_comments').attr('class', 'w-100 input_comment');
    }else{
    	$('#noOfShows_comments').removeAttr('class');
    	$('#noOfShows_comments').attr('class', 'w-100 input_comment validate_required');
    }
});

$('#save').click(function() {
	
	if(validate()){
		var projectCost = 	Number($('#projectCost').val());
		var projectCostPerScreen = 	Number($('#projectCostPerScreen').val());
		if(projectCost!='' && projectCostPerScreen !='' && projectCost > 0 && projectCostPerScreen > 0){
			 $('.loading').show();
			 $("form").attr("action","/post-signing/operating-assumptions/");
			$("#save").attr("type","submit");
		}
		else{
			alert('Please upload Project Cost')
		}
		
	}
	
});

$('#save_close').click(function() {
	form = $('#operating-assumptionsForm');
	form.attr('action', '/post-signing/operating-assumptions-save/').trigger('submit');
});





