$(document).ready(function()
	{
		
		$("#chngstatus").on('click',function()
				
		{
			
			var reportIds=[];
			
			$("input[name='rptName']:checked").each(function()
			{
				reportIds.push($(this).val());
			});
			
			if(reportIds.length == 0)
				{
					alert("Select at least one Report");
					return false;
				}
			//$("#reportIds").val(reportIds);
			//var form = $('#opsReportsform');
			//form.submit();
			$.ajax({
	        	 
        	    /*type: 'POST',
        	    data:'projectId='+reportIds,
        	    url: "/admin/ops-feasibility-mis/",
        	    
        	    success: function(){
        	    	alert("BD Assigned Successfully");
        	    	
        	    	location.reload();
        	    },
        	    error: function(){
        	    	alert("error while assigning report");
        	    	
        	    	location.reload();
            }*/

	        	 
        	    type: 'POST',
        	    url: "/admin/updateExtFeasibilityMis/",
        	    data: { 
 				    "ids": reportIds 
        	    },
        	    success : function(response) {
        			console.log('-----------', response);
        			
        			//console.log('=====', res)
        			$("#data-table").empty();
        			
        			var myTable= $("#data-table").DataTable( {
        				bDestroy:true,
        				fnDestroy:true
        	       
        			});
        			for(let i=0;i<response.length;i++){
        				
        				let data1=response[i];
        				var createDate = new Date(data1.createdDate).toString();
            			var res= createDate.split(" ");
            			var createdDate=res[2]+"-"+res[1]+"-"+res[3];
        				//date = date.customFormat("#DD#/#MMM#/#YYYY#");
        				myTable.row.add(['<a href="@{/project/getProjectId/}+${data1.id}" th:if="${data1.reportId != null }"'
        	               +' th:text="${data1.reportId}">' + data1.reportId + '</a>',data1.projectName,"Pre-Handover","Accepted",
        					'<span>'+ createdDate +'</span>','<input type="checkbox" value="'+data1.reportIdVersion+'" name="rptName">'	]);
        				myTable.draw();
        			}
        			
        			return false;
        		},
        		/*error:function(xhr,status,error){
        			console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
        			
        			}*/
        	
        	});
		});
		
	});

