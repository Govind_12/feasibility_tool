
$('#viewModalData').on('click', function() {
	var projectID =$("#id").val();
	 $.ajax({
         url: "/project/getAllProjectCostSummary/"+projectID,
         type: 'GET',
         success: function(response) {
         	console.log(response);
         	generateTableForProjectSummary(response);
         },
         error: function(xhr, status, error) {
        	 console.log(xhr+"  "+ status+"  "+ error);
         }
     });
});


function generateTableForProjectSummary(response){
	 $("#d1").text(response.projectCostDataList[0].cost.toFixed(2));
     $("#d2").text(response.projectCostDataList[0].depRate.toFixed(2));
     $("#d3").text(response.projectCostDataList[0].annualDep.toFixed(2));
     $("#d4").text(response.projectCostDataList[1].cost.toFixed(2));
     $("#d5").text(response.projectCostDataList[1].depRate);
     $("#d6").text(response.projectCostDataList[1].annualDep.toFixed(2));
     $("#d7").text(response.projectCostDataList[3].cost.toFixed(2));
     $("#d8").text(response.projectCostDataList[3].depRate.toFixed(2));
     $("#d9").text(response.projectCostDataList[3].annualDep.toFixed(2));
     $("#d10").text(response.projectCostDataList[4].cost.toFixed(2));
     $("#d11").text(response.projectCostDataList[4].depRate.toFixed(2));
     $("#d12").text(response.projectCostDataList[4].annualDep.toFixed(2));
     $("#d13").text(response.projectCostDataList[5].cost.toFixed(2));
     $("#d14").text(response.projectCostDataList[5].depRate.toFixed(2));
     $("#d15").text(response.projectCostDataList[5].annualDep.toFixed(2));
     $("#d16").text(response.projectCostDataList[6].cost.toFixed(2));
     $("#d17").text((response.projectCostDataList[6].depRate).toFixed(2));
     $("#d18").text(response.projectCostDataList[6].annualDep.toFixed(2));
     $("#d19").text(response.projectCostDataList[8].cost.toFixed(2));
     $("#d20").text(response.projectCostDataList[8].depRate.toFixed(2));
     $("#d21").text(response.projectCostDataList[8].annualDep.toFixed(2));
     $("#d22").text(response.projectCostDataList[9].cost.toFixed(2));
     $("#d23").text(response.projectCostDataList[9].depRate.toFixed(2));
     $("#d24").text(response.projectCostDataList[9].annualDep.toFixed(2));
     $("#d25").text(response.projectCostDataList[10].cost.toFixed(2));
     $("#d26").text(response.projectCostDataList[10].depRate.toFixed(2));
     $("#d27").text(response.projectCostDataList[10].annualDep.toFixed(2));
     $("#d28").text(response.projectCostDataList[11].cost.toFixed(2));
     $("#d29").text(response.projectCostDataList[11].depRate.toFixed(2));
     $("#d30").text(response.projectCostDataList[11].annualDep.toFixed(2));
     $("#d31").text(response.projectCostDataList[12].cost.toFixed(2));
     $("#d32").text(response.projectCostDataList[12].depRate.toFixed(2));
     $("#d33").text(response.projectCostDataList[12].annualDep.toFixed(2));
     $("#d34").text(response.projectCostDataList[13].cost.toFixed(2));
     $("#d35").text(response.projectCostDataList[13].depRate.toFixed(2));
     $("#d36").text(response.projectCostDataList[13].annualDep.toFixed(2));
     $("#d37").text(response.projectCostDataList[14].cost.toFixed(2));
     $("#d38").text(response.projectCostDataList[14].depRate.toFixed(2));
     $("#d39").text(response.projectCostDataList[14].annualDep.toFixed(2));
     
     $("#d40").text(response.projectCostDataList[15].cost.toFixed(2));
     $("#d41").text(response.projectCostDataList[15].depRate.toFixed(2));
     $("#d42").text(response.projectCostDataList[15].annualDep.toFixed(2));
     $("#d43").text(response.projectCostDataList[16].cost.toFixed(2));
     $("#d44").text(response.projectCostDataList[16].depRate.toFixed(2));
     $("#d45").text(response.projectCostDataList[16].annualDep.toFixed(2));
     $("#d46").text(response.projectCostDataList[17].cost.toFixed(2));
     $("#d47").text(response.projectCostDataList[17].depRate.toFixed(2));
     $("#d48").text(response.projectCostDataList[17].annualDep.toFixed(2));
     
     $("#d49").text(response.projectCostDataList[19].cost.toFixed(2));
     $("#d50").text(response.projectCostDataList[19].depRate.toFixed(2));
     $("#d51").text(response.projectCostDataList[19].annualDep.toFixed(2));
     $("#d52").text(response.projectCostDataList[20].cost.toFixed(2));
     $("#d53").text(response.projectCostDataList[20].depRate.toFixed(2));
     $("#d54").text(response.projectCostDataList[20].annualDep.toFixed(2));
     $("#d55").text(response.projectCostDataList[21].cost.toFixed(2));
     $("#d56").text(response.projectCostDataList[21].depRate.toFixed(2));
     $("#d57").text(response.projectCostDataList[21].annualDep.toFixed(2));
     $("#d58").text(response.projectCostDataList[22].cost.toFixed(2));
     $("#d59").text(response.projectCostDataList[22].depRate.toFixed(2));
     $("#d60").text(response.projectCostDataList[22].annualDep.toFixed(2));
     $("#d61").text(response.projectCostDataList[23].cost.toFixed(2));
     $("#d62").text(response.projectCostDataList[23].depRate.toFixed(2));
     $("#d63").text(response.projectCostDataList[23].annualDep.toFixed(2));
     $("#d64").text(response.projectCostDataList[24].cost.toFixed(2));
     $("#d65").text(response.projectCostDataList[24].depRate.toFixed(2));
     $("#d66").text(response.projectCostDataList[24].annualDep.toFixed(2));
     $("#d67").text(response.projectCostDataList[25].cost.toFixed(2));
     $("#d68").text(response.projectCostDataList[25].depRate.toFixed(2));
     $("#d69").text(response.projectCostDataList[25].annualDep.toFixed(2));
     $("#d70").text(response.projectCostDataList[26].cost.toFixed(2));
     $("#d71").text(response.projectCostDataList[26].depRate.toFixed(2));
     $("#d72").text(response.projectCostDataList[26].annualDep.toFixed(2));
     
     $("#d73").text(response.projectCostDataList[28].cost.toFixed(2));
     $("#d74").text(response.projectCostDataList[28].depRate.toFixed(2));
     $("#d75").text(response.projectCostDataList[28].annualDep.toFixed(2));
     $("#d76").text(response.projectCostDataList[29].cost.toFixed(2));
     $("#d77").text(response.projectCostDataList[29].depRate);
     $("#d78").text(response.projectCostDataList[29].annualDep);
     $("#d79").text(response.projectCostDataList[30].cost.toFixed(2));
     $("#d80").text(response.projectCostDataList[30].depRate);
     $("#d81").text(response.projectCostDataList[30].annualDep);
     $("#d82").text(response.projectCostDataList[31].cost.toFixed(2));
     $("#d83").text(response.projectCostDataList[31].depRate);
     $("#d84").text(response.projectCostDataList[31].annualDep);
     $("#d85").text(response.projectCostDataList[32].cost.toFixed(2));
     $("#d86").text((response.projectCostDataList[32].depRate).toFixed(2));
     $("#d87").text(response.projectCostDataList[32].annualDep);
     $("#d88").text(response.projectCostDataList[33].cost.toFixed(2));
     $("#d89").text(response.projectCostDataList[33].depRate);
     $("#d90").text(response.projectCostDataList[33].annualDep);
     $("#d91").text(response.projectCostDataList[34].cost.toFixed(2));
     $("#d92").text(response.projectCostDataList[34].depRate);
     $("#d93").text(response.projectCostDataList[34].annualDep);
     
     if(response.projectCostDataList[35].cost == null)
    	 response.projectCostDataList[35].cost = 0;
     
     $("#d94").text(response.projectCostDataList[35].cost.toFixed(2));
     $("#d95").text(response.projectCostDataList[35].depRate);
     $("#d96").text(response.projectCostDataList[35].annualDep);
     
     if(response.projectCostDataList[37].cost == null)
    	 response.projectCostDataList[37].cost = 0;
     
     $("#d97").text(response.projectCostDataList[36].cost.toFixed(2));
     $("#d98").text(response.projectCostDataList[36].depRate);
     $("#d99").text(response.projectCostDataList[36].annualDep);
     $("#d100").text(response.projectCostDataList[37].cost.toFixed(2));
     $("#d101").text(response.projectCostDataList[37].depRate);
     $("#d102").text(response.projectCostDataList[37].annualDep);

     if(response.projectCostDataList[38].cost == null)
    	 response.projectCostDataList[38].cost = 0;
     
     $("#d103").text(Number(response.projectCostDataList[34].cost.toFixed(2)) + Number(response.projectCostDataList[37].cost.toFixed(2)) + Number(response.projectCostDataList[35].cost.toFixed(2)));
     $("#d104").text(response.projectCostDataList[38].depRate);
     $("#d105").text(response.projectCostDataList[38].annualDep);
     
     $("#d106").text(Number(response.projectCostDataList[39].cost.toFixed(2)));
     $("#d107").text(response.projectCostDataList[39].depRate);
     $("#d108").text(response.projectCostDataList[39].annualDep);
}
$('#total-admits').click(function() {
	$('.loading').show();
	 $('#admits-table').empty();
	var projectID =$('#id').attr('value');
	 //console.log(projectID);
	 $.ajax({
         url: "/view-modal/getTotalAdmits/"+projectID,
         type: 'GET',
         success: function(response) {
        	 //console.log(response);
        	
        	 var content = '<h5>BENCHMARK CINEMAS</h5><table class="table table-bordered transparent fixed-layout">' +
        	 		'<thead>' +
						'<tr>' +
							'<th class="text-center text-middle" style="200px">Particulars</th>' +
							'<th colspan="3" class="p-m-0">' +
							'	<table class="p-m-0 border-hide">' +
							'		<thead>' +
							'			<tr>' +
							'				<th  colspan="3" class="text-center text-middle">PVR Properties</th>' +
							'			</tr>' +
							'			<tr>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[0].cinema_name)+'</th>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[1].cinema_name)+'</th>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[2].cinema_name)+'</th>' +
							'			</tr>' +
							'		</thead>' +
							'	</table>' +
							'</th>' +
						'</tr>' +
					'</thead>' +
					'	<tbody>' +
					'	<tr>' +
					'		<td class="text-left">Screens (nos)</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[0].noOfScreen)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[1].noOfScreen)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[2].noOfScreen)+'</td>' +
					'	</tr>' +
					/*'	<tr>' +
					'		<td class="text-left">Seats (nos)</td>' +
					'		<td width="155" >'+response.occupancy[0].seatCapacity+'</td>' +
					'		<td width="155">'+response.occupancy[1].seatCapacity+'</td>' +
					'		<td width="155">'+response.occupancy[2].seatCapacity+'</td>' +
					'	</tr>' +*/
					'	<tr>' +
					'		<td class="text-left">Admits (Lacs)</td>' +
					'		<td width="155" >'+formatData(response.occupancy[0].admits)+'</td>'+
					'		<td width="155">'+formatData(response.occupancy[1].admits)+'</td>' +
					'		<td width="155">'+formatData(response.occupancy[2].admits)+'</td>' +
					'	</tr>' +
					
					'</tbody></table>	'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px">Particulars</th>' +
								'<th colspan="3" class="p-m-0">' +
								'	<table class="p-m-0 border-hide">' +
								'		<thead>' +
								'			<tr>' +
								'				<th  colspan="3" class="text-center text-middle">Competitor Properties</th>' +
								'			</tr>' +
								'			<tr>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[0].compititionName)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[1].compititionName)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[2].compititionName)+'</th>' +
								'			</tr>' +
								'		</thead>' +
								'	</table>' +
								'</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>' +
						'	<tr>' +
						'		<td class="text-left">Screens (nos)</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[0].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[1].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[2].noOfScreen)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">Seats (nos)</td>' +
						'		<td width="155" >'+nullConversion(response.compitionMaster[0].seatCapacity)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[1].seatCapacity)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[2].seatCapacity)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">Admits (Lacs)</td>' +
						'		<td width="155" >'+formatData(response.compitionMaster[0].admits)+'</td>' +
						'		<td width="155">'+formatData(response.compitionMaster[1].admits)+'</td>' +
						'		<td width="155">'+formatData(response.compitionMaster[2].admits)+'</td>' +
						'	</tr>' +
						
						'</tbody>	</table>	'+
						'<h5>PROPOSED VALUES</h5>'+
						
							 '<table class="table table-bordered transparent fixed-layout">' +
			        	 		'<thead>' +
									'<tr>' +
										'<th class="text-center text-middle" style="200px">Admits</th>' +
										'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
									'</tr>' +
								'</thead>' +
								'	<tbody>';
						          for(var i=0;i<response.admits.length;i++){
						        	  content+='<tr class="total_row">' +
										'		<td width="155">'+response.admits[i].formatType+'</td>' +
										'		<td width="155"></td>' +
										'	</tr>'+
										'<tr class="">' +
										'		<td width="155">Normal seat	</td>' +
										'		<td width="155">'+formatData(response.admits[i].admits.normals)+'</td>' +
										'	</tr>'+
										'<tr class="">' +
										'		<td width="155">Recliner seat	</td>' +
										'		<td width="155">'+formatData(response.admits[i].admits.recliner)+'</td>' +
										'	</tr>'+
										'<tr class="">' +
										'		<td width="155">Lounger seat	</td>' +
										'		<td width="155">'+formatData(response.admits[i].admits.longers)+'</td>' +
										'	</tr>';
						            }
								
								'</tbody>	';
        	 
        		 content += "</table>"
        		$('#admits-table').append(content);
        		 $('.loading').hide();
        		 $('#totalAdmitsModal').modal();

         },
         error: function(xhr, status, error) {
        	 $('.loading').hide();
        	 console.log(xhr+"  "+ status+"  "+ error);
         }
     });
		
	
});


$('#rev-occupany').click(function() {
	$('.loading').show();
	 $('#occupancy-table').empty();
	var projectID =$('#id').attr('value');
	 $.ajax({
        url: "/view-modal/getTotalAdmits/"+projectID,
        type: 'GET',
        success: function(response) {
        	console.log(response)
        	let compOccupancy1='';
        	let compOccupancy2='';
        	let compOccupancy3='';
        	if(response.compitionMaster[0].occupancy === null){
        		compOccupancy1='NA';
        	}else{
        		compOccupancy1='NA';
        	}
        	if(response.compitionMaster[1].occupancy === null){
        		compOccupancy2='NA';
        	}else{
        		compOccupancy2='NA';
        	}
        	if(response.compitionMaster[2].occupancy === null){
        		compOccupancy3='NA';
        	}else{
        		compOccupancy3='NA';
        	}
       	
       	 var content = '<h5>BENCHMARK CINEMAS</h5><table class="table table-bordered transparent fixed-layout">' +
       	 		'<thead>' +
						'<tr>' +
							'<th class="text-center text-middle" style="200px">Particulars</th>' +
							'<th colspan="3" class="p-m-0">' +
							'	<table class="p-m-0 border-hide">' +
							'		<thead>' +
							'			<tr>' +
							'				<th  colspan="3" class="text-center text-middle">PVR Properties</th>' +
							'			</tr>' +
							'			<tr>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[0].cinema_name)+'</th>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[1].cinema_name)+'</th>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[2].cinema_name)+'</th>' +
							'			</tr>' +
							'		</thead>' +
							'	</table>' +
							'</th>' +
						'</tr>' +
					'</thead>' +
					'	<tbody>' +
					'	<tr>' +
					'		<td class="text-left">Screens (nos)</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[0].noOfScreen)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[1].noOfScreen)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[2].noOfScreen)+'</td>' +
					'	</tr>' +
				/*	'	<tr>' +
					'		<td class="text-left">Seats (nos)</td>' +
					'		<td width="155" >'+response.occupancy[0].seatCapacity+'</td>' +
					'		<td width="155">'+response.occupancy[1].seatCapacity+'</td>' +
					'		<td width="155">'+response.occupancy[2].seatCapacity+'</td>' +
					'	</tr>' +*/
					'	<tr>' +
					'		<td class="text-left">Occupancy</td>' +
					'		<td width="155" >'+nullConversion(response.occupancy[0].occupancy)+'%</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[1].occupancy)+'%</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[2].occupancy)+'%</td>' +
					'	</tr>' +
					
					'</tbody></table>	'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px">Particulars</th>' +
								'<th colspan="3" class="p-m-0">' +
								'	<table class="p-m-0 border-hide">' +
								'		<thead>' +
								'			<tr>' +
								'				<th  colspan="3" class="text-center text-middle">Competitor Properties</th>' +
								'			</tr>' +
								'			<tr>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[0].compititionName)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[1].compititionName)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[2].compititionName)+'</th>' +
								'			</tr>' +
								'		</thead>' +
								'	</table>' +
								'</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>' +
						'	<tr>' +
						'		<td class="text-left">Screens (nos)</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[0].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[1].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[2].noOfScreen)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">Seats (nos)</td>' +
						'		<td width="155" >'+nullConversion(response.compitionMaster[0].seatCapacity)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[1].seatCapacity)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[2].seatCapacity)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">Occupancy</td>' +
						'		<td width="155" >'+compOccupancy1+'</td>' +
						'		<td width="155">'+compOccupancy2+'</td>' +
						'		<td width="155">'+compOccupancy3+'</td>' +
						'	</tr>' +
						
						'</tbody>	</table>	'+
						'<h5>PROPOSED VALUES</h5>'+
						 '<table class="table table-bordered transparent fixed-layout">' +
		        	 		'<thead>' +
								'<tr>' +
									'<th class="text-center text-middle" style="200px">Occupancy</th>' +
									'<th class="text-center text-middle" style="200px">System generated value</th>' +
									'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
									'<th class="text-center text-middle" style="200px">Comments</th>' +
								'</tr>' +
							'</thead>' +
							'	<tbody>';
					          for(var i=0;i<response.admits.length;i++){
					        	  content+='<tr>' +
									'		<td width="155">'+nullConversion(response.admits[i].formatType)+'</td>' +
									'		<td width="155">'+nullConversion(response.admits[i].costType)+'%</td>' +
									'		<td width="155">'+nullConversion(response.admits[i].inputValues)+'%</td>' +
									'		<td width="155">'+response.admits[i].comments+'</td>' +
									'	</tr>';
					            }
							
       	 content+='</tbody></table>';
       		$('#occupancy-table').append(content);
       		$('.loading').hide();
       		$('#occupancyModal').modal();	

        },
        error: function(xhr, status, error) {
        	$('.loading').hide();
       	 console.log(xhr+"  "+ status+"  "+ error);
        }
    });
	
	
});


$('#rev-atp').click(function() {
	$('.loading').show();
	$('#atp-table').empty();
	var projectID =$('#id').attr('value');
	$.ajax({
        url: "/view-modal/getTotalAdmits/"+projectID,
        type: 'GET',
        success: function(response) {
       	 //console.log(response);
       	
       	 var content = '<h5>BENCHMARK CINEMAS</h5><table class="table table-bordered transparent fixed-layout">' +
       	 		'<thead>' +
						'<tr>' +
							'<th class="text-center text-middle" style="200px">Particulars</th>' +
							'<th colspan="3" class="p-m-0">' +
							'	<table class="p-m-0 border-hide">' +
							'		<thead>' +
							'			<tr>' +
							'				<th  colspan="3" class="text-center text-middle">PVR Properties</th>' +
							'			</tr>' +
							'			<tr>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[0].cinema_name)+'</th>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[1].cinema_name)+'</th>' +
							'				<th width="155" class="text-center">'+nullConversion(response.occupancy[2].cinema_name)+'</th>' +
							'			</tr>' +
							'		</thead>' +
							'	</table>' +
							'</th>' +
						'</tr>' +
					'</thead>' +
					'	<tbody>' +
					'	<tr>' +
					'		<td class="text-left">Screens (nos)</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[0].noOfScreen)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[1].noOfScreen)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[2].noOfScreen)+'</td>' +
					'	</tr>' +
			/*		'	<tr>' +
					'		<td class="text-left">Seats (nos)</td>' +
					'		<td width="155" >'+nullConversion(response.occupancy[0].seatCapacity+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[1].seatCapacity+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[2].seatCapacity+'</td>' +
					'	</tr>' +*/
					'	<tr>' +
					'		<td class="text-left">ATP (INR)</td>' +
					'		<td width="155" >'+nullConversion(response.occupancy[0].atp)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[1].atp)+'</td>' +
					'		<td width="155">'+nullConversion(response.occupancy[2].atp)+'</td>' +
					'	</tr>' +
					
					'</tbody></table>	'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px">Particulars</th>' +
								'<th colspan="3" class="p-m-0">' +
								'	<table class="p-m-0 border-hide">' +
								'		<thead>' +
								'			<tr>' +
								'				<th  colspan="3" class="text-center text-middle">Competitor Properties</th>' +
								'			</tr>' +
								'			<tr>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[0].compititionName)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[1].compititionName)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.compitionMaster[2].compititionName)+'</th>' +
								'			</tr>' +
								'		</thead>' +
								'	</table>' +
								'</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>' +
						'	<tr>' +
						'		<td class="text-left">Screens (nos)</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[0].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[1].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[2].noOfScreen)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">Seats (nos)</td>' +
						'		<td width="155" >'+nullConversion(response.compitionMaster[0].seatCapacity)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[1].seatCapacity)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[2].seatCapacity)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">ATP (INR)</td>' +
						'		<td width="155" >'+nullConversion(response.compitionMaster[0].atp)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[1].atp)+'</td>' +
						'		<td width="155">'+nullConversion(response.compitionMaster[2].atp)+'</td>' +
						'	</tr>' +
						
						'</tbody>	</table>	'+
						'<h5>PROPOSED VALUES</h5>'+
						
						       	 '<table class="table table-bordered transparent fixed-layout">' +
							 		'<thead>' +
										'<tr>' +
											'<th class="text-center text-middle" style="200px">ATP (INR)</th>' +
											'<th class="text-center text-middle" style="200px">System generated value</th>' +
											'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
											'<th class="text-center text-middle" style="200px">Comments</th>' +
										'</tr>' +
									'</thead>' +
									'	<tbody>';
						         for(var i=0;i<response.atp.length;i++){
						        	  content+='<tr class="total_row">' +
										'		<td width="155">'+nullConversion(response.atp[i].cinemaFormatType)+'</td>' +
										'		<td width="155"></td>' +
										'		<td width="155"></td>' +
										'		<td width="155"></td>' +
										'	</tr>'+
										'<tr class="">' +
										'		<td width="155">Normal seat	</td>' +
										'		<td width="155">'+nullConversion(response.atp[i].normal.costType)+'</td>' +
										'		<td width="155">'+nullConversion(response.atp[i].normal.inputValues)+'</td>' +
										'		<td width="155">'+nullConversion(response.atp[i].normal.comments)+'</td>' +
										'	</tr>'+
										'<tr class="">' +
										'		<td width="155">Recliner seat	</td>' +
										'		<td width="155">'+nullConversion(response.atp[i].recliner.costType)+'</td>' +
										'		<td width="155">'+nullConversion(response.atp[i].recliner.inputValues)+'</td>' +
										'		<td width="155">'+nullConversion(response.atp[i].recliner.comments)+'</td>' +
										'	</tr>'+
										'<tr class="">' +
										'		<td width="155">Lounger seat	</td>' +
										'		<td width="155">'+nullConversion(response.atp[i].lounger.costType)+'</td>' +
										'		<td width="155">'+nullConversion(response.atp[i].lounger.inputValues)+'</td>' +
										'		<td width="155">'+nullConversion(response.atp[i].lounger.comments)+'</td>' +
										'	</tr>';
						            }
								
								'</tbody>	';
       	 
       		 content += "</table>"
       		$('#atp-table').append(content);
       		$('.loading').hide();
       		$('#atpModal').modal();	

        },
        error: function(xhr, status, error) {
        	$('.loading').hide();
       	 console.log(xhr+"  "+ status+"  "+ error);
        }
    });
	
	
});


$('#rev-sph').click(function() {
	$('.loading').show();
	 $('#sph-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getTotalAdmits/"+projectID,
	        type: 'GET',
	        success: function(response) {
	       	 //console.log(response);
	       	
	       	 var content = '<h5>BENCHMARK CINEMAS</h5><table class="table table-bordered transparent fixed-layout">' +
	       	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px">Particulars</th>' +
								'<th colspan="3" class="p-m-0">' +
								'	<table class="p-m-0 border-hide">' +
								'		<thead>' +
								'			<tr>' +
								'				<th  colspan="3" class="text-center text-middle">PVR Properties</th>' +
								'			</tr>' +
								'			<tr>' +
								'				<th width="155" class="text-center">'+nullConversion(response.occupancy[0].cinema_name)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.occupancy[1].cinema_name)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.occupancy[2].cinema_name)+'</th>' +
								'			</tr>' +
								'		</thead>' +
								'	</table>' +
								'</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>' +
						'	<tr>' +
						'		<td class="text-left">Screens (nos)</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[0].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[1].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[2].noOfScreen)+'</td>' +
						'	</tr>' +
				/*		'	<tr>' +
						'		<td class="text-left">Seats (nos)</td>' +
						'		<td width="155" >'+nullConversion(response.occupancy[0].seatCapacity+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[1].seatCapacity+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[2].seatCapacity+'</td>' +
						'	</tr>' +*/
						'	<tr>' +
						'		<td class="text-left">ATP (INR)</td>' +
						'		<td width="155" >'+nullConversion(response.occupancy[0].atp)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[1].atp)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[2].atp)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">SPH (INR)</td>' +
						'		<td width="155" >'+nullConversion(response.occupancy[0].sph)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[1].sph)+'</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[2].sph)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">SPH/ATP</td>' +
						'		<td width="155" >'+nullConversion(response.occupancy[0].sphAtp)+'%</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[1].sphAtp)+'%</td>' +
						'		<td width="155">'+nullConversion(response.occupancy[2].sphAtp)+'%</td>' +
						'	</tr>' +
						
						'</tbody></table>	'+
							'<h5>PROPOSED VALUES</h5>'+
							 '<table class="table table-bordered transparent fixed-layout">' +
			        	 		'<thead>' +
									'<tr>' +
										'<th class="text-center text-middle" style="200px">SPH (INR)</th>' +
										'<th class="text-center text-middle" style="200px">System generated value</th>' +
										'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
										'<th class="text-center text-middle" style="200px">Comments</th>' +
									'</tr>' +
								'</thead>' +
								'	<tbody>';
						          for(var i=0;i<response.sph.length;i++){
						        	  content+='<tr>' +
										'		<td width="155">'+nullConversion(response.sph[i].formatType)+'</td>' +
										'		<td width="155">'+nullConversion(response.sph[i].costType)+'</td>' +
										'		<td width="155">'+nullConversion(response.sph[i].inputValues)+'</td>' +
										'		<td width="155">'+response.sph[i].comments+'</td>' +
										'	</tr>';
						            }
								
	       	 content+='</tbody></table>';
	       		$('#sph-table').append(content);
	       		$('.loading').hide();
	       		$('#sphModal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
	
	
});


$('#rev-ad-revenue').click(function() {
	$('.loading').show();
	 $('#ad-revenue-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getAdRevenue/"+projectID,
	        type: 'GET',
	        success: function(response) {
	       	 //console.log(response);
	       	
	       	 var content = '<h5>BENCHMARK CINEMAS</h5><table class="table table-bordered transparent fixed-layout">' +
	       	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px">Particulars</th>' +
								'<th colspan="3" class="p-m-0">' +
								'	<table class="p-m-0 border-hide">' +
								'		<thead>' +
								'			<tr>' +
								'				<th  colspan="3" class="text-center text-middle">PVR Properties</th>' +
								'			</tr>' +
								'			<tr>' +
								'				<th width="155" class="text-center">'+nullConversion(response.adrevenue.benchmarkCinemas[0].cinema_name)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.adrevenue.benchmarkCinemas[1].cinema_name)+'</th>' +
								'				<th width="155" class="text-center">'+nullConversion(response.adrevenue.benchmarkCinemas[2].cinema_name)+'</th>' +
								'			</tr>' +
								'		</thead>' +
								'	</table>' +
								'</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>' +
						'	<tr>' +
						'		<td class="text-left">Screens (nos)</td>' +
						'		<td width="155">'+nullConversion(response.adrevenue.benchmarkCinemas[0].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.adrevenue.benchmarkCinemas[1].noOfScreen)+'</td>' +
						'		<td width="155">'+nullConversion(response.adrevenue.benchmarkCinemas[2].noOfScreen)+'</td>' +
						'	</tr>' +
						'	<tr>' +
						'		<td class="text-left">Ad Revenue (INR)</td>' +
						'		<td width="155" >'+formatData(response.adrevenue.benchmarkCinemas[0].adRevenue)+'</td>' +
						'		<td width="155">'+formatData(response.adrevenue.benchmarkCinemas[1].adRevenue)+'</td>' +
						'		<td width="155">'+formatData(response.adrevenue.benchmarkCinemas[2].adRevenue)+'</td>' +
						'	</tr>' +
				/*		'	<tr>' +
						'		<td class="text-left">Seats (nos)</td>' +
						'		<td width="155" >'+response.adrevenue.benchmarkCinemas[0].seatCapacity+'</td>' +
						'		<td width="155">'+response.adrevenue.benchmarkCinemas[1].seatCapacity+'</td>' +
						'		<td width="155">'+response.adrevenue.benchmarkCinemas[2].seatCapacity+'</td>' +
						'	</tr>' +*/
						'</tbody></table>	'+
							'<h5>PROPOSED VALUES</h5>'+
							 '<table class="table table-bordered transparent fixed-layout">' +
			        	 		'<thead>' +
									'<tr>' +
										'<th class="text-center text-middle" style="200px"></th>' +
										'<th class="text-center text-middle" style="200px">System generated value</th>' +
										'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
										'<th class="text-center text-middle" style="200px">Comments</th>' +
									'</tr>' +
								'</thead>' +
								'	<tbody>'+
							       	'	<tr>' +
									'		<td class="text-left">Ad Revenue (INR)</td>' +
									'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
									'		<td width="155">'+formatData(response.projectDetails.inputValues)+'</td>' +
									'		<td width="155">'+response.projectDetails.comments+'</td>' +
									'	</tr>' ;
								
	       	 content+='</tbody></table>';
	       		$('#ad-revenue-table').append(content);
	       		$('.loading').hide();
	       		$('#AdRevenueModal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});

$('#personnel-expenses').click(function() {
	$('.loading').show();
	 $('#personnel-expenses-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getPersonalExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	       	 //console.log(response);
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Personnel Expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.personalExpenseBenchmark.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.personalExpenseBenchmark[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.personalExpenseBenchmark[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.personalExpenseBenchmark[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h5>PROPOSED VALUES</h5>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
								'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Personnel Expense (INR)</td>' +
							'		<td width="155">'+formatData(response.personalExpense.costType)+'</td>' +
							'		<td width="155">'+formatData(response.personalExpense.inputValues)+'</td>' +
							'		<td width="155">'+response.personalExpense.comments+'</td>' +
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#personnel-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#personnel-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});


$('#electricity-water').click(function() {
	$('.loading').show();
	 $('#electricity-water-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getElectricityExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	       	 //console.log(response);
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Electricity & water Expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.electicityAndWater.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.electicityAndWater[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.electicityAndWater[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.electicityAndWater[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h5>PROPOSED VALUES</h5>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
								'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Electricity & water Expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							'		<td width="155">'+formatData(response.projectDetails.inputValues)+'</td>' +
							'		<td width="155">'+response.projectDetails.comments+'</td>' +
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#electricity-water-table').append(content);
	       		$('.loading').hide();
	       		$('#electricity-water-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});


$('#rm-expenses').click(function() {
	$('.loading').show();
	 $('#rm-expenses-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getRmExpense/"+projectID,
	        type: 'GET',
	        success: function(response) {
	       	 //console.log(response);
	       	
	       	var content = '<table class="table table-bordered transparent fixed-layout">' +
   	 		'<thead>' +
					'<tr>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Benchmark Cinemas</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Total R&M Expense (INR)</th>' +
					'<th colspan="1" class="text-center text-middle" style="200px">Screens (nos)</th>' +
					'</tr>' +
				'</thead>' +
				'	<tbody>';
			      for(var i=0;i<response.repairAndMaintaince.length;i++){
		        	  content+='<tr>' +
						'		<td width="155">'+nullConversion(response.repairAndMaintaince[i].cinemaName)+'</td>' +
						'		<td width="155">'+formatData(response.repairAndMaintaince[i].navCode)+'</td>' +
						'		<td width="155">'+nullConversion(response.repairAndMaintaince[i].noOfScreen)+'</td>' +
						'	</tr>';
		            }
			      content+='</tbody></table>	'+
					'<h5>PROPOSED VALUES</h5>'+
					 '<table class="table table-bordered transparent fixed-layout">' +
	        	 		'<thead>' +
							'<tr>' +
								'<th class="text-center text-middle" style="200px"></th>' +
								'<th class="text-center text-middle" style="200px">System generated value</th>' +
								'<th class="text-center text-middle" style="200px">Final proposed value</th>' +
								'<th class="text-center text-middle" style="200px">Comments</th>' +
							'</tr>' +
						'</thead>' +
						'	<tbody>'+
					       	'	<tr>' +
							'		<td class="text-left">Total R&M Expense (INR)</td>' +
							'		<td width="155">'+formatData(response.projectDetails.costType)+'</td>' +
							'		<td width="155">'+formatData(response.projectDetails.inputValues)+'</td>' +
							'		<td width="155">'+response.projectDetails.comments+'</td>' +
							'	</tr>' ;
						
	       		content+='</tbody></table>';
	       		$('#rm-expenses-table').append(content);
	       		$('.loading').hide();
	       		$('#rm-expenses-modal').modal();	

	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});

$('#annual-rent').click(function() {
	$('.loading').show();
	 $('#annual-rent-table').empty();
		var projectID =$('#id').attr('value');
		 $.ajax({
	        url: "/view-modal/getAnnualRent/"+projectID,
	        type: 'GET',
	        success: function(response) {
		       	 //console.log(response);
		       	 switch (response.lesserLesse.rentPaymentTerm) {
					case "fixed":
						createFixedTable(response);
						break;
		
					case "revenue_share":
						createRevenueShareTable(response);
						break;
					case "mg_revenue_share":
						createMgRevenueShareTable(response);				
						break;
					case "other_1":
						createOther1Table(response);
						break;
					case "other_2":
						createOther2Table(response);
						break;
		       	 }
	        },
	        error: function(xhr, status, error) {
	        	$('.loading').hide();
	       	 console.log(xhr+"  "+ status+"  "+ error);
	        }
	    });
});

function formatData(rent){
	if(rent!=null){
		var x=rent.toFixed(0);
		x=x.toString();
		var afterPoint = '';
		if(x.indexOf('.') > 0)
		   afterPoint = x.substring(x.indexOf('.'),x.length);
		x = Math.floor(x);
		x=x.toString();
		var lastThree = x.substring(x.length-3);
		var otherNumbers = x.substring(0,x.length-3);
		if(otherNumbers != '')
		    lastThree = ',' + lastThree;
		var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
	}
	else{
		res = 'NA';
	}
	
	return res;
}

function formatCurrenry(rent){
	if(rent!=null){
		var x=rent.toFixed(2);
		x=x.toString();
		var afterPoint = '';
		if(x.indexOf('.') > 0)
		   afterPoint = x.substring(x.indexOf('.'),x.length);
		x = Math.floor(x);
		x=x.toString();
		var lastThree = x.substring(x.length-3);
		var otherNumbers = x.substring(0,x.length-3);
		if(otherNumbers != '')
		    lastThree = ',' + lastThree;
		var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
		
	}
	else{
		res = 'NA';
	}
	
	return res;
}


function createFixedTable(response){
	
	var content = '<h5>PAYMENT DETAILS</h5><table class="table table-bordered transparent" style="    margin-top: 15px;box-shadow: 0 0 0 2px #000;border: hidden;">'+
				   '     <tbody class="mb-2">'+
				   ' <tr>'+
				   '     <th width="160" rowspan="7" class="text-center text-middle" style="border: 0;border-right: 2px solid #000;">Rent</th>'+
				   '     <td class="text-left">Payment terms</td>'+
				   '     <td width="200" class="text-left">Fixed</td>'+
				   ' </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Rent per sqr ft per month</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.rent+'</td>'+
				 '   </tr>'+
				  '  <tr>'+
				   '     <td class="text-left">Percentage escalation </td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.percOfEscalation+'%</td>'+
				  '  </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Frequency of escalation</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.freqOfEscalation+' Years</td>'+
				  '  </tr>'+
				  '  <tr>'+
				   '     <td class="text-left">Other terms</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.explainRentAgreement+'</td>'+
				 '   </tr>'+
				'</tbody>'+
				'</table>';
	
			    content+='<h5>Year Wise Rent</h5>'+
				 '<div class="yearTableParent"><table class="table table-bordered transparent fixed-layout" style="margin-top:15px;">' +
			 		'<thead>' +
						'<tr>' +
							'<th class="text-center text-middle" style="">Lease Period (Yrs)</th>';
							    $.each(response.projectDetails.yearWiseRent, function(k, v) {
							    	content+='<td class="text-center text-middle" width="">'+k+'</td>';
								  });
				content+='</tr>' +
					'</thead>' +
					'	<tbody>'+
							'<tr>' +
							'<th class="text-center text-middle" style="">Rent (INR)</th>';
							 $.each(response.projectDetails.yearWiseRent, function(k, v) {
							    	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v)+'</td>';
								  });
				 content+='</tr></tbody></table></div>';
   		$('#annual-rent-table').append(content);
   		$('.loading').hide();
   		$('#annual-rent-modal').modal();	
}


function createRevenueShareTable(response){
	console.log(response)
	var content = '<h5>PAYMENT DETAILS</h5><table class="table table-bordered transparent" style="    margin-top: 15px;box-shadow: 0 0 0 2px #000;border: hidden;">'+
				   '     <tbody class="mb-2">'+
				   ' <tr>'+
				   '     <th width="160" rowspan="8" class="text-center text-middle" style="border: 0;border-right: 2px solid #000;">Rent</th>'+
				   '     <td class="text-left">Payment terms</td>'+
				   '     <td width="200" class="text-left">Revenue Share</td>'+
				   ' </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Box Office Percentage</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.boxOfficePercentage+'</td>'+
				 '   </tr>'+
				  '  <tr>'+
				   '     <td class="text-left">F&B Percentage</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.fandBPercentage+'%</td>'+
				  '  </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Ad Sale Percentage</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.adSalePercentage+' Years</td>'+
				  '  </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Others Percentage</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.othersPercentage+' Years</td>'+
				  '  </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Frequency of escalation</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.freqOfEscalation+' Years</td>'+
				  '  </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Percentage of escalation</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.percOfEscalation+' Years</td>'+
				  '  </tr>'+
				  '  <tr>'+
				   '     <td class="text-left">Other terms</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.explainRentAgreement+'</td>'+
				 '   </tr>'+
				'</tbody>'+
				'</table>';
	
			    content+='<h5>Year Wise Rent</h5>'+
				 '<div class="yearTableParent"><table class="table table-bordered transparent fixed-layout" style="margin-top:15px;">' +
			 		'<thead>' +
						'<tr>' +
							'<th class="text-center text-middle" style="">Year</th>';
							    $.each(response.projectDetails.yearWiseAdSalePercentage, function(k, v) {
							    	content+='<td class="text-center text-middle" width="">'+k+'</td>';
								  });
				  content+='</tr>' +
					'</thead>' +
					'	<tbody>'+
							'<tr>' +
								'<th class="text-center text-middle" style="">Share of Box Office Revenue (%)</th>';
								 	$.each(response.projectDetails.yearWiseBoxOfficePercentage, function(k, v) {
								    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
									  });
							'</tr>';
			   content+='</tbody>';		
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Share of F&B Revenue (%)</th>';
					 	$.each(response.projectDetails.yearWiseFAndBPercentage, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
						  });
				'</tr></tbody>';
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Share of Ad Revenue (%)</th>';
					 	$.each(response.projectDetails.yearWiseAdSalePercentage, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
						  });
				'</tr></tbody>';
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Share of Other Revenue (%)</th>';
					 	$.each(response.projectDetails.yearWiseOthersPercentage, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
						  });
				'</tr></tbody>';
			   content+='</table></div>';
   		$('#annual-rent-table').append(content);
   		$('.loading').hide();
   		$('#annual-rent-modal').modal();	
}



function createMgRevenueShareTable(response){
	
	var content = '<h5>PAYMENT DETAILS</h5><table class="table table-bordered transparent" style="    margin-top: 15px;box-shadow: 0 0 0 2px #000;border: hidden;">'+
				   '     <tbody class="mb-2">'+
				   ' <tr>'+
				   '     <th width="160" rowspan="11" class="text-center text-middle" style="border: 0;border-right: 2px solid #000;">Rent</th>'+
				   '     <td class="text-left">Payment terms</td>'+
				   '     <td width="200" class="text-left">Max (MG, Revenue Share)</td>'+
				   ' </tr>'+
				   '  <tr>'+
				  '      <td class="text-left">Rent per sqr ft per month</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.rent+'</td>'+
				 '   </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Frequency of escalation (Fixed Rent) </td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.rentFrequencyOfEscaltionYears_fixed+' Years</td>'+
				 '   </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Percentage escalation (Fixed Rent)</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.percentageEscalationInRent_fixed+'%</td>'+
				 '   </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Box Office Percentage</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.boxOfficePercentage+'%</td>'+
				 '   </tr>'+
				  '  <tr>'+
				   '     <td class="text-left">F&B Percentage</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.fandBPercentage+'%</td>'+
				  '  </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Ad Sale Percentage</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.adSalePercentage+'%</td>'+
				  '  </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Others Percentage</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.othersPercentage+'%</td>'+
				  '  </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Frequency of escalation (Revenue Share)</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.freqOfEscalation+' Years</td>'+
				  '  </tr>'+
				  '  <tr>'+
				  '      <td class="text-left">Percentage of escalation (Revenue Share)</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.percOfEscalation+'%</td>'+
				  '  </tr>'+
				  '  <tr>'+
				   '     <td class="text-left">Other terms</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.explainRentAgreement+'</td>'+
				 '   </tr>'+
				'</tbody>'+
				'</table>';
	
			    content+='<h5>Year Wise Rent</h5>'+
				 '<div class="yearTableParent"><table class="table table-bordered transparent fixed-layout" style="margin-top:15px;">' +
			 		'<thead>' +
						'<tr>' +
							'<th class="text-center text-middle" style="">Year</th>';
							    $.each(response.projectDetails.yearWiseRent, function(k, v) {
							    	content+='<td class="text-center text-middle" width="">'+k+'</td>';
								  });
				  content+='</tr>' +
					'</thead>' +
					'	<tbody>'+
							'<tr>' +
								'<th class="text-center text-middle" style="">Share of Box Office Revenue (%)</th>';
								 	$.each(response.projectDetails.yearWiseBoxOfficePercentage, function(k, v) {
								    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
									  });
							'</tr>';
			   content+='</tbody>';		
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Share of F&B Revenue (%)</th>';
					 	$.each(response.projectDetails.yearWiseFAndBPercentage, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
						  });
				'</tr></tbody>';
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Share of Ad Revenue (%)</th>';
					 	$.each(response.projectDetails.yearWiseAdSalePercentage, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
						  });
				'</tr></tbody>';
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Share of Other Revenue (%)</th>';
					 	$.each(response.projectDetails.yearWiseOthersPercentage, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
						  });
				'</tr></tbody>';
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Fixed Rent (INR)</th>';
					 	$.each(response.projectDetails.yearWiseRent, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v)+'</td>';
						  });
				'</tr></tbody>';
			   content+='</table></div>';
   		$('#annual-rent-table').append(content);
   		$('.loading').hide();
   		$('#annual-rent-modal').modal();	
}


function createOther1Table(response){
	if(response.projectDetails.paymentTerm == "fixed"){
		var content = '<h5>PAYMENT DETAILS</h5><table class="table table-bordered transparent" style="    margin-top: 15px;box-shadow: 0 0 0 2px #000;border: hidden;">'+
		   '     <tbody class="mb-2">'+
		   ' <tr>'+
		   '     <th width="160" rowspan="13" class="text-center text-middle" style="border: 0;border-right: 2px solid #000;">Rent</th>'+
		   '     <td class="text-left">Payment terms</td>'+
		   '     <td width="200" class="text-left">Other 1</td>'+
		   ' </tr>'+
		   '  <tr>'+
		  ' <td class="text-left">Starting year for MG</td>'+
		  ' <td width="200" class="text-left">'+response.lesserLesse.startingYearForMG+'</td>'+
		 '  </tr>'+
		   '  <tr>'+
			  ' <td class="text-left">MG Type</td>'+
			  ' <td width="200" class="text-left">Fixed</td>'+
			 ' </tr>'+ 
		   '  <tr>'+
		  '      <td class="text-left">Rent per sqr ft per month</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.rent+'</td>'+
		 '   </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Frequency of escalation (Fixed Rent) </td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.rentFrequencyOfEscaltionYears_fixed+' Years</td>'+
		 '   </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Percentage escalation (Fixed Rent)</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.percentageEscalationInRent_fixed+'%</td>'+
		 '   </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Box Office Percentage</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.boxOfficePercentage+'%</td>'+
		 '   </tr>'+
		  '  <tr>'+
		   '     <td class="text-left">F&B Percentage</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.fandBPercentage+'%</td>'+
		  '  </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Ad Sale Percentage</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.adSalePercentage+'%</td>'+
		  '  </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Others Percentage</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.othersPercentage+'%</td>'+
		  '  </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Frequency of escalation (Revenue Share)</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.freqOfEscalation+' Years</td>'+
		  '  </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Percentage of escalation (Revenue Share)</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.percOfEscalation+'%</td>'+
		  '  </tr>'+
		  '  <tr>'+
		   '     <td class="text-left">Other terms</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.explainRentAgreement+'</td>'+
		 '   </tr>'+
		'</tbody>'+
		'</table>';
	}
	
	if(response.projectDetails.paymentTerm == "average"){
		var content = '<h5>PAYMENT DETAILS</h5><table class="table table-bordered transparent" style="    margin-top: 15px;box-shadow: 0 0 0 2px #000;border: hidden;">'+
		   '     <tbody class="mb-2">'+
		   ' <tr>'+
		   '     <th width="160" rowspan="10" class="text-center text-middle" style="border: 0;border-right: 2px solid #000;">Rent</th>'+
		   '     <td class="text-left">Payment terms</td>'+
		   '     <td width="200" class="text-left">Other 1</td>'+
		   ' </tr>'+
		   '  <tr>'+
		  ' <td class="text-left">Starting year for MG</td>'+
		  ' <td width="200" class="text-left">'+response.lesserLesse.startingYearForMG+'</td>'+
		 '  </tr>'+
		   '  <tr>'+
			  ' <td class="text-left">MG Type</td>'+
			  ' <td width="200" class="text-left">Average</td>'+
			 ' </tr>'+ 
		  '  <tr>'+
		  '      <td class="text-left">Box Office Percentage</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.boxOfficePercentage+'%</td>'+
		 '   </tr>'+
		  '  <tr>'+
		   '     <td class="text-left">F&B Percentage</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.fandBPercentage+'%</td>'+
		  '  </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Ad Sale Percentage</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.adSalePercentage+'%</td>'+
		  '  </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Others Percentage</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.othersPercentage+'%</td>'+
		  '  </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Frequency of escalation (Revenue Share)</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.freqOfEscalation+' Years</td>'+
		  '  </tr>'+
		  '  <tr>'+
		  '      <td class="text-left">Percentage of escalation (Revenue Share)</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.percOfEscalation+'%</td>'+
		  '  </tr>'+
		  '  <tr>'+
		   '     <td class="text-left">Other terms</td>'+
		  '      <td width="200" class="text-left">'+response.projectDetails.explainRentAgreement+'</td>'+
		 '   </tr>'+
		'</tbody>'+
		'</table>';
	}
	
   content+='<h5>Year Wise Rent</h5>'+
	 '<div class="yearTableParent"><table class="table table-bordered transparent fixed-layout" style="margin-top:15px;">' +
		'<thead>' +
			'<tr>' +
				'<th class="text-center text-middle" style="">Year</th>';
				    $.each(response.projectDetails.yearWiseRent, function(k, v) {
				    	content+='<td class="text-center text-middle" width="">'+k+'</td>';
					  });
	  content+='</tr>' +
		'</thead>' +
		'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Share of Box Office Revenue (%)</th>';
					 	$.each(response.projectDetails.yearWiseBoxOfficePercentage, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
						  });
				'</tr>';
	content+='</tbody>';		
	content+= 	'	<tbody>'+
	'<tr>' +
		'<th class="text-center text-middle" style="">Share of F&B Revenue (%)</th>';
		 	$.each(response.projectDetails.yearWiseFAndBPercentage, function(k, v) {
		    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
			  });
	'</tr></tbody>';
	content+= 	'	<tbody>'+
	'<tr>' +
		'<th class="text-center text-middle" style="">Share of Ad Revenue (%)</th>';
		 	$.each(response.projectDetails.yearWiseAdSalePercentage, function(k, v) {
		    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
			  });
	'</tr></tbody>';
	content+= 	'	<tbody>'+
	'<tr>' +
		'<th class="text-center text-middle" style="">Share of Other Revenue (%)</th>';
		 	$.each(response.projectDetails.yearWiseOthersPercentage, function(k, v) {
		    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
			  });
	'</tr></tbody>';
	content+= 	'	<tbody>'+
	'<tr>' +
		'<th class="text-center text-middle" style="">Fixed Rent (INR)</th>';
		 	$.each(response.projectDetails.yearWiseRent, function(k, v) {
		    	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v)+'</td>';
			  });
	'</tr></tbody></div>';
	content+='</table>';
	$('#annual-rent-table').append(content);
	$('.loading').hide();
	$('#annual-rent-modal').modal();	
}


function createOther2Table(response){
	
	var content = '<h5>PAYMENT DETAILS</h5><table class="table table-bordered transparent" style=" margin-top: 15px;box-shadow: 0 0 0 2px #000;border: hidden;">'+
				   '     <tbody class="mb-2">'+
				   ' <tr>'+
				   '     <th width="160" rowspan="11" class="text-center text-middle" style="border: 0;border-right: 2px solid #000;">Rent</th>'+
				   '     <td class="text-left">Payment terms</td>'+
				   '     <td width="200" class="text-left">Other 2</td>'+
				   ' </tr>'+
				  '  <tr>'+
				   '     <td class="text-left">Other terms</td>'+
				  '      <td width="200" class="text-left">'+response.projectDetails.explainRentAgreement+'</td>'+
				 '   </tr>'+
				'</tbody>'+
				'</table>';
	
			    content+='<h5>Year Wise Rent</h5>'+
				 '<div class="yearTableParent"><table class="table table-bordered transparent fixed-layout" style="margin-top:15px;">' +
			 		'<thead>' +
						'<tr>' +
							'<th class="text-center text-middle" style="">Year</th>';
							    $.each(response.projectDetails.yearWiseRent, function(k, v) {
							    	content+='<td class="text-center text-middle" width="">'+k+'</td>';
								  });
				  content+='</tr>' +
					'</thead>' +
					'	<tbody>'+
							'<tr>' +
								'<th class="text-center text-middle" style="">Share of Box Office Revenue (%)</th>';
								 	$.each(response.projectDetails.yearWiseBoxOfficePercentage, function(k, v) {
								    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
									  });
							'</tr>';
			   content+='</tbody>';		
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Share of F&B Revenue (%)</th>';
					 	$.each(response.projectDetails.yearWiseFAndBPercentage, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
						  });
				'</tr></tbody>';
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Share of Ad Revenue (%)</th>';
					 	$.each(response.projectDetails.yearWiseAdSalePercentage, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
						  });
				'</tr></tbody>';
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Share of Other Revenue (%)</th>';
					 	$.each(response.projectDetails.yearWiseOthersPercentage, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+v+'</td>';
						  });
				'</tr></tbody>';
				content+= 	'	<tbody>'+
				'<tr>' +
					'<th class="text-center text-middle" style="">Fixed Rent (INR)</th>';
					 	$.each(response.projectDetails.yearWiseRent, function(k, v) {
					    	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v)+'</td>';
						  });
				'</tr></tbody>';
			   content+='</table></div>';
   		$('#annual-rent-table').append(content);
   		$('.loading').hide();
   		$('#annual-rent-modal').modal();	
}
$('#view-calculation').click(function() {
	$('.loading').show();
	 $('#view-calculation-table').empty();
	var projectID =$('#id').attr('value');
	 console.log(projectID);
	 $.ajax({
         url: "/view-modal/getViewCalculation/"+projectID,
         type: 'GET',
         success: function(response) {
        	 console.log(response);
        var content = '<div class="yearTableParent"><table class="table table-bordered transparent dicrease-font-size" style="margin-bottom:20px">'+
         '        <tr>'+
        '     <th style="width:220px">Particulars (INR Lacs)</th>';
        $.each(response.leasePeriod, function(k, v) {
        	if(k<15)
        		content+='<th class="text-center text-middle" width=""> Yr-'+(k+1)+'</th>';
	  });
        content+= '</tr>'+
         '<tr>'+
           '<td  style="width:220px" class="text-left">Net Sale of Tickets </td>';
        	$.each(response.summary.netTicketSales, function(k, v) {
        		if(k<16)	
        		content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
       	  });
            content+=''+
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">Sale of F&amp;B </td>'+
           '';$.each(response.summary.netFnBSalesForEachYear, function(k, v) {
        	   if(k<16)
              	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
          	  });
               content+='</th>'+
         '</tr>'+
         '<tr >'+
           '  <td style="width:220px" class="text-left">Sponsorship Revenues </td>'+
           '';$.each(response.summary.adRevenueForEachYear, function(k, v) {
        	   if(k<16)
             	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
         	  });
              content+='</th>'+
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">VAS Income </td>'+
           '';$.each(response.summary.vasIncomeForEachYear, function(k, v) {
        	   if(k<16)
            	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
        	  });
             content+='</th>'+
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">Other Operating Income </td>'+
           '';$.each(response.summary.otherOperatingIncome, function(k, v) {
        	   if(k<16)
           	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
       	  });
            content+='</th>'+
         '</tr>'+
         '<tr class="total_row">'+
           '  <td style="width:220px" class="text-left">Total Revenues </td>'+
           '';$.each(response.summary.totalRevenueForEachYear, function(k, v) {
        	   if(k<16)
              	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
          	  });
               content+='</th>'+
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">Film Distributor’s Share </td>'+
           '';$.each(response.summary.filmDistributorShare, function(k, v) {
        	   if(k<16)
             	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
         	  });
              content+='</th>'+
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">Consumption of F&amp;B </td>'+
           '';$.each(response.summary.consumptionOfFnBForEachYear, function(k, v) {
        	   if(k<16)
            	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
        	  });
             content+='</th>'+
         '</tr>'+
         '<tr class="total_row">'+
           '  <td style="width:220px" class="text-left">Gross Margin </td>'+
           '';$.each(response.summary.grossMarginForEachYear, function(k, v) {
        	   if(k<16)
           	content+='<td class="text-center text-middle" width="">'+v.toFixed(2)+'</td>';
       	  });
            content+='</th>'+
         '</tr>'+
         '<tr class="total_row">'+
           '  <td style="width:220px" class="text-left">Gross Margin (%)</td>'+
           '';$.each(response.summary.grossMarginPercent, function(k, v) {
        	   if(k<16)
              	content+='<td class="text-center text-middle" width="">'+v+'</td>';
          	  });
               content+='</th>'+
             
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">Personnel Expenses </td>'+
           '';$.each(response.summary.personnelExpenseForEachYear, function(k, v) {
        	   if(k<16)
             	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
         	  });
              content+='</th>'+
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">Electricity &amp; water charges </td>'+
           '';$.each(response.summary.electricityAndWaterExpenseForEachYear, function(k, v) {
        	   if(k<16)
            	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
        	  });
             content+='</th>'+
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">Repairs and maintenance </td>'+
           '';$.each(response.summary.repairAndMaintenanceExpenseExpenseForEachYear, function(k, v) {
        	   if(k<16)
           	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
       	  });
            content+='</th>'+
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">Other Operating Expenses  </td>'+
           '';$.each(response.summary.otherOperatingExpensesForEachYear, function(k, v) {
        	   if(k<16)
              	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
          	  });
               content+='</th>'+
         '</tr>'+
         '<tr class="total_row">'+
           '  <td style="width:220px" class="text-left">Total Fixed Expenditure   </td>'+
           '';$.each(response.summary.expenditureExRentAndCamForEachYear, function(k, v) {
        	   if(k<16)
             	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
         	  });
              content+='</th>'+
         '</tr>'+
         '<tr class="total_row">'+
           '  <td style="width:220px" class="text-left">EBITDAR  </td>'+
           '';$.each(response.summary.ebitdarForEachYear, function(k, v) {
        	   if(k<16)
            	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
        	  });
             content+='</th>'+
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">Rent </td>'+
           '';$.each(response.summary.rentEachYear, function(k, v) {
        	   if(k<16)
           	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
       	  });
            content+='</th>'+
         '</tr>'+
         '<tr>'+
           '  <td style="width:220px" class="text-left">Common Area Maintenance  </td>'+
           '';$.each(response.summary.camEachYear, function(k, v) {
        	   if(k<16)
              	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
          	  });
               content+='</th>'+
         '</tr>'+     
         '<tr class="total_row">'+
           '  <td style="width:220px" class="text-left">EBITDA  </td>'+
           '';$.each(response.summary.getEBITDAForEachYear, function(k, v) {
        	   if(k<16)
             	content+='<td class="text-center text-middle" width="">'+formatCurrenry(v/100000)+'</td>';
         	  });
              content+='</th>'+
         '</tr>'+
         '<tr class="total_row">'+
           '  <td style="width:220px" class="text-left">EBITDA Margin (%)  </td>'+
           '';$.each(response.summary.getEBITDAMarginForEachYear, function(k, v) {
        	   if(k<16)
            	content+='<td class="text-center text-middle" width="">'+v+'</td>';
        	  });
             content+='</th>'+
         '</tr> '+
         '<tr class="total_row">'+
           '  <td style="width:220px" class="text-left">Rent/Revenue Ratio  (%)</td>'+
           '';$.each(response.summary.rentRationDetails, function(k, v) {
        	   if(k<16)
           	content+='<td class="text-center text-middle" width="">'+v+'</td>';
       	  });
            content+='</th>'+
         '</tr>'+
         '<tr class="total_row">'+
          '   <td style="width:220px" class="text-left">(Rent+CAM)/Revenue Ratio (%)</td>'+
          '';$.each(response.summary.rentCamRevenueRatioDetails, function(k, v) {
        	  if(k<16)
             	content+='<td class="text-center text-middle" width="">'+v+'</td>';
         	  });
              content+='</th>'+
        ' </tr>';
             	content += "</table></div>";
             	
             	 var content1 = '<div class="yearTableParent 2"><table class="table table-bordered transparent dicrease-font-size" style="margin-bottom:20px"><tbody>'+
             	 	'  <tr>'+
                 	'  <th style="width:220px">Particulars (INR Lacs)</th>'+
                	'  <td style="width:220px">Values</td>'+
                	'</tr>'+
                	'  <tr>'+
                 	'  <th>WACC</th>'+
                	'  <td style="width:220px">'+response.summary.wacc+'</td>'+
                	'</tr>'+
                	'  <tr>'+
                 	'  <th>Project IRRPreTax</th>'+
                	'  <td style="width:220px">'+response.summary.projectIRRPreTax+'</td>'+
                	'</tr>'+
                	'  <tr>'+
                 	'  <th>Equity Irr</th>'+
                	'  <td style="width:220px">'+response.summary.equityIRR+'</td>'+
                	'</tr>'+
                	'  <tr>'+
                 	'  <th>NPV</th>'+
                	'  <td style="width:220px">'+formatCurrenry(response.summary.npv)+'</td>'+
                	'</tr>';
                   	content1 += "</tbody></table></div>";
             	
             	
        		$('#view-calculation-table').append(content);
        		$('#view-calculation-table').append(content1);
        		 $('.loading').hide();
        		 $('#view-calculation-modal').modal();

         },
         error: function(xhr, status, error) {
        	 $('.loading').hide();
        	 console.log(xhr+"  "+ status+"  "+ error);
         }
     });
		
	
});

function nullConversion(data) {
    var response = data ? data : "NA";
    return response;
}

