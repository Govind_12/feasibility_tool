const ROLE_BD="ROLE_BD";
const ROLE_OPERATION_TEAM = "ROLE_OPERATION_TEAM";
const ROLE_PROJECT_TEAM  = "ROLE_PROJECT_TEAM";
const ROLE_APPROVER  = "ROLE_APPROVER";

const ACTION_ITEMS="action_items";
const UPDATES = "updates";
const APPROVAL_PENDING_FROM_YOU="has pending approval for you";
const PRE_SIGNING="Pre-Signing";
const POST_SIGNING="Post-Signing";
const PRE_HANDOVER="Pre-Handover";

const FEASIBILITY_PRE_SIGNING="pre_signing";
const FEASIBILITY_POST_SIGNING="post_signing";
const FEASIBILITY_PRE_HANDOVER="pre_handover";

const HREF_PRE_SIGNING="project/";
const HREF_POST_SIGNING="post-signing/";
const HREF_PRE_HANDOVER="pre-handover/";





/*function autoScroll_() {

	setTimeout(function(){
	$('.innerTableParent').animate({
	scrollTop: $('.innerTableParent').get(0).scrollHeight}, 0); 
	},2000);	
	}*/

$(document).ready(function() {

	$("#tab-B").on( "click", tabB(UPDATES) );
	$("#tab-A").on( "click", tabB(ACTION_ITEMS) );
	
	$(document).on('click', '.remove', function() {
		var projectId = this.id;
		$('#hiddenId').val(projectId);
		$("#removeProject").modal('show');

	});

	$(document).on('click', '#deleteProjectDetails', function() {
		window.location.href = '/dashboard/getDeletebyproject/' + $('#hiddenId').val();
	});

});

/*$(window).on('load', function(){
alert(	autoScroll());
});*/

$(function() {

	var table = $("#data-table").dataTable({
		"autoWidth": false,
		"order" : [],
		"deferRender" : true,
		"columnDefs" : [ {
			"targets" : 4,
			"orderable" : true,
		} ]

	});

});

var tabB=function(type){
	setNotificationData(type)
}

var tabA=function(type){
	setNotificationData(type)
}



var setNotificationData = function(type) {
	$.ajax({

		url : "dashboard/getNotifications/" + type,
		success : function(result) {
			/*console.log(JSON.stringify(result.data));*/
			setData(result.data,type);
			//autoScroll_();
		},
		error : function() {
			console.log('Error in setNotificationData() ajax request');
		}

	});
}

var setData = function(data, type) {
	dataToAppend = "";
	
	
	if(data.length > 0){
	$.each(
					data,
					function(index, value) {
						buttonDiv = "";
						msg= (type==ACTION_ITEMS) ?  value.msg : value.updates;
						
						var feasibilityStatus=(value.feasibilityState== FEASIBILITY_POST_SIGNING)  ? (POST_SIGNING+" ") : ((value.feasibilityState== FEASIBILITY_PRE_HANDOVER) ? (PRE_HANDOVER+" "): "");
						var _href=(value.feasibilityState== FEASIBILITY_POST_SIGNING)  ? (HREF_POST_SIGNING) : ((value.feasibilityState== FEASIBILITY_PRE_HANDOVER) ? (HREF_PRE_HANDOVER): HREF_PRE_SIGNING);
							
						if (value.projectStatus <= 104) {
							buttonDiv = ' <div class="btnParent float-right text-align-right"> '
									
								+' <a href="'+_href+'getProjectId/'+value.projectId+'"> <button class=" queryBtn" spellcheck="true">  '
								+ ' <img src="/images/editIconImg.png"> Edit  </button> </a>'
								
								+' <button onclick="deleteProjectWithNotification(\''+value.projectId+'\')" class=" queryBtn" spellcheck="true"> '
								+ ' <img src="/images/deleteIconImg.png"> Delete  </button>'
									 
								+' </div> ';

						} 
						if(value.feasibilityState== FEASIBILITY_PRE_HANDOVER && (value.role == ROLE_BD) && (value.msgMapper!=null)  && (value.msgMapper.msgByRole!=ROLE_BD) && (value.msgMapper.operationChat || value.msgMapper.projectChat))
						{
							buttonDiv = 
								 ' <div class="btnParent float-right text-align-right"> '
								 +'<a href="'+_href+'operating-assumptions/'+value.projectId+'" > <button class=" queryBtn" spellcheck="true"> '
								 +' <img src="/images/reviewIcon.png"> Review '
								 +' </button> </a>'
								 +' </div> ';
						}/*else if (value.projectStatus == 105) {

							

						}*/ else if (((value.feasibilityState== FEASIBILITY_PRE_SIGNING && value.projectStatus == 108) || (value.feasibilityState== FEASIBILITY_POST_SIGNING && value.projectStatus == 208) || (value.feasibilityState== FEASIBILITY_PRE_HANDOVER && value.projectStatus == 308)) && value.role == ROLE_BD ) {
							buttonDiv = ' <div class="btnParent float-right text-align-right">'
								+' <button class="queryBtn" spellcheck="true" onclick="archiveProjectWithNotification(\''+value.projectId+'\')">'
							+' <img src="/images/archiveIconImg.png"> Archive'
							+' </button>'
							+' <a href="'+_href+'getProjectId/'+value.projectId+'"><button class=" queryBtn" spellcheck="true">'
						+' <img src="/images/updateIconImg.png"> Update'
							+' </button></a>'
						+' </div> ';


						}
						
						else if((value.msgMapper!=null) && (value.role == ROLE_BD)  && (value.msgMapper.msgByRole!=ROLE_BD)){
							buttonDiv = ' <div class="btnParent float-right text-align-right"> '
						 +' <a href="'+_href+'summary-sheet/'+value.projectId+'" ><button class=" queryBtn" spellcheck="true"> '
						 +' <img src="/images/answerIconImg.png"> Answer '
						 +' </button> </a>'
						 +' </div> ';
							
						}
						 //new for approver in case of query answered by BD
						else if((value.msgMapper!=null) && (value.role == ROLE_APPROVER) && (value.msgMapper.msgByRole!= ROLE_APPROVER)){
							 buttonDiv = 
								 ' <div class="btnParent float-right text-align-right"> '
								 +'<a href="project/review/'+value.projectId+'" > <button class=" queryBtn" spellcheck="true"> '
								 +' <img src="/images/reviewIcon.png"> Review '
								 +' </button> </a>'
								 +' </div> '; 
								
						 }
						 
						 
						 if(APPROVAL_PENDING_FROM_YOU==msg){
							 buttonDiv = 
							 ' <div class="btnParent float-right text-align-right"> '
							 +'<a href="project/review/'+value.projectId+'" > <button class=" queryBtn" spellcheck="true"> '
							 +' <img src="/images/reviewIcon.png"> Review '
							 +' </button> </a>'
							 +' </div> ';
							 
						 }
						 
						 //remove button div for updates
						 buttonDiv= (type==ACTION_ITEMS) ?  buttonDiv : "";
						 
						 if(value.role == ROLE_BD)
							 dataToAppend = '<tr>'
								 + '<td colspan="5">'+feasibilityStatus+'Report <a href="'+_href+'getProjectId/'+value.projectId+'"> '
								 + value.reportId + ' </a> for '+value.projectName +' '+ msg
								 + buttonDiv + ' </td>' + '</tr>' + dataToAppend;
						 else if (value.role == ROLE_APPROVER){
							 
							//always same link for each module of approver
							 _href=HREF_PRE_SIGNING;
							 
							 dataToAppend = '<tr>'
								 + '<td colspan="5">'+feasibilityStatus+'Report <a href="'+_href+'review/'+value.projectId+'"> '
								 + value.reportId + ' </a> for '+value.projectName +' '+ msg
								 + buttonDiv + ' </td>' + '</tr>' + dataToAppend;
							 
						 }
						 
						 else if (value.role==ROLE_OPERATION_TEAM)
						 {
							 if(type==ACTION_ITEMS){
								 
								 buttonDiv = 
									 ' <div class="btnParent float-right text-align-right"> '
									 +'<a href="'+_href+'operating-assumptions/'+value.projectId+'" > <button class=" queryBtn" spellcheck="true"> '
									 +' <img src="/images/reviewIcon.png"> Review '
									 +' </button> </a>'
									 +' </div> ';
								 
								 if((value.msgMapper!=null)  && (value.msgMapper.msgByRole==ROLE_BD) && value.msgMapper.operationChat){
									
									 buttonDiv = ' <div class="btnParent float-right text-align-right"> '
										 +' <a href="'+_href+'operating-assumptions/'+value.projectId+'" ><button class=" queryBtn" spellcheck="true"> '
										 +' <img src="/images/answerIconImg.png"> Answer '
										 +' </button> </a>'
										 +' </div> ';
									 
								 }
							 }
							 
							 dataToAppend = '<tr>'
								 + '<td colspan="5">'+feasibilityStatus+'Report <a href="'+_href+'operating-assumptions/'+value.projectId+'"> '
								 + value.reportId + ' </a> for '+value.projectName +' '+ msg
								 +buttonDiv+ ' </td>' + '</tr>' + dataToAppend;
						 }
						 
						 else if (value.role == ROLE_PROJECT_TEAM){
							 
							 if(type==ACTION_ITEMS){
								 
								 buttonDiv = 
									 ' <div class="btnParent float-right text-align-right"> '
									 +'<a href="'+_href+'operating-assumptions/'+value.projectId+'" > <button class=" queryBtn" spellcheck="true"> '
									 +' <img src="/images/reviewIcon.png"> Review '
									 +' </button> </a>'
									 +' </div> ';
								 
								 if((value.msgMapper!=null)  && (value.msgMapper.msgByRole==ROLE_BD) && value.msgMapper.projectChat){
									 buttonDiv = ' <div class="btnParent float-right text-align-right"> '
										 +' <a href="'+_href+'operating-assumptions/'+value.projectId+'" ><button class=" queryBtn" spellcheck="true"> '
										 +' <img src="/images/answerIconImg.png"> Answer '
										 +' </button> </a>'
										 +' </div> ';
									 
								 }
							 }
							 
							 dataToAppend = '<tr>'
								 + '<td colspan="5">'+feasibilityStatus+'Report <a href="'+_href+'operating-assumptions/'+value.projectId+'"> '
								 + value.reportId + ' </a> for '+value.projectName +' '+ msg
								 +buttonDiv+ ' </td>' + '</tr>' + dataToAppend;
							 
						 }
						 
						 
						
					});
	
	}
	else
		{
			dataToAppend = '<tr>' 
				+ '<td colspan="5" class="no_data"> <i class="far fa-bell-slash"></i> <p>No Data Found</p> </td>' + '</tr>';
		}
	
	
	
	
	if (type==ACTION_ITEMS ) {
		$('#tableA tr').remove();
		$('#tableA tbody').append(dataToAppend);
		$('#actionItemsLabel').html(data.length);
		//autoScroll();
		
	}
	else if(type==UPDATES ) {
		$('#tableB tr').remove();
		$('#tableB tbody').append(dataToAppend);
				$('#updatesLabel').html(data.length);
		//autoScroll();
		
	}
	
		
}




var deleteProjectWithNotification = function(projectId) {
	
	if(confirm("Do you want to delete this Project ?"))
		{
		$.ajax({

			url : "project/deleteProjectWithNotification/" + projectId,
			async: false,
			success : function(result) {
				// refresh action items
				setNotificationData(ACTION_ITEMS);
				location.reload();
				
			},
			error : function(error,xhr, a) {
				console.log('Error in deleteProjectWithNotification() ajax request');
				}

			});
		}
}

var archiveProjectWithNotification=function(projectId){
	
	if(confirm("Do you want to archive this Project ?"))
	{
	$.ajax({

		url : "project/archiveProjectWithNotification/" + projectId,
		async: false,
		success : function(result) {
			// refresh action items
			setNotificationData(ACTION_ITEMS);
			location.reload();
			
		},
		error : function(error,xhr, a) {
			console.log('Error in archiveProject() ajax request');
		}

	});
	}
	
}


$('.downloadFullReportBtn').click(function() {
	$('.loading').show();
	var reportId = $("#id").val();
	$.ajax({
		url :"/report/generate-file/"+ reportId,
		success : function(response) {
			console.log(response);
			if(response.status==200){
				window.location.href="/report/files/"+reportId;
				$('.loading').hide();
			}
			$('.loading').hide();
		},
		error:function(xhr,status,error){
			console.log('Error ' +JSON.stringify(status)+ "Error : " + error )
			$('.loading').hide();
			}
	}); 
	
});





///project/review/5c2efcb93b722f14daf5ff07
