function check() {
	var opt-input = $('#opt-input').val();
	var screenValue = $('#screenValue').val();
	var workingdays = $('#workingdays').val();
	var yearOneFootfall = $('#yearOneFootfall').val();
	var complimentaryTickets = $('#complimentaryTickets').val();
	var complimentary1 = $('#complimentary1').val();
	var complimentaryf&b = $('#complimentaryf&b').val();
	var yearoneadincome = $('#yearoneadincome').val();
	var vpfoperator = $('#vpfoperator').val();
	var vpfvalue = $('#vpfvalue').val();
	var applicableyear = $('#applicableyear').val();
	var mainstream = $('#mainstream').val();
	var gold = $('#gold').val();
	var imax = $('#imax').val();
	var pxl = $('#pxl').val();
	var dx = $('#dx').val();
	var playhouse = $('#playhouse').val();
	var ulteraPremium = $('#ulteraPremium').val();
	var onyx = $('#onyx').val();
	var fnbGst = $('#fnbGst').val();
	var fnbCogs = $('#fnbCogs').val();
	var net = $('#net').val();
	
	if (!opt-input.match(/^[a-zA-Z0-9_]*$/)) 
	{
		return false;
		warningMsg("input should be Alphanumeric value or not be empty");
	}
	else if (!screenValue.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!workingdays.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!yearOneFootfall.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!complimentaryTickets.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!complimentary1.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!complimentaryf&b.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!yearoneadincome.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!vpfoperator.match(/^[a-zA-Z0-9_.<>]+\s(?:(?:<|<=|>|>=|==|!=)\s\d+|matches\s[a-zA-Z0-9_.<>|]+)$/)){
		return false;
	}
	else if (!vpfvalue.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!applicableyear.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!mainstream.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!gold.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!imax.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!pxl.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!dx.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!playhouse.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!ulteraPremium.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!onyx.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!fnbGst.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!fnbCogs.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else if (!net.match(/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/)){
		return false;
	}
	else {
		return true;
	     }

}

function warningMsg(msg){
	$('#opt-input').html(msg);
}

