$(document).ready(function() {

  $(document).on('click', '.remove', function() {
		var userId = this.id;
		$('#hiddenId').val(userId);
		$("#removeUser").modal('show');

	});

	$(document).on('click', '#deleteUserDetails', function() {
		window.location.href = '/user/delete/' + $('#hiddenId').val();
	});
	
	
});
