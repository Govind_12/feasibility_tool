package com.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

import com.services.LogServiceImpl;

@Service
public class CustomLogoutHandler implements LogoutHandler {
	
	@Autowired
	LogServiceImpl logService;

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response,
			org.springframework.security.core.Authentication authentication) {
		String username = authentication.getPrincipal().toString();
		logService.saveLogDetails(request, username, null);

	}
}