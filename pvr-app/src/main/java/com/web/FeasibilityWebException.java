package com.web;

public class FeasibilityWebException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public int code;
	
	public FeasibilityWebException() {
		super();
	}
	public FeasibilityWebException(String message, Throwable cause, boolean enableSuppression,
                               boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	public FeasibilityWebException(String message, Throwable cause) {
		super(message, cause);
	}
	public FeasibilityWebException(String message) {
		super(message);
	}
	public FeasibilityWebException(String message, int code) {
		super(message);
		this.code = code;
	}
	public FeasibilityWebException(Throwable cause) {
		super(cause);
	}
}
