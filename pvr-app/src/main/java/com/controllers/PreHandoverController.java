package com.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.common.constants.Constants;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.FixedConstant;
import com.common.models.IrrCalculationData;
import com.common.models.ProjectDetails;
import com.services.LogServiceImpl;
import com.services.PreHandoverService;
import com.services.ProjectDetailService;
import com.util.UpdateUrlUtil;
import com.util.UserRoleUtil;
import com.web.Response;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("pre-handover")
public class PreHandoverController {

	@Autowired
	private PreHandoverService preHandoverService;

	@Autowired
	private ProjectDetailService projectDetailService;

	@Autowired
	LogServiceImpl logService;

	@GetMapping("")
	public String preHandOver(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("projectList", preHandoverService.getAllExistingProjects());
		model.addAttribute("preactive", true);
		model.addAttribute("activeStep1", "active");
		model.addAttribute("disableHeader", true);
		return "preHandover/development-details";
	}

	// step -1

	@GetMapping("development-details/{projectId}")
	public String step1(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("projectList", preHandoverService.getAllExistingProjects());
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep1", "active");
		return "preHandover/development-details";
	}

	@PostMapping("development-details")
	public String saveAndNextStep1(@ModelAttribute("projectDetails") ProjectDetails projectDetails,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		if (!projectDetails.getIsRejected()) {
			projectDetails.setIsRejected(false);
			projectDetails.setIsSendForOpsAndProject(false);
			projectDetails.setIsProjectSubmit(false);
			projectDetails.setIsOperationSubmit(false);
		}
		ProjectDetails details = preHandoverService.savePreHandoverStep1(projectDetails);
		return "redirect:/pre-handover/developer-details/" + details.getId();
	}

	@PostMapping("development-details-save")
	public String saveAndCloseStep1(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		preHandoverService.savePreHandoverStep1(projectDetails);
		return "redirect:/dashboard";
	}

	@GetMapping("{projectId}")
	public String getProjectById(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);

		ProjectDetails details = projectDetailService.getProjectById(projectId);
		ProjectDetails latestProjectDetails = projectDetailService
				.getLatestProjectWithReportId(Constants.REPORT_TYPE_EXISTING, details.getReportId());
		details.setReportId(latestProjectDetails.getReportId());
		details.setReportIdVersion(latestProjectDetails.getReportIdVersion());
		details.setReportIdSubVersion(latestProjectDetails.getReportIdSubVersion());
		details.setStatus(0);
		details.setReportType(Constants.REPORT_TYPE_EXISTING);
		model.addAttribute("projectDetails", details);
		model.addAttribute("projectList", preHandoverService.getAllExistingProjects());
		model.addAttribute("activeStep1", "active");
		model.addAttribute("disableHeader", true);
		return "preHandover/development-details";
	}

	// step -2

	@GetMapping("developer-details/{projectId}")
	public String step2(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);

		model.addAttribute("activeStep2", "active");
		model.addAttribute("projectId", projectId);
		model.addAttribute("cinemaMasterDetails", projectDetailService.getAllCinemaMasterDetails());
		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);
		if (!ObjectUtils.isEmpty(projectDetails.getPvrProjectDelivered())) {
			projectDetails.setPvrProjectDeliveredFlag(true);
		}
		model.addAttribute("projectDetails", projectDetails);
		return "preHandover/developer-details";
	}

	@PostMapping("developer-details")
	public String saveAndNextStep2(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep2(projectDetails);
		return "redirect:/pre-handover/upcoming-cinemas/" + details.getId();
	}

	@PostMapping("developer-details-save")
	public String saveAndCloseStep2(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.saveStep2(projectDetails);
		return "redirect:/dashboard";
	}

	// step -3

	@GetMapping("upcoming-cinemas/{projectId}")
	public String step3(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectDetails", details);
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep3", "active");
		return "preHandover/upcoming-cinemas";
	}

	@PostMapping("upcoming-cinemas")
	public String saveAndNextStep3(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep3(projectDetails);
		return "redirect:/pre-handover/lessor-lessee-schedule/" + details.getId();
	}

	@PostMapping("upcoming-cinemas-save")
	public String saveAndCloseStep3(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.saveStep3(projectDetails);
		return "redirect:/dashboard";
	}

	// step -4

	@GetMapping("lessor-lessee-schedule/{projectId}")
	public String step4(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectDetails", details);
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep4", "active");
		return "preHandover/lessor-lessee-schedule";
	}

	@PostMapping("lessor-lessee-schedule")
	public String saveAndNextStep4(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep4(projectDetails);
		return "redirect:/pre-handover/project-layout-assumptions/" + details.getId();
	}

	@PostMapping("lessor-lessee-schedule-save")
	public String saveAndCloseStep4(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.saveStep4(projectDetails);
		return "redirect:/dashboard";
	}

	// step -5

	@GetMapping({ "project-layout-assumptions/{projectId}" })
	public String step5(Model model, @PathVariable("projectId") String projectId) {
		ProjectDetails detailor = this.projectDetailService.getProjectById(projectId);
		List<String> splitor = detailor.getMultipleFileURLStep5();
		model.addAttribute("filor", splitor);
		model.addAttribute("preactive", Boolean.valueOf(true));
		model.addAttribute("activeStep5", "active");
		model.addAttribute("projectId", projectId);
		model.addAttribute("projectDetails", detailor);
		return "preHandover/project-layout-assumptions";
	}

	@PostMapping({ "project-layout-assumptions" })
	public String saveAndNextStep5(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			@RequestParam(name = "files[]", required = false) List<MultipartFile> files,
			@RequestParam(name = "oldfile[]", required = false) List<String> oldfiles) {
		ProjectDetails details = this.projectDetailService.saveStep5(projectDetails, files, oldfiles);
		return "redirect:/dashboard";
	}

	@PostMapping({ "project-layout-assumptions-save" })
	public String saveAndCloseStep5(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			@RequestParam(name = "files[]", required = false) List<MultipartFile> files,
			@RequestParam(name = "oldfile[]", required = false) List<String> oldfiles) {
		this.projectDetailService.saveandCloseStep5(projectDetails, files, oldfiles);
		return "redirect:/dashboard";
	}

	// project-layout-assumptions-save
	// operating-assumptions

	// step -6

	@GetMapping("operating-assumptions/{projectId}")
	public String step6(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep6", "active");
		if (UserRoleUtil.isCurrentUserBD()) {
			model.addAttribute("content", true);
			model.addAttribute("contentProject", true);

			ProjectDetails project = projectDetailService.getProjectById(projectId);
			if (project.getStatus() < 105 || project.getStatus() == Constants.PRE_HAND_STATUS_REJECTED) {
				model.addAttribute("isSendForOpsANdProjectButton", true);
			} else {
				model.addAttribute("isSendForOpsANdProjectButton", false);
			}
		}
		if (UserRoleUtil.isCurrentUserOperation()) {
			model.addAttribute("content", false);
			model.addAttribute("contentProject", true);
		}
		if (UserRoleUtil.isCurrentUserProjectTeam()) {
			model.addAttribute("content", true);
			model.addAttribute("contentProject", false);
		}
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		return "preHandover/operating-assumptions";
	}

	@PostMapping("operating-assumptions")
	public String saveAndNextStep6(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep6(projectDetails);
		return "redirect:/pre-handover/summary-sheet/" + details.getId();
	}

	@PostMapping("operating-assumptions-save")
	public String saveAndCloseStep6(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		if (projectDetailService.isCurrentUserHasOpeartionRole()) {
			projectDetails.setAllowSaveAndClose(true);
		}
		projectDetailService.saveStep6(projectDetails);
		return "redirect:/dashboard";
	}

	@PostMapping("operating-assumptions-submit")
	public String saveAndSubmitStep6(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.saveStep6(projectDetails);
		return "redirect:/dashboard";
	}

	// step -7
	@GetMapping("summary-sheet/{projectId}")
	public String step7(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep7", "active");
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		return "preHandover/summary-sheet";
	}

	@PostMapping(value = "summary-sheet")
	private String submitForApproval(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetails.setIsRejected(false);
		projectDetailService.saveStep7(projectDetails);
		return "redirect:/dashboard";
	}

	@GetMapping("getProjectId/{projectId}")
	public String getUserEmployeeDetailsById2(@PathVariable("projectId") String projectId, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching project details by user id for pre handover");
		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectDetails", projectDetails);
		String url = UpdateUrlUtil.getUrl(projectDetails.getFeasibilityState(), projectDetails.getStatus(),
				projectDetails.getId());
		if (UserRoleUtil.isCurrentUserBD() && projectDetails.getStatus() == Constants.STEP_5
				&& (projectDetails.getIsProjectSubmit() && projectDetails.getIsOperationSubmit())) {
			url = Constants.OPERATING_ASSUMPTION_POST_SINGING + "/" + projectId;
		}
		return "redirect:/" + "pre-handover/" + url;
	}

	@GetMapping("getProjectDetailsById/{projectId}")
	@ResponseBody
	public ResponseEntity<Response<ProjectDetails>> getProjectById(@PathVariable("projectId") String projectId,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("Post Signing Controller get query");
		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);
		return new ResponseEntity<Response<ProjectDetails>>(
				new Response<ProjectDetails>(HttpStatus.OK.value(), "Project Fetched successfully", projectDetails),
				HttpStatus.OK);

	}

	@PostMapping("save-query")
	public @ResponseBody QueryMapper submitQuery(@ModelAttribute MsgMapper msgMapper, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("Post Signing Controller submit query");
		return preHandoverService.submitQuery(msgMapper);

	}

	@PostMapping("save-query-finalsummary")
	public @ResponseBody QueryMapper submitFinalQuery(@ModelAttribute MsgMapper msgMapper, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("Post Signing COntroller submit query");
		return preHandoverService.submitFinalQuery(msgMapper);

	}

	@GetMapping("update/{projectId}")
	public String updateProject(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.updateProject(projectId);
		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectDetails", projectDetails);
		String url = UpdateUrlUtil.getUrl(projectDetails.getFeasibilityState(), projectDetails.getStatus(),
				projectDetails.getId());

		return "redirect:/" + "pre-handover/" + url;

	}

	@PostMapping("send-for-operation")
	@ResponseBody
	public void submitForOperation(@ModelAttribute MsgMapper msgMapper, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("submit for  project");
		msgMapper.setIsProjectOrOperation(Constants.OPERATION);
		preHandoverService.submitForOperationOrProjectTeam(msgMapper);
	}

	@PostMapping("send-for-project")
	@ResponseBody
	public void submitForProject(@ModelAttribute MsgMapper msgMapper, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("submit for opearion");
		msgMapper.setIsProjectOrOperation(Constants.PROJECT);
		preHandoverService.submitForOperationOrProjectTeam(msgMapper);
	}

	@GetMapping("archiveProjectWithDashBoard/{projectId}")
	public String archiveProjectWithDashBoard(@PathVariable("projectId") String projectId, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.updateProjectWithNotification(projectId, Constants.PRE_HAND_STATUS_ARCHIVED);
		return "redirect:/dashboard";
	}

	@GetMapping("updateNotificationStatus/{projectId}/{queryType}/{step}")
	@ResponseBody
	public boolean updateNotificationStatus(@PathVariable("projectId") String projectId,
			@PathVariable("queryType") String queryType, @PathVariable("step") String step,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("PreHandover updateNotificationStatus");
		return preHandoverService.updateNotificationStatus(projectId, queryType, step);
	}

	@PostMapping({ "project-layout-assumptions-next" })
	public String saveAndNextStep(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			@RequestParam(name = "files[]", required = false) List<MultipartFile> files,
			@RequestParam(name = "oldfile[]", required = false) List<String> oldfiles) {
		ProjectDetails details = this.projectDetailService.saveStep5(projectDetails, files, oldfiles);
		return "redirect:/pre-handover/operating-assumptions/" + details.getId();
	}
	
	@GetMapping("operating-assumptions/getRevenueAssumptionConstant/{projectId}")
	@ResponseBody
	public FixedConstant getRevenueAssumptionConstant(Model model, @PathVariable("projectId") String projectId) {
		FixedConstant fixedConstant = projectDetailService.getRevenueAssumptionConstant(projectId);
		return fixedConstant;
	}
	
	@PostMapping(value = "/save-executive-note")
	@ResponseBody
	public ProjectDetails saveExecutiveNote(ProjectDetails projectDetails, HttpServletRequest request, Model model) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving project details");
	    return projectDetailService.saveExecutiveNote(projectDetails);
	}
	
	@RequestMapping({"get-pandl-tab/{projectId}"})
	public String getPAndLTabData(@PathVariable("projectId") String projectId,Model model) {
		IrrCalculationData data = projectDetailService.getPAndLTabData(projectId);
		ProjectDetails project = projectDetailService.getProjectById(projectId);

		model.addAttribute("activeStep8", "active");
	    model.addAttribute("pandlDetails", data);	    
		model.addAttribute("projectDetails", project);
		return "preHandover/pAndLTab";
	}
	
//	@GetMapping("summaryTab/{projectId}")
//	public String step8(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
//		logService.saveLogDetails(request, "Call recieved", null);
//		model.addAttribute("projectId", projectId);
//		model.addAttribute("activeStep8", "active");
//		if (UserRoleUtil.isCurrentUserBD()) {
//			model.addAttribute("content", true);
//			model.addAttribute("contentProject", true);
//
//			ProjectDetails project = projectDetailService.getProjectById(projectId);
//			if (project.getStatus() < 105 || project.getStatus() == Constants.PRE_HAND_STATUS_REJECTED) {
//				model.addAttribute("isSendForOpsANdProjectButton", true);
//			} else {
//				model.addAttribute("isSendForOpsANdProjectButton", false);
//			}
//		}
//		if (UserRoleUtil.isCurrentUserOperation()) {
//			model.addAttribute("content", false);
//			model.addAttribute("contentProject", true);
//		}
//		if (UserRoleUtil.isCurrentUserProjectTeam()) {
//			model.addAttribute("content", true);
//			model.addAttribute("contentProject", false);
//		}
//		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
//		return "preHandover/summaryTab";
//	}
}
