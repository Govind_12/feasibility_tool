package com.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.common.models.User;
import com.services.LogServiceImpl;
import com.services.UserService;
import com.util.UserRoleUtil;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping(value = "user")
@Slf4j
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	LogServiceImpl logService;

	@GetMapping("create")
	public String userCreate(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching list of all user");
		// model.addAttribute("companies", companyService.getAllCompaines());
		model.addAttribute("title", "Create");
		// model.addAttribute("approver", userService.getAllApprover());
		List<String> designationList = userService.getDesignationList();

		model.addAttribute("designationList", designationList);
		return "user/create";
	}

	@GetMapping("")
	public String usersList(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);

		model.addAttribute("userListActive", true);
		model.addAttribute("setting", true);
		model.addAttribute("title", "User");
		model.addAttribute("users", userService.getAllUsers());
		return "user/view";
	}

	@PostMapping(value = "save")
	public String saveUser(@ModelAttribute("user") User user, Model model, RedirectAttributes redirectAttributes,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving user details");

		boolean flag = user.getId().equals("") ? false : true;
		user.setId(user.getId().equals("") ? null : user.getId());
		try {
			user = userService.saveUser(user, "N");

			if (user.getUserNameExist()) {

				if (!ObjectUtils.isEmpty(user.getId())) {
					redirectAttributes.addFlashAttribute("error", "Username " + user.getUsername() + " already exist");
					redirectAttributes.addFlashAttribute("title", "Update");
					redirectAttributes.addFlashAttribute("disabledAuthorities", true);
					redirectAttributes.addFlashAttribute("user", user);
					return "redirect:/user/update/" + user.getId();
				}

				redirectAttributes.addFlashAttribute("error", "Username " + user.getUsername() + " already exist");
				redirectAttributes.addFlashAttribute("title", "User");
				redirectAttributes.addFlashAttribute("user", user);
				return "redirect:/user/create";
			} else
				redirectAttributes.addFlashAttribute("msg",
						flag ? "User updated successfully" : "User saved successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}

		redirectAttributes.addFlashAttribute("title", "User");
		return "redirect:/user";
	}

	@PostMapping(value = "uploadProfile")
	public @ResponseBody String uploadProfileUser(@ModelAttribute("user") User user, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving user details");
		model.addAttribute("user", userService.saveUser(user, "N"));
		return "user/create";
	}

	@GetMapping("get-user/{userName}")
	public @ResponseBody Map<String, Object> getUserByUserName(@PathVariable("userName") String userName,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching user by user name using ajax.");
		return userService.isUserExistByUserName(userName);
	}

	@GetMapping("userDetails/{userId}")
	public String getUserEmployeeDetailsById(@PathVariable("userId") String userId,
			@RequestParam(value = "title", required = false) String title, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching user details by user id");
		model.addAttribute("users", userService.getUserEmployeeDetailsById(userId));
		if (!ObjectUtils.isEmpty(title))
			model.addAttribute("title", title);
		return "user/view";
	}

	@GetMapping(value = "update/{id}")
	public String updateDevice(@PathVariable("id") String id, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		User user = userService.getUserById(id);
		// List<ApproverMapper> approver=userService.getAllApprover();
		/*
		 * if(ObjectUtils.isEmpty(approver)) { user.setApproverIsDisabled(false); }else
		 * { user.setApproverIsDisabled(true); }
		 */
		model.addAttribute("user", user);
		// System.out.println(userService.getUserById(id));
		model.addAttribute("disabledAuthorities", true);
		model.addAttribute("title", "Update");
		return "user/create";
	}

	@GetMapping(value = "delete/{id}")
	public String deleteUser(@PathVariable("id") String id, Model model, RedirectAttributes redirectAttributes,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		User user = userService.getUserById(id);
		user.setStatus(0);
		user.setPreHandOverApproverType(-1);
		user.setApproverType(-1);
		user = userService.saveUser(user, "Y");

		redirectAttributes.addFlashAttribute("msg", "User deleted successfully");

		redirectAttributes.addFlashAttribute("title", "User");
		return "redirect:/user";
	}

	@GetMapping("getProfileById")
	public @ResponseBody User getProfileById(HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching user by user id");
		String userId = UserRoleUtil.getCurrentUserId();
		return userService.getUserById(userId);
	}

	@PostMapping(value = "updateUserProfile")
	public String updateUserProfile(@ModelAttribute("user") User user, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving user details");

		user.setId(UserRoleUtil.getCurrentUserId());
		boolean flag = user.getId().equals("") ? false : true;
		user.setId(user.getId().equals("") ? null : user.getId());
		// user.setFullName(user.getFirstName()+" "+user.getLastName());
		user = userService.saveUser(user, "N");
		redirectAttributes.addFlashAttribute("msg", flag ? "User updated successfully" : "User saved successfully");

		return "redirect:/user";
	}

}
