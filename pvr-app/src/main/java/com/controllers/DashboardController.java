package com.controllers;

import java.io.IOException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.common.models.Notification;
import com.common.models.ProjectDetails;
import com.common.models.User;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.services.DashboardService;
import com.services.LogServiceImpl;
import com.services.UserService;
import com.web.Response;
import com.common.models.BDData;
import com.fasterxml.jackson.core.type.TypeReference;

@Controller
@RequestMapping("dashboard")
public class DashboardController {

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	LogServiceImpl logService;
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "")
	public String defaultDashboard(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		Map<String, Object> dashboardDetails = dashboardService.getDashoradDetails();
		dashboardService.getDashboardData(dashboardDetails, model);
		return "index";
	}

	@RequestMapping(value = "getNotifications/{type}")
	public @ResponseBody ResponseEntity<Response<List<Notification>>> getNotifications(Model model,
			@PathVariable String type, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		List<Notification> notificationList = dashboardService.getNotifications(type);
		return new ResponseEntity<Response<List<Notification>>>(new Response<List<Notification>>(HttpStatus.OK.value(),
				"successfully fetched notification list", notificationList), HttpStatus.OK);
	}

	@RequestMapping(value = "getDeletebyproject/{projectId}")
	public String deleteProjectbyId(@PathVariable("projectId") String projectId, HttpServletRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		logService.saveLogDetails(request, "Call recieved", null);
		dashboardService.deletebyProjectById(projectId);
		return "redirect:/dashboard";
	}

	@RequestMapping({ "getbdreport/{userid}" })
	public ModelAndView defaultDashboard2(Model model, @PathVariable String userid) throws Exception {
		ModelAndView mv = new ModelAndView("DataSource/bdsetting");
		List<User> usr = (List<User>) userService.getAllUsers().stream()
				.filter(article -> article.getAuthorities().contains("ROLE_BD")).collect(Collectors.toList());
		model.addAttribute("bduser", usr);
		Map<String, List<ProjectDetails>> dashboardDetails = this.dashboardService.getDashoradDetails2(userid);
		this.dashboardService.getDashboardData2(dashboardDetails, mv);
		return mv;
	}

	@RequestMapping({ "bdassign" })
	public String getbdassign(Model model) {
		List<User> usr = (List<User>) userService.getAllUsers().stream()
				.filter(article -> article.getAuthorities().contains("ROLE_BD")).collect(Collectors.toList());
		model.addAttribute("bduser", usr);
		return "DataSource/bdsetting";
	}

	@PostMapping({ "setbdreport" })
	public String setdbassign(@RequestParam String from, @RequestParam String to,
			@RequestParam("ids[]") List<Integer> ids, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		BDData bdData = new BDData();
		bdData.setFrom(from);
		bdData.setTo(to);
		bdData.setIds(ids);
		this.dashboardService.setReport(bdData);
		return "DataSource/bdsetting";
	}
}
