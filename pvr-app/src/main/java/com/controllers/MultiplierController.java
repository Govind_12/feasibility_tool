package com.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.common.models.MultipliersMaster;
import com.services.LogServiceImpl;
import com.services.MultiplierService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("multiplier")
@Slf4j
public class MultiplierController {

	@Autowired
	private MultiplierService multiplierService;

	@Autowired
	LogServiceImpl logService;

	@GetMapping("")
	public String multiplierList(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching list of all multiplier");
		model.addAttribute("title", "multipliers");
		model.addAttribute("multipliers", multiplierService.getAllMultipliers());
		return "multiplier/view";
	}

	@GetMapping("create")
	public String create(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("title", "Create");
		return "multiplier/create";
	}

	@PostMapping(value = "save")
	public String saveMutiplier(@ModelAttribute("multiplier") MultipliersMaster multiplier, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);

		if (!ObjectUtils.isEmpty(multiplier.getId())) {
			multiplierService.saveMultiplier(multiplier);
			redirectAttributes.addFlashAttribute("msg", "Multiplier saved successfully");
			return "redirect:/multiplier";
		} else {
			boolean result = multiplierService.isMultiplierPresent(multiplier);
			if (result) {
				multiplier.setCostType(null);
				redirectAttributes.addFlashAttribute("multiplier", multiplier);
				redirectAttributes.addFlashAttribute("error", multiplier.getCostTypeText() + " already exists.");
				return "redirect:/multiplier/create";
			}
			multiplierService.saveMultiplier(multiplier);
			redirectAttributes.addFlashAttribute("msg", "Multiplier saved successfully");
			return "redirect:/multiplier";
		}

	}

	@GetMapping(value = "update/{id}")
	public String updateCompany(@PathVariable("id") String id, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("title", "Update");
		model.addAttribute("multiplier", multiplierService.getMultiplierById(id));
		return "multiplier/create";
	}
}
