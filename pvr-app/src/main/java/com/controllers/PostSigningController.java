package com.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.common.constants.Constants;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.FixedConstant;
import com.common.models.IrrCalculationData;
import com.common.models.ProjectDetails;
import com.services.LogServiceImpl;
import com.services.PostSigningService;
import com.services.ProjectDetailService;
import com.util.UpdateUrlUtil;
import com.web.Response;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("post-signing")
public class PostSigningController {

	@Autowired
	private PostSigningService postSigningService;

	@Autowired
	private ProjectDetailService projectDetailService;

	@Autowired
	LogServiceImpl logService;

	@GetMapping("")
	public String postSigning(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("projectList", postSigningService.getAllExistingProjects());
		model.addAttribute("postactive", true);
		model.addAttribute("activeStep1", "active");
		model.addAttribute("disableHeader", true);
		return "postSigning/development-details";
	}

	// step -1

	@GetMapping("development-details/{projectId}")
	public String step1(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("projectList", postSigningService.getAllExistingProjects());
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep1", "active");
		return "postSigning/development-details";
	}

	@PostMapping("development-details")
	public String saveAndNextStep1(@ModelAttribute("projectDetails") ProjectDetails projectDetails,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = postSigningService.savePostSigningStep1(projectDetails);
		return "redirect:/post-signing/developer-details/" + details.getId();
	}

	@PostMapping("development-details-save")
	public String saveAndCloseStep1(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		postSigningService.savePostSigningStep1(projectDetails);
		return "redirect:/dashboard";
	}

	@GetMapping("{projectId}")
	public String getProjectById(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);

		ProjectDetails details = projectDetailService.getProjectById(projectId);
		ProjectDetails latestProjectDetails = projectDetailService
				.getLatestProjectWithReportId(Constants.REPORT_TYPE_EXISTING, details.getReportId());
		details.setReportId(latestProjectDetails.getReportId());
		details.setReportIdVersion(latestProjectDetails.getReportIdVersion());
		details.setReportIdSubVersion(latestProjectDetails.getReportIdSubVersion());
		details.setStatus(0);
		details.setReportType(Constants.REPORT_TYPE_EXISTING);
		model.addAttribute("projectDetails", details);
		model.addAttribute("projectList", postSigningService.getAllExistingProjects());
		model.addAttribute("activeStep1", "active");
		model.addAttribute("disableHeader", true);
		return "postSigning/development-details";
	}

	// step -2

	@GetMapping("developer-details/{projectId}")
	public String step2(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep2", "active");
		model.addAttribute("cinemaMasterDetails", projectDetailService.getAllCinemaMasterDetails());
		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);
		if (!ObjectUtils.isEmpty(projectDetails.getPvrProjectDelivered())) {
			projectDetails.setPvrProjectDeliveredFlag(true);
		}
		model.addAttribute("projectDetails", projectDetails);
		return "postSigning/developer-details";
	}

	@PostMapping("developer-details")
	public String saveAndNextStep2(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep2(projectDetails);
		return "redirect:/post-signing/upcoming-cinemas/" + details.getId();
	}

	@PostMapping("developer-details-save")
	public String saveAndCloseStep2(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.saveStep2(projectDetails);
		return "redirect:/dashboard";
	}

	// step -3

	@GetMapping("upcoming-cinemas/{projectId}")
	public String step3(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectDetails", details);
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep3", "active");
		return "postSigning/upcoming-cinemas";
	}

	@PostMapping("upcoming-cinemas")
	public String saveAndNextStep3(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails) {
		ProjectDetails details = projectDetailService.saveStep3(projectDetails);
		return "redirect:/post-signing/lessor-lessee-schedule/" + details.getId();
	}

	@PostMapping("upcoming-cinemas-save")
	public String saveAndCloseStep3(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails) {
		projectDetailService.saveStep3(projectDetails);
		return "redirect:/dashboard";
	}

	// step -4

	@GetMapping("lessor-lessee-schedule/{projectId}")
	public String step4(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectDetails", details);
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep4", "active");
		return "postSigning/lessor-lessee-schedule";
	}

	@PostMapping("lessor-lessee-schedule")
	public String saveAndNextStep4(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep4(projectDetails);
		return "redirect:/post-signing/project-layout-assumptions/" + details.getId();
	}

	@PostMapping("lessor-lessee-schedule-save")
	public String saveAndCloseStep4(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.saveStep4(projectDetails);
		return "redirect:/dashboard";
	}

	// step -5

	@GetMapping({ "project-layout-assumptions/{projectId}" })
	public String step5(Model model, @PathVariable("projectId") String projectId) {
		ProjectDetails detailor = this.projectDetailService.getProjectById(projectId);
		List<String> splitor = detailor.getMultipleFileURLStep5();
		model.addAttribute("filor", splitor);
		model.addAttribute("activeStep5", "active");
		model.addAttribute("projectId", projectId);
		model.addAttribute("projectDetails", detailor);
		return "postSigning/project-layout-assumptions";
	}

	@PostMapping({ "project-layout-assumptions" })
	public String saveAndNextStep5(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			@RequestParam(name = "files[]", required = false) List<MultipartFile> files,
			@RequestParam(name = "oldfile[]", required = false) List<String> oldfiles) {
		ProjectDetails details = this.projectDetailService.saveStep5(projectDetails, files, oldfiles);
		return "redirect:/post-signing/operating-assumptions/" + details.getId();
	}

	@PostMapping({ "project-layout-assumptions-save" })
	public String saveAndCloseStep5(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			@RequestParam(name = "files[]", required = false) List<MultipartFile> files,
			@RequestParam(name = "oldfile[]", required = false) List<String> oldfiles) {
		this.projectDetailService.saveandCloseStep5(projectDetails, files, oldfiles);
		return "redirect:/dashboard";
	}

	// step -6

	@GetMapping("operating-assumptions/{projectId}")
	public String step6(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep6", "active");
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		return "postSigning/operating-assumptions";
	}

	@PostMapping("operating-assumptions")
	public String saveAndCloseStep6(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep6(projectDetails);
		return "redirect:/post-signing/summary-sheet/" + details.getId();
	}

	@PostMapping("operating-assumptions-save")
	public String saveAndNextStep6(Model model, @ModelAttribute("projectDetails") ProjectDetails projectDetails,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep6(projectDetails);
		return "redirect:/dashboard";

	}

	// step -7

	@GetMapping("summary-sheet/{projectId}")
	public String step7(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("projectId", projectId);
		model.addAttribute("activeStep7", "active");
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		return "postSigning/summary-sheet";
	}

	@PostMapping(value = "summary-sheet")
	private String submitForApproval(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.saveStep7(projectDetails);
		return "redirect:/dashboard";
	}

	@GetMapping("getProjectId/{projectId}")
	public String getUserEmployeeDetailsById2(@PathVariable("projectId") String projectId, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching project details by user id for post sign");
		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectDetails", projectDetails);
		String url = UpdateUrlUtil.getUrl(projectDetails.getFeasibilityState(), projectDetails.getStatus(),
				projectDetails.getId());
		return "redirect:/" + "post-signing/" + url;

	}

	@PostMapping("send-operation-projectTeam")
	public String sendOperationAndProjectTeam(Model model,
			@ModelAttribute("projectDetails") ProjectDetails projectDetails, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("sending for project-team and operation-team");
		projectDetails.setReportStatus(true);
		projectDetailService.submitOeratingAssumptionforOperationAndOperationTeam(projectDetails);
		return "redirect:/dashboard";

	}

	@GetMapping("getProjectDetailsById/{projectId}")
	@ResponseBody
	public ResponseEntity<Response<ProjectDetails>> getProjectById(@PathVariable("projectId") String projectId,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("Post Signing Controller get query");
		ProjectDetails projectDetails = postSigningService.getProjectById(projectId);
		return new ResponseEntity<Response<ProjectDetails>>(
				new Response<ProjectDetails>(HttpStatus.OK.value(), "Project Fetched successfully", projectDetails),
				HttpStatus.OK);

	}

	@PostMapping("save-query")
	public @ResponseBody QueryMapper submitQuery(@ModelAttribute MsgMapper msgMapper, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("Post Signing COntroller submit query");
		return postSigningService.submitQuery(msgMapper);
	}

	@GetMapping("update/{projectId}")
	public String updateProject(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.updateProject(projectId);
		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectDetails", projectDetails);
		String url = UpdateUrlUtil.getUrl(projectDetails.getFeasibilityState(), projectDetails.getStatus(),
				projectDetails.getId());
		return "redirect:/" + "post-signing/" + url;
	}

	@GetMapping("archiveProjectWithDashBoard/{projectId}")
	public String archiveProjectWithDashBoard(@PathVariable("projectId") String projectId, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.updateProjectWithNotification(projectId, Constants.POST_STATUS_ARCHIVED);
		return "redirect:/dashboard";
	}
	
	@GetMapping("operating-assumptions/getRevenueAssumptionConstant/{projectId}")
	@ResponseBody
	public FixedConstant getRevenueAssumptionConstant(Model model, @PathVariable("projectId") String projectId) {
		FixedConstant fixedConstant = projectDetailService.getRevenueAssumptionConstant(projectId);
		return fixedConstant;
	}
	
	@PostMapping(value = "/save-executive-note")
	@ResponseBody
	public ProjectDetails saveExecutiveNote(ProjectDetails projectDetails, HttpServletRequest request, Model model) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving project details");
	    return projectDetailService.saveExecutiveNote(projectDetails);
	}
	@RequestMapping({"get-pandl-tab/{projectId}"})
	public String getPAndLTabData(@PathVariable("projectId") String projectId,Model model) {
		IrrCalculationData data = projectDetailService.getPAndLTabData(projectId);
		model.addAttribute("activeStep8", "active");
	    model.addAttribute("pandlDetails", data);
		return "postSigning/pAndLTab";
	}
	
//	@GetMapping("summaryTab/{projectId}")
//	public String step8(Model model, @PathVariable("projectId") String projectId, HttpServletRequest request) {
//		logService.saveLogDetails(request, "Call recieved", null);
//		model.addAttribute("projectId", projectId);
//		model.addAttribute("activeStep8", "active");
//		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
//		return "postSigning/summaryTab";
//	}
}
