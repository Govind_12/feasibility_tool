package com.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//192.168.1.130/raj.rohit/pvr-tool.git

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.common.mappers.FhcMapper;
import com.common.mappers.ServiceChargeMapper;
import com.common.models.AtpCapDetials;
import com.common.models.CinemaCategoryMaster;
import com.common.models.CinemaFormatMaster;
import com.common.models.FixedConstant;
import com.common.models.ProjectDetails;
import com.common.models.SeatTypeMaster;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.helper.MisHelper;
import com.services.AdminService;
import com.services.AdminServiceImpl;
import com.services.FixedConstantsService;
import com.services.LogServiceImpl;
import com.services.mis.MisService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("admin")
public class AdminController {

	@Autowired
	private FixedConstantsService fixedConstantsService;

	@Autowired
	private MisService misService;
	
	@Autowired
	LogServiceImpl logService;

	@Autowired
	private AdminService adminService;

	@GetMapping("film-hire-cost")
	public String getFilmHireCost(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FixedConstant fixedConstants = fixedConstantsService.getFixedConstants();
		models.addAttribute("operatingAssump", fixedConstants);
		return "admin/fhc";
	}

	@PostMapping("film-hire-cost")
	public String saveFilmHireCost(@ModelAttribute("fhcMapper") FhcMapper fhcMapper, RedirectAttributes attributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		Boolean result = fixedConstantsService.saveFhc(fhcMapper);
		log.info(result + "");
		if (result) {
			attributes.addFlashAttribute("success", "FHC details saved successfully.");
		} else {
			attributes.addFlashAttribute("error", "Oops! Something went wrong.");
		}

		return "redirect:/admin/film-hire-cost";
	}

	@PostMapping("edit-film-hire-cost")
	public String editFilmHireCost(@ModelAttribute("fhcMapper") FhcMapper fhcMapper, RedirectAttributes attributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		Boolean result = fixedConstantsService.saveFhc(fhcMapper);
		log.info(result + "");
		if (result) {
			attributes.addFlashAttribute("success", "FHC details updated successfully.");
		} else {
			attributes.addFlashAttribute("error", "Oops! Something went wrong.");
		}

		return "redirect:/admin/film-hire-cost";
	}

	@GetMapping("irr-constant-details")
	public String irrConstantDetails(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		models.addAttribute("fixedConstant", fixedConstantsService.getFixedConstants());
		return "admin/irrConstantDetails";
	}

	@PostMapping(value = "irr-constant-details")
	public String irrConstantDetails(@ModelAttribute("fixedConstant") FixedConstant fixedConstant, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		fixedConstantsService.irrConstantDetails(fixedConstant);
		redirectAttributes.addFlashAttribute("msgs", "Capex_Assumption saved successfully");
		return "redirect:/admin/irr-constant-details";
	}

	@GetMapping("gst-tax")
	public String getTaxAssumption(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FixedConstant fixedConstant1 = fixedConstantsService.getFixedConstants();
		models.addAttribute("fixedConstants", fixedConstant1);
		models.addAttribute("taxAssump", fixedConstant1);
		return "admin/taxAssumption";
	}

	@PostMapping(value = "saveTaxAssumption")
	public String saveTaxAssumption(@ModelAttribute("taxAssumption") FixedConstant fixedConstant, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		fixedConstantsService.saveTaxsAssumption(fixedConstant);
		redirectAttributes.addFlashAttribute("success", "Tax Assumptions saved successfully");
		return "redirect:/admin/gst-tax";
	}

	@GetMapping("is-fhc-present/{name}")
	@ResponseBody
	public boolean editFilmHireCost(@PathVariable String name, Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FhcMapper fhcMapper = new FhcMapper();
		fhcMapper.setName(name);
		boolean result = fixedConstantsService.isFhcPresent(fhcMapper);
		log.info(result + "");
		return result;
	}

	@GetMapping("get-cinemaFormat-List")
	public String getAllCinemaFormat(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		List<CinemaFormatMaster> categoryMasterList = fixedConstantsService.getAllCinemaFormat();
		models.addAttribute("categoryMasterList", categoryMasterList);
		return "admin/cinemaFormat";
	}

	@GetMapping("get-cinemaFormat/{id}")
	public String getCinemaFormat(Model models, @PathVariable("id") String id, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		CinemaFormatMaster categoryMaster = fixedConstantsService.getCinemaFormat(id);
		models.addAttribute("categoryMaster", categoryMaster);
		return "admin/editCinemaFormat";
	}

	@PostMapping("save/cinemaFormat")
	public String saveCinemaFormat(@ModelAttribute("categoryMaster") CinemaFormatMaster cinemaCategoryMaster,
			RedirectAttributes attributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		cinemaCategoryMaster.setSaveMapping(false);
		Boolean result = fixedConstantsService.saveCinemaFormat(cinemaCategoryMaster);
		log.info(result + "");
		if (result) {
			attributes.addFlashAttribute("success", "Cinema Format details saved successfully.");
		} else {
			attributes.addFlashAttribute("error", "Oops! Something went wrong.");
		}

		return "redirect:/admin/get-cinemaFormat-List";
	}

	@GetMapping("get-cinemaCategory-List")
	public String getAllCinemaCategory(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		List<CinemaCategoryMaster> categoryMasterList = fixedConstantsService.getAllCinemaCategory();
		models.addAttribute("categoryMasterList", categoryMasterList);
		return "admin/cinemaCategory";
	}

	@GetMapping("get-cinemaCategory/{id}")
	public String getCinemaCategory(Model models, @PathVariable("id") String id) {
		CinemaCategoryMaster categoryMaster = fixedConstantsService.getCinemaCategory(id);
		models.addAttribute("categoryMaster", categoryMaster);
		return "admin/editCinemaCategory";
	}

	@PostMapping("save/cinemaCategory")
	public String saveCinemaCategory(@ModelAttribute("categoryMaster") CinemaCategoryMaster cinemaCategoryMaster,
			RedirectAttributes attributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		Boolean result = fixedConstantsService.saveCinemaCategory(cinemaCategoryMaster);
		log.info(result + "");
		if (result) {
			attributes.addFlashAttribute("success", "Cinema Category details saved successfully.");
		} else {
			attributes.addFlashAttribute("error", "Oops! Something went wrong.");
		}

		return "redirect:/admin/get-cinemaCategory-List";
	}

	@GetMapping("get-seatType-List")
	public String getAllSeatType(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		List<SeatTypeMaster> seatMasterList = fixedConstantsService.getAllSeatType();
		models.addAttribute("seatMasterList", seatMasterList);
		return "admin/seatCategory";
	}

	@GetMapping("get-seatType/{id}")
	public String getSeatType(Model models, @PathVariable("id") String id, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		SeatTypeMaster seatTypeMaster = fixedConstantsService.getSeatType(id);
		models.addAttribute("seatTypeMaster", seatTypeMaster);
		return "admin/editSeatCategory";
	}

	@PostMapping("save/seatType")
	public String saveSeatType(@ModelAttribute("seatTypeMaster") SeatTypeMaster seatTypeMaster,
			RedirectAttributes attributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		Boolean result = fixedConstantsService.saveSeatType(seatTypeMaster);
		log.info(result + "");
		if (result) {
			attributes.addFlashAttribute("success", "Seat Type details saved successfully.");
		} else {
			attributes.addFlashAttribute("error", "Oops! Something went wrong.");
		}

		return "redirect:/admin/get-seatType-List";
	}

	@GetMapping("atpcap")
	public String getAtpCap(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FixedConstant fixedConstants = fixedConstantsService.getFixedConstants();
		models.addAttribute("atpCapting", fixedConstants);
		return "admin/atpCap";
	}

	@PostMapping(value = "saveAtpCap")
	public String saveAtpCap(FixedConstant fixedConstant, Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("ATP-CAP  " + fixedConstant.getAtpCapDetails().toString());
		fixedConstantsService.saveAtpCap(fixedConstant);
		redirectAttributes.addFlashAttribute("success",
				"ATP-CAP for " + fixedConstant.getAtpCapDetails().get(0).getState() + " saved successfully");
		return "redirect:/admin/atpcap";
	}

	@GetMapping("get-cinemaFormatMapping-List")
	public String getAllCinemaFormatMapping(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		List<CinemaFormatMaster> formatMappingList = fixedConstantsService.getAllCinemaFormat();
		models.addAttribute("formatMappingList", formatMappingList);
		return "admin/cinemaFormatMapping";
	}

	@GetMapping("get-cinemaFormatMapping/{id}")
	public String getCinemaFormatMapping(Model models, @PathVariable("id") String id, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		CinemaFormatMaster cinemaFormatMappingMaster = fixedConstantsService.getCinemaFormat(id);
		List<CinemaFormatMaster> formatMappingList = fixedConstantsService.getAllCinemaFormat();

		models.addAttribute("formatMappingList",
				fixedConstantsService.getFormatMappingViewList(formatMappingList, id, cinemaFormatMappingMaster));
		models.addAttribute("mappingMaster", cinemaFormatMappingMaster);
		return "admin/editCinemaFormatMapping";
	}

	@PostMapping("save/cinemaFormatMapping")
	public String saveCinemaFormatMapping(@ModelAttribute("mappingMaster") CinemaFormatMaster cinemaFormatMaster,
			RedirectAttributes attributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		cinemaFormatMaster.setSaveMapping(true);
		Boolean result = fixedConstantsService.saveCinemaFormat(cinemaFormatMaster);
		log.info(result + "");
		if (result) {
			attributes.addFlashAttribute("success1", "Cinema Format Mapping details saved successfully.");
		} else {
			attributes.addFlashAttribute("error1", "Oops! Something went wrong.");
		}

		return "redirect:/admin/get-cinemaFormat-List";
	}

	@GetMapping("atpcap/{state}/{screenType}")
	public String getOpearatngAssumption(Model models, @PathVariable String state, @PathVariable String screenType,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("state  " + state);
		FixedConstant fixedConstant = new FixedConstant();
		AtpCapDetials atpCap = new AtpCapDetials();
		List<AtpCapDetials> atpCapDetails = new ArrayList<AtpCapDetials>();
		atpCap.setState(state);
		atpCapDetails.add(atpCap);
		fixedConstant.setAtpCapDetails(atpCapDetails);
		fixedConstant.setScreenType(screenType);
		fixedConstantsService.deleteAtpCap(fixedConstant);
		redirectAttributes.addFlashAttribute("success", "ATP-CAP for " + state + " deleted successfully");
		return "redirect:/admin/atpcap";
	}

	@GetMapping("assumptionstate")
	public String serviceChargeTicket(Model model1, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FixedConstant fixedConstant1 = fixedConstantsService.getFixedConstants();
		Map<String, Double> treeMap = new TreeMap<>();

		if (!ObjectUtils.isEmpty(fixedConstant1.getServiceChargePerTicket())) {
			treeMap.putAll(fixedConstant1.getServiceChargePerTicket());
		}

		model1.addAttribute("serviceCharge", treeMap);
		model1.addAttribute("lbtAvg", fixedConstant1.getLbtAverageRate());
		model1.addAttribute("healthCess", fixedConstant1.getHealthCess());
		log.info(new Gson().toJson(fixedConstant1.getServiceChargePerTicket()));
		return "admin/growthAssumptionState";
	}

	@PostMapping(value = "assumptionstate")
	public String saveServiceCharge(@ModelAttribute("serviceCharge") ServiceChargeMapper serviceCharges, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		fixedConstantsService.saveServiceChargePerTickets(serviceCharges);
		redirectAttributes.addFlashAttribute("success", "Assumtionstate saved successfully");
		return "redirect:/admin/assumptionstate";
	}

	@GetMapping("cogs")
	public String getCogs(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FixedConstant fixedConstants = fixedConstantsService.getFixedConstants();
		models.addAttribute("operatingAssump", fixedConstants);
		return "admin/cogs";
	}

	@PostMapping("cogs")
	public String saveCogs(@ModelAttribute("fhcMapper") FhcMapper fhcMapper, RedirectAttributes attributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		Boolean result = fixedConstantsService.saveCogs(fhcMapper);
		log.info(result + "");
		if (result) {
			attributes.addFlashAttribute("success", "COGS details saved successfully.");
		} else {
			attributes.addFlashAttribute("error", "Oops! Something went wrong.");
		}

		return "redirect:/admin/irr-constant-details";
	}

	@PostMapping("edit-cogs")
	public String editCogs(@ModelAttribute("fhcMapper") FhcMapper fhcMapper, RedirectAttributes attributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		Boolean result = fixedConstantsService.saveCogs(fhcMapper);
		log.info(result + "");
		if (result) {
			attributes.addFlashAttribute("success", "COGS details saved successfully.");
		} else {
			attributes.addFlashAttribute("error", "Oops! Something went wrong.");
		}

		return "redirect:/admin/irr-constant-details";
	}

	@GetMapping("operating-assumption")
	public String getOpearatingAssumption(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FixedConstant fixedConstants = fixedConstantsService.getFixedConstants();
		models.addAttribute("operatingAssump", fixedConstants);
		return "admin/operatingAssumption";
	}

	@PostMapping(value = "operating-assumption")
	public String saveOperatingAssumption(@ModelAttribute("operatAssumption") FixedConstant fixedConstant, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		fixedConstantsService.saveOpeartingsAssumption(fixedConstant);
		redirectAttributes.addFlashAttribute("successos", "Operating Assumption saved successfully");
		return "redirect:/admin/irr-constant-details";
	}

	@PostMapping(value = "edit-operating-assumption")
	public String editOperatingAssumption(@ModelAttribute("operatAssumption") FixedConstant fixedConstant, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		fixedConstantsService.saveOpeartingsAssumption(fixedConstant);
		redirectAttributes.addFlashAttribute("successos", "Operating Assumption updated successfully");
		return "redirect:/admin/irr-constant-details";
	}

	@GetMapping("delete-operating-assumption/{city}")
	public String deleteOpearatingAssumption(@PathVariable("city") String city, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FhcMapper fhcMapper = new FhcMapper();
		fhcMapper.setName(city);
		fixedConstantsService.deleteOpearatingAssumption(fhcMapper);
		redirectAttributes.addFlashAttribute("successos", "Operating Assumption for " + city + " updated successfully");
		return "redirect:/admin/irr-constant-details";
	}

	@GetMapping("gst-on-ticket")
	public String getGstOnTicket(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FixedConstant fixedConstants = fixedConstantsService.getFixedConstants();
		models.addAttribute("fixedConstants", fixedConstants);
		return "admin/gst-on-ticket";
	}

	@PostMapping("gst-on-ticket")
	public String saveGstOnTicket(@ModelAttribute("fhcMapper") FhcMapper fhcMapper, RedirectAttributes attributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		fixedConstantsService.saveGstOnTicket(fhcMapper);
		attributes.addFlashAttribute("successGst", "GST on ticket updated successfully");
		return "redirect:/admin/gst-tax";
	}

	@GetMapping("feasibility-mis")
	public String mis(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		return "admin/mis";
	}

	@PostMapping("feasibility-mis")
	public void getMis(Model models, @ModelAttribute MisHelper misHelper, HttpServletResponse response, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		try {
			byte[] output = misService.getMis(misHelper);

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename= Feasibility Mis.xlsx");

			response.getOutputStream().write(output);
			response.getOutputStream().flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@PostMapping({ "feasibility-ops-mis" })
	public void geOpstMis(Model models, @ModelAttribute MisHelper misHelper, HttpServletResponse response) {
		try {
			byte[] output = this.misService.getOpsMis(misHelper);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename= Feasibility Operational Mis.xlsx");
			response.getOutputStream().write(output);
			response.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@PostMapping({ "feasibility-ext-mis" })
	public void getExtMis(Model models, @ModelAttribute MisHelper misHelper, HttpServletResponse response) {
		try {
			byte[] output = this.misService.getExtMis(misHelper);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename= Feasibility Exit Mis.xlsx");
			response.getOutputStream().write(output);
			response.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GetMapping({ "feasibility-opsMis" })
	public String opsMis(Model model) {
		List<ProjectDetails> dashboardDetails = adminService.getOpsReports();
		model.addAttribute("projectDetails", dashboardDetails);
		return "admin/operationalMis";
	}

	@GetMapping({ "feasibility-exitMis" })
	public String extMis(Model model) {
		List<ProjectDetails> dashboardDetails = adminService.getExitReports();
		model.addAttribute("projectDetails", dashboardDetails);
		return "admin/exitMis";
	}

	@PostMapping({ "opsFeasibilityMis" })
	@ResponseBody
	public List<ProjectDetails> changeRptToOps(Model model, @RequestParam("ids[]") List<Integer> ids) {
		List<ProjectDetails> dashboardDetails = adminService.updateOpsReports(ids);
		model.addAttribute("projectDetails", dashboardDetails);
		return dashboardDetails;
	}

	@PostMapping({ "updateExtFeasibilityMis" })
	@ResponseBody
	public List<ProjectDetails> updateExtReports(Model model, @RequestParam("ids[]") List<Integer> ids) {
		List<ProjectDetails> dashboardDetails = adminService.updateExtReports(ids);
		model.addAttribute("projectDetails", dashboardDetails);
		return dashboardDetails;
	}

	@GetMapping("log")
	public String usersList(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);

		model.addAttribute("userListActive", true);
		model.addAttribute("setting", true);
		model.addAttribute("title", "Log");
		model.addAttribute("logs", logService.getAllLogs());
		return "admin/log";
	}

	@GetMapping({ "changeProjName" })
	public String changeProjName(Model model,HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		List<ProjectDetails> dashboardDetails = this.adminService.getReportsForChangeProjName();
		model.addAttribute("projectDetails", dashboardDetails);
		model.addAttribute("status", null);
		return "admin/changeProjName";
	}

	@SuppressWarnings("unchecked")
	@PostMapping({ "updateProjName" })
	@ResponseBody
	public List<ProjectDetails> updateProjName(HttpServletRequest req, Model modal, RedirectAttributes attributes) {
		Map<String, String> projData = null;
		logService.saveLogDetails(req, "Call recieved", null);
		try {
			String data = req.getParameter("jsonObject");
			ObjectMapper mapper = new ObjectMapper();
			projData = (Map<String, String>) mapper.readValue(data, Map.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<ProjectDetails> dashboardDetails = this.adminService.updateProjectNames(projData);
		modal.addAttribute("projectDetails", dashboardDetails);
		attributes.addFlashAttribute("status", "Project Name Changed Successfully");
		return dashboardDetails;
	}
	
	@PostMapping("save/screen/type")
	@ResponseBody
	public String saveScreenType(@RequestBody CinemaFormatMaster cinemaFormatMaster, RedirectAttributes attributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		
		List<String> cinemaFormatSubCategoryList = new ArrayList<>();
		if(!ObjectUtils.isEmpty(cinemaFormatMaster.getCinemaSubCategory())) {
			for(String str : cinemaFormatMaster.getCinemaSubCategory()) {
				cinemaFormatSubCategoryList.add(str.trim());
			}
			cinemaFormatMaster.setCinemaSubCategory(cinemaFormatSubCategoryList);
			
		}
		List<String> cinemaFormatMappingList = new ArrayList<>();
		if(!ObjectUtils.isEmpty(cinemaFormatMaster.getCinemaFormatMapping())) {
			for(String str : cinemaFormatMaster.getCinemaFormatMapping()) {
				cinemaFormatMappingList.add(str.trim());
			}
			cinemaFormatMaster.setCinemaFormatMapping(cinemaFormatMappingList);
			
		}
		Boolean result = fixedConstantsService.saveScreenType(cinemaFormatMaster);
		log.info(result + "");
		if (result) {
			attributes.addFlashAttribute("success", "Add new screen saved successfully.");
		} else {
			attributes.addFlashAttribute("error", "Oops! Something went wrong.");
		}

		return "/admin/get-cinemaFormat-List";
	}
}
