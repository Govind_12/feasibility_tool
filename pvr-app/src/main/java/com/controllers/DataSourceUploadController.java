package com.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.common.mappers.UploadFilesMapper;
import com.common.models.ProjectCostSummary;
import com.services.DataSourceService;
import com.services.LogServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("upload")
public class DataSourceUploadController {

	@Autowired
	private DataSourceService dataSourceService;

	@Autowired
	LogServiceImpl logService;

	@GetMapping("")
	public String fileUpload() {
		return "DataSource/upload";
	}

	@PostMapping("/files")
	public String uploadDataSource(@ModelAttribute("UploadFilesMapper") UploadFilesMapper uploadMapper, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		dataSourceService.saveDataSource(uploadMapper);
		return "redirect:/upload";
	}

	@RequestMapping(value = "/projectCostSummary", method = RequestMethod.POST, headers = ("content-type=multipart/*"))
	@ResponseBody
	public ProjectCostSummary uploadProjectCosts(@RequestParam("file") MultipartFile file, String id, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		UploadFilesMapper uploadMapperFile = new UploadFilesMapper();
		uploadMapperFile.setFile(file);
		uploadMapperFile.setProjectId(id);
		log.info("uploadMapperFile.getProjectId() :App -> " + uploadMapperFile.getProjectId());
		return dataSourceService.getProjectCost(uploadMapperFile);

	}
	
	  @GetMapping({"/dataMigrate"})
	  public String dataMigrate() {
	    return "admin/data-migrate";
	  }

}
