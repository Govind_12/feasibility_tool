package com.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.common.models.ProjectDetails;
import com.fasterxml.jackson.databind.JsonNode;
import com.services.LogServiceImpl;
import com.services.ProjectDetailService;
import com.services.mis.MisService;

@Controller
@RequestMapping("report")
public class ReportDownload {

	@Value("${path.fileDownloadPath}")
	private String fileDownloadPath;

	@Autowired
	private ProjectDetailService projectDetailService;

	@Autowired
	LogServiceImpl logService;
	
	@Autowired
	private MisService misService;

	@RequestMapping(value = "generate-file/{projectId}", method = RequestMethod.GET)
	@ResponseBody
	public JsonNode generatetFile(@PathVariable("projectId") String projectId, HttpServletResponse response,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails projectDetails = projectDetailService.getProjectDetailsById(projectId);
		JsonNode pdfResponse = null;
		synchronized (projectDetails) {
			pdfResponse = projectDetailService.HtmlToPdfConversion(null, projectId);
		}
		return pdfResponse;
	}

	@RequestMapping(value = "/files/{projectId}", method = RequestMethod.GET)
	@ResponseBody
	public void getFile(@PathVariable("projectId") String projectId, HttpServletResponse response,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		String folder = "report-" + projectId;
		String fileExtension = ".pdf";
		ProjectDetails projectDetails = projectDetailService.getProjectDetailsById(projectId);
		String filePath = fileDownloadPath + folder + File.separator + projectDetails.getProjectName() + fileExtension;
		response.setContentType("application/pdf");
		response.addHeader("Content-Disposition",
				"attachment; filename=" + projectDetails.getProjectName() + fileExtension);
		try {
			Files.copy(new File(filePath).toPath(), response.getOutputStream());
			response.getOutputStream().flush();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@RequestMapping(value = "XcelFile" , method = RequestMethod.POST)
	@ResponseBody
	public void downLoadXcelFile(Model model, @ModelAttribute ProjectDetails prjDetails, HttpServletResponse resp) {
		try {
			byte[] output = this.misService.irrAsXcel(prjDetails.getId());
			resp.setContentType("application/vnd.ms-excel");
			resp.addHeader("Content-Disposition", "attachment; filename=" + prjDetails.getId() + ".xlsx");
			resp.getOutputStream().write(output);
			resp.getOutputStream().flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
