package com.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.util.UserRoleUtil;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Controller
@RequestMapping("index")
public class IndexController {
	
	@Value("${pvr.auth.header}")
	private String tokenHeader;
	
    @Value("${pvr.token.secret}")
    private String secret;

	@GetMapping("")
	public String index(HttpServletRequest request) {
		String authToken = (String) request.getSession().getAttribute(this.tokenHeader);
		String fullName = "";
		try {
			Claims claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(authToken).getBody();
			if (!ObjectUtils.isEmpty(claims)) {
				fullName = claims.getIssuer();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		request.getSession().setAttribute("fullName", fullName != null ? fullName : "");
		
		return UserRoleUtil.isCurrentUserIsRoleAdmin() ? "redirect:/upload" : "redirect:/dashboard";
	}
}