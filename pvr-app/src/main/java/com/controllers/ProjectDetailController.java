package com.controllers;

import java.util.List;

import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.common.constants.Constants;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.CinemaMaster;
import com.common.models.FinalSummary;
import com.common.models.FixedConstant;
import com.common.models.IrrCalculationData;
import com.common.models.ProjectCostSummary;
import com.common.models.ProjectDetails;
import com.services.CityService;
import com.services.FixedConstantsService;
import com.services.LogServiceImpl;
import com.services.ProjectDetailService;
import com.util.UpdateUrlUtil;
import com.web.Response;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping("project")
public class ProjectDetailController {

	@Autowired
	private ProjectDetailService projectDetailService;

	@Autowired
	private CityService cityService;

	@Autowired
	private FixedConstantsService fixedConstantsService;

	@Autowired
	LogServiceImpl logService;

	@RequestMapping(value = "")
	public String defaultDashboard(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);

		List<ProjectDetails> allProjects = projectDetailService.getAllProjects();

		Map<Integer, ProjectDetails> map = allProjects.stream()
				.collect(Collectors.groupingBy(ProjectDetails::getReportIdVersion,
						Collectors.collectingAndThen(Collectors.toList(), values -> values.get(0))));

		allProjects.clear();
		map.forEach((reportIdVersion, projectDetails) -> {
			if (projectDetails.getStatus() == Constants.STATUS_ARCHIVED
					|| projectDetails.getStatus() == Constants.STATUS_ACCEPTED)
				allProjects.add(projectDetails);
		});

		ProjectDetails latestProjectDetails = projectDetailService
				.getLatestProjectWithReportId(Constants.REPORT_TYPE_NEW, null);

		model.addAttribute("reportId", latestProjectDetails.getReportId());
		model.addAttribute("cities", cityService.getAllCities());
		model.addAttribute("allprojects", allProjects);
		model.addAttribute("activeStep1", "active");

		return "projectDetail/developmentDetails";
	}

	@GetMapping("getProjectById/{projectId}")
	public ProjectDetails getUserEmployeeDetailsById(@PathVariable("projectId") String projectId, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching user details by user id");
		return projectDetailService.getProjectById(projectId);
	}

	@RequestMapping(value = "finalSummary")
	public String index2() {
		return "finalSummary/summary";
	}

	@GetMapping("review/{id}")
	public String review(@PathVariable("id") String id, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("projectDetails", projectDetailService.getProjectDetailsById(id));
		return "finalSummary/summary";
	}

	@GetMapping("approve/{projectId}")
	public String approveProject(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.approveProject(projectId);
		return "redirect:/dashboard";
	}

	@GetMapping("reject/{projectId}")
	public String rejectProject(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.rejectProject(projectId);
		return "redirect:/dashboard";
	}

	@PostMapping("reject")
	public String rejectProject(@ModelAttribute MsgMapper msgMapper, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		// projectDetailService.rejectProject(projectId);
		projectDetailService.rejectProjectDetails(msgMapper);
		return "redirect:/dashboard";
	}

	@GetMapping("allProjectDetails")
	public String companyList(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching list of all companies");
		model.addAttribute("projectDetails", projectDetailService.getOldReports());
		return "projectDetail/view";
	}

	@GetMapping("query/{projectId}")
	public @ResponseBody FinalSummary query(@PathVariable String projectId) {
		log.info("fetching query by project id");
		FinalSummary finalSummary = projectDetailService.getQueryByProjectId(projectId);
		return finalSummary;
	}

	@PostMapping("save-query")
	public @ResponseBody QueryMapper submitQuery(@ModelAttribute MsgMapper msgMapper, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("submit query");
		return projectDetailService.submitQuery(msgMapper);

	}

	// step 1

	@GetMapping("/developmentDetails/{projectId}")
	public String getDevelopmentDetails(@PathVariable("projectId") String projectId, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("activeStep1", "active");
		model.addAttribute("projectId", projectId);
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		return "projectDetail/developmentDetails";
	}

	@PostMapping(value = "development-details")
	private String saveAndNextStep1(@ModelAttribute("projectDetails") ProjectDetails projectDetails, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep1(projectDetails);
		return "redirect:/project/developerDetails/" + details.getId();
	}

	@PostMapping(value = "development-details-save")
	public String saveAndCloseStep1(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving project details");
		projectDetailService.saveStep1(projectDetails);
		redirectAttributes.addFlashAttribute("msg", "Project saved successfully");
		return "redirect:/dashboard";
	}

	@GetMapping("get-all-project")
	@ResponseBody
	public List<ProjectDetails> getAllProject() {
		return projectDetailService.getExisitingProjectDetails();
	}

	@GetMapping("get-project-by-id/{projectId}")
	public String getProjectByIdStep1(Model model, @PathVariable("projectId") String projectId,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.getProjectById(projectId);
		ProjectDetails latestProjectDetails = projectDetailService
				.getLatestProjectWithReportId(Constants.REPORT_TYPE_EXISTING, details.getReportId());

		details.setReportId(latestProjectDetails.getReportId());
		details.setReportIdVersion(latestProjectDetails.getReportIdVersion());
		details.setReportIdSubVersion(latestProjectDetails.getReportIdSubVersion());

		details.setReportType(Constants.REPORT_TYPE_EXISTING);
		details.setStatus(0);
		model.addAttribute("projectDetails", details);
		model.addAttribute("projectList", projectDetailService.getPreSigningProjectsByStatus());
		model.addAttribute("activeStep1", "active");
		return "projectDetail/developmentDetails";
	}

	// step 2
	@GetMapping("/developerDetails/{projectId}")
	public String step2(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("activeStep2", "active");
		model.addAttribute("projectId", projectId);
		model.addAttribute("cinemaMasterDetails", projectDetailService.getAllCinemaMasterDetails());
		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);
		if (!ObjectUtils.isEmpty(projectDetails.getPvrProjectDelivered())) {
			projectDetails.setPvrProjectDeliveredFlag(true);
		}
		model.addAttribute("projectDetails", projectDetails);
		return "projectDetail/developerDetails";
	}

	@PostMapping(value = "developer-details")
	private String saveAndNextStep2(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep2(projectDetails);
		return "redirect:/project/upcomingCinemas/" + details.getId();
	}

	@PostMapping(value = "developer-details-save")
	public String saveAndCloseStep2(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving project details");
		projectDetailService.saveStep2(projectDetails);
		redirectAttributes.addFlashAttribute("msg", "Project saved successfully");
		return "redirect:/dashboard";
	}

	// step 3
	@GetMapping("/upcomingCinemas/{projectId}")
	public String step3(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("activeStep3", "active");
		model.addAttribute("projectId", projectId);
		ProjectDetails projectById = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectDetails", projectById);
		return "projectDetail/upcomingCinemas";
	}

	@PostMapping(value = "upcoming-cinemas-save")
	public String saveAndCloseStep3(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving project details");
		projectDetailService.saveStep3(projectDetails);
		redirectAttributes.addFlashAttribute("msg", "Project saved successfully");
		return "redirect:/dashboard";
	}

	@PostMapping(value = "upcoming-cinemas")
	private String saveAndNextStep3(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectDetails.toString());
		ProjectDetails details = projectDetailService.saveStep3(projectDetails);
		return "redirect:/project/lesserLessee/" + details.getId();
	}

	// step 4
	@GetMapping("/lesserLessee/{projectId}")
	public String step4(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("activeStep4", "active");
		model.addAttribute("projectId", projectId);
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		return "projectDetail/lesserLessee";
	}

	@PostMapping(value = "lesser-lessee")
	private String saveAndNextStep4(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectDetails.toString());
		ProjectDetails details = projectDetailService.saveStep4(projectDetails);
		return "redirect:/project/projectLayout/" + details.getId();
	}

	@PostMapping(value = "lesser-lessee-save")
	public String saveAndCloseStep4(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving project details");
		projectDetailService.saveStep4(projectDetails);
		redirectAttributes.addFlashAttribute("msg", "Project saved successfully");
		return "redirect:/dashboard";
	}

	// step 5

//	@GetMapping("/projectLayout/{projectId}")
//	public String step5(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
//		logService.saveLogDetails(request, "Call recieved", null);
//		model.addAttribute("activeStep5", "active");
//		model.addAttribute("projectId", projectId);
//		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
//		return "projectDetail/projectLayout";
//	}

	@GetMapping("/projectLayout/{projectId}")
	public String step5(@PathVariable("projectId") String projectId, Model model) {
		ProjectDetails detailor = this.projectDetailService.getProjectById(projectId);
		List<String> splitor = detailor.getMultipleFileURLStep5();
		model.addAttribute("activeStep5", "active");
		model.addAttribute("projectId", projectId);
		model.addAttribute("filor", splitor);
		model.addAttribute("projectDetails", detailor);
		return "projectDetail/projectLayout";
	}

	@PostMapping(value = "project-layout-save")
	public String saveandCloseStep5(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request,
			@RequestParam(name = "files[]", required = false) List<MultipartFile> files,
			@RequestParam(value = "oldfile[]", required = false) List<String> oldfiles) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving project details");
		projectDetailService.saveandCloseStep5(projectDetails, files, oldfiles);
		redirectAttributes.addFlashAttribute("msg", "Project saved successfully");
		return "redirect:/dashboard";
	}

//	@PostMapping(value = "project-layout")
//	private String saveandNextStep5(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
//			HttpServletRequest request) {
//		logService.saveLogDetails(request, "Call recieved", null);
//		ProjectDetails details = projectDetailService.saveStep5(projectDetails);
//		return "redirect:/project/operatingAssumptions/" + details.getId();
//	}
	
	@PostMapping({ "project-layout" })
	private String saveandNextStep5(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			@RequestParam(name = "files[]", required = false) List<MultipartFile> files,
			@RequestParam(value = "oldfile[]", required = false) List<String> oldfiles,HttpServletRequest request) {   
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = this.projectDetailService.saveStep5(projectDetails, files, oldfiles);
		return "redirect:/project/operatingAssumptions/" + details.getId();
	}

	// step 6

	@GetMapping("/operatingAssumptions/{projectId}")
	public String step6(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("activeStep6", "active");
		model.addAttribute("projectId", projectId);
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		return "projectDetail/operatingAssumptions";
	}

	@PostMapping(value = "operating-assumptions")
	private String saveAndNextStep6(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails details = projectDetailService.saveStep6(projectDetails);
		return "redirect:/project/summary-sheet/" + details.getId();
	}

	@PostMapping(value = "operating-assumptions-save")
	private String saveAndCloseStep6(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.saveStep6(projectDetails);
		return "redirect:/dashboard";
	}
	
	
	@GetMapping("/summaryTab/{projectId}")
	public String step8(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("activeStep8", "active");
		model.addAttribute("projectId", projectId);
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		return "projectDetail/summaryTab";
	}

	// step 7

	@GetMapping("/summary-sheet/{projectId}")
	public String step7(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("activeStep7", "active");
		model.addAttribute("projectId", projectId);
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		return "projectDetail/summarySheet";
	}

	@PostMapping(value = "summary-sheet")
	private String submitForApproval(@ModelAttribute("projectDetail") ProjectDetails projectDetails, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.saveStep7(projectDetails);
		return "redirect:/dashboard";
	}

	@PostMapping(value = "summary-sheet-update/{projectId}")
	public String updatePreSigning(@PathVariable("projectId") String projectId, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving project details");
		return "redirect:/dashboard";
	}

	@GetMapping("/tab_G/{projectId}")
	public String answerToStep7(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		// @RequestBody List<MsgMapper> msgMapperList, HttpServletRequest request){
		model.addAttribute("type", "tab_G");
		model.addAttribute("surveyId", projectId);
		model.addAttribute("projectDetails", projectDetailService.getProjectById(projectId));
		// model.addAttribute("msgMapperList", msgMapperList);
		return "projectDetail/update";
	}

	@GetMapping("getProjectId/{projectId}")
	public String getUserEmployeeDetailsById2(@PathVariable("projectId") String projectId, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching project details by user id");
		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);
		model.addAttribute("projectDetails", projectDetails);
		String url = UpdateUrlUtil.getUrl(projectDetails.getFeasibilityState(), projectDetails.getStatus(),
				projectDetails.getId());
		return "redirect:/" + UpdateUrlUtil.getFeasibilityUrl(projectDetails.getFeasibilityState()) + "/" + url;
	}

	@GetMapping("deleteProjectWithNotification/{projectId}")
	@ResponseBody
	public Boolean deleteProject(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);

		return projectDetailService.updateProjectWithNotification(projectId, Constants.STATUS_DELETE);

	}

	@GetMapping("archiveProjectWithNotification/{projectId}")
	@ResponseBody
	public Boolean archiveProjectWithNotification(@PathVariable("projectId") String projectId,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);

		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);

		int projectStatus = projectDetails.getFeasibilityState().equals(Constants.PRE_SIGNING)
				? Constants.STATUS_ARCHIVED
				: (projectDetails.getFeasibilityState().equals(Constants.POST_SIGNING) ? Constants.POST_STATUS_ARCHIVED
						: Constants.PRE_HAND_STATUS_ARCHIVED);

		return projectDetailService.updateProjectWithNotification(projectId, projectStatus);

	}

	@GetMapping("archiveProjectWithDashBoard/{projectId}")
	public String archiveProjectWithDashBoard(@PathVariable("projectId") String projectId, Model model,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);

		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);

		int projectStatus = projectDetails.getFeasibilityState().equals(Constants.PRE_SIGNING)
				? Constants.STATUS_ARCHIVED
				: (projectDetails.getFeasibilityState().equals(Constants.POST_SIGNING) ? Constants.POST_STATUS_ARCHIVED
						: Constants.PRE_HAND_STATUS_ARCHIVED);

		projectDetailService.updateProjectWithNotification(projectId, projectStatus);
		return "redirect:/dashboard";
	}

	// projectDetailService.getLatestProjectWithReportId(Constants.REPORT_TYPE_NEW)
	@GetMapping("getLatestProjectWithReportId/{reportType}")
	@ResponseBody
	public ProjectDetails getLatestProjectWithReportId(@PathVariable("reportType") String reportType,
			@RequestParam("reportId") String reportId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		return projectDetailService.getLatestProjectWithReportId(reportType, reportId);

	}

	@GetMapping("getAllProjectCostSummary/{projectId}")
	@ResponseBody
	public ProjectCostSummary getAllProjectCostSummary(@PathVariable("projectId") String projectId,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return projectDetailService.fetchAllProjectCost(projectId);
	}

	@GetMapping("getProjectDetailsById/{projectId}")
	@ResponseBody
	public ResponseEntity<Response<ProjectDetails>> getProjectById(@PathVariable("projectId") String projectId,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);
		return new ResponseEntity<Response<ProjectDetails>>(
				new Response<ProjectDetails>(HttpStatus.OK.value(), "Project Fetched successfully", projectDetails),
				HttpStatus.OK);

	}

	@GetMapping("get-cinema-master-details")
	@ResponseBody
	public ResponseEntity<Response<List<CinemaMaster>>> getCinemaMasterDetails(HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		List<CinemaMaster> cinemaMasters = projectDetailService.getAllCinemaMasterDetails();
		return new ResponseEntity<Response<List<CinemaMaster>>>(new Response<List<CinemaMaster>>(HttpStatus.OK.value(),
				"Cinema master details Fetched successfully", cinemaMasters), HttpStatus.OK);

	}

	@GetMapping("update/{projectId}")
	public String updateProject(@PathVariable("projectId") String projectId, Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		projectDetailService.updateProject(projectId);

		ProjectDetails projectDetails = projectDetailService.getProjectById(projectId);

		model.addAttribute("projectDetails", projectDetails);
		String url = UpdateUrlUtil.getUrl(projectDetails.getFeasibilityState(), projectDetails.getStatus(),
				projectDetails.getId());
		return "redirect:/" + "project/" + url;
	}

	@GetMapping("operatingAssumptions/getRevenueAssumptionConstant/{projectId}")
	@ResponseBody
	public FixedConstant getRevenueAssumptionConstant(Model model, @PathVariable("projectId") String projectId) {
		FixedConstant fixedConstant = projectDetailService.getRevenueAssumptionConstant(projectId);
		return fixedConstant;
	}
	
	@PostMapping(value = "/save-executive-note")
	@ResponseBody
	public ProjectDetails saveExecutiveNote(ProjectDetails projectDetails, HttpServletRequest request, Model model) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("saving project details");
	    return projectDetailService.saveExecutiveNote(projectDetails);
	}

    @GetMapping({"allExistingProjectDetailsForCopy"})
    public String existingProjectListForCopy(Model model) {
      log.info("fetching list of all companies");
      model.addAttribute("projectDetails", projectDetailService.getExistingOldReportsForCopy());
      return "projectDetail/copyFeasibility";
    }
    
    @GetMapping({"copyFeasibilityReport/{projectId}"})
    public String copyFeasibilityReport(@PathVariable("projectId") String projectId, Model model) {
      projectDetailService.copyExistingProject(projectId);
      return "redirect:/dashboard";
    }

	@RequestMapping({"get-pandl-tab/{projectId}"})
	public String getPAndLTabData(@PathVariable("projectId") String projectId,Model model) {
		IrrCalculationData data = projectDetailService.getPAndLTabData(projectId);
	    model.addAttribute("pandlDetails", data);
	    model.addAttribute("activeStep8", "active");
		return "projectDetail/pAndLTab";
	}
}

