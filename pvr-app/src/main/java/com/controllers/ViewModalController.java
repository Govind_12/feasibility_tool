package com.controllers;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.services.LogServiceImpl;
import com.services.ViewModalDetailService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("view-modal")
@SuppressWarnings("rawtypes")
public class ViewModalController {
	
	@Autowired
	private ViewModalDetailService viewModalDetailService;
	
	@Autowired
	LogServiceImpl logService;
	
	@GetMapping("getTotalAdmits/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getTotalAdmits(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getTotalAdmits(projectId);
	}
	
	
	@GetMapping("getAdRevenue/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getAdRevenue(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getAdRevenue(projectId);
	}
	
	
	@GetMapping("getPersonalExpense/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getPersonalExpense(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getPersonalExpense(projectId);
	}

	
	@GetMapping("getElectricityExpense/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getElectricityExpense(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getElectricityExpense(projectId);
	}

	
	@GetMapping("getRmExpense/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getRmExpense(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getRmExpense(projectId);
	}
	
	
	@GetMapping("getWebSales/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getWebSales(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getWebSales(projectId);
	}
	
	
	@GetMapping("getHousekeeping/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getHousekeeping(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getHousekeeping(projectId);
	}
	
	
	@GetMapping("getSecurityExpense/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getSecurityExpense(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getSecurityExpense(projectId);
	}
	
	
	@GetMapping("getCommunicationExpense/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getCommunicationExpense(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getCommunicationExpense(projectId);
	}
	
	
	@GetMapping("getTravellingExpense/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getTravellingExpense(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getTravellingExpense(projectId);
	}
	
	
	@GetMapping("getLeegalfee/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getLeegalfee(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getLeegalfee(projectId);
	}
	
	
	@GetMapping("getInsuranceExpense/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getInsuranceExpense(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getInsuranceExpense(projectId);
	}
	
	
	@GetMapping("getInternetExpenses/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getInternetExpenses(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getInternetExpenses(projectId);
	}
	
	
	@GetMapping("getPrintingStationary/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getPrintingStationary(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getPrintingStationary(projectId);
	}
	
	
	@GetMapping("getMarketingExpense/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getMarketingExpense(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getMarketingExpense(projectId);
	}
	
	
	@GetMapping("getMiscellaneousExpense/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getMiscellaneousExpense(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getMiscellaneousExpense(projectId);
	}
	
	@GetMapping("getAnnualRent/{projectId}")
	@ResponseBody
	public Map<String, ? super ArrayList> getAnnualRent(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info(projectId);
		return viewModalDetailService.getAnnualRent(projectId);
	}
	@GetMapping("getViewCalculation/{projectId}")
	@ResponseBody
	public Map<String, ? super Object> getViewCalculation(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching view calculation modal..");
		return viewModalDetailService.getViewCalculation(projectId);
	}
	@GetMapping("getOccupancyDetails/{projectId}")
	@ResponseBody
	public Map<String, ? super Object> getOccupancyDetails(@PathVariable("projectId") String projectId, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		log.info("fetching view calculation modal..");
		return viewModalDetailService.getViewOccupancy(projectId);
	}
}
