package com.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.common.models.City;
import com.services.CityService;
import com.services.LogServiceImpl;

@Controller
@RequestMapping("city")
public class CityController {

	@Autowired
	private CityService cityService;
	
	@Autowired
	LogServiceImpl logService;

	@GetMapping("")
	public List<City> usersList(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		model.addAttribute("cities", cityService.getAllCities());
		return cityService.getAllCities();
	}

	@GetMapping(value = "get-state/{city}")
	@ResponseBody
	public List<City> getUserEmployeeDetailsById(@PathVariable("city") String city, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		System.out.println(cityService.getStateBycityName(city));
		return cityService.getStateBycityName(city);
	}

}
