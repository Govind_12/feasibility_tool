package com.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.config.Layout;
import com.services.AuthService;
import com.services.LogServiceImpl;
import com.services.UserService;

@Controller
public class AuthController {

	@Autowired
	private UserService userService;

	@Autowired
	private AuthService authService;

	@Autowired
	LogServiceImpl logService;

	@GetMapping("/login")
	@Layout(value = "layouts/noLayout")
	public String signin(HttpServletRequest request, Model model) {
		logService.saveLogDetails(request, "Call recieved", null);
		return "signin";
	}

	@GetMapping("/logout")
	@Layout(value = "layouts/noLayout")
	public String logout(HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		authService.logout(request);
		return "signin";
	}

	@GetMapping("/access-denied")
	public String accessDenied(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		return "access-denied";
	}

	@GetMapping("/error")
	public String error(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		return "error";
	}

	@GetMapping(value = "/forgotPassword")
	@ResponseBody
	public String[] forgotPassword(@RequestParam String username, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		System.out.println(username);
		String[] forgotPassword = userService.forgotPassword(username);
		return forgotPassword;
	}
}
