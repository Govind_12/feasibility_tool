package com.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.common.models.FixedConstant;
import com.services.FixedConstantsService;
import com.services.LogServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("assumption")
public class GrowthAssumptionController {

	@Autowired
	private FixedConstantsService fixedConstantsService;

	@Autowired
	LogServiceImpl logService;

	@GetMapping("")
	public String growthAssumption(Model model) {
		return "admin/growthAssumptions";
	}

	@GetMapping("getFixedConstants")
	@ResponseBody
	public FixedConstant getFixedConstants(Model model, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FixedConstant fixedConstant = fixedConstantsService.getFixedConstants();
		return fixedConstant;
	}

	@GetMapping("capex")
	public String getCapexAssumption(Model models, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		FixedConstant fixedConstant1 = fixedConstantsService.getFixedConstants();
		models.addAttribute("capexAssump", fixedConstant1);
		return "admin/capexAssumption";
	}

	@PostMapping(value = "saveCapexAssumption")
	public String saveCapexAssumption(@ModelAttribute("capexAssumption") FixedConstant fixedConstant, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		fixedConstantsService.saveCapexsAssumption(fixedConstant);
		redirectAttributes.addFlashAttribute("msgs", "Capex_Assumption saved successfully");
		return "redirect:/assumption/capex";
	}

	@RequestMapping(value = "temp")
	public String temp(FixedConstant fixedConstant) {
		return "admin/temp";
	}

	@RequestMapping(value = "saveGrowthAssumptions", method = RequestMethod.POST)
	public String saveGrowthAssumptions(FixedConstant fixedConstant, RedirectAttributes attributes,
			HttpServletRequest request) {
		logService.saveLogDetails(request, "Call recieved", null);
		fixedConstantsService.saveGrowthAssumptions(fixedConstant);
		attributes.addFlashAttribute("success", "Growth assumptions saved successfully");
		return "redirect:/assumption";
	}

}
