package com.util;

import org.springframework.stereotype.Component;

import com.common.constants.Constants;

@Component
public class UpdateUrlUtil {

	
	
	public static String getUrl(String feasibilityState, Integer status, String projectId) {
		String url = "";
		String updatedUrl = "";
		if (feasibilityState.equals(Constants.PRE_SIGNING)) {
			switch (status) {

			case Constants.STEP_1:
				url = Constants.DEVELOPMENT_DETAILS;
				break;

			case Constants.STEP_2:
				url = Constants.DEVELOPER_DETAILS;
				break;

			case Constants.STEP_3:
				url = Constants.UPCOMING_CINEMAS;
				break;

			case Constants.STEP_4:
				url = Constants.LESSER_LESSEE;
				break;

			case Constants.STEP_5:
				url = Constants.PROJECT_LAYOUT;
				break;

			case Constants.STEP_6:
				url = Constants.OPERATING_ASSUMPTION;
				break;

			case (Constants.STATUS_SEND_FOR_APPROVAL):
				url = Constants.SUMMARY_SHEET;
				break;
			case (Constants.STATUS_ACCEPTED):
				url = Constants.SUMMARY_SHEET;
				break;
			case (Constants.STATUS_REJECTED):
				url = Constants.SUMMARY_SHEET;
				break;
			case (Constants.STATUS_ARCHIVED):
				url = Constants.SUMMARY_SHEET;
				break;

			default:
				url = Constants.SUMMARY_SHEET;
			}
			updatedUrl = url + "/" + projectId;
		} else if (feasibilityState.equals(Constants.POST_SIGNING)) {
			switch (status) {

			case Constants.STEP_1:
				url = Constants.DEVELOPMENT_DETAILS_POST_SINGING;
				break;

			case Constants.STEP_2:
				url = Constants.DEVELOPER_DETAILS_POST_SINGING;
				break;

			case Constants.STEP_3:
				url = Constants.UPCOMING_CINEMAS_POST_SINGING;
				break;

			case Constants.STEP_4:
				url = Constants.LESSE_LESSE_POST_SINGING;
				break;

			case Constants.STEP_5:
				url = Constants.PROJECT_LAYOUT_POST_SINGING;
				break;

			case Constants.STEP_6:
				url = Constants.OPERATING_ASSUMPTION_POST_SINGING;
				break;

			case (Constants.STATUS_SEND_FOR_APPROVAL):
				url = Constants.SUMMARY_SHEET_POST_SINGING;
				break;
			case (Constants.STATUS_ACCEPTED):
				url = Constants.SUMMARY_SHEET;
				break;
			case (Constants.STATUS_REJECTED):
				url = Constants.SUMMARY_SHEET;
				break;
			case (Constants.STATUS_ARCHIVED):
				url = Constants.SUMMARY_SHEET;
				break;
			default:
				url = Constants.SUMMARY_SHEET;
			}
			updatedUrl = url + "/" + projectId;
		} else if (feasibilityState.equals(Constants.PRE_HANDOVER)) {
			switch (status) {

			case Constants.STEP_1:
				url = Constants.DEVELOPMENT_DETAILS_POST_SINGING;
				break;

			case Constants.STEP_2:
				url = Constants.DEVELOPER_DETAILS_POST_SINGING;
				break;

			case Constants.STEP_3:
				url = Constants.UPCOMING_CINEMAS_POST_SINGING;
				break;

			case Constants.STEP_4:
				url = Constants.LESSE_LESSE_POST_SINGING;
				break;

			case Constants.STEP_5:
				url = Constants.PROJECT_LAYOUT_POST_SINGING;
				break;

			case Constants.STEP_6:
				url = Constants.OPERATING_ASSUMPTION_POST_SINGING;
				break;

			case (Constants.STATUS_SEND_FOR_APPROVAL):
				url = Constants.SUMMARY_SHEET_POST_SINGING;
				break;
			case (Constants.STATUS_ACCEPTED):
				url = Constants.SUMMARY_SHEET;
				break;
			case (Constants.STATUS_REJECTED):
				url = Constants.SUMMARY_SHEET;
				break;
			case (Constants.STATUS_ARCHIVED):
				url = Constants.SUMMARY_SHEET;
				break;
			default:
				url = Constants.SUMMARY_SHEET;
			}
			updatedUrl = url + "/" + projectId;
		}

		return updatedUrl;
	}

	public static String getFeasibilityUrl(String feasibilityState) {
		if (feasibilityState.equals(Constants.PRE_SIGNING)) {
			return "project";
		}
		else if (feasibilityState.equals(Constants.POST_SIGNING)) {
			return "post-signing";
		}
		else if (feasibilityState.equals(Constants.PRE_HANDOVER)) {
			return "pre-handover";
		}
		return feasibilityState;
	}
}
