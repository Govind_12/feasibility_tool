package com.util;


public class MarkConstant {

	public final static String IS_ERROR = "isError";
	public final static String STATUS_CODE = "status";
	public final static String MESSAGE = "message";
	public final static String DATA = "data";

	public final static String CONTENT_TYPE = "Content-Type";
	public final static String AUTH_HEADER = "X-AUTH-HEADER";

	public final static String EMAIL = "email";
	public final static String U_KEY = "username";
	public final static String P_KEY = "password";
	
	

}
