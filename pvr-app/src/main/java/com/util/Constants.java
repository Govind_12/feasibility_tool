package com.util;

public class Constants {

	public static final int STATUS_ACTIVE = 1;
	public static final String ADMIN = "ADMIN"; 
	
	public static final String AUTH_TOKEN = "token";
	public static final String USER = "user";
	public static final String ID = "id";
	public static final String USER_ID = "userId";
	public static final String CODE = "code";
	public static final String CRN = "crn";
	public static final String AUTHORITIES = "authorities";
	public static final String COMPANY_NAME = "companyName";
	public static final String AUTHORITY = "authority";

	public final static String ADMIN_REQUEST = "adminRequest";
	
	public final static String X_AUTH_HEADER = "X-AUTH-HEADER";
	public static final String STATUS_CODE = "status";
	public static final String DATA = "data";

	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_BD = "ROLE_BD";
	public static final String ROLE_APPROVER = "ROLE_APPROVER";
	public static final String ROLE_OPERATION = "ROLE_OPERATION_TEAM";
	public static final String ROLE_PROJECT_TEAM = "ROLE_PROJECT_TEAM";
	
	public static final String USER_PROFILE_IMAGE_NAME = "hashedUserImage";
	
	public final static String INVALID_USER_MESSAGE = "Invalid User.";
	
	public final static int  INVALID_USER_CODE= 1001;
	
	
}
