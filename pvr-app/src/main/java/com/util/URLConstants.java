package com.util;


public class URLConstants {

	public final static String AUTHORIZATION_URL = "auth";

	public final static String SAVE_COMPANY_DETAILS = "company";

	public final static String GET_COMPAINES = "company/all";
	
	public final static String GET_COMPANY_DETAIL_BY_ID = "company/getCompanyById/%s";

	public final static String SAVE_USER_DETAILS = "user";

	public final static String GET_USERS = "user/all";

	public final static String GET_USER_BY_USER_NAME = "user/username/%s";
	public final static String GET_COMPANY_OWNER = "user/company-owner-crn/%s";

	public final static String VALIDATE_BY_CRN_NO = "company/validateByCRNno/%s";

	public final static String FETCH_COMPANY_BY_CRN_NO = "company/%s";

	public final static String GET_USER_DETAIL_BY_ID = "user/%s";
	
	public final static String GET_USER_EMPLOYEE_DETAIL_BY_ID = "user/getUserEmployeeDetailById/%s";
	
	public final static String GET_USER_BY_AUTHORITY = "user/getUserByAuthority/%s";

	
	public final static String SAVE_ALL_CINEMA_MASTER = "upload/cinema_master";
	public final static String SAVE_ALL_COMPITION_MASTER = "upload/compition_master";
	public final static String SAVE_ALL_BBDO_MASTER = "upload/bbdo_master";
	
	public final static String SAVE_STEP_1 = "projectDetails/step1";
	public final static String SAVE_STEP_2 = "projectDetails/step2";
	public final static String SAVE_STEP_3 = "projectDetails/step3";
	public final static String SAVE_STEP_4 = "projectDetails/step4";
	public final static String SAVE_STEP_5 = "projectDetails/step5";
	public final static String SAVE_STEP_6 = "projectDetails/step6";
	public final static String SAVE_STEP_7 = "projectDetails/step7";
	public final static String SAVE_CLOSE_STEP_5 = "projectDetails/save-close-step5";
	
	public final static String GET_PROJECT_DETAIL_BY_ID = "projectDetails/getProjectById/%s";
	public final static String GET_ALL_PROJECT_DETAILS = "projectDetails/get-all-project-detail";
	public final static String GET_PRE_SIGNING_PROJECT_BY_STATUS = "projectDetails/get-pre-signing-project-by-status";
	
	public final static String SAVE_ALL_CITY_TIER = "upload/city_tier";
	public final static String SAVE_ALL_NAVISION_MASTER = "upload/navision-master";
	public final static String SAVE_ALL_CATEGORY_MASTER = "upload/category-master";
	public final static String SAVE_ALL_PROJECT_COST = "upload/projectCostSummary";
	public final static String SAVE_ALL_SEAT_CATEGORY_MASTER = "upload/seatCategory-master";
	public final static String GET_PROJECT_COST_SUMMARY = "upload/getAllProjectCostSummary/%s";
	
	public final static String GET_ALL_CITIES = "city/get-all-city";
	public final static String GET_STATE_BY_CITY = "city/get-all-city?q=city_name:%s";
	
	public final static String GET_DASHBOARD_DETAILS = "dashboard";
	
	public final static String APPROVE_PROJECT = "projectDetails/approve-project/%s";
	
	public final static String REJECT_PROJECT = "projectDetails/reject-project/%s";
	public final static String GET_NOTIFICATIONS = "notification/%s";
	
	public final static String GET_PROJECT__BY_ID = "projectDetails/getProjectDetailsById/%s";
	
	public final static String QUERY_BY_PROJECT_ID =  "projectDetails/get-query-by-project-id/%s";
	

	public final static String SAVE_MULTIPLIER_DETAILS = "multiplier";
	public final static String FETCH_MUTIPLIER_BY_ID = "multiplier/%s";
	public final static String GET_MUTIPLIERS = "multiplier/all";
	
	public final static String SAVE_QUERY = "projectDetails/save-query";
	/*public final static String DELETE_PROJECT_WITH_NOTIFICATION = "projectDetails/delete-project/%s";*/
	public final static String UPDATE_PROJECT_WITH_NOTIFICATION = "projectDetails/updateProjectAndNotificationStatus/%s/%s";
	public final static String GET_LATEST_PROJECT = "projectDetails/getLatestProject/%s?reportId=%s";
	public final static String GET_FIXED_CONSTANTS = "fixedConstants";
	
	public final static String SAVE_SERVICE_CHARGE = "fixedConstants/serviceCharge";
	public final static String SAVE_GROWTH_ASSUMPTION = "fixedConstants/saveGrowthAssumptions";
	public final static String SAVE_CAPEX_ASSUMPTION = "fixedConstants/capexAssumption";
	public final static String SAVE_TAX_ASSUMPTION = "fixedConstants/taxAssumption";
	public final static String SAVE_OPERATING_ASSUMPTION = "fixedConstants/operatAssumption";
	
	public final static String SAVE_ATPCAP_ASSUMPTION = "fixedConstants/atpCap";
	
	public final static String SAVE_VPF_ASSUMPTION = "fixedConstants/capexAssumption";
	public final static String GET_VPF_CONSTANTS = "fixedConstants";
	
	public final static String SAVE_FIXED_CONSTANT = "fixedConstants/irr-constant-details";
	
	// post-signing
	public final static String GET_ALL_POST_SIGNING_EXISTING_PROJECT = "post-signing/get-all-project";
	
	public final static String SAVE_DEVELOPMENT_DETAILS = "post-signing/development-details";
	
	public final static String SAVE_PRE_HANDOVER_DEVELOPMENT_DETAILS = "pre-handover/development-details";
	
	public final static String GET_ALL_PRE_HANDOVER_EXISTING_PROJECT = "pre-handover/get-all-project";
	
	// Modal service details
	
	public final static String GET_TOTAL_ADMITS = "view-modal/get-total-admits/%s";

	public static final String GET_AD_REVENUE = "view-modal/get-ad-revenue/%s";

	public static final String GET_PERSONAL_EXPENSES = "view-modal/get-personal-expenses/%s";

	public static final String GET_RM_EXPENSES = "view-modal/get-repair-maintaince-expenses/%s";

	public static final String GET_ELECTRITY_EXPENSES = "view-modal/get-water-electricity-expenses/%s";

	public static final String GET_WEB_SALES = "view-modal/get-websale/%s";

	public static final String GET_HOUSEKEPING = "view-modal/get-housekeeping/%s";

	public static final String GET_MISCELLANEOUS_EXPENSE = "view-modal/get-miscellaneous/%s";

	public static final String GET_SECURITY_EXPENSE = "view-modal/get-security/%s";

	public static final String GET_COMMUNICATION_EXPENSE = "view-modal/get-communnication/%s";

	public static final String GET_TRAVELLING_EXPENSE = "view-modal/get-travelling/%s";

	public static final String GET_LEEGAL_FEE = "view-modal/get-legal/%s";

	public static final String GET_INSURANCE_EXPENSE = "view-modal/get-insurance/%s";

	public static final String GET_INTERNET_EXPENSES = "view-modal/get-internet/%s";

	public static final String GET_PRINTING_STATIONARY = "view-modal/get-printingAndStationary/%s";

	public static final String GET_MARKETING_EXPENSE = "view-modal/get-marketing/%s";
	
	public static final String DELETE_PROJECT_DETAILS_BY_ID = "projectDetails/deleteProjectById/%s";

	public static final String GET_ANNUAL_RENT = "view-modal/get-annual-rent/%s";
	
	public final static String GET_ALL_OLD_REPORTS = "projectDetails/access-all-reports";

	public final static String SEND_FOR_OPERATING_ASSUMPTIONS = "projectDetails/operating-assumtion-for-all";
	
	public static final String GET_VIEW_CALCULATION = "view-modal/get-calculation/%s";
	public static final String GET_VIEW_OCCUPANCY = "view-modal/get-occupancy/%s";
	
	
	public final static String REJECT_PROJECT_DETAILS = "projectDetails/reject";

	public static final String UPDATE_PROJECT = "projectDetails/update-project/%s";
	
	public final static String GET_ALL_CINEMA_MASTER_DETAILS = "projectDetails/get-all-cinema-master-details";

	public static final String DOWNLOAD_PDF = "report-download/generate/%s";
	
	public final static String GET_EXISTING_PROJECT = "projectDetails/get-existing-project";
	
	public final static String PRE_HANDOVER_SAVE_QUERY = "pre-handover/save-query";
	
	public final static String PRE_HANDOVER_SAVE_QUERY_FINAL = "pre-handover/save-query-final";

	public final static String SEND_FOR_OPREATION_PROJECT_TEAM = "pre-handover/send-for-project-or-operation";
	

	public static final String UPDATE_NOTIFICATION_STATUS = "pre-handover/updateNotificationStatus/%s/%s/%s";

	public final static String GET_USER_DETAIL_BY_APPROVER_ID = "user/approver-id/%s";

	public static final String SAVE_FHC =  "admin/fhc";

	public static final String IS_FHC_PRESENT ="admin/isFhcPresent";
	
	public static final String GET_ALL_CINEMA_FORMAT_MASTER ="admin/get-cinemaFormat-List";
	
	public static final String GET_CINEMA_FORMAT_MASTER ="admin/get-cinemaFormat/%s";
	
	public static final String SAVE_CINEMA_FORMAT =  "admin/save/cinemaFormat";
	
    public static final String GET_ALL_CINEMA_CATEGORY_MASTER ="admin/get-cinemaCategory-List";
	
	public static final String GET_CINEMA_CATEGORY_MASTER ="admin/get-cinemaCategory/%s";
	
	public static final String SAVE_CINEMA_CATEGORY =  "admin/save/cinemaCategory";
	
    public static final String GET_ALL_SEAT_TYPE ="admin/get-seatType-List";
	
	public static final String GET_SEAT_TYPE_MASTER ="admin/get-seatType/%s";
	
	public static final String SAVE_SEAT_TYPE =  "admin/save/seatType";

	public final static String DELETE_ATP = "admin/delete-atp";
	
	public static final String SAVE_COGS =  "admin/cogs";

	public static final String DELETE_OPERATING_ASSUPTION = "admin/delete-operating-assumption";

	public static final String SAVE_GST_ON_TICKET = "admin/gst-on-ticket";

	public static final String IF_MULTIPLIER_PRESENT = "multiplier/if-multiplier_present";

	public static final String FETCH_ALL_PROJECT_DETAILS = "projectDetails/fetch-all-project-detail";

	public static final String FORGOT_PASSWORD = "user/forgot-password";
	
	public static final String GET_FIXED_CONSTANTS_FOR_OPERATION_TAB = "projectDetails/operatingAssumptions/getRevenueAssumptionConstant/%s";

	public static final String SAVE_LOG_DETAILS = "admin/save-log-details";
	
	public final static String GET_LOGS = "admin/log-detail";
	
	public final static String SAVE_EXECUTIVE_NOTE = "projectDetails/save-executive-note/";
	
	public static final String SAVE_NEW_SCREEN_TYPE =  "admin/save/screen/type";

}
