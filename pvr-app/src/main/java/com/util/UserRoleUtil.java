package com.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.web.FeasibilityWebException;


@Component
public class UserRoleUtil {
	

	public static String getCurrentUserCrn() {

		try {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		
		HttpSession session = attr.getRequest().getSession(false);
		
		//System.out.println(new Gson().toJson(session));
		String crn = session.getAttribute(Constants.CRN).toString();
		if (crn.length() > 2) {
			return crn.substring(1, crn.length() - 1);
		}
		}catch (Exception e) {
			throw new FeasibilityWebException(Constants.INVALID_USER_MESSAGE, Constants.INVALID_USER_CODE);

		}
		return null;
	}

	public static String getCurrentUserId() {

		try {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(false);
		
		String userId = session.getAttribute(Constants.USER_ID).toString();
		if (userId.length() > 2) {
			return userId.substring(1, userId.length() - 1);
		}
		return userId;
		}catch (Exception e) {
			throw new FeasibilityWebException(Constants.INVALID_USER_MESSAGE, Constants.INVALID_USER_CODE);

		}
	}

	public static String getCurrentUser() {

		try {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(false);
		
		return session.getAttribute(Constants.USER).toString();
		}catch (Exception e) {
			throw new FeasibilityWebException(Constants.INVALID_USER_MESSAGE, Constants.INVALID_USER_CODE);
		}
	}

	public static String generateNewReportId() {
		String reportId = "N-";

		String yearString = (DateUtil.getCurrentYear() + "").substring(2);
		int month = DateUtil.getCurrentMonth();
		String monthString = month + "";
		/*
		 * if(month < 10) { monthString = "0"+ month+""; }
		 */

		int day = DateUtil.getCurrentDay();
		String dayString = day + "";
		/*
		 * if(day < 10) { dayString = "0"+day+""; }
		 */
		int hour = DateUtil.getCurrentHour();
		String hourString = hour + "";

		int minute = DateUtil.getCurrentMinute();
		String minuteString = minute + "";

		reportId += yearString + monthString + dayString + hourString + minuteString;

		return reportId;

	}

	public static boolean isCurrentUserApprover() {

		try {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(false);
		String authoritiesString = session.getAttribute(Constants.AUTHORITIES).toString();
		if (authoritiesString.contains(Constants.ROLE_APPROVER)) {
			return true;
		}
		}catch (Exception e) {
			throw new FeasibilityWebException(Constants.INVALID_USER_MESSAGE, Constants.INVALID_USER_CODE);

		}
		return false;
	}

	public static boolean isCurrentUserAdmin() {

		try {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(false);
		
		//System.out.println("session AUTHORITIES: " + session.getAttribute(Constants.AUTHORITIES));
		String authoritiesString = session.getAttribute(Constants.AUTHORITIES).toString();
		if (authoritiesString.contains(Constants.ROLE_ADMIN)) {
			return true;
		}
		}catch (Exception e) {
			throw new FeasibilityWebException(Constants.INVALID_USER_MESSAGE, Constants.INVALID_USER_CODE);

		}
		return false;
	}

	public static boolean isCurrentUserIsRoleAdmin() {

		try {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(false);
		
		String authoritiesString = session.getAttribute(Constants.AUTHORITIES).toString();
		if (authoritiesString.contains(Constants.ROLE_ADMIN)) {
			return true;
		}
		}catch (Exception e) {
			throw new FeasibilityWebException(Constants.INVALID_USER_MESSAGE, Constants.INVALID_USER_CODE);

		}
		return false;
	}

	public static boolean isCurrentUserBD() {

		try {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(false);
		
		//System.out.println("session AUTHORITIES: " + session.getAttribute(Constants.AUTHORITIES));
		String authoritiesString = session.getAttribute(Constants.AUTHORITIES).toString();
		if (authoritiesString.contains(Constants.ROLE_BD)) {
			return true;
		}
		}catch (Exception e) {
			throw new FeasibilityWebException(Constants.INVALID_USER_MESSAGE, Constants.INVALID_USER_CODE);

		}
		return false;
	}
	
	
	public static boolean isCurrentUserOperation() {

		try {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(false);
		
		//System.out.println("session AUTHORITIES: " + session.getAttribute(Constants.AUTHORITIES));
		String authoritiesString = session.getAttribute(Constants.AUTHORITIES).toString();
		if (authoritiesString.contains(Constants.ROLE_OPERATION)) {
			return true;
		}
		}catch (Exception e) {
			throw new FeasibilityWebException(Constants.INVALID_USER_MESSAGE, Constants.INVALID_USER_CODE);

		}
		return false;
	}
	
	public static boolean isCurrentUserProjectTeam() {

		try {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(false);
		//System.out.println("session AUTHORITIES: " + session.getAttribute(Constants.AUTHORITIES));
		String authoritiesString = session.getAttribute(Constants.AUTHORITIES).toString();
		if (authoritiesString.contains(Constants.ROLE_PROJECT_TEAM)) {
			return true;
		}
		}catch (Exception e) {
			throw new FeasibilityWebException(Constants.INVALID_USER_MESSAGE, Constants.INVALID_USER_CODE);

		}
		return false;
	}
	
	
	
	
	public static List<Integer> getAllApproverType(){
		List<Integer> list=new ArrayList<>();
		list.add(106);
		list.add(107);
		list.add(210);
		list.add(211);
		list.add(212);
		list.add(213);
		list.add(214);
		list.add(215);
		return list;
	}
}