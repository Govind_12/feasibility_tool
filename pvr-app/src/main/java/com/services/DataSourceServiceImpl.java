
package com.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import com.common.constants.Constants;
import com.common.mappers.UploadFilesMapper;
import com.common.models.AllIndiaRank;
import com.common.models.BbdoMaster;
import com.common.models.CategoryDataSource;
import com.common.models.CinemaMaster;
import com.common.models.CityTier;
import com.common.models.CompitionMaster;
import com.common.models.Credit;
import com.common.models.Employement;
import com.common.models.FactorAs;
import com.common.models.IndicatorAsAcross;
import com.common.models.IndicatorAsPerCapita;
import com.common.models.Navision;
import com.common.models.ProjectCostData;
import com.common.models.ProjectCostSummary;
import com.common.models.ShareInState;
import com.common.sql.models.SeatCategory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.MarkConstant;
import com.util.RestServiceUtil;
import com.util.URLConstants;
import com.web.FeasibilityWebException;

import lombok.extern.slf4j.Slf4j;

@Service("dataSourceServiceimpl")
@Slf4j
public class DataSourceServiceImpl implements DataSourceService {

	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@Autowired
	private RestServiceUtil restServiceUtil;

	@Override
	public void saveDataSource(UploadFilesMapper uploadFilesMapper) {

		if (uploadFilesMapper.getType() == Constants.CINEMA_MASTER) {
			uploadCinemaMaster(uploadFilesMapper);
		} else if (uploadFilesMapper.getType() == Constants.BBDO_MASTER) {
			uploadBbdoMaster(uploadFilesMapper);
		} else if (uploadFilesMapper.getType() == Constants.COMPITION_MASTER) {
			uploadCompitionMaster(uploadFilesMapper);
		} else if (uploadFilesMapper.getType() == Constants.CITY_TIER_MASTER) {
			uploadCityTierMaster(uploadFilesMapper);
		} else if (uploadFilesMapper.getType() == Constants.NAVISION_MASTER) {
			uploadNavisionMaster(uploadFilesMapper);
		} else if (uploadFilesMapper.getType() == Constants.CATEGORY_MASTER) {
			uploadCategoryMaster(uploadFilesMapper);
		} else if (uploadFilesMapper.getType() == Constants.SEAT_CATEGORY) {
			uploadSeatCategoryMaster(uploadFilesMapper);
		}

	}

	public void uploadCinemaMaster(UploadFilesMapper uploadFilesMapper) {

		if (ObjectUtils.isEmpty(uploadFilesMapper.getFile())) {
			throw new FeasibilityWebException("file info cannot be empty");
		}
		int status;
		try {

			List<CinemaMaster> cinemaMaster = getAllCinemaMater(uploadFilesMapper.getFile());
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_ALL_CINEMA_MASTER, cinemaMaster, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving company information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving company information");
			throw new FeasibilityWebException("Error while saving company information");
		}
	}

	public List<CinemaMaster> getAllCinemaMater(MultipartFile file) {
		Workbook dataSource;
		List<CinemaMaster> cinemaMapperList = new ArrayList<>();

		try {
			String fileName = file.getOriginalFilename().toLowerCase();
			if (fileName.endsWith(".xlsx")) {
				dataSource = new XSSFWorkbook(file.getInputStream());

			} else {
				dataSource = new HSSFWorkbook(file.getInputStream());
			}

			for (int i = 0; i < dataSource.getNumberOfSheets(); i++) {
				Sheet sheet = dataSource.getSheetAt(i);
				for (Row row : sheet) {
					if (row.getRowNum() != 0) {

						CinemaMaster cinema = new CinemaMaster();
						double[] doubleList = new double[2];
						for (Cell cell : row) {
							if (cell != null) {
								if (cell.getColumnIndex() == 0) {
									Double d = cell.getNumericCellValue();
									cinema.setHopk(d.intValue());
								}
								if (cell.getColumnIndex() == 1) {
									cinema.setCinema_strCode(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 2) {
									cinema.setCinema_name(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 3) {
									cinema.setVista_info_Cinema_Name(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 4) {
									cinema.setCinema_location(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 5) {
									cinema.setState_strName(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 6) {
									cinema.setNavCinemaCode(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 7) {
									cinema.setOwner_strCompany(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 8) {
									cinema.setCinema_Resign(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 9) {
									cinema.setNav_cinemadesc(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 10) {
									cinema.setCinema_str_Run(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 11) {
									Double d2 = cell.getNumericCellValue();
									doubleList[0] = d2.doubleValue();
								}
								if (cell.getColumnIndex() == 12) {
									Double d3 = cell.getNumericCellValue();
									doubleList[1] = d3.doubleValue();
								}
								if (cell.getColumnIndex() == 13) {
									Double d1 = cell.getNumericCellValue();
									cinema.setLoyalty_complex_id(d1.intValue());
								}
								if (cell.getColumnIndex() == 14) {
									Double d2 = cell.getNumericCellValue();
									cinema.setNoOfScreen(d2.intValue());
								}
								if (cell.getColumnIndex() == 15) {
									Double d3 = cell.getNumericCellValue();
									cinema.setGold(d3.intValue());
								}
								if (cell.getColumnIndex() == 16) {
									Double d4 = cell.getNumericCellValue();
									cinema.setPxl(d4.intValue());
								}
								if (cell.getColumnIndex() == 17) {
									Double d5 = cell.getNumericCellValue();
									cinema.setUltraPremium(d5.intValue());
								}
								if (cell.getColumnIndex() == 18) {
									Double d6 = cell.getNumericCellValue();
									cinema.setDx(d6.intValue());
								}
								if (cell.getColumnIndex() == 19) {
									Double d7 = cell.getNumericCellValue();
									cinema.setPremier(d7.intValue());
								}
								if (cell.getColumnIndex() == 20) {
									Double d8 = cell.getNumericCellValue();
									cinema.setImax(d8.intValue());
								}
								if (cell.getColumnIndex() == 21) {
									Double d9 = cell.getNumericCellValue();
									cinema.setPlayHouse(d9.intValue());
								}
								if (cell.getColumnIndex() == 22) {
									Double d10 = cell.getNumericCellValue();
									cinema.setOnxy(d10.intValue());
								}
								if (cell.getColumnIndex() == 23) {
									Double d11 = cell.getNumericCellValue();
									cinema.setMainStream(d11.intValue());
								}
								if (cell.getColumnIndex() == 24) {
									cinema.setCinemaCategory(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 25) {
									Double d12 = cell.getNumericCellValue();
									cinema.setTotalCapacity(d12.intValue());
								}

								cinema.setLocation(doubleList);
							}

						}
						cinemaMapperList.add(cinema);

					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return cinemaMapperList;
	}

	public void uploadCompitionMaster(UploadFilesMapper uploadFilesMapper) {

		if (ObjectUtils.isEmpty(uploadFilesMapper.getFile())) {
			throw new FeasibilityWebException("file info cannot be empty");
		}
		int status;
		try {

			List<CompitionMaster> compitionMaster = getAllCompitionMaster(uploadFilesMapper.getFile());
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_ALL_COMPITION_MASTER, compitionMaster,
					null, HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving compition master information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving compition master information");
			throw new FeasibilityWebException("Error while saving compition master information");
		}
	}

	public List<CompitionMaster> getAllCompitionMaster(MultipartFile file) {
		Workbook dataSource;
		List<CompitionMaster> compitionMasterList = new ArrayList<>();

		try {
			String fileName = file.getOriginalFilename().toLowerCase();
			if (fileName.endsWith(".xlsx")) {
				dataSource = new XSSFWorkbook(file.getInputStream());

			} else {
				dataSource = new HSSFWorkbook(file.getInputStream());
			}

			for (int i = 0; i < dataSource.getNumberOfSheets(); i++) {
				Sheet sheet = dataSource.getSheetAt(i);
				for (Row row : sheet) {
					
					if (row.getRowNum() > 1) {

						CompitionMaster compitionMaster = new CompitionMaster();
						for (Cell cell : row) {

							if (cell != null) {

								switch (cell.getColumnIndex()) {

								case Constants.COLUMN_INDEX_0:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										compitionMaster.setCompititionName(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_1:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										compitionMaster.setCity(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_2:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										compitionMaster.setCompitionChain(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_3:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double screen = cell.getNumericCellValue();
										compitionMaster.setNoOfScreen(screen.intValue());
									}
									break;
								case Constants.COLUMN_INDEX_4:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double seat = cell.getNumericCellValue();
										compitionMaster.setSeatCapacity(seat.longValue());
									}
									break;
								case Constants.COLUMN_INDEX_5:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double admits = cell.getNumericCellValue();
										compitionMaster.setAdmits(admits.longValue());
									}
									break;
								case Constants.COLUMN_INDEX_6:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double atp = cell.getNumericCellValue();
										compitionMaster.setAtp(atp.intValue());
									}
									break;
								case Constants.COLUMN_INDEX_7:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										compitionMaster.setOccupancy(cell.getNumericCellValue());
									}
									break;
								}
							}
						}
						
						if(!ObjectUtils.isEmpty(compitionMaster.getCompititionName())) {
							compitionMasterList.add(compitionMaster);
						}
						
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return compitionMasterList;
	}

	public void uploadBbdoMaster(UploadFilesMapper uploadFilesMapper) {

		if (ObjectUtils.isEmpty(uploadFilesMapper.getFile())) {
			throw new FeasibilityWebException("file info cannot be empty");
		}
		int status;
		try {

			List<BbdoMaster> bbdoMaster = getAllBbdoMaster(uploadFilesMapper.getFile());
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_ALL_BBDO_MASTER, bbdoMaster, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving bbdo master information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving compition master information");
			throw new FeasibilityWebException("Error while saving bbdo master information");
		}
	}

	public List<BbdoMaster> getAllBbdoMaster(MultipartFile file) {
		Workbook dataSource;
		List<BbdoMaster> bbdoMasterList = new ArrayList<>();

		try {
			String fileName = file.getOriginalFilename().toLowerCase();
			if (fileName.endsWith(".xlsx")) {
				dataSource = new XSSFWorkbook(file.getInputStream());

			} else {
				dataSource = new HSSFWorkbook(file.getInputStream());
			}

			for (int i = 0; i < dataSource.getNumberOfSheets(); i++) {
				Sheet sheet = dataSource.getSheetAt(i);

				for (Row row : sheet) {
					// System.out.println("before row "+ row.getRowNum());
					if (row.getRowNum() > 3) {
						// System.out.println("after row "+row.getRowNum());
						BbdoMaster bbdoMaster = new BbdoMaster();

						for (Cell cell : row) {

							if (cell != null || cell.getCellType() != Cell.CELL_TYPE_BLANK) {

								AllIndiaRank al = new AllIndiaRank();
								ShareInState sis = new ShareInState();
								FactorAs fa = new FactorAs();
								IndicatorAsAcross iac = new IndicatorAsAcross();
								IndicatorAsPerCapita indicatorAspc = new IndicatorAsPerCapita();
								Employement employeement = new Employement();
								Credit credit = new Credit();

								if (cell.getColumnIndex() == 2) {
									bbdoMaster.setState(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 3) {
									bbdoMaster.setTown(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 4) {
									bbdoMaster.setGrade(cell.getStringCellValue());
								}

								if (cell.getColumnIndex() == 5) {
									Double d = cell.getNumericCellValue();
									al.setMii(d.intValue());
								}

								if (cell.getColumnIndex() == 6) {
									Double d2 = cell.getNumericCellValue();
									al.setMpv(d2.intValue());
									bbdoMaster.setAllIndiaRank(al);
								}

								if (cell.getColumnIndex() == 7) {
									Double d3 = cell.getNumericCellValue();
									bbdoMaster.setMpv(d3.intValue());
								}

								if (cell.getColumnIndex() == 8) {
									Double d4 = cell.getNumericCellValue();
									bbdoMaster.setMii(d4.intValue());
								}

								if (cell.getColumnIndex() == 9) {
									Double d5 = cell.getNumericCellValue();
									bbdoMaster.setMei(d5.intValue());
								}

								if (cell.getColumnIndex() == 10) {
									Double d6 = cell.getNumericCellValue();
									bbdoMaster.setPopn(d6.intValue());
								}

								if (cell.getColumnIndex() == 11) {
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double d7 = cell.getNumericCellValue();
										bbdoMaster.setPopnHalfyearly(d7.intValue());
									}

								}

								if (cell.getColumnIndex() == 12) {
									Double d8 = cell.getNumericCellValue();
									sis.setMpv(d8.intValue());
								}

								if (cell.getColumnIndex() == 13) {
									Double d9 = cell.getNumericCellValue();
									sis.setPopn(d9.intValue());

								}

								if (cell.getColumnIndex() == 14) {
									Double d10 = cell.getNumericCellValue();
									bbdoMaster.setAllIndiaranks(d10.intValue());
								}

								if (cell.getColumnIndex() == 15) {
									Double d11 = cell.getNumericCellValue();
									fa.setMeans(d11.intValue());
								}

								if (cell.getColumnIndex() == 16) {
									Double d12 = cell.getNumericCellValue();
									fa.setConsumption(d12.intValue());
								}

								if (cell.getColumnIndex() == 17) {
									Double d13 = cell.getNumericCellValue();
									fa.setAwareness(d13.intValue());
								}
								if (cell.getColumnIndex() == 18) {
									Double d14 = cell.getNumericCellValue();
									fa.setMktSupport(d14.intValue());

								}
								if (cell.getColumnIndex() == 19) {
									Double d15 = cell.getNumericCellValue();
									indicatorAspc.setIncome(d15.intValue());
								}

								if (cell.getColumnIndex() == 20) {
									Double d16 = cell.getNumericCellValue();
									indicatorAspc.setBankDeposit(d16.intValue());
								}

								if (cell.getColumnIndex() == 21) {
									Double d17 = cell.getNumericCellValue();
									indicatorAspc.setAffluentPopn(d17.intValue());

								}

								if (cell.getColumnIndex() == 22) {
									Double d18 = cell.getNumericCellValue();
									iac.setCd1(d18.intValue());
								}

								if (cell.getColumnIndex() == 23) {
									Double d19 = cell.getNumericCellValue();
									iac.setCd2(d19.intValue());
								}

								if (cell.getColumnIndex() == 24) {
									Double d20 = cell.getNumericCellValue();
									iac.setCd3(d20.intValue());
								}
								if (cell.getColumnIndex() == 25) {
									Double d21 = cell.getNumericCellValue();
									iac.setCar(d21.intValue());
								}
								if (cell.getColumnIndex() == 26) {
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double d22 = cell.getNumericCellValue();
										/*System.out.println(d22);*/
										iac.setPhone(d22.floatValue());
									}
								}
								if (cell.getColumnIndex() == 27) {
									Double d23 = cell.getNumericCellValue();
									iac.setFmcg(d23.intValue());

								}
								if (cell.getColumnIndex() == 28) {
									Double d24 = cell.getNumericCellValue();
									employeement.setTrade(d24.intValue());
								}

								if (cell.getColumnIndex() == 29) {
									Double d25 = cell.getNumericCellValue();
									employeement.setTransport(d25.intValue());
									iac.setEmployment(employeement);
								}

								if (cell.getColumnIndex() == 30) {
									Double d26 = cell.getNumericCellValue();
									credit.setTrade(d26.intValue());
								}

								if (cell.getColumnIndex() == 31) {
									Double d27 = cell.getNumericCellValue();
									credit.setTransport(d27.intValue());

								}

								if (cell.getColumnIndex() == 32) {
									Double d28 = cell.getNumericCellValue();
									bbdoMaster.setTv(d28.intValue());
								}

								if (cell.getColumnIndex() == 33) {
									Double d29 = cell.getNumericCellValue();
									bbdoMaster.setRadio(d29.intValue());
								}

								if (cell.getColumnIndex() == 34) {
									Double d30 = cell.getNumericCellValue();
									bbdoMaster.setCinema(d30.intValue());
								}

								if (cell.getColumnIndex() == 35) {
									Double d31 = cell.getNumericCellValue();
									bbdoMaster.setPrintMedia(d31.intValue());
								}

								if (cell.getColumnIndex() == 36) {
									Double d32 = cell.getNumericCellValue();
									bbdoMaster.setFemaleLiteracy(d32.intValue());
								}

								bbdoMaster.setShareInState(sis);
								bbdoMaster.setFactorAsMpv(fa);
								bbdoMaster.setIndicatorAsperCapita(indicatorAspc);
								bbdoMaster.setIndicatorAsCross(iac);
								iac.setCredit(credit);
								bbdoMaster.setIndicatorAsCross(iac);
							}

						}

						bbdoMasterList.add(bbdoMaster);

					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return bbdoMasterList;
	}

	public void uploadCityTierMaster(UploadFilesMapper uploadFilesMapper) {

		if (ObjectUtils.isEmpty(uploadFilesMapper.getFile())) {
			throw new FeasibilityWebException("file info cannot be empty");
		}
		int status;
		try {

			List<CityTier> cinemaMaster = getAllCityTierMaster(uploadFilesMapper.getFile());
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_ALL_CITY_TIER, cinemaMaster, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving city-tier information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving city-tier information");
			throw new FeasibilityWebException("Error while saving city-tier information");
		}
	}

	public List<CityTier> getAllCityTierMaster(MultipartFile file) {
		Workbook dataSource;
		List<CityTier> cityTierMapperList = new ArrayList<>();

		try {
			String fileName = file.getOriginalFilename().toLowerCase();
			if (fileName.endsWith(".xlsx")) {
				dataSource = new XSSFWorkbook(file.getInputStream());

			} else {
				dataSource = new HSSFWorkbook(file.getInputStream());
			}

			for (int i = 0; i < dataSource.getNumberOfSheets(); i++) {
				Sheet sheet = dataSource.getSheetAt(i);
				for (Row row : sheet) {
					if (row.getRowNum() > 0) {

						CityTier cityTier = new CityTier();

						for (Cell cell : row) {
							if (cell != null) {
								if (cell.getColumnIndex() == 2) {
									cityTier.setCity(cell.getStringCellValue());
								}
								if (cell.getColumnIndex() == 4) {
									cityTier.setTier(cell.getStringCellValue());
								}
							}
						}
						cityTierMapperList.add(cityTier);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return cityTierMapperList;
	}

	public void uploadNavisionMaster(UploadFilesMapper uploadFilesMapper) {
		if (ObjectUtils.isEmpty(uploadFilesMapper.getFile())) {
			throw new FeasibilityWebException("file info cannot be empty");
		}
		int status;
		try {

			List<Navision> compMaster = getAllNavisionMaster(uploadFilesMapper.getFile());
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_ALL_NAVISION_MASTER, compMaster, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving comp-master information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving comp-master information");
			throw new FeasibilityWebException("Error while saving comp-master information");
		}
	}

	public List<Navision> getAllNavisionMaster(MultipartFile file) {
		Workbook dataSource;
		List<Navision> navisionMapperList = new ArrayList<>();

		try {
			String fileName = file.getOriginalFilename().toLowerCase();
			if (fileName.endsWith(".xlsx")) {
				dataSource = new XSSFWorkbook(file.getInputStream());
			} else {
				dataSource = new HSSFWorkbook(file.getInputStream());
			}
			/* dataSource.getNumberOfSheets() */
			for (int i = 0; i < dataSource.getNumberOfSheets(); i++) {
				Sheet sheet = dataSource.getSheetAt(i);
				// FormulaEvaluator evaluator =
				// dataSource.getCreationHelper().createFormulaEvaluator();
				for (Row row : sheet) {
					if (row.getRowNum() > 2) {

						Navision navision = new Navision();
						for (Cell cell : row) {
							if (cell != null) {
								// CellValue cellValue = evaluator.evaluate(cell);
								switch (cell.getColumnIndex()) {

								case Constants.COLUMN_INDEX_0:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										navision.setYear(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_1:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										navision.setQuater(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_2:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										navision.setMisLoc(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_3:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										navision.setNavCinemaCode(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_4:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										navision.setCinemaName(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_5:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										navision.setCity(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_6:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										navision.setState(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_7:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double d1 = cell.getNumericCellValue();
										navision.setNoOfScreen(d1.intValue());
									}
									break;
								case Constants.COLUMN_INDEX_8:
									if (cell.getCellType() == cell.CELL_TYPE_FORMULA) {
										// System.out.println("Formula is " + cell.getCellFormula());
										switch (cell.getCachedFormulaResultType()) {
										case Cell.CELL_TYPE_NUMERIC:
											// System.out.println("Last evaluated as: " + cell.getNumericCellValue());
											break;
										case Cell.CELL_TYPE_STRING:
											navision.setCompType(cell.getStringCellValue());
											// System.out.println("Last evaluated as \"" + cell.getRichStringCellValue()
											// + "\"");
											break;
										}
									}else {
										if (cell.getCellType() == cell.CELL_TYPE_STRING) {
											navision.setCompType(cell.getStringCellValue());
										}
									}
									break;
								case Constants.COLUMN_INDEX_9:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d2 = cell.getNumericCellValue();
										navision.setPersonalExpense(Math.round(d2));
									}
									break;
								case Constants.COLUMN_INDEX_10:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d3 = cell.getNumericCellValue();
										navision.setElectricAndWaterExpense(Math.round(d3));
									}
									break;
								case Constants.COLUMN_INDEX_11:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d4 = cell.getNumericCellValue();
										navision.setRAndmEngg(Math.round(d4));
									}
									break;
								case Constants.COLUMN_INDEX_12:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d5 = cell.getNumericCellValue();
										navision.setRAndmIt(Math.round(d5));
									}
									break;
								case Constants.COLUMN_INDEX_13:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d6 = cell.getNumericCellValue();
										navision.setRAndMFb(Math.round(d6));
									}
									break;
								case Constants.COLUMN_INDEX_14:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d8 = cell.getNumericCellValue();
										navision.setHouseKeepingCharges(Math.round(d8));
									}
									break;
								case Constants.COLUMN_INDEX_15:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d9 = cell.getNumericCellValue();
										navision.setSecurityCharges(Math.round(d9));
									}
									break;
								case Constants.COLUMN_INDEX_16:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d10 = cell.getNumericCellValue();
										navision.setMarketingExpenses(Math.round(d10));
									}
									break;
								case Constants.COLUMN_INDEX_17:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d11 = cell.getNumericCellValue();
										navision.setPrintingAndStationary(Math.round(d11));
									}
									break;
								case Constants.COLUMN_INDEX_18:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d12 = cell.getNumericCellValue();
										navision.setCommunicationCost(Math.round(d12));
									}
									break;
								case Constants.COLUMN_INDEX_19:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d13 = cell.getNumericCellValue();
										navision.setInternateCharges(Math.round(d13));
									}
									break;
								case Constants.COLUMN_INDEX_20:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d14 = cell.getNumericCellValue();
										navision.setLegalAndProfessional(Math.round(d14));
									}
									break;
								case Constants.COLUMN_INDEX_21:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d15 = cell.getNumericCellValue();
										navision.setTravellingAndConveyance(Math.round(d15));
									}
									break;
								case Constants.COLUMN_INDEX_22:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d16 = cell.getNumericCellValue();
										navision.setInsuranceExpanse(Math.round(d16));
									}
									break;
								case Constants.COLUMN_INDEX_23:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d17 = cell.getNumericCellValue();
										navision.setMiscellaneousExpense(Math.round(d17));
									}
									break;
								case Constants.COLUMN_INDEX_24:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d18 = cell.getNumericCellValue();
										navision.setAdvertisement(Math.round(d18));
									}
									break;
								case Constants.COLUMN_INDEX_25:
									if ((cell.getCellType() == cell.CELL_TYPE_FORMULA)
											|| (cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d19 = cell.getNumericCellValue();
										navision.setCam(Math.round(d19));
									}
									break;
								}

							}

						}
						navisionMapperList.add(navision);

					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return navisionMapperList;
	}

	public void uploadCategoryMaster(UploadFilesMapper uploadFilesMapper) {
		if (ObjectUtils.isEmpty(uploadFilesMapper.getFile())) {
			throw new FeasibilityWebException("file info cannot be empty");
		}
		int status;
		try {

			List<CategoryDataSource> categoryMaster = getAllCategoryMaster(uploadFilesMapper.getFile());
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_ALL_CATEGORY_MASTER, categoryMaster, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving category-master information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving category-master information");
			throw new FeasibilityWebException("Error while saving category-master information");
		}
	}

	public List<CategoryDataSource> getAllCategoryMaster(MultipartFile file) {
		Workbook dataSource;
		List<CategoryDataSource> categoryMapperList = new ArrayList<>();

		try {
			String fileName = file.getOriginalFilename().toLowerCase();
			if (fileName.endsWith(".xlsx")) {
				dataSource = new XSSFWorkbook(file.getInputStream());
			} else {
				dataSource = new HSSFWorkbook(file.getInputStream());
			}

			for (int i = 0; i < dataSource.getNumberOfSheets(); i++) {
				Sheet sheet = dataSource.getSheetAt(i);
				for (Row row : sheet) {
					if (row.getRowNum() > 0) {

						CategoryDataSource categoryMaster = new CategoryDataSource();
						for (Cell cell : row) {

							if (cell != null) {

								switch (cell.getColumnIndex()) {
								case Constants.COLUMN_INDEX_0:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double d1 = cell.getNumericCellValue();
										categoryMaster.setHopk(d1.intValue());
									}
									break;
								case Constants.COLUMN_INDEX_1:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										categoryMaster.setCinemaName(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_2:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										categoryMaster.setCinemaNavCode(cell.getStringCellValue());
									}
									break;

								case Constants.COLUMN_INDEX_3:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										categoryMaster.setCinemaCategory(cell.getStringCellValue());
									}
									break;
								}

							}

						}
						categoryMapperList.add(categoryMaster);

					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return categoryMapperList;
	}

	public void uploadSeatCategoryMaster(UploadFilesMapper uploadFilesMapper) {
		if (ObjectUtils.isEmpty(uploadFilesMapper.getFile())) {
			throw new FeasibilityWebException("file info cannot be empty");
		}
		int status;
		try {

			List<SeatCategory> categoryMaster = getAllSeatMaster(uploadFilesMapper.getFile());
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_ALL_SEAT_CATEGORY_MASTER, categoryMaster,
					null, HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving seat-category-master information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving seat-category-master information");
			throw new FeasibilityWebException("Error while saving seat-category-master information");
		}
	}

	public List<SeatCategory> getAllSeatMaster(MultipartFile file) {
		Workbook dataSource;
		List<SeatCategory> seatCategoryMapperList = new ArrayList<>();

		try {
			String fileName = file.getOriginalFilename().toLowerCase();
			if (fileName.endsWith(".xlsx")) {
				dataSource = new XSSFWorkbook(file.getInputStream());
			} else {
				dataSource = new HSSFWorkbook(file.getInputStream());
			}

			for (int i = 0; i < 1; i++) {
				Sheet sheet = dataSource.getSheetAt(i);
				// FormulaEvaluator evaluator =
				// dataSource.getCreationHelper().createFormulaEvaluator();
				for (Row row : sheet) {
					if (row.getRowNum() > 0) {

						SeatCategory seatCategory = new SeatCategory();
						for (Cell cell : row) {
							if (cell != null) {
								// CellValue cellValue = evaluator.evaluate(cell);
								switch (cell.getColumnIndex()) {

								case Constants.COLUMN_INDEX_0:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double d1 = cell.getNumericCellValue();
										seatCategory.setCinema_hopk(d1.intValue());
									}
									break;
								case Constants.COLUMN_INDEX_1:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										seatCategory.setCinema_Name(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_2:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										seatCategory.setCinema_state(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_3:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										seatCategory.setCinema_Format(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_4:
									if (cell.getCellType() == cell.CELL_TYPE_FORMULA) {

									}
									break;
								case Constants.COLUMN_INDEX_5:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										seatCategory.setCinOperator_strCode(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_6:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double d11 = cell.getNumericCellValue();
										seatCategory.setAudi_Number(d11.intValue());
									}
									break;
								case Constants.COLUMN_INDEX_7:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										Double d1 = cell.getNumericCellValue();
										seatCategory.setArea_category(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_8:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										seatCategory.setArea_cat_Code(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_9:
									if ((cell.getCellType() == cell.CELL_TYPE_NUMERIC)) {
										Double d2 = cell.getNumericCellValue();
										seatCategory.setSession_lngSessionId(d2.intValue());
									}
									break;
								case Constants.COLUMN_INDEX_10:
									if ((cell.getCellType() == cell.CELL_TYPE_STRING)) {
										// Double d3 = cell.getNumericCellValue();
										seatCategory.setFilm_strCode(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_11:
									if (cell.getCellType() == cell.CELL_TYPE_STRING) {
										seatCategory.setMovie_Name(cell.getStringCellValue());
									}
									break;
								case Constants.COLUMN_INDEX_12:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double d5 = cell.getNumericCellValue();
										seatCategory.setAdmits(d5.intValue());
									}
									break;
								case Constants.COLUMN_INDEX_13:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double d6 = cell.getNumericCellValue();
										seatCategory.setGBOC(d6.doubleValue());
									}
									break;
								case Constants.COLUMN_INDEX_14:
									if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
										Double d8 = cell.getNumericCellValue();
										seatCategory.setTotal_Seats(d8.intValue());
									}
									break;

								}

							}

						}
						seatCategoryMapperList.add(seatCategory);

					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return seatCategoryMapperList;
	}

	@Override
	public ProjectCostSummary getProjectCost(UploadFilesMapper uploadFilesMapper) {
		if (ObjectUtils.isEmpty(uploadFilesMapper.getFile())) {
			throw new FeasibilityWebException("file info cannot be empty");
		}
		int status;
		try {

			ProjectCostSummary ProCostSummary = getProjectCostSummaryData(uploadFilesMapper.getFile(),
					uploadFilesMapper.getProjectId());
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_ALL_PROJECT_COST, ProCostSummary, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}
			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectCostSummary>() {
					});
				}
				return new ProjectCostSummary();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}

		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving category-master information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving category-master information");
			throw new FeasibilityWebException("Error while saving category-master information");
		}

	}

	public ProjectCostSummary getProjectCostSummaryData(MultipartFile file, String projectId) {
		Workbook dataSource;
		List<ProjectCostData> projectCostDataList = new ArrayList<>();

		ProjectCostSummary ProjectCostObject = new ProjectCostSummary();

		try {
			String fileName = file.getOriginalFilename().toLowerCase();
			if (fileName.endsWith(".xlsx")) {
				dataSource = new XSSFWorkbook(file.getInputStream());

			} else {
				dataSource = new HSSFWorkbook(file.getInputStream());
			}

			Sheet sheet = dataSource.getSheetAt(0);
			for (Row row : sheet) {
				if (row.getRowNum() > 3) {

					// ProjectCostSummary projectCostSummary = new ProjectCostSummary();
					ProjectCostData projectCostSummary = new ProjectCostData();
					for (Cell cell : row) {
						if (cell != null) {
							switch (cell.getColumnIndex()) {

							case Constants.COLUMN_INDEX_0:
								if (cell.getCellType() == cell.CELL_TYPE_STRING) {

								}
								break;
							case Constants.COLUMN_INDEX_1:
								if (cell.getCellType() == cell.CELL_TYPE_FORMULA) {
									// System.out.println("Formula is " + cell.getCellFormula());
									switch (cell.getCachedFormulaResultType()) {
									case Cell.CELL_TYPE_NUMERIC:
										// System.out.println("Last evaluated as: " + cell.getNumericCellValue());
										break;
									case Cell.CELL_TYPE_STRING:
										projectCostSummary.setDescription(cell.getStringCellValue());
										// System.out.println("Last evaluated as \"" + cell.getRichStringCellValue()
										// + "\"");
										break;
									}
								} else if (cell.getCellType() == cell.CELL_TYPE_STRING) {

									projectCostSummary.setDescription(cell.getStringCellValue());
									// System.out.println("Last evaluated as \"" + cell.getRichStringCellValue()
									// + "\"");
									break;
								}
								break;
							case Constants.COLUMN_INDEX_2:
								if (cell.getCellType() == cell.CELL_TYPE_FORMULA) {
									// System.out.println("Formula is " + cell.getCellFormula());
									switch (cell.getCachedFormulaResultType()) {
									case Cell.CELL_TYPE_NUMERIC:
										Double d1 = cell.getNumericCellValue();
										projectCostSummary.setCost(d1);
										// System.out.println("Last evaluated as: " + cell.getNumericCellValue());
										break;
									case Cell.CELL_TYPE_STRING:
										// System.out.println("Last evaluated as \"" + cell.getRichStringCellValue()
										// + "\"");
										break;
									}
								}
								break;
							case Constants.COLUMN_INDEX_3:
								if (cell.getCellType() == cell.CELL_TYPE_FORMULA) {
									// System.out.println("Formula is " + cell.getCellFormula());
									switch (cell.getCachedFormulaResultType()) {
									case Cell.CELL_TYPE_NUMERIC:
										Double d2 = cell.getNumericCellValue();
										projectCostSummary.setDepRate((d2.doubleValue() * 100));
										// System.out.println("Last evaluated as: " + cell.getNumericCellValue());
										break;
									case Cell.CELL_TYPE_STRING:
										// System.out.println("Last evaluated as \"" + cell.getRichStringCellValue()
										// + "\"");
										break;
									}
								} else if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {

									Double d2 = cell.getNumericCellValue();
									projectCostSummary.setDepRate(d2.doubleValue() * 100); // System.out.println("Last
																							// evaluated as \"" +
																							// cell.getRichStringCellValue()
									// + "\"");
									break;
								}
								break;
							case Constants.COLUMN_INDEX_4:
								if (cell.getCellType() == cell.CELL_TYPE_FORMULA) {
									// System.out.println("Formula is " + cell.getCellFormula());
									switch (cell.getCachedFormulaResultType()) {
									case Cell.CELL_TYPE_NUMERIC:
										Double d3 = cell.getNumericCellValue();
										projectCostSummary.setAnnualDep(d3.intValue());
										// System.out.println("Last evaluated as: " + cell.getNumericCellValue());
										break;
									case Cell.CELL_TYPE_STRING:

										// System.out.println("Last evaluated as \"" + cell.getRichStringCellValue()
										// + "\"");
										break;
									}
								}
								break;

							}
						}

					}

					// projectCostSummaryList.setProjectCostDataList(projectCostDataList);
					projectCostDataList.add(projectCostSummary);
					// projectCostSummaryList.add(projectCostSummary);

				}

			}
			ProjectCostObject.setParentId(projectId);
			ProjectCostObject.setProjectCostDataList(projectCostDataList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return ProjectCostObject;

	}

}
