package com.services;

import java.util.List;

import com.common.models.City;

public interface CityService {

	List<City> getAllCities();

	List<City> getStateBycityName(String city);

}
