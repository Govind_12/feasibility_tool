package com.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.common.mappers.MisMapper;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.CinemaMaster;
import com.common.models.FinalSummary;
import com.common.models.FixedConstant;
import com.common.models.IrrCalculationData;
import com.common.models.ProjectCostSummary;
import com.common.models.ProjectDetails;
import com.common.models.RevenueAssumptionsGrowthRate;
import com.fasterxml.jackson.databind.JsonNode;

public interface ProjectDetailService {

	public ProjectDetails saveStep1(ProjectDetails projectDetails);

	public ProjectDetails saveStep2(ProjectDetails projectDetails);

	public ProjectDetails saveStep3(ProjectDetails projectDetails);

	public ProjectDetails saveStep4(ProjectDetails projectDetails);

	public ProjectDetails saveStep5(ProjectDetails projectDetails, List<MultipartFile> files, List<String> oldfiles);

	public ProjectDetails saveStep6(ProjectDetails projectDetails);

	public ProjectDetails saveStep7(ProjectDetails projectDetails);

	public ProjectDetails getProjectById(String projectId);

	public ProjectDetails approveProject(String projectId);

	public ProjectDetails rejectProject(String projectId);

	public ProjectDetails rejectProjectDetails(MsgMapper msgMapper);

	public List<ProjectDetails> getAllProjects();

	public List<ProjectDetails> getPreSigningProjectsByStatus();

	public ProjectDetails getProjectDetailsById(String id);

	public FinalSummary getQueryByProjectId(String projectId);

	public QueryMapper submitQuery(MsgMapper msgMapper);

	public Boolean updateProjectWithNotification(String projectId, int status);

	public ProjectDetails getLatestProjectWithReportId(String reportType, String reportId);

	public ProjectCostSummary fetchAllProjectCost(String projectId);

	public ProjectDetails updateProjectDetailsbyId(String projectId);

	public List<ProjectDetails> getOldReports();

	public ProjectDetails submitOeratingAssumptionforOperationAndOperationTeam(ProjectDetails projectDetails);

	public List<CinemaMaster> getAllCinemaMasterDetails();

	public ProjectDetails updateProject(String projectId);

	public JsonNode HtmlToPdfConversion(Object object, String projectId);

	public List<ProjectDetails> getExisitingProjectDetails();

	public ProjectDetails saveandCloseStep5(ProjectDetails projectDetails, List<MultipartFile> files, List<String> oldfiles);

	public List<MisMapper> fetchAllProjects();

	public boolean isCurrentUserHasOpeartionRole();

	public FixedConstant getRevenueAssumptionConstant(String projectId);

	public ProjectDetails saveExecutiveNote(ProjectDetails projectDetails);

	public List<ProjectDetails> getExistingOldReportsForCopy();
	
	Boolean copyExistingProject(String paramString);
	
	List<MisMapper> fetchAllOperationalProjects();

	List<MisMapper> fetchAllExitProjects();

	public IrrCalculationData getPAndLTabData(String projectId);

	public IrrCalculationData getProjectIrrCalculationsById(String projectId);

	public List<FixedConstant> getFixedConstants();

}
