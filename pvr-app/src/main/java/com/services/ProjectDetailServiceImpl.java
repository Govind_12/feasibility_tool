package com.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.multipart.MultipartFile;

import com.common.constants.Constants;
import com.common.mappers.MisMapper;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.CinemaMaster;
import com.common.models.FinalSummary;
import com.common.models.FixedConstant;
import com.common.models.IrrCalculationData;
import com.common.models.ProjectCostSummary;
import com.common.models.ProjectDetails;
import com.common.models.RevenueAssumptionsGrowthRate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.MarkConstant;
import com.util.RestServiceUtil;
import com.util.URLConstants;
import com.util.UserRoleUtil;
import com.web.FeasibilityWebException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProjectDetailServiceImpl implements ProjectDetailService {

	@Value("${path.fileUploadPath}")
	private String uploFilePath;

	@Autowired
	private RestServiceUtil restServiceUtil;

	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@Override
	public ProjectDetails saveStep1(ProjectDetails projectDetails) {

		if (ObjectUtils.isEmpty(projectDetails)) {
			throw new FeasibilityWebException("Project detail info cannot be empty");
		}
		int status;

		MultipartFile file = projectDetails.getUploadFileStep1();
		String filePath = "";
		UserRoleUtil userRole = new UserRoleUtil();

		if (file!=null && !file.isEmpty()) {
			try {
				String uploadsDir = "step_1_" + UserRoleUtil.generateNewReportId() + "_";
				String path = uploFilePath;
				
				if (!new File(path).exists()) {
					new File(path).mkdirs();
				}

				String fileName = file.getOriginalFilename();
				filePath = path + "/" + uploadsDir+fileName;
				File dest = new File(filePath);
				file.transferTo(dest);
				projectDetails.setFileURLStep1(uploadsDir+fileName);

			} catch (Exception e) {
				e.printStackTrace();
			}
			projectDetails.setUploadFileStep1(null);
			
		} else {
			projectDetails.setUploadFileStep1(null);
			projectDetails.setFileURLStep1(null);

		}

		try {
			

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_STEP_1, projectDetails, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while saving Project detail information");
		}
	}

	@Override
	public ProjectDetails saveStep2(ProjectDetails projectDetails) {

		if (ObjectUtils.isEmpty(projectDetails)) {
			throw new FeasibilityWebException("Project detail info cannot be empty");
		}
		int status;

		MultipartFile file = projectDetails.getUploadFileStep2();
		String filePath = "";
		UserRoleUtil userRole = new UserRoleUtil();

		if (file!=null && !file.isEmpty()) {
			try {
				String uploadsDir = "step_2_" + UserRoleUtil.generateNewReportId() + "_";
				String path = uploFilePath;
				
				if (!new File(path).exists()) {
					new File(path).mkdirs();
				}

				String fileName = file.getOriginalFilename();
				filePath = path + "/" + uploadsDir+fileName;
				File dest = new File(filePath);
				file.transferTo(dest);
				projectDetails.setFileURLStep2(uploadsDir+fileName);

			} catch (Exception e) {
				e.printStackTrace();
			}
			projectDetails.setUploadFileStep2(null);
			
		} else {
			projectDetails.setUploadFileStep2(null);
			projectDetails.setFileURLStep2(null);

		}
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_STEP_2, projectDetails, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while saving Project detail information");
		}
	}

	@Override
	public ProjectDetails saveStep3(ProjectDetails projectDetails) {

		if (ObjectUtils.isEmpty(projectDetails)) {
			throw new FeasibilityWebException("Project detail info cannot be empty");
		}
		int status;

		try {
			
			//remove null item
			projectDetails.getPvrProperties().removeIf(p->ObjectUtils.isEmpty(p.getPropertyName()) && ObjectUtils.isEmpty(p.getAddress()));
			projectDetails.getCompetitveProperties().removeIf(c->ObjectUtils.isEmpty(c.getPropertyName()) && ObjectUtils.isEmpty(c.getAddress()));
			
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_STEP_3, projectDetails, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while saving Project detail information");
		}
	}

	@Override
	public ProjectDetails saveStep4(ProjectDetails projectDetails) {

		if (ObjectUtils.isEmpty(projectDetails)) {
			throw new FeasibilityWebException("Project detail info cannot be empty");
		}
		int status;

		try {
		
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_STEP_4, projectDetails, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while saving Project detail information");
		}
	}

//	@Override
//	public ProjectDetails saveStep5(ProjectDetails projectDetails) {
//		if (ObjectUtils.isEmpty(projectDetails)) {
//			throw new FeasibilityWebException("Project detail info cannot be empty");
//		}
//		int status;
//
//		MultipartFile file = projectDetails.getUploadFileStep5();
//		String filePath = "";
//		UserRoleUtil userRole = new UserRoleUtil();
//		
//
//		if (file!=null && !file.isEmpty()) {
//			try {
//				String uploadsDir = "step_5_" + UserRoleUtil.generateNewReportId() + "_";
//				String path = uploFilePath;
//				
//				if (!new File(path).exists()) {
//					new File(path).mkdirs();
//				}
//
//				String fileName = file.getOriginalFilename();
//				filePath = path + "/" + uploadsDir+fileName;
//				File dest = new File(filePath);
//				file.transferTo(dest);
//				projectDetails.setFileURLStep5(uploadsDir+fileName);
//
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			projectDetails.setUploadFileStep5(null);
//			
//		} else {
//			projectDetails.setUploadFileStep5(null);
//			projectDetails.setFileURLStep5(null);
//
//		}
//		
//
//		try {
//		
//
//			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_STEP_5, projectDetails, null,
//					HttpMethod.POST);
//
//			status = response.get(MarkConstant.STATUS_CODE).intValue();
//			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
//			if (status != 200) {
//				throw new FeasibilityWebException(errorMsg, status);
//			}
//
//			try {
//				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
//					String data = response.get(MarkConstant.DATA).toString();
//					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
//					});
//				}
//				return new ProjectDetails();
//			} catch (IOException ex) {
//				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
//			}
//		} catch (FeasibilityWebException e) {
//			String errorCode = e.getMessage();
//			if (errorCode.equals("401")) {
//				log.error("Token Expired.");
//				throw new FeasibilityWebException("Error while saving Project detail information", 401);
//			}
//
//			if (errorCode.equals("403")) {
//				log.error("Access Denied this api.");
//				throw new FeasibilityWebException("Access Denied this api.", 403);
//			}
//
//			if (e.code == 500) {
//				log.error(e.getMessage());
//				throw new FeasibilityWebException(e.getMessage(), e.code);
//			}
//			log.error("Error while saving survey information");
//			throw new FeasibilityWebException("Error while saving Project detail information");
//		}
//	}

	public ProjectDetails saveStep5(ProjectDetails projectDetails, List<MultipartFile> multifile,
			List<String> oldfiles) {
		if (ObjectUtils.isEmpty(projectDetails))
			throw new FeasibilityWebException("Project detail info cannot be empty");
		String filePath = "";
		List<String> urlpath = new ArrayList<>();
		UserRoleUtil userRole = new UserRoleUtil();
		for (MultipartFile file : multifile) {
			if (file != null && !file.isEmpty()) {
				try {
					String uploadsDir = "step_5_" + UserRoleUtil.generateNewReportId() + "_";
					String path = this.uploFilePath;
					if (!(new File(path)).exists())
						(new File(path)).mkdirs();
					String fileName = file.getOriginalFilename();
					filePath = path + "/" + uploadsDir + fileName;
					File dest = new File(filePath);
					file.transferTo(dest);
					urlpath.add(uploadsDir + fileName);
				} catch (Exception e) {
					e.printStackTrace();
				}
				projectDetails.setUploadFileStep5(null);
				continue;
			}
			projectDetails.setUploadFileStep5(null);
			projectDetails.setFileURLStep5(null);
		}
		if (!ObjectUtils.isEmpty(oldfiles))
			urlpath.addAll(oldfiles);
		projectDetails.setMultipleFileURLStep5(urlpath);
		try {
			JsonNode response = this.restServiceUtil.makeRequest("projectDetails/step5", projectDetails, null,
					HttpMethod.POST);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while saving Project detail information");
		}
	}

	@Override
	public ProjectDetails saveStep6(ProjectDetails projectDetails) {
		if (ObjectUtils.isEmpty(projectDetails)) {
			throw new FeasibilityWebException("Project detail info cannot be empty");
		}
		int status;

		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_STEP_6, projectDetails, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while saving Project detail information");
		}

	}

	@Override
	public ProjectDetails getProjectById(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_PROJECT_DETAIL_BY_ID, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Project by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching User by id");
			throw new FeasibilityWebException("Error while fetching Project by id");
		}
	}

	@Override
	public ProjectDetails saveStep7(ProjectDetails projectDetails) {
		if (ObjectUtils.isEmpty(projectDetails)) {
			throw new FeasibilityWebException("survey-Design info cannot be empty");
		}
		int status;

		try {
		

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_STEP_7, projectDetails, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while saving Project detail information");
		}
	}

	@Override
	public List<ProjectDetails> getAllProjects() {

		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_ALL_PROJECT_DETAILS, null, null,
					HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {
					});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}

		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all Project details", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Project details");
			throw new FeasibilityWebException("Error while fetching all Project details");
		}

	}
	
	@Override
	public List<ProjectDetails> getPreSigningProjectsByStatus() {

		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_PRE_SIGNING_PROJECT_BY_STATUS, null, null,
					HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {
					});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}

		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all Project details", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Project details");
			throw new FeasibilityWebException("Error while fetching all Project details");
		}

	}

	@Override
	public ProjectDetails approveProject(String projectId) {
		int status;
		String url = String.format(URLConstants.APPROVE_PROJECT, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while approve project by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while approve project by id");
			throw new FeasibilityWebException("Error while approve project by id");
		}
	}

	@Override
	public ProjectDetails rejectProject(String projectId) {
		int status;
		String url = String.format(URLConstants.REJECT_PROJECT, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while reject project by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while reject project by id");
			throw new FeasibilityWebException("Error while reject project by id");
		}
	}

	@Override
	public ProjectDetails getProjectDetailsById(String id) {
		int status;
		String url = String.format(URLConstants.GET_PROJECT__BY_ID, id);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Project by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching User by id");
			throw new FeasibilityWebException("Error while fetching Project by id");
		}
	}

	@Override
	public FinalSummary getQueryByProjectId(String projectId) {
		int status;
		String url = String.format(URLConstants.QUERY_BY_PROJECT_ID, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<FinalSummary>() {
					});
				}
				return new FinalSummary();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while approve project by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while approve project by id");
			throw new FeasibilityWebException("Error while approve project by id");
		}
	}

	@Override
	public QueryMapper submitQuery(MsgMapper SAVE_QUERY) {
		int status;

		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_QUERY, SAVE_QUERY, null, HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<QueryMapper>() {
					});
				}
				return new QueryMapper();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving query", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving query");
			throw new FeasibilityWebException("Error while saving query");
		}
	}

	@Override
	public Boolean updateProjectWithNotification(String projectId, int projectStatus) {
		
		
		int status;
		
		try {
			
			String url = String.format(URLConstants.UPDATE_PROJECT_WITH_NOTIFICATION, projectId, projectStatus);
			
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);
			System.out.println("response: " + response);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}

		} catch (FeasibilityWebException e) {

		}
		return null;
	}

	@Override
	public ProjectDetails getLatestProjectWithReportId(String reportType, String reportId) {
		int status;
		String url =null;
		
		if(reportId==null)
			url = String.format(URLConstants.GET_LATEST_PROJECT, reportType,"");
		else
			url = String.format(URLConstants.GET_LATEST_PROJECT, reportType,reportId);
		
		
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public ProjectCostSummary fetchAllProjectCost(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_PROJECT_COST_SUMMARY, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectCostSummary>() {
					});
				}
				return new ProjectCostSummary();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public ProjectDetails updateProjectDetailsbyId(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_PROJECT__BY_ID, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Project by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching User by id");
			throw new FeasibilityWebException("Error while fetching Project by id");
		}
	}

	@Override
	public List<ProjectDetails> getOldReports() {
		
		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_ALL_OLD_REPORTS, null, null,
					HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {
					});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}

		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all old reports ", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Project details");
			throw new FeasibilityWebException("Error while fetching all old reports");
		}

	}

	@Override
	public ProjectDetails submitOeratingAssumptionforOperationAndOperationTeam(ProjectDetails projectDetails) {
		if (ObjectUtils.isEmpty(projectDetails)) {
			throw new FeasibilityWebException("projectDetails info cannot be empty");
		}
		int status;

		try {
		
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SEND_FOR_OPERATING_ASSUMPTIONS, projectDetails, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while sending Project detail information to project team and operation team");
		}
	}
	
	
	
	
	@Override
	public ProjectDetails rejectProjectDetails(@ModelAttribute MsgMapper msgMapper) {

		if (ObjectUtils.isEmpty(msgMapper)) {
			throw new FeasibilityWebException("Project detail info cannot be empty");
		}
		int status;

		try {
			
			JsonNode response = restServiceUtil.makeRequest(URLConstants.REJECT_PROJECT_DETAILS, msgMapper, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while rejecting project details", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while rejecting project details");
			throw new FeasibilityWebException("Error while rejecting project details");
		}
	}

	@Override
	public ProjectDetails updateProject(String projectId) {
		int status;
		String url = String.format(URLConstants.UPDATE_PROJECT, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while approve project by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while approve project by id");
			throw new FeasibilityWebException("Error while approve project by id");
		}
	}
	
	
	
	public List<CinemaMaster> getAllCinemaMasterDetails() {

		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_ALL_CINEMA_MASTER_DETAILS, null, null,
					HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<CinemaMaster>>() {
					});
				}
				return new ArrayList<CinemaMaster>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for dashboard on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching cinema master details", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching cinema master details");
			throw new FeasibilityWebException("Error while fetching cinema master details");
		}
	}

	@Override
	public JsonNode HtmlToPdfConversion(Object object, String projectId) {
		try {
			String url = String.format(URLConstants.DOWNLOAD_PDF, projectId);
			JsonNode response = restServiceUtil.makeRequest(url, null, null,
					HttpMethod.GET);

			return response;
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching cinema master details", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching cinema master details");
			throw new FeasibilityWebException("Error while fetching cinema master details");
		}
	}
	
	public List<ProjectDetails> getExisitingProjectDetails() {

		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_EXISTING_PROJECT, null, null,
					HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {
					});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}

		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all Project details", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Project details");
			throw new FeasibilityWebException("Error while fetching all Project details");
		}
	}

	@Override
	public ProjectDetails saveandCloseStep5(ProjectDetails projectDetails, List<MultipartFile> multifile,
			List<String> oldfiles) {

		if (ObjectUtils.isEmpty(projectDetails)) {
			throw new FeasibilityWebException("Project detail info cannot be empty");
		}
		int status = 0;
		String filePath = "";
		List<String> urlpath = new ArrayList<>();
		for (MultipartFile file : multifile) {
			if (file != null && !file.isEmpty()) {
				try {
					String uploadsDir = "step_5_" + UserRoleUtil.generateNewReportId() + "_";
					String path = this.uploFilePath;
					if (!(new File(path)).exists())
						(new File(path)).mkdirs();
					String fileName = file.getOriginalFilename();
					filePath = path + "/" + uploadsDir + fileName;
					File dest = new File(filePath);
					file.transferTo(dest);
					urlpath.add(uploadsDir + fileName);
				} catch (Exception e) {
					e.printStackTrace();
				}
				projectDetails.setUploadFileStep5(null);
				continue;
			}
			projectDetails.setUploadFileStep5(null);
			projectDetails.setFileURLStep5(null);
		}
		if (!ObjectUtils.isEmpty(oldfiles))
			urlpath.addAll(oldfiles);
		projectDetails.setMultipleFileURLStep5(urlpath);
		
		try {
	
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_CLOSE_STEP_5, projectDetails, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while saving Project detail information");
		}
	
	}

	@Override
	public List<MisMapper> fetchAllProjects() {
		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.FETCH_ALL_PROJECT_DETAILS, null, null,
					HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<MisMapper>>() {
					});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}

		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all Project details", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Project details");
			throw new FeasibilityWebException("Error while fetching all Project details");
		}
	}
	
	public boolean isCurrentUserHasOpeartionRole() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Collection<? extends GrantedAuthority> hiTechUser = auth.getAuthorities();
		for (GrantedAuthority authorities : hiTechUser) {
			if ((authorities.getAuthority()).equals(Constants.ROLE_OPERATION_TEAM)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public FixedConstant getRevenueAssumptionConstant(String projectId) {
		int status;
		try {
			String url = String.format(URLConstants.GET_FIXED_CONSTANTS_FOR_OPERATION_TAB, projectId);

			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();

					return OBJECT_MAPPER.readValue(data, new TypeReference<FixedConstant>() {
					});
				}
				return new FixedConstant();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Fixed Constatnts.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Fixed Constatnts", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			log.error("Error while fetching cities");
			throw new FeasibilityWebException("Error while fetching Fixed Constatnts");
		}
	}

	@Override
	public ProjectDetails saveExecutiveNote(ProjectDetails projectDetails) {
		
		if (ObjectUtils.isEmpty(projectDetails) || ObjectUtils.isEmpty(projectDetails.getId())) {
			throw new FeasibilityWebException("projectDetails info cannot be empty");
		}
		int status;
		try {
			String url = String.format(URLConstants.SAVE_EXECUTIVE_NOTE);
			JsonNode response = restServiceUtil.makeRequest(url, projectDetails, null, HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}
			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();

					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Fixed Constatnts.", 500);
			}
			
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while sending Project detail information to project team and operation team");
		}
		
	}

	public List<ProjectDetails> getExistingOldReportsForCopy() {
		try {
			JsonNode response = this.restServiceUtil
					.makeRequest("projectDetails/get-all-existing-project-detail-for-copy", null, null, HttpMethod.GET);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return (List<ProjectDetails>) OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all Project details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Project details");
			throw new FeasibilityWebException("Error while fetching all Project details");
		}
	}

	public List<MisMapper> fetchAllOperationalProjects() {
		try {
			JsonNode response = this.restServiceUtil.makeRequest("projectDetails/fetch-all-ops-project-detail", null,
					null, HttpMethod.GET);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return (List<MisMapper>) OBJECT_MAPPER.readValue(data, new TypeReference<List<MisMapper>>() {});

				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all Project details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Project details");
			throw new FeasibilityWebException("Error while fetching all Project details");
		}
	}

	public Boolean copyExistingProject(String projectId) {
		try {
			String url = String.format("projectDetails/copyFromExistingRerport/%s", new Object[] { projectId });
			JsonNode response = this.restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);
			System.out.println("response: " + response);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return (Boolean) OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {});

				}
				return Boolean.valueOf(false);
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException feasibilityWebException) {
			return null;
		}
	}

	public List<MisMapper> fetchAllExitProjects() {
		try {
			JsonNode response = this.restServiceUtil.makeRequest("projectDetails/fetch-all-ext-project-detail", null,
					null, HttpMethod.GET);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return (List<MisMapper>) OBJECT_MAPPER.readValue(data, new TypeReference<List<MisMapper>>() {});

				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all Project details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Project details");
			throw new FeasibilityWebException("Error while fetching all Project details");
		}
	}

	@Override
	public IrrCalculationData getPAndLTabData(String projectId) {
		try {
			String url = String.format("projectDetails/get-pandl-tab/%s", new Object[] { projectId });
			JsonNode response = this.restServiceUtil.makeRequest(url, null,
					null, HttpMethod.GET);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<IrrCalculationData>() {});

				}
				return new IrrCalculationData();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all Project details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Project details");
			throw new FeasibilityWebException("Error while fetching all Project details");
		}
	}
	
	@Override
	public IrrCalculationData getProjectIrrCalculationsById(String projectId) {
		String url = String.format("projectDetails/getProjectIrrCalcById/%s", new Object[] { projectId });
		try {
			JsonNode response = this.restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<IrrCalculationData>() {});
				}
				return new IrrCalculationData();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Project by id", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching User by id");
			throw new FeasibilityWebException("Error while fetching Project by id");
		}
	}

	@Override
	public List<FixedConstant> getFixedConstants() {
		try {
			JsonNode response = this.restServiceUtil.makeRequest("projectDetails/getAllFixedConsts", null, null,
					HttpMethod.GET);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<FixedConstant>>() {});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all Project details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Project details");
			throw new FeasibilityWebException("Error while fetching all Project details");
		}
	}
	
}
