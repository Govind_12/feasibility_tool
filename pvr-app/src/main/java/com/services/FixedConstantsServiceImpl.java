package com.services;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.common.mappers.FhcMapper;
import com.common.mappers.ServiceChargeMapper;
import com.common.models.CinemaCategoryMaster;
import com.common.models.CinemaFormatMaster;
import com.common.models.FixedConstant;
import com.common.models.SeatTypeMaster;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.MarkConstant;
import com.util.RestServiceUtil;
import com.util.URLConstants;
import com.web.FeasibilityWebException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FixedConstantsServiceImpl implements FixedConstantsService {

	@Autowired
	private RestServiceUtil restServiceUtil;

	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@Override
	public FixedConstant getFixedConstants() {

		int status;
		try {
			String url = URLConstants.GET_FIXED_CONSTANTS;

			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();

					return OBJECT_MAPPER.readValue(data, new TypeReference<FixedConstant>() {
					});
				}
				return new FixedConstant();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Fixed Constatnts.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Fixed Constatnts", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			log.error("Error while fetching cities");
			throw new FeasibilityWebException("Error while fetching Fixed Constatnts");
		}
	}

	@Override
	public FixedConstant saveServiceChargePerTickets(ServiceChargeMapper fixedConstant) {

		if (ObjectUtils.isEmpty(fixedConstant)) {
			throw new FeasibilityWebException("ServiceCharge info cannot be empty");
		}
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_SERVICE_CHARGE, fixedConstant, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<FixedConstant>() {
					});
				}
				return new FixedConstant();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for ServiceChargePerTickets on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving ServiceChargePerTickets information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving ServiceCharges information");
			throw new FeasibilityWebException("Error while saving ServiceCharges information");
		}
	}

	@Override
	public FixedConstant saveCapexsAssumption(FixedConstant fixedConstant) {
		if (ObjectUtils.isEmpty(fixedConstant)) {
			throw new FeasibilityWebException("Capex_Assumption(Fixed_Constant) info cannot be empty");
		}
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_CAPEX_ASSUMPTION, fixedConstant, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<FixedConstant>() {
					});
				}
				return new FixedConstant();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Capex_Assumption on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Capex_Assumption information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Capex_Assumption information");
			throw new FeasibilityWebException("Error while saving Capex_Assumption information");
		}

	}

	@Override
	public FixedConstant saveGrowthAssumptions(FixedConstant fixedConstant) {
		int status;
		try {
			String url = URLConstants.SAVE_GROWTH_ASSUMPTION;

			JsonNode response = restServiceUtil.makeRequest(url, fixedConstant, null, HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();

					return OBJECT_MAPPER.readValue(data, new TypeReference<FixedConstant>() {
					});
				}
				return new FixedConstant();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error at saving Fixed Constatnts.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Fixed Constatnts", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			log.error("Error while saving Fixed Constatnts");
			throw new FeasibilityWebException("Error while saving Fixed Constatnts");
		}
	}

	@Override
	public FixedConstant saveTaxsAssumption(FixedConstant fixedConstant) {

		if (ObjectUtils.isEmpty(fixedConstant)) {
			throw new FeasibilityWebException("Tax_Assumption(Fixed_Constant) info cannot be empty");
		}
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_TAX_ASSUMPTION, fixedConstant, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<FixedConstant>() {
					});
				}
				return new FixedConstant();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Tax_Assumption on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Tax_Assumption information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Tax_Assumption information");
			throw new FeasibilityWebException("Error while saving Tax_Assumption information");
		}
	}

	@Override
	public FixedConstant saveOpeartingsAssumption(FixedConstant fixedConstant) {

		if (ObjectUtils.isEmpty(fixedConstant)) {
			throw new FeasibilityWebException("Opeartings_Assumption(VPF) info cannot be empty");
		}
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_OPERATING_ASSUMPTION, fixedConstant, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<FixedConstant>() {
					});
				}
				return new FixedConstant();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Opeartings_Assumption (VPF) on web.",
						500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Opeartings_Assumption(VPF) information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Opeartings_Assumption information");
			throw new FeasibilityWebException("Error while saving Opeartings_Assumption information");
		}
	}

	@Override
	public FixedConstant saveAtpCap(FixedConstant fixedConstant) {

		if (ObjectUtils.isEmpty(fixedConstant)) {
			throw new FeasibilityWebException("Atp-Cap info cannot be empty");
		}
		int status;
		try {

			System.out.println(fixedConstant);
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_ATPCAP_ASSUMPTION, fixedConstant, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<FixedConstant>() {
					});
				}
				return new FixedConstant();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}

	}

	@Override
	public Boolean saveFhc(FhcMapper fhcMapper) {
		if (ObjectUtils.isEmpty(fhcMapper)) {
			throw new FeasibilityWebException("Fhc Mapper info cannot be empty");
		}
		int status;
		try {

			System.out.println(fhcMapper);
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_FHC, fhcMapper, null, HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}
	}

	@Override
	public boolean isFhcPresent(FhcMapper fhcMapper) {
		if (ObjectUtils.isEmpty(fhcMapper)) {
			throw new FeasibilityWebException("Fhc Mapper info cannot be empty");
		}
		int status;
		try {

			System.out.println(fhcMapper);
			JsonNode response = restServiceUtil.makeRequest(URLConstants.IS_FHC_PRESENT, fhcMapper, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}
		return false;
	}

	@Override
	public FixedConstant irrConstantDetails(FixedConstant fixedConstant) {
		if (ObjectUtils.isEmpty(fixedConstant)) {
			throw new FeasibilityWebException("Fixed_Constant info cannot be empty");
		}
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_FIXED_CONSTANT, fixedConstant, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<FixedConstant>() {
					});
				}
				return new FixedConstant();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Fixed_Constant on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Fixed_Constant information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Fixed_Constant information");
			throw new FeasibilityWebException("Error while saving Fixed_Constantinformation");
		}

	}

	public boolean deleteAtpCap(FixedConstant fixedConstant) {
		if (ObjectUtils.isEmpty(fixedConstant)) {
			throw new FeasibilityWebException("State cannot be empty");
		}
		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.DELETE_ATP, fixedConstant, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Fixed_Constant on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Fixed_Constant information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Fixed_Constant information");
			throw new FeasibilityWebException("Error while saving Fixed_Constantinformation");
		}

	}

	@Override
	public List<CinemaFormatMaster> getAllCinemaFormat() {
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_ALL_CINEMA_FORMAT_MASTER, null, null,
					HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<CinemaFormatMaster>>() {
					});
				}
				return null;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for ServiceChargePerTickets on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving ServiceChargePerTickets information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving ServiceCharges information");
			throw new FeasibilityWebException("Error while saving ServiceCharges information");
		}
	}

	@Override
	public CinemaFormatMaster getCinemaFormat(String id) {

		int status;
		try {
			String url = String.format(URLConstants.GET_CINEMA_FORMAT_MASTER, id);
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<CinemaFormatMaster>() {
					});
				}
				return null;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for ServiceChargePerTickets on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving ServiceChargePerTickets information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving ServiceCharges information");
			throw new FeasibilityWebException("Error while saving ServiceCharges information");
		}
	}

	@Override
	public Boolean saveCinemaFormat(CinemaFormatMaster cinemaCategoryMaster) {
		if (ObjectUtils.isEmpty(cinemaCategoryMaster)) {
			throw new FeasibilityWebException("cinemaCategoryMaster info cannot be empty");
		}
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_CINEMA_FORMAT, cinemaCategoryMaster, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}
	}

	@Override
	public List<CinemaCategoryMaster> getAllCinemaCategory() {

		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_ALL_CINEMA_CATEGORY_MASTER, null, null,
					HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<CinemaCategoryMaster>>() {
					});
				}
				return null;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for ServiceChargePerTickets on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving ServiceChargePerTickets information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving ServiceCharges information");
			throw new FeasibilityWebException("Error while saving ServiceCharges information");
		}
	}

	@Override
	public CinemaCategoryMaster getCinemaCategory(String id) {

		int status;
		try {
			String url = String.format(URLConstants.GET_CINEMA_CATEGORY_MASTER, id);
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<CinemaCategoryMaster>() {
					});
				}
				return null;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for ServiceChargePerTickets on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving ServiceChargePerTickets information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving ServiceCharges information");
			throw new FeasibilityWebException("Error while saving ServiceCharges information");
		}
	}

	@Override
	public Boolean saveCinemaCategory(CinemaCategoryMaster cinemaCategoryMaster) {
		if (ObjectUtils.isEmpty(cinemaCategoryMaster)) {
			throw new FeasibilityWebException("cinemaCategoryMaster info cannot be empty");
		}
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_CINEMA_CATEGORY, cinemaCategoryMaster,
					null, HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}
	}

	@Override
	public List<SeatTypeMaster> getAllSeatType() {

		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_ALL_SEAT_TYPE, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<SeatTypeMaster>>() {
					});
				}
				return null;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for ServiceChargePerTickets on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving ServiceChargePerTickets information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving ServiceCharges information");
			throw new FeasibilityWebException("Error while saving ServiceCharges information");
		}
	}

	@Override
	public SeatTypeMaster getSeatType(String id) {

		int status;
		try {
			String url = String.format(URLConstants.GET_SEAT_TYPE_MASTER, id);
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<SeatTypeMaster>() {
					});
				}
				return null;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for ServiceChargePerTickets on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving ServiceChargePerTickets information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving ServiceCharges information");
			throw new FeasibilityWebException("Error while saving ServiceCharges information");
		}
	}

	@Override
	public Boolean saveSeatType(SeatTypeMaster seatTypeMaster) {
		if (ObjectUtils.isEmpty(seatTypeMaster)) {
			throw new FeasibilityWebException("cinemaCategoryMaster info cannot be empty");
		}
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_SEAT_TYPE, seatTypeMaster, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}
	}

	public Boolean saveCogs(FhcMapper fhcMapper) {
		if (ObjectUtils.isEmpty(fhcMapper)) {
			throw new FeasibilityWebException("Fhc Mapper info cannot be empty");
		}
		int status;
		try {

			System.out.println(fhcMapper);
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_COGS, fhcMapper, null, HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}
	}

	public List<CinemaFormatMaster> getFormatMappingViewList(List<CinemaFormatMaster> cinemaFormatList, String id,
			CinemaFormatMaster cinemaFormatMappingMaster) {
		List<CinemaFormatMaster> formatMappingViewList = cinemaFormatList.stream()
				.filter(predicate -> !predicate.getId().equalsIgnoreCase(id)).collect(Collectors.toList());
		List<String> checkedList = cinemaFormatMappingMaster.getCinemaFormatMapping();
		int i = 0;
		if (checkedList != null) {
			for (CinemaFormatMaster cinemaFormatMaster : formatMappingViewList) {
				if (checkedList.contains(cinemaFormatMaster.getCinemaCategory())) {

				} else {
					checkedList.add(i, null);
				}
				i++;
			}
		}
		cinemaFormatMappingMaster.setCinemaFormatMapping(checkedList);
		return formatMappingViewList;
	}

	@Override
	public Boolean deleteOpearatingAssumption(FhcMapper fhcMapper) {
		if (ObjectUtils.isEmpty(fhcMapper)) {
			throw new FeasibilityWebException("Fhc Mapper info cannot be empty");
		}
		int status;
		try {

			System.out.println(fhcMapper);
			JsonNode response = restServiceUtil.makeRequest(URLConstants.DELETE_OPERATING_ASSUPTION, fhcMapper, null, HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}
	}

	@Override
	public Boolean saveGstOnTicket(FhcMapper fhcMapper) {
		if (ObjectUtils.isEmpty(fhcMapper)) {
			throw new FeasibilityWebException("Fhc Mapper info cannot be empty");
		}
		int status;
		try {

			System.out.println(fhcMapper);
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_GST_ON_TICKET, fhcMapper, null, HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}
	}

	@Override
	public Boolean saveScreenType(CinemaFormatMaster cinemaCategoryMaster) {
		if (ObjectUtils.isEmpty(cinemaCategoryMaster)) {
			throw new FeasibilityWebException("cinemaCategoryMaster info cannot be empty");
		}
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_NEW_SCREEN_TYPE, cinemaCategoryMaster, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}
	}

}
