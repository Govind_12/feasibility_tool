package com.services;

import java.util.List;
import java.util.Map;

import com.common.models.ProjectDetails;

public interface AdminService {

	List<ProjectDetails> getOpsReports();
	  
	List<ProjectDetails> getExitReports();

	List<ProjectDetails> updateOpsReports(List<Integer> ids);

	List<ProjectDetails> updateExtReports(List<Integer> ids);

	List<ProjectDetails> getReportsForChangeProjName();

	List<ProjectDetails> updateProjectNames(Map<String, String> projData);

}
