package com.services;

import java.util.List;

import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.ProjectDetails;

public interface PreHandoverService {
	
public ProjectDetails savePreHandoverStep1(ProjectDetails projectDetails);
	
	public List<ProjectDetails> getAllExistingProjects();
	
	public  QueryMapper submitQuery(MsgMapper msgMapper);
	
	public  QueryMapper submitFinalQuery(MsgMapper msgMapper);
	
	public  String submitForOperationOrProjectTeam(MsgMapper msgMapper);
	
	public boolean updateNotificationStatus(String projectId,String queryType,String step);

}
