package com.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.common.mappers.ApproverMapper;
import com.common.models.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.AuthorizationUtil;
import com.util.Constants;
import com.util.MarkConstant;
import com.util.RestServiceUtil;
import com.util.URLConstants;
import com.util.UserRoleUtil;
import com.web.FeasibilityWebException;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UserService {

	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@Autowired
	private RestServiceUtil restServiceUtil;

	@Autowired
	private FileUtilityService fileUtilityService;

	// @Value("${awss3.profileFolder}")
	private String profileFolder;

	@Autowired
	private AuthorizationUtil authorizationUtil;

	public List<User> getAllUsers() {
		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_USERS, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();

					return OBJECT_MAPPER.readValue(data, new TypeReference<List<User>>() {
					});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for users on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching users", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			log.error("Error while fetching users");
			throw new FeasibilityWebException("Error while fetching users");
		}
	}

	public User saveUser(User user, String deleteFlag) {
		if (ObjectUtils.isEmpty(user)) {
			throw new FeasibilityWebException("user info cannot be empty");
		}
		try {

			String designation = user.getDesignation();
			if ("Y".equalsIgnoreCase(deleteFlag)) {
				designation = null;
			}
			if (designation != null) {
				switch (designation) {
				case com.common.constants.Constants.DESIGNATION_CDO:
					user.setApproverType(com.common.constants.Constants.STATUS_APPROVER_1);
					user.setPreHandOverApproverType(com.common.constants.Constants.STATUS_APPROVER_1);
					break;
				case com.common.constants.Constants.DESIGNATION_CO:
					user.setApproverType(com.common.constants.Constants.STATUS_APPROVER_2);
					user.setPreHandOverApproverType(com.common.constants.Constants.STATUS_APPROVER_2);
					break;
				case com.common.constants.Constants.DESIGNATION_CPO:
					user.setApproverType(com.common.constants.Constants.STATUS_APPROVER_5);
					user.setPreHandOverApproverType(com.common.constants.Constants.STATUS_APPROVER_3);
					break;
				case com.common.constants.Constants.DESIGNATION_CEO:
					user.setApproverType(com.common.constants.Constants.STATUS_APPROVER_6);
					user.setPreHandOverApproverType(com.common.constants.Constants.STATUS_APPROVER_4);
					break;
				case com.common.constants.Constants.DESIGNATION_JMD:
					user.setApproverType(com.common.constants.Constants.STATUS_APPROVER_3);
					user.setPreHandOverApproverType(com.common.constants.Constants.STATUS_APPROVER_5);
					break;
				case com.common.constants.Constants.DESIGNATION_MD:
					user.setApproverType(com.common.constants.Constants.STATUS_APPROVER_4);
					user.setPreHandOverApproverType(com.common.constants.Constants.STATUS_APPROVER_6);
				}
			}

			user.setFullName(user.getFirstName());
			if (!StringUtils.isEmpty(user.getLastName())) {
				user.setFullName(user.getFirstName() + " " + user.getLastName());
			}
			if (!StringUtils.isEmpty(user.getId())) {
				user.setUpdatedBy(UserRoleUtil.getCurrentUser());
				User user1 = this.getUserById(user.getId());
				user.setApproverType(user1.getApproverType());
				user.setPreHandOverApproverType(user1.getPreHandOverApproverType());
			} else {
				user.setCreatedBy(UserRoleUtil.getCurrentUser());
			}
		} catch (NullPointerException ex) {
			log.info("Last name null.");
		}
		int status;
		try {

			if (ObjectUtils.isEmpty(user.getId())) {
				Map<String, Object> userObject = isUserExistByUserName(user.getUsername());

				if (userObject.get(Constants.CODE).equals(200)) {
					user.setUserNameExist(true);
					return user;
				}
			} else {
				List<User> userList = this.getAllUsers().stream()
						.filter(predicate -> !predicate.getId().equals(user.getId())).collect(Collectors.toList());
				for (User user2 : userList) {
					if (user2.getUsername().equalsIgnoreCase(user.getUsername())) {
						user.setUserNameExist(true);
						return user;
					}
				}

			}

			user.setHashedUserImage("");
			// upload user image
			if (user.getUserProfileFileUpload() != null && user.getUserProfileFileUpload().getSize() != 0) {
				String fileName = user.getUserProfileFileUpload().getOriginalFilename();

				boolean isUploadSuccess = false;
				if (!isUploadSuccess) {
					throw new FeasibilityWebException("Error in AWS s3 upload ");
				}
			}
			// make multipart file as null to call rest service with json request
			user.setUserProfileFileUpload(null);
			/*
			 * if (ObjectUtils.isEmpty(user.getId())) {
			 * 
			 * if (UserRoleUtil.isCurrentUserAdmin()) { if
			 * (!ObjectUtils.isEmpty(user.getAuthorities()) &&
			 * com.common.constants.Constants.ROLE_ADMIN.equals(user.getAuthorities())) {
			 * user.setParentId(UserRoleUtil.getCurrentUserId()); } } }
			 */

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_USER_DETAILS, user, null,
					HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {

				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<User>() {
					});
				}

				return new User();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for users on web.", 500);
			}

		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving user information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			log.error("Error while saving user information");
			throw new FeasibilityWebException("Error while saving user information");
		}
	}

	public Map<String, Object> isUserExistByUserName(String userName) {

		Map<String, Object> data = new HashMap<>();
		User user = this.getUserByUserName(userName);
		if (ObjectUtils.isEmpty(user.getId())) {
			data.put(Constants.CODE, 404);
		} else {
			data.put(Constants.CODE, 200);
			data.put(Constants.USER, user);
		}

		return data;
	}

	public User getCompanyOwner(String crn) {
		int status;
		String url = String.format(URLConstants.GET_COMPANY_OWNER, crn);
		try {

			JsonNode response = authorizationUtil.makeRequestGet(url);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<User>() {
					});
				}
				return new User();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for users on web.", 500);
			}

		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching form user by username", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			log.error("Error while fetching user by username.");
			throw new FeasibilityWebException("Error while fetching user by username.");
		}
	}

	public User getUserByUserName(String username) {
		int status;
		String url = String.format(URLConstants.GET_USER_BY_USER_NAME, username);
		try {

			JsonNode response = authorizationUtil.makeRequestGet(url);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<User>() {
					});
				}
				return new User();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for users on web.", 500);
			}

		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching form user by username", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			log.error("Error while fetching user by username.");
			throw new FeasibilityWebException("Error while fetching user by username.");
		}
	}

	private void validateUser(User user) {

		if (ObjectUtils.isEmpty(user)) {
			log.info("user canot be null occur on web validation while saving.");
			throw new FeasibilityWebException("user cannot empty while saving");
		}

		try {

			User existingUser = this.getUserByUserName(user.getUsername());
			if (!ObjectUtils.isEmpty(existingUser) && (ObjectUtils.isEmpty(existingUser.getHashedUserImage())
					|| !existingUser.getHashedUserImage().equals(user.getHashedUserImage()))) {
				if (!ObjectUtils.isEmpty(user.getUserProfileFileUpload()) && !ObjectUtils.isEmpty(user.getUsername())) {

					user.setHashedUserImage(
							fileUtilityService.getFileName(user.getUserProfileFileUpload().getOriginalFilename()));
					fileUtilityService.saveFile(user.getUserProfileFileUpload(), user.getUsername(), "profile",
							user.getHashedUserImage());
				}
			}

		} catch (FeasibilityWebException e) {
			log.info("error occur on uploading profile while user saving.");
			throw new FeasibilityWebException("user cannot empty while saving", e);
		}
	}

	public User getUserById(String userId) {

		int status;
		String url = String.format(URLConstants.GET_USER_DETAIL_BY_ID, userId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();

					return OBJECT_MAPPER.readValue(data, new TypeReference<User>() {
					});
				}
				return new User();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for User on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching User by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching User by id");
			throw new FeasibilityWebException("Error while fetching User by id");
		}
	}

	public List<User> getUserEmployeeDetailsById(String userId) {

		int status;
		String url = String.format(URLConstants.GET_USER_EMPLOYEE_DETAIL_BY_ID, userId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<User>>() {
					});
				}
				return new ArrayList<User>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for User on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching User by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching User details by id");
			throw new FeasibilityWebException("Error while fetching User details by id");
		}
	}

	public List<User> getUserByAuthority(String authority) {

		int status;
		String url = String.format(URLConstants.GET_USER_BY_AUTHORITY, authority);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<User>>() {
					});
				}
				return new ArrayList<User>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for User on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching User by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching User details by id");
			throw new FeasibilityWebException("Error while fetching User details by id");
		}
	}

	public List<ApproverMapper> getAllApprover() {
		List<ApproverMapper> approver = new ArrayList<>();

		List<User> getAllUser = this.getAllUsers();
		List<User> userAll = getAllUser.stream()
				.filter(predicate -> predicate.getAuthorities().equals(com.common.constants.Constants.ROLE_APPROVER))
				.collect(Collectors.toList());

		List<Integer> dataInputValue = UserRoleUtil.getAllApproverType();

		if (!ObjectUtils.isEmpty(userAll)) {
			List<Integer> type = userAll.stream().map(User::getApproverType).collect(Collectors.toList());
			dataInputValue.removeAll(type);
		}
		for (Integer approverType : dataInputValue) {

			ApproverMapper approvedUser = new ApproverMapper();

			switch (approverType) {
			case com.common.constants.Constants.STATUS_APPROVER_1:

				approvedUser.setApprover(com.common.constants.Constants.ROLE_APPROVER_1);
				approvedUser.setValue(com.common.constants.Constants.STATUS_APPROVER_1);

				break;
			case com.common.constants.Constants.STATUS_APPROVER_2:

				approvedUser.setApprover(com.common.constants.Constants.ROLE_APPROVER_2);
				approvedUser.setValue(com.common.constants.Constants.STATUS_APPROVER_2);

				break;
			case com.common.constants.Constants.STATUS_APPROVER_3:

				approvedUser.setApprover(com.common.constants.Constants.ROLE_APPROVER_3);
				approvedUser.setValue(com.common.constants.Constants.STATUS_APPROVER_3);

				break;
			case com.common.constants.Constants.STATUS_APPROVER_4:

				approvedUser.setApprover(com.common.constants.Constants.ROLE_APPROVER_4);
				approvedUser.setValue(com.common.constants.Constants.STATUS_APPROVER_4);

				break;
			case com.common.constants.Constants.STATUS_APPROVER_5:

				approvedUser.setApprover(com.common.constants.Constants.ROLE_APPROVER_5);
				approvedUser.setValue(com.common.constants.Constants.STATUS_APPROVER_5);

				break;
			case com.common.constants.Constants.STATUS_APPROVER_6:

				approvedUser.setApprover(com.common.constants.Constants.ROLE_APPROVER_6);
				approvedUser.setValue(com.common.constants.Constants.STATUS_APPROVER_6);
				break;
			case com.common.constants.Constants.STATUS_APPROVER_7:

				approvedUser.setApprover(com.common.constants.Constants.ROLE_APPROVER_7);
				approvedUser.setValue(com.common.constants.Constants.STATUS_APPROVER_7);

				break;
			case com.common.constants.Constants.STATUS_APPROVER_8:

				approvedUser.setApprover(com.common.constants.Constants.ROLE_APPROVER_8);
				approvedUser.setValue(com.common.constants.Constants.STATUS_APPROVER_8);

				break;

			}
			approver.add(approvedUser);
		}

		return approver;
	}

	public List<String> getDesignationList() {
		List<String> designationList = new ArrayList<String>();
		List<User> approvers = this.getUserByAuthority(Constants.ROLE_APPROVER);
		designationList.add(com.common.constants.Constants.DESIGNATION_CDO);
		designationList.add(com.common.constants.Constants.DESIGNATION_CO);
		designationList.add(com.common.constants.Constants.DESIGNATION_CPO);
		designationList.add(com.common.constants.Constants.DESIGNATION_CEO);
		designationList.add(com.common.constants.Constants.DESIGNATION_JMD);
		designationList.add(com.common.constants.Constants.DESIGNATION_MD);

		if (approvers != null) {
			for (int i = 0; i < approvers.size(); i++) {
				User user = approvers.get(i);
				String userDesignation = user.getDesignation();
				if (userDesignation != null) {
					switch (userDesignation) {
					case com.common.constants.Constants.DESIGNATION_CDO:
						designationList.remove(com.common.constants.Constants.DESIGNATION_CDO);
						break;
					case com.common.constants.Constants.DESIGNATION_CO:
						designationList.remove(com.common.constants.Constants.DESIGNATION_CO);
						break;
					case com.common.constants.Constants.DESIGNATION_CPO:
						designationList.remove(com.common.constants.Constants.DESIGNATION_CPO);
						break;
					case com.common.constants.Constants.DESIGNATION_CEO:
						designationList.remove(com.common.constants.Constants.DESIGNATION_CEO);
						break;
					case com.common.constants.Constants.DESIGNATION_JMD:
						designationList.remove(com.common.constants.Constants.DESIGNATION_JMD);
						break;
					case com.common.constants.Constants.DESIGNATION_MD:
						designationList.remove(com.common.constants.Constants.DESIGNATION_MD);

					}
				}
			}
		}
		return designationList;
	}

	public User getUserByApproverId(Integer approverId) {

		int status;
		String url = String.format(URLConstants.GET_USER_DETAIL_BY_APPROVER_ID, approverId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();

					return OBJECT_MAPPER.readValue(data, new TypeReference<User>() {
					});
				}
				return new User();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for User on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching User by approverId", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching User by approverId");
			throw new FeasibilityWebException("Error while fetching User by approverId");
		}
	}

	public String[] forgotPassword(String email) {
		
		User user = new User();
		user.setEmail(email);
		int status;
		try {
			
			JsonNode response = restServiceUtil.makeRequest(URLConstants.FORGOT_PASSWORD,user,null,HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<String[]>() {
					});
				}
				return null;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for users on web.", 500);
			}

		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching form user by username", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			log.error("Error while fetching user by username.");
			throw new FeasibilityWebException("Error while fetching user by username.");
		}
		
	}

}
