package com.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.MarkConstant;
import com.util.RestServiceUtil;
import com.util.URLConstants;
import com.web.FeasibilityWebException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@SuppressWarnings("rawtypes")
public class ViewModalDetailServiceImpl implements ViewModalDetailService {

	@Autowired
	private RestServiceUtil restServiceUtil;

	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@Override
	public Map<String, ? super ArrayList> getTotalAdmits(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_TOTAL_ADMITS, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	public Map<String, ? super ArrayList> getAdRevenue(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_AD_REVENUE, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	public Map<String, ? super ArrayList> getPersonalExpense(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_PERSONAL_EXPENSES, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public Map<String, ? super ArrayList> getElectricityExpense(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_ELECTRITY_EXPENSES, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public Map<String, ? super ArrayList> getRmExpense(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_RM_EXPENSES, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public Map<String, ? super ArrayList> getWebSales(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_WEB_SALES, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public Map<String, ? super ArrayList> getHousekeeping(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_HOUSEKEPING, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}

	}

	@Override
	public Map<String, ? super ArrayList> getSecurityExpense(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_SECURITY_EXPENSE, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}

	}

	@Override
	public Map<String, ? super ArrayList> getCommunicationExpense(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_COMMUNICATION_EXPENSE, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}

	}

	@Override
	public Map<String, ? super ArrayList> getTravellingExpense(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_TRAVELLING_EXPENSE, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}

	}

	@Override
	public Map<String, ? super ArrayList> getLeegalfee(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_LEEGAL_FEE, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}

	}

	@Override
	public Map<String, ? super ArrayList> getInsuranceExpense(String projectId) {

		int status;
		String url = String.format(URLConstants.GET_INSURANCE_EXPENSE, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public Map<String, ? super ArrayList> getInternetExpenses(String projectId) {

		int status;
		String url = String.format(URLConstants.GET_INTERNET_EXPENSES, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public Map<String, ? super ArrayList> getPrintingStationary(String projectId) {

		int status;
		String url = String.format(URLConstants.GET_PRINTING_STATIONARY, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public Map<String, ? super ArrayList> getMarketingExpense(String projectId) {

		int status;
		String url = String.format(URLConstants.GET_MARKETING_EXPENSE, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public Map<String, ? super ArrayList> getMiscellaneousExpense(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_MISCELLANEOUS_EXPENSE, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}

	}

	@Override
	public Map<String, ? super ArrayList> getAnnualRent(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_ANNUAL_RENT, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public Map<String, ? super Object> getViewCalculation(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_VIEW_CALCULATION, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

	@Override
	public Map<String, ? super Object> getViewOccupancy(String projectId) {
		int status;
		String url = String.format(URLConstants.GET_VIEW_OCCUPANCY, projectId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, ? super ArrayList>>() {
					});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Latest Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Latest Project");
			throw new FeasibilityWebException("Error while fetching Latest Project");
		}
	}

}
