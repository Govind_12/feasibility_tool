package com.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import com.common.constants.Constants;
import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.ProjectDetails;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.MarkConstant;
import com.util.RestServiceUtil;
import com.util.URLConstants;
import com.util.UserRoleUtil;
import com.web.FeasibilityWebException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PreHandoverServiceImpl implements PreHandoverService {

	@Value("${path.fileUploadPath}")
	private String uploFilePath;

	@Autowired
	private RestServiceUtil restServiceUtil;

	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	public ProjectDetails savePreHandoverStep1(ProjectDetails projectDetails) {

		if (ObjectUtils.isEmpty(projectDetails)) {
			throw new FeasibilityWebException("Project details  info cannot be empty");
		}

		int status;

		MultipartFile file = projectDetails.getUploadFileStep1();
		String filePath = "";

		if (file != null && !file.isEmpty()) {
			try {
				String uploadsDir = "step_1_" + UserRoleUtil.generateNewReportId() + "_";
				String path = uploFilePath;

				if (!new File(path).exists()) {
					new File(path).mkdirs();
				}

				String fileName = file.getOriginalFilename();
				filePath = path + "/" + uploadsDir + fileName;
				File dest = new File(filePath);
				file.transferTo(dest);
				projectDetails.setFileURLStep1(uploadsDir + fileName);

			} catch (Exception e) {
				e.printStackTrace();
			}
			projectDetails.setUploadFileStep1(null);

		} else {
			projectDetails.setUploadFileStep1(null);
			projectDetails.setFileURLStep1(null);

		}

		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_PRE_HANDOVER_DEVELOPMENT_DETAILS,
					projectDetails, null, HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
					});
				}
				return new ProjectDetails();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while saving Project detail information");
		}
	}

	@Override
	public List<ProjectDetails> getAllExistingProjects() {
		int status;
		String url = String.format(URLConstants.GET_ALL_PRE_HANDOVER_EXISTING_PROJECT);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {
					});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Project", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching User by id");
			throw new FeasibilityWebException("Error while fetching Project");
		}
	}

	@Override
	public QueryMapper submitQuery(MsgMapper SAVE_QUERY) {
		int status;

		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.PRE_HANDOVER_SAVE_QUERY, SAVE_QUERY, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<QueryMapper>() {
					});
				}
				return new QueryMapper();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving query", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving query");
			throw new FeasibilityWebException("Error while saving query");
		}
	}

	@Override
	public QueryMapper submitFinalQuery(MsgMapper SAVE_QUERY) {
		int status;

		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.PRE_HANDOVER_SAVE_QUERY_FINAL, SAVE_QUERY,
					null, HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<QueryMapper>() {
					});
				}
				return new QueryMapper();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving query", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving query");
			throw new FeasibilityWebException("Error while saving query");
		}
	}

	@Override
	public String submitForOperationOrProjectTeam(MsgMapper msgMapper) {
		int status;

		try {
			// String url =
			// String.format(URLConstants.SEND_FOR_OPREATION_PROJECT_TEAM,msgMapper);
			JsonNode response = restServiceUtil.makeRequest(URLConstants.SEND_FOR_OPREATION_PROJECT_TEAM, msgMapper,
					null, HttpMethod.POST);
			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<String>() {
					});
				}
				return "";
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();

			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while updating projectId and status", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving query");
			throw new FeasibilityWebException("Error while updating projectId and status");
		}
	}

	@Override
	public boolean updateNotificationStatus(String projectId,String queryType,String step) {
		int status;
		String url = String.format(URLConstants.UPDATE_NOTIFICATION_STATUS, projectId,queryType,step);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return false;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for upadte Notification on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while upadte Notification", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while upadte Notification");
			throw new FeasibilityWebException("Error while upadte Notification ");
		}
	}
	
}
