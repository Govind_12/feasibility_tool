package com.services.mis;

import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.common.mappers.MisMapper;
import com.common.models.FixedConstant;
import com.common.models.IrrCalculationData;
import com.common.models.ProjectDetails;
import com.helper.MisHelper;
import com.services.ProjectDetailService;
import org.apache.poi.ss.usermodel.Font;

@Service
public class MisServiceImpl implements MisService {

	@Autowired
	private ProjectDetailService projectDetailService;

	public static Date getDateObj(String date) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (date != null)
			try {
				return format.parse(date);
			} catch (ParseException e) {
				SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
				try {
					return format1.parse(date);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		return null;

	}

	public List<MisMapper> getMisProjects(MisHelper misHelper) {
		List<MisMapper> list = projectDetailService.fetchAllProjects().stream()
				.filter(data -> data.getDateOfSendForApproval() != null).collect(Collectors.toList());
		return list.stream()
				.filter(dates -> getDateObj(dates.getDateOfSendForApproval()).before(getDateObj(misHelper.getEndDate()))
						&& getDateObj(dates.getDateOfSendForApproval()).after(getDateObj(misHelper.getStartDate())))
				.collect(Collectors.toList());
	}

	public List<MisMapper> getOperationalMisProjects(MisHelper misHelper) {
		List<MisMapper> list = (List<MisMapper>) this.projectDetailService.fetchAllOperationalProjects().stream()
				.filter(data -> (data.getDateOfSendForApproval() != null)).collect(Collectors.toList());
		return (List<MisMapper>) list.stream().filter(
				dates -> (getDateObj(dates.getDateOfSendForApproval()).before(getDateObj(misHelper.getEndDate()))
						&& getDateObj(dates.getDateOfSendForApproval()).after(getDateObj(misHelper.getStartDate()))))

				.collect(Collectors.toList());
	}

	public List<MisMapper> getExitMisProjects(MisHelper misHelper) {
		List<MisMapper> list = (List<MisMapper>) this.projectDetailService.fetchAllExitProjects().stream()
				.filter(data -> (data.getDateOfSendForApproval() != null)).collect(Collectors.toList());
		return (List<MisMapper>) list.stream().filter(
				dates -> (getDateObj(dates.getDateOfSendForApproval()).before(getDateObj(misHelper.getEndDate()))
						&& getDateObj(dates.getDateOfSendForApproval()).after(getDateObj(misHelper.getStartDate()))))

				.collect(Collectors.toList());
	}

	@Override
	public byte[] getMis(MisHelper misHelper) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet spreadsheet = workbook.createSheet();
		XSSFCellStyle header = workbook.createCellStyle();
		header.setAlignment(CellStyle.ALIGN_CENTER);
		header.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
		header.setFillPattern(CellStyle.SOLID_FOREGROUND);
		header.setBorderTop(BorderStyle.THICK);
		header.setTopBorderColor(IndexedColors.BLACK.getIndex());
		header.setBorderLeft(BorderStyle.THICK);
		header.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		header.setBorderRight(BorderStyle.THICK);
		header.setRightBorderColor(IndexedColors.BLACK.getIndex());

		XSSFFont headerFont = (XSSFFont) workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10.5);
		headerFont.setBold(true);
		headerFont.setItalic(false);
		header.setFont(headerFont);
		XSSFRow row = spreadsheet.createRow(0);
		XSSFCell cell;
		// first row
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 5));
		cell = row.createCell(0);
		cell.setCellValue("PROJECT DETAILS");
		cell.setCellStyle(header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 6, 14));
		cell = row.createCell(5);
		cell.setCellValue("DEVELOPMENT DETAILS");
		cell.setCellStyle(header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 15, 22));
		cell = row.createCell(14);
		cell.setCellValue("DEVELOPER DETAILS");
		cell.setCellStyle(header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 23, 33));
		cell = row.createCell(22);
		cell.setCellValue("CINEMA SPECS");
		cell.setCellStyle(header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 34, 41));
		cell = row.createCell(33);
		cell.setCellValue("CAPEX");
		cell.setCellStyle(header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 42, 55));
		cell = row.createCell(41);
		cell.setCellValue("KEY LEASE TERMS");
		cell.setCellStyle(header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 56, 61));
		cell = row.createCell(55);
		cell.setCellValue("OPERATING ASSUMPTIONS");
		cell.setCellStyle(header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 62, 79));
		cell = row.createCell(61);
		cell.setCellValue("FINANCIALS");
		cell.setCellStyle(header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 80, 81));
		cell = row.createCell(79);
		cell.setCellValue("RETURN METRICES");
		cell.setCellStyle(header);

		// second row
		XSSFCellStyle header1 = workbook.createCellStyle();
		header1.setAlignment(CellStyle.ALIGN_JUSTIFY);
		header1.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		header1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		header1.setBorderTop(BorderStyle.THIN);
		header1.setTopBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderLeft(BorderStyle.THIN);
		header1.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderRight(BorderStyle.THIN);
		header1.setRightBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderBottom(BorderStyle.THICK);
		header1.setBottomBorderColor(IndexedColors.BLACK.getIndex());

		headerFont = (XSSFFont) workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10.5);
		headerFont.setBold(false);
		headerFont.setItalic(false);
		header1.setFont(headerFont);
		// spreadsheet.setColumnWidth(13, 10);
		row = spreadsheet.createRow(1);
		row.setHeight((short) 600);
		cell = row.createCell(0);
		cell.setCellValue("Proj Code");
		cell.setCellStyle(header1);
		cell = row.createCell(1);
		cell.setCellValue("Property Name");
		cell.setCellStyle(header1);
		cell = row.createCell(2);
		cell.setCellValue("Feasibility Date");
		cell.setCellStyle(header1);
		cell = row.createCell(3);
		cell.setCellValue("Handover Date");
		cell.setCellStyle(header1);

		cell = row.createCell(4);
		cell.setCellValue("Feasibility Stage");
		cell.setCellStyle(header1);

		cell = row.createCell(5);
		cell.setCellValue("Likely Date of Opening");
		cell.setCellStyle(header1);
		cell = row.createCell(6);
		cell.setCellValue("City");
		cell.setCellStyle(header1);
		cell = row.createCell(7);
		cell.setCellValue("State");
		cell.setCellStyle(header1);
		cell = row.createCell(8);
		cell.setCellValue("City Tier");
		cell.setCellStyle(header1);
		cell = row.createCell(9);
		cell.setCellValue("City Population");
		cell.setCellStyle(header1);
		cell = row.createCell(10);
		cell.setCellValue("Development Size");
		cell.setCellStyle(header1);
		cell = row.createCell(11);
		cell.setCellValue("Retail Area of the Project");
		cell.setCellStyle(header1);
		cell = row.createCell(12);
		cell.setCellValue("Type of Project");
		cell.setCellStyle(header1);
		cell = row.createCell(13);
		cell.setCellValue("Tenancy Mix");
		cell.setCellStyle(header1);
		cell = row.createCell(14);
		cell.setCellValue("Upcoming Infrastructure  development in catchment");
		cell.setCellStyle(header1);
		cell = row.createCell(15);
		cell.setCellValue("Developer Name");
		cell.setCellStyle(header1);
		cell = row.createCell(16);
		cell.setCellValue("Name of owners");
		cell.setCellStyle(header1);
		cell = row.createCell(17);
		cell.setCellValue("Gross turnover of company");
		cell.setCellStyle(header1);
		cell = row.createCell(18);
		cell.setCellValue("Top 3 projects delivered");
		cell.setCellStyle(header1);
		cell = row.createCell(19);
		cell.setCellValue("PVR projects in pipeline");
		cell.setCellStyle(header1);
		cell = row.createCell(20);
		cell.setCellValue("PVR projects delivered");
		cell.setCellStyle(header1);
		cell = row.createCell(21);
		cell.setCellValue("Retail Projects Delivered");
		cell.setCellStyle(header1);
		cell = row.createCell(22);
		cell.setCellValue("Current project financing");
		cell.setCellStyle(header1);
		cell = row.createCell(23);
		cell.setCellValue("No. of Screens");
		cell.setCellStyle(header1);
		cell = row.createCell(24);
		cell.setCellValue("Mainstream");
		cell.setCellStyle(header1);
		cell = row.createCell(25);
		cell.setCellValue("Directors Cut");
		cell.setCellStyle(header1);
		cell = row.createCell(26);
		cell.setCellValue("Gold");
		cell.setCellStyle(header1);
		cell = row.createCell(27);
		cell.setCellValue("IMAX");
		cell.setCellStyle(header1);
		cell = row.createCell(28);
		cell.setCellValue("PXL");
		cell.setCellStyle(header1);
		cell = row.createCell(29);
		cell.setCellValue("4DX ");
		cell.setCellStyle(header1);
		cell = row.createCell(30);
		cell.setCellValue("Playhouse");
		cell.setCellStyle(header1);
		cell = row.createCell(31);
		cell.setCellValue("Onyx");
		cell.setCellStyle(header1);
		cell = row.createCell(32);
		cell.setCellValue("Others");
		cell.setCellStyle(header1);
		cell = row.createCell(33);
		cell.setCellValue("Total Seats");
		cell.setCellStyle(header1);
		cell = row.createCell(34);
		cell.setCellValue("Security Deposit");
		cell.setCellStyle(header1);
		cell = row.createCell(35);
		cell.setCellValue("Security Deposit (# of months of rent)");
		cell.setCellStyle(header1);
		cell = row.createCell(36);
		cell.setCellValue("Security Deposit (# of months of cam)");
		cell.setCellStyle(header1);
		cell = row.createCell(37);
		cell.setCellValue("Security Deposit (Rent)");
		cell.setCellStyle(header1);
		cell = row.createCell(38);
		cell.setCellValue("Security Deposit (CAM)");
		cell.setCellStyle(header1);
		cell = row.createCell(39);
		cell.setCellValue("Project cost");
		cell.setCellStyle(header1);
		cell = row.createCell(40);
		cell.setCellValue("Project Capex + Deposit");
		cell.setCellStyle(header1);
		cell = row.createCell(41);
		cell.setCellValue("Cost Per Screen");
		cell.setCellStyle(header1);

		cell = row.createCell(42);
		cell.setCellValue("Lease Tenure");
		cell.setCellStyle(header1);
		cell = row.createCell(43);
		cell.setCellValue("PVR Lock-in Period");
		cell.setCellStyle(header1);
		cell = row.createCell(44);
		cell.setCellValue("Developer Lock-in Period");
		cell.setCellStyle(header1);
		cell = row.createCell(45);
		cell.setCellValue("Gross Leasable Area");
		cell.setCellStyle(header1);
		cell = row.createCell(46);
		cell.setCellValue("Loading %");
		cell.setCellStyle(header1);
		cell = row.createCell(47);
		cell.setCellValue("Carpet Area");
		cell.setCellStyle(header1);
		cell = row.createCell(48);
		cell.setCellValue("Area per seat");
		cell.setCellStyle(header1);
		cell = row.createCell(49);
		cell.setCellValue("Explain Rent Agreement");
		cell.setCellStyle(header1);
		cell = row.createCell(50);
		cell.setCellValue("Explain CAM agreements");
		cell.setCellStyle(header1);
		cell = row.createCell(51);
		cell.setCellValue("Rent per sq ft");
		cell.setCellStyle(header1);
		cell = row.createCell(52);
		cell.setCellValue("CAM per sq ft");
		cell.setCellStyle(header1);
		cell = row.createCell(53);
		cell.setCellValue("Revenue Share - Box");
		cell.setCellStyle(header1);
		cell = row.createCell(54);
		cell.setCellValue("Revenue Share - F&B");
		cell.setCellStyle(header1);
		cell = row.createCell(55);
		cell.setCellValue("Revenue Share - Ad");
		cell.setCellStyle(header1);
		cell = row.createCell(56);
		cell.setCellValue("Revenue Share - Others");
		cell.setCellStyle(header1);
		cell = row.createCell(57);
		cell.setCellValue("Yr 2 Admits");
		cell.setCellStyle(header1);
		cell = row.createCell(58);
		cell.setCellValue("Yr 2 Occupancy");
		cell.setCellStyle(header1);
		cell = row.createCell(59);
		cell.setCellValue("Yr 2 ATP");
		cell.setCellStyle(header1);
		cell = row.createCell(60);
		cell.setCellValue("Yr 2 SPH");
		cell.setCellStyle(header1);
		cell = row.createCell(61);
		cell.setCellValue("SPH:ATP ratio");
		cell.setCellStyle(header1);

		cell = row.createCell(62);
		cell.setCellValue("Yr 2 Revenue");
		cell.setCellStyle(header1);
		cell = row.createCell(63);
		cell.setCellValue("Yr 2 Net Sale of Tickets");
		cell.setCellStyle(header1);
		cell = row.createCell(64);
		cell.setCellValue("Yr 2 Sale of F&B");
		cell.setCellStyle(header1);
		cell = row.createCell(65);
		cell.setCellValue("Yr 2 Ad revenue");
		cell.setCellStyle(header1);
		cell = row.createCell(66);
		cell.setCellValue("Yr 2 VAS Income");
		cell.setCellStyle(header1);
		cell = row.createCell(67);
		cell.setCellValue("Yr 2 Other Operating Income");
		cell.setCellStyle(header1);
		cell = row.createCell(68);
		cell.setCellValue("Yr 2 EBITDA");
		cell.setCellStyle(header1);
		cell = row.createCell(69);
		cell.setCellValue("Yr 2 EBITDA margin");
		cell.setCellStyle(header1);
		cell = row.createCell(70);
		cell.setCellValue("Film Distributor's Share");
		cell.setCellStyle(header1);
		cell = row.createCell(71);
		cell.setCellValue("Consumption of F&B");
		cell.setCellStyle(header1);
		cell = row.createCell(72);
		cell.setCellValue("Gross Margin");
		cell.setCellStyle(header1);
		cell = row.createCell(73);
		cell.setCellValue("Personnel Expenses");
		cell.setCellStyle(header1);
		cell = row.createCell(74);
		cell.setCellValue("Rent");
		cell.setCellStyle(header1);
		cell = row.createCell(75);
		cell.setCellValue("CAM");
		cell.setCellStyle(header1);
		cell = row.createCell(76);
		cell.setCellValue("Electricity & Water");
		cell.setCellStyle(header1);
		cell = row.createCell(77);
		cell.setCellValue("Repairs & Maintenance");
		cell.setCellStyle(header1);
		cell = row.createCell(78);
		cell.setCellValue("Other Operating Expenses");
		cell.setCellStyle(header1);
		cell = row.createCell(79);
		cell.setCellValue("Total Fixed Expenditure");
		cell.setCellStyle(header1);

		cell = row.createCell(80);
		cell.setCellValue("EBITDA Payback Period");
		cell.setCellStyle(header1);
		cell = row.createCell(81);
		cell.setCellValue("IRR Post tax");
		cell.setCellStyle(header1);

		// third row
		header1 = workbook.createCellStyle();
		header1.setAlignment(CellStyle.ALIGN_CENTER);
		header1.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		header1.setFillPattern(CellStyle.SOLID_FOREGROUND);

		headerFont = (XSSFFont) workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10.5);
		headerFont.setBold(false);
		headerFont.setItalic(false);
		header1.setFont(headerFont);
		row = spreadsheet.createRow(2);

		cell = row.createCell(0);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(1);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(2);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(3);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(4);
		cell.setCellValue("");
		cell.setCellStyle(header1);

		cell = row.createCell(5);
		cell.setCellValue("");
		cell.setCellStyle(header1);

		cell = row.createCell(6);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(7);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(8);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(9);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(10);
		cell.setCellValue("sq. ft.");
		cell.setCellStyle(header1);
		cell = row.createCell(11);
		cell.setCellValue("sq. ft.");
		cell.setCellStyle(header1);
		cell = row.createCell(12);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(13);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(14);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(15);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(16);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(17);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(18);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(19);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(20);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(21);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(22);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(23);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(24);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(25);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(26);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(27);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(28);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(29);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(30);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(31);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(32);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(33);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(34);
		cell.setCellValue("Rs lacs");
		cell.setCellStyle(header1);
		cell = row.createCell(35);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(36);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(37);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(38);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(39);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(40);
		cell.setCellValue("Rs lacs");
		cell.setCellStyle(header1);
		cell = row.createCell(41);
		cell.setCellValue("");
		cell.setCellStyle(header1);

		cell = row.createCell(42);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(43);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(44);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(45);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(46);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(47);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(48);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(49);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(50);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(51);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(52);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(53);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(54);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(55);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(56);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(57);
		cell.setCellValue("Rs lacs");
		cell.setCellStyle(header1);
		cell = row.createCell(58);
		cell.setCellValue("%");
		cell.setCellStyle(header1);
		cell = row.createCell(59);
		cell.setCellValue("Rs");
		cell.setCellStyle(header1);
		cell = row.createCell(60);
		cell.setCellValue("Rs");
		cell.setCellStyle(header1);
		cell = row.createCell(61);
		cell.setCellValue("");
		cell.setCellStyle(header1);

		cell = row.createCell(62);
		cell.setCellValue("Rs lacs");
		cell.setCellStyle(header1);
		cell = row.createCell(63);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(64);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(65);
		cell.setCellValue("Rs lacs");
		cell.setCellStyle(header1);
		cell = row.createCell(66);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(67);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(68);
		cell.setCellValue("Rs lacs");
		cell.setCellStyle(header1);
		cell = row.createCell(69);
		cell.setCellValue("%");
		cell.setCellStyle(header1);
		cell = row.createCell(70);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(71);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(72);
		cell.setCellValue("%");
		cell.setCellStyle(header1);
		cell = row.createCell(73);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(74);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(75);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(76);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(77);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(78);
		cell.setCellValue("");
		cell.setCellStyle(header1);
		cell = row.createCell(79);
		cell.setCellValue("");
		cell.setCellStyle(header1);

		cell = row.createCell(80);
		cell.setCellValue("%");
		cell.setCellStyle(header1);
		cell = row.createCell(81);
		cell.setCellValue("%");
		cell.setCellStyle(header1);

		row.setHeight((short) 300);

		// FOURTH ROW
		row = spreadsheet.createRow(3);
		short rowNo = 3;
		List<MisMapper> projectList = this.getMisProjects(misHelper);
		if (!ObjectUtils.isEmpty(projectList) && projectList.size() > 0) {

			for (MisMapper project : projectList) {
				row = spreadsheet.createRow(rowNo);

				row.createCell((short) 0).setCellValue(project.getReportId());
				row.createCell((short) 1).setCellValue(project.getProjectName());
				row.createCell((short) 2).setCellValue(project.getDateOfSendForApproval());
				row.createCell((short) 3).setCellValue(project.getDateOfHandover());
				System.err.println("project.getFeasibilityState()-------------------->>>>"+project.getFeasibilityState());
				row.createCell((short) 4).setCellValue(project.getFeasibilityState());
				row.createCell((short) 5).setCellValue(project.getDateOfOpening());
				row.createCell((short) 6).setCellValue(project.getProjectLocation().getCity());
				row.createCell((short) 7).setCellValue(project.getProjectLocation().getState());
				row.createCell((short) 8).setCellValue(project.getProjectLocation().getCityTier());
				row.createCell((short) 9).setCellValue(project.getCityPopulation().toString());
				row.createCell((short) 10).setCellValue(project.getTotalDeveloperSize());
				row.createCell((short) 11).setCellValue(project.getRetailAreaOfProject());
				row.createCell((short) 12).setCellValue(project.getProjectType());
				row.createCell((short) 13).setCellValue(project.getTenancyMix());
				row.createCell((short) 14).setCellValue(project.getUpcomingInfrastructureDevlopment());
				row.createCell((short) 15).setCellValue(project.getDeveloper());
				row.createCell((short) 16).setCellValue(project.getDeveloperOwner());
				row.createCell((short) 17).setCellValue(project.getGrossTurnOver());

				if (!ObjectUtils.isEmpty(project.getProjectDelivered1())
						&& !ObjectUtils.isEmpty(project.getProjectDelivered2())
						&& !ObjectUtils.isEmpty(project.getProjectDelivered3())) {
					row.createCell((short) 18).setCellValue(project.getProjectDelivered1() + ","
							+ project.getProjectDelivered2() + "," + project.getProjectDelivered3());
				} else if (!ObjectUtils.isEmpty(project.getProjectDelivered1())
						&& !ObjectUtils.isEmpty(project.getProjectDelivered2())) {
					row.createCell((short) 18)
							.setCellValue(project.getProjectDelivered1() + "," + project.getProjectDelivered2());
				} else {
					row.createCell((short) 18).setCellValue(project.getProjectDelivered1());
				}

				if (!ObjectUtils.isEmpty(project.getPvrProjectPipeline())
						&& project.getPvrProjectPipeline().size() > 0) {
					StringBuilder str = new StringBuilder();
					for (int i = 0; i < project.getPvrProjectPipeline().size(); i++) {
						str.append(project.getPvrProjectPipeline().get(i));
						str.append(",");
					}
					row.createCell((short) 19).setCellValue(str.toString());
				} else
					row.createCell((short) 19).setCellValue("");
				if (!ObjectUtils.isEmpty(project.getPvrProjectDelivered())
						&& project.getPvrProjectDelivered().size() > 0) {
					StringBuilder str = new StringBuilder();
					for (int i = 0; i < project.getPvrProjectDelivered().size(); i++) {
						str.append(project.getPvrProjectDelivered().get(i));
						str.append(",");
					}
					row.createCell((short) 20).setCellValue(str.toString());
				} else
					row.createCell((short) 20).setCellValue("");
				row.createCell((short) 21).setCellValue(project.getRetailProjectDelivered());
				row.createCell((short) 22).setCellValue(project.getCurrentProjectFinancing());

				row.createCell((short) 23)
						.setCellValue(project.getNoOfScreens() != null ? project.getNoOfScreens() : 0);
				row.createCell((short) 24).setCellValue(project.getMainstream().toString());
				row.createCell((short) 25).setCellValue(project.getDirectorsCut().toString());
				row.createCell((short) 26).setCellValue(project.getGold().toString());
				row.createCell((short) 27).setCellValue(project.getImax().toString());
				row.createCell((short) 28).setCellValue(project.getPxl().toString());
				row.createCell((short) 29).setCellValue(project.getDx().toString());
				row.createCell((short) 30).setCellValue(project.getPlayhouse().toString());
				row.createCell((short) 31).setCellValue(project.getOnyx().toString());
				row.createCell((short) 32).setCellValue(project.getOthers().toString());
				row.createCell((short) 33)
						.setCellValue(project.getTotalColumnValSum() != null ? project.getTotalColumnValSum() : 0);

				row.createCell((short) 34).setCellValue(
						project.getSecurityDeposit() != null ? project.getSecurityDeposit().toString() : "");
				row.createCell((short) 35).setCellValue(project.getSecurityDepositForMonthRent().toString());
				row.createCell((short) 36).setCellValue(project.getSecurityDepositForMonthCam().toString());
				row.createCell((short) 37).setCellValue(project.getRentSecurityDepositForFixedZeroMg().toString());
				row.createCell((short) 38).setCellValue(project.getCamSecurityDepositForFixedZeroMg().toString());
				row.createCell((short) 39)
						.setCellValue(project.getProjectCost() != null ? project.getProjectCost() : 0);
				row.createCell((short) 40)
						.setCellValue(project.getSecurityCapex() != null ? project.getSecurityCapex().toString() : "");
				row.createCell((short) 41).setCellValue(
						project.getProjectCostPerScreen() != null ? project.getProjectCostPerScreen() : 0);
				row.createCell((short) 42)
						.setCellValue(project.getLeasePeriod() != null ? project.getLeasePeriod() : 0);
				row.createCell((short) 43)
						.setCellValue(project.getPvrLockInPeriod() != null ? project.getPvrLockInPeriod() : 0);
				row.createCell((short) 44).setCellValue(
						project.getDevloperLockedInPeriod() != null ? project.getDevloperLockedInPeriod() : 0);
				row.createCell((short) 45)
						.setCellValue(project.getLeasableArea() != null ? project.getLeasableArea() : 0);
				row.createCell((short) 46)
						.setCellValue(project.getLoadingPercentage() != null ? project.getLoadingPercentage() : 0);
				row.createCell((short) 47).setCellValue(project.getCarpetArea() != null ? project.getCarpetArea() : 0);
				row.createCell((short) 48).setCellValue(
						project.getCarpetAreaPerSeat() != null ? project.getCarpetAreaPerSeat().toString() : "");
				row.createCell((short) 49).setCellValue(project.getRentAgreement());
				row.createCell((short) 50).setCellValue(project.getCamAgreement());
				row.createCell((short) 51).setCellValue(project.getRentPerSqFt().toString());
				row.createCell((short) 52).setCellValue(project.getCamPerSqFt().toString());
				row.createCell((short) 53).setCellValue(project.getBoxOfficePercentage().toString());
				row.createCell((short) 54).setCellValue(project.getFAndBPercentage().toString());
				row.createCell((short) 55).setCellValue(project.getAdSalePercentage().toString());
				row.createCell((short) 56).setCellValue(project.getOthersPercentage().toString());

				row.createCell((short) 57)
						.setCellValue(project.getAdmits() != null ? project.getAdmits().toString() : "");
				row.createCell((short) 58)
						.setCellValue(project.getOccupancy() != null ? project.getOccupancy().toString() : "");
				row.createCell((short) 59).setCellValue(project.getAtp() != null ? project.getAtp().toString() : "");
				row.createCell((short) 60).setCellValue(project.getSph() != null ? project.getSph().toString() : "");
				row.createCell((short) 61)
						.setCellValue(project.getAtpSphRatio() != null ? project.getAtpSphRatio().toString() : "");

				row.createCell((short) 62)
						.setCellValue(project.getRevenue() != null ? project.getRevenue().toString() : "");
				row.createCell((short) 63).setCellValue(
						project.getNetSaleOfTickets() != null ? project.getNetSaleOfTickets().toString() : "");
				row.createCell((short) 64)
						.setCellValue(project.getSalseOfFnB() != null ? project.getSalseOfFnB().toString() : "");
				row.createCell((short) 65)
						.setCellValue(project.getAdRevenue() != null ? project.getAdRevenue().toString() : "");
				row.createCell((short) 66)
						.setCellValue(project.getVasIncome() != null ? project.getVasIncome().toString() : "");
				row.createCell((short) 67).setCellValue(
						project.getOtherOperatingIncome() != null ? project.getOtherOperatingIncome().toString() : "");
				row.createCell((short) 68)
						.setCellValue(project.getEbitda() != null ? project.getEbitda().toString() : "");
				row.createCell((short) 69)
						.setCellValue(project.getEbitdaMargin() != null ? project.getEbitdaMargin().toString() : "");
				row.createCell((short) 70).setCellValue(
						project.getFilmDistributorShare() != null ? project.getFilmDistributorShare().toString() : "");
				row.createCell((short) 71).setCellValue(
						project.getFnbConsumption() != null ? project.getFnbConsumption().toString() : "");
				row.createCell((short) 72)
						.setCellValue(project.getGrossMargin() != null ? project.getGrossMargin().toString() : "");
				row.createCell((short) 73).setCellValue(
						project.getPersonnelExpenses() != null ? project.getPersonnelExpenses().toString() : "");
				row.createCell((short) 74).setCellValue(project.getRent() != null ? project.getRent().toString() : "");
				row.createCell((short) 75).setCellValue(project.getCam() != null ? project.getCam().toString() : "");

				row.createCell((short) 76).setCellValue(
						project.getElectricityWater() != null ? project.getElectricityWater().toString() : "");
				row.createCell((short) 77).setCellValue(project.getRm() != null ? project.getRm().toString() : "");
				row.createCell((short) 78)
						.setCellValue(project.getOtherOperatingExpense() != null
								? project.getOtherOperatingExpense().toString()
								: "");
				row.createCell((short) 79)
						.setCellValue(project.getTotalFixedExp() != null ? project.getTotalFixedExp().toString() : "");
				row.createCell((short) 80).setCellValue(
						project.getEbitdaPaybackPeriod() != null ? project.getEbitdaPaybackPeriod().toString() : "");
				row.createCell((short) 81)
						.setCellValue(project.getPostTaxIrr() != null ? project.getPostTaxIrr().toString() : "");

				rowNo++;
			}

		} else {
			row.createCell((short) 0).setCellValue("No data found for the dates selected");
		}

		byte[] columns = new byte[80];
		for (int j = 0; j < columns.length; j++) {
			spreadsheet.autoSizeColumn(j);
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			workbook.write(baos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] xls = baos.toByteArray();
		return xls;

	}

	public byte[] getOpsMis(MisHelper misHelper) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet spreadsheet = workbook.createSheet();
		XSSFCellStyle header = workbook.createCellStyle();
		header.setAlignment((short) 2);
		header.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
		header.setFillPattern((short) 1);
		header.setBorderTop(BorderStyle.THICK);
		header.setTopBorderColor(IndexedColors.BLACK.getIndex());
		header.setBorderLeft(BorderStyle.THICK);
		header.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		header.setBorderRight(BorderStyle.THICK);
		header.setRightBorderColor(IndexedColors.BLACK.getIndex());
		XSSFFont headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBold(true);
		headerFont.setItalic(false);
		header.setFont((Font) headerFont);
		XSSFRow row = spreadsheet.createRow(0);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4));
		XSSFCell cell = row.createCell(0);
		cell.setCellValue("PROJECT DETAILS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 5, 13));
		cell = row.createCell(5);
		cell.setCellValue("DEVELOPMENT DETAILS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 14, 21));
		cell = row.createCell(14);
		cell.setCellValue("DEVELOPER DETAILS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 22, 32));
		cell = row.createCell(22);
		cell.setCellValue("CINEMA SPECS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 33, 40));
		cell = row.createCell(33);
		cell.setCellValue("CAPEX");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 41, 54));
		cell = row.createCell(41);
		cell.setCellValue("KEY LEASE TERMS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 55, 60));
		cell = row.createCell(55);
		cell.setCellValue("OPERATING ASSUMPTIONS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 61, 78));
		cell = row.createCell(61);
		cell.setCellValue("FINANCIALS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 79, 80));
		cell = row.createCell(79);
		cell.setCellValue("RETURN METRICES");
		cell.setCellStyle((CellStyle) header);
		XSSFCellStyle header1 = workbook.createCellStyle();
		header1.setAlignment((short) 5);
		header1.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		header1.setFillPattern((short) 1);
		header1.setBorderTop(BorderStyle.THIN);
		header1.setTopBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderLeft(BorderStyle.THIN);
		header1.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderRight(BorderStyle.THIN);
		header1.setRightBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderBottom(BorderStyle.THICK);
		header1.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBold(false);
		headerFont.setItalic(false);
		header1.setFont((Font) headerFont);
		row = spreadsheet.createRow(1);
		row.setHeight((short) 600);
		cell = row.createCell(0);
		cell.setCellValue("Proj Code");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(1);
		cell.setCellValue("Property Name");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(2);
		cell.setCellValue("Created By");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(3);
		cell.setCellValue("Feasibility State");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(4);
		cell.setCellValue("Feasibility Date");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(5);
		cell.setCellValue("Handover Date");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(6);
		cell.setCellValue("Likely Date of Opening");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(7);
		cell.setCellValue("City");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(8);
		cell.setCellValue("State");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(9);
		cell.setCellValue("City Tier");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(10);
		cell.setCellValue("City Population");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(11);
		cell.setCellValue("Development Size");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(12);
		cell.setCellValue("Retail Area of the Project");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(13);
		cell.setCellValue("Type of Project");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(14);
		cell.setCellValue("Tenancy Mix");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(15);
		cell.setCellValue("Upcoming Infrastructure  development in catchment");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(16);
		cell.setCellValue("Developer Name");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(17);
		cell.setCellValue("Name of owners");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(18);
		cell.setCellValue("Gross turnover of company");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(19);
		cell.setCellValue("Top 3 projects delivered");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(20);
		cell.setCellValue("PVR projects in pipeline");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(21);
		cell.setCellValue("PVR projects delivered");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(22);
		cell.setCellValue("Retail Projects Delivered");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(23);
		cell.setCellValue("Current project financing");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(24);
		cell.setCellValue("No. of Screens");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(25);
		cell.setCellValue("Mainstream");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(26);
		cell.setCellValue("Directors Cut");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(27);
		cell.setCellValue("Gold");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(28);
		cell.setCellValue("IMAX");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(29);
		cell.setCellValue("PXL");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(30);
		cell.setCellValue("4DX ");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(31);
		cell.setCellValue("Playhouse");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(32);
		cell.setCellValue("Onyx");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(33);
		cell.setCellValue("Others");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(34);
		cell.setCellValue("Total Seats");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(35);
		cell.setCellValue("Security Deposit");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(36);
		cell.setCellValue("Security Deposit (# of months of rent)");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(37);
		cell.setCellValue("Security Deposit (# of months of cam)");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(38);
		cell.setCellValue("Security Deposit (Rent)");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(39);
		cell.setCellValue("Security Deposit (CAM)");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(40);
		cell.setCellValue("Project cost");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(41);
		cell.setCellValue("Project Capex + Deposit");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(42);
		cell.setCellValue("Cost Per Screen");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(43);
		cell.setCellValue("Lease Tenure");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(44);
		cell.setCellValue("PVR Lock-in Period");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(45);
		cell.setCellValue("Developer Lock-in Period");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(46);
		cell.setCellValue("Gross Leasable Area");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(47);
		cell.setCellValue("Loading %");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(48);
		cell.setCellValue("Carpet Area");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(49);
		cell.setCellValue("Area per seat");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(50);
		cell.setCellValue("Explain Rent Agreement");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(51);
		cell.setCellValue("Explain CAM agreements");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(52);
		cell.setCellValue("Rent per sq ft");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(53);
		cell.setCellValue("CAM per sq ft");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(54);
		cell.setCellValue("Revenue Share - Box");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(55);
		cell.setCellValue("Revenue Share - F&B");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(56);
		cell.setCellValue("Revenue Share - Ad");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(57);
		cell.setCellValue("Revenue Share - Others");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(58);
		cell.setCellValue("Yr 2 Admits");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(59);
		cell.setCellValue("Yr 2 Occupancy");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(60);
		cell.setCellValue("Yr 2 ATP");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(61);
		cell.setCellValue("Yr 2 SPH");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(62);
		cell.setCellValue("SPH:ATP ratio");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(63);
		cell.setCellValue("Yr 2 Revenue");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(64);
		cell.setCellValue("Yr 2 Net Sale of Tickets");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(65);
		cell.setCellValue("Yr 2 Sale of F&B");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(66);
		cell.setCellValue("Yr 2 Ad revenue");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(67);
		cell.setCellValue("Yr 2 VAS Income");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(68);
		cell.setCellValue("Yr 2 Other Operating Income");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(69);
		cell.setCellValue("Yr 2 EBITDA");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(70);
		cell.setCellValue("Yr 2 EBITDA margin");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(71);
		cell.setCellValue("Film Distributor's Share");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(72);
		cell.setCellValue("Consumption of F&B");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(73);
		cell.setCellValue("Gross Margin");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(74);
		cell.setCellValue("Personnel Expenses");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(75);
		cell.setCellValue("Rent");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(76);
		cell.setCellValue("CAM");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(77);
		cell.setCellValue("Electricity & Water");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(78);
		cell.setCellValue("Repairs & Maintenance");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(79);
		cell.setCellValue("Other Operating Expenses");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(80);
		cell.setCellValue("Total Fixed Expenditure");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(81);
		cell.setCellValue("EBITDA Payback Period");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(82);
		cell.setCellValue("IRR Post tax");
		cell.setCellStyle((CellStyle) header1);
		header1 = workbook.createCellStyle();
		header1.setAlignment((short) 2);
		header1.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		header1.setFillPattern((short) 1);
		headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBold(false);
		headerFont.setItalic(false);
		header1.setFont((Font) headerFont);
		row = spreadsheet.createRow(2);
		cell = row.createCell(0);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(1);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(2);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(3);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(4);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(5);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(6);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(7);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(8);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(9);
		cell.setCellValue("sq.ft.");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(10);
		cell.setCellValue("sq.ft.");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(11);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(12);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(13);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(14);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(15);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(16);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(17);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(18);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(19);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(20);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(21);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(22);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(23);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(24);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(25);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(26);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(27);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(28);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(29);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(30);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(31);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(32);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(33);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(34);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(35);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(36);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(37);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(38);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(39);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(40);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(41);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(42);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(43);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(44);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(45);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(46);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(47);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(48);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(49);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(50);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(51);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(52);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(53);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(54);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(55);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(56);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(57);
		cell.setCellValue("%");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(58);
		cell.setCellValue("Rs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(59);
		cell.setCellValue("Rs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(60);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(61);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(62);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(63);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(64);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(65);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(66);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(67);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(68);
		cell.setCellValue("%");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(69);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(70);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(71);
		cell.setCellValue("%");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(72);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(73);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(74);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(75);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(76);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(77);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(78);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(79);
		cell.setCellValue("%");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(80);
		cell.setCellValue("%");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(81);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(82);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		row.setHeight((short) 300);
		row = spreadsheet.createRow(3);
		short rowNo = 3;
		List<MisMapper> projectList = getOperationalMisProjects(misHelper);
		if (!ObjectUtils.isEmpty(projectList) && projectList.size() > 0) {
			for (MisMapper project : projectList) {
				row = spreadsheet.createRow(rowNo);
				row.createCell(0).setCellValue(project.getReportId());
				row.createCell(1).setCellValue(project.getProjectName());
				row.createCell(2).setCellValue(project.getCreatedBy());
				row.createCell(3).setCellValue(project.getFeasibilityState());
				row.createCell(4).setCellValue(project.getDateOfSendForApproval());
				row.createCell(5).setCellValue(project.getDateOfHandover());
				row.createCell(6).setCellValue(project.getDateOfOpening());
				row.createCell(7).setCellValue(project.getProjectLocation().getCity());
				row.createCell(8).setCellValue(project.getProjectLocation().getState());
				row.createCell(9).setCellValue(project.getProjectLocation().getCityTier());
				row.createCell(10).setCellValue(project.getCityPopulation().toString());
				row.createCell(11).setCellValue(project.getTotalDeveloperSize().doubleValue());
				row.createCell(12).setCellValue(project.getRetailAreaOfProject().doubleValue());
				row.createCell(13).setCellValue(project.getProjectType());
				row.createCell(14).setCellValue(project.getTenancyMix());
				row.createCell(15).setCellValue(project.getUpcomingInfrastructureDevlopment());
				row.createCell(16).setCellValue(project.getDeveloper());
				row.createCell(17).setCellValue(project.getDeveloperOwner());
				row.createCell(18).setCellValue(project.getGrossTurnOver());
				if (!ObjectUtils.isEmpty(project.getProjectDelivered1())
						&& !ObjectUtils.isEmpty(project.getProjectDelivered2())
						&& !ObjectUtils.isEmpty(project.getProjectDelivered3())) {
					row.createCell(19).setCellValue(project.getProjectDelivered1() + ","
							+ project.getProjectDelivered2() + "," + project.getProjectDelivered3());
				} else if (!ObjectUtils.isEmpty(project.getProjectDelivered1())
						&& !ObjectUtils.isEmpty(project.getProjectDelivered2())) {
					row.createCell(19)
							.setCellValue(project.getProjectDelivered1() + "," + project.getProjectDelivered2());
				} else {
					row.createCell(19).setCellValue(project.getProjectDelivered1());
				}
				if (!ObjectUtils.isEmpty(project.getPvrProjectPipeline())
						&& project.getPvrProjectPipeline().size() > 0) {
					StringBuilder str = new StringBuilder();
					for (int i = 0; i < project.getPvrProjectPipeline().size(); i++) {
						str.append(project.getPvrProjectPipeline().get(i));
						str.append(",");
					}
					row.createCell(20).setCellValue(str.toString());
				} else {
					row.createCell(20).setCellValue("");
				}
				if (!ObjectUtils.isEmpty(project.getPvrProjectDelivered())
						&& project.getPvrProjectDelivered().size() > 0) {
					StringBuilder str = new StringBuilder();
					for (int i = 0; i < project.getPvrProjectDelivered().size(); i++) {
						str.append(project.getPvrProjectDelivered().get(i));
						str.append(",");
					}
					row.createCell(21).setCellValue(str.toString());
				} else {
					row.createCell(21).setCellValue("");
				}
				row.createCell(22).setCellValue(project.getRetailProjectDelivered());
				row.createCell(23).setCellValue(project.getCurrentProjectFinancing());
				row.createCell(24)
						.setCellValue((project.getNoOfScreens() != null) ? project.getNoOfScreens().intValue() : 0.0D);
				row.createCell(25).setCellValue(project.getMainstream().toString());
				row.createCell(26).setCellValue(project.getDirectorsCut().toString());
				row.createCell(27).setCellValue(project.getGold().toString());
				row.createCell(28).setCellValue(project.getImax().toString());
				row.createCell(29).setCellValue(project.getPxl().toString());
				row.createCell(30).setCellValue(project.getDx().toString());
				row.createCell(31).setCellValue(project.getPlayhouse().toString());
				row.createCell(32).setCellValue(project.getOnyx().toString());
				row.createCell(33).setCellValue(project.getOthers().toString());
				row.createCell(34).setCellValue(
						(project.getTotalColumnValSum() != null) ? project.getTotalColumnValSum().intValue() : 0.0D);
				row.createCell(35).setCellValue(
						(project.getSecurityDeposit() != null) ? project.getSecurityDeposit().toString() : "");
				row.createCell(36).setCellValue(project.getSecurityDepositForMonthRent().toString());
				row.createCell(37).setCellValue(project.getSecurityDepositForMonthCam().toString());
				row.createCell(38).setCellValue(project.getRentSecurityDepositForFixedZeroMg().toString());
				row.createCell(39).setCellValue(project.getCamSecurityDepositForFixedZeroMg().toString());
				row.createCell(40).setCellValue(
						(project.getProjectCost() != null) ? project.getProjectCost().doubleValue() : 0.0D);
				row.createCell(41).setCellValue(
						(project.getSecurityCapex() != null) ? project.getSecurityCapex().toString() : "");
				row.createCell(42)
						.setCellValue((project.getProjectCostPerScreen() != null)
								? project.getProjectCostPerScreen().doubleValue()
								: 0.0D);
				row.createCell(43)
						.setCellValue((project.getLeasePeriod() != null) ? project.getLeasePeriod().intValue() : 0.0D);
				row.createCell(44).setCellValue(
						(project.getPvrLockInPeriod() != null) ? project.getPvrLockInPeriod().intValue() : 0.0D);
				row.createCell(45)
						.setCellValue((project.getDevloperLockedInPeriod() != null)
								? project.getDevloperLockedInPeriod().intValue()
								: 0.0D);
				row.createCell(46).setCellValue(
						(project.getLeasableArea() != null) ? project.getLeasableArea().doubleValue() : 0.0D);
				row.createCell(47).setCellValue(
						(project.getLoadingPercentage() != null) ? project.getLoadingPercentage().doubleValue() : 0.0D);
				row.createCell(48)
						.setCellValue((project.getCarpetArea() != null) ? project.getCarpetArea().doubleValue() : 0.0D);
				row.createCell(49).setCellValue(
						(project.getCarpetAreaPerSeat() != null) ? project.getCarpetAreaPerSeat().toString() : "");
				row.createCell(50).setCellValue(project.getRentAgreement());
				row.createCell(51).setCellValue(project.getCamAgreement());
				row.createCell(52).setCellValue(project.getRentPerSqFt().toString());
				row.createCell(53).setCellValue(project.getCamPerSqFt().toString());
				row.createCell(54).setCellValue(project.getBoxOfficePercentage().toString());
				row.createCell(55).setCellValue(project.getFAndBPercentage().toString());
				row.createCell(56).setCellValue(project.getAdSalePercentage().toString());
				row.createCell(57).setCellValue(project.getOthersPercentage().toString());
				row.createCell(58).setCellValue((project.getAdmits() != null) ? project.getAdmits().toString() : "");
				row.createCell(59)
						.setCellValue((project.getOccupancy() != null) ? project.getOccupancy().toString() : "");
				row.createCell(60).setCellValue((project.getAtp() != null) ? project.getAtp().toString() : "");
				row.createCell(61).setCellValue((project.getSph() != null) ? project.getSph().toString() : "");
				row.createCell(62)
						.setCellValue((project.getAtpSphRatio() != null) ? project.getAtpSphRatio().toString() : "");
				row.createCell(63).setCellValue((project.getRevenue() != null) ? project.getRevenue().toString() : "");
				row.createCell(64).setCellValue(
						(project.getNetSaleOfTickets() != null) ? project.getNetSaleOfTickets().toString() : "");
				row.createCell(65)
						.setCellValue((project.getSalseOfFnB() != null) ? project.getSalseOfFnB().toString() : "");
				row.createCell(66)
						.setCellValue((project.getAdRevenue() != null) ? project.getAdRevenue().toString() : "");
				row.createCell(67)
						.setCellValue((project.getVasIncome() != null) ? project.getVasIncome().toString() : "");
				row.createCell(68)
						.setCellValue((project.getOtherOperatingIncome() != null)
								? project.getOtherOperatingIncome().toString()
								: "");
				row.createCell(69).setCellValue((project.getEbitda() != null) ? project.getEbitda().toString() : "");
				row.createCell(70)
						.setCellValue((project.getEbitdaMargin() != null) ? project.getEbitdaMargin().toString() : "");
				row.createCell(71)
						.setCellValue((project.getFilmDistributorShare() != null)
								? project.getFilmDistributorShare().toString()
								: "");
				row.createCell(72).setCellValue(
						(project.getFnbConsumption() != null) ? project.getFnbConsumption().toString() : "");
				row.createCell(73)
						.setCellValue((project.getGrossMargin() != null) ? project.getGrossMargin().toString() : "");
				row.createCell(74).setCellValue(
						(project.getPersonnelExpenses() != null) ? project.getPersonnelExpenses().toString() : "");
				row.createCell(75).setCellValue((project.getRent() != null) ? project.getRent().toString() : "");
				row.createCell(76).setCellValue((project.getCam() != null) ? project.getCam().toString() : "");
				row.createCell(77).setCellValue(
						(project.getElectricityWater() != null) ? project.getElectricityWater().toString() : "");
				row.createCell(78).setCellValue((project.getRm() != null) ? project.getRm().toString() : "");
				row.createCell(79)
						.setCellValue((project.getOtherOperatingExpense() != null)
								? project.getOtherOperatingExpense().toString()
								: "");
				row.createCell(80).setCellValue(
						(project.getTotalFixedExp() != null) ? project.getTotalFixedExp().toString() : "");
				row.createCell(81).setCellValue(
						(project.getEbitdaPaybackPeriod() != null) ? project.getEbitdaPaybackPeriod().toString() : "");
				row.createCell(82)
						.setCellValue((project.getPostTaxIrr() != null) ? project.getPostTaxIrr().toString() : "");
				rowNo = (short) (rowNo + 1);
			}
		} else {
			row.createCell(0).setCellValue("No data found for the dates selected");
		}
		byte[] columns = new byte[80];
		for (int j = 0; j < columns.length; j++)
			spreadsheet.autoSizeColumn(j);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			workbook.write(baos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] xls = baos.toByteArray();
		return xls;
	}

	public byte[] getExtMis(MisHelper misHelper) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet spreadsheet = workbook.createSheet();
		XSSFCellStyle header = workbook.createCellStyle();
		header.setAlignment((short) 2);
		header.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
		header.setFillPattern((short) 1);
		header.setBorderTop(BorderStyle.THICK);
		header.setTopBorderColor(IndexedColors.BLACK.getIndex());
		header.setBorderLeft(BorderStyle.THICK);
		header.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		header.setBorderRight(BorderStyle.THICK);
		header.setRightBorderColor(IndexedColors.BLACK.getIndex());
		XSSFFont headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBold(true);
		headerFont.setItalic(false);
		header.setFont((Font) headerFont);
		XSSFRow row = spreadsheet.createRow(0);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4));
		XSSFCell cell = row.createCell(0);
		cell.setCellValue("PROJECT DETAILS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 5, 13));
		cell = row.createCell(5);
		cell.setCellValue("DEVELOPMENT DETAILS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 14, 21));
		cell = row.createCell(14);
		cell.setCellValue("DEVELOPER DETAILS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 22, 32));
		cell = row.createCell(22);
		cell.setCellValue("CINEMA SPECS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 33, 40));
		cell = row.createCell(33);
		cell.setCellValue("CAPEX");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 41, 54));
		cell = row.createCell(41);
		cell.setCellValue("KEY LEASE TERMS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 55, 60));
		cell = row.createCell(55);
		cell.setCellValue("OPERATING ASSUMPTIONS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 61, 78));
		cell = row.createCell(61);
		cell.setCellValue("FINANCIALS");
		cell.setCellStyle((CellStyle) header);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 79, 80));
		cell = row.createCell(79);
		cell.setCellValue("RETURN METRICES");
		cell.setCellStyle((CellStyle) header);
		XSSFCellStyle header1 = workbook.createCellStyle();
		header1.setAlignment((short) 5);
		header1.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		header1.setFillPattern((short) 1);
		header1.setBorderTop(BorderStyle.THIN);
		header1.setTopBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderLeft(BorderStyle.THIN);
		header1.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderRight(BorderStyle.THIN);
		header1.setRightBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderBottom(BorderStyle.THICK);
		header1.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBold(false);
		headerFont.setItalic(false);
		header1.setFont((Font) headerFont);
		row = spreadsheet.createRow(1);
		row.setHeight((short) 600);
		cell = row.createCell(0);
		cell.setCellValue("Proj Code");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(1);
		cell.setCellValue("Property Name");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(2);
		cell.setCellValue("Created By");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(3);
		cell.setCellValue("Feasibility State");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(4);
		cell.setCellValue("Feasibility Date");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(5);
		cell.setCellValue("Handover Date");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(6);
		cell.setCellValue("Likely Date of Opening");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(7);
		cell.setCellValue("City");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(8);
		cell.setCellValue("State");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(9);
		cell.setCellValue("City Tier");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(10);
		cell.setCellValue("City Population");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(11);
		cell.setCellValue("Development Size");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(12);
		cell.setCellValue("Retail Area of the Project");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(13);
		cell.setCellValue("Type of Project");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(14);
		cell.setCellValue("Tenancy Mix");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(15);
		cell.setCellValue("Upcoming Infrastructure  development in catchment");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(16);
		cell.setCellValue("Developer Name");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(17);
		cell.setCellValue("Name of owners");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(18);
		cell.setCellValue("Gross turnover of company");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(19);
		cell.setCellValue("Top 3 projects delivered");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(20);
		cell.setCellValue("PVR projects in pipeline");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(21);
		cell.setCellValue("PVR projects delivered");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(22);
		cell.setCellValue("Retail Projects Delivered");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(23);
		cell.setCellValue("Current project financing");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(24);
		cell.setCellValue("No. of Screens");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(25);
		cell.setCellValue("Mainstream");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(26);
		cell.setCellValue("Directors Cut");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(27);
		cell.setCellValue("Gold");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(28);
		cell.setCellValue("IMAX");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(29);
		cell.setCellValue("PXL");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(30);
		cell.setCellValue("4DX ");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(31);
		cell.setCellValue("Playhouse");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(32);
		cell.setCellValue("Onyx");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(33);
		cell.setCellValue("Others");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(34);
		cell.setCellValue("Total Seats");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(35);
		cell.setCellValue("Security Deposit");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(36);
		cell.setCellValue("Security Deposit (# of months of rent)");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(37);
		cell.setCellValue("Security Deposit (# of months of cam)");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(38);
		cell.setCellValue("Security Deposit (Rent)");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(39);
		cell.setCellValue("Security Deposit (CAM)");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(40);
		cell.setCellValue("Project cost");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(41);
		cell.setCellValue("Project Capex + Deposit");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(42);
		cell.setCellValue("Cost Per Screen");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(43);
		cell.setCellValue("Lease Tenure");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(44);
		cell.setCellValue("PVR Lock-in Period");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(45);
		cell.setCellValue("Developer Lock-in Period");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(46);
		cell.setCellValue("Gross Leasable Area");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(47);
		cell.setCellValue("Loading %");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(48);
		cell.setCellValue("Carpet Area");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(49);
		cell.setCellValue("Area per seat");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(50);
		cell.setCellValue("Explain Rent Agreement");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(51);
		cell.setCellValue("Explain CAM agreements");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(52);
		cell.setCellValue("Rent per sq ft");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(53);
		cell.setCellValue("CAM per sq ft");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(54);
		cell.setCellValue("Revenue Share - Box");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(55);
		cell.setCellValue("Revenue Share - F&B");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(56);
		cell.setCellValue("Revenue Share - Ad");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(57);
		cell.setCellValue("Revenue Share - Others");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(58);
		cell.setCellValue("Yr 2 Admits");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(59);
		cell.setCellValue("Yr 2 Occupancy");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(60);
		cell.setCellValue("Yr 2 ATP");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(61);
		cell.setCellValue("Yr 2 SPH");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(62);
		cell.setCellValue("SPH:ATP ratio");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(63);
		cell.setCellValue("Yr 2 Revenue");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(64);
		cell.setCellValue("Yr 2 Net Sale of Tickets");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(65);
		cell.setCellValue("Yr 2 Sale of F&B");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(66);
		cell.setCellValue("Yr 2 Ad revenue");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(67);
		cell.setCellValue("Yr 2 VAS Income");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(68);
		cell.setCellValue("Yr 2 Other Operating Income");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(69);
		cell.setCellValue("Yr 2 EBITDA");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(70);
		cell.setCellValue("Yr 2 EBITDA margin");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(71);
		cell.setCellValue("Film Distributor's Share");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(72);
		cell.setCellValue("Consumption of F&B");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(73);
		cell.setCellValue("Gross Margin");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(74);
		cell.setCellValue("Personnel Expenses");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(75);
		cell.setCellValue("Rent");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(76);
		cell.setCellValue("CAM");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(77);
		cell.setCellValue("Electricity & Water");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(78);
		cell.setCellValue("Repairs & Maintenance");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(79);
		cell.setCellValue("Other Operating Expenses");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(80);
		cell.setCellValue("Total Fixed Expenditure");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(81);
		cell.setCellValue("EBITDA Payback Period");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(82);
		cell.setCellValue("IRR Post tax");
		cell.setCellStyle((CellStyle) header1);
		header1 = workbook.createCellStyle();
		header1.setAlignment((short) 2);
		header1.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		header1.setFillPattern((short) 1);
		headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBold(false);
		headerFont.setItalic(false);
		header1.setFont((Font) headerFont);
		row = spreadsheet.createRow(2);
		cell = row.createCell(0);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(1);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(2);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(3);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(4);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(5);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(6);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(7);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(8);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(9);
		cell.setCellValue("sq.ft.");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(10);
		cell.setCellValue("sq.ft.");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(11);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(12);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(13);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(14);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(15);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(16);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(17);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(18);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(19);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(20);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(21);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(22);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(23);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(24);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(25);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(26);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(27);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(28);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(29);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(30);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(31);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(32);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(33);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(34);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(35);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(36);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(37);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(38);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(39);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(40);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(41);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(42);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(43);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(44);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(45);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(46);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(47);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(48);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(49);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(50);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(51);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(52);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(53);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(54);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(55);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(56);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(57);
		cell.setCellValue("%");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(58);
		cell.setCellValue("Rs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(59);
		cell.setCellValue("Rs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(60);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(61);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(62);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(63);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(64);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(65);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(66);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(67);
		cell.setCellValue("Rslacs");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(68);
		cell.setCellValue("%");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(69);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(70);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(71);
		cell.setCellValue("%");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(72);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(73);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(74);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(75);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(76);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(77);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(78);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(79);
		cell.setCellValue("%");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(80);
		cell.setCellValue("%");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(81);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(82);
		cell.setCellValue("");
		cell.setCellStyle((CellStyle) header1);
		row.setHeight((short) 300);
		row = spreadsheet.createRow(3);
		short rowNo = 3;
		List<MisMapper> projectList = getExitMisProjects(misHelper);
		if (!ObjectUtils.isEmpty(projectList) && projectList.size() > 0) {
			for (MisMapper project : projectList) {
				row = spreadsheet.createRow(rowNo);
				row.createCell(0).setCellValue(project.getReportId());
				row.createCell(1).setCellValue(project.getProjectName());
				row.createCell(2).setCellValue(project.getCreatedBy());
				row.createCell(3).setCellValue(project.getFeasibilityState());
				row.createCell(4).setCellValue(project.getDateOfSendForApproval());
				row.createCell(5).setCellValue(project.getDateOfHandover());
				row.createCell(6).setCellValue(project.getDateOfOpening());
				row.createCell(7).setCellValue(project.getProjectLocation().getCity());
				row.createCell(8).setCellValue(project.getProjectLocation().getState());
				row.createCell(9).setCellValue(project.getProjectLocation().getCityTier());
				row.createCell(10).setCellValue(project.getCityPopulation().toString());
				row.createCell(11).setCellValue(project.getTotalDeveloperSize().doubleValue());
				row.createCell(12).setCellValue(project.getRetailAreaOfProject().doubleValue());
				row.createCell(13).setCellValue(project.getProjectType());
				row.createCell(14).setCellValue(project.getTenancyMix());
				row.createCell(15).setCellValue(project.getUpcomingInfrastructureDevlopment());
				row.createCell(16).setCellValue(project.getDeveloper());
				row.createCell(17).setCellValue(project.getDeveloperOwner());
				row.createCell(18).setCellValue(project.getGrossTurnOver());
				if (!ObjectUtils.isEmpty(project.getProjectDelivered1())
						&& !ObjectUtils.isEmpty(project.getProjectDelivered2())
						&& !ObjectUtils.isEmpty(project.getProjectDelivered3())) {
					row.createCell(19).setCellValue(project.getProjectDelivered1() + ","
							+ project.getProjectDelivered2() + "," + project.getProjectDelivered3());
				} else if (!ObjectUtils.isEmpty(project.getProjectDelivered1())
						&& !ObjectUtils.isEmpty(project.getProjectDelivered2())) {
					row.createCell(19)
							.setCellValue(project.getProjectDelivered1() + "," + project.getProjectDelivered2());
				} else {
					row.createCell(19).setCellValue(project.getProjectDelivered1());
				}
				if (!ObjectUtils.isEmpty(project.getPvrProjectPipeline())
						&& project.getPvrProjectPipeline().size() > 0) {
					StringBuilder str = new StringBuilder();
					for (int i = 0; i < project.getPvrProjectPipeline().size(); i++) {
						str.append(project.getPvrProjectPipeline().get(i));
						str.append(",");
					}
					row.createCell(20).setCellValue(str.toString());
				} else {
					row.createCell(20).setCellValue("");
				}
				if (!ObjectUtils.isEmpty(project.getPvrProjectDelivered())
						&& project.getPvrProjectDelivered().size() > 0) {
					StringBuilder str = new StringBuilder();
					for (int i = 0; i < project.getPvrProjectDelivered().size(); i++) {
						str.append(project.getPvrProjectDelivered().get(i));
						str.append(",");
					}
					row.createCell(21).setCellValue(str.toString());
				} else {
					row.createCell(21).setCellValue("");
				}
				row.createCell(22).setCellValue(project.getRetailProjectDelivered());
				row.createCell(23).setCellValue(project.getCurrentProjectFinancing());
				row.createCell(24)
						.setCellValue((project.getNoOfScreens() != null) ? project.getNoOfScreens().intValue() : 0.0D);
				row.createCell(25).setCellValue(project.getMainstream().toString());
				row.createCell(26).setCellValue(project.getDirectorsCut().toString());
				row.createCell(27).setCellValue(project.getGold().toString());
				row.createCell(28).setCellValue(project.getImax().toString());
				row.createCell(29).setCellValue(project.getPxl().toString());
				row.createCell(30).setCellValue(project.getDx().toString());
				row.createCell(31).setCellValue(project.getPlayhouse().toString());
				row.createCell(32).setCellValue(project.getOnyx().toString());
				row.createCell(33).setCellValue(project.getOthers().toString());
				row.createCell(34).setCellValue(
						(project.getTotalColumnValSum() != null) ? project.getTotalColumnValSum().intValue() : 0.0D);
				row.createCell(35).setCellValue(
						(project.getSecurityDeposit() != null) ? project.getSecurityDeposit().toString() : "");
				row.createCell(36).setCellValue(project.getSecurityDepositForMonthRent().toString());
				row.createCell(37).setCellValue(project.getSecurityDepositForMonthCam().toString());
				row.createCell(38).setCellValue(project.getRentSecurityDepositForFixedZeroMg().toString());
				row.createCell(39).setCellValue(project.getCamSecurityDepositForFixedZeroMg().toString());
				row.createCell(40).setCellValue(
						(project.getProjectCost() != null) ? project.getProjectCost().doubleValue() : 0.0D);
				row.createCell(41).setCellValue(
						(project.getSecurityCapex() != null) ? project.getSecurityCapex().toString() : "");
				row.createCell(42)
						.setCellValue((project.getProjectCostPerScreen() != null)
								? project.getProjectCostPerScreen().doubleValue()
								: 0.0D);
				row.createCell(43)
						.setCellValue((project.getLeasePeriod() != null) ? project.getLeasePeriod().intValue() : 0.0D);
				row.createCell(44).setCellValue(
						(project.getPvrLockInPeriod() != null) ? project.getPvrLockInPeriod().intValue() : 0.0D);
				row.createCell(45)
						.setCellValue((project.getDevloperLockedInPeriod() != null)
								? project.getDevloperLockedInPeriod().intValue()
								: 0.0D);
				row.createCell(46).setCellValue(
						(project.getLeasableArea() != null) ? project.getLeasableArea().doubleValue() : 0.0D);
				row.createCell(47).setCellValue(
						(project.getLoadingPercentage() != null) ? project.getLoadingPercentage().doubleValue() : 0.0D);
				row.createCell(48)
						.setCellValue((project.getCarpetArea() != null) ? project.getCarpetArea().doubleValue() : 0.0D);
				row.createCell(49).setCellValue(
						(project.getCarpetAreaPerSeat() != null) ? project.getCarpetAreaPerSeat().toString() : "");
				row.createCell(50).setCellValue(project.getRentAgreement());
				row.createCell(51).setCellValue(project.getCamAgreement());
				row.createCell(52).setCellValue(project.getRentPerSqFt().toString());
				row.createCell(53).setCellValue(project.getCamPerSqFt().toString());
				row.createCell(54).setCellValue(project.getBoxOfficePercentage().toString());
				row.createCell(55).setCellValue(project.getFAndBPercentage().toString());
				row.createCell(56).setCellValue(project.getAdSalePercentage().toString());
				row.createCell(57).setCellValue(project.getOthersPercentage().toString());
				row.createCell(58).setCellValue((project.getAdmits() != null) ? project.getAdmits().toString() : "");
				row.createCell(59)
						.setCellValue((project.getOccupancy() != null) ? project.getOccupancy().toString() : "");
				row.createCell(60).setCellValue((project.getAtp() != null) ? project.getAtp().toString() : "");
				row.createCell(61).setCellValue((project.getSph() != null) ? project.getSph().toString() : "");
				row.createCell(62)
						.setCellValue((project.getAtpSphRatio() != null) ? project.getAtpSphRatio().toString() : "");
				row.createCell(63).setCellValue((project.getRevenue() != null) ? project.getRevenue().toString() : "");
				row.createCell(64).setCellValue(
						(project.getNetSaleOfTickets() != null) ? project.getNetSaleOfTickets().toString() : "");
				row.createCell(65)
						.setCellValue((project.getSalseOfFnB() != null) ? project.getSalseOfFnB().toString() : "");
				row.createCell(66)
						.setCellValue((project.getAdRevenue() != null) ? project.getAdRevenue().toString() : "");
				row.createCell(67)
						.setCellValue((project.getVasIncome() != null) ? project.getVasIncome().toString() : "");
				row.createCell(68)
						.setCellValue((project.getOtherOperatingIncome() != null)
								? project.getOtherOperatingIncome().toString()
								: "");
				row.createCell(69).setCellValue((project.getEbitda() != null) ? project.getEbitda().toString() : "");
				row.createCell(70)
						.setCellValue((project.getEbitdaMargin() != null) ? project.getEbitdaMargin().toString() : "");
				row.createCell(71)
						.setCellValue((project.getFilmDistributorShare() != null)
								? project.getFilmDistributorShare().toString()
								: "");
				row.createCell(72).setCellValue(
						(project.getFnbConsumption() != null) ? project.getFnbConsumption().toString() : "");
				row.createCell(73)
						.setCellValue((project.getGrossMargin() != null) ? project.getGrossMargin().toString() : "");
				row.createCell(74).setCellValue(
						(project.getPersonnelExpenses() != null) ? project.getPersonnelExpenses().toString() : "");
				row.createCell(75).setCellValue((project.getRent() != null) ? project.getRent().toString() : "");
				row.createCell(76).setCellValue((project.getCam() != null) ? project.getCam().toString() : "");
				row.createCell(77).setCellValue(
						(project.getElectricityWater() != null) ? project.getElectricityWater().toString() : "");
				row.createCell(78).setCellValue((project.getRm() != null) ? project.getRm().toString() : "");
				row.createCell(79)
						.setCellValue((project.getOtherOperatingExpense() != null)
								? project.getOtherOperatingExpense().toString()
								: "");
				row.createCell(80).setCellValue(
						(project.getTotalFixedExp() != null) ? project.getTotalFixedExp().toString() : "");
				row.createCell(81).setCellValue(
						(project.getEbitdaPaybackPeriod() != null) ? project.getEbitdaPaybackPeriod().toString() : "");
				row.createCell(82)
						.setCellValue((project.getPostTaxIrr() != null) ? project.getPostTaxIrr().toString() : "");
				rowNo = (short) (rowNo + 1);
			}
		} else {
			row.createCell(0).setCellValue("No data found for the dates selected");
		}
		byte[] columns = new byte[80];
		for (int j = 0; j < columns.length; j++)
			spreadsheet.autoSizeColumn(j);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			workbook.write(baos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] xls = baos.toByteArray();
		return xls;
	}

	@Override
	public byte[] irrAsXcel(String projectId) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet spreadsheet = workbook.createSheet();
		XSSFCellStyle header = workbook.createCellStyle();
		header.setAlignment((short) 2);
		header.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
		header.setFillPattern((short) 1);
		header.setBorderTop(BorderStyle.THICK);
		header.setTopBorderColor(IndexedColors.BLACK.getIndex());
		header.setBorderLeft(BorderStyle.THICK);
		header.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		header.setBorderRight(BorderStyle.THICK);
		header.setRightBorderColor(IndexedColors.BLACK.getIndex());
		XSSFFont headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBold(true);
		headerFont.setItalic(false);
		header.setFont((Font) headerFont);
		XSSFRow row = spreadsheet.createRow(0);
		XSSFCellStyle header1 = workbook.createCellStyle();
		header1.setAlignment((short) 5);
		header1.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		header1.setFillPattern((short) 1);
		header1.setBorderTop(BorderStyle.THIN);
		header1.setTopBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderLeft(BorderStyle.THIN);
		header1.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderRight(BorderStyle.THIN);
		header1.setRightBorderColor(IndexedColors.BLACK.getIndex());
		header1.setBorderBottom(BorderStyle.THICK);
		header1.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBold(false);
		headerFont.setItalic(false);
		header1.setFont((Font) headerFont);
		IrrCalculationData irrData = this.projectDetailService.getProjectIrrCalculationsById(projectId);
		ProjectDetails projData = this.projectDetailService.getProjectById(projectId);
		List<FixedConstant> fixedConstant = this.projectDetailService.getFixedConstants();
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));
		XSSFCell cell = row.createCell(0);
		cell.setCellValue("FINANCIALS");
		cell.setCellStyle((CellStyle) header);
		row = spreadsheet.createRow(1);
		row.setHeight((short) 600);
		cell = row.createCell(0);
		cell.setCellValue("Particulars");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(1);
		cell.setCellValue("Units");
		cell.setCellStyle((CellStyle) header1);
		int size = irrData.getNetSaleOfTicket().size();
		for (int i = 0; i <= size; i++) {
			cell = row.createCell(i + 2);
			cell.setCellValue("Year " + i);
			cell.setCellStyle((CellStyle) header1);
		}
		row = spreadsheet.createRow(2);
		cell = row.createCell(0);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));
		cell.setCellValue("Profit And Loss Account");
		cell.setCellStyle((CellStyle) header1);
		int rowNum = 2;
		Map<Integer, Double> hm = null;
		XSSFRow tempRow = null;
		for (int n = 1; n <= 19; n++) {
			tempRow = spreadsheet.createRow(rowNum + n);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("INR LACS");
			if (n == 1) {
				hm = irrData.getNetTicketSales();
				cell = tempRow.createCell(0);
				cell.setCellValue("Net Sale of Tickets ");
			} else if (n == 2) {
				cell = tempRow.createCell(0);
				cell.setCellValue("Sale of F&B");
				hm = irrData.getNetFnBSalesForEachYear();
			} else if (n == 3) {
				hm = irrData.getSponsorshipRevenueForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Sponsorship Revenues");
			} else if (n == 4) {
				hm = irrData.getVasIncomeForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("VAS Income");
			} else if (n == 5) {
				hm = irrData.getOtherOperatingIncome();
				cell = tempRow.createCell(0);
				cell.setCellValue("Other Operating Income");
			} else if (n == 6) {
				hm = irrData.getTotalRevenueForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Total Revenues");
			} else if (n == 7) {
				hm = irrData.getFilmDistributorShare();
				cell = tempRow.createCell(0);
				cell.setCellValue("Film Distributor's Share");
			} else if (n == 8) {
				hm = irrData.getConsumptionOfFnBForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Consumption of F&B");
			} else if (n == 9) {
				hm = irrData.getGrossMarginForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Gross Margin");
			} else if (n == 10) {
				hm = irrData.getGrossMarginPercent();
				cell = tempRow.createCell(0);
				cell.setCellValue("Gross Margin %");
				cell = tempRow.createCell(1);
				cell.setCellValue("%");
			} else if (n == 11) {
				hm = irrData.getPersonnelExpenseForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Personnel Expenses");
			} else if (n == 12) {
				hm = irrData.getRentEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Rent");
			} else if (n == 13) {
				hm = irrData.getElectricityAndWaterExpenseForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Electricity and water ");
			} else if (n == 14) {
				hm = irrData.getRepairAndMaintenanceExpenseExpenseForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Repairs and maintenance");
			} else if (n == 15) {
				hm = irrData.getCamEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Common Area Maintenance");
			} else if (n == 16) {
				hm = irrData.getOtherOperatingExpensesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Other Operating Expenses");
			} else if (n == 17) {
				hm = irrData.getExpenditureExRentAndCamForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Total Fixed Expenditure");
			} else if (n == 18) {
				hm = irrData.getGetEBITDAForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("EBITDA");
			} else if (n == 19) {
				hm = irrData.getGetEBITDAMarginForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("EBITDA MARGIN %");
				cell = tempRow.createCell(1);
				cell.setCellValue("%");
			}
			for (int i2 = 3, i3 = 1; i3 <= size; i2++, i3++)
				tempRow.createCell((short) i2).setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
		}
		row = spreadsheet.createRow(22);
		cell = row.createCell(0);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));
		cell.setCellValue("Project Returns Summary");
		cell.setCellStyle((CellStyle) header1);
		row = spreadsheet.createRow(23);
		cell = row.createCell(0);
		cell.setCellValue("Project Capex + Deposits");
		cell = row.createCell(1);
		cell.setCellValue("INR Lacs");
		cell = row.createCell(3);
		cell.setCellValue(projData.getProjectCost().doubleValue() + (!ObjectUtils.isEmpty(projData.getLesserLesse().getRentSecurityDepositForFixedZeroMg()) ?
				projData.getLesserLesse().getRentSecurityDepositForFixedZeroMg() : 0.0 ));
		row = spreadsheet.createRow(24);
		cell = row.createCell(0);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));
		cell.setCellValue("Payback Period");
		cell.setCellStyle((CellStyle) header1);
		row = spreadsheet.createRow(25);
		cell = row.createCell(0);
		cell.setCellValue("EBITDA");
		cell = row.createCell(1);
		cell.setCellValue("Yrs");
		cell = row.createCell(3);
		cell.setCellValue(irrData.getGetEBITDAPayback().doubleValue());
		row = spreadsheet.createRow(26);
		cell = row.createCell(0);
		cell.setCellValue("Free Cash Flow");
		cell = row.createCell(1);
		cell.setCellValue("Yrs");
		cell = row.createCell(3);
		cell.setCellValue(irrData.getGetFCFPayback().doubleValue());
		row = spreadsheet.createRow(28);
		cell = row.createCell(0);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));
		cell.setCellValue("Return Metrices");
		cell.setCellStyle((CellStyle) header1);
		row = spreadsheet.createRow(29);
		cell = row.createCell(0);
		cell.setCellValue("Project IRR- Pre Tax");
		cell = row.createCell(1);
		cell.setCellValue("%");
		cell = row.createCell(3);
		cell.setCellValue(irrData.getProjectIRRPreTax().doubleValue());
		row = spreadsheet.createRow(30);
		cell = row.createCell(0);
		cell.setCellValue("Project IRR- Post Tax");
		cell = row.createCell(1);
		cell.setCellValue("%");
		cell = row.createCell(3);
		cell.setCellValue(irrData.getProjectIRRPostTax().doubleValue());
		row = spreadsheet.createRow(31);
		cell = row.createCell(0);
		cell.setCellValue("Equity IRR");
		cell = row.createCell(1);
		cell.setCellValue("%");
		cell = row.createCell(3);
		cell.setCellValue(irrData.getEquityIRR().doubleValue());
		row = spreadsheet.createRow(32);
		cell = row.createCell(0);
		cell.setCellValue("ROCE (EBITDA Yr 2/ Cap Emp)");
		cell = row.createCell(1);
		cell.setCellValue("%");
		cell = row.createCell(3);
		cell.setCellValue(irrData.getGetROCE().doubleValue());
		row = spreadsheet.createRow(33);
		cell = row.createCell(0);
		cell.setCellValue("Net Present Value (NPV)");
		cell = row.createCell(1);
		cell.setCellValue("INR Lacs");
		cell = row.createCell(3);
		cell.setCellValue(irrData.getNpv().doubleValue());
		row = spreadsheet.createRow(35);
		cell = row.createCell(0);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));
		cell.setCellValue("Common Size");
		cell.setCellStyle((CellStyle) header1);
		row = spreadsheet.createRow(36);
		cell = row.createCell(0);
		cell.setCellValue("Particulars");
		cell.setCellStyle((CellStyle) header1);
		cell = row.createCell(1);
		cell.setCellValue("Units");
		cell.setCellStyle((CellStyle) header1);
		for (int m = 0; m <= size; m++) {
			cell = row.createCell(m + 2);
			cell.setCellValue("Year " + m);
			cell.setCellStyle((CellStyle) header1);
		}
		rowNum = 36;
		for (int k = 1; k <= 16; k++) {
			tempRow = spreadsheet.createRow(rowNum + k);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("%");
			if (k == 1) {
				hm = irrData.getNetSaleOfTicket();
				cell = tempRow.createCell(0);
				cell.setCellValue("Net Sale of Tickets ");
			} else if (k == 2) {
				cell = tempRow.createCell(0);
				cell.setCellValue("Sale of F&B");
				hm = irrData.getSaleOfFandB();
			} else if (k == 3) {
				hm = irrData.getSponsorshipRevenue();
				cell = tempRow.createCell(0);
				cell.setCellValue("Sponsorship Revenues");
			} else if (k == 4) {
				hm = irrData.getVasIncomeNextYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("VAS Income");
			} else if (k == 5) {
				hm = irrData.getOtherOperatingRevenue();
				cell = tempRow.createCell(0);
				cell.setCellValue("Other Operating Income");
			} else if (k == 6) {
				hm = irrData.getTotalRevenue();
				cell = tempRow.createCell(0);
				cell.setCellValue("Total Revenues");
			} else if (k == 7) {
				hm = irrData.getFilmDistributorShares();
				cell = tempRow.createCell(0);
				cell.setCellValue("Film Distributor's Share");
			} else if (k == 8) {
				hm = irrData.getConsumptionFandB();
				cell = tempRow.createCell(0);
				cell.setCellValue("Consumption of F&B");
			} else if (k == 9) {
				hm = irrData.getPersonnelExpenses();
				cell = tempRow.createCell(0);
				cell.setCellValue("Personnel Expenses");
			} else if (k == 10) {
				hm = irrData.getRentDetails();
				cell = tempRow.createCell(0);
				cell.setCellValue("Rent");
			} else if (k == 11) {
				hm = irrData.getElectricityAndWaterCharges();
				cell = tempRow.createCell(0);
				cell.setCellValue("Electricity and water");
			} else if (k == 12) {
				hm = irrData.getRepairAndMaintaince();
				cell = tempRow.createCell(0);
				cell.setCellValue("Repairs and maintenance");
			} else if (k == 13) {
				hm = irrData.getCamDetails();
				cell = tempRow.createCell(0);
				cell.setCellValue("Common Area Maintenance");
			} else if (k == 14) {
				hm = irrData.getOtherOperatingExpense();
				cell = tempRow.createCell(0);
				cell.setCellValue("Other Operating Expenses");
			} else if (k == 15) {
				hm = irrData.getExpenditureExRentAndCamp();
				cell = tempRow.createCell(0);
				cell.setCellValue("Total Fixed Expenditure");
			} else if (k == 16) {
				hm = irrData.getEbitdaDetails();
				cell = tempRow.createCell(0);
				cell.setCellValue("EBITDA");
			}
			for (int i2 = 3, i3 = 1; i3 <= size; i2++, i3++)
				tempRow.createCell((short) i2).setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
		}
		row = spreadsheet.createRow(54);
		cell = row.createCell(0);
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));
		cell.setCellValue("Project Payback & IRR");
		cell.setCellStyle((CellStyle) header1);
		rowNum = 55;
		Double value = Double.valueOf(0.0D);
		int i1;
		for (i1 = 1; i1 <= 5; i1++) {
			tempRow = spreadsheet.createRow(rowNum + i1);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("INR Lacs");
			if (i1 == 1) {
				hm = irrData.getGetEBITDAForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("EBITDA");
			} else if (i1 == 2) {
				hm = irrData.getCumulativeEBITDAForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Cumulative EBITDA");
			} else if (i1 == 3) {
				cell = tempRow.createCell(0);
				cell.setCellValue("EBITDA Payback");
				cell = tempRow.createCell(1);
				cell.setCellValue("Yrs");
				hm = irrData.getCumulativeEBITDAForEachYear();
			} else if (i1 == 4) {
				hm = irrData.getTaxForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Tax");
			} else if (i1 == 5) {
				hm = irrData.getCashInflowAfterTaxForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Cash Inflow After Tax");
				cell.setCellStyle((CellStyle) header1);
			}
			double firstVal = 0.0D;
			for (int i2 = 3, i3 = 1; i3 <= size; i2++, i3++) {
				value = Double.valueOf(Math.round(((Double) hm.get(Integer.valueOf(i3))).doubleValue() / 100000.0D));
				if (i1 == 3 && i3 == 1) {
					if (((Double) hm.get(Integer.valueOf(i3))).doubleValue() < projData.getProjectCost()
							.doubleValue()) {
						value = Double.valueOf(1.0D);
					} else {
						DecimalFormat twoDecimals = new DecimalFormat("#.##");
						value = Double.valueOf(twoDecimals.format(projData.getProjectCost().doubleValue()
								/ ((Double) hm.get(Integer.valueOf(i3))).doubleValue()));
					}
				} else if (i1 == 3) {
					if (projData.getProjectCost().doubleValue() > ((Double) hm.get(Integer.valueOf(i3)))
							.doubleValue()) {
						value = Double.valueOf(1.0D);
					} else {
						DecimalFormat twoDecimals = new DecimalFormat("#.##");
						firstVal = (projData.getProjectCost().doubleValue()
								- ((Double) hm.get(Integer.valueOf(i3 - 1))).doubleValue())
								/ ((Double) irrData.getGetEBITDAForEachYear().get(Integer.valueOf(i3))).doubleValue();
						value = Double
								.valueOf(twoDecimals.format(firstVal * ((value.doubleValue() < 1.0D) ? 0 : value.doubleValue())));
					}
				}
				tempRow.createCell((short) i2).setCellValue(value.doubleValue());
			}
		}
		row = spreadsheet.createRow(61);
		cell = row.createCell(0);
		cell.setCellValue("Outflows");
		cell.setCellStyle((CellStyle) header1);
		rowNum = 61;
		for (i1 = 1; i1 <= 4; i1++) {
			tempRow = spreadsheet.createRow(rowNum + i1);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("INR Lacs");
			if (i1 == 1) {
				hm = irrData.getCapexForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Capex");
			} else if (i1 == 2) {
				hm = irrData.getSecurityDepositForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Security Deposit");
			} else if (i1 == 3) {
				hm = irrData.getDisposalOfAssets();
				cell = tempRow.createCell(0);
				cell.setCellValue("Disposal of assets");
			} else if (i1 == 4) {
				hm = irrData.getTotalOutflowsForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Total outflows");
			}
			for (int i2 = 2, i3 = 0; i3 <= size; i2++, i3++)
				tempRow.createCell((short) i2).setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
		}
		row = spreadsheet.createRow(67);
		cell = row.createCell(0);
		cell.setCellValue("Free Cash Flows");
		cell.setCellStyle((CellStyle) header1);
		rowNum = 67;
		for (i1 = 1; i1 <= 3; i1++) {
			tempRow = spreadsheet.createRow(rowNum + i1);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("INR Lacs");
			if (i1 == 1) {
				hm = irrData.getFreeCashFlowPreTaxForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Free Cash Flows pre tax");
			} else if (i1 == 2) {
				hm = irrData.getFreeCashFlowPostTaxForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Free Cash Flows post tax");
			} else if (i1 == 3) {
				hm = irrData.getCumulativeFCFPostTaxForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Cumulative FCF post tax");
			}
			for (int i2 = 2, i3 = 0; i3 <= size; i2++, i3++)
				tempRow.createCell((short) i2).setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
		}
		row = spreadsheet.createRow(67);
		cell = row.createCell(0);
		cell.setCellValue("Free Cash Flows");
		cell.setCellStyle((CellStyle) header1);
		rowNum = 70;
		for (i1 = 1; i1 <= 5; i1++) {
			tempRow = spreadsheet.createRow(rowNum + i1);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("INR Lacs");
			if (i1 == 1) {
				hm = irrData.getGetPATForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("PAT");
			} else if (i1 == 2) {
				hm = irrData.getDepreciationForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Add: Depreciation");
			} else if (i1 == 3) {
				hm = irrData.getRepaymentForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Less: Debt repayment");
			} else if (i1 == 4) {
				hm = irrData.getLessCapexForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Less: Capex");
			} else if (i1 == 5) {
				hm = irrData.getEquityCashFlowsForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Equity cash flow");
			}
			for (int i2 = 2, i3 = 0; i3 <= size; i2++, i3++)
				tempRow.createCell((short) i2).setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
		}
		row = spreadsheet.createRow(76);
		cell = row.createCell(0);
		cell.setCellValue("P&L workings");
		cell.setCellStyle((CellStyle) header1);
		rowNum = 76;
		for (i1 = 1; i1 <= 26; i1++) {
			tempRow = spreadsheet.createRow(rowNum + i1);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("INR Lacs");
			if (i1 == 1) {
				hm = irrData.getAdmitsYearWise();
				cell = tempRow.createCell(0);
				cell.setCellValue("Admits");
				cell = tempRow.createCell(1);
				cell.setCellValue("#");
			} else if (i1 == 2) {
				hm = irrData.getOccupancyForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Occupancy %");
				cell = tempRow.createCell(1);
				cell.setCellValue("%");
			} else if (i1 == 3) {
				hm = irrData.getAtpForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("ATP");
			} else if (i1 == 4) {
				hm = irrData.getGrossTicketSalesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Gross ticket Sales");
			} else if (i1 == 5) {
				cell = tempRow.createCell(0);
				cell.setCellValue("GST Rate");
			} else if (i1 == 6) {
				hm = irrData.getGstForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("GST");
			} else if (i1 == 7) {
				hm = irrData.getShowtaxForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Show Tax");
			} else if (i1 == 8) {
				hm = irrData.getHealthCess();
				cell = tempRow.createCell(0);
				cell.setCellValue("Health Cess");
			} else if (i1 == 9) {
				hm = irrData.getServiceChargePerYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Service charge");
			} else if (i1 == 10) {
				hm = irrData.getLbtExpenseForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("LBT Expense");
			} else if (i1 == 11) {
				hm = irrData.getNetTicketSalesForFhcForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Net ticket sales (for FHC)");
			} else if (i1 == 12) {
				hm = irrData.getNet3dSales();
				cell = tempRow.createCell(0);
				cell.setCellValue("3D income (net)");
			} else if (i1 == 13) {
				hm = irrData.getNetComplimentaryTickets();
				cell = tempRow.createCell(0);
				cell.setCellValue("Complimentary tickets (net)");
			} else if (i1 == 14) {
				hm = irrData.getNetTicketSales();
				cell = tempRow.createCell(0);
				cell.setCellValue("Net ticket sales");
			} else if (i1 == 15) {
				hm = irrData.getSphForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("SPH");
			} else if (i1 == 16) {
				hm = irrData.getSphForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("SPH as % of ATP");
				cell = tempRow.createCell(1);
				cell.setCellValue("%");
			} else if (i1 == 17) {
				hm = irrData.getGrossFnBForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Gross F&B");
			} else if (i1 == 18) {
				cell = tempRow.createCell(0);
				cell.setCellValue("GST %");
				cell = tempRow.createCell(1);
				cell.setCellValue("%");
			} else if (i1 == 19) {
				hm = irrData.getGstFoodForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("GST");
			} else if (i1 == 20) {
				hm = irrData.getNetFnBRevenueForCogs();
				cell = tempRow.createCell(0);
				cell.setCellValue("Net F&B revenue (for COGS)");
			} else if (i1 == 21) {
				hm = irrData.getNetComplimentaryFnBForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Complimentary F&B (net)");
			} else if (i1 == 22) {
				hm = irrData.getNetFnBSalesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Net F&B sales");
			} else if (i1 == 23) {
				hm = irrData.getAdRevenueForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Advertisement Income");
			} else if (i1 == 24) {
				hm = irrData.getConveniencefeeForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Convenience Fee Income");
			} else if (i1 == 25) {
				hm = irrData.getVpfIncomeYearWise();
				cell = tempRow.createCell(0);
				cell.setCellValue("VPF Income");
			} else if (i1 == 26) {
				hm = irrData.getMiscellaneousIncomeForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Misc Income");
			}
			for (int i2 = 3, i3 = 1; i3 <= size; i2++, i3++) {
				if (i1 == 18) {
					tempRow.createCell((short) i2).setCellValue(5.0D);
				} else if (i1 == 16) {
					tempRow.createCell((short) i2)
							.setCellValue(((Double) irrData.getSphForEachYear().get(Integer.valueOf(i3))).doubleValue()
									/ ((Double) irrData.getAtpForEachYear().get(Integer.valueOf(i3))).doubleValue()
									* 100.0D);
				} else if (i1 == 5) {
					tempRow.createCell((short) i2).setCellValue(
							((FixedConstant) fixedConstant.get(0)).getPersonnel().getGstApplicable().doubleValue());
				} else {
					tempRow.createCell((short) i2).setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
				}
			}
		}
		row = spreadsheet.createRow(104);
		cell = row.createCell(0);
		cell.setCellValue("Other Expenses");
		cell.setCellStyle((CellStyle) header1);
		rowNum = 104;
		for (i1 = 1; i1 <= 13; i1++) {
			tempRow = spreadsheet.createRow(rowNum + i1);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("INR Lacs");
			if (i1 == 1) {
				hm = irrData.getHousekeepingChargesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("House Keeping Charges");
			} else if (i1 == 2) {
				hm = irrData.getSecurityChargesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Security Charges");
			} else if (i1 == 3) {
				hm = irrData.getMarketingExpenseForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Marketing Expenses");
			} else if (i1 == 4) {
				hm = irrData.getCommunicationChargesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Communication Costs");
			} else if (i1 == 5) {
				hm = irrData.getInternetChargesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Internet Charges");
			} else if (i1 == 6) {
				hm = irrData.getLegalChargesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Legal & Professional Fee");
			} else if (i1 == 7) {
				hm = irrData.getTravellingChargesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Travelling & Conveyance");
			} else if (i1 == 8) {
				hm = irrData.getInsuranceChargesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Insurance Expenses");
			} else if (i1 == 9) {
				hm = irrData.getMiscellaneousCharges();
				cell = tempRow.createCell(0);
				cell.setCellValue("Miscellaneous Expenses");
			} else if (i1 == 10) {
				hm = irrData.getPrintingAndStationaryChargesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Printing & Stationery exp");
			} else if (i1 == 11) {
				hm = irrData.getRateAndTaxes();
				cell = tempRow.createCell(0);
				cell.setCellValue("Rates & Taxes");
			} else if (i1 == 12) {
				hm = irrData.getMunicipalTaxForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Municipal/ Property Tax ");
			} else if (i1 == 13) {
				hm = irrData.getTotalOverheadExpensesForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Total Other Expenses");
			}
			for (int i2 = 3, i3 = 1; i3 <= size; i2++, i3++)
				tempRow.createCell((short) i2).setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
		}
		row = spreadsheet.createRow(120);
		cell = row.createCell(0);
		cell.setCellValue("Depreciation workings");
		cell.setCellStyle((CellStyle) header1);
		rowNum = 120;
		for (i1 = 1; i1 <= 5; i1++) {
			tempRow = spreadsheet.createRow(rowNum + i1);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("INR Lacs");
			if (i1 == 1) {
				hm = irrData.getOpeningGrossBlock();
				cell = tempRow.createCell(0);
				cell.setCellValue("Opening Gross Block");
			} else if (i1 == 2) {
				hm = irrData.getAdditions();
				cell = tempRow.createCell(0);
				cell.setCellValue("Additions");
			} else if (i1 == 3) {
				hm = irrData.getClosingGrossBlock();
				cell = tempRow.createCell(0);
				cell.setCellValue("Closing Gross Block");
			} else if (i1 == 4) {
				hm = irrData.getDepreciationForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Depreciation ");
			} else if (i1 == 5) {
				hm = irrData.getNetGrossForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Net Block");
			}
			int l = 0;
			for (int i2 = 3, i3 = 0; i3 < hm.size(); i2++, i3++) {
				if (i1 == 3 || i1 == 2) {
					tempRow.createCell((short) i2 - 1)
							.setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
				} else if (i3 > 0) {
					tempRow.createCell((short) i2 - 1)
							.setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
				}
			}
		}
		row = spreadsheet.createRow(127);
		cell = row.createCell(0);
		cell.setCellValue("Debt Schedule");
		cell.setCellStyle((CellStyle) header1);
		rowNum = 127;
		for (i1 = 1; i1 <= 5; i1++) {
			tempRow = spreadsheet.createRow(rowNum + i1);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("INR Lacs");
			if (i1 == 1) {
				hm = irrData.getOpeningDebt();
				cell = tempRow.createCell(0);
				cell.setCellValue("Opening Debt");
			} else if (i1 == 2) {
				hm = irrData.getDrawdownForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Drawdown");
			} else if (i1 == 3) {
				hm = irrData.getRepaymentForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Repayment");
			} else if (i1 == 4) {
				hm = irrData.getClosingDebtForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Closing Debt");
			} else if (i1 == 5) {
				hm = irrData.getInterestUnderDebtScheduleForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Interest");
			}
			for (int i2 = 3, i3 = 0; i3 < hm.size(); i2++, i3++) {
				if (i1 == 4 || i1 == 5) {
					tempRow.createCell((short) i2 - 1)
							.setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
				} else if (i1 == 2) {
					tempRow.createCell((short) i2 - 1)
							.setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
				} else if (i3 > 0) {
					tempRow.createCell((short) i2 - 1)
							.setCellValue(((Double) hm.get(Integer.valueOf(i3))).doubleValue());
				}
			}
		}
		row = spreadsheet.createRow(132);
		cell = row.createCell(0);
		cell.setCellValue("WACC Calculations");
		cell.setCellStyle((CellStyle) header1);
		row = spreadsheet.createRow(133);
		cell = row.createCell(2);
		cell.setCellValue("Pre Tax");
		cell = row.createCell(3);
		cell.setCellValue("Post Tax");
		rowNum = 133;
		for (i1 = 1; i1 <= 5; i1++) {
			tempRow = spreadsheet.createRow(rowNum + i1);
			tempRow.setHeight((short) 600);
			cell = tempRow.createCell(1);
			cell.setCellValue("%");
			if (i1 == 1) {
				cell = tempRow.createCell(0);
				cell.setCellValue("Cost Of Equity");
				cell = tempRow.createCell(2);
				tempRow.createCell(2)
						.setCellValue(((FixedConstant) fixedConstant.get(0)).getCostOfEquity().doubleValue());
				cell = tempRow.createCell(3);
				tempRow.createCell(3)
						.setCellValue(((FixedConstant) fixedConstant.get(0)).getCostOfEquity().doubleValue());
			} else if (i1 == 2) {
				cell = tempRow.createCell(0);
				cell.setCellValue("Cost Of Debt");
				cell = tempRow.createCell(2);
				tempRow.createCell(2)
						.setCellValue(((FixedConstant) fixedConstant.get(0)).getCostOfDebt().doubleValue());
				cell = tempRow.createCell(3);
				tempRow.createCell(3).setCellValue(Math.round(((FixedConstant) fixedConstant.get(0)).getCostOfDebt()
						.doubleValue()
						* (1.0D - ((FixedConstant) fixedConstant.get(0)).getIncomeTax().doubleValue() / 100.0D)));
			} else if (i1 == 3) {
				cell = tempRow.createCell(0);
				cell.setCellValue("Debt Proportion");
				cell = tempRow.createCell(3);
				tempRow.createCell(3)
						.setCellValue(((FixedConstant) fixedConstant.get(0)).getDebtProportion().doubleValue());
			} else if (i1 == 4) {
				hm = irrData.getDepreciationForEachYear();
				cell = tempRow.createCell(0);
				cell.setCellValue("Equity Proportion");
				cell = tempRow.createCell(3);
				tempRow.createCell(3)
						.setCellValue(((FixedConstant) fixedConstant.get(0)).getEquityProportion().doubleValue());
			} else if (i1 == 5) {
				cell = tempRow.createCell(0);
				cell.setCellValue("WACC");
				cell = tempRow.createCell(3);
				tempRow.createCell(3).setCellValue(irrData.getWacc().doubleValue());
			}
		}
		byte[] columns = new byte[80];
		for (int j = 0; j < columns.length; j++)
			spreadsheet.autoSizeColumn(j);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			workbook.write(baos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] xls = baos.toByteArray();
		return xls;
	}


}
