package com.services.mis;

import com.helper.MisHelper;

public interface MisService {

	byte[] getMis(MisHelper misHelper);

	byte[] getOpsMis(MisHelper paramMisHelper);

	byte[] getExtMis(MisHelper paramMisHelper);

	byte[] irrAsXcel(String projectId);

}
