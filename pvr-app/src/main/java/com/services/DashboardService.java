package com.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.common.constants.Constants;
import com.common.constants.MessageConstants;
import com.common.models.BDData;
import com.common.models.Notification;
import com.common.models.ProjectDetails;
import com.common.models.User;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.MarkConstant;
import com.util.RestServiceUtil;
import com.util.URLConstants;
import com.util.UserRoleUtil;
import com.web.FeasibilityWebException;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DashboardService {

	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@Autowired
	private RestServiceUtil restServiceUtil;

	@Autowired
	private ProjectDetailService projectDetailService;

	@Autowired
	private UserService userService;

	public Map<String, Object> getDashoradDetails() {

		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_DASHBOARD_DETAILS, null, null,
					HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, List<ProjectDetails>>>() {
					});
				}
				return new HashMap<String, Object>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for dashboard on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching dashboard details", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching dashboard details");
			throw new FeasibilityWebException("Error while fetching dashboard details");
		}
	}

	public List<Notification> getNotifications(String type) {

		int status;
		try {
			String finalUrl = String.format(URLConstants.GET_NOTIFICATIONS, type);
			JsonNode response = restServiceUtil.makeRequest(finalUrl, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<Notification>>() {
					});
				}
				return new ArrayList<Notification>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Notification on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Notification list", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Notification list");
			throw new FeasibilityWebException("Error while fetching Notification list");
		}
	}

	public ProjectDetails deletebyProjectById(String projectId)
			throws JsonParseException, JsonMappingException, IOException {

		if (ObjectUtils.isEmpty(projectId)) {
			throw new FeasibilityWebException("projectId  info cannot be empty");
		}
		int status;
		try {
			String finalUrl = String.format(URLConstants.DELETE_PROJECT_DETAILS_BY_ID, projectId);
			JsonNode response = restServiceUtil.makeRequest(finalUrl, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
				String data = response.get(MarkConstant.DATA).toString();
				return OBJECT_MAPPER.readValue(data, new TypeReference<ProjectDetails>() {
				});
			}
			return new ProjectDetails();
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while deleting Project detail information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving survey information");
			throw new FeasibilityWebException("Error while deleting Project detail information");
		}
	}

	public void getDashboardData(Map<String, Object> dashboardDetails, Model model) {

		@SuppressWarnings("unchecked")
		List<ProjectDetails> projectDetails = (List<ProjectDetails>) dashboardDetails.get("projectDetails");
		boolean approverFlag = UserRoleUtil.isCurrentUserApprover();
		boolean operationFlag = UserRoleUtil.isCurrentUserOperation();
		boolean projectFlag = UserRoleUtil.isCurrentUserProjectTeam();
		boolean bdFlag = UserRoleUtil.isCurrentUserBD();
		if (!ObjectUtils.isEmpty(projectDetails)) {

			for (int i = 0; i < projectDetails.size(); i++) {
				ProjectDetails projectDetailObject = (ProjectDetails) projectDetails.get(i);

				if (Constants.PRE_SIGNING.equalsIgnoreCase(projectDetailObject.getFeasibilityState())) {
					int status = projectDetailObject.getStatus();
					switch (status) {
					case Constants.STEP_1:
					case Constants.STEP_2:
					case Constants.STEP_3:
					case Constants.STEP_4:
					case Constants.STEP_5:
					case Constants.STEP_6:
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_DRAFT_STATUS);
						projectDetailObject.setDashboardEditButtonFlag("Y");
						projectDetailObject.setDashboardDeleteButtonFlag("Y");
						break;
					case Constants.STATUS_SEND_FOR_APPROVAL:
					case Constants.STATUS_APPROVER_1:
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_APPROVAl_STATUS);
						projectDetailObject.setDashboardDownloadButtonFlag("Y");
						break;
					case Constants.STATUS_REJECTED:
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_REJECTED_STATUS);
						projectDetailObject.setDashboardUpdateButtonFlag("Y");
						projectDetailObject.setDashboardArchiveButtonFlag("Y");
						break;
					}
				} else if (Constants.POST_SIGNING.equalsIgnoreCase(projectDetailObject.getFeasibilityState())) {
					int status = projectDetailObject.getStatus();
					switch (status) {
					case Constants.STEP_1:
					case Constants.STEP_2:
					case Constants.STEP_3:
					case Constants.STEP_4:
					case Constants.STEP_5:
					case Constants.STEP_6:
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_DRAFT_STATUS);
						projectDetailObject.setDashboardEditButtonFlag("Y");
						projectDetailObject.setDashboardDeleteButtonFlag("Y");
						break;
					case Constants.STATUS_SEND_FOR_APPROVAL:
					case Constants.STATUS_APPROVER_1:
					case Constants.STATUS_APPROVER_2:
					case Constants.STATUS_APPROVER_3:
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_APPROVAl_STATUS);
						projectDetailObject.setDashboardDownloadButtonFlag("Y");
						break;
					case Constants.POST_STATUS_REJECTED:
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_REJECTED_STATUS);
						projectDetailObject.setDashboardUpdateButtonFlag("Y");
						projectDetailObject.setDashboardArchiveButtonFlag("Y");
						break;
					}

				} else if (Constants.PRE_HANDOVER.equalsIgnoreCase(projectDetailObject.getFeasibilityState())
						&& !(UserRoleUtil.isCurrentUserOperation() || UserRoleUtil.isCurrentUserProjectTeam())) {
					int status = projectDetailObject.getStatus();
					switch (status) {
					case Constants.STEP_1:
					case Constants.STEP_2:
					case Constants.STEP_3:
					case Constants.STEP_4:
					case Constants.STEP_5:
					case Constants.STEP_6:
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_DRAFT_STATUS);
						projectDetailObject.setDashboardEditButtonFlag("Y");
						projectDetailObject.setDashboardDeleteButtonFlag("Y");
						break;
					case Constants.STATUS_SEND_FOR_APPROVAL:
					case Constants.STATUS_APPROVER_1:
					case Constants.STATUS_APPROVER_2:
					case Constants.STATUS_APPROVER_3:
					case Constants.STATUS_APPROVER_4:
					case Constants.STATUS_APPROVER_5:
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_APPROVAl_STATUS);
						projectDetailObject.setDashboardDownloadButtonFlag("Y");
						break;
					case Constants.PRE_HAND_STATUS_REJECTED:
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_REJECTED_STATUS);
						projectDetailObject.setDashboardUpdateButtonFlag("Y");
						projectDetailObject.setDashboardArchiveButtonFlag("Y");
						break;
					}

				}
				if (approverFlag) {

					projectDetailObject.setDashboardReviewButtonFlag("Y");
					projectDetailObject.setDashboardUpdateButtonFlag("N");
					projectDetailObject.setDashboardArchiveButtonFlag("N");
					projectDetailObject.setDashboardEditButtonFlag("N");
					projectDetailObject.setDashboardDeleteButtonFlag("N");
					projectDetailObject.setDashboardDownloadButtonFlag("N");

				} else if (operationFlag) {

					projectDetailObject.setDashboardReviewButtonFlag("Y");
					projectDetailObject.setDashboardUpdateButtonFlag("N");
					projectDetailObject.setDashboardArchiveButtonFlag("N");
					projectDetailObject.setDashboardEditButtonFlag("N");
					projectDetailObject.setDashboardDeleteButtonFlag("N");
					projectDetailObject.setDashboardDownloadButtonFlag("N");

					int status = projectDetailObject.getStatus();
					if ((status == Constants.STEP_5 || projectDetailObject.getIsRejected()) && !projectDetailObject.getIsOperationSubmit()) {
						projectDetailObject
								.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_STATUS_PROJECT_TEAM);
					} else if (projectDetailObject.getIsOperationSubmit()
							&& !projectDetailObject.getIsProjectSubmit()) {
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_FROM_PROJECT);
					} else if (projectDetailObject.getIsProjectSubmit() && projectDetailObject.getIsOperationSubmit() && !(status >= 96)) {
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_FROM_BD);
					} else if (status == Constants.STEP_6) {
						projectDetailObject.setDashboardStatusName(MessageConstants.FEASIBILITY_REPORT_GENERATED);
						projectDetailObject.setDashboardDownloadButtonFlag("Y");
					} else if (status == Constants.STATUS_SEND_FOR_APPROVAL) {
						projectDetailObject.setDashboardStatusName(MessageConstants.SEND_FOR_APPROVAL);
					} else if (status == Constants.STATUS_APPROVER_1) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.STATUS_APPROVER_2) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.STATUS_APPROVER_3) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.STATUS_APPROVER_4) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.STATUS_APPROVER_5) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.STATUS_APPROVER_6) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.PRE_HAND_STATUS_REJECTED) {
						User user = userService.getUserById(projectDetailObject.getRejectedBy());
						projectDetailObject.setDashboardStatusName(MessageConstants.REJECTED_FROM + user.getUsername());
					}

				} else if (projectFlag) {
					projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_STATUS_PROJECT_TEAM);
					projectDetailObject.setDashboardReviewButtonFlag("Y");
					projectDetailObject.setDashboardUpdateButtonFlag("N");
					projectDetailObject.setDashboardArchiveButtonFlag("N");
					projectDetailObject.setDashboardEditButtonFlag("N");
					projectDetailObject.setDashboardDeleteButtonFlag("N");
					projectDetailObject.setDashboardDownloadButtonFlag("N");

					int status = projectDetailObject.getStatus();
					if ((status == Constants.STEP_5 || projectDetailObject.getIsRejected())&& !projectDetailObject.getIsProjectSubmit()) {
						projectDetailObject
								.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_STATUS_PROJECT_TEAM);
					} else if (projectDetailObject.getIsProjectSubmit()
							&& !projectDetailObject.getIsOperationSubmit()) {
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_FROM_OPERATION);
					} else if (projectDetailObject.getIsProjectSubmit() && projectDetailObject.getIsOperationSubmit() && !(status >= 96)) {
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_FROM_BD);
					} else if (status == Constants.STEP_6) {
						projectDetailObject.setDashboardStatusName(MessageConstants.FEASIBILITY_REPORT_GENERATED);
						projectDetailObject.setDashboardDownloadButtonFlag("Y");
					} else if (status == Constants.STATUS_SEND_FOR_APPROVAL) {
						projectDetailObject.setDashboardStatusName(MessageConstants.SEND_FOR_APPROVAL);
					} else if (status == Constants.STATUS_APPROVER_1) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.STATUS_APPROVER_2) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.STATUS_APPROVER_3) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.STATUS_APPROVER_4) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.STATUS_APPROVER_5) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.STATUS_APPROVER_6) {
						User user = userService.getUserByApproverId(status);
						projectDetailObject.setDashboardStatusName(MessageConstants.ACCEPTED_FROM + user.getUsername());
					} else if (status == Constants.PRE_HAND_STATUS_REJECTED) {
						User user = userService.getUserById(projectDetailObject.getRejectedBy());
						projectDetailObject.setDashboardStatusName(MessageConstants.REJECTED_FROM  + user.getUsername());
					}

				} else if (bdFlag) {
					int status = projectDetailObject.getStatus();
					projectDetailObject.setDashboardReviewButtonFlag("N");
					if (projectDetailObject.getIsSendForOpsAndProject() && (!projectDetailObject.getIsOperationSubmit()
							&& !projectDetailObject.getIsProjectSubmit())) {
						projectDetailObject.setDashboardStatusName(MessageConstants.PENDING_FROM_OPS_AND_PROJECT);
						projectDetailObject.setDashboardReviewButtonFlag("Y");
						projectDetailObject.setDashboardEditButtonFlag("N");
						projectDetailObject.setDashboardDeleteButtonFlag("N");
					} else if (projectDetailObject.getIsSendForOpsAndProject()
							&& (projectDetailObject.getIsOperationSubmit()
									&& !projectDetailObject.getIsProjectSubmit())) {
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_FROM_PROJECT);
						projectDetailObject.setDashboardReviewButtonFlag("Y");
						projectDetailObject.setDashboardEditButtonFlag("N");
						projectDetailObject.setDashboardDeleteButtonFlag("N");
					} else if (projectDetailObject.getIsSendForOpsAndProject()
							&& (!projectDetailObject.getIsOperationSubmit()
									&& projectDetailObject.getIsProjectSubmit())) {
						projectDetailObject.setDashboardStatusName(MessageConstants.DASHBORAD_PENDING_FROM_OPERATION);
						projectDetailObject.setDashboardReviewButtonFlag("Y");
						projectDetailObject.setDashboardEditButtonFlag("N");
						projectDetailObject.setDashboardDeleteButtonFlag("N");
					}else if((projectDetailObject.getIsOperationSubmit()
							&& projectDetailObject.getIsProjectSubmit()) && status == Constants.STEP6 && !projectDetailObject.getIsRejected()) {
						projectDetailObject.setDashboardDownloadButtonFlag("Y");
					}

				} else {

				}

			}

			model.addAttribute("projectDetails", projectDetails);
		}

	}

	@SuppressWarnings("unchecked")
	public Map<String, List<ProjectDetails>> getDashoradDetails2(String userid) {
		try {
			String finalUrl = String.format("dashboard/getreport/%s", new Object[] { userid });
			JsonNode response = this.restServiceUtil.makeRequest(finalUrl, null, null, HttpMethod.GET);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return (Map<String, List<ProjectDetails>>) OBJECT_MAPPER.readValue(data, new TypeReference<Map<String, List<ProjectDetails>>>() {});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for dashboard on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching dashboard details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching dashboard details");
			throw new FeasibilityWebException("Error while fetching dashboard details");
		}
	}
	  
	public void getDashboardData2(Map<String, List<ProjectDetails>> dashboardDetails, ModelAndView model) {
		List<ProjectDetails> projectDetailsmodel = new ArrayList<>();
		@SuppressWarnings("unchecked")
		List<ProjectDetails> projectDetails = (List<ProjectDetails>) dashboardDetails.get("projectDetails");
		boolean bdFlag = true;
		if (!ObjectUtils.isEmpty(projectDetails)) {
			for (int i = 0; i < projectDetails.size(); i++) {
				ProjectDetails projectDetailObject = projectDetails.get(i);
				if ("pre_signing".equalsIgnoreCase(projectDetailObject.getFeasibilityState())) {
					int status = projectDetailObject.getStatus();
					switch (status) {
					case 91:
					case 92:
					case 93:
					case 94:
					case 95:
					case 96:
						projectDetailObject.setDashboardStatusName("Draft");
						projectDetailObject.setDashboardEditButtonFlag("Y");
						projectDetailObject.setDashboardDeleteButtonFlag("Y");
						break;
					case 105:
					case 106:
						projectDetailObject.setDashboardStatusName("Approval Pending");
						projectDetailObject.setDashboardDownloadButtonFlag("Y");
						break;
					case 108:
						projectDetailObject.setDashboardStatusName("Rejected");
						projectDetailObject.setDashboardUpdateButtonFlag("Y");
						projectDetailObject.setDashboardArchiveButtonFlag("Y");
						break;
					}
				} else if ("post_signing".equalsIgnoreCase(projectDetailObject.getFeasibilityState())) {
					int status = projectDetailObject.getStatus();
					switch (status) {
					case 91:
					case 92:
					case 93:
					case 94:
					case 95:
					case 96:
						projectDetailObject.setDashboardStatusName("Draft");
						projectDetailObject.setDashboardEditButtonFlag("Y");
						projectDetailObject.setDashboardDeleteButtonFlag("Y");
						break;
					case 105:
					case 106:
					case 107:
					case 108:
						projectDetailObject.setDashboardStatusName("Approval Pending");
						projectDetailObject.setDashboardDownloadButtonFlag("Y");
						break;
					case 208:
						projectDetailObject.setDashboardStatusName("Rejected");
						projectDetailObject.setDashboardUpdateButtonFlag("Y");
						projectDetailObject.setDashboardArchiveButtonFlag("Y");
						break;
					}
				} else if ("pre_handover".equalsIgnoreCase(projectDetailObject.getFeasibilityState())
						&& !UserRoleUtil.isCurrentUserOperation() && !UserRoleUtil.isCurrentUserProjectTeam()) {
					int status = projectDetailObject.getStatus();
					switch (status) {
					case 91:
					case 92:
					case 93:
					case 94:
					case 95:
					case 96:
						projectDetailObject.setDashboardStatusName("Draft");
						projectDetailObject.setDashboardEditButtonFlag("Y");
						projectDetailObject.setDashboardDeleteButtonFlag("Y");
						break;
					case 105:
					case 106:
					case 107:
					case 108:
					case 109:
					case 110:
						projectDetailObject.setDashboardStatusName("Approval Pending");
						projectDetailObject.setDashboardDownloadButtonFlag("Y");
						break;
					case 308:
						projectDetailObject.setDashboardStatusName("Rejected");
						projectDetailObject.setDashboardUpdateButtonFlag("Y");
						projectDetailObject.setDashboardArchiveButtonFlag("Y");
						break;
					}
				}
				if (bdFlag) {
					int status = projectDetailObject.getStatus();
					projectDetailObject.setDashboardReviewButtonFlag("N");
					if (projectDetailObject.getIsSendForOpsAndProject().booleanValue()
							&& !projectDetailObject.getIsOperationSubmit().booleanValue()
							&& !projectDetailObject.getIsProjectSubmit().booleanValue()) {
						projectDetailObject.setDashboardStatusName("Pending with ops and project Team");
						projectDetailObject.setDashboardReviewButtonFlag("Y");
						projectDetailObject.setDashboardEditButtonFlag("N");
						projectDetailObject.setDashboardDeleteButtonFlag("N");
					} else if (projectDetailObject.getIsSendForOpsAndProject().booleanValue()
							&& projectDetailObject.getIsOperationSubmit().booleanValue()
							&& !projectDetailObject.getIsProjectSubmit().booleanValue()) {
						projectDetailObject.setDashboardStatusName("Pending with Project Team");
						projectDetailObject.setDashboardReviewButtonFlag("Y");
						projectDetailObject.setDashboardEditButtonFlag("N");
						projectDetailObject.setDashboardDeleteButtonFlag("N");
					} else if (projectDetailObject.getIsSendForOpsAndProject().booleanValue()
							&& !projectDetailObject.getIsOperationSubmit().booleanValue()
							&& projectDetailObject.getIsProjectSubmit().booleanValue()) {
						projectDetailObject.setDashboardStatusName("Pending with Operation");
						projectDetailObject.setDashboardReviewButtonFlag("Y");
						projectDetailObject.setDashboardEditButtonFlag("N");
						projectDetailObject.setDashboardDeleteButtonFlag("N");
					} else if (projectDetailObject.getIsOperationSubmit().booleanValue()
							&& projectDetailObject.getIsProjectSubmit().booleanValue()
							&& status == Constants.STEP6.intValue()
							&& !projectDetailObject.getIsRejected().booleanValue()) {
						projectDetailObject.setDashboardDownloadButtonFlag("Y");
					}
				}
				projectDetailsmodel.add(projectDetailObject);
			}
			model.addObject("projectDetails", projectDetailsmodel);
		}
	}

	@SuppressWarnings("unchecked")
	public Map<Object, Object> setReport(BDData bdata) {
		if (ObjectUtils.isEmpty(bdata))
			throw new FeasibilityWebException("BD Data info cannot be empty");
		try {
			JsonNode response = this.restServiceUtil.makeRequest("dashboard/setreport/", bdata, null, HttpMethod.POST);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return (Map<Object, Object>) OBJECT_MAPPER.readValue(data, new TypeReference<Map<Object, Object>>(){});
				}
				return new HashMap<>();
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new FeasibilityWebException("Response Converson error for Atp-Cap on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Atp-Cap information", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Atp-Cap information");
			throw new FeasibilityWebException("Error while saving Atp-Cap information");
		}
	}

}
