package com.services;

import java.util.List;

import com.common.mappers.FhcMapper;
import com.common.mappers.ServiceChargeMapper;
import com.common.models.CinemaCategoryMaster;
import com.common.models.CinemaFormatMaster;
import com.common.models.FixedConstant;
import com.common.models.SeatTypeMaster;

public interface FixedConstantsService {
	
	public FixedConstant getFixedConstants();
	
	public FixedConstant saveServiceChargePerTickets(ServiceChargeMapper fixedConstant);
	
	public FixedConstant saveCapexsAssumption(FixedConstant fixedConstant);
	
	public FixedConstant saveTaxsAssumption(FixedConstant fixedConstant);
	
	public FixedConstant saveOpeartingsAssumption(FixedConstant fixedConstant);
	
	public FixedConstant saveGrowthAssumptions(FixedConstant fixedConstant) ;
	
	public FixedConstant saveAtpCap(FixedConstant fixedConstant) ;
	public FixedConstant irrConstantDetails(FixedConstant fixedConstant);

	public Boolean saveFhc(FhcMapper fhcMapper);

	public boolean isFhcPresent(FhcMapper fhcMapper);

	public boolean deleteAtpCap(FixedConstant fixedConstant);
	
	public List<CinemaFormatMaster> getAllCinemaFormat();

	public CinemaFormatMaster getCinemaFormat(String id);
	
	public Boolean saveCinemaFormat(CinemaFormatMaster cinemaCategoryMaster);
	
	public List<CinemaCategoryMaster> getAllCinemaCategory();

	public CinemaCategoryMaster getCinemaCategory(String id);
	
	public Boolean saveCinemaCategory(CinemaCategoryMaster cinemaCategoryMaster);
	
	public List<SeatTypeMaster> getAllSeatType();

	public SeatTypeMaster getSeatType(String id);
	
	public Boolean saveSeatType(SeatTypeMaster seatTypeMaster);

	public Boolean saveCogs(FhcMapper fhcMapper);
	
	public List<CinemaFormatMaster> getFormatMappingViewList(List<CinemaFormatMaster> cinemaFormatList,String id,CinemaFormatMaster cinemaFormatMappingMaster);

	public Boolean deleteOpearatingAssumption(FhcMapper fhcMapper);

	public Boolean saveGstOnTicket(FhcMapper fhcMapper);

	public Boolean saveScreenType(CinemaFormatMaster cinemaCategoryMaster);
	
}