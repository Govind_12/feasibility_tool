package com.services;

import org.springframework.web.multipart.MultipartFile;

import com.common.mappers.UploadFilesMapper;
import com.common.models.ProjectCostSummary;

public interface DataSourceService {

	void saveDataSource(UploadFilesMapper uploadFilesMapper);

	ProjectCostSummary getProjectCost(UploadFilesMapper uploadFilesMapper);

	
}
