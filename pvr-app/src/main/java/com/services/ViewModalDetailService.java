package com.services;

import java.util.ArrayList;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface ViewModalDetailService {

	
	Map<String, ? super ArrayList> getTotalAdmits(String projectId);
	Map<String, ? super ArrayList> getAdRevenue(String projectId);
	Map<String, ? super ArrayList> getPersonalExpense(String projectId);
	Map<String, ? super ArrayList> getElectricityExpense(String projectId);
	Map<String, ? super ArrayList> getRmExpense(String projectId);
	Map<String, ? super ArrayList> getWebSales(String projectId);
	Map<String, ? super ArrayList> getHousekeeping(String projectId);
	Map<String, ? super ArrayList> getSecurityExpense(String projectId);
	Map<String, ? super ArrayList> getCommunicationExpense(String projectId);
	Map<String, ? super ArrayList> getTravellingExpense(String projectId);
	Map<String, ? super ArrayList> getLeegalfee(String projectId);
	Map<String, ? super ArrayList> getInsuranceExpense(String projectId);
	Map<String, ? super ArrayList> getInternetExpenses(String projectId);
	Map<String, ? super ArrayList> getPrintingStationary(String projectId);
	Map<String, ? super ArrayList> getMarketingExpense(String projectId);
	Map<String, ? super ArrayList> getMiscellaneousExpense(String projectId);
	Map<String, ? super ArrayList> getAnnualRent(String projectId);
	Map<String, ? super Object> getViewCalculation(String projectId);
	Map<String, ? super Object> getViewOccupancy(String projectId);
}
