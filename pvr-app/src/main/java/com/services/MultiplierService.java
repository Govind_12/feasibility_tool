package com.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.common.models.Company;
import com.common.models.MultipliersMaster;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.MarkConstant;
import com.util.RestServiceUtil;
import com.util.URLConstants;
import com.web.FeasibilityWebException;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MultiplierService {
	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@Autowired
	private RestServiceUtil restServiceUtil;

	public MultipliersMaster saveMultiplier(MultipliersMaster multiplier) {
		if (ObjectUtils.isEmpty(multiplier)) {
			throw new FeasibilityWebException("multiplier info cannot be empty");
		}
		int status;
		try {

			JsonNode response = restServiceUtil.makeRequest(URLConstants.SAVE_MULTIPLIER_DETAILS, multiplier, null,
					HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<MultipliersMaster>() {
					});
				}
				return new MultipliersMaster();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Multipliers on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while saving Multipliers information", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while saving Multipliers information");
			throw new FeasibilityWebException("Error while saving Multipliers information");
		}
	}

	public List<MultipliersMaster> getAllMultipliers() {

		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_MUTIPLIERS, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<MultipliersMaster>>() {
					});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Multipliers on web.", 500);
			}

		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching all Multipliers details", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching all Multipliers details");
			throw new FeasibilityWebException("Error while fetching all Multipliers details");
		}
	}

	public MultipliersMaster getMultiplierById(String multiplierId) {

		int status;
		String url = String.format(URLConstants.FETCH_MUTIPLIER_BY_ID, multiplierId);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<MultipliersMaster>() {
					});
				}
				return new MultipliersMaster();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Multipliers on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Multipliers by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Multipliers by id");
			throw new FeasibilityWebException("Error while fetching Multipliers by id");
		}
	}

	public boolean isMultiplierPresent(MultipliersMaster multiplier) {
		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.IF_MULTIPLIER_PRESENT, multiplier, null, HttpMethod.POST);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<Boolean>() {
					});
				}
				return  false;
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Multipliers on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching Multipliers by id", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching Multipliers by id");
			throw new FeasibilityWebException("Error while fetching Multipliers by id");
		}
		
	}
}
