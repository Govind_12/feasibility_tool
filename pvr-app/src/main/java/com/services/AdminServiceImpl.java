package com.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.common.models.ProjectDetails;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.RestServiceUtil;
import com.web.FeasibilityWebException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AdminServiceImpl implements AdminService{

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@Autowired
	private RestServiceUtil restServiceUtil;

	@SuppressWarnings("unchecked")
	public List<ProjectDetails> getOpsReports() {
		try {
			JsonNode response = this.restServiceUtil.makeRequest("admin/getOpsReports", null, null, HttpMethod.GET);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return (List<ProjectDetails>) OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for dashboard on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching dashboard details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching dashboard details");
			throw new FeasibilityWebException("Error while fetching dashboard details");
		}
	}

	@SuppressWarnings("unchecked")
	public List<ProjectDetails> updateOpsReports(List<Integer> ids) {
		try {
			Map<String, List<Integer>> idmap = new HashMap<>();
			idmap.put("ids", ids);
			JsonNode response = this.restServiceUtil.makeRequest("admin/updateOpsReports", idmap, null,
					HttpMethod.POST);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return (List<ProjectDetails>) OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for dashboard on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching dashboard details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching dashboard details");
			throw new FeasibilityWebException("Error while fetching dashboard details");
		}
	}

	@SuppressWarnings("unchecked")
	public List<ProjectDetails> getExitReports() {
		try {
			JsonNode response = this.restServiceUtil.makeRequest("admin/getExitReports", null, null, HttpMethod.GET);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return (List<ProjectDetails>) OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for dashboard on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching dashboard details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching dashboard details");
			throw new FeasibilityWebException("Error while fetching dashboard details");
		}
	}

	@SuppressWarnings("unchecked")
	public List<ProjectDetails> updateExtReports(List<Integer> ids) {
		try {
			Map<String, List<Integer>> idmap = new HashMap<>();
			idmap.put("ids", ids);
			JsonNode response = this.restServiceUtil.makeRequest("admin/updateExtReports", idmap, null,
					HttpMethod.POST);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return (List<ProjectDetails>) OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for dashboard on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching dashboard details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching dashboard details");
			throw new FeasibilityWebException("Error while fetching dashboard details");
		}
	}

	public List<ProjectDetails> getReportsForChangeProjName() {
		try {
			JsonNode response = this.restServiceUtil.makeRequest("admin/getListForProjName", null, null,
					HttpMethod.GET);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String data = response.get("data").toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<ProjectDetails>>() {});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for dashboard on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching dashboard details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching dashboard details");
			throw new FeasibilityWebException("Error while fetching dashboard details");
		}
	}

	public List<ProjectDetails> updateProjectNames(Map<String, String> data) {
		if (ObjectUtils.isEmpty(data))
			throw new FeasibilityWebException("data info cannot be empty");
		try {
			JsonNode response = this.restServiceUtil.makeRequest("admin/updateProjName", data, null, HttpMethod.POST);
			int status = response.get("status").intValue();
			String errorMsg = response.get("message").asText();
			if (status != 200)
				throw new FeasibilityWebException(errorMsg, status);
			try {
				if (!ObjectUtils.isEmpty(response.get("data"))) {
					String projdata = response.get("data").toString();
					return OBJECT_MAPPER.readValue(projdata, new TypeReference<List<ProjectDetails>>() {});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for Project detail on web.", 500);
			}
		} catch (FeasibilityWebException e) {
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while rejecting project details", 401);
			}
			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}
			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while rejecting project details");
			throw new FeasibilityWebException("Error while rejecting project details");
		}
	}

}
