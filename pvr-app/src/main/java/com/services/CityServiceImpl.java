package com.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.common.models.City;
import com.common.models.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.util.MarkConstant;
import com.util.RestServiceUtil;
import com.util.URLConstants;
import com.web.FeasibilityWebException;

import lombok.extern.slf4j.Slf4j;

@Service("cityServiceimpl")
@Slf4j
public class CityServiceImpl  implements CityService{
	
	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@Autowired
	private RestServiceUtil restServiceUtil;

	
	public List<City> getAllCities() {
		int status;
		try {
			JsonNode response = restServiceUtil.makeRequest(URLConstants.GET_ALL_CITIES, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();

					return OBJECT_MAPPER.readValue(data, new TypeReference<List<City>>() {
					});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for cities on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			e.printStackTrace();
			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching cities", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			log.error("Error while fetching cities");
			throw new FeasibilityWebException("Error while fetching cities");
		}
	}


	@Override
	public List<City> getStateBycityName(String city) {

		int status;
		String url = String.format(URLConstants.GET_STATE_BY_CITY, city);
		try {
			JsonNode response = restServiceUtil.makeRequest(url, null, null, HttpMethod.GET);

			status = response.get(MarkConstant.STATUS_CODE).intValue();
			String errorMsg = response.get(MarkConstant.MESSAGE).asText();
			if (status != 200) {
				throw new FeasibilityWebException(errorMsg, status);
			}

			try {
				if (!ObjectUtils.isEmpty(response.get(MarkConstant.DATA))) {
					String data = response.get(MarkConstant.DATA).toString();
					return OBJECT_MAPPER.readValue(data, new TypeReference<List<City>>() {
					});
				}
				return new ArrayList<>();
			} catch (IOException ex) {
				throw new FeasibilityWebException("Response Converson error for State on web.", 500);
			}
		} catch (FeasibilityWebException e) {

			String errorCode = e.getMessage();
			if (errorCode.equals("401")) {
				log.error("Token Expired.");
				throw new FeasibilityWebException("Error while fetching State by city", 401);
			}

			if (errorCode.equals("403")) {
				log.error("Access Denied this api.");
				throw new FeasibilityWebException("Access Denied this api.", 403);
			}

			if (e.code == 500) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}

			if (e.code == 404) {
				log.error(e.getMessage());
				throw new FeasibilityWebException(e.getMessage(), e.code);
			}
			log.error("Error while fetching User by id");
			throw new FeasibilityWebException("Error while fetching State by city");
		}
	}
	
}
