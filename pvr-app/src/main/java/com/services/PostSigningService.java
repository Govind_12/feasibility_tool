package com.services;

import java.util.List;
import java.util.Optional;

import com.common.mappers.MsgMapper;
import com.common.mappers.QueryMapper;
import com.common.models.ProjectDetails;

public interface PostSigningService {

	public ProjectDetails savePostSigningStep1(ProjectDetails projectDetails);
	
	public List<ProjectDetails> getAllExistingProjects();
	
	public  QueryMapper submitQuery(MsgMapper msgMapper);

	public ProjectDetails getProjectById(String projectId);
	
	public ProjectDetails updateProject(String projectId);


	
}
