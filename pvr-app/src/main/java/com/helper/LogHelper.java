package com.helper;

import lombok.Data;

@Data
public class LogHelper {

	private String id;
	private String requestTime;
	private String requestURI;
	private String requestIP;
	private String requestBrowser;
	private String requestStatus;
	private String exceptionInCase;
}
