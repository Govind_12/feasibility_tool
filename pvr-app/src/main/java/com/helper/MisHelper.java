package com.helper;

import lombok.Data;

@Data
public class MisHelper {

	private String startDate;
	private String endDate;
}
